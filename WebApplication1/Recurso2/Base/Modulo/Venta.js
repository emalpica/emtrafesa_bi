﻿//Dinero Genreado en ventas: Millones de Soles


function Grafica_Ventas_VVA_S(Marcas, Data, divGrafica) {
    var chart = Highcharts.chart(divGrafica, {
        chart: {
            type: 'bar',
            spacingTop: 2,
        },
        title: {
            text: 'DINERO GENERADO EN VENTAS',
            align: 'left',
            x: 0,
        },
        subtitle: {
            text: ''
        },
        yAxis: {
            title: {
                text: 'Millones de soles'
            },
            labels: {
                formatter: function () {
                    return (this.value) + 'M';
                }
            }
        },
        xAxis: {
            categories: Marcas//['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
        },
        chart: {
            inverted: true,
            polar: false
        },
        plotOptions: {
            series: {
                stacking: 'normal',
                dataLabels: {
                    enabled: true,
                    style: {
                        "font-family": "'Open Sans', sans-serif",
                        "-webkit-text-stroke": "2px black",
                        "color": "#ffff",
                        "fontSize": "12px",
                    },
                    format: '{point.y:.2f} M'
                }
            }
        },
        tooltip: {
            headerFormat: '<b>{point.x}</b><br/>',
            pointFormat: '{series.name}: {point.y:.2f}M<br/>'
        },
        series: [{
            format: '{point.y:.2f}',
            name: 'Cantidad',
            type: 'column',
            colorByPoint: true,
            data: Data, //[29.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4],
            showInLegend: false
        }]
    });
}



function Grafica_Ventas_VVA_P(Marcas, Data, divGrafica ) {
    Highcharts.chart(divGrafica, {
        chart: {
            type: 'bar',
            spacingTop: 6,
        },
        title: {
            text: 'PASAJES VENDIDOS',
            align: 'left',
            x: 0,
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: Marcas//['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
        },
        yAxis: {
            title: {
                text: 'Cantidad de pasajes en millones'
            },
            labels: {
                formatter: function () {
                    return (this.value) + 'M';
                }
            }
        },
        plotOptions: {
            series: {
                stacking: 'normal',
                dataLabels: {
                    enabled: true,
                    style: {
                        "font-family": "'Open Sans', sans-serif",
                        "-webkit-text-stroke": "2px black",
                        "color": "#ffff",
                        "fontSize": "12px",
                    },
                    format: '{point.y:.2f} M'
                }
            }
        },
        tooltip: {
            headerFormat: '<b>{point.x}</b><br/>',
            pointFormat: '{series.name}: {point.y:.2f}M<br/>'
        },
        chart: {
            inverted: true,
            polar: false
        },

        series: [{
            format: '{point.y:.2f}',
            name: 'Cantidad',
            type: 'column',
            colorByPoint: true,
            data: Data, //[29.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4],
            showInLegend: false
        }]
    });
}