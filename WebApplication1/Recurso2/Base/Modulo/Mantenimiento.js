﻿//KPI: PRODUCTIVIDAD
function Graficar_Productividad_KPI(fecha, Name, data, idDiv) {
    Highcharts.chart(idDiv, {
        chart: {
            type: 'line'
        },
        title: {
            text: 'KPI DE PRODUCTIVIDAD DE PERSONAL DE MANTENIMIENTO'
        },
        subtitle: {
            text: 'Meses'
        },
        xAxis: {
            categories: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']//JSON.parse(fecha)
        },
        yAxis: {
            min: 0,
            title: {
                text: ''
            },
            labels: {
                formatter: function () {
                    return (this.value) + ' %';
                }
            },
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true,
                    format: '{point.y} %'
                },
                enableMouseTracking: false
            }
        },
        series: JSON.parse(data)
    });
}

//FALLA DEL SISTEMA:
function PieChart(series, idDiv, titulo2) {
    Highcharts.chart(idDiv, {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: titulo2
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: false
            },
            showInLegend: true
        },
        //},
        series: [JSON.parse(series)]
    });
}