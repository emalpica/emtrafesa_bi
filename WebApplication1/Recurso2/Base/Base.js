﻿function fn_LlamadoMetodo(url, data, success, error, complete)
{
    $.ajax({
        type: "POST",
        url: url,
        data: data,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: success,
        error: error,
        complete: complete
    });
}

function fn_Mensaje_Alerta(tipo, mensaje, data, idDiv) {
    var mensaje_resultado = "";
    if (tipo == "error") {
        mensaje_resultado =
            "<div id=\"toast-container\" class=\"toast-top-center\">" +
            "    <div class=\"toast toast-error\" aria-live=\"assertive\" style=\"display: block;\">" +
            "        <div class=\"toast-message\">" + mensaje + data + "</div>" +
            "    </div>" +
            "</div>";
    }
    if (tipo == "alerta")
    {
     mensaje_resultado =
     "<div id=\"toast-container\" class=\"toast-top-center\">"+
     "  <div class=\"toast toast-warning\" aria-live=\"assertive\" style=\"display: block;\">"+
     "    <div class=\"toast-message\">" + mensaje + data + "</div>" +
     "  </div>" +
     "</div>";
    }
    if (tipo == "confirmacion")
    {
     mensaje_resultado =
     "<div id=\"toast-container\" class=\"toast-top-center\">" +
     "    <div class=\"toast toast-success\" aria-live=\"polite\" style=\"display: block; \">" +
     "        <div class=\"toast-message\">" + mensaje + data + "</div>" +
     "    </div>" +
     "</div>";
    }
    if (tipo == "general") {
     mensaje_resultado =
     "<div id=\"toast-container\" class=\"toast-top-center\">"+
     "    <div class=\"toast toast-info\" aria-live=\"polite\" style=\"display: block; \">" +
     "        <div class=\"toast-message\">" + mensaje + data + "</div>" +
     "    </div>" +
     "</div>";
    }
    $("[id$=" + idDiv + "]").append(mensaje_resultado);
    $("[id$=" + idDiv + "]").fadeIn();
    setTimeout(function () {
        $("[id$=" + idDiv + "]").fadeOut();
    }, 2000);
}

function fn_ValidarANulos(json) {
    var json = JSON.parse(json);
    for (let i = 0; i < json.length; i++) {
        var x = json[i].data;
        for (let j = 0; j < x.length; j++) {
            if (x[j] === 0) {
                x[j] = null;
            }
            else {
                x[j] = x[j];
            }
        }
    }
    var json = JSON.stringify(json);
    return json;
}
