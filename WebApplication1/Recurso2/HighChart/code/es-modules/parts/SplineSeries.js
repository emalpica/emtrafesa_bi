/* *
 * (c) 2010-2019 Torstein Honsi
 *
 * License: www.highcharts.com/license
 */

'use strict';

import H from './Globals.js';
import './Utilities.js';
import './Color.js';
import './Legend.js';
import './Series.js';
import './Options.js';

var color = H.color,
    LegendSymbolMixin = H.LegendSymbolMixin,
    pick = H.pick,
    Series = H.Series,
    seriesType = H.seriesType;

/**
 * Area series type.
 *
 * @private
 * @class
 * @name Highcharts.seriesTypes.area
 *
 * @augments Highcharts.Series
 */
seriesType(
    'area',
    'line',

    /**
     * The area series type.
     *
     * @sample {highcharts} highcharts/demo/area-basic/
     *         Area chart
     * @sample {highstock} stock/demo/area/
     *         Area chart
     *
     * @extends      plotOptions.line
     * @excluding    useOhlcData
     * @product      highcharts highstock
     * @optionparent plotOptions.area
     */
    {

        /**
         * Fill color or gradient for the area. When `null`, the series' `color`
         * is used with the series' `fillOpacity`.
         *
         * In styled mode, the fill color can be set with the `.highcharts-area`
         * class name.
         *
         * @sample {highcharts} highcharts/plotoptions/area-fillcolor-default/
         *         Null by default
         * @sample {highcharts} highcharts/plotoptions/area-fillcolor-gradient/
         *         Gradient
         *
         * @type      {Highcharts.ColorString|Highcharts.GradientColorObject|Highcharts.PatternObject}
         * @product   highcharts highstock
         * @apioption plotOptions.area.fillColor
         */

        /**
         * Fill opacity for the area. When you set an explicit `fillColor`,
         * the `fillOpacity` is not applied. Instead, you should define the
         * opacity in the `fillColor` with an rgba color definition. The
         * `fillOpacity` setting, also the default setting, overrides the alpha
         * component of the `color` setting.
         *
         * In styled mode, the fill opacity can be set with the
         * `.highcharts-area` class name.
         *
         * @sample {highcharts} highcharts/plotoptions/area-fillopacity/
         *         Automatic fill color and fill opacity of 0.1
         *
         * @type      {number}
         * @default   {highcharts} 0.75
         * @default   {highstock} 0.75
         * @product   highcharts highstock
         * @apioption plotOptions.area.fillOpacity
         */

        /**
         * A separate color for the graph line. By default the line takes the
         * `color` of the series, but the lineColor setting allows setting a
         * separate color for the line without altering the `fillColor`.
         *
         * In styled mode, the line stroke can be set with the
         * `.highcharts-graph` class name.
         *
         * @sample {highcharts} highcharts/plotoptions/area-linecolor/
         *         Dark gray line
         *
         * @type      {Highcharts.ColorString}
         * @product   highcharts highstock
         * @apioption plotOptions.area.lineColor
         */

        /**
         * A separate color for the negative part of the area.
         *
         * In styled mode, a negative color is set with the
         * `.highcharts-negative` class name.
         *
         * @see [negativeColor](#plotOptions.area.negativeColor)
         *
         * @sample {highcharts} highcharts/css/series-negative-color/
         *         Negative color in styled mode
         *
         * @type      {Highcharts.ColorString|Highcharts.GradientColorObject|Highcharts.PatternObject}
         * @since     3.0
         * @product   highcharts
         * @apioption plotOptions.area.negativeFillColor
         */

        /**
         * Whether the whole area or just the line should respond to mouseover
         * tooltips and other mouse or touch events.
         *
         * @sample {highcharts|highstock} highcharts/plotoptions/area-trackbyarea/
         *         Display the tooltip when the area is hovered
         *
         * @type      {boolean}
         * @default   false
         * @since     1.1.6
         * @product   highcharts highstock
         * @apioption plotOptions.area.trackByArea
         */

        /**
         * When this is true, the series will not cause the Y axis to cross
         * the zero plane (or [threshold](#plotOptions.series.threshold) option)
         * unless the data actually crosses the plane.
         *
         * For example, if `softThreshold` is `false`, a series of 0, 1, 2,
         * 3 will make the Y axis show negative values according to the
         * `minPadding` option. If `softThreshold` is `true`, the Y axis starts
         * at 0.
         *
         * @since   4.1.9
         * @product highcharts highstock
         */
        softThreshold: false,

        /**
         * The Y axis value to serve as the base for the area, for
         * distinguishing between values above and below a threshold. The area
         * between the graph and the threshold is filled.
         *
         * * If a number is given, the Y axis will scale to the threshold.
         * * If `null`, the scaling behaves like a line series with fill between
         *   the graph and the Y axis minimum.
         * * If `Infinity` or `-Infinity`, the area between the graph and the
         *   corresponing Y axis extreme is filled (since v6.1.0).
         *
         * @sample {highcharts} highcharts/plotoptions/area-threshold/
         *         A threshold of 100
         * @sample {highcharts} highcharts/plotoptions/area-threshold-infinity/
         *         A threshold of Infinity
         *
         * @since   2.0
         * @product highcharts highstock
         */
        threshold: 0


    },
    /**
     * @lends seriesTypes.area.prototype
     */
    {

        singleStacks: false,

        // Return an array of stacked points, where null and missing points are
        // replaced by dummy points in order for gaps to be drawn correctly in
        // stacks.
        getStackPoints: function (points) {
            var series = this,
                segment = [],
                keys = [],
                xAxis = this.xAxis,
                yAxis = this.yAxis,
                stack = yAxis.stacks[this.stackKey],
                pointMap = {},
                seriesIndex = series.index,
                yAxisSeries = yAxis.series,
                seriesLength = yAxisSeries.length,
                visibleSeries,
                upOrDown = pick(yAxis.options.reversedStacks, true) ? 1 : -1,
                i;


            points = points || this.points;

            if (this.options.stacking) {

                for (i = 0; i < points.length; i++) {
                // Reset after point update (#7326)
                    points[i].leftNull = points[i].rightNull = null;

                    // Create a map where we can quickly look up the points by
                    // their X values.
                    pointMap[points[i].x] = points[i];
                }

                // Sort the keys (#1651)
                H.objectEach(stack, function (stackX, x) {
                // nulled after switching between
                // grouping and not (#1651, #2336)
                    if (stackX.total !== null) {
                        keys.push(x);
                    }
                });
                keys.sort(function (a, b) {
                    return a - b;
                });

                visibleSeries = yAxisSeries.map(function (s) {
                    return s.visible;
                });

                keys.forEach(function (x, idx) {
                    var y = 0,
                        stackPoint,
                        stackedValues;

                    if (pointMap[x] && !pointMap[x].isNull) {
                        segment.push(pointMap[x]);

                        // Find left and right cliff. -1 goes left, 1 goes
                        // right.
                        [-1, 1].forEach(function (direction) {
                            var nullName = direction === 1 ?
                                    'rightNull' :
  ow-true/
 *         Numerical values
 * @sample {highcharts} highcharts/series/data-array-of-arrays/
 *         Arrays of numeric x and y
 * @sample {highcharts} highcharts/series/data-array-of-arrays-datetime/
 *         Arrays of datetime x and y
 * @sample {highcharts} highcharts/series/data-array-of-name-value/
 *         Arrays of point.name and y
 * @sample {highcharts} highcharts/series/data-array-of-objects/
 *         Config objects
 *
 * @type      {Array<number|Array<(number|string),(number|null)>|null|*>}
 * @extends   series.line.data
 * @product   highcharts highstock
 * @apioption series.spline.data
 */
