/* *
 * (c) 2010-2019 Torstein Honsi
 *
 * License: www.highcharts.com/license
 */

'use strict';

import H from './Globals.js';
import './Utilities.js';
import './Color.js';
import './Legend.js';
import './Series.js';
import './Options.js';

var color = H.color,
    LegendSymbolMixin = H.LegendSymbolMixin,
    pick = H.pick,
    Series = H.Series,
    seriesType = H.seriesType;

/**
 * Area series type.
 *
 * @private
 * @class
 * @name Highcharts.seriesTypes.area
 *
 * @augments Highcharts.Series
 */
seriesType(
    'area',
    'line',

    /**
     * The area series type.
     *
     * @sample {highcharts} highcharts/demo/area-basic/
     *         Area chart
     * @sample {highstock} stock/demo/area/
     *         Area chart
     *
     * @extends      plotOptions.line
     * @excluding    useOhlcData
     * @product      highcharts highstock
     * @optionparent plotOptions.area
     */
    {

        /**
         * Fill color or gradient for the area. When `null`, the series' `color`
         * is used with the series' `fillOpacity`.
         *
         * In styled mode, the fill color can be set with the `.highcharts-area`
         * class name.
         *
         * @sample {highcharts} highcharts/plotoptions/area-fillcolor-default/
         *         Null by default
         * @sample {highcharts} highcharts/plotoptions/area-fillcolor-gradient/
         *         Gradient
         *
         * @type      {Highcharts.ColorString|Highcharts.GradientColorObject|Highcharts.PatternObject}
         * @product   highcharts highstock
         * @apioption plotOptions.area.fillColor
         */

        /**
         * Fill opacity for the area. When you set an explicit `fillColor`,
         * the `fillOpacity` is not applied. Instead, you should define the
         * opacity in the `fillColor` with an rgba color definition. The
         * `fillOpacity` setting, also the default setting, overrides the alpha
         * component of the `color` setting.
         *
         * In styled mode, the fill opacity can be set with the
         * `.highcharts-area` class name.
         *
         * @sample {highcharts} highcharts/plotoptions/area-fillopacity/
         *         Automatic fill color and fill opacity of 0.1
         *
         * @type      {number}
         * @default   {highcharts} 0.75
         * @default   {highstock} 0.75
         * @product   highcharts highstock
         * @apioption plotOptions.area.fillOpacity
         */

        /**
         * A separate color for the graph line. By default the line takes the
         * `color` of the series, but the lineColor setting allows setting a
         * separate color for the line without altering the `fillColor`.
         *
         * In styled mode, the line stroke can be set with the
         * `.highcharts-graph` class name.
         *
         * @sample {highcharts} highcharts/plotoptions/area-linecolor/
         *         Dark gray line
         *
         * @type      {Highcharts.ColorString}
         * @product   highcharts highstock
         * @apioption plotOptions.area.lineColor
         */

        /**
         * A separate color for the negative part of the area.
         *
         * In styled mode, a negative color is set with the
         * `.highcharts-negative` class name.
         *
         * @see [negativeColor](#plotOptions.area.negativeColor)
         *
         * @sample {highcharts} highcharts/css/series-negative-color/
         *         Negative color in styled mode
         *
         * @type      {Highcharts.ColorString|Highcharts.GradientColorObject|Highcharts.PatternObject}
         * @since     3.0
         * @product   highcharts
         * @apioption plotOptions.area.negativeFillColor
         */

        /**
         * Whether the whole area or just the line should respond to mouseover
         * tooltips and other mouse or touch events.
         *
         * @sample {highcharts|highstock} highcharts/plotoptions/area-trackbyarea/
         *         Display the tooltip when the area is hovered
         *
         * @type      {boolean}
         * @default   false
         * @since     1.1.6
         * @product   highcharts highstock
         * @apioption plotOptions.area.trackByArea
         */

        /**
         * When this is true, the series will not cause the Y axis to cross
         * the zero plane (or [threshold](#plotOptions.series.threshold) option)
         * unless the data actually crosses the plane.
         *
         * For example, if `softThreshold` is `false`, a series of 0, 1, 2,
         * 3 will make the Y axis show negative values according to the
         * `minPadding` option. If `softThreshold` is `true`, the Y axis starts
         * at 0.
         *
         * @since   4.1.9
         * @product highcharts highstock
         */
        softThreshold: false,

        /**
         * The Y axis value to serve as the base for the area, for
         * distinguishing between values above and below a threshold. The area
         * between the graph and the threshold is filled.
         *
         * * If a number is given, the Y axis will scale to the threshold.
         * * If `null`, the scaling behaves like a line series with fill between
         *   the graph and the Y axis minimum.
         * * If `Infinity` or `-Infinity`, the area between the graph and the
         *   corresponing Y axis extreme is filled (since v6.1.0).
         *
         * @sample {highcharts} highcharts/plotoptions/area-threshold/
         *         A threshold of 100
         * @sample {highcharts} highcharts/plotoptions/area-threshold-infinity/
         *         A threshold of Infinity
         *
         * @since   2.0
         * @product highcharts highstock
         */
        threshold: 0


    },
    /**
     * @lends seriesTypes.area.prototype
     */
    {

        singleStacks: false,

        // Return an array of stacked points, where null and missing points are
        // replaced by dummy points in order for gaps to be drawn correctly in
        // stacks.
        getStackPoints: function (points) {
            var series = this,
                segment = [],
                keys = [],
                xAxis = this.xAxis,
                yAxis = this.yAxis,
                stack = yAxis.stacks[this.stackKey],
                pointMap = {},
                seriesIndex = series.index,
                yAxisSeries = yAxis.series,
                seriesLength = yAxisSeries.length,
                visibleSeries,
                upOrDown = pick(yAxis.options.reversedStacks, true) ? 1 : -1,
                i;


            points = points || this.points;

            if (this.options.stacking) {

                for (i = 0; i < points.length; i++) {
                // Reset after point update (#7326)
                    points[i].leftNull = points[i].rightNull = null;

                    // Create a map where we can quickly look up the points by
                    // their X values.
                    pointMap[points[i].x] = points[i];
                }

                // Sort the keys (#1651)
                H.objectEach(stack, function (stackX, x) {
                // nulled after switching between
                // grouping and not (#1651, #2336)
                    if (stackX.total !== null) {
                        keys.push(x);
                    }
                });
                keys.sort(function (a, b) {
                    return a - b;
                });

                visibleSeries = yAxisSeries.map(function (s) {
                    return s.visible;
                });

                keys.forEach(function (x, idx) {
                    var y = 0,
                        stackPoint,
                        stackedValues;

                    if (pointMap[x] && !pointMap[x].isNull) {
                        segment.push(pointMap[x]);

                        // Find left and right cliff. -1 goes left, 1 goes
                        // right.
                        [-1, 1].forEach(function (direction) {
                            var nullName = direction === 1 ?
                                    'rightNull' :
  es not make a copy.
     *
     * @function Highcharts.Point#setNestedProperty
     *
     * @param {object} object
     *        The object to set the value on.
     *
     * @param {*} value
     *        The value to set.
     *
     * @param {string} key
     *        Key to the property to set.
     *
     * @return {object}
     *         The modified object.
     */
    setNestedProperty: function (object, value, key) {
        var nestedKeys = key.split('.');

        nestedKeys.reduce(function (result, key, i, arr) {
            var isLastKey = arr.length - 1 === i;

            result[key] = (
                isLastKey ?
                    value :
                    (H.isObject(result[key], true) ? result[key] : {})
            );
            return result[key];
        }, object);
        return object;
    },

    /**
     * Transform number or array configs into objects. Used internally to unify
     * the different configuration formats for points. For example, a simple
     * number `10` in a line series will be transformed to `{ y: 10 }`, and an
     * array config like `[1, 10]` in a scatter series will be transformed to
     * `{ x: 1, y: 10 }`.
     *
     * @function Highcharts.Point#optionsToObject
     *
     * @param {number|object|Array<number|string>|null} options
     *        The input option.
     *
     * @return {object}
     *         Transformed options.
     */
    optionsToObject: function (options) {
        var ret = {},
            series = this.series,
            keys = series.options.keys,
            pointArrayMap = keys || series.pointArrayMap || ['y'],
            valueCount = pointArrayMap.length,
            firstItemType,
            i = 0,
            j = 0;

        if (isNumber(options) || options === null) {
            ret[pointArrayMap[0]] = options;

        } else if (isArray(options)) {
            // with leading x value
            if (!keys && options.length > valueCount) {
                firstItemType = typeof options[0];
                if (firstItemType === 'string') {
                    ret.name = options[0];
                } else if (firstItemType === 'number') {
                    ret.x = options[0];
                }
                i++;
            }
            while (j < valueCount) {
                // Skip undefined positions for keys
                if (!keys || options[i] !== undefined) {
                    if (pointArrayMap[j].indexOf('.') > 0) {
                        // Handle nested keys, e.g. ['color.pattern.image']
                        // Avoid function call unless necessary.
                        H.Point.prototype.setNestedProperty(
                            ret, options[i], pointArrayMap[j]
                        );
                    } else {
                        ret[pointArrayMap[j]] = options[i];
                    }
                }
                i++;
                j++;
            }
        } else if (typeof options === 'object') {
            ret = options;

            // This is the fastest way to detect if there are individual point
            // dataLabels that need to be considered in drawDataLabels. These
            // can only occur in object configs.
            if (options.dataLabels) {
                series._hasPointLabels = true;
            }

            // Same approach as above for markers
            if (options.marker) {
                series._hasPointMarkers = true;
            }
        }
        return ret;
    },

    /**
     * Get the CSS class names for individual points. Used internally where the
     * returned value is set on every point.
     *
     * @function Highcharts.Point#getClassName
     *
     * @return {string}
     *         The class names.
     */
    getClassName: function () {
        return 'highcharts-point' +
            (this.selected ? ' highcharts-point-select' : '') +
            (this.negative ? ' highcharts-negative' : '') +
            (this.isNull ? ' highcharts-null-point' : '') +
            (this.colorIndex !== undefined ? ' highcharts-color-' +
                this.colorIndex : '') +
            (this.options.className ? ' ' + this.options.className : '') +
            (this.zone && this.zone.className ? ' ' +
                this.zone.className.replace('highcharts-negative', '') : '');
    },

    /**
     * In a series with `zones`, return the zone that the point belongs to.
     *
     * @function Highcharts.Point#getZone
     *
     * @return {Highcharts.PlotSeriesZonesOptions}
     *         The zone item.
     */
    getZone: function () {
        var series = this.series,
            zones = series.zones,
            zoneAxis = series.zoneAxis || 'y',
            i = 0,
            zone;

        zone = zones[i];
        while (this[zoneAxis] >= zone.value) {
            zone = zones[++i];
        }

        // For resetting or reusing the point (#8100)
        if (!this.nonZonedColor) {
            this.nonZonedColor = this.color;
        }

        if (zone && zone.color && !this.options.color) {
            this.color = zone.color;
        } else {
            this.color = this.nonZonedColor;
        }

        return zone;
    },

    /**
     * Destroy a point to clear memory. Its reference still stays in
     * `series.data`.
     *
     * @private
     * @function Highcharts.Point#destroy
     */
    destroy: function () {
        var point = this,
            series = point.series,
            chart = series.chart,
            hoverPoints = chart.hoverPoints,
            prop;

        chart.pointCount--;

        if (hoverPoints) {
            point.setState();
            erase(hoverPoints, point);
            if (!hoverPoints.length) {
                chart.hoverPoints = null;
            }

        }
        if (point === chart.hoverPoint) {
            point.onMouseOut();
        }

        // Remove all events and elements
        if (point.graphic || point.dataLabel || point.dataLabels) {
            removeEvent(point);
            point.destroyElements();
        }

        if (point.legendItem) { // pies have legend items
            chart.legend.destroyItem(point);
        }

        for (prop in point) {
            point[prop] = null;
        }
    },

    /**
     * Destroy SVG elements associated with the point.
     *
     * @private
     * @function Highcharts.Point#destroyElements
     */
    destroyElements: function (kinds) {
        var point = this,
            props = [],
            prop,
            i;

        kinds = kinds || { graphic: 1, dataLabel: 1 };
        if (kinds.graphic) {
            props.push('graphic', 'shadowGroup');
        }
        if (kinds.dataLabel) {
            props.push('dataLabel', 'dataLabelUpper', 'connector');
        }

        i = props.length;
        while (i--) {
            prop = props[i];
            if (point[prop]) {
                point[prop] = point[prop].destroy();
            }
        }

        ['dataLabel', 'connector'].forEach(function (prop) {
            var plural = prop + 's';
            if (kinds[prop] && point[plural]) {
                point[plural].forEach(function (item) {
                    if (item.element) {
                        item.destroy();
                    }
                });
                delete point[plural];
            }
        });
    },

    /**
     * Return the configuration hash needed for the data label and tooltip
     * formatters.
     *
     * @function Highcharts.Point#getLabelConfig
     *
     * @return {Highcharts.PointLabelObject}
     *         Abstract object used in formatters and formats.
     */
    getLabelConfig: function () {
        return {
            x: this.category,
            y: this.y,
            color: this.color,
            colorIndex: this.colorIndex,
            key: this.name || this.category,
            series: this.series,
            point: this,
            percentage: this.percentage,
            total: this.total || this.stackTotal
        };
    },

    /**
     * Extendable method for formatting each point's tooltip line.
     *
     * @function Highcharts.Point#tooltipFormatter
     *
     * @param {string} pointFormat
     *        The point format.
     *
     * @return {string}
     *         A string to be concatenated in to the common tooltip text.
     */
    tooltipFormatter: function (pointFormat) {

        // Insert options for valueDecimals, valuePrefix, and valueSuffix
        var series = this.series,
            seriesTooltipOptions = series.tooltipOptions,
            valueDecimals = pick(seriesTooltipOptions.valueDecimals, ''),
            valuePrefix = seriesTooltipOptions.valuePrefix || '',
            valueSuffix = seriesTooltipOptions.valueSuffix || '';

        // Replace default point style with class name
        if (series.chart.styledMode) {
            pointFormat = series.chart.tooltip.styledModeFormat(pointFormat);
        }

        // Loop over the point array map and replace unformatted values with
        // sprintf formatting markup
        (series.pointArrayMap || ['y']).forEach(function (key) {
            key = '{point.' + key; // without the closing bracket
            if (valuePrefix || valueSuffix) {
                pointFormat = pointFormat.replace(
                    RegExp(key + '}', 'g'),
                    valuePrefix + key + '}' + valueSuffix
                );
            }
            pointFormat = pointFormat.replace(
                RegExp(key + '}', 'g'),
                key + ':,.' + valueDecimals + 'f}'
            );
        });

        return format(pointFormat, {
            point: this,
            series: this.series
        }, series.chart.time);
    },

    /**
     * Fire an event on the Point object.
     *
     * @private
     * @function Highcharts.Point#firePointEvent
     *
     * @param {string} eventType
     *        Type of the event.
     *
     * @param {object} eventArgs
     *        Additional event arguments.
     *
     * @param {Function} defaultFunction
     *        Default event handler.
     *
     * @fires Highcharts.Point#event:*
     */
    firePointEvent: function (eventType, eventArgs, defaultFunction) {
        var point = this,
            series = this.series,
            seriesOptions = series.options;

        // load event handlers on demand to save time on mouseover/out
        if (
            seriesOptions.point.events[eventType] ||
            (
                point.options &&
                point.options.events &&
                point.options.events[eventType]
            )
        ) {
            this.importEvents();
        }

        // add default handler if in selection mode
        if (eventType === 'click' && seriesOptions.allowPointSelect) {
            defaultFunction = function (event) {
                // Control key is for Windows, meta (= Cmd key) for Mac, Shift
                // for Opera.
                if (point.select) { // #2911
                    point.select(
                        null,
                        event.ctrlKey || event.metaKey || event.shiftKey
                    );
                }
            };
        }

        fireEvent(this, eventType, eventArgs, defaultFunction);
    },

    /**
     * For categorized axes this property holds the category name for the
     * point. For other axes it holds the X value.
     *
     * @name Highcharts.Point#category
     * @type {number|string}
     */

    /**
     * The name of the point. The name can be given as the first position of the
     * point configuration array, or as a `name` property in the configuration:
     *
     * @example
     * // Array config
     * data: [
     *     ['John', 1],
     *     ['Jane', 2]
     * ]
     *
     * // Object config
     * data: [{
     *        name: 'John',
     *        y: 1
     * }, {
     *     name: 'Jane',
     *     y: 2
     * }]
     *
     * @name Highcharts.Point#name
     * @type {string}
     */

    /**
     * The percentage for points in a stacked series or pies.
     *
     * @name Highcharts.Point#percentage
     * @type {number}
     */

    /**
     * The total of values in either a stack for stacked series, or a pie in a
     * pie series.
     *
     * @name Highcharts.Point#total
     * @type {number}
     */

    /**
     * For certain series types, like pie charts, where individual points can
     * be shown or hidden.
     *
     * @name Highcharts.Point#visible
     * @type {boolean}
     */
    visible: true
};
