﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Intranet.Master" AutoEventWireup="true" CodeBehind="CostoPlanilla.aspx.cs" Inherits="WebApplication1.Modulo.Area.Personal.CsotosPlanilla" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContenedorDashboard" runat="server">
        <h3 class="panel-body" style="color: #283593; margin-top: 0px;">COSTOS DE PLANILLA</h3>
       <div class="text-left Left aligned text" style="margin-left: 18px;">
        <p><strong>Objetivo:</strong> El objetivo del indicador es mostrar los costos de planilla de la empresa y a la vez sus distintos departamentos y áreas</p>
    </div>
    <div id="AlertaMensajes">
    </div>

      <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-headline">
                <div class="panel-body">
                    <div class="form-inline">
                        <div class="form-group col-md-2 col-sm-6 col-sm-12">
                            <label for="formGroupExampleInput">Sucursal:</label>
                            <asp:DropDownList ID="cmbSucursal_cantidadArea" ClientIDMode="Static" class="form-control" runat="server" Style="width: 100%;"></asp:DropDownList>
                        </div>
                        <div class="form-group col-md-2 col-sm-6 col-sm-12">
                            <label for="formGroupExampleInput">Area:</label>
                            <asp:DropDownList ID="cmbArea_cantidadArea" ClientIDMode="Static" class="form-control" runat="server" Style="width: 100%;"></asp:DropDownList>
                        </div>
                        <div class="form-group col-md-2 col-sm-6 col-sm-12">
                            <label for="formGroupExampleInput">Subarea:</label>
                            <asp:DropDownList ID="cmbSubarea_cantidadArea" ClientIDMode="Static" class="form-control" runat="server" Style="width: 100%;"></asp:DropDownList>
                        </div>
                         <div class="form-group col-md-2 col-sm-6 col-sm-12">
                            <br />
                            <label for="formGroupExampleInput">Limpiar:</label>
                             <br />
                             <span class="lnr lnr-magic-wand-limpiar"  onclick="CambiarestadoFiltro()"></span>
                        </div>
                        <div class="form-group col-md-2 col-sm-6 col-sm-12">
                            <br />
                            <button id="G_Disponibilidad_Flotas" type="button" class="btn btn-primary btn-sm" style="margin-top: 6px;">GRAFICAR</button>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>



    <div class="row">
        <div class="col-md-3">
            <div class="metric">
                <div id="cantidad">
                </div>
            </div>
        </div>

        <div class="col-md-9">
            <div class="panel-body" style="background: white;">
                <div id="G_Personal_CostoPlanilla_PersonalArea" style="min-width: 310px; height: 300px; margin: 0 auto; display: none"></div>
            </div>
             <div class="col-md-9">
               <div class="text-left Left aligned text">
                <p><strong>Interpretacion:</strong> Se desea que los costos de planilla no aumenten y/o sean los menores posibles</p>
            </div>
               </div>
        </div>
    </div>

       <br />
     
  <div class="col-lg-6">
       <div class="row">
           <div id="Alerta">
         </div>
            <div class="col-lg-12">
             <div class="panel panel-headline">
                <div class="panel-body">
                    <div class="form-inline">

                        <div class="form-group col-lg-3 col-md-3 col-sm-3">
                        <label for="formGroupExampleInput">Sucursal:</label>
                            <asp:DropDownList ID="cmbSucursal_General" ClientIDMode="Static" class="form-control" runat="server" Style="width: 100%;"></asp:DropDownList>
                        </div>

                          <div class="form-group col-lg-3 col-md-3 col-sm-3">
                            <label for="formGroupExampleInput">Departamento:</label>
                            <asp:DropDownList ID="cmbDepartamento_general" ClientIDMode="Static" class="form-control" runat="server" Style="width: 100%;"></asp:DropDownList>
                        </div>

                        <div class="form-group col-lg-3 col-md-3 col-sm-3">
                        <label for="formGroupExampleInput">Area:</label>
                            <asp:DropDownList ID="cmbArea_general" ClientIDMode="Static" class="form-control" runat="server" Style="width: 100%;"></asp:DropDownList>
                        </div>

                          <div class="form-group col-lg-3 col-md-3 col-sm-3">
                            <label for="formGroupExampleInput">SubArea:</label>
                            <asp:DropDownList ID="cmbSubArea_general" ClientIDMode="Static" class="form-control" runat="server" Style="width: 100%;"></asp:DropDownList>
                        </div>
                           <div class="form-group col-lg-3 col-md-3 col-sm-3">
                            <label for="formGroupExampleInput">Mes:</label>
                            <asp:DropDownList ID="cmbMes_General" ClientIDMode="Static" class="form-control" runat="server" Style="width: 100%;"></asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-3 col-md-3 col-sm-3">
                            <br />
                            <label for="formGroupExampleInput">Agrupación:</label>
                            <label class="fancy-checkbox">
                                <input type="checkbox" id="opc_trimestre">
                                <span>Trimestre</span>
                            </label>
                            <label class="fancy-checkbox">
                                <input type="checkbox" id="opc_bimestre">
                                <span>Bimestre</span>
                            </label><br />
                    </div>
                      <div class="form-group col-lg-3 col-md-3 col-sm-3">
                            <br />
                            <label for="formGroupExampleInput">Limpiar:</label>
                          <br />
                             <span class="lnr lnr-magic-wand-limpiar"  onclick="Cambiarestado()"></span>
                        </div>

                         
                        <div class="form-group col-lg-3 col-md-3 col-sm-3">
                        <br />
                            <button id="Personal_CostoPlanillaE" type="button" class="btn btn-primary btn-sm">GRAFICAR</button>
                    </div>
              
                        </div>
                    </div>
               </div>
            </div>
       </div>
 
         <div class="col-lg-15">
          <div class="row panel-body">
              <div id="CostoPlanilla" style="min-width: 310px; height: 400px; margin: 0 auto; display:none"></div>  
               </div>   
          </div>
               <div class="text-left Left aligned text">
                <p><strong>Interpretacion:</strong> Se desea que los costos de planilla no aumenten y/o sean los menores posibles</p>
            </div>
 </div>



       <div class="col-lg-6">
              <div id="AlertaMensaje">
         </div>
          <div class="row">
            <div class="col-lg-12">

            <div class="panel panel-headline">
                <div class="panel-body">
                    <div class="form-inline">

                       <div class="form-group col-lg-3 col-md-3 col-sm-3">
                        <label for="formGroupExampleInput">Sucursal:</label>
                            <asp:DropDownList ID="cmbESucursal_General" ClientIDMode="Static" class="form-control" runat="server" Style="width: 100%;"></asp:DropDownList>
                        </div>

                      <div class="form-group col-lg-3 col-md-3 col-sm-3">
                        <label for="formGroupExampleInput">Area:</label>
                            <asp:DropDownList ID="cmbEArea_general" ClientIDMode="Static" class="form-control" runat="server" Style="width: 100%;"></asp:DropDownList>
                        </div>

                        <div class="form-group col-lg-3 col-md-3 col-sm-3">
                            <label for="formGroupExampleInput">SubArea:</label>
                            <asp:DropDownList ID="cmbESubArea_general" ClientIDMode="Static" class="form-control" runat="server" Style="width: 100%;"></asp:DropDownList>
                        </div>
                          <div class="form-group col-lg-3 col-md-3 col-sm-3">
                            <label for="formGroupExampleInput">Año:</label>
                            <asp:DropDownList ID="cmbEAnio_General" ClientIDMode="Static" class="form-control" runat="server" Style="width: 100%;"></asp:DropDownList>
                        </div>
                           <div class="form-group col-lg-3 col-md-3 col-sm-3">
                            <label for="formGroupExampleInput">Mes:</label>
                            <asp:DropDownList ID="cmbEMes_General" ClientIDMode="Static" class="form-control" runat="server" Style="width: 100%;"></asp:DropDownList>
                        </div>
                       </div>
                        <div class="form-group col-lg-3 col-md-3 col-sm-3">
                            <br />
                            <label for="formGroupExampleInput">Agrupación:</label>
                        <label class="fancy-checkbox">
                                <input type="checkbox" id="opc_Etrimestre">
                                <span>Trimestre</span>
                            </label>
                        <label class="fancy-checkbox">
                                <input type="checkbox" id="opc_Ebimestre">
                                <span>Bimestre</span>
                            </label>
                      </div>
                           <div class="form-group col-lg-3 col-md-3 col-sm-3">
                               <br />
                            <label for="formGroupExampleInput">Limpiar:</label>  
                               <br />
                              <span class="lnr lnr-magic-wand-limpiar"  onclick="Estado()"></span>
                       </div>
                    
                           <div class="form-group col-lg-3 col-md-3 col-sm-3">
                        <br />
                            <button id="Personal_CostoPlanilla_EvaluacionArea" type="button" class="btn btn-primary btn-sm">GRAFICAR</button>
                        </div>
                    </div>
                    </div>
               </div>
            </div>
        
         <div class="col-lg-15">
         <div class="row panel-body">
              <div id="Evolucion_Mensual" style="min-width: 310px; height: 400px; margin: 0 auto; display:none"></div>  
               </div>   
           </div>   
         <div class="text-left Left aligned text">
                <p><strong>Interpretacion:</strong> Se desea que los costos de planilla no aumenten y/o sean los menores posibles</p>
            </div>
    
   </div>
<script>
    var sucursal = ""; var area = ""; var subarea = ""; var fechaInicio = ""; var fechaFin = ""; var dataFiltro = "";
    fn_General_CostoPlanilla_CantidadGeneral();
    Llamada_Grafica_CostoPlanilla_CantidadArea(sucursal, area, subarea, fechaInicio, fechaFin);
    fn_Cargar_Filtro();
    $(document).ready(function () {

        $("#cmbSucursal_cantidadArea").change(function () { 
        fn_Validar_Filtro();
        fn_Cargar_Filtro();
        }); 

        $("#cmbArea_cantidadArea").change(function () {
            fn_Validar_Filtro();
            fn_Cargar_Filtro();
        }); 

        $("#cmbSubarea_cantidadArea").change(function () {
            fn_Validar_Filtro();
            fn_Cargar_Filtro();
        }); 

        $("#G_Disponibilidad_Flotas").click(function () {
            fn_Validar_Filtro();
            fn_General_CostoPlanilla_CantidadGeneral();
            Llamada_Grafica_CostoPlanilla_CantidadArea(sucursal, area, subarea, fechaInicio, fechaFin);
        });
    });
    function fn_Cargar_Filtro()
    {
        if (sucursal == 0 || sucursal == "") fn_General_Cargar_FiltroCSucursal(area, subarea, fechaInicio, fechaFin);
        if (area == 0 || area == "") fn_General_Cargar_FiltroCArea(sucursal, subarea, fechaInicio, fechaFin);
        if (subarea == 0 || subarea == "") fn_General_Cargar_FiltroCSubArea(sucursal, area, fechaInicio, fechaFin);
    }

    function fn_Validar_Filtro()
    {
        sucursal = $("#cmbSucursal_cantidadArea").val(); if (sucursal != 0) { sucursal = $("#cmbSucursal_cantidadArea option:selected").text(); } else { sucursal = ""; }
        area = $("#cmbArea_cantidadArea").val(); if (area != 0) { area = $("#cmbArea_cantidadArea option:selected").text(); } else { area = ""; }
        subarea = $("#cmbSubarea_cantidadArea").val(); if (subarea != 0) { subarea = $("#cmbSubarea_cantidadArea option:selected").text(); } else { subarea = ""; }
    }

    function CambiarestadoFiltro() {

        if (sucursal != 0) $("#cmbSucursal_cantidadArea").val(0); document.getElementById("cmbSucursal_cantidadArea").disabled = false;
        if (area != 0) $("#cmbArea_cantidadArea").val(0); document.getElementById("cmbArea_cantidadArea").disabled = false;
        if (subarea != 0) $("#cmbSubarea_cantidadArea").val(0); document.getElementById("cmbSubarea_cantidadArea").disabled = false;
        fn_Validar_Filtro();
        fn_Cargar_Filtro();
        Llamada_Grafica_CostoPlanilla_CantidadArea(sucursal, area, subarea, fechaInicio, fechaFin);
    }

    function fn_General_CostoPlanilla_CantidadGeneral() {
        var html = '';
        var sucess = function (response) {
            var obj = JSON.parse(response.d);
            var data_html = "";
            if (obj == null) {
                data_html =
                    "<span class=\"icon\"><i class=\"fa fa - bar - chart\"></i></span>" +
                    " <p>" +
                    "<span class=\"number\">1,252</span>" +
                    "<span class=\"title\">Costos General de Colaboradores</span>" +
                    "</p>";
                $("div[id$=cantidad]").html(data_html);
            }
            else {
                for (var i = 0; i < obj.length; i++) {
                    data_html =
                        "<span class=\"icon\"><i class=\"fa fa-bar-chart\"></i></span><p>" +
                        "<span class=\"number\" id=\"costoPlanilla_cantidadG\">" + obj[i] + " Mil "+
                        "</span><span class=\"title\">Cantidad General de Costo de Planilla </span></p>";
                }
                $("div[id$=cantidad]").html(data_html);
            }
        };
        var error = function (xhr, ajaxOptions, thrownError) {
        };
        fn_LlamadoMetodo("CostoPlanilla.aspx/Personal_CostosPlanilla_CS_cantidadGeneral", JSON.stringify({}), sucess, error);
    }

    function fn_General_Cargar_FiltroCSucursal(area, subarea, fechaInicio, fechaFin) {
        var html = '';
        dataFiltro = $("#cmbSucursal_cantidadArea option:selected").text();
        var sucess = function (response) {
            var obj = JSON.parse(response.d);
            var data_html = "";
            if (obj == null) {
                data_html = "<option value='0'>Seleccione Opción</option>";
                $("select[id$=cmbSucursal_cantidadArea]").html(data_html);
            }
            else {
                data_html += "<option value='0'>Seleccione Opción</option>";
                for (var i = 0; i < obj.length; i++) {
                    if (dataFiltro != "") {
                        if (obj[i] != dataFiltro) {
                            data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>";
                        }
                    }
                    else { data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>"; }
                    $("select[id$=cmbSucursal_cantidadArea]").html(data_html);
                }
            }
        };
        var error = function (xhr, ajaxOptions, thrownError) {
        };
        fn_LlamadoMetodo("CostoPlanilla.aspx/F_Personal_CostosPlanilla_CS_ddSucursal", JSON.stringify({ area: area, subarea: subarea, fechaInicio: fechaInicio, fechaFin: fechaFin }), sucess, error);
    }

    function fn_General_Cargar_FiltroCArea(sucursal, subarea, fechaInicio, fechaFin) {
        var html = '';
        var sucess = function (response) {
            var obj = JSON.parse(response.d);
            var data_html = "";
            if (obj == null) {
                data_html = "<option value='0'>Seleccione Opción</option>";
                $("select[id$=cmbArea_cantidadArea]").html(data_html);
            }
            else {
                data_html += "<option value='0'>Seleccione Opción</option>";
                for (var i = 0; i < obj.length; i++) {
                    data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>";
                }
                $("select[id$=cmbArea_cantidadArea]").html(data_html);
            }
            //  fn_Mensaje_Alerta("Confirmacion", "", "holaa", "AlertaMensaje", html);

        };
        var error = function (xhr, ajaxOptions, thrownError) {

        };
        fn_LlamadoMetodo("CostoPlanilla.aspx/F_Personal_CostosPlanilla_CS_ddArea", JSON.stringify({ sucursal: sucursal, subarea: subarea, fechaInicio: fechaInicio, fechaFin: fechaFin }), sucess, error);
    }

    function fn_General_Cargar_FiltroCSubArea(sucursal, area, fechaInicio, fechaFin) {
        var html = '';
        var sucess = function (response) {
            var obj = JSON.parse(response.d);
            var data_html = "";
            if (obj == null) {
                data_html = "<option value='0'>Seleccione Opción</option>";
                $("select[id$=cmbSubarea_cantidadArea]").html(data_html);
            }
            else {
                data_html += "<option value='0'>Seleccione Opción</option>";
                for (var i = 0; i < obj.length; i++) {
                    data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>";
                }
                $("select[id$=cmbSubarea_cantidadArea]").html(data_html);
            }
        };
        var error = function (xhr, ajaxOptions, thrownError) {
        };
        fn_LlamadoMetodo("CostoPlanilla.aspx/F_Personal_CostosPlanilla_CS_ddSubArea", JSON.stringify({ sucursal: sucursal, area: area, fechaInicio: fechaInicio, fechaFin: fechaFin }), sucess, error);
    }  

    function Llamada_Grafica_CostoPlanilla_CantidadArea(sucursal, area, subarea, fechaInicio, fechaFin) {
        var sucess = function (response) {
            BarrasChart(response.d.Departamentos, response.d.Data);
            $('#G_Personal_CostoPlanilla_PersonalArea').show();
        };
        var error = function (xhr, ajaxOptions, thrownError) {
        };
        fn_LlamadoMetodo("CostoPlanilla.aspx/Grafica_Personal_CostosPlanilla_CS_cantidadArea", JSON.stringify({ sucursal: sucursal, area: area, subarea: subarea, fechaInicio: fechaInicio, fechaFin: fechaFin }), sucess, error);
    }

    function BarrasChart(Departamentos, Data) {
        var chart = Highcharts.chart('G_Personal_CostoPlanilla_PersonalArea', {
            title: {
                text: 'COSTOS DE PLANILLA SEGÚN EL DEPARTAMENTO'
            },
            xAxis: {
                categories: Departamentos
            },
            yAxis: {
                title: {
                    text: ''
                },
                labels: {
                    formatter: function () {
                        return (this.value) + ' Mil';
                    }
                },
            },
            chart: {
                inverted: true,
                polar: false
            },
            tooltip: {
                headerFormat: '<b>{point.x}</b><br/>',
                pointFormat: '{series.name}: {point.y:.1f} Mil'
            },
            plotOptions: {
                series: {
                    stacking: 'normal',
                    dataLabels: {
                        enabled: true,
                        style: {
                            "font-family": "'Open Sans', sans-serif",
                            "color": "#ffffff",
                            //"fontSize": "12px",
                            "fontWeight": "normal",
                            "textOutline": false,
                        },
                        format: '{point.y}'
                    }
                }
            },
            series: [{
                name: 'Cantidad',
                type: 'column',
                colorByPoint: true,
                data: Data,
                showInLegend: false
            }]
        });
    }

    /////// EVOLUCIÓN HISTÓRICA_EVOLUCIÓN GENERAL MENSUAL

    var Departamento = ""; var Area = ""; var SubArea = ""; var Sucursal = ""; var Mes = ""; var agrupacion = "0";
    var temp = false;
    fn_CargarCombos(Mes, Departamento, Area, SubArea, Sucursal);
    Cargar_Grafica("", "", "", "", "", "0");
    $(document).ready(function () {

        $("#opc_trimestre").change(function () {
            $("#opc_bimestre").prop("checked", false);
            ValidarCampo();
            if (agrupacion === "3") {
                $("#cmbMes_General").prop('disabled', true);
                fn_Mensaje_Alerta("alerta", " No podra seleccionar Mes", "", "Alerta");
            }
            else {
                $("#cmbMes_General").prop('disabled', false);

            }
            Cargar_Grafica(Mes, Departamento, Area, SubArea, Sucursal, agrupacion);
        });

        $("#opc_bimestre").change(function () {
            $("#opc_trimestre").prop("checked", false);
            ValidarCampo();
            if (agrupacion === "2") {
                $("#cmbMes_General").prop('disabled', true);
                fn_Mensaje_Alerta("alerta", " No podra seleccionar Mes", "", "Alerta");
            }
            else {
                $("#cmbMes_General").prop('disabled', false);

            }
            Cargar_Grafica(Mes, Departamento, Area, SubArea, Sucursal, agrupacion);
        });
        $("#cmbDepartamento_general").change(function () {
            ValidarCampo();
            fn_CargarCombos(Mes, Departamento, Area, SubArea, Sucursal);
        });

        $("#cmbArea_general").change(function () {
            ValidarCampo();
            fn_CargarCombos(Mes, Departamento, Area, SubArea, Sucursal);
        });

        $("#cmbSubArea_general").change(function () {
            ValidarCampo();
            fn_CargarCombos(Mes, Departamento, Area, SubArea, Sucursal);
        });

        $("#cmbSucursal_General").change(function () {
            ValidarCampo();
            fn_CargarCombos(Mes, Departamento, Area, SubArea, Sucursal);
        });

        $("#cmbMes_General").change(function () {
            ValidarCampo();
            $("#opc_trimestre").prop("checked", false);
            $("#opc_bimestre").prop("checked", false);
            if (($("#cmbMes_General").val()) == 0) {
                $("#opc_trimestre").prop('disabled', false);
                $("#opc_bimestre").prop('disabled', false);

            }
            else {

                $("#opc_trimestre").prop('disabled', true);
                $("#opc_bimestre").prop('disabled', true);
                fn_Mensaje_Alerta("alerta", " No podra seleccionar Trimestre y Bimestre", "", "Alerta");

            }
            fn_CargarCombos(Mes, Departamento, Area, SubArea, Sucursal);
        });

        $("#Personal_CostoPlanillaE").click(function () {

            ValidarCampo();
            Cargar_Grafica(Mes, Departamento, Area, SubArea, Sucursal, agrupacion);
        });
    });
    function fn_CargarCombos(Mes, Departamento, Area, SubArea, Sucursal) {
        if (Departamento == 0 || Departamento == "") fn_General_Cargar_FiltroDepartamento(Mes, Area, SubArea, Sucursal);
        if (Area == 0 || Area == "") fn_General_Cargar_FiltroArea(Mes, Departamento, SubArea, Sucursal);
        if (SubArea == 0 || SubArea == "") fn_General_Cargar_FiltroSubArea(Mes, Departamento, Area, Sucursal);
        if (Sucursal == 0 || Sucursal == "") fn_General_Cargar_FiltroSucursal(Mes, Departamento, Area, SubArea);
        if (Mes == 0 || Mes == "") fn_General_Cargar_FiltroMes(Departamento, Area, SubArea, Sucursal);
    }
    function fn_General_Cargar_FiltroMes(Departamento, Area, SubArea, Sucursal) {
        var html = '';
        var sucess = function (response) {
            var obj = JSON.parse(response.d);
            var data_html = "";
            if (obj == null) {
                data_html = "<option value='0'>Seleccione Opción</option>";
                $("select[id$=cmbMes_General]").html(data_html);
            }
            else {
                data_html += "<option value='0'>Seleccione Opción</option>";
                for (var i = 0; i < obj.length; i++) {
                    data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>";
                }
                $("select[id$=cmbMes_General]").html(data_html);
            }
        };
        var error = function (xhr, ajaxOptions, thrownError) {
        };
        fn_LlamadoMetodo("CostoPlanilla.aspx/Personal_CostoPlanilla_EvolucionGeneralMensual_ddlMes_CS", JSON.stringify({ departamento: Departamento, area: Area, subArea: SubArea, sucursal: Sucursal }), sucess, error);
    }
    function fn_General_Cargar_FiltroDepartamento(Mes, Area, SubArea, Sucursal) {
        var html = '';
        var sucess = function (response) {
            var obj = JSON.parse(response.d);
            var data_html = "";
            if (obj == null) {
                data_html = "<option value='0'>Seleccione Opción</option>";
                $("select[id$=cmbDepartamento_general]").html(data_html);
            }
            else {
                data_html += "<option value='0'>Seleccione Opción</option>";
                for (var i = 0; i < obj.length; i++) {
                    data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>";
                }
                $("select[id$=cmbDepartamento_general]").html(data_html);
            }
        };
        var error = function (xhr, ajaxOptions, thrownError) {
        };
        fn_LlamadoMetodo("CostoPlanilla.aspx/Personal_CostoPlanilla_EvolucionGeneralMensual_ddlDepartamento_CS", JSON.stringify({ mes: Mes, area: Area, subArea: SubArea, sucursal: Sucursal }), sucess, error);
    }
    function fn_General_Cargar_FiltroArea(Mes, Departamento, SubArea, Sucursal) {
        var html = '';
        var sucess = function (response) {
            var obj = JSON.parse(response.d);
            var data_html = "";
            if (obj == null) {
                data_html = "<option value='0'>Seleccione Opción</option>";
                $("select[id$=cmbArea_general]").html(data_html);
            }
            else {
                data_html += "<option value='0'>Seleccione Opción</option>";
                for (var i = 0; i < obj.length; i++) {
                    data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>";
                }
                $("select[id$=cmbArea_general]").html(data_html);
            }
        };
        var error = function (xhr, ajaxOptions, thrownError) {
        };
        fn_LlamadoMetodo("CostoPlanilla.aspx/Personal_CostoPlanilla_EvolucionGeneralMensual_ddlArea_CS", JSON.stringify({ mes: Mes, departamento: Departamento, subArea: SubArea, sucursal: Sucursal }), sucess, error);
    }
    function fn_General_Cargar_FiltroSubArea(Mes, Departamento, Area, Sucursal) {
        var html = '';
        var sucess = function (response) {
            var obj = JSON.parse(response.d);
            var data_html = "";
            if (obj == null) {
                data_html = "<option value='0'>Seleccione Opción</option>";
                $("select[id$=cmbSubArea_general]").html(data_html);
            }
            else {
                data_html += "<option value='0'>Seleccione Opción</option>";
                for (var i = 0; i < obj.length; i++) {
                    data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>";
                }
                $("select[id$=cmbSubArea_general]").html(data_html);
            }
        };
        var error = function (xhr, ajaxOptions, thrownError) {
        };
        fn_LlamadoMetodo("CostoPlanilla.aspx/Personal_CostoPlanilla_EvolucionGeneralMensual_ddlSubArea_CS", JSON.stringify({ mes: Mes, departamento: Departamento, area: Area, sucursal: Sucursal }), sucess, error);
    }
    function fn_General_Cargar_FiltroSucursal(Mes, Departamento, Area, SubArea) {
        var html = '';
        var sucess = function (response) {
            var obj = JSON.parse(response.d);
            var data_html = "";
            if (obj == null) {
                data_html = "<option value='0'>Seleccione Opción</option>";
                $("select[id$=cmbSucursal_General]").html(data_html);
            }
            else {
                data_html += "<option value='0'>Seleccione Opción</option>";
                for (var i = 0; i < obj.length; i++) {
                    data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>";
                }
                $("select[id$=cmbSucursal_General]").html(data_html);
            }
        };
        var error = function (xhr, ajaxOptions, thrownError) {
        };
        fn_LlamadoMetodo("CostoPlanilla.aspx/Personal_CostoPlanilla_EvolucionGeneralMensual_ddlSucursal_CS", JSON.stringify({ mes: Mes, departamento: Departamento, area: Area, subArea: SubArea }), sucess, error);
    }
    function ValidarCampo() {
        Departamento = $("#cmbDepartamento_general").val(); if (Departamento != 0) { Departamento = $("#cmbDepartamento_general option:selected").text(); } else { Departamento = ""; }
        Area = $("#cmbArea_general").val(); if (Area != 0) { Area = $("#cmbArea_general option:selected").text(); } else { Area = ""; }
        SubArea = $("#cmbSubArea_general").val(); if (SubArea != 0) { SubArea = $("#cmbSubArea_general option:selected").text(); } else { SubArea = ""; }
        Sucursal = $("#cmbSucursal_General").val(); if (Sucursal != 0) { Sucursal = $("#cmbSucursal_General option:selected").text(); } else { Sucursal = ""; }
        Mes = $("#cmbMes_General").val(); if (Mes != 0) { Mes = $("#cmbMes_General option:selected").text(); } else { Mes = ""; }
        if ($("#opc_trimestre").prop('checked')) { agrupacion = "3"; }
        if ($("#opc_bimestre").prop('checked')) { agrupacion = "2"; }
        if ($("#opc_bimestre").prop('checked') == false && $("#opc_trimestre").prop('checked') == false) { agrupacion = "0"; }

    }
    function Cambiarestado() {
        if (Departamento != 0) $("#cmbDepartamento_general").val(0); document.getElementById("cmbDepartamento_general").disabled = false;
        if (Area != 0) $("#cmbArea_general").val(0); document.getElementById("cmbArea_general").disabled = false;
        if (Mes != 0) $("#cmbMes_General").val(0); document.getElementById("cmbMes_General").disabled = false;
        if (SubArea != 0) $("#cmbSubArea_general").val(0); document.getElementById("cmbSubArea_general").disabled = false;
        if (Sucursal != 0) $("#cmbSucursal_General").val(0); document.getElementById("cmbSucursal_General").disabled = false;
        $("#opc_trimestre").prop("checked", false); $("#opc_trimestre").prop('disabled', false);
        $("#opc_bimestre").prop("checked", false); $("#opc_bimestre").prop('disabled', false);
        ValidarCampo();
        fn_CargarCombos(Mes, Departamento, Area, SubArea, Sucursal);
        Cargar_Grafica("", "", "", "", "", "0");
    }
    function Cargar_Grafica(Mes, Departamento, Area, SubArea, Sucursal, agrupacion) {
        var sucess = function (response) {
            json = response.d.Series
            LineChart(response.d.Fecha, response.d.Name, fn_ValidarANulos(json));
            $('#CostoPlanilla').show();
        };
        var error = function (xhr, ajaxOptions, thrownError) {
            fn_Mensaje_Alerta("error", " Ocurrio un error mientras se cargaban los datos", "", "Alerta");
        };
        fn_LlamadoMetodo("CostoPlanilla.aspx/G_Personal_CostoPlanilla_EvolucionGeneralMensual_CS", JSON.stringify({ mes: Mes, departamento: Departamento, area: Area, subArea: SubArea, sucursal: Sucursal, agrupacionF: agrupacion }), sucess, error);
    }
    function LineChart(fecha, Name, data) {
        Highcharts.chart('CostoPlanilla', {
            chart: {
                type: 'line'
            },
            title: {
                text: 'EVOLUCIÓN GENERAL MENSUAL'
            },
            subtitle: {
                text: 'MESES'
            },
            xAxis: {
                categories: JSON.parse(fecha)
            },
            yAxis: {
                min: 0,


                title: {
                    text: 'TOTAL'
                },
                labels: {
                    formatter: function () {
                        return (this.value) + ' Mil';
                    }
                },
            },
            tooltip: {
                headerFormat: '<b>{point.x}</b><br/>',
                pointFormat: '{series.name}: {point.y:.1f} Mil'
            },
            plotOptions: {
                line: {
                    dataLabels: {
                        enabled: true
                    },
                    enableMouseTracking: true
                }
            },
            series: JSON.parse(data)
        });
    }


    //   EVOLUCIÓN HISTÓRICA_EVOLUCIÓN POR AREA MENSUAL

    var EArea = ""; var ESubArea = ""; var ESucursal = ""; var EAnio = ""; var EMes = ""; var EAnio = ""; var EAgrupacion = "";
    var temp = false;
    fn_Combos(EAnio, EMes, EArea, ESubArea, ESucursal);
    Cargar_Grafica_Chart("", "", "", "", "", "0");

    $(document).ready(function () {
        $("#opc_Etrimestre").change(function () {
            $("#opc_Ebimestre").prop("checked", false);
            Validar();
            if (EAgrupacion === "3") {
                $("#cmbEMes_General").prop('disabled', true);
                $("#cmbEAnio_General").prop('disabled', true);
                fn_Mensaje_Alerta("alerta", " No podra seleccionar Mes y año ", "", "AlertaMensaje");
            }
            else {
                $("#cmbEMes_General").prop('disabled', false);
                $("#cmbEAnio_General").prop('disabled', false);

            }
            Cargar_Grafica_Chart(EAnio, EMes, EArea, ESubArea, ESucursal, EAgrupacion);
        });

        $("#opc_Ebimestre").change(function () {
            $("#opc_Etrimestre").prop("checked", false);
            Validar();
            if (EAgrupacion === "2") {
                $("#cmbEMes_General").prop('disabled', true);
                $("#cmbEAnio_General").prop('disabled', true);
                fn_Mensaje_Alerta("alerta", " No podra seleccionar mes y año", "", "AlertaMensaje");
            }
            else {
                $("#cmbEMes_General").prop('disabled', false);
                $("#cmbEAnio_General").prop('disabled', false);

            }
            Cargar_Grafica_Chart(EAnio, EMes, EArea, ESubArea, ESucursal, EAgrupacion);
        });
        $("#cmbEMes_General").change(function () {
            if (($("#cmbEAnio_General").val()) != 0) {
                $("#opc_Etrimestre").prop('disabled', true);
                $("#opc_Ebimestre").prop('disabled', true);
                $("#cmbEMes_General").prop('disabled', false);
            }
            else {
                $("#cmbEMes_General").val(0); document.getElementById("cmbEMes_General").disabled = false;
                $("#cmbEMes_General").prop("disabled", true);
                fn_Mensaje_Alerta("alerta", " No podra seleccionar un mes sin un año", "", "AlertaMensaje");
            }

            Validar();
            fn_Combos(EAnio, EMes, EArea, ESubArea, ESucursal);
        });

        $("#cmbEAnio_General").change(function () {
            Validar();
            $("#opc_trimestre").prop("checked", false);
            $("#opc_bimestre").prop("checked", false);
            if (($("#cmbEAnio_General").val()) == 0) {
                $("#opc_Etrimestre").prop('disabled', false);
                $("#opc_Ebimestre").prop('disabled', false);
                $("#cmbEMes_General").prop('disabled', true);
                $("#cmbEMes_General").val(0); document.getElementById("cmbEMes_General").disabled = false;
            }
            else {

                $("#opc_Etrimestre").prop('disabled', true);
                $("#opc_Ebimestre").prop('disabled', true);
                $("#cmbEMes_General").prop('disabled', false);
                fn_Mensaje_Alerta("alerta", " No podra seleccionar Trimestre y Bimestre", "", "AlertaMensaje");

            }
            fn_Combos(EAnio, EMes, EArea, ESubArea, ESucursal);
        });

        $("#cmbEArea_general").change(function () {
            Validar();
            fn_Combos(EAnio, EMes, EArea, ESubArea, ESucursal);
        });

        $("#cmbESubArea_general").change(function () {
            Validar();
            fn_Combos(EAnio, EMes, EArea, ESubArea, ESucursal);
        });

        $("#cmbESucursal_General").change(function () {
            Validar();
            fn_Combos(EAnio, EMes, EArea, ESubArea, ESucursal);
        });

        $("#Personal_CostoPlanilla_EvaluacionArea").click(function () {

            Validar();
            Cargar_Grafica_Chart(EAnio, EMes, EArea, ESubArea, ESucursal, EAgrupacion);
        });
    });

    function fn_Combos(EAnio, EMes, EArea, ESubArea, ESucursal) {
        if (EArea == 0 || EArea == "") fn_FiltroEArea(EAnio, EMes, ESubArea, ESucursal);
        if (ESubArea == 0 || ESubArea == "") fn_EFiltroSubArea(EAnio, EMes, EArea, ESucursal);
        if (ESucursal == 0 || ESucursal == "") fn_EFiltroSucursal(EAnio, EMes, EArea, ESubArea);
        if (EAnio == 0 || EAnio == "") fn_EFiltroAnio(EMes, EArea, ESubArea, ESucursal);
        if (EMes == 0 || EMes == "") fn_EFiltroMes(EAnio, EArea, ESubArea, ESucursal);
    }

    function Validar() {
        EArea = $("#cmbEArea_general").val(); if (EArea != 0) { EArea = $("#cmbEArea_general option:selected").text(); } else { EArea = ""; }
        ESubArea = $("#cmbESubArea_general").val(); if (ESubArea != 0) { ESubArea = $("#cmbESubArea_general option:selected").text(); } else { ESubArea = ""; }
        ESucursal = $("#cmbESucursal_General").val(); if (ESucursal != 0) { ESucursal = $("#cmbESucursal_General option:selected").text(); } else { ESucursal = ""; }
        EAnio = $("#cmbEAnio_General").val(); if (EAnio != 0) { EAnio = $("#cmbEAnio_General option:selected").text(); } else { EAnio = ""; }
        EMes = $("#cmbEMes_General").val(); if (EMes != 0) { EMes = $("#cmbEMes_General option:selected").text(); } else { EMes = ""; }
        if ($("#opc_Etrimestre").prop('checked')) { EAgrupacion = "3"; }
        if ($("#opc_Ebimestre").prop('checked')) { EAgrupacion = "2"; }
        if ($("#opc_Ebimestre").prop('checked') == false && $("#opc_Etrimestre").prop('checked') == false) { EAgrupacion = "0"; }
    }
    function Estado() {
        if (EArea != 0) $("#cmbEArea_general").val(0); document.getElementById("cmbEArea_general").disabled = false;
        if (ESubArea != 0) $("#cmbESubArea_general").val(0); document.getElementById("cmbESubArea_general").disabled = false;
        if (ESucursal != 0) $("#cmbESucursal_General").val(0); document.getElementById("cmbESucursal_General").disabled = false;
        if (EAnio != 0) $("#cmbEAnio_General").val(0); document.getElementById("cmbEAnio_General").disabled = false;
        if (EMes != 0) $("#cmbEMes_General").val(0); document.getElementById("cmbEMes_General").disabled = false;
        $("#opc_Etrimestre").prop("checked", false);
        $("#opc_Ebimestre").prop("checked", false);
        Validar();
        fn_Combos(EAnio, EMes, EArea, ESubArea, ESucursal)
        Cargar_Grafica_Chart(EAnio, EMes, EArea, ESubArea, ESucursal, EAgrupacion);
    }
    function fn_FiltroEArea(EAnio, EMes, ESubArea, ESucursal) {
        var html = '';
        var sucess = function (response) {
            var obj = JSON.parse(response.d);
            var data_html = "";
            if (obj == null) {
                data_html = "<option value='0'>Seleccione Opción</option>";
                $("select[id$=cmbEArea_general]").html(data_html);
            }
            else {
                data_html += "<option value='0'>Seleccione Opción</option>";
                for (var i = 0; i < obj.length; i++) {
                    data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>";
                }
                $("select[id$=cmbEArea_general]").html(data_html);
            }
        };
        var error = function (xhr, ajaxOptions, thrownError) {
        };
        fn_LlamadoMetodo("CostoPlanilla.aspx/Personal_CostoPlanilla_EvolucionAreaMensual_ddlArea_CS", JSON.stringify({ anio: EAnio, mes: EMes, subArea: ESubArea, sucursal: ESucursal }), sucess, error);
    }

    function fn_EFiltroSubArea(EAnio, EMes, EArea, ESucursal) {
        var html = '';
        var sucess = function (response) {
            var obj = JSON.parse(response.d);
            var data_html = "";
            if (obj == null) {
                data_html = "<option value='0'>Seleccione Opción</option>";
                $("select[id$=cmbESubArea_general]").html(data_html);
            }
            else {
                data_html += "<option value='0'>Seleccione Opción</option>";
                for (var i = 0; i < obj.length; i++) {
                    data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>";
                }
                $("select[id$=cmbESubArea_general]").html(data_html);
            }
        };
        var error = function (xhr, ajaxOptions, thrownError) {
        };
        fn_LlamadoMetodo("CostoPlanilla.aspx/Personal_CostoPlanilla_EvolucionAreaMensual_ddlSubArea_CS", JSON.stringify({ anio: EAnio, mes: EMes, area: EArea, sucursal: ESucursal }), sucess, error);
    }

    function fn_EFiltroSucursal(EAnio, EMes, EArea, ESubArea) {
        var html = '';
        var sucess = function (response) {
            var obj = JSON.parse(response.d);
            var data_html = "";
            if (obj == null) {
                data_html = "<option value='0'>Seleccione Opción</option>";
                $("select[id$=cmbESucursal_General]").html(data_html);
            }
            else {
                data_html += "<option value='0'>Seleccione Opción</option>";
                for (var i = 0; i < obj.length; i++) {
                    data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>";
                }
                $("select[id$=cmbESucursal_General]").html(data_html);
            }
        };
        var error = function (xhr, ajaxOptions, thrownError) {
        };
        fn_LlamadoMetodo("CostoPlanilla.aspx/Personal_CostoPlanilla_EvolucionAreaMensual_ddlSucursal_CS", JSON.stringify({ anio: EAnio, mes: EMes, area: EArea, subArea: ESubArea }), sucess, error);
    }


    function fn_EFiltroAnio(EMes, EArea, ESubArea, ESucursal) {
        var html = '';
        var sucess = function (response) {
            var obj = JSON.parse(response.d);
            var data_html = "";
            if (obj == null) {
                data_html = "<option value='0'>Seleccione Opción</option>";
                $("select[id$=cmbEAnio_General]").html(data_html);
            }
            else {
                data_html += "<option value='0'>Seleccione Opción</option>";
                for (var i = 0; i < obj.length; i++) {
                    data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>";
                }
                $("select[id$=cmbEAnio_General]").html(data_html);
            }
        };
        var error = function (xhr, ajaxOptions, thrownError) {
        };
        fn_LlamadoMetodo("CostoPlanilla.aspx/Personal_CostoPlanilla_EvolucionAreaMensual_ddlAnio_CS", JSON.stringify({ mes: EMes, area: EArea, subArea: ESubArea, sucursal: ESucursal }), sucess, error);
    }
    function fn_EFiltroMes(EAnio, EArea, ESubArea, ESucursal) {
        var html = '';
        var sucess = function (response) {
            var obj = JSON.parse(response.d);
            var data_html = "";
            if (obj == null) {
                data_html = "<option value='0'>Seleccione Opción</option>";
                $("select[id$=cmbEMes_General]").html(data_html);
            }
            else {
                data_html += "<option value='0'>Seleccione Opción</option>";
                for (var i = 0; i < obj.length; i++) {
                    data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>";
                }
                $("select[id$=cmbEMes_General]").html(data_html);
            }
        };
        var error = function (xhr, ajaxOptions, thrownError) {
        };
        fn_LlamadoMetodo("CostoPlanilla.aspx/Personal_CostoPlanilla_EvolucionAreaMensual_ddlMes_CS", JSON.stringify({ anio: EAnio, area: EArea, subArea: ESubArea, sucursal: ESucursal }), sucess, error);
    }

    function Cargar_Grafica_Chart(EAnio, EMes, EArea, ESubArea, ESucursal, EAgrupacion) {
        var sucess = function (response) {
            //fn_Mensaje_Alerta("Confirmacion", "Se esta ejecutando los datos", "", "AlertaMensaje");
            json = response.d.Series
            GraficaChart(response.d.Fecha, response.d.Name, fn_ValidarANulos(json));
            $('#Evolucion_Mensual').show();
            ValidarCampo();
        };
        var error = function (xhr, ajaxOptions, thrownError) {
            fn_Mensaje_Alerta("error", " Ocurrio un error mientras se cargaban los datos", "", "AlertaMensaje");
        };
        fn_LlamadoMetodo("CostoPlanilla.aspx/G_Personal_CostoPlanilla_EvolucionAreaMensual_CS", JSON.stringify({ anio: EAnio, mes: EMes, area: EArea, subArea: ESubArea, sucursal: ESucursal, agrupacionA: EAgrupacion }), sucess, error);
    }

    function GraficaChart(fecha, Name, data) {
        Highcharts.chart('Evolucion_Mensual', {
            chart: {
                type: 'line'
            },
            title: {
                text: 'EVOLUCIÓN POR ÁREA MENSUAL'
            },
            subtitle: {
                text: 'DEPARTAMENTO'
            },
            xAxis: {
                categories: JSON.parse(fecha)
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'TOTAL '
                },
                labels: {
                    formatter: function () {
                        return (this.value) + ' Mil';
                    }
                },
            },
            tooltip: {
                headerFormat: '<b>{point.x}</b><br/>',
                pointFormat: '{series.name}: {point.y:.1f} Mil'
            },
            plotOptions: {
                line: {
                    dataLabels: {
                        enabled: true
                    },
                    enableMouseTracking: true
                }
            },
            series: JSON.parse(data)
        });
    }
</script>


</asp:Content>
