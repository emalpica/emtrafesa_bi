﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage/Intranet.Master" AutoEventWireup="true" CodeBehind="SuspensionesAmonestaciones.aspx.cs" Inherits="WebApplication1.Modulo.Area.Personal.SuspensionesAmonestaciones" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
 <!-- Compiled and minified CSS -->

 <!-- Compiled and minified JavaScript -->
        
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContenedorDashboard" runat="server">
       
     <!-- **********************************************************************************************************************************************************
      FUNCIONALIDAD JAVASCRIPT
      *********************************************************************************************************************************************************** -->
    <!--main content start-->

    <script>


        //######  PERSONAL SUSPENSIONES - EVOLUCION ACTUAL CANTIDAD POR DEPARTAMENTO  ######

        // TODO 1 : ZONA DE VARIABLES
        var area = "";
        var subArea = ""; 
        var sucursal = "";
        var data_html = "";

        //TODO 2 : LLAMADA A FUNCIONES YA IMPLEMENTADOS

        // !Llama a la funcion que se en carga de llenar los filtros
        CargarCombosGenerales(area, subArea, sucursal);
        Grafica_Personal_Suspensiones_Departamento("", "", "");


        // TODO 3 : CAPTURAR ELEMENTOS MEDIANTE JQUERY
        $(document).ready(function () {

            // !Jquery que carga la sucursal
            $("#cmbSucursal").change(function () {
                validarCombos();
                CargarCombosGenerales(area, subArea, sucursal);
            });

            //!Jquery que carga el area
            $("#cmbArea").change(function () {
                validarCombos();
                CargarCombosGenerales(area, subArea, sucursal);
            });

            // !Jquery que carga el subarea
            $("#cmbsubArea").change(function () {
                validarCombos();
                CargarCombosGenerales(area, subArea, sucursal);
            });

             // ! JQUERY que carga la grafica 
            $("#Personal_Departamento").click(function () {
                validarCombos();
                Grafica_Personal_Suspensiones_Departamento(area, subArea, sucursal);
                
            });

            $("#btnLimpiarD").click(function(){
                LimpiarData();
                validarCombos();
                Grafica_Personal_Suspensiones_Departamento("", "", "");

            });
        });

        // TODO 4 : FUNCIONES PARA INTERACTUAR CON LOS FILTROS (COMBOS)

        function CambiarEstado() {
            if (area != 0) $("#cmbArea").val(0); document.getElementById("cmbArea").disabled = false;
            if (subArea != 0) $("#cmbsubArea").val(0); document.getElementById("cmbsubArea").disabled = false;
            if (sucursal != 0) $("#cmbSucursal").val(0); document.getElementById("cmbSucursal").disabled = false;
            validarCombos();
            CargarCombosGenerales(area, subArea, sucursal);
            Grafica_Personal_Suspensiones_Departamento("", "", "");
        }

        // !Captura el valor seleccionado en el filtro area, subarea y  sucursal 
        function validarCombos() {
            area = $("#cmbArea").val(); if (area != 0) { area = $("#cmbArea  option:selected").text(); } else { area = ""; }
            subArea = $("#cmbsubArea").val(); if (subArea != 0) { subArea = $("#cmbsubArea  option:selected").text(); } else { subArea = ""; }
            sucursal = $("#cmbSucursal").val(); if (sucursal != 0) { sucursal = $("#cmbSucursal  option:selected").text(); } else { sucursal = ""; }
        }

        // ! Funcion que permite cargar datos a los combos
        function CargarCombosGenerales(area, subArea, sucursal) {
            if (area == 0 || area == "") fn_General_Cargar_FiltroArea(sucursal, subArea);
            if (subArea == 0 || subArea == "") fn_General_Cargar_FiltrosubArea(sucursal, area);
            if (sucursal == 0 || sucursal == "") fn_General_Cargar_FiltroSucursal(area, subArea);
        }

        function LimpiarData() {
            
            // if(DepartamentoEHGM!=0) $('#cmbDepartamento_EHGM').val(0);
            if(sucursal!=0) $('#cmbSucursal').val(0);
            if(area!=0) $('#cmbArea').val(0);
            if(subArea!=0) $('#cmbsubArea').val(0);
            // if(AnioEHGM!=0) $('#cmbAnio_EHGM').val("");
            // if(MesEHGM!=0) $('#cmbMes_EHGM').val("");
            // $("#opc_trimestreEHGM").prop("checked", false);
            // $("#opc_bimestreEHGM").prop("checked", false);

        }


        //TODO 5: FUNCIONES QUE IMPLEMENTAN LLAMADAS AJAX 

        // ! Funcion para cargar las sucursales en el combo sucursal
        function fn_General_Cargar_FiltroSucursal(area, subArea) {
            var html = '';
            var sucess = function (response) {
                var obj = JSON.parse(response.d);
                var data_html = "";
                if (obj == null) {
                    data_html = "<option value='0'>Seleccionar</option>";
                    $("select[id$=cmbSucursal]").html(data_html);
                }
                else {
                    data_html += "<option value='0'>Seleccionar</option>";
                    for (var i = 0; i < obj.length; i++) {
                        data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>";
                    }
                    $("select[id$=cmbSucursal]").html(data_html);
                }
            };
            var error = function (xhr, ajaxOptions, thrownError) {
            };
            fn_LlamadoMetodo("SuspensionesAmonestaciones.aspx/Personal_Suspensiones_ListarSucursal", JSON.stringify({  area: area, subArea: subArea }), sucess, error);
        }

        // ! Funcion que carga las areas en el combo area
        function fn_General_Cargar_FiltroArea(sucursal, subArea) {
            var html = '';
            var sucess = function (response) {
                var obj = JSON.parse(response.d);
                var data_html = "";
                if (obj == null) {
                    data_html = "<option value='0'>Seleccionar</option>";
                    $("select[id$=cmbArea]").html(data_html);
                }
                else {
                    data_html += "<option value='0'>Seleccionar</option>";
                    for (var i = 0; i < obj.length; i++) {
                        data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>";
                    }
                    $("select[id$=cmbArea]").html(data_html);
                }
            };
            var error = function (xhr, ajaxOptions, thrownError) {
            };
            fn_LlamadoMetodo("SuspensionesAmonestaciones.aspx/Personal_Suspensiones_ListarArea", JSON.stringify({ sucursal: sucursal, subArea: subArea }), sucess, error);
        }

        // !Funcion que carga las subareas en el combo subArea
        function fn_General_Cargar_FiltrosubArea(sucursal, area) {
            var html = '';
            var sucess = function (response) {
                var obj = JSON.parse(response.d);
                var data_html = "";
                if (obj == null) {
                    data_html = "<option value='0'>Seleccionar</option>";
                    $("select[id$=cmbsubArea]").html(data_html);
                }
                else {
                    data_html += "<option value='0'>Seleccionar</option>";
                    for (var i = 0; i < obj.length; i++) {
                        data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>";
                    }
                    $("select[id$=cmbsubArea]").html(data_html);
                }
            };
            var error = function (xhr, ajaxOptions, thrownError) {
            };
            fn_LlamadoMetodo("SuspensionesAmonestaciones.aspx/Personal_Suspensiones_ListarsubArea", JSON.stringify({ area: area, sucursal: sucursal }), sucess, error);
        }
    
        // TODO 6: FUNCION QUE IMPLEMENTA LA GRAFICA
        function Grafica_Personal_Suspensiones_Departamento(area, subArea, sucursal) {
            var sucess = function (response) {
                // fn_Mensaje_Alerta("Confirmacion", "Se esta ejecutando los datos", "", "Mensaje");
                BarrasChart(response.d.Marcas, response.d.Data);
                $('#Suspensiones_Graficar').show();
            };
            var error = function (xhr, ajaxOptions, thrownError) {
                fn_Mensaje_Alerta("Error", " Ocurrio un error mientras se cargaban los datos", "", "Mensaje");
            };
            fn_LlamadoMetodo("SuspensionesAmonestaciones.aspx/Grafica_Personal_Suspensiones_CantidadDepartamento", JSON.stringify({ area: area, subArea: subArea, sucursal: sucursal }), sucess, error);
        }

        // TODO : FUNCION HIGHTCHART QUE PERMITE DAR FORMA A LA GRAFICA
        function BarrasChart(Marcas, Data) { 
         var chart = Highcharts.chart('Suspensiones_Graficar', {
             title: {
                 text: 'CANTIDAD DE SUSPENSIONES SEGÚN DEPARTAMENTO',
                 widthAdjust: -50,
                 margin:10  
                
             },
             xAxis: {
                 categories: Marcas
             },

             yAxis: {
                 title: {
                     text: ''
                 },
             },
             chart: {
                 inverted: true,
                 polar: false,
         
             },
             plotOptions: {
                 series: {
                     stacking: 'normal',
                     dataLabels: {
                         enabled: true,
                         style: {
                             "font-family": "'Open Sans', sans-serif",
                             "-webkit-text-stroke": "2px black",
                             "color": "#ffff",
                             "fontSize": "12px",
                         },
                         format: '{point.y} '
                     }
                 }
             },
             series: [{
                 name: 'Cantidad',
                 type: 'column',
                 colorByPoint: true,
                 data: Data,
                 showInLegend: false
             }]
         });
     }



// ######  KPI: PERSONAL SUSPENSIONES - GRAFICA 2 : EVOLUCION HISTORICA  GENERAL MENSUAL ######

   


// ######  KPI:PERSONAL SUSPENSIONES - GRAFICA 3: EVOLUCION HISTORICA POR AREA MENSUAL ######

    </script>


    <!-- **********************************************************************************************************************************************************
      ESTRUCTURA HTML5
    *********************************************************************************************************************************************************** -->
    <!--main content start-->

<h3 class="panel-body" style="color: #283593; margin-bottom: 0px !important;">SUSPENSIONES Y AMONESTACIONES</h3>
    <div class="text-left Left aligned text" style="margin-left: 18px;">
        <p><strong>Objetivo:</strong> El objetivo del Indicador es mostrar la cantidad de faltas y/o Amonestacion de los colaboradores de la empresa y a la vez sus distintos departamentos y areas.</p>
    </div>
<div class="col-lg-12 main-chart" >
        <div class="panel panel-headline" style="margin-bottom: 3px !important;" >
            <div class="panel-body" class="col-lg-6 main-chart">
                <div class="grid-container ">
                        
                    <div class="row">
                        <!-- Opciones -->

                        <!-- Sucursal -->
                        <div class="col-md-12 alinearComponentes">
                            <div class="col-md-2 ">

                                <!-- <div class="input-group"> -->
                                    <label for=""> <b>Sucursal:</b> </label> 
                                    <asp:DropDownList ID="cmbSucursal" ClientIDMode="Static"  class="form-control" runat="server" Style="width: 100%;" ></asp:DropDownList>
                                <!-- </div> -->

                            </div>

                            <!-- Area -->
                            <div class="col-md-2  " >
                                <!-- <div class="input-group" > -->
                                    <label for=""> <b>Area:</b> </label> 
                                    <asp:DropDownList ID="cmbArea" ClientIDMode="Static"  class="form-control" runat="server" Style="width: 100%;" ></asp:DropDownList>
                                <!-- </div> -->

                            </div>

                             <!-- SubArea -->
                            <div class="col-md-2 " >

                                    <!-- <div class="input-group" > -->
                                        <label for=""> <b>SubArea:</b> </label> 
                                        <asp:DropDownList ID="cmbsubArea" ClientIDMode="Static"  class="form-control" runat="server" Style="width: 100%;" ></asp:DropDownList>
                                    <!-- </div> -->
    
                            </div>

                            <!-- Botones -->
                            <div class="col-md-6  alinearComponentes">
                                
                                <!-- ResetarCHBX -->
                                <div class="col-md-3">
                                    <label for="">Limpiar</label>
                                    <span class="lnr lnr-magic-wand-limpiar" id="btnLimpiarD" > </span>    
                                </div>
                                
                                <!-- BotonGraficar -->
                                <div class="col-md-9">  
                                    <div class="input-group" >

                                         <button id="Personal_Departamento" type="button" class="btn btn-primary btn-sm">GRAFICAR</button>
                                     </div>
                                </div>
                            </div> 

                        </div>   
                    </div>
                </div>
            </div>
        </div>

        <div class="panel panel-headline" style="margin-bottom: 26px !important;">
            <div class="panel-body" class="col-lg-5 main-chart">
                <!-- Grafica -->

      <div class="container-indicador">
        <div class="row">
          <div class="col-md-12">
           <%-- <div class="panel panel-headline">--%>
                <div class="panel-body">
                    <div class="form-inline container-kpi">
                        <div id="Suspensiones_Graficar" style="min-width: 93%; height: 300px; display:none "></div>
                       </div>
                   </div>
             <%--   </div>--%>
            </div>
         </div>
             </div>
                


            </div>
        </div>

       <!--INTERPRETACION DE LA GRAFICA-->
        <!-- <div class="panel panel-headline ">
            <div class="panel-body" class="col-lg-1 main-chart">
               <span> <b>INTERPRETACIÓN:</b>  Se desea que los costos sean los mínimos posisbles.</span>
            </div>
        </div> -->

       
</div>



  <!--KPI: PERSONAL SUSPENSIONES - GRAFICA: EVOLUCION HISTORICA GENERAL MENSUAL   -->
  


   

    <!-- ################################################################################-->



    

    <!--···································································································-->
    
    

  

    <!-- **********************************************************************************************************************************************************
      DISEÑO CSS3 AND FLEXBOX
      *********************************************************************************************************************************************************** -->
    
        <style>
        
        /* .centrar{
            margin-bottom: 10px;
        } */
        .alinearComponentes{
            display: flex;
            flex-direction:  row;
            align-items: center;
            justify-content: flex-start;
            margin-right: 4px;
        }

        .alinearCentrar{
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: center;
            margin-top: 4px;
        }

        

        
        </style>
      <!--main content start-->
    
    
                
</asp:content>




