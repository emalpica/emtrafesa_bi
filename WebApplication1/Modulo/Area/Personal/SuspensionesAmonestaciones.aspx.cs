﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Script.Serialization;
using System.Web.Services;
using ENTIDAD.Graficas;

namespace WebApplication1.Modulo.Area.Personal
{
    public partial class SuspensionesAmonestaciones : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        public static object Personal_Suspensiones_ListarSucursal( String area, String subArea)
        {
            DataTable filtro_Sucursal_suspensiones = LOGICA.PersonalL.Instancia.Personal_Suspensiones_ListarSucursal( area, subArea);
            JavaScriptSerializer sr = new JavaScriptSerializer();
            List<String> sucursal = new List<String>();

            if (filtro_Sucursal_suspensiones != null)
            {
                for (int i = 0; i < filtro_Sucursal_suspensiones.Rows.Count; i++)
                {
                    if ((filtro_Sucursal_suspensiones.Rows[i]["Sucursal"].ToString()) == "NULL")
                    {
                        sucursal.Add("NO INDICA");
                    }
                    else
                    {
                        sucursal.Add(filtro_Sucursal_suspensiones.Rows[i]["Sucursal"].ToString());
                    }
                }
            }
            else { return null; }
            return sr.Serialize(sucursal);
        }

        [WebMethod]
        public static String Personal_Suspensiones_ListarArea(String sucursal, String subArea)
        {
            DataTable filtro_area = LOGICA.PersonalL.Instancia.Personal_Suspensiones_ListarArea(sucursal, subArea);
            JavaScriptSerializer sr = new JavaScriptSerializer();
            List<string> cmbArea = new List<string>();
            if (filtro_area != null)
            {
                for (int i = 0; i < filtro_area.Rows.Count; i++)
                {
                    if ((filtro_area.Rows[i]["Area"].ToString()) == "NULL")
                    {
                        cmbArea.Add("NO INDICA");
                    }
                    else
                    {
                        cmbArea.Add(filtro_area.Rows[i]["Area"].ToString());
                    }
                }
            }
            else
            {
                return null;
            }
            return sr.Serialize(cmbArea);
        }


        [WebMethod]
        public static String Personal_Suspensiones_ListarsubArea( String area, String sucursal)
        {
            DataTable filtro_subArea = LOGICA.PersonalL.Instancia.Personal_Suspensiones_ListarSubArea( area, sucursal);
            JavaScriptSerializer sr = new JavaScriptSerializer();
            List<string> cmbsubArea = new List<string>();
            if (filtro_subArea != null)
            {
                for (int i = 0; i < filtro_subArea.Rows.Count; i++)
                {
                    if ((filtro_subArea.Rows[i]["SubArea"].ToString()) == "NULL")
                    {
                        cmbsubArea.Add("NO INDICA");
                    }
                    else
                    {
                        cmbsubArea.Add(filtro_subArea.Rows[i]["SubArea"].ToString());
                    }

                }
            }
            else
            {
                return null;
            }
            return sr.Serialize(cmbsubArea);

        }
        [WebMethod]
        public static Object Grafica_Personal_Suspensiones_CantidadDepartamento(String area, String subArea, String sucursal)
        {
            List<String> Marcas = new List<String>();
            List<Decimal> Data = new List<Decimal>();
            DataTable dt = LOGICA.PersonalL.Instancia.Grafica_Persona_Suspensiones_Cantidad_Suspensiones_Departamento(area, subArea, sucursal);

            if (dt != null)
            {
                int x = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    Data.Add(Convert.ToDecimal((dr[x + 1]).ToString()));
                    Marcas.Add((dr[x]).ToString());
                }

                JavaScriptSerializer sr = new JavaScriptSerializer();
                return new { Marcas, Data, };
            }
            else { return null; }
        }


         /* ####### FUNCION PARA GRAFICAR EVOLUCION HISTORICA GENERAL MENSUAL   ##### */
         
         [WebMethod]
         public static object Personal_Suspensiones_ListarSucursal_EHGM_ASPX1(String Anio, String Area, String SubArea, String Departamento, String Mes){
            DataTable filtro_Sucursal_EHGM = LOGICA.PersonalL.Instancia.Personal_Suspensiones_ListarSucursal_EHGM_LO( Anio, Area, SubArea,Departamento, Mes);
            JavaScriptSerializer sr = new JavaScriptSerializer();
            List<string> cmbSucursal_EHGM1 = new List<string>();

            if (filtro_Sucursal_EHGM != null)
            {
                for (int i = 0; i < filtro_Sucursal_EHGM.Rows.Count; i++)
                {
                    if ((filtro_Sucursal_EHGM.Rows[i]["Sucursal"].ToString()) == "NULL")
                    {
                        cmbSucursal_EHGM1.Add("NO INDICA");
                    }
                    else
                    {
                        cmbSucursal_EHGM1.Add(filtro_Sucursal_EHGM.Rows[i]["Sucursal"].ToString());
                    }

                }
            }
            else
            {
                return null;
            }
            return sr.Serialize(cmbSucursal_EHGM1);

         }

        [WebMethod]
        public static Object Personal_Suspensiones_ListarDepartamento_EHGM_ASPX(String Anio, String Area, String SubArea, String Sucursal, String Mes){
            DataTable filtro_Departamento_EHGM = LOGICA.PersonalL.Instancia.Personal_Suspensiones_ListarDepartamento_EHGM_LO( Anio, Area, SubArea, Sucursal, Mes);
            JavaScriptSerializer sr = new JavaScriptSerializer();
            List<string> cmbDepartamento_EHGM = new List<string>();

            if (filtro_Departamento_EHGM != null)
            {
                for (int i = 0; i < filtro_Departamento_EHGM.Rows.Count; i++)
                {
                    if (filtro_Departamento_EHGM.Rows[i]["Departamento"].ToString() == "NULL")
                    {
                        cmbDepartamento_EHGM.Add("NO INDICA");
                    }
                    else
                    {
                        cmbDepartamento_EHGM.Add(filtro_Departamento_EHGM.Rows[i]["Departamento"].ToString());
                    }

                }
            }
            else
            {
                return null;
            }
            return sr.Serialize(cmbDepartamento_EHGM);

         }

        [WebMethod]
        public static Object Personal_Suspensiones_ListarArea_EHGM_ASPX(String Anio, String SubArea, String Sucursal, String Departamento,String Mes){
            DataTable dt_Area_EHGM=LOGICA.PersonalL.Instancia.Personal_Suspensiones_ListarArea_EHGM_LO(  Anio,  SubArea,  Sucursal, Departamento, Mes);
            JavaScriptSerializer sr = new JavaScriptSerializer();
            List<String> cmbArea_EHGM= new List<string>();

            if( dt_Area_EHGM!=null){
                for(int i=0; i<dt_Area_EHGM.Rows.Count; i++){
                    if( dt_Area_EHGM.Rows[i]["Area"].ToString()=="NULL"){
                        cmbArea_EHGM.Add("NO INDICA");
                    }
                    else{
                        cmbArea_EHGM.Add(dt_Area_EHGM.Rows[i]["Area"].ToString() );
                    }
                }

            } 
            else{
                return null;
          }

          return sr.Serialize(cmbArea_EHGM);

        }

         [WebMethod]
         public static Object Personal_Suspensiones_Listar_SubArea_EHGM_ASPX(String Departamento, String Area, String Sucursal, String Anio, String Mes){
            DataTable dt_SubArea_EHGM= LOGICA.PersonalL.Instancia.Personal_Suspensiones_ListarSubArea_EHGM_LO( Departamento,  Area,  Sucursal,  Anio,  Mes);
            JavaScriptSerializer sr = new JavaScriptSerializer();
            List<String> cmbSubArea_EHGM = new  List<String>();
            
            if(dt_SubArea_EHGM != null){
                for(int i=0; i<dt_SubArea_EHGM.Rows.Count;i++){
                    if( dt_SubArea_EHGM.Rows[i]["SubArea"].ToString()=="NULL"){
                        cmbSubArea_EHGM.Add("NO INDICA");
                    }
                    else{
                        cmbSubArea_EHGM.Add(dt_SubArea_EHGM.Rows[i]["SubArea"].ToString());
                    }
                }
            }
            else{
                return null;
            }

            return sr.Serialize(cmbSubArea_EHGM);
        
        }

        [WebMethod]
        public static Object Personal_Suspensiones_ListarAño_EHGM_ASPX(string Departamento, string SubArea, string Area, string Sucursal, string Mes){
            DataTable dt_Año_EHGM= LOGICA.PersonalL.Instancia.Personal_Suspensiones_ListarAño_EHGM_LO(  Departamento, SubArea, Area, Sucursal,  Mes);
            JavaScriptSerializer sr = new JavaScriptSerializer();
            List<String> cmbAnio_EHGM = new  List<String>();
            
            if(dt_Año_EHGM != null){
                for(int i=0; i<dt_Año_EHGM.Rows.Count;i++){
                    if( dt_Año_EHGM.Rows[i]["Anio"].ToString()=="NULL"){
                        cmbAnio_EHGM.Add("NO INDICA");
                    }
                    else{
                        cmbAnio_EHGM.Add(dt_Año_EHGM.Rows[i]["Anio"].ToString());
                    }
                }
            }
            else{
                return null;
            }

            return sr.Serialize(cmbAnio_EHGM);
        
        }

        [WebMethod]
        public static Object Personal_Suspensiones_ListarMes_EHGM_ASPX(String Departamento, string SubArea, string Area, string Sucursal, string Año){
            DataTable dt_Mes_EHGM= LOGICA.PersonalL.Instancia.Personal_Suspensiones_ListarMes_EHGM_LO( Departamento,  SubArea,Area,  Sucursal,Año);
            JavaScriptSerializer sr = new JavaScriptSerializer();
            List<String> cmbMes_EHGM = new  List<String>();
            
            if(dt_Mes_EHGM != null){
                for(int i=0; i<dt_Mes_EHGM.Rows.Count;i++){
                    if( dt_Mes_EHGM.Rows[i]["Mes"].ToString()=="NULL"){
                        cmbMes_EHGM.Add("NO INDICA");
                    }
                    else{
                        cmbMes_EHGM.Add(dt_Mes_EHGM.Rows[i]["Mes"].ToString());
                    }
                }
            }
            else{
                return null;
            }

            return sr.Serialize(cmbMes_EHGM);
        }

        //[WebMethod]
        // public static object Grafica_Personal_Suspensiones_EvolucionGeneralMensual(string Anio, string Departamento, string Area, string SubArea, string Sucursal, string Mes,String agrupacionF)
        //{

        //    int indiceColum = 0;
        //    int agrupacion = Convert.ToInt32(agrupacionF);
        //    List<Object> contenedor = new List<Object>();
        //    List<String> Columnas = new List<String>(); //nombre de marca
        //    List<Ratio> series = new List<Ratio>();
        //    DataTable dt = LOGICA.PersonalL.Instancia.Grafica_Personal_Suspensiones_EvolucionGeneralMensual_LO(Anio,Departamento, Area, SubArea, Sucursal,Mes);
        //    LineChart data = new LineChart();

        //    if (dt != null)
        //    {
        //        foreach (DataColumn dataColumn in dt.Columns)
        //        {
        //            Columnas.Add(dataColumn.ColumnName);
        //        }
        //        int j = 1;

        //        foreach (DataRow dr in dt.Rows)
        //        {
        //            j = 1;
        //            data.Fecha.Add(dr[0].ToString());
        //            data.Name = Columnas;

        //            while (j < Columnas.Count)
        //            {
        //                if (data.Series.Count < Columnas.Count - 1)
        //                    data.Series.Add(new Ratio());
        //                string value = dr[j].ToString();

        //                data.Series[j - 1].data.Add(Convert.ToDecimal(value));
        //                data.Series[j - 1].name = Columnas[j];
        //                j++;
        //            }
        //        }
        //        if (agrupacion == 0)
        //        {
        //            JavaScriptSerializer sr = new JavaScriptSerializer();
        //            return new { Fecha = sr.Serialize(data.Fecha), Name = sr.Serialize(data.Name), Series = sr.Serialize(data.Series), };
        //        }
        //    }
        //    //trimestre and bmestre
        //    String fechaAnio = "";
        //    Decimal acumulador = 0;
        //    Decimal total = 0;
        //    int contadorTrimestre = 1;
        //    String anioActual = "";
        //    String nombreAgrupacion = "";
        //    LineChart data1 = new LineChart();
        //    if (agrupacion != 0)
        //    {
        //        if (agrupacion == 2) { nombreAgrupacion = "Bimestre ";  }
        //        if (agrupacion == 3) { nombreAgrupacion = "Trimestre ";  }
        //        for (int m = 0; m < data.Fecha.Count; m++)
        //        {


        //            if (fechaAnio != anioActual) { contadorTrimestre = 1; }
        //            if (m % agrupacion == 0)
        //            {
        //                data1.Fecha.Add(nombreAgrupacion + (contadorTrimestre).ToString());
        //                contadorTrimestre++;
        //            }
        //            else
        //            {
        //                if (m == data.Fecha.Count - 1)
        //                {
        //                    data1.Fecha.Add(nombreAgrupacion + (contadorTrimestre).ToString());
        //                    contadorTrimestre++;
        //                }
        //            }

        //        }

        //        int contadorObjeto = 0;
        //        for (int i = 0; i < data.Series.Count; i++)
        //        {
        //            data1.Series.Add(new Ratio());
        //            for (int j = 0; j < data.Series[i].data.Count; j++)
        //            {
        //                contadorObjeto = j + 1;
        //                var value = data.Series[i].data[j];
        //                acumulador = acumulador + value;
        //                if (contadorObjeto % agrupacion == 0 && contadorObjeto != 0)
        //                {
        //                    total = acumulador / agrupacion;
        //                    data1.Series[i].data.Add(Math.Round(total, 1));
        //                    acumulador = 0;
        //                    total = 0;
        //                }
        //                else
        //                {
        //                    if (contadorObjeto == data.Series[i].data.Count)
        //                    {
        //                        total = acumulador / agrupacion;
        //                        data1.Series[i].data.Add(Math.Round(total, 1));
        //                        acumulador = 0;
        //                        total = 0;
        //                    }
        //                }

        //            }
        //            data1.Series[i].name = Columnas[i + 1];
        //        }
        //        JavaScriptSerializer sr = new JavaScriptSerializer();
        //        return new { Fecha = sr.Serialize(data1.Fecha), Name = sr.Serialize(data.Name), Series = sr.Serialize(data1.Series) };
        //    }
        //    else { return null; }

    

/* ####### FUNCION PARA GRAFICAR EVOLUCION HISTORICA AREA MENSUAL   ##### */
       
       
    }
}