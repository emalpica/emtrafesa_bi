﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Intranet.Master" AutoEventWireup="true" CodeBehind="FaltasDescansosMedico.aspx.cs" Inherits="WebApplication1.Modulo.Area.Personal.FaltasDescansosMedico" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContenedorDashboard" runat="server">

    <h3 class="panel-body" style="color: #283593; margin-top: 0px;">FALTAS Y DESCANSOS MÉDICOS</h3>
        <div class="text-left Left aligned text" style="margin-left: 18px;">
        <p><strong>Objetivo:</strong> El objetivo del indicador es mostrar la cantidad de faltas y/o amonestaciones de los colaboradores de la empresa y a la vez sus distintos departamentos y áreas.</p>
    </div>

    <div class="row">
        <div class="col-lg-12">
         <div id="Mensaje">
            </div>
                <div class="panel panel-headline">
                   <div class="panel-body">
                      <div class="form-inline">
                          <div class="form-group col-lg-2 col-md-2 col-sm-3">
                        <label for="formGroupExampleInput">Tipo:</label>
                            <asp:DropDownList ID="cmbTipo_General" ClientIDMode="Static" class="form-control" runat="server" Style="width: 100%;"> </asp:DropDownList>
                          </div>

                      <div class="form-group col-lg-2 col-md-2 col-sm-3">
                        <label for="formGroupExampleInput">Sucursal:</label>
                            <asp:DropDownList ID="cmbSucursal_General" ClientIDMode="Static" class="form-control" runat="server" Style="width: 100%;"></asp:DropDownList>
                        </div>

                        <div class="form-group col-lg-2 col-md-2 col-sm-3">
                        <label for="formGroupExampleInput">Area:</label>
                            <asp:DropDownList ID="cmbArea_general" ClientIDMode="Static" class="form-control" runat="server" Style="width: 100%;"></asp:DropDownList>
                        </div>

                        <div class="form-group col-lg-2 col-md-2 col-sm-3">
                            <label for="formGroupExampleInput">SubArea:</label>
                            <asp:DropDownList ID="cmbSubArea_general" ClientIDMode="Static" class="form-control" runat="server" Style="width: 100%;"></asp:DropDownList>
                        </div>

                          <div class="form-group col-md-1 col-md-1 col-md-1 col-md-1">
                            <br />
                            <label for="formGroupExampleInput">Limpiar:</label>
                              <br />
                             <span class="lnr lnr-magic-wand-limpiar"  onclick="Cambiarestado()"></span>
                       
                        </div>

                        <div class="form-group col-md-2 col-sm-6 col-sm-12">
                        <br />
                            <button id="Personal_AusentismoYDescansoG" type="button" class="btn btn-primary btn-sm">GRAFICAR</button>
                        </div>
                    </div>
               </div>
            </div>

                <div class="col-md-3">
            <div class="metric">
                <div id="cantidad">
                </div>
            </div>
        </div>
       <div class="col-md-9">
          <div class="panel-body">
              <div id="AusentismoYFaltas" style="min-width: 310px; height: 400px; margin: 0 auto; display:none"></div>  
         </div>   
           <div class="col-md-9">
               <div class="text-left Left aligned text">
                <p><strong>Interpretacion:</strong> Se desea que las faltas y/o amonestaciones disminuyan y/o sean las menores posibles.</p>
            </div>
                </div>
       </div>
   </div>
 </div>

    <%--   <h3 class="panel-body" style="color: #283593; margin-top: 0px;">EVOLUCIÓN GENERAL MENSUAL</h3>--%>

  <div class="col-lg-6">
      <div id="Alert">
        </div>  
          <div class="row">
            <div class="col-lg-12">
             <div class="panel panel-headline">
                <div class="panel-body">
                    <div class="form-inline">
                       <div class="form-group col-lg-3 col-md-3 col-sm-3">
                        <label for="formGroupExampleInput">Tipo:</label>
                            <asp:DropDownList ID="cmbTipo_General_M" ClientIDMode="Static" class="form-control" runat="server" Style="width: 100%;"> </asp:DropDownList>
                          </div>
                       <div class="form-group col-lg-3 col-md-3 col-sm-3">
                        <label for="formGroupExampleInput">Sucursal:</label>
                            <asp:DropDownList ID="cmbSucursal_General_M" ClientIDMode="Static" class="form-control" runat="server" Style="width: 100%;"></asp:DropDownList>
                        </div>

                        <div class="form-group col-lg-3 col-md-3 col-sm-3">
                            <label for="formGroupExampleInput">Departamento:</label>
                            <asp:DropDownList ID="cmbDepartamento_general_M" ClientIDMode="Static" class="form-control" runat="server" Style="width: 100%;"></asp:DropDownList>
                        </div>

                        <div class="form-group col-lg-3 col-md-3 col-sm-3">
                        <label for="formGroupExampleInput">Area:</label>
                            <asp:DropDownList ID="cmbArea_general_M" ClientIDMode="Static" class="form-control" runat="server" Style="width: 100%;"></asp:DropDownList>
                        </div>

                         <div class="form-group col-lg-3 col-md-3 col-sm-3">
                            <label for="formGroupExampleInput">SubArea:</label>
                            <asp:DropDownList ID="cmbSubArea_general_M" ClientIDMode="Static" class="form-control" runat="server" Style="width: 100%;"></asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-3 col-md-3 col-sm-3">
                            <label for="formGroupExampleInput">Mes:</label>
                            <asp:DropDownList ID="cmbMes_General" ClientIDMode="Static" class="form-control" runat="server" Style="width: 100%;"></asp:DropDownList>
                        </div>
                         <div class="form-group col-lg-3 col-md-3 col-sm-3">
                            <br />
                            <label for="formGroupExampleInput">Agrupación:</label>
                            <label class="fancy-checkbox">
                                <input type="checkbox" id="opc_trimestre">
                                <span>Trimestre</span>
                            </label>
                            <label class="fancy-checkbox">
                                <input type="checkbox" id="opc_bimestre">
                                <span>Bimestre</span>
                            </label><br />
                    </div>  
                        <div class="form-group col-lg-3 col-md-3 col-sm-3">
                            <br />
                            <label for="formGroupExampleInput">Limpiar:</label>
                            <br />
                             <span class="lnr lnr-magic-wand-limpiar"  onclick="Cambiarestado_M()"></span>

                        </div>
                          <div class="form-group col-lg-3 col-md-3 col-sm-3">
                        <label for="formGroupExampleInput"></label>
                       </div>
                         <div class="form-group col-lg-4 col-md-4 col-sm-4">
            
                            <button id="Personal_AusentismoYDescanso_Evolucion_Mes" type="button" class="btn btn-primary btn-sm">GRAFICAR</button>
                        </div>
                      
                    </div>
               </div>
            </div>
       </div>
 </div>
      <div class="col-lg-15">
          <div class="row panel-body">
              <div id="AusentismoYDescanso_EvolucionMensual" style="min-width: 310px; height: 400px; margin: 0 auto; display:none"></div>   
          </div>   
       </div>
     <div class="text-left Left aligned text">
                <p><strong>Interpretacion:</strong>Se desea que las faltas y/o amonestaciones disminuyan y/o sean las menores posibles. </p>
            </div>
       </div>
    
    

     
 <%--  <h3 class="panel-body" style="color: #283593; margin-top: 0px;">EVOLUCIÓN POR ÁREA MENSUAL</h3>--%>
         
         <div class="col-lg-6">
              <div id="AlertaMensaje">
         </div>
          <div class="row">
            <div class="col-lg-12">

            <div class="panel panel-headline">
                <div class="panel-body">
                    <div class="form-inline">
                          <div class="form-group col-lg-3 col-md-3 col-sm-3">
                        <label for="formGroupExampleInput">Tipo:</label>
                            <asp:DropDownList ID="cmbETipo_General" ClientIDMode="Static" class="form-control" runat="server" Style="width: 100%;"> </asp:DropDownList>
                          </div>
                       <div class="form-group col-lg-3 col-md-3 col-sm-3">
                        <label for="formGroupExampleInput">Sucursal:</label>
                            <asp:DropDownList ID="cmbESucursal_General" ClientIDMode="Static" class="form-control" runat="server" Style="width: 100%;"></asp:DropDownList>
                        </div>

                      <div class="form-group col-lg-3 col-md-3 col-sm-3">
                        <label for="formGroupExampleInput">Area:</label>
                            <asp:DropDownList ID="cmbEArea_general" ClientIDMode="Static" class="form-control" runat="server" Style="width: 100%;"></asp:DropDownList>
                        </div>

                        <div class="form-group col-lg-3 col-md-3 col-sm-3">
                            <label for="formGroupExampleInput">SubArea:</label>
                            <asp:DropDownList ID="cmbESubArea_general" ClientIDMode="Static" class="form-control" runat="server" Style="width: 100%;"></asp:DropDownList>
                        </div>
                         <div class="form-group col-lg-3 col-md-3 col-sm-3">
                            <label for="formGroupExampleInput">Año:</label>
                            <asp:DropDownList ID="cmbEAnio_General" ClientIDMode="Static" class="form-control" runat="server" Style="width: 100%;"></asp:DropDownList>
                        </div>
                           <div class="form-group col-lg-3 col-md-3 col-sm-3">
                            <label for="formGroupExampleInput">Mes:</label>
                            <asp:DropDownList ID="cmbEMes_General" ClientIDMode="Static" class="form-control" runat="server" Style="width: 100%;"></asp:DropDownList>
                        </div>
                       </div>
                        <div class="form-group col-lg-3 col-md-3 col-sm-3">
                            <br />
                            <label for="formGroupExampleInput">Agrupación:</label>
                        <label class="fancy-checkbox">
                                <input type="checkbox" id="opc_Etrimestre">
                                <span>Trimestre</span>
                            </label>
                        <label class="fancy-checkbox">
                                <input type="checkbox" id="opc_Ebimestre">
                                <span>Bimestre</span>
                            </label>
                      </div>
                           <div class="form-group col-lg-3 col-md-3 col-sm-3">
                               <br />
                            <label for="formGroupExampleInput">Limpiar:</label>  
                               <br />
                              <span class="lnr lnr-magic-wand-limpiar"  onclick="Estado()"></span>
                       </div>
                    
                       <div class="form-group col-lg-4 col-md-4 col-sm-4">
                   
                        <br />
                            <button id="Personal_AusentismoYDescanso_EvaluacionArea" type="button" class="btn btn-primary btn-sm">GRAFICAR</button>
                        </div>
                    
                    </div>
               </div>
            </div>
        </div>
     <div class="col-lg-15">
         <div class="row panel-body">
              <div id="Evolucion_Area_Mensual" style="min-width: 310px; height: 400px; margin: 0 auto; display:none"></div>  
               </div>   
           </div>

            <div class="text-left Left aligned text">
                <p><strong>Interpretacion:</strong>Se desea que las faltas y/o amonestaciones disminuyan y/o sean las menores posibles. </p>
            </div>
 </div> 
    <script>
        var Area = ""; var SubArea = ""; var Sucursal = ""; var tipo = "0"; var CTipo = "";var titulo = "";
        fn_CargarCombos(CTipo, Area, SubArea, Sucursal);
        fn_General_AusentismoYDescanso_cantidadGeneral(tipo);
        Cargar_Grafica("", "", "","0",titulo);
        $(document).ready(function () {

            $("#cmbArea_general").change(function () {
                ValidarCampo();
                fn_CargarCombos(CTipo, Area, SubArea, Sucursal);
            });
            $("#cmbTipo_General").change(function () {
                ValidarCampo();
                fn_CargarCombos(CTipo, Area, SubArea, Sucursal);
            });
            $("#cmbSubArea_general").change(function () {
                ValidarCampo();
                fn_CargarCombos(CTipo, Area, SubArea, Sucursal);
            });

            $("#cmbSucursal_General").change(function () {
                ValidarCampo();
                fn_CargarCombos(CTipo, Area, SubArea, Sucursal);
            });

            $("#Personal_AusentismoYDescansoG").click(function () {
                ValidarCampo();
                fn_General_AusentismoYDescanso_cantidadGeneral(tipo);
                Cargar_Grafica(Area, SubArea, Sucursal, tipo,titulo);
            });
        });

        function fn_CargarCombos(CTipo,Area, SubArea, Sucursal) {
            if (Area == 0 || Area == "") fn_General_Cargar_FiltroArea(Sucursal, SubArea);
            if (SubArea == 0 || SubArea == "") fn_General_Cargar_FiltroSubArea(Sucursal, Area);
            if (Sucursal == 0 || Sucursal == "") fn_General_Cargar_FiltroSucursal(Area, SubArea);
            if (CTipo == 0 || CTipo == "") fn_General_Cargar_FiltroTipo();
        }
        function ValidarCampo() {
            Area = $("#cmbArea_general").val(); if (Area != 0) { Area = $("#cmbArea_general option:selected").text(); } else { Area = ""; }
            SubArea = $("#cmbSubArea_general").val(); if (SubArea != 0) { SubArea = $("#cmbSubArea_general option:selected").text(); } else { SubArea = ""; }
            Sucursal = $("#cmbSucursal_General").val(); if (Sucursal != 0) { Sucursal = $("#cmbSucursal_General option:selected").text(); } else { Sucursal = ""; }
            CTipo = $("#cmbTipo_General").val(); if (CTipo != 0) { CTipo = $("#cmbTipo_General option:selected").text(); } else { CTipo = ""; }
                if (CTipo === "Descansos Medicos") {
                    tipo = "1";
                } else {
                    tipo = "0";
                }
        }
        function Cambiarestado() {
            if (Area != 0) $("#cmbArea_general").val(0); document.getElementById("cmbArea_general").disabled = false;
            if (SubArea != 0) $("#cmbSubArea_general").val(0); document.getElementById("cmbSubArea_general").disabled = false;
            if (Sucursal != 0) $("#cmbSucursal_General").val(0); document.getElementById("cmbSucursal_General").disabled = false;
            ValidarCampo();
            fn_CargarCombos(Area, SubArea, Sucursal);
            tipo = "0";
            fn_General_AusentismoYDescanso_cantidadGeneral(tipo);
            Cargar_Grafica("", "", "", "0");
        }
        function fn_General_AusentismoYDescanso_cantidadGeneral() {
            var html = '';
            var sucess = function (response) {
                var obj = JSON.parse(response.d);
                var data_html = "";
                if (tipo === "1") {
                    if (obj == null) {
                        data_html =
                            "<span class=\"icon\"><i class=\"fa fa - bar - chart\"></i></span>" +
                            " <p>" +
                            "<span class=\"number\">1,252</span>" +
                            "<span class=\"title\">Cantidad General de Descansos Médicos </span>" +
                            "</p>";
                        $("div[id$=cantidad]").html(data_html);
                    }
                    else {
                        for (var i = 0; i < obj.length; i++) {
                            data_html =
                                "<span class=\"icon\"><i class=\"fa fa-bar-chart\"></i></span><p>" +
                                "<span class=\"number\" id=\"Descanso_cantidadG\">" + obj[i] +
                                "</span><span class=\"title\">Cantidad General de Descansos Médicos </span></p>";
                        }
                        $("div[id$=cantidad]").html(data_html);
                    }
                }
                else {
                    if (tipo === "0") {
                        if (obj == null) {
                            data_html =
                                "<span class=\"icon\"><i class=\"fa fa - bar - chart\"></i></span>" +
                                " <p>" +
                                "<span class=\"number\">1,252</span>" +
                                "<span class=\"title\">Cantidad General de Faltas </span>" +
                                "</p>";
                            $("div[id$=cantidad]").html(data_html);
                        }
                        else {
                            for (var i = 0; i < obj.length; i++) {
                                data_html =
                                    "<span class=\"icon\"><i class=\"fa fa-bar-chart\"></i></span><p>" +
                                    "<span class=\"number\" id=\"Faltas_cantidadG\">" + obj[i] +
                                    "</span><span class=\"title\">Cantidad General de Faltas </span></p>";
                            }
                            $("div[id$=cantidad]").html(data_html);
                        }

                    }
                }
            };
            var error = function (xhr, ajaxOptions, thrownError) {
            };
            fn_LlamadoMetodo("FaltasDescansosMedico.aspx/Personal_AusentismoYDescanso_cantidadGeneral_CS", JSON.stringify({ TipoG:tipo }), sucess, error);
        }
        function fn_General_Cargar_FiltroTipo(){
            var html = '';
            var sucess = function (response) {
                var obj = JSON.parse(response.d);
                var data_html = "";
                if (obj == null) {
                    data_html = "<option value='0'>Seleccione Opción</option>";
                    $("select[id$=cmbTipo_General]").html(data_html);
                }
                else {
                    data_html += "<option value='0'>Seleccione Opción</option>";
                    for (var i = 0; i < obj.length; i++) {
                        data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>";
                    }
                    $("select[id$=cmbTipo_General]").html(data_html);
                }
            };
            var error = function (xhr, ajaxOptions, thrownError) {
            };
            fn_LlamadoMetodo("FaltasDescansosMedico.aspx/Personal_AusentismoyDescanso_Tipo_CS", JSON.stringify({  }), sucess, error);
        }
        function fn_General_Cargar_FiltroArea(Sucursal, SubArea) {
            var html = '';
            var sucess = function (response) {
                var obj = JSON.parse(response.d);
                var data_html = "";
                if (obj == null) {
                    data_html = "<option value='0'>Seleccione Opción</option>";
                    $("select[id$=cmbArea_general]").html(data_html);
                }
                else {
                    data_html += "<option value='0'>Seleccione Opción</option>";
                    for (var i = 0; i < obj.length; i++) {
                        data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>";
                    }
                    $("select[id$=cmbArea_general]").html(data_html);
                }
            };
            var error = function (xhr, ajaxOptions, thrownError) {
            };
            fn_LlamadoMetodo("FaltasDescansosMedico.aspx/Personal_AusentismoYDescanso_CantidadArea_ddlArea_CS", JSON.stringify({ sucursal: Sucursal, subArea: SubArea }), sucess, error);
        }
        function fn_General_Cargar_FiltroSubArea(Sucursal, Area) {
            var html = '';
            var sucess = function (response) {
                var obj = JSON.parse(response.d);
                var data_html = "";
                if (obj == null) {
                    data_html = "<option value='0'>Seleccione Opción</option>";
                    $("select[id$=cmbSubArea_general]").html(data_html);
                }
                else {
                    data_html += "<option value='0'>Seleccione Opción</option>";
                    for (var i = 0; i < obj.length; i++) {
                        data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>";
                    }
                    $("select[id$=cmbSubArea_general]").html(data_html);
                }
            };
            var error = function (xhr, ajaxOptions, thrownError) {
            };
            fn_LlamadoMetodo("FaltasDescansosMedico.aspx/Personal_AusentismoYDescanso_CantidadArea_ddlSubArea_CS", JSON.stringify({ sucursal: Sucursal, area: Area }), sucess, error);
        }

        function fn_General_Cargar_FiltroSucursal(Area, SubArea) {
            var html = '';
            var sucess = function (response) {
                var obj = JSON.parse(response.d);
                var data_html = "";
                if (obj == null) {
                    data_html = "<option value='0'>Seleccione Opción</option>";
                    $("select[id$=cmbSucursal_General]").html(data_html);
                }
                else {
                    data_html += "<option value='0'>Seleccione Opción</option>";
                    for (var i = 0; i < obj.length; i++) {
                        data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>";
                    }
                    $("select[id$=cmbSucursal_General]").html(data_html);
                }
            };
            var error = function (xhr, ajaxOptions, thrownError) {
            };
            fn_LlamadoMetodo("FaltasDescansosMedico.aspx/Personal_AusentismoYDescanso_CantidadArea_ddlSucursal_CS", JSON.stringify({ area: Area, subArea: SubArea }), sucess, error);
        }

        function Cargar_Grafica(Area, SubArea, Sucursal,tipo,titulo) {
            var sucess = function (response) {
              if (tipo === "1" ) {
                    titulo = "CANTIDAD DE DESCANSOS MÉDICOS SEGÚN DEPARTAMENTO";
                }
               else  {
                    titulo = "CANTIDAD DE FALTAS SEGÚN DEPARTAMENTO";
                }
                BarrasChart(response.d.Marcas, response.d.Data,titulo);
                $('#AusentismoYFaltas').show();
            };
            var error = function (xhr, ajaxOptions, thrownError) {
                fn_Mensaje_Alerta("Error", " Ocurrio un error mientras se cargaban los datos", "", "Mensaje");
            };
            fn_LlamadoMetodo("FaltasDescansosMedico.aspx/G_Personal_AusentismoYDescanso_CantidadArea_CS", JSON.stringify({ area: Area, subArea: SubArea, sucursal: Sucursal, TipoG:tipo }), sucess, error);
        }
        function BarrasChart(Marcas, Data,titulo) {
            var chart = Highcharts.chart('AusentismoYFaltas', {
                title: {
                    text: titulo

                },
                xAxis: {
                    categories: Marcas
                },

                yAxis: {
                    title: {
                        text: ''
                    },
                },
                chart: {
                    inverted: true,
                    polar: false,

                },
                plotOptions: {
                    series: {
                        stacking: 'normal',
                        dataLabels: {
                            enabled: true,
                            style: {
                                "font-family": "'Open Sans', sans-serif",
                                "-webkit-text-stroke": "2px black",
                                "color": "#ffff",
                                "fontSize": "12px",
                            },
                            format: '{point.y} '
                        }
                    }
                },
                series: [{
                    name: 'Total',
                    type: 'column',
                    colorByPoint: true,
                    data: Data,
                    showInLegend: false
                }]
            });
        }

        //EVOLUCIÓN GENERAL MENSUAL
        var ddlDepartamento = ""; var ddlArea = ""; var ddlSubArea = ""; var ddlSucursal = ""; var Mes = ""; var agrupacion = "0"; var tipoM = "0"; var CTipoM = "";
        var tituloM = "";
        fn_CargarCombos_EvolucionMensual(CTipoM,Mes, ddlDepartamento, ddlArea, ddlSubArea, ddlSucursal);
        Cargar_Grafica_EvolucionMensual("", "", "", "", "", "0", "0", tituloM);
        $(document).ready(function () {

            $("#opc_trimestre").change(function () {
                $("#opc_bimestre").prop("checked", false);
                ValidarCampo_M();
                Cargar_Grafica_EvolucionMensual(Mes, ddlDepartamento, ddlArea, ddlSubArea, ddlSucursal, agrupacion, tipoM, tituloM);
            });

            $("#opc_bimestre").change(function () {
                $("#opc_trimestre").prop("checked", false);
                ValidarCampo_M();
                Cargar_Grafica_EvolucionMensual(Mes, ddlDepartamento, ddlArea, ddlSubArea, ddlSucursal, agrupacion, tipoM, tituloM);
            });
            $("#cmbTipo_General_M").change(function () {
                ValidarCampo_M();
                fn_CargarCombos_EvolucionMensual(CTipoM,Mes, ddlDepartamento, ddlArea, ddlSubArea, ddlSucursal);
            });
            $("#cmbMes_General").change(function () {
                ValidarCampo_M();
                fn_CargarCombos_EvolucionMensual(CTipoM,Mes, ddlDepartamento, ddlArea, ddlSubArea, ddlSucursal);
            });
            $("#cmbDepartamento_general_M").change(function () {
                ValidarCampo_M();
                fn_CargarCombos_EvolucionMensual(CTipoM,Mes, ddlDepartamento, ddlArea, ddlSubArea, ddlSucursal);
            });

            $("#cmbArea_general_M").change(function () {
                ValidarCampo_M();
                fn_CargarCombos_EvolucionMensual(CTipoM,Mes, ddlDepartamento, ddlArea, ddlSubArea, ddlSucursal);
            });

            $("#cmbSubArea_general_M").change(function () {
                ValidarCampo_M();
                fn_CargarCombos_EvolucionMensual(CTipoM,Mes, ddlDepartamento, ddlArea, ddlSubArea, ddlSucursal);
            });

            $("#cmbSucursal_General_M").change(function () {
                ValidarCampo_M();
                fn_CargarCombos_EvolucionMensual(CTipoM,Mes, ddlDepartamento, ddlArea, ddlSubArea, ddlSucursal);
            });

            $("#Personal_AusentismoYDescanso_Evolucion_Mes").click(function () {

                ValidarCampo_M();
                Cargar_Grafica_EvolucionMensual(Mes, ddlDepartamento, ddlArea, ddlSubArea, ddlSucursal, agrupacion, tipoM, tituloM);
            });
        });


        function fn_CargarCombos_EvolucionMensual(CTipoM,Mes, ddlDepartamento, ddlArea, ddlSubArea, ddlSucursal) {
            if (ddlDepartamento == 0 || ddlDepartamento == "") fn_General_Cargar_FiltroDepartamento_M(Mes, ddlArea, ddlSubArea, ddlSucursal);
            if (ddlArea == 0 || ddlArea == "") fn_General_Cargar_FiltroArea_M(Mes, ddlDepartamento, ddlSubArea, ddlSucursal);
            if (ddlSubArea == 0 || ddlSubArea == "") fn_General_Cargar_FiltroSubArea_M(Mes, ddlDepartamento, ddlArea, ddlSucursal);
            if (ddlSucursal == 0 || ddlSucursal == "") fn_General_Cargar_FiltroSucursal_M(Mes, ddlDepartamento, ddlArea, ddlSubArea);
            if (Mes == 0 || Mes == "") fn_General_Cargar_FiltroMes_M(ddlDepartamento, ddlArea, ddlSubArea, ddlSucursal);
            if (CTipoM == 0 || CTipoM == "") fn_General_Cargar_FiltroTipo_M();
        }

        function ValidarCampo_M() {
            ddlDepartamento = $("#cmbDepartamento_general_M").val(); if (ddlDepartamento != 0) { ddlDepartamento = $("#cmbDepartamento_general_M option:selected").text(); } else { ddlDepartamento = ""; }
            ddlArea = $("#cmbArea_general_M").val(); if (ddlArea != 0) { ddlArea = $("#cmbArea_general_M option:selected").text(); } else { ddlArea = ""; }
            ddlSubArea = $("#cmbSubArea_general_M").val(); if (ddlSubArea != 0) { ddlSubArea = $("#cmbSubArea_general_M option:selected").text(); } else { ddlSubArea = ""; }
            ddlSucursal = $("#cmbSucursal_General_M").val(); if (ddlSucursal != 0) { ddlSucursal = $("#cmbSucursal_General_M option:selected").text(); } else { ddlSucursal = ""; }
            Mes = $("#cmbMes_General").val(); if (Mes != 0) { Mes = $("#cmbMes_General option:selected").text(); } else { Mes = ""; }
            CTipoM = $("#cmbTipo_General_M").val(); if (CTipoM != 0) { CTipoM = $("#cmbTipo_General_M option:selected").text(); } else { CTipoM = ""; }
            if ($("#opc_trimestre").prop('checked')) { agrupacion = "3"; }
            if ($("#opc_bimestre").prop('checked')) { agrupacion = "2"; }
            if ($("#opc_bimestre").prop('checked') == false && $("#opc_trimestre").prop('checked') == false) { agrupacion = "0"; }
            if (CTipoM === "Descansos Medicos") {
                tipoM = "1";
            } else {
                tipoM = "0";
            }
        }

        function Cambiarestado_M() {
            if (ddlDepartamento != 0) $("#cmbDepartamento_general_M").val(0); document.getElementById("cmbDepartamento_general_M").disabled = false;
            if (ddlArea != 0) $("#cmbArea_general_M").val(0); document.getElementById("cmbArea_general_M").disabled = false;
            if (ddlSubArea != 0) $("#cmbSubArea_general_M").val(0); document.getElementById("cmbSubArea_general_M").disabled = false;
            if (ddlSucursal != 0) $("#cmbSucursal_General_M").val(0); document.getElementById("cmbSucursal_General_M").disabled = false;
            if (Mes != 0) $("#cmbMes_General").val(0); document.getElementById("cmbMes_General").disabled = false;
            $("#opc_trimestre").prop("checked", false);
            $("#opc_bimestre").prop("checked", false);
            ValidarCampo_M();
            fn_CargarCombos_EvolucionMensual(CTipoM, Mes, ddlDepartamento, ddlArea, ddlSubArea, ddlSucursal);
            tipoM = "0";
            Cargar_Grafica_EvolucionMensual("", "", "", "", "", "0", "0", tituloM);
        }
        function fn_General_Cargar_FiltroTipo_M() {
            var html = '';
            var sucess = function (response) {
                var obj = JSON.parse(response.d);
                var data_html = "";
                if (obj == null) {
                    data_html = "<option value='0'>Seleccione Opción</option>";
                    $("select[id$=cmbTipo_General_M]").html(data_html);
                }
                else {
                    data_html += "<option value='0'>Seleccione Opción</option>";
                    for (var i = 0; i < obj.length; i++) {
                        data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>";
                    }
                    $("select[id$=cmbTipo_General_M]").html(data_html);
                }
            };
            var error = function (xhr, ajaxOptions, thrownError) {
            };
            fn_LlamadoMetodo("FaltasDescansosMedico.aspx/Personal_AusentismoyDescanso_Tipo_CS", JSON.stringify({}), sucess, error);
        }
        function fn_General_Cargar_FiltroMes_M(ddlDepartamento, ddlArea, ddlSubArea, ddlSucursal) {
            var html = '';
            var sucess = function (response) {
                var obj = JSON.parse(response.d);
                var data_html = "";
                if (obj == null) {
                    data_html = "<option value='0'>Seleccione Opción</option>";
                    $("select[id$=cmbMes_General]").html(data_html);
                }
                else {
                    data_html += "<option value='0'>Seleccione Opción</option>";
                    for (var i = 0; i < obj.length; i++) {
                        data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>";
                    }
                    $("select[id$=cmbMes_General]").html(data_html);
                }
            };
            var error = function (xhr, ajaxOptions, thrownError) {
            };
            fn_LlamadoMetodo("FaltasDescansosMedico.aspx/Personal_AusentismoYDescanso_EvolucionGeneralMensual_ddlMes_CS", JSON.stringify({ departamento: ddlDepartamento, area: ddlArea, subArea: ddlSubArea, sucursal: ddlSucursal }), sucess, error);
        }
        function fn_General_Cargar_FiltroSucursal_M(Mes, ddlDepartamento, ddlArea, ddlSubArea) {
            var html = '';
            var sucess = function (response) {
                var obj = JSON.parse(response.d);
                var data_html = "";
                if (obj == null) {
                    data_html = "<option value='0'>Seleccione Opción</option>";
                    $("select[id$=cmbSucursal_General_M]").html(data_html);
                }
                else {
                    data_html += "<option value='0'>Seleccione Opción</option>";
                    for (var i = 0; i < obj.length; i++) {
                        data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>";
                    }
                    $("select[id$=cmbSucursal_General_M]").html(data_html);
                }
            };
            var error = function (xhr, ajaxOptions, thrownError) {
            };
            fn_LlamadoMetodo("FaltasDescansosMedico.aspx/Personal_AusentismoYDescanso_EvolucionGeneralMensual_ddlSucursal_CS", JSON.stringify({ mes: Mes, departamento: ddlDepartamento, area: ddlArea, subArea: ddlSubArea, sucursal: ddlSucursal }), sucess, error);
        }

        function fn_General_Cargar_FiltroDepartamento_M(Mes, ddlArea, ddlSubArea, ddlSucursal) {
            var html = '';
            var sucess = function (response) {
                var obj = JSON.parse(response.d);
                var data_html = "";
                if (obj == null) {
                    data_html = "<option value='0'>Seleccione Opción</option>";
                    $("select[id$=cmbDepartamento_general_M]").html(data_html);
                }
                else {
                    data_html += "<option value='0'>Seleccione Opción</option>";
                    for (var i = 0; i < obj.length; i++) {
                        data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>";
                    }
                    $("select[id$=cmbDepartamento_general_M]").html(data_html);
                }
            };
            var error = function (xhr, ajaxOptions, thrownError) {
            };
            fn_LlamadoMetodo("FaltasDescansosMedico.aspx/Personal_AusentismoYDescanso_EvolucionGeneralMensual_ddlDepartamento_CS", JSON.stringify({ mes: Mes, area: ddlArea, subArea: ddlSubArea, sucursal: ddlSucursal }), sucess, error);
        }


        function fn_General_Cargar_FiltroArea_M(Mes, ddlDepartamento, ddlSubArea, ddlSucursal) {
            var html = '';
            var sucess = function (response) {
                var obj = JSON.parse(response.d);
                var data_html = "";
                if (obj == null) {
                    data_html = "<option value='0'>Seleccione Opción</option>";
                    $("select[id$=cmbArea_general_M]").html(data_html);
                }
                else {
                    data_html += "<option value='0'>Seleccione Opción</option>";
                    for (var i = 0; i < obj.length; i++) {
                        data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>";
                    }
                    $("select[id$=cmbArea_general_M]").html(data_html);
                }
            };
            var error = function (xhr, ajaxOptions, thrownError) {
            };
            fn_LlamadoMetodo("FaltasDescansosMedico.aspx/Personal_AusentismoYDescanso_EvolucionGeneralMensual_ddlArea_CS", JSON.stringify({ mes: Mes, departamento: ddlDepartamento, subArea: ddlSubArea, sucursal: ddlSucursal }), sucess, error);
        }
        function fn_General_Cargar_FiltroSubArea_M(Mes, ddlDepartamento, ddlArea, ddlSucursal) {
            var html = '';
            var sucess = function (response) {
                var obj = JSON.parse(response.d);
                var data_html = "";
                if (obj == null) {
                    data_html = "<option value='0'>Seleccione Opción</option>";
                    $("select[id$=cmbSubArea_general_M]").html(data_html);
                }
                else {
                    data_html += "<option value='0'>Seleccione Opción</option>";
                    for (var i = 0; i < obj.length; i++) {
                        data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>";
                    }
                    $("select[id$=cmbSubArea_general_M]").html(data_html);
                }
            };
            var error = function (xhr, ajaxOptions, thrownError) {
            };
            fn_LlamadoMetodo("FaltasDescansosMedico.aspx/Personal_AusentismoYDescanso_EvolucionGeneralMensual_ddlSubArea_CS", JSON.stringify({ mes: Mes, departamento: ddlDepartamento, area: ddlArea, sucursal: ddlSucursal }), sucess, error);
        }


        function Cargar_Grafica_EvolucionMensual(Mes, ddlDepartamento, ddlArea, ddlSubArea, ddlSucursal, agrupacion, tipoM, tituloM) {
            var sucess = function (response) {
               
                if (tipoM === "1") {
                    tituloM = "EVOLUCIÓN GENERAL MENSUAL DE DESCANSOS MÉDICOS ";
                }
                else {
                    tituloM = "EVOLUCIÓN GENERAL MENSUAL DE FALTAS ";
                }
                json = response.d.Series
                LineChart_M(response.d.Fecha, response.d.Name, fn_ValidarANulos(json), tituloM);
                $('#AusentismoYDescanso_EvolucionMensual').show();

            };
            var error = function (xhr, ajaxOptions, thrownError) {
                fn_Mensaje_Alerta("Error", " Ocurrio un error mientras se cargaban los datos", "", "Alert");
            };
            fn_LlamadoMetodo("FaltasDescansosMedico.aspx/G_Personal_AusentismoYDescanso_EvolucionGeneralMensual_CS", JSON.stringify({ mes: Mes, departamento: ddlDepartamento, area: ddlArea, subArea: ddlSubArea, sucursal: ddlSucursal, agrupacionF: agrupacion, TipoG: tipoM }), sucess, error);
        }

        function LineChart_M(fecha, Name, data, tituloM) {
            Highcharts.chart('AusentismoYDescanso_EvolucionMensual', {
                chart: {

                    type: 'line'
                },
                title: {
                    text: tituloM
                },
                subtitle: {
                    text: 'MESES'
                },
                xAxis: {
                    categories: JSON.parse(fecha)
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: ' '
                    }
                },
                plotOptions: {
                    line: {
                        dataLabels: {
                            enabled: true
                        },
                        enableMouseTracking: true
                    }
                },
                series: JSON.parse(data)
            });
        }

        ///////   EVOLUCIÓN HISTÓRICA_EVOLUCIÓN POR AREA MENSUAL

        var EArea = ""; var ESubArea = ""; var ESucursal = ""; var EAnio = ""; var ETrimestre = ""; var EAnio = ""; var EMes = ""; var EAgrupacion = "0";
        var Etipo = "0"; var ECTipo = ""; var ETitulo = "";
        fn_Combos(ECTipo,EAnio, EMes, EArea, ESubArea, ESucursal);
        Cargar_Grafica_Chart("", "", "", "", "", "0","0", ETitulo);

        $(document).ready(function () {

            $("#opc_Etrimestre").change(function () {
                $("#opc_Ebimestre").prop("checked", false);
                Validar();
                Cargar_Grafica_Chart(EAnio, EMes, EArea, ESubArea, ESucursal, EAgrupacion, Etipo, ETitulo); 
            });
            $("#opc_Ebimestre").change(function () {
                $("#opc_Etrimestre").prop("checked", false);
                Validar();
                Cargar_Grafica_Chart(EAnio, EMes, EArea, ESubArea, ESucursal, EAgrupacion, Etipo, ETitulo);  
            });
            $("#cmbEMes_General").change(function () {
                Validar();
                fn_Combos(ECTipo, EAnio, EMes, EArea, ESubArea, ESucursal);
            });
            $("#cmbETipo_General").change(function () {
                Validar();
                fn_Combos(ECTipo, EAnio, EMes, EArea, ESubArea, ESucursal);
            });
            $("#cmbEAnio_General").change(function () {
                Validar();
                fn_Combos(ECTipo, EAnio, EMes, EArea, ESubArea, ESucursal);
            });

            $("#cmbEArea_general").change(function () {
                Validar();
                fn_Combos(ECTipo, EAnio, EMes, EArea, ESubArea, ESucursal);
            });

            $("#cmbESubArea_general").change(function () {
                Validar();
                fn_Combos(ECTipo, EAnio, EMes, EArea, ESubArea, ESucursal);
            });

            $("#cmbESucursal_General").change(function () {
                Validar();
                fn_Combos(ECTipo, EAnio, EMes, EArea, ESubArea, ESucursal);
            });

            $("#Personal_AusentismoYDescanso_EvaluacionArea").click(function () {

                Validar();
                Cargar_Grafica_Chart(EAnio, EMes, EArea, ESubArea, ESucursal, EAgrupacion, Etipo, ETitulo); 

            });
        });

        function fn_Combos(EAnio, EMes, EArea, ESubArea, ESucursal) {
            if (EArea == 0 || EArea == "") fn_FiltroEArea(EAnio, EMes, ESubArea, ESucursal);
            if (ESubArea == 0 || ESubArea == "") fn_EFiltroSubArea(EAnio, EMes, EArea, ESucursal);
            if (ESucursal == 0 || ESucursal == "") fn_EFiltroSucursal(EAnio, EMes, EArea, ESubArea);
            if (EAnio == 0 || EAnio == "") fn_EFiltroAnio(EMes, EArea, ESubArea, ESucursal);
            if (EMes == 0 || EMes == "") fn_EFiltroMes(EAnio, EArea, ESubArea, ESucursal);
            if (ECTipo == 0 || ECTipo == "") fn_General_Cargar_EFiltroTipo();
        }

        function Validar() {
            EArea = $("#cmbEArea_general").val(); if (EArea != 0) { EArea = $("#cmbEArea_general option:selected").text(); } else { EArea = ""; }
            ESubArea = $("#cmbESubArea_general").val(); if (ESubArea != 0) { ESubArea = $("#cmbESubArea_general option:selected").text(); } else { ESubArea = ""; }
            ESucursal = $("#cmbESucursal_General").val(); if (ESucursal != 0) { ESucursal = $("#cmbESucursal_General option:selected").text(); } else { ESucursal = ""; }
            EAnio = $("#cmbEAnio_General").val(); if (EAnio != 0) { EAnio = $("#cmbEAnio_General option:selected").text(); } else { EAnio = ""; }
            EMes = $("#cmbEMes_General").val(); if (EMes != 0) { EMes = $("#cmbEMes_General option:selected").text(); } else { EMes = ""; }
            ECTipo = $("#cmbETipo_General").val(); if (ECTipo != 0) { ECTipo = $("#cmbETipo_General option:selected").text(); } else { ECTipo = ""; }
            if ($("#opc_Etrimestre").prop('checked')) { EAgrupacion = "3"; }
            if ($("#opc_Ebimestre").prop('checked')) { EAgrupacion = "2"; }
            if ($("#opc_Ebimestre").prop('checked') == false && $("#opc_Etrimestre").prop('checked') == false) { EAgrupacion = "0"; }
            if (ECTipo === "Descansos Medicos") {
                Etipo = "1";
            } else {
                Etipo = "0";
            }
        }
        function Estado() {
            if (EArea != 0) $("#cmbEArea_general").val(0); document.getElementById("cmbEArea_general").disabled = false;
            if (ESubArea != 0) $("#cmbESubArea_general").val(0); document.getElementById("cmbESubArea_general").disabled = false;
            if (ESucursal != 0) $("#cmbESucursal_General").val(0); document.getElementById("cmbESucursal_General").disabled = false;
            if (EAnio != 0) $("#cmbEAnio_General").val(0); document.getElementById("cmbEAnio_General").disabled = false;
            if (EMes != 0) $("#cmbEMes_General").val(0); document.getElementById("cmbEMes_General").disabled = false;
            if (ECTipo != 0) $("#cmbETipo_General").val(0); document.getElementById("cmbETipo_General").disabled = false;
            $("#opc_Etrimestre").prop("checked", false);
            $("#opc_Ebimestre").prop("checked", false);
            Validar();
            fn_Combos(EAnio, EMes, EArea, ESubArea, ESucursal)
            Etipo = "0";
            Cargar_Grafica_Chart(EAnio, EMes, EArea, ESubArea, ESucursal, EAgrupacion, Etipo, ETitulo);

        }
        function fn_General_Cargar_EFiltroTipo(){
            var html = '';
            var sucess = function (response) {
                var obj = JSON.parse(response.d);
                var data_html = "";
                if (obj == null) {
                    data_html = "<option value='0'>Seleccione Opción</option>";
                    $("select[id$=cmbETipo_General]").html(data_html);
                }
                else {
                    data_html += "<option value='0'>Seleccione Opción</option>";
                    for (var i = 0; i < obj.length; i++) {
                        data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>";
                    }
                    $("select[id$=cmbETipo_General]").html(data_html);
                }
            };
            var error = function (xhr, ajaxOptions, thrownError) {
            };
            fn_LlamadoMetodo("FaltasDescansosMedico.aspx/Personal_AusentismoyDescanso_Tipo_CS", JSON.stringify({}), sucess, error);
        }
        function fn_FiltroEArea(EAnio, EMes, ESubArea, ESucursal) {
            var html = '';
            var sucess = function (response) {
                var obj = JSON.parse(response.d);
                var data_html = "";
                if (obj == null) {
                    data_html = "<option value='0'>Seleccione Opción</option>";
                    $("select[id$=cmbEArea_general]").html(data_html);
                }
                else {
                    data_html += "<option value='0'>Seleccione Opción</option>";
                    for (var i = 0; i < obj.length; i++) {
                        data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>";
                    }
                    $("select[id$=cmbEArea_general]").html(data_html);


                }
            };
            var error = function (xhr, ajaxOptions, thrownError) {
            };
            fn_LlamadoMetodo("FaltasDescansosMedico.aspx/Personal_AusentismoYDescanso_EvolucionAreaMensual_ddlArea_CS", JSON.stringify({ anio: EAnio, mes: EMes, subArea: ESubArea, sucursal: ESucursal }), sucess, error);
        }

        function fn_EFiltroSubArea(EAnio, EMes, EArea, ESucursal) {
            var html = '';
            var sucess = function (response) {
                var obj = JSON.parse(response.d);
                var data_html = "";
                if (obj == null) {
                    data_html = "<option value='0'>Seleccione Opción</option>";
                    $("select[id$=cmbESubArea_general]").html(data_html);
                }
                else {
                    data_html += "<option value='0'>Seleccione Opción</option>";
                    for (var i = 0; i < obj.length; i++) {
                        data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>";
                    }
                    $("select[id$=cmbESubArea_general]").html(data_html);
                }
            };
            var error = function (xhr, ajaxOptions, thrownError) {
            };
            fn_LlamadoMetodo("FaltasDescansosMedico.aspx/Personal_AusentismoYDescanso_EvolucionAreaMensual_ddlSubArea_CS", JSON.stringify({ anio: EAnio, mes: EMes, area: EArea, sucursal: ESucursal }), sucess, error);
        }

        function fn_EFiltroSucursal(EAnio, EMes, EArea, ESubArea) {
            var html = '';
            var sucess = function (response) {
                var obj = JSON.parse(response.d);
                var data_html = "";
                if (obj == null) {
                    data_html = "<option value='0'>Seleccione Opción</option>";
                    $("select[id$=cmbESucursal_General]").html(data_html);
                }
                else {
                    data_html += "<option value='0'>Seleccione Opción</option>";
                    for (var i = 0; i < obj.length; i++) {
                        data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>";
                    }
                    $("select[id$=cmbESucursal_General]").html(data_html);
                }
            };
            var error = function (xhr, ajaxOptions, thrownError) {
            };
            fn_LlamadoMetodo("FaltasDescansosMedico.aspx/Personal_AusentismoYDescanso_EvolucionAreaMensual_ddlSucursal_CS", JSON.stringify({ anio: EAnio, mes: EMes, area: EArea, subArea: ESubArea }), sucess, error);
        }


        function fn_EFiltroAnio(EMes, EArea, ESubArea, ESucursal) {
            var html = '';
            var sucess = function (response) {
                var obj = JSON.parse(response.d);
                var data_html = "";
                if (obj == null) {
                    data_html = "<option value='0'>Seleccione Opción</option>";
                    $("select[id$=cmbEAnio_General]").html(data_html);
                }
                else {
                    data_html += "<option value='0'>Seleccione Opción</option>";
                    for (var i = 0; i < obj.length; i++) {
                        data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>";
                    }
                    $("select[id$=cmbEAnio_General]").html(data_html);
                }
            };
            var error = function (xhr, ajaxOptions, thrownError) {
            };
            fn_LlamadoMetodo("FaltasDescansosMedico.aspx/Personal_AusentismoYDescanso_EvolucionAreaMensual_ddlAnio_CS", JSON.stringify({ mes: EMes, area: EArea, subArea: ESubArea, sucursal: ESucursal }), sucess, error);
        }

        function fn_EFiltroMes(EAnio, EArea, ESubArea, ESucursal) {
            var html = '';
            var sucess = function (response) {
                var obj = JSON.parse(response.d);
                var data_html = "";
                if (obj == null) {
                    data_html = "<option value='0'>Seleccione Opción</option>";
                    $("select[id$=cmbEMes_General]").html(data_html);
                }
                else {
                    data_html += "<option value='0'>Seleccione Opción</option>";
                    for (var i = 0; i < obj.length; i++) {
                        data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>";
                    }
                    $("select[id$=cmbEMes_General]").html(data_html);
                }
            };
            var error = function (xhr, ajaxOptions, thrownError) {
            };
            fn_LlamadoMetodo("FaltasDescansosMedico.aspx/Personal_AusentismoYDescanso_EvolucionAreaMensual_ddlMes_CS", JSON.stringify({ anio: EAnio, area: EArea, subArea: ESubArea, sucursal: ESucursal }), sucess, error);
        }
        function Cargar_Grafica_Chart(EAnio, EMes, EArea, ESubArea, ESucursal, EAgrupacion, Etipo, ETitulo) {
            var sucess = function (response) {
                //fn_Mensaje_Alerta("Confirmacion", "Se esta ejecutando los datos", "", "AlertaMensaje");
                if (Etipo === "1") {
                    ETitulo = "EVOLUCIÓN POR ÁREA MENSUAL DE DESCANSOS MÉDICOS ";
                }
                else {
                    ETitulo = "EVOLUCIÓN POR ÁREA MENSUAL DE FALTAS ";
                }
                json = response.d.Series
                GraficaChart(response.d.Fecha, response.d.Name, fn_ValidarANulos(json), ETitulo);
                $('#Evolucion_Area_Mensual').show();
                ValidarCampo();
            };
            var error = function (xhr, ajaxOptions, thrownError) {
                fn_Mensaje_Alerta("Error", " Ocurrio un error mientras se cargaban los datos", "", "AlertaMensaje");
            };
            fn_LlamadoMetodo("FaltasDescansosMedico.aspx/G_Personal_AusentismoYDescanso_EvolucionAreaMensual_CS", JSON.stringify({ anio: EAnio, mes: EMes, area: EArea, subArea: ESubArea, sucursal: ESucursal, agrupacionA: EAgrupacion, TipoG: Etipo }), sucess, error);
        }

        function GraficaChart(fecha, Name, data, ETitulo) {
            Highcharts.chart('Evolucion_Area_Mensual', {
                chart: {

                    type: 'line'

                },
                title: {
                    text: ETitulo
                },
                subtitle: {
                    text: 'DEPARTAMENTO'
                },
                xAxis: {
                    categories: JSON.parse(fecha)
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: ' '
                    }
                },
                plotOptions: {
                    line: {
                        dataLabels: {
                            enabled: true
                        },
                        enableMouseTracking: true
                    }
                },
                series: JSON.parse(data)
            });
        }

       
    </script>

</asp:Content>
