﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Intranet.Master" AutoEventWireup="true" CodeBehind="CantidadColaboradores.aspx.cs" Inherits="WebApplication1.Modulo.Area.Personal.CantidadColaboradores" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContenedorDashboard" runat="server">

     <h3 class="panel-body" style="color: #283593; margin-top: 0px;">CANTIDAD DE COLABORADORES</h3>
  
     <div class="text-left Left aligned text" style="margin-left: 18px;">
        <p><strong>Objetivo:</strong> El objetivo del indicador es mostrar en tiempo real la cantidad de colaboradores con la que cuenta la empresa y a la vez sus distintos departamentos y áreas.</p>
    </div>
  <div class="row">
        <div class="col-lg-12">
         <div id="Mensaje">
            </div>
                <div class="panel panel-headline">
                   <div class="panel-body">
                      <div class="form-inline">

                      <div class="form-group col-lg-2 col-md-2 col-sm-3">
                        <label for="formGroupExampleInput">Sucursal:</label>
                            <asp:DropDownList ID="cmbSucursalG_GeneralG" ClientIDMode="Static" class="form-control" runat="server" Style="width: 100%;"></asp:DropDownList>
                        </div>

                        <div class="form-group col-lg-2 col-md-2 col-sm-3">
                        <label for="formGroupExampleInput">Area:</label>
                            <asp:DropDownList ID="cmbAreaG_generalG" ClientIDMode="Static" class="form-control" runat="server" Style="width: 100%;"></asp:DropDownList>
                        </div>

                        <div class="form-group col-lg-2 col-md-2 col-sm-3">
                            <label for="formGroupExampleInput">SubArea:</label>
                            <asp:DropDownList ID="cmbSubAreaG_generalG" ClientIDMode="Static" class="form-control" runat="server" Style="width: 100%;"></asp:DropDownList>
                        </div>

                          <div class="form-group col-md-1 col-md-1 col-md-1 col-md-1">
                            <br />
                            <label for="formGroupExampleInput">Limpiar:</label>
                              <br />
                             <span class="lnr lnr-magic-wand-limpiar"  onclick="CambiarestadoG()"></span>
                        </div>

                        <div class="form-group col-md-2 col-sm-6 col-sm-12">
                        <br />
                            <button id="Personal_CantidadColabG" type="button" class="btn btn-primary btn-sm">GRAFICAR</button>
                        </div>
                    </div>
               </div>
            </div>

        <div class="col-md-3">
            <div class="metric">
                <div id="cantidad">
                </div>
            </div>
        </div>
       <div class="col-md-9">
          <div class="panel-body">
              <div id="CantidadColaboradores_Cantidad" style="min-width: 310px; height: 400px; margin: 0 auto; display:none"></div>  
         </div>   
           <div class="col-md-9">
               <div class="text-left Left aligned text">
                <p><strong>Interpretacion:</strong> </p>
            </div>
                </div>
       </div>
   </div>
 </div>
                
  <div class="col-lg-6">
       <div class="row">
           <div id="Alerta">
         </div>
            <div class="col-lg-12">
             <div class="panel panel-headline">
                <div class="panel-body">
                    <div class="form-inline">

                        <div class="form-group col-lg-3 col-md-3 col-sm-3">
                        <label for="formGroupExampleInput">Sucursal:</label>
                            <asp:DropDownList ID="cmbSucursal_General" ClientIDMode="Static" class="form-control" runat="server" Style="width: 100%;"></asp:DropDownList>
                        </div>

                          <div class="form-group col-lg-3 col-md-3 col-sm-3">
                            <label for="formGroupExampleInput">Departamento:</label>
                            <asp:DropDownList ID="cmbDepartamento_general" ClientIDMode="Static" class="form-control" runat="server" Style="width: 100%;"></asp:DropDownList>
                        </div>

                        <div class="form-group col-lg-3 col-md-3 col-sm-3">
                        <label for="formGroupExampleInput">Area:</label>
                            <asp:DropDownList ID="cmbArea_general" ClientIDMode="Static" class="form-control" runat="server" Style="width: 100%;"></asp:DropDownList>
                        </div>

                          <div class="form-group col-lg-3 col-md-3 col-sm-3">
                            <label for="formGroupExampleInput">SubArea:</label>
                            <asp:DropDownList ID="cmbSubArea_general" ClientIDMode="Static" class="form-control" runat="server" Style="width: 100%;"></asp:DropDownList>
                        </div>
                           <div class="form-group col-lg-3 col-md-3 col-sm-3">
                            <label for="formGroupExampleInput">Mes:</label>
                            <asp:DropDownList ID="cmbMes_General" ClientIDMode="Static" class="form-control" runat="server" Style="width: 100%;"></asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-3 col-md-3 col-sm-3">
                            <br />
                            <label for="formGroupExampleInput">Agrupación:</label>
                            <label class="fancy-checkbox">
                                <input type="checkbox" id="opc_trimestre">
                                <span>Trimestre</span>
                            </label>
                            <label class="fancy-checkbox">
                                <input type="checkbox" id="opc_bimestre">
                                <span>Bimestre</span>
                            </label><br />
                    </div>
                      <div class="form-group col-lg-3 col-md-3 col-sm-3">
                            <br />
                            <label for="formGroupExampleInput">Limpiar:</label>
                          <br />
                             <span class="lnr lnr-magic-wand-limpiar"  onclick="Cambiarestado()"></span>
                        </div>

                         
                        <div class="form-group col-lg-3 col-md-3 col-sm-3">
                        <br />
                            <button id="Personal_CantidadColaboradoresE" type="button" class="btn btn-primary btn-sm">GRAFICAR</button>
                    </div>
              
                        </div>
                    </div>
               </div>
            </div>
       </div>
 
         <div class="col-lg-15">
          <div class="row panel-body">
              <div id="Colaboradores" style="min-width: 310px; height: 400px; margin: 0 auto; display:none"></div>  
               </div>   
          </div>
               <div class="text-left Left aligned text">
                <p><strong>Interpretacion:</strong>  Se desea que la cantidad de colaboradores sea la menor.</p>
            </div>
 </div>



       <div class="col-lg-6">
              <div id="AlertaMensaje">
         </div>
          <div class="row">
            <div class="col-lg-12">

            <div class="panel panel-headline">
                <div class="panel-body">
                    <div class="form-inline">

                       <div class="form-group col-lg-3 col-md-3 col-sm-3">
                        <label for="formGroupExampleInput">Sucursal:</label>
                            <asp:DropDownList ID="cmbESucursal_General" ClientIDMode="Static" class="form-control" runat="server" Style="width: 100%;"></asp:DropDownList>
                        </div>

                      <div class="form-group col-lg-3 col-md-3 col-sm-3">
                        <label for="formGroupExampleInput">Area:</label>
                            <asp:DropDownList ID="cmbEArea_general" ClientIDMode="Static" class="form-control" runat="server" Style="width: 100%;"></asp:DropDownList>
                        </div>

                        <div class="form-group col-lg-3 col-md-3 col-sm-3">
                            <label for="formGroupExampleInput">SubArea:</label>
                            <asp:DropDownList ID="cmbESubArea_general" ClientIDMode="Static" class="form-control" runat="server" Style="width: 100%;"></asp:DropDownList>
                        </div>
                          <div class="form-group col-lg-3 col-md-3 col-sm-3">
                            <label for="formGroupExampleInput">Año:</label>
                            <asp:DropDownList ID="cmbEAnio_General" ClientIDMode="Static" class="form-control" runat="server" Style="width: 100%;"></asp:DropDownList>
                        </div>
                           <div class="form-group col-lg-3 col-md-3 col-sm-3">
                            <label for="formGroupExampleInput">Mes:</label>
                            <asp:DropDownList ID="cmbEMes_General" ClientIDMode="Static" class="form-control" runat="server" Style="width: 100%;"></asp:DropDownList>
                        </div>
                       </div>
                        <div class="form-group col-lg-3 col-md-3 col-sm-3">
                            <br />
                            <label for="formGroupExampleInput">Agrupación:</label>
                        <label class="fancy-checkbox">
                                <input type="checkbox" id="opc_Etrimestre">
                                <span>Trimestre</span>
                            </label>
                        <label class="fancy-checkbox">
                                <input type="checkbox" id="opc_Ebimestre">
                                <span>Bimestre</span>
                            </label>
                      </div>
                           <div class="form-group col-lg-3 col-md-3 col-sm-3">
                               <br />
                            <label for="formGroupExampleInput">Limpiar:</label>  
                               <br />
                              <span class="lnr lnr-magic-wand-limpiar"  onclick="Estado()"></span>
                       </div>
                    
                           <div class="form-group col-lg-3 col-md-3 col-sm-3">
                        <br />
                            <button id="Personal_CantidadColaboradores_EvaluacionArea" type="button" class="btn btn-primary btn-sm">GRAFICAR</button>
                        </div>
                    </div>
                    </div>
               </div>
            </div>
        
         <div class="col-lg-15">
         <div class="row panel-body">
              <div id="Evolucion_Mensual" style="min-width: 310px; height: 400px; margin: 0 auto; display:none"></div>  
               </div>   
           </div>   
         <div class="text-left Left aligned text">
                <p><strong>Interpretacion:</strong>  Se desea que la cantidad de colaboradores sea la menor.</p>
            </div>
    
   </div>
    
    <script>
        //CANTIDAD POR DEPARTAMENTO
        var AreaG = ""; var SubAreaG = ""; var SucursalG = "";
        fn_CargarCombosG(AreaG, SubAreaG, SucursalG);
        fn_General_CantidadColaboradores_cantidadGeneral();
        Cargar_GraficaG("", "", "");
        $(document).ready(function () {

            $("#cmbAreaG_generalG").change(function () {
                ValidarCampoG();
                fn_CargarCombosG(AreaG, SubAreaG, SucursalG);
            });

            $("#cmbSubAreaG_generalG").change(function () {
                ValidarCampoG();
                fn_CargarCombosG(AreaG, SubAreaG, SucursalG);
            });

            $("#cmbSucursalG_GeneralG").change(function () {
                ValidarCampoG();
                fn_CargarCombosG(AreaG, SubAreaG, SucursalG);
            });

            $("#Personal_CantidadColabG").click(function () {

                ValidarCampoG();
                fn_General_CantidadColaboradores_cantidadGeneral();
                Cargar_GraficaG(AreaG, SubAreaG, SucursalG);
            });
        });

        function fn_CargarCombosG(AreaG, SubAreaG, SucursalG) {
            if (AreaG == 0 || AreaG == "") fn_General_Cargar_FiltroAreaG(SucursalG, SubAreaG);
            if (SubAreaG == 0 || SubAreaG == "") fn_General_Cargar_FiltroSubAreaG(SucursalG, AreaG);
            if (SucursalG == 0 || SucursalG == "") fn_General_Cargar_FiltroSucursalG(AreaG, SubAreaG);
        }
        function ValidarCampoG() {
            AreaG = $("#cmbAreaG_generalG").val(); if (AreaG != 0) { AreaG = $("#cmbAreaG_generalG option:selected").text(); } else { AreaG = ""; }
            SubAreaG = $("#cmbSubAreaG_generalG").val(); if (SubAreaG != 0) { SubAreaG = $("#cmbSubAreaG_generalG option:selected").text(); } else { SubAreaG = ""; }
            SucursalG = $("#cmbSucursalG_GeneralG").val(); if (SucursalG != 0) { SucursalG = $("#cmbSucursalG_GeneralG option:selected").text(); } else { SucursalG = ""; }
        }
        function CambiarestadoG() {
            if (AreaG != 0) $("#cmbAreaG_generalG").val(0); document.getElementById("cmbAreaG_generalG").disabled = false;
            if (SubAreaG != 0) $("#cmbSubAreaG_generalG").val(0); document.getElementById("cmbSubAreaG_generalG").disabled = false;
            if (SucursalG != 0) $("#cmbSucursalG_GeneralG").val(0); document.getElementById("cmbSucursalG_GeneralG").disabled = false;
            ValidarCampoG();
            fn_CargarCombosG(AreaG, SubAreaG, SucursalG);
            Cargar_GraficaG("", "", "");
        }
        function fn_General_Cargar_FiltroAreaG(SucursalG, SubAreaG) {
            var html = '';
            var sucess = function (response) {
                var obj = JSON.parse(response.d);
                var data_html = "";
                if (obj == null) {
                    data_html = "<option value='0'>Seleccione Opción</option>";
                    $("select[id$=cmbAreaG_generalG]").html(data_html);
                }
                else {
                    data_html += "<option value='0'>Seleccione Opción</option>";
                    for (var i = 0; i < obj.length; i++) {
                        data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>";
                    }
                    $("select[id$=cmbAreaG_generalG]").html(data_html);
                }
            };
            var error = function (xhr, ajaxOptions, thrownError) {
            };
            fn_LlamadoMetodo("CantidadColaboradores.aspx/Personal_CantidadColaboradores_CantidadArea_ddlArea_CS", JSON.stringify({ sucursal: SucursalG, subArea: SubAreaG }), sucess, error);
        }
        function fn_General_Cargar_FiltroSubAreaG(SucursalG, AreaG) {
            var html = '';
            var sucess = function (response) {
                var obj = JSON.parse(response.d);
                var data_html = "";
                if (obj == null) {
                    data_html = "<option value='0'>Seleccione Opción</option>";
                    $("select[id$=cmbSubAreaG_generalG]").html(data_html);
                }
                else {
                    data_html += "<option value='0'>Seleccione Opción</option>";
                    for (var i = 0; i < obj.length; i++) {
                        data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>";
                    }
                    $("select[id$=cmbSubAreaG_generalG]").html(data_html);
                }
            };
            var error = function (xhr, ajaxOptions, thrownError) {
            };
            fn_LlamadoMetodo("CantidadColaboradores.aspx/Personal_CantidadColaboradores_CantidadArea_ddlSubArea_CS", JSON.stringify({ sucursal: SucursalG, area: AreaG }), sucess, error);
        }

        function fn_General_Cargar_FiltroSucursalG(AreaG, SubAreaG) {
            var html = '';
            var sucess = function (response) {
                var obj = JSON.parse(response.d);
                var data_html = "";
                if (obj == null) {
                    data_html = "<option value='0'>Seleccione Opción</option>";
                    $("select[id$=cmbSucursalG_GeneralG]").html(data_html);
                }
                else {
                    data_html += "<option value='0'>Seleccione Opción</option>";
                    for (var i = 0; i < obj.length; i++) {
                        data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>";
                    }
                    $("select[id$=cmbSucursalG_GeneralG]").html(data_html);
                }
            };
            var error = function (xhr, ajaxOptions, thrownError) {
            };
            fn_LlamadoMetodo("CantidadColaboradores.aspx/Personal_CantidadColaboradores_CantidadArea_ddlSucursal_CS", JSON.stringify({ area: AreaG, subArea: SubAreaG }), sucess, error);
        }

        function fn_General_CantidadColaboradores_cantidadGeneral() {
            var html = '';
            var sucess = function (response) {
                var obj = JSON.parse(response.d);
                var data_html = "";
                if (obj == null) {
                    data_html =
                        "<span class=\"icon\"><i class=\"fa fa - bar - chart\"></i></span>" +
                        " <p>" +
                        "<span class=\"number\">1,252</span>" +
                        "<span class=\"title\">Cantidad General de Colaboradores </span>" +
                        "</p>";
                    $("div[id$=cantidad]").html(data_html);
                }
                else {
                    for (var i = 0; i < obj.length; i++) {
                        CantidadColaboradores_Cantidad
                        data_html =
                            "<span class=\"icon\"><i class=\"fa fa-bar-chart\"></i></span><p>" +
                            "<span class=\"number\" id=\"CantidadColaboradores_cantidadG\">" + obj[i] +
                            "</span><span class=\"title\">Cantidad General de Colaboradores </span></p>";
                    }
                    $("div[id$=cantidad]").html(data_html);
                }
            };
            var error = function (xhr, ajaxOptions, thrownError) {
            };
            fn_LlamadoMetodo("CantidadColaboradores.aspx/G_Personal_CantidadColaboradores_cantidadGeneral_CS", JSON.stringify({}), sucess, error);
        }

        function Cargar_GraficaG(AreaG, SubAreaG, SucursalG) {
            var sucess = function (response) {
                // fn_Mensaje_Alerta("Confirmacion", "Se esta ejecutando los datos", "", "Mensaje");
                BarrasChart(response.d.Marcas, response.d.Data);
                $('#CantidadColaboradores_Cantidad').show();
            };
            var error = function (xhr, ajaxOptions, thrownError) {
                fn_Mensaje_Alerta("Error", " Ocurrio un error mientras se cargaban los datos", "", "Mensaje");
            };
            fn_LlamadoMetodo("CantidadColaboradores.aspx/G_Personal_CantidadColaboradores_CantidadArea_CS", JSON.stringify({ area: AreaG, subArea: SubAreaG, sucursal: SucursalG }), sucess, error);
        }
        function BarrasChart(Marcas, Data) {
            var chart = Highcharts.chart('CantidadColaboradores_Cantidad', {
                title: {
                    text: 'CANTIDAD DE COLABORADORES  SEGÚN DEPARTAMENTO',

                },
                xAxis: {
                    categories: Marcas
                },

                yAxis: {
                    title: {
                        text: ''
                    },
                },
                chart: {
                    inverted: true,
                    polar: false,

                },
                plotOptions: {
                    series: {
                        stacking: 'normal',
                        dataLabels: {
                            enabled: true,
                            style: {
                                "font-family": "'Open Sans', sans-serif",
                                "-webkit-text-stroke": "2px black",
                                "color": "#ffff",
                                "fontSize": "12px",
                            },
                            format: '{point.y} '
                        }
                    }
                },
                series: [{
                    name: 'Cantidad',
                    type: 'column',
                    colorByPoint: true,
                    data: Data,
                    showInLegend: false
                }]
            });
        }


        /////// EVOLUCIÓN HISTÓRICA_EVOLUCIÓN GENERAL MENSUAL
        var Departamento = ""; var Area = ""; var SubArea = ""; var Sucursal = ""; var Mes = ""; var agrupacion = "0";
        var temp = false;
        fn_CargarCombos(Mes, Departamento, Area, SubArea, Sucursal);
        Cargar_Grafica("", "", "", "", "", "0");
        $(document).ready(function () {

            $("#opc_trimestre").change(function () {
                $("#opc_bimestre").prop("checked", false);
                ValidarCampo();
                if (agrupacion === "3") {
                    $("#cmbMes_General").prop('disabled', true);
                    fn_Mensaje_Alerta("alerta", " No podra seleccionar Mes", "", "Alerta");
                }
                else {
                    $("#cmbMes_General").prop('disabled', false);

                }
                Cargar_Grafica(Mes, Departamento, Area, SubArea, Sucursal, agrupacion);
            });

            $("#opc_bimestre").change(function () {
                $("#opc_trimestre").prop("checked", false);
                ValidarCampo();
                if (agrupacion==="2") {
                    $("#cmbMes_General").prop('disabled', true);
                    fn_Mensaje_Alerta("alerta", " No podra seleccionar Mes", "", "Alerta");
                }
                else {
                    $("#cmbMes_General").prop('disabled', false);

                }
                Cargar_Grafica(Mes, Departamento, Area, SubArea, Sucursal, agrupacion);
            });
            $("#cmbDepartamento_general").change(function () {
                ValidarCampo();
                fn_CargarCombos(Mes, Departamento, Area, SubArea, Sucursal);
            });

            $("#cmbArea_general").change(function () {
                ValidarCampo();
                fn_CargarCombos(Mes, Departamento, Area, SubArea, Sucursal);
            });

            $("#cmbSubArea_general").change(function () {
                ValidarCampo();
                fn_CargarCombos(Mes, Departamento, Area, SubArea, Sucursal);
            });

            $("#cmbSucursal_General").change(function () {
                ValidarCampo();
                fn_CargarCombos(Mes, Departamento, Area, SubArea, Sucursal);
            });
       
            $("#cmbMes_General").change(function () {
                ValidarCampo();
                $("#opc_trimestre").prop("checked", false);
                $("#opc_bimestre").prop("checked", false); 
                if (($("#cmbMes_General").val()) == 0) {
                    $("#opc_trimestre").prop('disabled', false);
                    $("#opc_bimestre").prop('disabled', false);
                    
                }
                else {

                    $("#opc_trimestre").prop('disabled', true);
                    $("#opc_bimestre").prop('disabled', true);
                    fn_Mensaje_Alerta("alerta", " No podra seleccionar Trimestre y Bimestre", "", "Alerta");
                   
                }
                fn_CargarCombos(Mes, Departamento, Area, SubArea, Sucursal);
                });
            
            $("#Personal_CantidadColaboradoresE").click(function () {

                ValidarCampo();
                Cargar_Grafica(Mes, Departamento, Area, SubArea, Sucursal, agrupacion);
            });
        });
        function fn_CargarCombos(Mes, Departamento, Area, SubArea, Sucursal) {
            if (Departamento == 0 || Departamento == "") fn_General_Cargar_FiltroDepartamento(Mes, Area, SubArea, Sucursal);
            if (Area == 0 || Area == "") fn_General_Cargar_FiltroArea(Mes, Departamento, SubArea, Sucursal);
            if (SubArea == 0 || SubArea == "") fn_General_Cargar_FiltroSubArea(Mes, Departamento, Area, Sucursal);
            if (Sucursal == 0 || Sucursal == "") fn_General_Cargar_FiltroSucursal(Mes, Departamento, Area, SubArea);
             if (Mes == 0 || Mes == "") fn_General_Cargar_FiltroMes(Departamento, Area, SubArea, Sucursal);
        }
        function fn_General_Cargar_FiltroMes(Departamento, Area, SubArea, Sucursal) {
            var html = '';
            var sucess = function (response) {
                var obj = JSON.parse(response.d);
                var data_html = "";
                if (obj == null) {
                    data_html = "<option value='0'>Seleccione Opción</option>";
                    $("select[id$=cmbMes_General]").html(data_html);
                }
                else {
                    data_html += "<option value='0'>Seleccione Opción</option>";
                    for (var i = 0; i < obj.length; i++) {
                        data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>";
                    }
                    $("select[id$=cmbMes_General]").html(data_html);
                }
            };
            var error = function (xhr, ajaxOptions, thrownError) {
            };
            fn_LlamadoMetodo("CantidadColaboradores.aspx/Personal_CantidadColaboradores_EvolucionGeneralMensual_ddlMes_CS", JSON.stringify({ departamento: Departamento, area: Area, subArea: SubArea, sucursal: Sucursal }), sucess, error);
        }

        function fn_General_Cargar_FiltroDepartamento(Mes, Area, SubArea, Sucursal) {
            var html = '';
            var sucess = function (response) {
                var obj = JSON.parse(response.d);
                var data_html = "";
                if (obj == null) {
                    data_html = "<option value='0'>Seleccione Opción</option>";
                    $("select[id$=cmbDepartamento_general]").html(data_html);
                }
                else {
                    data_html += "<option value='0'>Seleccione Opción</option>";
                    for (var i = 0; i < obj.length; i++) {
                        data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>";
                    }
                    $("select[id$=cmbDepartamento_general]").html(data_html);
                }
            };
            var error = function (xhr, ajaxOptions, thrownError) {
            };
            fn_LlamadoMetodo("CantidadColaboradores.aspx/Personal_CantidadColaboradores_EvolucionGeneralMensual_dllDepartamento_CS", JSON.stringify({ mes: Mes, area: Area, subArea: SubArea, sucursal: Sucursal }), sucess, error);
        }


        function fn_General_Cargar_FiltroArea(Mes, Departamento, SubArea, Sucursal) {
            var html = '';
            var sucess = function (response) {
                var obj = JSON.parse(response.d);
                var data_html = "";
                if (obj == null) {
                    data_html = "<option value='0'>Seleccione Opción</option>";
                    $("select[id$=cmbArea_general]").html(data_html);
                }
                else {
                    data_html += "<option value='0'>Seleccione Opción</option>";
                    for (var i = 0; i < obj.length; i++) {
                        data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>";
                    }
                    $("select[id$=cmbArea_general]").html(data_html);
                }
            };
            var error = function (xhr, ajaxOptions, thrownError) {
            };
            fn_LlamadoMetodo("CantidadColaboradores.aspx/Personal_CantidadColaboradores_EvolucionGeneralMensual_ddlArea_CS", JSON.stringify({ mes: Mes, departamento: Departamento, subArea: SubArea, sucursal: Sucursal }), sucess, error);
        }

        function fn_General_Cargar_FiltroSubArea(Mes, Departamento, Area, Sucursal) {
            var html = '';
            var sucess = function (response) {
                var obj = JSON.parse(response.d);
                var data_html = "";
                if (obj == null) {
                    data_html = "<option value='0'>Seleccione Opción</option>";
                    $("select[id$=cmbSubArea_general]").html(data_html);
                }
                else {
                    data_html += "<option value='0'>Seleccione Opción</option>";
                    for (var i = 0; i < obj.length; i++) {
                        data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>";
                    }
                    $("select[id$=cmbSubArea_general]").html(data_html);
                }
            };
            var error = function (xhr, ajaxOptions, thrownError) {
            };
            fn_LlamadoMetodo("CantidadColaboradores.aspx/Personal_CantidadColaboradores_EvolucionGeneralMensual_ddlSubArea_CS", JSON.stringify({ mes: Mes, departamento: Departamento, area: Area, sucursal: Sucursal }), sucess, error);
        }

        function fn_General_Cargar_FiltroSucursal(Mes, Departamento, Area, SubArea) {
            var html = '';
            var sucess = function (response) {
                var obj = JSON.parse(response.d);
                var data_html = "";
                if (obj == null) {
                    data_html = "<option value='0'>Seleccione Opción</option>";
                    $("select[id$=cmbSucursal_General]").html(data_html);
                }
                else {
                    data_html += "<option value='0'>Seleccione Opción</option>";
                    for (var i = 0; i < obj.length; i++) {
                        data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>";
                    }
                    $("select[id$=cmbSucursal_General]").html(data_html);
                }
            };
            var error = function (xhr, ajaxOptions, thrownError) {
            };
            fn_LlamadoMetodo("CantidadColaboradores.aspx/Personal_CantidadColaboradores_EvolucionGeneralMensual_dllSucursal_CS", JSON.stringify({ mes: Mes, departamento: Departamento, area: Area, subArea: SubArea }), sucess, error);
        }

        function ValidarCampo() {
            Departamento = $("#cmbDepartamento_general").val(); if (Departamento != 0) { Departamento = $("#cmbDepartamento_general option:selected").text(); } else { Departamento = ""; }
            Area = $("#cmbArea_general").val(); if (Area != 0) { Area = $("#cmbArea_general option:selected").text(); } else { Area = ""; }
            SubArea = $("#cmbSubArea_general").val(); if (SubArea != 0) { SubArea = $("#cmbSubArea_general option:selected").text(); } else { SubArea = ""; }
            Sucursal = $("#cmbSucursal_General").val(); if (Sucursal != 0) { Sucursal = $("#cmbSucursal_General option:selected").text(); } else { Sucursal = ""; }
            Mes = $("#cmbMes_General").val(); if (Mes != 0) { Mes = $("#cmbMes_General option:selected").text(); } else { Mes = ""; }
            if ($("#opc_trimestre").prop('checked')) { agrupacion = "3"; }
            if ($("#opc_bimestre").prop('checked')) { agrupacion = "2"; }
            if ($("#opc_bimestre").prop('checked') == false && $("#opc_trimestre").prop('checked') == false) { agrupacion = "0"; }

        }
        function Cambiarestado() {
            if (Departamento != 0) $("#cmbDepartamento_general").val(0); document.getElementById("cmbDepartamento_general").disabled = false;
            if (Area != 0) $("#cmbArea_general").val(0); document.getElementById("cmbArea_general").disabled = false;
            if (Mes != 0) $("#cmbMes_General").val(0); document.getElementById("cmbMes_General").disabled = false;
            if (SubArea != 0) $("#cmbSubArea_general").val(0); document.getElementById("cmbSubArea_general").disabled = false;
            if (Sucursal != 0) $("#cmbSucursal_General").val(0); document.getElementById("cmbSucursal_General").disabled = false;
            $("#opc_trimestre").prop("checked", false); $("#opc_trimestre").prop('disabled', false);
            $("#opc_bimestre").prop("checked", false); $("#opc_bimestre").prop('disabled', false);
            ValidarCampo();
            fn_CargarCombos(Mes, Departamento, Area, SubArea, Sucursal);
            Cargar_Grafica("", "", "", "", "", "0");
        }

        function Cargar_Grafica(Mes, Departamento, Area, SubArea, Sucursal, agrupacion) {
            var sucess = function (response) {
                json = response.d.Series
                LineChart(response.d.Fecha, response.d.Name, fn_ValidarANulos(json));
                $('#Colaboradores').show();
            };
            var error = function (xhr, ajaxOptions, thrownError) {
                fn_Mensaje_Alerta("error", " Ocurrio un error mientras se cargaban los datos", "", "Alerta");
            };
            fn_LlamadoMetodo("CantidadColaboradores.aspx/G_Personal_CantidadColaboradores_EvolucionGeneralMensual_CS", JSON.stringify({ mes: Mes, departamento: Departamento, area: Area, subArea: SubArea, sucursal: Sucursal, agrupacionF: agrupacion }), sucess, error);
        }

        function LineChart(fecha, Name, data) {
            Highcharts.chart('Colaboradores', {
                chart: {
                    type: 'line'
                },
                title: {
                    text: 'EVOLUCIÓN GENERAL MENSUAL'
                },
                subtitle: {
                    text: 'MESES'
                },
                xAxis: {
                    categories: JSON.parse(fecha)
                },
                yAxis: {
                    min: 0,


                    title: {
                        text: 'CANTIDAD DE COLABORADORES '
                    }
                },
                plotOptions: {
                    line: {
                        dataLabels: {
                            enabled: true
                        },
                        enableMouseTracking: true
                    }
                },
                series: JSON.parse(data)
            });
        }


        ///////   EVOLUCIÓN HISTÓRICA_EVOLUCIÓN POR AREA MENSUAL

        var EArea = ""; var ESubArea = ""; var ESucursal = ""; var EAnio = ""; var EMes = ""; var EAnio = ""; var EAgrupacion = "";
        var temp = false;
        fn_Combos(EAnio, EMes, EArea, ESubArea, ESucursal);
        Cargar_Grafica_Chart("", "", "", "", "", "0");

        $(document).ready(function () {
            $("#opc_Etrimestre").change(function () {
                $("#opc_Ebimestre").prop("checked", false);
                Validar();
                if (EAgrupacion === "3") {
                    $("#cmbEMes_General").prop('disabled', true);
                    $("#cmbEAnio_General").prop('disabled', true);
                    fn_Mensaje_Alerta("alerta", " No podra seleccionar Mes y año ", "", "AlertaMensaje");
                }
                else {
                    $("#cmbEMes_General").prop('disabled', false);
                    $("#cmbEAnio_General").prop('disabled', false);

                }
                Cargar_Grafica_Chart(EAnio, EMes, EArea, ESubArea, ESucursal, EAgrupacion);
            });

            $("#opc_Ebimestre").change(function () {
                $("#opc_Etrimestre").prop("checked", false);
                Validar();
                if (EAgrupacion === "2") {
                    $("#cmbEMes_General").prop('disabled', true);
                    $("#cmbEAnio_General").prop('disabled', true);
                    fn_Mensaje_Alerta("alerta", " No podra seleccionar mes y año", "", "AlertaMensaje");
                }
                else {
                    $("#cmbEMes_General").prop('disabled', false);
                    $("#cmbEAnio_General").prop('disabled', false);

                }
                Cargar_Grafica_Chart(EAnio, EMes, EArea, ESubArea, ESucursal, EAgrupacion);
            });
            $("#cmbEMes_General").change(function () {
                if (($("#cmbEAnio_General").val()) != 0) {
                    $("#opc_Etrimestre").prop('disabled', true);
                    $("#opc_Ebimestre").prop('disabled', true);
                    $("#cmbEMes_General").prop('disabled', false);
                }
                else {
                    $("#cmbEMes_General").val(0); document.getElementById("cmbEMes_General").disabled = false;
                    $("#cmbEMes_General").prop("disabled", true);
                    fn_Mensaje_Alerta("alerta", " No podra seleccionar un mes sin un año", "", "AlertaMensaje");
                }
            
                Validar();
               fn_Combos(EAnio, EMes, EArea, ESubArea, ESucursal);
            });

            $("#cmbEAnio_General").change(function () {
                Validar();
                $("#opc_trimestre").prop("checked", false);
                $("#opc_bimestre").prop("checked", false);
                if (($("#cmbEAnio_General").val()) == 0) {
                    $("#opc_Etrimestre").prop('disabled', false);
                    $("#opc_Ebimestre").prop('disabled', false);
                    $("#cmbEMes_General").prop('disabled', true);
                    $("#cmbEMes_General").val(0); document.getElementById("cmbEMes_General").disabled = false;
                }
                else {

                    $("#opc_Etrimestre").prop('disabled', true);
                    $("#opc_Ebimestre").prop('disabled', true);
                    $("#cmbEMes_General").prop('disabled', false);
                    fn_Mensaje_Alerta("alerta", " No podra seleccionar Trimestre y Bimestre", "", "AlertaMensaje");

                }
                fn_Combos(EAnio, EMes, EArea, ESubArea, ESucursal);
            });

            $("#cmbEArea_general").change(function () {
                Validar();
                fn_Combos(EAnio, EMes, EArea, ESubArea, ESucursal);
            });

            $("#cmbESubArea_general").change(function () {
                Validar();
                fn_Combos(EAnio, EMes, EArea, ESubArea, ESucursal);
            });

            $("#cmbESucursal_General").change(function () {
                Validar();
                fn_Combos(EAnio, EMes, EArea, ESubArea, ESucursal);
            });

            $("#Personal_CantidadColaboradores_EvaluacionArea").click(function () {

                Validar();
                Cargar_Grafica_Chart(EAnio, EMes, EArea, ESubArea, ESucursal, EAgrupacion);
            });
        });

        function fn_Combos(EAnio, EMes, EArea, ESubArea, ESucursal) {
            if (EArea == 0 || EArea == "") fn_FiltroEArea(EAnio, EMes, ESubArea, ESucursal);
            if (ESubArea == 0 || ESubArea == "") fn_EFiltroSubArea(EAnio, EMes, EArea, ESucursal);
            if (ESucursal == 0 || ESucursal == "") fn_EFiltroSucursal(EAnio, EMes, EArea, ESubArea);
            if (EAnio == 0 || EAnio == "") fn_EFiltroAnio(EMes, EArea, ESubArea, ESucursal);
            if (EMes == 0 || EMes == "") fn_EFiltroMes(EAnio, EArea, ESubArea, ESucursal);
        }

        function Validar() {
            EArea = $("#cmbEArea_general").val(); if (EArea != 0) { EArea = $("#cmbEArea_general option:selected").text(); } else { EArea = ""; }
            ESubArea = $("#cmbESubArea_general").val(); if (ESubArea != 0) { ESubArea = $("#cmbESubArea_general option:selected").text(); } else { ESubArea = ""; }
            ESucursal = $("#cmbESucursal_General").val(); if (ESucursal != 0) { ESucursal = $("#cmbESucursal_General option:selected").text(); } else { ESucursal = ""; }
            EAnio = $("#cmbEAnio_General").val(); if (EAnio != 0) { EAnio = $("#cmbEAnio_General option:selected").text(); } else { EAnio = ""; }
            EMes = $("#cmbEMes_General").val(); if (EMes != 0) { EMes = $("#cmbEMes_General option:selected").text(); } else { EMes = ""; }
            if ($("#opc_Etrimestre").prop('checked')) { EAgrupacion = "3"; }
            if ($("#opc_Ebimestre").prop('checked')) { EAgrupacion = "2"; }
            if ($("#opc_Ebimestre").prop('checked') == false && $("#opc_Etrimestre").prop('checked') == false) { EAgrupacion = "0"; }
        }
        function Estado() {
            if (EArea != 0) $("#cmbEArea_general").val(0); document.getElementById("cmbEArea_general").disabled = false;
            if (ESubArea != 0) $("#cmbESubArea_general").val(0); document.getElementById("cmbESubArea_general").disabled = false;
            if (ESucursal != 0) $("#cmbESucursal_General").val(0); document.getElementById("cmbESucursal_General").disabled = false;
            if (EAnio != 0) $("#cmbEAnio_General").val(0); document.getElementById("cmbEAnio_General").disabled = false;
            if (EMes != 0) $("#cmbEMes_General").val(0); document.getElementById("cmbEMes_General").disabled = false;
            $("#opc_Etrimestre").prop("checked", false);
            $("#opc_Ebimestre").prop("checked", false);
            Validar();
            fn_Combos(EAnio, EMes, EArea, ESubArea, ESucursal)
            Cargar_Grafica_Chart(EAnio, EMes, EArea, ESubArea, ESucursal, EAgrupacion);
        }
        function fn_FiltroEArea(EAnio, EMes, ESubArea, ESucursal) {
            var html = '';
            var sucess = function (response) {
                var obj = JSON.parse(response.d);
                var data_html = "";
                if (obj == null) {
                    data_html = "<option value='0'>Seleccione Opción</option>";
                    $("select[id$=cmbEArea_general]").html(data_html);
                }
                else {
                    data_html += "<option value='0'>Seleccione Opción</option>";
                    for (var i = 0; i < obj.length; i++) {
                        data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>";
                    }
                    $("select[id$=cmbEArea_general]").html(data_html);
                }
            };
            var error = function (xhr, ajaxOptions, thrownError) {
            };
            fn_LlamadoMetodo("CantidadColaboradores.aspx/Personal_CantidadColaboradores_EvolucionAreaMensual_ddlArea_CS", JSON.stringify({ anio: EAnio, mes: EMes, subArea: ESubArea, sucursal: ESucursal }), sucess, error);
        }

        function fn_EFiltroSubArea(EAnio, EMes, EArea, ESucursal) {
            var html = '';
            var sucess = function (response) {
                var obj = JSON.parse(response.d);
                var data_html = "";
                if (obj == null) {
                    data_html = "<option value='0'>Seleccione Opción</option>";
                    $("select[id$=cmbESubArea_general]").html(data_html);
                }
                else {
                    data_html += "<option value='0'>Seleccione Opción</option>";
                    for (var i = 0; i < obj.length; i++) {
                        data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>";
                    }
                    $("select[id$=cmbESubArea_general]").html(data_html);
                }
            };
            var error = function (xhr, ajaxOptions, thrownError) {
            };
            fn_LlamadoMetodo("CantidadColaboradores.aspx/Personal_CantidadColaboradores_EvolucionAreaMensual_ddlSubArea_CS", JSON.stringify({ anio: EAnio, mes: EMes, area: EArea, sucursal: ESucursal }), sucess, error);
        }

        function fn_EFiltroSucursal(EAnio, EMes, EArea, ESubArea) {
            var html = '';
            var sucess = function (response) {
                var obj = JSON.parse(response.d);
                var data_html = "";
                if (obj == null) {
                    data_html = "<option value='0'>Seleccione Opción</option>";
                    $("select[id$=cmbESucursal_General]").html(data_html);
                }
                else {
                    data_html += "<option value='0'>Seleccione Opción</option>";
                    for (var i = 0; i < obj.length; i++) {
                        data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>";
                    }
                    $("select[id$=cmbESucursal_General]").html(data_html);
                }
            };
            var error = function (xhr, ajaxOptions, thrownError) {
            };
            fn_LlamadoMetodo("CantidadColaboradores.aspx/Personal_CantidadColaboradores_EvolucionAreaMensual_ddlSucursal_CS", JSON.stringify({ anio: EAnio, mes: EMes, area: EArea, subArea: ESubArea }), sucess, error);
        }


        function fn_EFiltroAnio(EMes, EArea, ESubArea, ESucursal) {
            var html = '';
            var sucess = function (response) {
                var obj = JSON.parse(response.d);
                var data_html = "";
                if (obj == null) {
                    data_html = "<option value='0'>Seleccione Opción</option>";
                    $("select[id$=cmbEAnio_General]").html(data_html);
                }
                else {
                    data_html += "<option value='0'>Seleccione Opción</option>";
                    for (var i = 0; i < obj.length; i++) {
                        data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>";
                    }
                    $("select[id$=cmbEAnio_General]").html(data_html);
                }
            };
            var error = function (xhr, ajaxOptions, thrownError) {
            };
            fn_LlamadoMetodo("CantidadColaboradores.aspx/Personal_CantidadColaboradores_EvolucionAreaMensual_ddlAnio_CS", JSON.stringify({ mes: EMes, area: EArea, subArea: ESubArea, sucursal: ESucursal }), sucess, error);
        }
        function fn_EFiltroMes(EAnio,EArea, ESubArea, ESucursal) {
            var html = '';
            var sucess = function (response) {
                var obj = JSON.parse(response.d);
                var data_html = "";
                if (obj == null) {
                    data_html = "<option value='0'>Seleccione Opción</option>";
                    $("select[id$=cmbEMes_General]").html(data_html);
                }
                else {
                    data_html += "<option value='0'>Seleccione Opción</option>";
                    for (var i = 0; i < obj.length; i++) {
                        data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>";
                    }
                    $("select[id$=cmbEMes_General]").html(data_html);
                }
            };
            var error = function (xhr, ajaxOptions, thrownError) {
            };
            fn_LlamadoMetodo("CantidadColaboradores.aspx/Personal_CantidadColaboradores_EvolucionAreaMensual_ddlMes_CS", JSON.stringify({ anio: EAnio, area: EArea, subArea: ESubArea, sucursal: ESucursal }), sucess, error);
        }

        function Cargar_Grafica_Chart(EAnio, EMes, EArea, ESubArea, ESucursal, EAgrupacion) {
            var sucess = function (response) {
                //fn_Mensaje_Alerta("Confirmacion", "Se esta ejecutando los datos", "", "AlertaMensaje");
                json = response.d.Series
                GraficaChart(response.d.Fecha, response.d.Name, fn_ValidarANulos(json));
                $('#Evolucion_Mensual').show();
                ValidarCampo();
            };
            var error = function (xhr, ajaxOptions, thrownError) {
                fn_Mensaje_Alerta("error", " Ocurrio un error mientras se cargaban los datos", "", "AlertaMensaje");
            };
            fn_LlamadoMetodo("CantidadColaboradores.aspx/G_Personal_CantidadColaboradores_EvolucionAreaMensual_CS", JSON.stringify({ anio: EAnio, mes: EMes, area: EArea, subArea: ESubArea, sucursal: ESucursal, agrupacionA: EAgrupacion }), sucess, error);
        }

        function GraficaChart(fecha, Name, data) {
            Highcharts.chart('Evolucion_Mensual', {
                chart: {
                    type: 'line'
                },
                title: {
                    text: 'EVOLUCIÓN POR ÁREA MENSUAL'
                },
                subtitle: {
                    text: 'DEPARTAMENTO'
                },
                xAxis: {
                    categories: JSON.parse(fecha)
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'CANTIDAD DE COLABORADORES '
                    }
                },
                plotOptions: {
                    line: {
                        dataLabels: {
                            enabled: true
                        },
                        enableMouseTracking: true
                    }
                },
                series: JSON.parse(data)
            });
        }

     </script>

</asp:Content>
