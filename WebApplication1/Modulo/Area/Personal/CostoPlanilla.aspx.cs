﻿using ENTIDAD.Graficas;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Script.Serialization;
using System.Web.Services;

namespace WebApplication1.Modulo.Area.Personal
{
    public partial class CsotosPlanilla : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        public static object Personal_CostosPlanilla_CS_cantidadGeneral()
        {
            List<String> Cantidad = new List<String>();
            JavaScriptSerializer sr = new JavaScriptSerializer();
            DataTable cantidad = LOGICA.PersonalL.Instancia.G_Personal_CostosPlanilla_LO_cantidadGeneral();
            if (cantidad != null)
            {
                for (int i = 0; i < cantidad.Rows.Count; i++)
                {
                    Cantidad.Add("S/" + Math.Round((Convert.ToDecimal(cantidad.Rows[i]["Cantidad"]) / 1000), 2));
                }
            }
            else { return null; }
            return sr.Serialize(Cantidad);
        }

        [WebMethod]
        public static Object Grafica_Personal_CostosPlanilla_CS_cantidadArea(String sucursal, String area, String subarea, String fechaInicio, String fechaFin)
        {
            List<String> Departamentos = new List<String>(); //nombre de marca
            List<Decimal> Data = new List<Decimal>();
            DataTable dt = LOGICA.PersonalL.Instancia.G_Personal_costosPlanilla_LO_cantidadArea(sucursal, area, subarea, fechaInicio, fechaFin);

            if (dt != null)
            {
                int x = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    if ((Convert.ToInt32((dr[x + 1])).ToString()) == "NULL")
                    {
                        Departamentos.Add("NO INDICA");
                    }
                    else
                    {
                        Data.Add(Convert.ToDecimal((dr[x + 1])));
                        Departamentos.Add((dr[x]).ToString());
                    }
                }

                JavaScriptSerializer sr = new JavaScriptSerializer();
                return new { Departamentos, Data, };
            }
            else { return null; }
        }

        [WebMethod]
        public static object F_Personal_CostosPlanilla_CS_ddSucursal(String area, String subarea, String fechaInicio, String fechaFin)
        {
            List<String> Sucursal = new List<String>();
            JavaScriptSerializer sr = new JavaScriptSerializer();
            DataTable filtro_marca = LOGICA.PersonalL.Instancia.G_Personal_CostoPlanilla_LO_ddlSucursal(area, subarea, fechaInicio, fechaFin);
            if (filtro_marca != null)
            {
                for (int i = 0; i < filtro_marca.Rows.Count; i++)
                {
                    if ((filtro_marca.Rows[i]["Sucursal"].ToString()) == "NULL")
                    {
                        Sucursal.Add("NO INDICA");
                    }
                    else
                    {
                        Sucursal.Add(filtro_marca.Rows[i]["Sucursal"].ToString());
                    }

                }
            }
            else { return null; }
            return sr.Serialize(Sucursal);
        }

        [WebMethod]
        public static object F_Personal_CostosPlanilla_CS_ddArea(String sucursal, String subarea, String fechaInicio, String fechaFin)
        {
            List<String> Area = new List<String>();
            JavaScriptSerializer sr = new JavaScriptSerializer();
            DataTable filtro_area = LOGICA.PersonalL.Instancia.G_Personal_CostoPlanilla_LO_ddlArea(sucursal, subarea, fechaInicio, fechaFin);
            if (filtro_area != null)
            {
                for (int i = 0; i < filtro_area.Rows.Count; i++)
                {
                    if ((filtro_area.Rows[i]["Area"].ToString()) == "NULL")
                    {
                        Area.Add("NO INDICA");
                    }
                    else
                    {
                        Area.Add(filtro_area.Rows[i]["Area"].ToString());
                    }

                }
            }
            else { return null; }
            return sr.Serialize(Area);
        }

        [WebMethod]
        public static object F_Personal_CostosPlanilla_CS_ddSubArea(String sucursal, String area, String fechaInicio, String fechaFin)
        {
            List<String> SubArea = new List<String>();
            JavaScriptSerializer sr = new JavaScriptSerializer();
            DataTable filtro_subArea = LOGICA.PersonalL.Instancia.G_Personal_CostoPlanilla_LO_ddlSubArea(sucursal, area, fechaInicio, fechaFin);
            if (filtro_subArea != null)
            {
                for (int i = 0; i < filtro_subArea.Rows.Count; i++)
                {
                    if ((filtro_subArea.Rows[i]["SubArea"].ToString()) == "NULL")
                    {
                        SubArea.Add("NO INDICA");
                    }
                    else
                    {
                        SubArea.Add(filtro_subArea.Rows[i]["SubArea"].ToString());
                    }

                }
            }
            else { return null; }
            return sr.Serialize(SubArea);
        }

        #region EVOLUCIÓN GENERAL MENSUAL
        public static String ConvertMes(String mes)
        {
            if (mes == "Enero") { mes = "1"; }
            if (mes == "Febrero") { mes = "2"; }
            if (mes == "Marzo") { mes = "3"; }
            if (mes == "Abril") { mes = "4"; }
            if (mes == "Mayo") { mes = "5"; }
            if (mes == "Junio") { mes = "6"; }
            if (mes == "Julio") { mes = "7"; }
            if (mes == "Agosto") { mes = "8"; }
            if (mes == "Septiembre") { mes = "9"; }
            if (mes == "Octubre") { mes = "10"; }
            if (mes == "Noviembre") { mes = "11"; }
            if (mes == "Diciembre") { mes = "12"; }
            return mes;
        }
        [WebMethod]
        public static object Personal_CostoPlanilla_EvolucionGeneralMensual_ddlSucursal_CS(String mes, String departamento, String area, String subArea)
        {
            String EGMes;
            EGMes = ConvertMes(mes);
            List<String> Sucursal = new List<String>();
            JavaScriptSerializer sr = new JavaScriptSerializer();
            DataTable filtro_Sucursal = LOGICA.PersonalL.Instancia.Personal_CostoPlanilla_EvolucionGeneralMensual_ddlSucursal_LO(EGMes, departamento, area, subArea);
            if (filtro_Sucursal != null)
            {
                for (int i = 0; i < filtro_Sucursal.Rows.Count; i++)
                {
                    if ((filtro_Sucursal.Rows[i]["Sucursal"].ToString()) == "NULL")
                    {
                        Sucursal.Add("NO INDICA");
                    }
                    else
                    {
                        Sucursal.Add(filtro_Sucursal.Rows[i]["Sucursal"].ToString());
                    }

                }
            }
            else { return null; }
            return sr.Serialize(Sucursal);
        }
        [WebMethod]
        public static object Personal_CostoPlanilla_EvolucionGeneralMensual_ddlDepartamento_CS(String mes, String area, String subArea, String sucursal)
        {
            String EGMes;
            EGMes = ConvertMes(mes);
            List<String> Departamento = new List<String>();
            JavaScriptSerializer sr = new JavaScriptSerializer();
            DataTable filtro_Departamento = LOGICA.PersonalL.Instancia.Personal_CostoPlanilla_EvolucionGeneralMensual_ddlDepartamento_LO(EGMes, area, subArea, sucursal);
            if (filtro_Departamento != null)
            {
                for (int i = 0; i < filtro_Departamento.Rows.Count; i++)
                {
                    if ((filtro_Departamento.Rows[i]["Departamento"].ToString()) == "NULL")
                    {
                        Departamento.Add("NO INDICA");
                    }
                    else
                    {
                        Departamento.Add(filtro_Departamento.Rows[i]["Departamento"].ToString());
                    }

                }
            }
            else
            { return null; }
            return sr.Serialize(Departamento);
        }
        [WebMethod]
        public static object Personal_CostoPlanilla_EvolucionGeneralMensual_ddlArea_CS(String mes, String departamento, String subArea, String sucursal)
        {
            String EGMes;
            EGMes = ConvertMes(mes);
            List<String> Area = new List<String>();
            JavaScriptSerializer sr = new JavaScriptSerializer();
            DataTable filtro_Area = LOGICA.PersonalL.Instancia.Personal_CostoPlanilla_EvolucionGeneralMensual_ddlArea_LO(EGMes, departamento, subArea, sucursal);
            if (filtro_Area != null)
            {
                for (int i = 0; i < filtro_Area.Rows.Count; i++)
                {
                    if ((filtro_Area.Rows[i]["Area"].ToString()) == "NULL")
                    {
                        Area.Add("NO INDICA");
                    }
                    else
                    {
                        Area.Add(filtro_Area.Rows[i]["Area"].ToString());
                    }

                }
            }
            else { return null; }
            return sr.Serialize(Area);
        }
        [WebMethod]
        public static object Personal_CostoPlanilla_EvolucionGeneralMensual_ddlSubArea_CS(String mes, String departamento, String area, String sucursal)
        {
            String EGMes;
            EGMes = ConvertMes(mes);
            List<String> SubArea = new List<String>();
            JavaScriptSerializer sr = new JavaScriptSerializer();
            DataTable filtro_SubArea = LOGICA.PersonalL.Instancia.Personal_CostoPlanilla_EvolucionGeneralMensual_ddlSubArea_LO(EGMes, departamento, area, sucursal);
            if (filtro_SubArea != null)
            {
                for (int i = 0; i < filtro_SubArea.Rows.Count; i++)
                {
                    if ((filtro_SubArea.Rows[i]["SubArea"].ToString()) == "NULL")
                    {
                        SubArea.Add("NO INDICA");
                    }
                    else
                    {
                        SubArea.Add(filtro_SubArea.Rows[i]["SubArea"].ToString());
                    }

                }
            }
            else { return null; }
            return sr.Serialize(SubArea);
        }
        [WebMethod]
        public static object Personal_CostoPlanilla_EvolucionGeneralMensual_ddlMes_CS(String departamento, String area, String subArea, String sucursal)
        {
            List<String> Mes = new List<String>();
            JavaScriptSerializer sr = new JavaScriptSerializer();
            DataTable filtro_Mes = LOGICA.PersonalL.Instancia.Personal_CostoPlanilla_EvolucionGeneralMensual_ddlMes_LO(departamento, area, subArea, sucursal);
            if (filtro_Mes != null)
            {
                for (int i = 0; i < filtro_Mes.Rows.Count; i++)
                {
                    if ((filtro_Mes.Rows[i]["Meses"].ToString()) == "NULL")
                    {
                        Mes.Add("NO INDICA");
                    }
                    else
                    {
                        Mes.Add(filtro_Mes.Rows[i]["Meses"].ToString());
                    }

                }
            }
            else { return null; }
            return sr.Serialize(Mes);
        }
        [WebMethod]
        public static object G_Personal_CostoPlanilla_EvolucionGeneralMensual_CS(String mes, String departamento, String area, String subArea, String sucursal, String agrupacionF)
        {
            String EGMes;
            EGMes = ConvertMes(mes);
            int agrupacion = Convert.ToInt32(agrupacionF);
            List<Object> contenedor = new List<Object>();
            List<String> Columnas = new List<String>();
            List<Ratio> series = new List<Ratio>();
            DataTable dt = LOGICA.PersonalL.Instancia.G_Personal_CostoPlanilla_EvolucionGeneralMensual_LO(EGMes, departamento, area, subArea, sucursal);
            LineChart data = new LineChart();

            if (dt != null)
            {
                foreach (DataColumn dataColumn in dt.Columns)
                {
                    Columnas.Add(dataColumn.ColumnName);
                }
                int j = 1;

                foreach (DataRow dr in dt.Rows)
                {
                    j = 1;
                    data.Fecha.Add(dr[0].ToString());
                    data.Name = Columnas;

                    while (j < Columnas.Count)
                    {
                        if (data.Series.Count < Columnas.Count - 1)
                            data.Series.Add(new Ratio());
                        string value = dr[j].ToString();
                        value = ((Convert.ToDecimal(value)) / 1000).ToString("N1");
                        data.Series[j - 1].data.Add(Convert.ToDecimal(value));
                        data.Series[j - 1].name = Columnas[j];
                        j++;
                    }
                }
                if (agrupacion == 0)
                {
                    JavaScriptSerializer sr = new JavaScriptSerializer();
                    return new { Fecha = sr.Serialize(data.Fecha), Name = sr.Serialize(data.Name), Series = sr.Serialize(data.Series), };
                }
            }
            //trimestre and bmestre
            String fechaAnio = "";
            Decimal acumulador = 0;
            Decimal total = 0;
            int contadorTrimestre = 1;
            String anioActual = "";
            String nombreAgrupacion = "";
            LineChart data1 = new LineChart();
            if (agrupacion != 0)
            {
                if (agrupacion == 2) { nombreAgrupacion = "Bimestre "; }
                if (agrupacion == 3) { nombreAgrupacion = "Trimestre "; }
                for (int m = 0; m < data.Fecha.Count; m++)
                {


                    if (fechaAnio != anioActual) { contadorTrimestre = 1; }
                    if (m % agrupacion == 0)
                    {
                        data1.Fecha.Add(nombreAgrupacion + (contadorTrimestre).ToString());
                        contadorTrimestre++;
                    }
                    else
                    {
                        if (m == data.Fecha.Count - 1)
                        {
                            data1.Fecha.Add(nombreAgrupacion + (contadorTrimestre).ToString());
                            contadorTrimestre++;
                        }
                    }

                }

                int contadorObjeto = 0;
                for (int i = 0; i < data.Series.Count; i++)
                {
                    data1.Series.Add(new Ratio());
                    for (int j = 0; j < data.Series[i].data.Count; j++)
                    {
                        contadorObjeto = j + 1;
                        var value = data.Series[i].data[j];
                        acumulador = acumulador + value;
                        if (contadorObjeto % agrupacion == 0 && contadorObjeto != 0)
                        {
                            total = acumulador / agrupacion;
                            data1.Series[i].data.Add(Math.Round(total, 1));
                            acumulador = 0;
                            total = 0;
                        }
                        else
                        {
                            if (contadorObjeto == data.Series[i].data.Count)
                            {
                                total = acumulador / agrupacion;
                                data1.Series[i].data.Add(Math.Round(total, 1));
                                acumulador = 0;
                                total = 0;
                            }
                        }

                    }
                    data1.Series[i].name = Columnas[i + 1];
                }
                JavaScriptSerializer sr = new JavaScriptSerializer();
                return new { Fecha = sr.Serialize(data1.Fecha), Name = sr.Serialize(data.Name), Series = sr.Serialize(data1.Series) };
            }
            else { return null; }
        }
        #endregion
        #region EVOLUCIÓN POR ÁREA MENSUAL

        [WebMethod]
        public static object Personal_CostoPlanilla_EvolucionAreaMensual_ddlSucursal_CS(String anio, String mes, String area, String subArea)
        {
            String EAMes;
            EAMes = ConvertMes(mes);
            List<String> ddlSucursal = new List<String>();
            JavaScriptSerializer sr = new JavaScriptSerializer();
            DataTable filtro_Sucursal = LOGICA.PersonalL.Instancia.Personal_CostoPlanilla_EvolucionAreaMensual_ddlSucursal_LO(anio, EAMes, area, subArea);
            if (filtro_Sucursal != null)
            {
                for (int i = 0; i < filtro_Sucursal.Rows.Count; i++)
                {
                    if ((filtro_Sucursal.Rows[i]["Sucursal"].ToString()) == "NULL")
                    {
                        ddlSucursal.Add("NO INDICA");
                    }
                    else
                    {
                        ddlSucursal.Add(filtro_Sucursal.Rows[i]["Sucursal"].ToString());
                    }

                }
            }
            else { return null; }
            return sr.Serialize(ddlSucursal);
        }
        [WebMethod]
        public static object Personal_CostoPlanilla_EvolucionAreaMensual_ddlArea_CS(String anio, String mes, String subArea, String sucursal)
        {
            String EAMes;
            EAMes = ConvertMes(mes);
            List<String> ddlArea = new List<String>();
            JavaScriptSerializer sr = new JavaScriptSerializer();
            DataTable filtro_Area = LOGICA.PersonalL.Instancia.Personal_CostoPlanilla_EvolucionAreaMensual_ddlArea_LO(anio, EAMes, subArea, sucursal);
            if (filtro_Area != null)
            {
                for (int i = 0; i < filtro_Area.Rows.Count; i++)
                {
                    if ((filtro_Area.Rows[i]["Area"].ToString()) == "NULL")
                    {
                        ddlArea.Add("NO INDICA");
                    }
                    else
                    {
                        ddlArea.Add(filtro_Area.Rows[i]["Area"].ToString());
                    }

                }
            }
            else { return null; }
            return sr.Serialize(ddlArea);
        }
        [WebMethod]
        public static object Personal_CostoPlanilla_EvolucionAreaMensual_ddlSubArea_CS(String anio, String mes, String area, String sucursal)
        {
            String EAMes;
            EAMes = ConvertMes(mes);
            List<String> ddlSubArea = new List<String>();
            JavaScriptSerializer sr = new JavaScriptSerializer();
            DataTable filtro_SubArea = LOGICA.PersonalL.Instancia.Personal_CostoPlanilla_EvolucionAreaMensual_ddlSubArea_LO(anio, EAMes, area, sucursal);
            if (filtro_SubArea != null)
            {
                for (int i = 0; i < filtro_SubArea.Rows.Count; i++)
                {
                    if ((filtro_SubArea.Rows[i]["SubArea"].ToString()) == "NULL")
                    {
                        ddlSubArea.Add("NO INDICA");
                    }
                    else
                    {
                        ddlSubArea.Add(filtro_SubArea.Rows[i]["SubArea"].ToString());
                    }

                }
            }
            else { return null; }
            return sr.Serialize(ddlSubArea);
        }
        [WebMethod]
        public static object Personal_CostoPlanilla_EvolucionAreaMensual_ddlAnio_CS(String mes, String area, String subArea, String sucursal)
        {
            String EAMes;
            EAMes = ConvertMes(mes);
            List<String> ddlAnio = new List<String>();
            JavaScriptSerializer sr = new JavaScriptSerializer();
            DataTable filtro_Anio = LOGICA.PersonalL.Instancia.Personal_CostoPlanilla_EvolucionAreaMensual_ddlAnio_LO(EAMes, area, subArea, sucursal);
            if (filtro_Anio != null)
            {
                for (int i = 0; i < filtro_Anio.Rows.Count; i++)
                {
                    if ((filtro_Anio.Rows[i]["Anio"].ToString()) == "NULL")
                    {
                        ddlAnio.Add("NO INDICA");
                    }
                    else
                    {
                        ddlAnio.Add(filtro_Anio.Rows[i]["Anio"].ToString());
                    }

                }
            }
            else { return null; }
            return sr.Serialize(ddlAnio);
        }
        [WebMethod]
        public static object Personal_CostoPlanilla_EvolucionAreaMensual_ddlMes_CS(String anio, String area, String subArea, String sucursal)
        {
            List<String> ddlMes = new List<String>();
            JavaScriptSerializer sr = new JavaScriptSerializer();
            DataTable filtro_Mes = LOGICA.PersonalL.Instancia.Personal_CostoPlanilla_EvolucionAreaMensual_ddlMes_LO(anio, area, subArea, sucursal);
            if (filtro_Mes != null)
            {
                for (int i = 0; i < filtro_Mes.Rows.Count; i++)
                {
                    if ((filtro_Mes.Rows[i]["Meses"].ToString()) == "NULL")
                    {
                        ddlMes.Add("NO INDICA");
                    }
                    else
                    {
                        ddlMes.Add(filtro_Mes.Rows[i]["Meses"].ToString());
                    }

                }
            }
            else { return null; }
            return sr.Serialize(ddlMes);
        }

        [WebMethod]
        public static object G_Personal_CostoPlanilla_EvolucionAreaMensual_CS(String anio, String mes, String area, String subArea, String sucursal, String agrupacionA)
        {
            String EAMes;
            EAMes = ConvertMes(mes);
            int agrupacion = Convert.ToInt32(agrupacionA);
            List<Object> contenedor = new List<Object>();
            List<String> Columnas = new List<String>();
            List<Ratio> series = new List<Ratio>();
            DataTable dt = LOGICA.PersonalL.Instancia.G_Personal_CostoPlanilla_EvolucionAreaMensual_LO(anio, EAMes, area, subArea, sucursal);
            LineChart data = new LineChart();

            if (dt != null)
            {
                foreach (DataColumn dataColumn in dt.Columns)
                {
                    Columnas.Add(dataColumn.ColumnName);
                }
                int j = 1;

                foreach (DataRow dr in dt.Rows)
                {
                    j = 1;
                    data.Fecha.Add(dr[0].ToString());
                    data.Name = Columnas;

                    while (j < Columnas.Count)
                    {
                        if (data.Series.Count < Columnas.Count - 1)
                            data.Series.Add(new Ratio());
                        string value = dr[j].ToString();
                        value = ((Convert.ToDecimal(value)) / 1000).ToString("N1");
                        data.Series[j - 1].data.Add(Convert.ToDecimal(value));
                        data.Series[j - 1].name = Columnas[j];
                        j++;
                    }
                }
                if (agrupacion == 0)
                {
                    JavaScriptSerializer sr = new JavaScriptSerializer();
                    return new { Fecha = sr.Serialize(data.Fecha), Name = sr.Serialize(data.Name), Series = sr.Serialize(data.Series), };
                }
            }
            //trimestre and bmestre
            String fechaAnio = "";
            Decimal acumulador = 0;
            Decimal total = 0;
            int contadorTrimestre = 1;
            String anioActual = "";
            String nombreAgrupacion = "";
            LineChart data1 = new LineChart();
            if (agrupacion != 0)
            {
                if (agrupacion == 2) { nombreAgrupacion = "Bimestre "; }
                if (agrupacion == 3) { nombreAgrupacion = "Trimestre "; }
                for (int m = 0; m < data.Fecha.Count; m++)
                {


                    if (fechaAnio != anioActual) { contadorTrimestre = 1; }
                    if (m % agrupacion == 0)
                    {
                        data1.Fecha.Add(nombreAgrupacion + (contadorTrimestre).ToString());
                        contadorTrimestre++;
                    }
                    else
                    {
                        if (m == data.Fecha.Count - 1)
                        {
                            data1.Fecha.Add(nombreAgrupacion + (contadorTrimestre).ToString());
                            contadorTrimestre++;
                        }
                    }

                }

                int contadorObjeto = 0;
                for (int i = 0; i < data.Series.Count; i++)
                {
                    data1.Series.Add(new Ratio());
                    for (int j = 0; j < data.Series[i].data.Count; j++)
                    {
                        contadorObjeto = j + 1;
                        var value = data.Series[i].data[j];
                        acumulador = acumulador + value;
                        if (contadorObjeto % agrupacion == 0 && contadorObjeto != 0)
                        {
                            total = acumulador / agrupacion;
                            data1.Series[i].data.Add(Math.Round(total, 1));
                            acumulador = 0;
                            total = 0;
                        }
                        else
                        {
                            if (contadorObjeto == data.Series[i].data.Count)
                            {
                                total = acumulador / agrupacion;
                                data1.Series[i].data.Add(Math.Round(total, 1));
                                acumulador = 0;
                                total = 0;
                            }
                        }

                    }
                    data1.Series[i].name = Columnas[i + 1];
                }
                JavaScriptSerializer sr = new JavaScriptSerializer();
                return new { Fecha = sr.Serialize(data1.Fecha), Name = sr.Serialize(data.Name), Series = sr.Serialize(data1.Series) };
            }
            else { return null; }
        }

        #endregion
    }
}