<%@ Page Title="" Language="C#" MasterPageFile="/MasterPage/Intranet.Master" AutoEventWireup="true" CodeBehind="InformeVenta.aspx.cs" Inherits="WebApplication1.Modulo.Area.Venta.InformeVenta" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="/Recurso2/Base/Modulo/Venta.js"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContenedorDashboard" runat="server">
    <script>
        var divGrafica = "G_Ventas_VVA_S";
         $(document).ready(function () {
             GraficarVentas_S(divGrafica);
             GraficarVentas_P("G_Ventas_VVA_P");
             GraficarComparativos_CMH();
             Listar_Mayor_Crecimiento_Menual();
             Listar_Menor_Crecimiento_Menual();
        });

        function GraficarVentas_S(divGrafica) {

            var sucess = function (response) {
                Grafica_Ventas_VVA_S(response.d.Marcas, response.d.Data, divGrafica);
                //$('#G_Ventas_VVA_S').show();
            };
            var error = function (xhr, ajaxOptions, thrownError) {
                fn_Mensaje_Alerta("Error", " Ocurri� un error en la carga de datos.", "", "AlertaMensaje");
            };
            fn_LlamadoMetodo("InformeVenta.aspx/Grafica_Informes_Venta_S", JSON.stringify({}), sucess, error);
        }

        

        function GraficarComparativos_CMH() {
            var sucess = function (response) {
                Grafica_Comparativo_CMH( response.d.Fecha, response.d.Name, response.d.Series);
                $('#G_Comparativo_CMH').show();
            };
            var error = function (xhr, ajaxOptions, thrownError) {
                fn_Mensaje_Alerta("Error", " Ocurri� un error en la carga de datos.", "", "AlertaMensaje");
            };
            fn_LlamadoMetodo("InformeVenta.aspx/Grafica_Comparativo_Historico", JSON.stringify({ }), sucess, error);
        }

        function Grafica_Comparativo_CMH(Fecha, Name, data)
        {
            Highcharts.chart('G_Comparativo_CMH', {
                chart: {
                    type: 'column',
                    spacingTop: 5,
                },
                title: {
                    text: 'COMPARATIVO POR MES EN HISTORICO',
                    align: 'left',
                    x: 0,
                },
                xAxis: {
                    categories: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']//JSON.parse(Fecha)
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: null
                    },
                    labels: {
                        formatter: function () {
                            return (this.value) + 'M';
                        }
                    },
                    stackLabels: {
                        enabled: true,
                        style: {
                            fontWeight: 'bold',
                            color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                        }
                    }
                },
                legend: {
                    align: 'right',
                    x: -30,
                    verticalAlign: 'top',
                    y: 25,
                    floating: true,
                    backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
                    borderColor: '#CCC',
                    borderWidth: 1,
                    shadow: false
                },
                tooltip: {
                    headerFormat: '<b>{point.x}</b><br/>',
                    pointFormat: '{series.name}: {point.y:.2f}M<br/>Total: {point.stackTotal:.2f}M'
                },
                plotOptions: {
                    column: {
                        stacking: 'normal',
                        dataLabels: {
                            enabled: true,
                            color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                            format: '{point.y:.2f}M',
                            borderColor: '#FF5722',
                            borderWidth: 8,
                            shadow: false
                        }
                    }
                },
                series: JSON.parse(data)
            });            
        }

        function Listar_Mayor_Crecimiento_Menual()
        {
            var sucess = function (response) {
                Tabla_Ranking_Mayor(response.d.Sucursal, response.d.Porcentaje);
            };
            var error = function (xhr, ajaxOptions, thrownError) {
                fn_Mensaje_Alerta("Error", " Ocurri� un error en la carga de datos.", "", "AlertaMensaje");
            };
            fn_LlamadoMetodo("InformeVenta.aspx/Listar_Mayor_Crecimiento_Menual", JSON.stringify({}), sucess, error);

        }

        //Pasajes vendidos en millones
        function GraficarVentas_P(divGrafica) {
            var sucess = function (response) {
                Grafica_Ventas_VVA_P(response.d.Marcas, response.d.Data, divGrafica);
                //$('#G_Ventas_VVA_P').show();
            };
            var error = function (xhr, ajaxOptions, thrownError) {
                fn_Mensaje_Alerta("Error", " Ocurri� un error en la carga de datos.", "", "AlertaMensaje");
            };
            fn_LlamadoMetodo("InformeVenta.aspx/Grafica_Informes_Venta_P", JSON.stringify({}), sucess, error);
        }


        function Tabla_Ranking_Mayor(Sucursal, Porcentaje )
        {
            var i = 0;
            var eri=  "</thead>" +
                      "<tbody>";
            var html;
            html = "<div>" +
                " <h4> Ranking Mayor Crecimiento Mensual</h4>" +
                " <section id=\"unseen\">" +
                " <table class=\"table table-bordered table-striped table-condensed\">" +
                "<thead>";
            for (i = 0; i < Sucursal.length; i++) {
                "<tr>" +
                    " <th>"+Sucursal[i]+"</th>" +
                    "</tr>";
            }

            html += eri;
            for (i = 0; i < Sucursal.length; i++)
            {
                html += 
                    "<tr>"+
                    " <td style=\"font - size: 13px;\">" +Sucursal[i]+ "</td>"+     
                     "</tr>"+
                     "<tr>"+
                    " <td style=\"font - size: 13px;\">" +Porcentaje[i]+ "</td>"+     
                     "</tr>";

            }
              html+=    "</tbody>"+
                          "</table>"+
                          "</section>"+
                "</div>";
            $('div[id$=tablaMC]').append(html);
        }

        function Listar_Menor_Crecimiento_Menual() {
            var sucess = function (response) {
                Tabla_Ranking_Menor(response.d.Sucursal, response.d.Porcentaje);
            };
            var error = function (xhr, ajaxOptions, thrownError) {
                fn_Mensaje_Alerta("Error", " Ocurri� un error en la carga de datos.", "", "AlertaMensaje");
            };
            fn_LlamadoMetodo("InformeVenta.aspx/Listar_Menor_Crecimiento_Menual", JSON.stringify({}), sucess, error);

        }

        function Tabla_Ranking_Menor(Sucursal, Porcentaje) {
            var i = 0;
            var eri=  "</thead>" +
                      "<tbody>";
            var html;
            html = "<div>" +
                " <h4> Ranking Menor Crecimiento Mensual</h4>" +
                " <section id=\"unseen\">" +
                " <table class=\"table table-bordered table-striped table-condensed\">" +
                "<thead>";
            for (i = 0; i < Sucursal.length; i++) {
                "<tr>" +
                    " <th>"+Sucursal[i]+"</th>" +
                    "</tr>";
            }

            html += eri;
            for (i = 0; i < Sucursal.length; i++)
            {
                html += 
                    "<tr>"+
                    " <td  style=\"font - size: 13px;\">" +Sucursal[i]+ "</td>"+     
                     "</tr>"+
                     "<tr>"+
                    " <td  style=\"font - size: 13px;\">" +Porcentaje[i]+ "</td>"+     
                     "</tr>";

            }
              html+=                              
                              "</tbody>"+
                          "</table>"+
                          "</section>"+
                "</div>";
                
            $('div[id$=tablaNC]').append(html);
        }
        
    </script>

    <h3 class="panel-body" style="COLOR: #283593;margin-top: 0px;">INFORMES DE VENTAS ANUALES</h3>
            <div id="AlertaMensaje"></div>
        <div class="row">
            <div class="col-lg-8 col-md-10">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-6">
                        <div id="G_Ventas_VVA_S" style="min-width: 310px; max-width: 800px; height: 180px; margin: 0 auto"></div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-6">  
                            <div id="G_Ventas_VVA_P" style="min-width: 310px; max-width: 800px; height: 180px; margin: 0 auto"></div>
                    </div>
                </div>

                <br />
                <div class="row">
                    <div class="col-lg-12">
                   <%--     <div class="row">--%>
                            <div id="G_Comparativo_CMH" style="min-width: 310px; height: 250px; margin: 0 auto"></div>
                        <%--</div>--%>
                    </div>
                </div>
                <br />
            </div>
                    <div class="col-lg-2 col-md-2" style="padding-left: 0px;">
                        <div class="panel" id="tablaMC"></div>
                    </div>
                    <div class="col-lg-2 col-md-2" style="padding-left: 0px;">
                        <div class="panel" id="tablaNC"></div>
                    </div>
        </div>
</asp:Content>
