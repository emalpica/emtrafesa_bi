﻿using ENTIDAD.Graficas;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Script.Serialization;
using System.Web.Services;

namespace WebApplication1.Modulo.Area.Venta
{
    public partial class InformeVenta : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        public static Object Grafica_Informes_Venta_S()
        {
            List<String> Marcas = new List<String>(); //nombre de marca
            DataTable dt = LOGICA.VentaL.Instancia.Listar_Informes_Venta();
            List<Decimal> Data = new List<Decimal>();

            if (dt != null)
            {
                int x = 0;
                int j = 0, m = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    Data.Add(Convert.ToDecimal((dr[m + 1]).ToString()) / 1000000);
                    Marcas.Add((dr[m]).ToString());
                }
                JavaScriptSerializer sr = new JavaScriptSerializer();
                return new { Marcas, Data, };
            }
            else { return null; }
        }

        [WebMethod]
        public static Object Grafica_Informes_Venta_P()
        {
            List<String> Marcas = new List<String>(); //nombre de marca
            DataTable dt = LOGICA.VentaL.Instancia.Listar_Informes_Venta();
            List<Decimal> Data = new List<Decimal>();

            if (dt != null)
            {
                int x = 0;
                int j = 0, m = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    Data.Add(Convert.ToDecimal((dr[m + 2]).ToString()) / 1000000);
                    Marcas.Add((dr[m]).ToString());
                }
                JavaScriptSerializer sr = new JavaScriptSerializer();
                return new { Marcas, Data, };
            }
            else { return null; }

        }

        [WebMethod]
        public static Object Grafica_Comparativo_Historico()
        {
            List<String> Columna = new List<String>();
            List<Ratio> series = new List<Ratio>();
            DataTable dt = LOGICA.VentaL.Instancia.Listar_Comparativo_Historico();
            LineChart data = new LineChart();
            Ratio ratio = new Ratio();
            String valueAnio = "";
            int j = 0;
            if (dt != null)
            {
                String temp = "";
                foreach (DataRow dr in dt.Rows)
                {
                    valueAnio = dr[j + 1].ToString();
                    if (temp != valueAnio) { Columna.Add(valueAnio); temp = valueAnio; }
                }
                foreach (DataRow dr in dt.Rows)
                {
                    var fecha = dr[0].ToString();
                    if (fecha != "12")
                    { data.Fecha.Add(dr[0].ToString()); }
                    else { data.Fecha.Add(dr[0].ToString()); break; }
                }
                var temp2 = ""; var mes = "";
                j = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    valueAnio = dr[1].ToString();
                    if (temp2 != valueAnio)
                    {
                        data.Series.Add(new Ratio());
                        temp2 = valueAnio;
                        String value = Convert.ToString(dr[2]);
                        value = ((Convert.ToDecimal(value)) / 1000000).ToString("N1");
                        //data.Series[j].data.Add((Convert.ToDecimal(value))/1000000);
                        data.Series[j].data.Add((Convert.ToDecimal(value)));
                        data.Series[j].name = Columna[j];
                    }
                    else
                    {
                        String value = Convert.ToString(dr[2]);
                        value = ((Convert.ToDecimal(value)) / 1000000).ToString("N1");
                        data.Series[j].data.Add((Convert.ToDecimal(value)));
                        data.Series[j].name = Columna[j];
                    }
                    mes = dr[0].ToString();
                    if (mes == "12") { j++; }
                }
                JavaScriptSerializer sr = new JavaScriptSerializer();
                return new { Fecha = sr.Serialize(data.Fecha), Name = sr.Serialize(data.Name), Series = sr.Serialize(data.Series), };
            }
            else { return null; }
        }

        [WebMethod]
        public static Object Listar_Mayor_Crecimiento_Menual()
        {
            List<String> Sucursal = new List<String>(); //nombre de marca
            List<String> Porcentaje = new List<String>();
            DataTable dt = LOGICA.VentaL.Instancia.Listar_Mayor_Crecimiento_Menual();
            if (dt != null)
            {
                int x = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    Porcentaje.Add(((dr[x + 1]).ToString()));
                    Sucursal.Add((dr[x]).ToString());
                }
                JavaScriptSerializer sr = new JavaScriptSerializer();
                return new { Sucursal, Porcentaje, };
            }
            else { return null; }
        }

        [WebMethod]
        public static Object Listar_Menor_Crecimiento_Menual()
        {
            List<String> Sucursal = new List<String>(); //nombre de marca
            List<String> Porcentaje = new List<String>();
            DataTable dt = LOGICA.VentaL.Instancia.Listar_Menor_Crecimiento_Menual();
            if (dt != null)
            {
                int x = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    Porcentaje.Add(((dr[x + 1]).ToString()));
                    Sucursal.Add((dr[x]).ToString());
                }
                JavaScriptSerializer sr = new JavaScriptSerializer();
                return new { Sucursal, Porcentaje, };
            }
            else { return null; }
        }

    }
}