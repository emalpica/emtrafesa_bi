﻿using ENTIDAD.Graficas;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Script.Serialization;
using System.Web.Services;


namespace WebApplication1.Modulo.Area.Encomienda
{
    public partial class RutaVehiculo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

          public static String ConvertMes(String mes)
        {
            if (mes == "Enero") { mes = "1"; }
            if (mes == "Febrero") { mes = "2"; }
            if (mes == "Marzo") { mes = "3"; }
            if (mes == "Abril") { mes = "4"; }
            if (mes == "Mayo") { mes = "5"; }
            if (mes == "Junio") { mes = "6"; }
            if (mes == "Julio") { mes = "7"; }
            if (mes == "Agosto") { mes = "8"; }
            if (mes == "Septiembre") { mes = "9"; }
            if (mes == "Octubre") { mes = "10"; }
            if (mes == "Noviembre") { mes = "11"; }
            if (mes == "Diciembre") { mes = "12"; }
            return mes;
        }


        // ###################### GRAFICA: CANTIDAD VIAJE SEGUN RUTA POR TIPO VEHICULO ####################

        [WebMethod]
         public static object Grafica_Encomiendas_RutasVehiculos_GeneralTipoVehiculo_CS(String Año, String Mes, String Trimestre, String Bimestre )
        {
            List<Object> contenedor = new List<Object>();
            List<String> Columnas = new List<String>(); //nombre de marca
            List<Ratio> series = new List<Ratio>();
            DataTable dt = LOGICA.EncomiendaL.Instancia.Grafica_Encomiendas_RutasVehiculos_GeneralTipoVehiculo_LO(Año, Mes, Trimestre, Bimestre);
            LineChart data = new LineChart();

            if (dt != null)
            {
                foreach (DataColumn dataColumn in dt.Columns)
                {
                    Columnas.Add(dataColumn.ColumnName);
                }
                int j = 1;

                foreach (DataRow dr in dt.Rows)
                {
                    j = 1;
                    data.Fecha.Add(dr[0].ToString());
                    data.Name = Columnas;

                    while (j < Columnas.Count)
                    {
                        if (data.Series.Count < Columnas.Count - 1)
                            data.Series.Add(new Ratio());
                        string value = dr[j].ToString();

                        data.Series[j - 1].data.Add(Convert.ToDecimal(value));
                        data.Series[j - 1].name = Columnas[j];
                        j++;
                    }
                }
               
                    JavaScriptSerializer sr = new JavaScriptSerializer();
                    return new { Fecha = sr.Serialize(data.Fecha), Name = sr.Serialize(data.Name), Series = sr.Serialize(data.Series), };
               
            }    
            else { return null; }   
        }


        // Listar Año - General por Tipo de Vehiculo
        [WebMethod]
        public static object Encomiendas_RutasVehiculos_ListarAño_GTV_CS(String Mes, String Trimestre, String Bimestre){
            List<String> cmbAño_GTV = new List<String>();
            JavaScriptSerializer sr = new JavaScriptSerializer();
            DataTable filtro_Año = LOGICA.EncomiendaL.Instancia.Encomiendas_RutasVehiculos_ListarAño_GTV_LO( Mes, Trimestre, Bimestre);
            if (filtro_Año != null)
            {
                for (int i = 0; i < filtro_Año.Rows.Count; i++)
                {
                    if ((filtro_Año.Rows[i]["Año"].ToString()) == "NULL")
                    {
                        cmbAño_GTV.Add("NO INDICA");
                    }
                    else
                    {
                        cmbAño_GTV.Add(filtro_Año.Rows[i]["Año"].ToString());
                    }

                }
            }
            else { return null; }
            return sr.Serialize(cmbAño_GTV);
        }

        //Listar Mes - General por Tipo de Vehiculo
         [WebMethod]
        public static object Encomiendas_RutasVehiculos_ListarMes_GTV_CS(String Año){
            List<String> cmbMes_GTV = new List<String>();
            JavaScriptSerializer sr = new JavaScriptSerializer();
            DataTable filtro_Mes = LOGICA.EncomiendaL.Instancia.Encomiendas_RutasVehiculos_ListarMes_GTV_LO(Año);
            if (filtro_Mes != null)
            {
                for (int i = 0; i < filtro_Mes.Rows.Count; i++)
                {
                    if ((filtro_Mes.Rows[i]["Meses"].ToString()) == "NULL")
                    {
                        cmbMes_GTV.Add("NO INDICA");
                    }
                    else
                    {
                        cmbMes_GTV.Add(filtro_Mes.Rows[i]["Meses"].ToString());
                    }

                }
            }
            else { return null; }
            return sr.Serialize(cmbMes_GTV);
        }

        //Listar Bimestre - General por Tipo de Vehiculo
         [WebMethod]
        public static object Encomiendas_RutasVehiculos_ListarBimestre_GTV_CS(String Año){
            List<String> cmbBimestre_GTV = new List<String>();
            JavaScriptSerializer sr = new JavaScriptSerializer();
            DataTable filtro_Bimestre = LOGICA.EncomiendaL.Instancia.Encomiendas_RutasVehiculos_ListarBimestre_GTV_LO(Año);
            if (filtro_Bimestre != null)
            {
                for (int i = 0; i < filtro_Bimestre.Rows.Count; i++)
                {
                    if ((filtro_Bimestre.Rows[i]["Bimestre"].ToString()) == "NULL")
                    {
                        cmbBimestre_GTV.Add("NO INDICA");
                    }
                    else
                    {
                        cmbBimestre_GTV.Add(filtro_Bimestre.Rows[i]["Bimestre"].ToString());
                    }

                }
            }
            else { return null; }
            return sr.Serialize(cmbBimestre_GTV);

        }


        [WebMethod]
        public static object Encomiendas_RutasVehiculos_ListarTrimestre_GTV_CS(String Año){
            List<String> cmbTrimestre_GTV = new List<String>();
            JavaScriptSerializer sr = new JavaScriptSerializer();
            DataTable filtro_Trimestre = LOGICA.EncomiendaL.Instancia.Encomiendas_RutasVehiculos_ListarTrimestre_GTV_LO(Año);
            if (filtro_Trimestre != null)
            {
                for (int i = 0; i < filtro_Trimestre.Rows.Count; i++)
                {
                    if ((filtro_Trimestre.Rows[i]["Trimestre"].ToString()) == "NULL")
                    {
                        cmbTrimestre_GTV.Add("NO INDICA");
                    }
                    else
                    {
                        cmbTrimestre_GTV.Add(filtro_Trimestre.Rows[i]["Trimestre"].ToString());
                    }

                }
            }
            else { return null; }
            return sr.Serialize(cmbTrimestre_GTV);

        }

        
        // ##### GRAFICA: CANTIDAD VIAJE SEGUN RUTA POR TIEMPO ######

        [WebMethod]
          public static object Grafica_Encomiendas_RutasVehiculos_GeneralRutaTiempo_CS(String anio, String mes, String tipo,String agrupacionF )
        {
            String EOMes;
            EOMes = ConvertMes(mes);
            int agrupacion = Convert.ToInt32(agrupacionF);
            List<Object> contenedor = new List<Object>();
            List<String> Columnas = new List<String>();
            List<String> Col = new List<String>();
            List<Ratio> series = new List<Ratio>();
            DataTable dt = LOGICA.EncomiendaL.Instancia.Grafica_Encomiendas_RutasVehiculos_GeneralRutaTiempo_LO(anio, mes, tipo);
            DataTable tri = LOGICA.EncomiendaL.Instancia.Grafica_Encomienda_RutasVehiculos_GeneralRutaTiempo_AgrupacionTrimestre_LO(anio,tipo);
            DataTable bi = LOGICA.EncomiendaL.Instancia.Grafica_Encomienda_RutasVehiculo_GeneralRutaTiempo_AgrupacionBimestre_LO(anio,tipo);
            LineChart data = new LineChart();
            Ratio ratio = new Ratio();

            if (dt != null|| tri!=null || bi !=null)
            {
                if (agrupacion == 0)
                {
                    foreach (DataColumn dataColumn in dt.Columns)
                    {
                       Columnas.Add(dataColumn.ColumnName);
                    }
                    int j = 1;
                    foreach (DataRow dr in dt.Rows)
                    {
                        j = 1;
                       
                        data.Fecha.Add(dr[0].ToString());
                        data.Name = Columnas;
                        while (j < Columnas.Count-1)
                        {
                            if (data.Series.Count < Columnas.Count - 2)
                                data.Series.Add(new Ratio());
                            string value = dr[j].ToString();
                            value = ((Convert.ToDecimal(value))/ 1000).ToString("N1");
                            data.Series[j - 1].data.Add(Convert.ToDecimal(value));
                                data.Series[j - 1].name = Columnas[j];
                                j++;
                          
                        
                         }
                    }
                }
                if (agrupacion == 2)
                {
                    foreach (DataColumn dataColumn in bi.Columns)
                    {
                        Col.Add(dataColumn.ColumnName);
                    }
                    int j = 1;

                    foreach (DataRow dr in bi.Rows)
                    {
                        j = 1;
                        data.Fecha.Add(dr[0].ToString());
                        data.Name = Col;
                        while (j < Col.Count-1)
                        {
                            if (data.Series.Count < Col.Count - 2)
                                data.Series.Add(new Ratio());
                            string value = dr[j].ToString();
                            value = ((Convert.ToDecimal(value)) / 1000).ToString("N1");
                            data.Series[j - 1].data.Add(Convert.ToDecimal(value));
                            data.Series[j - 1].name = Col[j];
                            j++;
                        }
                    }
                }
                if (agrupacion == 3)
                {
                    foreach (DataColumn dataColumn in tri.Columns)
                    {
                        Col.Add(dataColumn.ColumnName);
                    }
                    int j = 1;

                    foreach (DataRow dr in tri.Rows)
                    {
                        j = 1;
                        data.Fecha.Add(dr[0].ToString());
                        data.Name = Col;
                        while (j < Col.Count-1)
                        {
                            if (data.Series.Count < Col.Count - 2)
                                data.Series.Add(new Ratio());
                            string value = dr[j].ToString();
                            value = ((Convert.ToDecimal(value)) / 1000).ToString("N1");
                            data.Series[j - 1].data.Add(Convert.ToDecimal(value));
                            data.Series[j - 1].name = Col[j];
                            j++;
                        }
                    }
                }
                JavaScriptSerializer sr = new JavaScriptSerializer();
                return new { Fecha = sr.Serialize(data.Fecha), Name = sr.Serialize(data.Name), Series = sr.Serialize(data.Series), };
            }
            else { return null; }


        }

          [WebMethod]
         public static object Encomiendas_RutasVehiculos_ListarAño_GRT_CS(String mes, String tipo){
            List<String> cmbAño_GRT = new List<String>();
            JavaScriptSerializer sr = new JavaScriptSerializer();
            DataTable filtro_Año_GRT = LOGICA.EncomiendaL.Instancia.Encomiendas_RutasVehiculos_ListarAño_GRT_LO( mes, tipo);
            if (filtro_Año_GRT != null)
            {
                for (int i = 0; i < filtro_Año_GRT.Rows.Count; i++)
                {
                    if ((filtro_Año_GRT.Rows[i]["Anio"].ToString()) == "NULL")
                    {
                        cmbAño_GRT.Add("NO INDICA");
                    }
                    else
                    {
                        cmbAño_GRT.Add(filtro_Año_GRT.Rows[i]["Anio"].ToString());
                    }

                }
            }
            else { return null; }
            return sr.Serialize(cmbAño_GRT);

        }
        
        [WebMethod]
         public static object Encomiendas_RutasVehiculos_ListarMes_GRT_CS(String anio, String tipo){
            List<String> cmbMes_GRT = new List<String>();
            JavaScriptSerializer sr = new JavaScriptSerializer();
            DataTable filtro_Mes_GRT = LOGICA.EncomiendaL.Instancia.Encomiendas_RutasVehiculos_ListarMes_GRT_LO( anio, tipo);
            if (filtro_Mes_GRT != null)
            {
                for (int i = 0; i < filtro_Mes_GRT.Rows.Count; i++)
                {
                    if ((filtro_Mes_GRT.Rows[i]["Mes"].ToString()) == "NULL")
                    {
                        cmbMes_GRT.Add("NO INDICA");
                    }
                    else
                    {
                        cmbMes_GRT.Add(filtro_Mes_GRT.Rows[i]["Mes"].ToString());
                    }

                }
            }
            else { return null; }
            return sr.Serialize(cmbMes_GRT);

        }

        [WebMethod]
        public static object Encomiendas_RutasVehiculos_ListarTipo_GRT_CS(String anio, String mes){
            List<String> cmbTipo_GRT = new List<String>();
            JavaScriptSerializer sr = new JavaScriptSerializer();
            DataTable filtro_Tipo_GRT = LOGICA.EncomiendaL.Instancia.Encomiendas_RutasVehiculos_ListarTipo_GRT_LO( anio, mes);
            if (filtro_Tipo_GRT != null)
            {
                for (int i = 0; i < filtro_Tipo_GRT.Rows.Count; i++)
                {
                    if ((filtro_Tipo_GRT.Rows[i]["TipoVehiculo"].ToString()) == "NULL")
                    {
                        cmbTipo_GRT.Add("NO INDICA");
                    }
                    else
                    {
                        cmbTipo_GRT.Add(filtro_Tipo_GRT.Rows[i]["TipoVehiculo"].ToString());
                    }

                }
            }
            else { return null; }
            return sr.Serialize(cmbTipo_GRT);

        }


        // ------------------------   GRAFICA: CANTIDAD VIAJE SEGUN RUTA  { GENERAL POR RUTA}


         [WebMethod]
          public static object Grafica_Encomiendas_RutaVehiculos_CantidadViajesRuta_CS(String Año, String Mes, String agrupacionF )
        {
           int indiceColum = 0;
            int agrupacion = Convert.ToInt32(agrupacionF);
            List<Object> contenedor = new List<Object>();
            List<String> Columnas = new List<String>(); //nombre de marca
            List<Ratio> series = new List<Ratio>();
            DataTable dt = LOGICA.EncomiendaL.Instancia.Grafica_Encomiendas_RutaVehiculos_CantidadViajesRuta_LO(Año, Mes);
            LineChart data = new LineChart();

            if (dt != null)
            {
                foreach (DataColumn dataColumn in dt.Columns)
                {
                    Columnas.Add(dataColumn.ColumnName);
                }
                int j = 1;

                foreach (DataRow dr in dt.Rows)
                {
                    j = 1;
                    data.Fecha.Add(dr[0].ToString());
                    data.Name = Columnas;

                    while (j < Columnas.Count)
                    {
                        if (data.Series.Count < Columnas.Count - 1)
                            data.Series.Add(new Ratio());
                        string value = dr[j].ToString();

                        data.Series[j - 1].data.Add(Convert.ToDecimal(value));
                        data.Series[j - 1].name = Columnas[j];
                        j++;
                    }
                }
                if (agrupacion == 0)
                {
                    JavaScriptSerializer sr = new JavaScriptSerializer();
                    return new { Fecha = sr.Serialize(data.Fecha), Name = sr.Serialize(data.Name), Series = sr.Serialize(data.Series), };
                }
            }
            //trimestre and bmestre
            String fechaAnio = "";
            Decimal acumulador = 0;
            Decimal total = 0;
            int contadorTrimestre = 1;
            String anioActual = "";
            String nombreAgrupacion = "";
            LineChart data1 = new LineChart();
            if (agrupacion != 0)
            {
                if (agrupacion == 2) { nombreAgrupacion = "Bimestre "; }
                if (agrupacion == 3) { nombreAgrupacion = "Trimestre "; }
                for (int m = 0; m < data.Fecha.Count; m++)
                {


                    if (fechaAnio != anioActual) { contadorTrimestre = 1; }
                    if (m % agrupacion == 0)
                    {
                        data1.Fecha.Add(nombreAgrupacion + (contadorTrimestre).ToString());
                        contadorTrimestre++;
                    }
                    else
                    {
                        if (m == data.Fecha.Count - 1)
                        {
                            data1.Fecha.Add(nombreAgrupacion + (contadorTrimestre).ToString());
                            contadorTrimestre++;
                        }
                    }

                }

                int contadorObjeto = 0;
                for (int i = 0; i < data.Series.Count; i++)
                {
                    data1.Series.Add(new Ratio());
                    for (int j = 0; j < data.Series[i].data.Count; j++)
                    {
                        contadorObjeto = j + 1;
                        var value = data.Series[i].data[j];
                        acumulador = acumulador + value;
                        if (contadorObjeto % agrupacion == 0 && contadorObjeto != 0)
                        {
                            total = acumulador / agrupacion;
                            data1.Series[i].data.Add(Math.Round(total, 1));
                            acumulador = 0;
                            total = 0;
                        }
                        else
                        {
                            if (contadorObjeto == data.Series[i].data.Count)
                            {
                                total = acumulador / agrupacion;
                                data1.Series[i].data.Add(Math.Round(total, 1));
                                acumulador = 0;
                                total = 0;
                            }
                        }

                    }
                    data1.Series[i].name = Columnas[i + 1];
                }
                JavaScriptSerializer sr = new JavaScriptSerializer();
                return new { Fecha = sr.Serialize(data1.Fecha), Name = sr.Serialize(data.Name), Series = sr.Serialize(data1.Series) };
            }
            else { return null; }   
        }

         [WebMethod]
         public static object Encomiendas_RutaVehiculos_ListarMes_CVR_CS(String Año){
            List<String> cmbMes_CVR = new List<String>();
            JavaScriptSerializer sr = new JavaScriptSerializer();
            DataTable filtro_Mes_GRT = LOGICA.EncomiendaL.Instancia.Encomiendas_RutaVehiculos_ListarMes_CVR_LO( Año);
            if (filtro_Mes_GRT != null)
            {
                for (int i = 0; i < filtro_Mes_GRT.Rows.Count; i++)
                {
                    if ((filtro_Mes_GRT.Rows[i]["Mes"].ToString()) == "NULL")
                    {
                        cmbMes_CVR.Add("NO INDICA");
                    }
                    else
                    {
                        cmbMes_CVR.Add(filtro_Mes_GRT.Rows[i]["Mes"].ToString());
                    }

                }
            }
            else { return null; }
            return sr.Serialize(cmbMes_CVR);

        }
        
        [WebMethod]
         public static object Encomiendas_RutaVehiculos_ListarAño_CVR_CS(String Mes){
            List<String> cmbAño_CVR = new List<String>();
            JavaScriptSerializer sr = new JavaScriptSerializer();
            DataTable filtro_Año_GRT = LOGICA.EncomiendaL.Instancia.Encomiendas_RutaVehiculos_ListarAño_CVR_LO( Mes);
            if (filtro_Año_GRT != null)
            {
                for (int i = 0; i < filtro_Año_GRT.Rows.Count; i++)
                {
                    if ((filtro_Año_GRT.Rows[i]["Anio"].ToString()) == "NULL")
                    {
                        cmbAño_CVR.Add("NO INDICA");
                    }
                    else
                    {
                        cmbAño_CVR.Add(filtro_Año_GRT.Rows[i]["Anio"].ToString());
                    }

                }
            }
            else { return null; }
            return sr.Serialize(cmbAño_CVR);

        }

    
    }

    
}