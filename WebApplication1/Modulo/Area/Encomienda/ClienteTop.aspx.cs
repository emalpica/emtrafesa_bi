﻿using ENTIDAD.Graficas;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Script.Serialization;
using System.Web.Services;


namespace WebApplication1.Modulo.Area.Encomienda
{
    public partial class ClienteTop : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        #region Cliente top
        //GIAMPIERE 10/ 09 / 2019
        //CantidaEncomiendas
        public static String ConvertMes(String mes)
        {
            if (mes == "Enero") { mes = "1"; }
            if (mes == "Febrero") { mes = "2"; }
            if (mes == "Marzo") { mes = "3"; }
            if (mes == "Abril") { mes = "4"; }
            if (mes == "Mayo") { mes = "5"; }
            if (mes == "Junio") { mes = "6"; }
            if (mes == "Julio") { mes = "7"; }
            if (mes == "Agosto") { mes = "8"; }
            if (mes == "Septiembre") { mes = "9"; }
            if (mes == "Octubre") { mes = "10"; }
            if (mes == "Noviembre") { mes = "11"; }
            if (mes == "Diciembre") { mes = "12"; }
            return mes;
        }
        [WebMethod]
        public static object Encomienda_ClientesTop_CantidaEncomiendas_ddlOperacion_CS(String mes, String anio)
        {
            String EOMes;
            EOMes = ConvertMes(mes);
            List<String> Tipo = new List<String>();
            JavaScriptSerializer sr = new JavaScriptSerializer();
            DataTable filtro_Tipo = LOGICA.EncomiendaL.Instancia.Encomienda_ClientesTop_CantidaEncomiendas_ddlOperacion_LO(EOMes, anio);
            if (filtro_Tipo != null)
            {
                for (int i = 0; i < filtro_Tipo.Rows.Count; i++)
                {
                    if ((filtro_Tipo.Rows[i]["Operacion"].ToString()) == "NULL")
                    {
                        Tipo.Add("NO INDICA");
                    }
                    else
                    {
                        Tipo.Add(filtro_Tipo.Rows[i]["Operacion"].ToString());
                    }

                }
            }
            else { return null; }
            return sr.Serialize(Tipo);
        }
        [WebMethod]
        public static object Encomienda_ClientesTop_CantidaEncomiendas_ddlMes_CS(String anio, String operacion)
        {
            List<String> Mes = new List<String>();
            JavaScriptSerializer sr = new JavaScriptSerializer();
            DataTable filtro_Mes = LOGICA.EncomiendaL.Instancia.Encomienda_ClientesTop_CantidaEncomiendas_ddlMes_LO(anio, operacion);
            if (filtro_Mes != null)
            {
                for (int i = 0; i < filtro_Mes.Rows.Count; i++)
                {
                    if ((filtro_Mes.Rows[i]["Meses"].ToString()) == "NULL")
                    {
                        Mes.Add("NO INDICA");
                    }
                    else
                    {
                        Mes.Add(filtro_Mes.Rows[i]["Meses"].ToString());
                    }

                }
            }
            else { return null; }
            return sr.Serialize(Mes);
        }
        [WebMethod]
        public static object Encomienda_ClientesTop_CantidaEncomiendas_ddlAño_CS(String mes, String operacion)
        {
            String EOMes;
            EOMes = ConvertMes(mes);
            List<String> Anio = new List<String>();
            JavaScriptSerializer sr = new JavaScriptSerializer();
            DataTable filtro_Anio = LOGICA.EncomiendaL.Instancia.Encomienda_ClientesTop_CantidaEncomiendas_ddlAño_LO(EOMes, operacion);
            if (filtro_Anio != null)
            {
                for (int i = 0; i < filtro_Anio.Rows.Count; i++)
                {
                    if ((filtro_Anio.Rows[i]["Anio"].ToString()) == "NULL")
                    {
                        Anio.Add("NO INDICA");
                    }
                    else
                    {
                        Anio.Add(filtro_Anio.Rows[i]["Anio"].ToString());
                    }

                }
            }
            else { return null; }
            return sr.Serialize(Anio);
        }
        [WebMethod]
        public static object G_Encomienda_ClientesTop_CantidaEncomiendas_CS(String anio, String mes, String operacion, String agrupacionF)
        {
            String EOMes;
            EOMes = ConvertMes(mes);
            int agrupacion = Convert.ToInt32(agrupacionF);
            List<Object> contenedor = new List<Object>();
            List<String> Columnas = new List<String>();
            List<String> Col = new List<String>();
            List<Ratio> series = new List<Ratio>();
            DataTable dt = LOGICA.EncomiendaL.Instancia.G_Encomienda_ClientesTop_CantidaEncomiendas_LO(anio, EOMes, operacion);
            DataTable tri = LOGICA.EncomiendaL.Instancia.Encomienda_ClientesTop_CantidaEncomiendas_AgrupacionTrimestre_LO(anio, operacion);
            DataTable bi = LOGICA.EncomiendaL.Instancia.Encomienda_ClientesTop_CantidaEncomiendas_AgrupacionBimestre_LO(anio, operacion);
            LineChart data = new LineChart();
            Ratio ratio = new Ratio();

            if (dt != null)
            {
                if (agrupacion == 0)
                {
                    foreach (DataColumn dataColumn in dt.Columns)
                    {
                        Columnas.Add(dataColumn.ColumnName);
                    }
                    int j = 1;
                    foreach (DataRow dr in dt.Rows)
                    {
                        j = 1;

                        data.Fecha.Add(dr[0].ToString());
                        data.Name = Columnas;
                        while (j < Columnas.Count-1)
                        {
                            if (data.Series.Count < Columnas.Count -2 )
                                data.Series.Add(new Ratio());
                            string value = dr[j].ToString();
                            data.Series[j - 1].data.Add(Convert.ToDecimal(value));
                            data.Series[j - 1].name = Columnas[j];
                            j++;


                        }
                    }
                }
                if (agrupacion == 2)
                {
                    foreach (DataColumn dataColumn in bi.Columns)
                    {
                        Col.Add(dataColumn.ColumnName);
                    }
                    int j = 1;

                    foreach (DataRow dr in bi.Rows)
                    {
                        j = 1;
                        data.Fecha.Add(dr[0].ToString());
                        data.Name = Col;
                        while (j < Col.Count - 1)
                        {
                            if (data.Series.Count < Col.Count - 2)
                                data.Series.Add(new Ratio());
                            string value = dr[j].ToString();
                            data.Series[j - 1].data.Add(Convert.ToDecimal(value));
                            data.Series[j - 1].name = Col[j];
                            j++;
                        }
                    }
                }
                if (agrupacion == 3)
                {
                    foreach (DataColumn dataColumn in tri.Columns)
                    {
                        Col.Add(dataColumn.ColumnName);
                    }
                    int j = 1;

                    foreach (DataRow dr in tri.Rows)
                    {
                        j = 1;
                        data.Fecha.Add(dr[0].ToString());
                        data.Name = Col;
                        while (j < Col.Count - 1)
                        {
                            if (data.Series.Count < Col.Count - 2)
                                data.Series.Add(new Ratio());
                            string value = dr[j].ToString();
                            data.Series[j - 1].data.Add(Convert.ToDecimal(value));
                            data.Series[j - 1].name = Col[j];
                            j++;
                        }
                    }
                }
                JavaScriptSerializer sr = new JavaScriptSerializer();
                return new { Fecha = sr.Serialize(data.Fecha), Name = sr.Serialize(data.Name), Series = sr.Serialize(data.Series), };
            }
            else { return null; }

        }
        //GIAMPIERE 10/ 09 / 2019
        //Segun Rentablidad
        [WebMethod]
        public static object Encomienda_ClientesTop_Rentabilidad_ddlOperacion_CS(String mes, String anio)
        {
            String EOMes;
            EOMes = ConvertMes(mes);
            List<String> Tipo = new List<String>();
            JavaScriptSerializer sr = new JavaScriptSerializer();
            DataTable filtro_Tipo = LOGICA.EncomiendaL.Instancia.Encomienda_ClientesTop_Rentabilidad_ddlOperacion_LO(EOMes, anio);
            if (filtro_Tipo != null)
            {
                for (int i = 0; i < filtro_Tipo.Rows.Count; i++)
                {
                    if ((filtro_Tipo.Rows[i]["Operacion"].ToString()) == "NULL")
                    {
                        Tipo.Add("NO INDICA");
                    }
                    else
                    {
                        Tipo.Add(filtro_Tipo.Rows[i]["Operacion"].ToString());
                    }

                }
            }
            else { return null; }
            return sr.Serialize(Tipo);
        }
        [WebMethod]
        public static object Encomienda_ClientesTop_Rentabilidad_ddlMes_CS(String anio, String operacion)
        {
            List<String> Mes = new List<String>();
            JavaScriptSerializer sr = new JavaScriptSerializer();
            DataTable filtro_Mes = LOGICA.EncomiendaL.Instancia.Encomienda_ClientesTop_Rentabilidad_ddlMes_LO(anio, operacion);
            if (filtro_Mes != null)
            {
                for (int i = 0; i < filtro_Mes.Rows.Count; i++)
                {
                    if ((filtro_Mes.Rows[i]["Meses"].ToString()) == "NULL")
                    {
                        Mes.Add("NO INDICA");
                    }
                    else
                    {
                        Mes.Add(filtro_Mes.Rows[i]["Meses"].ToString());
                    }

                }
            }
            else { return null; }
            return sr.Serialize(Mes);
        }
        [WebMethod]
        public static object Encomienda_ClientesTop_Rentabilidad_ddlAño_CS(String mes, String operacion)
        {
            String EOMes;
            EOMes = ConvertMes(mes);
            List<String> Anio = new List<String>();
            JavaScriptSerializer sr = new JavaScriptSerializer();
            DataTable filtro_Anio = LOGICA.EncomiendaL.Instancia.Encomienda_ClientesTop_Rentabilidad_ddlAño_LO(EOMes, operacion);
            if (filtro_Anio != null)
            {
                for (int i = 0; i < filtro_Anio.Rows.Count; i++)
                {
                    if ((filtro_Anio.Rows[i]["Anio"].ToString()) == "NULL")
                    {
                        Anio.Add("NO INDICA");
                    }
                    else
                    {
                        Anio.Add(filtro_Anio.Rows[i]["Anio"].ToString());
                    }

                }
            }
            else { return null; }
            return sr.Serialize(Anio);
        }
        [WebMethod]
        public static object G_Encomienda_ClientesTop_Rentabilidad_CS(String anio, String mes, String operacion, String agrupacionF)
        {
            String EOMes;
            EOMes = ConvertMes(mes);
            int agrupacion = Convert.ToInt32(agrupacionF);
            List<Object> contenedor = new List<Object>();
            List<String> Columnas = new List<String>();
            List<String> Col = new List<String>();
            List<Ratio> series = new List<Ratio>();
            DataTable dt = LOGICA.EncomiendaL.Instancia.G_Encomienda_ClientesTop_Rentabilidad_LO(anio, EOMes, operacion);
            DataTable tri = LOGICA.EncomiendaL.Instancia.Encomienda_ClientesTop_Rentabilidad_AgrupacionTrimestre_LO(anio, operacion);
            DataTable bi = LOGICA.EncomiendaL.Instancia.Encomienda_ClientesTop_Rentabilidad_AgrupacionBimestre_LO(anio, operacion);
            LineChart data = new LineChart();
            Ratio ratio = new Ratio();

            if (dt != null)
            {
                if (agrupacion == 0)
                {
                    foreach (DataColumn dataColumn in dt.Columns)
                    {
                        Columnas.Add(dataColumn.ColumnName);
                    }
                    int j = 1;
                    foreach (DataRow dr in dt.Rows)
                    {
                        j = 1;

                        data.Fecha.Add(dr[0].ToString());
                        data.Name = Columnas;
                        while (j < Columnas.Count - 1)
                        {
                            if (data.Series.Count < Columnas.Count - 2)
                                data.Series.Add(new Ratio());
                            string value = dr[j].ToString();
                            data.Series[j - 1].data.Add(Convert.ToDecimal(value));
                            data.Series[j - 1].name = Columnas[j];
                            j++;


                        }
                    }
                }
                if (agrupacion == 2)
                {
                    foreach (DataColumn dataColumn in bi.Columns)
                    {
                        Col.Add(dataColumn.ColumnName);
                    }
                    int j = 1;

                    foreach (DataRow dr in bi.Rows)
                    {
                        j = 1;
                        data.Fecha.Add(dr[0].ToString());
                        data.Name = Col;
                        while (j < Col.Count-1)
                        {
                            if (data.Series.Count < Col.Count - 2)
                                data.Series.Add(new Ratio());
                            string value = dr[j].ToString();
                            data.Series[j - 1].data.Add(Convert.ToDecimal(value));
                            data.Series[j - 1].name = Col[j];
                            j++;
                        }
                    }
                }
                if (agrupacion == 3)
                {
                    foreach (DataColumn dataColumn in tri.Columns)
                    {
                        Col.Add(dataColumn.ColumnName);
                    }
                    int j = 1;

                    foreach (DataRow dr in tri.Rows)
                    {
                        j = 1;
                        data.Fecha.Add(dr[0].ToString());
                        data.Name = Col;
                        while (j < Col.Count-1)
                        {
                            if (data.Series.Count < Col.Count - 2)
                                data.Series.Add(new Ratio());
                            string value = dr[j].ToString();
                            data.Series[j - 1].data.Add(Convert.ToDecimal(value));
                            data.Series[j - 1].name = Col[j];
                            j++;
                        }
                    }
                }
                JavaScriptSerializer sr = new JavaScriptSerializer();
                return new { Fecha = sr.Serialize(data.Fecha), Name = sr.Serialize(data.Name), Series = sr.Serialize(data.Series), };
            }
            else { return null; }

        }
        #endregion
    }
}