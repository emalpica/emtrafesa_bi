﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Intranet.Master" AutoEventWireup="true" CodeBehind="ClienteTop.aspx.cs" Inherits="WebApplication1.Modulo.Area.Encomienda.ClienteTop" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContenedorDashboard" runat="server">
 
    <h3 class="panel-body" style="color: #283593; margin-top: 0px;">CLIENTES TOP</h3>
       <div class="text-left Left aligned text" style="margin-left: 18px;">
        <p><strong>Objetivo:</strong> El objetivo del indicador es mostrar los clientes que más recurren a la empresa a enviar encomiendas y/o giros.</p>
    </div>
  <div class="col-lg-6">
       <div class="row">
            <div class="col-lg-12">
             <div class="panel panel-headline">
                <div class="panel-body">
                    <div class="form-inline">

                         <div class="form-group col-lg-3 col-md-3 col-sm-3">
                            <label for="formGroupExampleInput">Operación:</label>
                            <asp:DropDownList ID="cmbOperacion_general" ClientIDMode="Static" class="form-control" runat="server" Style="width: 100%;"></asp:DropDownList>
                        </div>
                          <div class="form-group col-lg-3 col-md-3 col-sm-3">
                            <label for="formGroupExampleInput">Año:</label>
                              <asp:DropDownList ID="cmbAnio_General" ClientIDMode="Static" class="form-control" runat="server" Style="width: 100%;"></asp:DropDownList>
                        </div>
                         <div class="form-group col-lg-3 col-md-3 col-sm-3">
                            <label for="formGroupExampleInput">Mes:</label>
                           <asp:DropDownList ID="cmbMes_General" ClientIDMode="Static" class="form-control" runat="server" Style="width: 100%;"></asp:DropDownList>
                        </div>

                         <div class="form-group col-lg-3 col-md-3 col-sm-3">
                            <label for="formGroupExampleInput">Agrupación:</label>
                            <label class="fancy-checkbox">
                                <input type="checkbox" id="opc_trimestre">
                                <span>Trimestre</span>
                            </label>
                            <label class="fancy-checkbox">
                                <input type="checkbox" id="opc_bimestre">
                                <span>Bimestre</span>
                            </label>
                            <br />
                        </div>

                        <div class="form-group col-lg-3 col-md-3 col-sm-3">
                            <label for="formGroupExampleInput">Limpiar:</label>
                            <span class="lnr lnr-magic-wand-limpiar" onclick="Estado()"></span>
                        </div>
                        <div class="form-group col-lg-3 col-md-3 col-sm-3">
                            <button id="Clientetop_CantidadEncomienda" type="button" class="btn btn-primary btn-sm">GRAFICAR</button>
                        </div>

                    </div>
                </div>
            </div>
        </div>


         <div class="col-lg-12">
          <div class="row panel-body">
            <div id="CANTIDADENCOMIENDA" style="min-width: 310px; height: 400px; margin: 0 auto; display: none"></div>
            </div>
              </div>
            <div class="col-lg-12">
             <div class="text-left Left aligned text">
                <p><strong>Interpretacion:</strong> Se desea que la cantidad de encomiendas y/o giros de cada cliente aumente</p>
            </div>
           </div>
        </div>
      </div>
      <div class="col-lg-6">
       <div class="row">
            <div class="col-lg-12">
             <div class="panel panel-headline">
                <div class="panel-body">
                    <div class="form-inline">

                         <div class="form-group col-lg-3 col-md-3 col-sm-3">
                            <label for="formGroupExampleInput">Operación:</label>
                            <asp:DropDownList ID="cmbROperacion_general" ClientIDMode="Static" class="form-control" runat="server" Style="width: 100%;"></asp:DropDownList>
                        </div>
                          <div class="form-group col-lg-3 col-md-3 col-sm-3">
                            <label for="cmbRAnio_General">Año:</label>
                              <asp:DropDownList ID="cmbRAnio_General" ClientIDMode="Static" class="form-control" runat="server" Style="width: 100%;"></asp:DropDownList>
                        </div>
                         <div class="form-group col-lg-3 col-md-3 col-sm-3">
                            <label for="formGroupExampleInput">Mes:</label>
                           <asp:DropDownList ID="cmbRMes_General" ClientIDMode="Static" class="form-control" runat="server" Style="width: 100%;"></asp:DropDownList>
                        </div>

                         <div class="form-group col-lg-3 col-md-3 col-sm-3">
                            <label for="formGroupExampleInput">Agrupación:</label>
                            <label class="fancy-checkbox">
                                <input type="checkbox" id="opc_triRMestre">
                                <span>Trimestre</span>
                            </label>
                            <label class="fancy-checkbox">
                                <input type="checkbox" id="opc_biRMestre">
                                <span>Bimestre</span>
                            </label>
                            <br />
                        </div>

                        <div class="form-group col-lg-3 col-md-3 col-sm-3">
                            <label for="formGroupExampleInput">Limpiar:</label>
                            <span class="lnr lnr-magic-wand-limpiar" onclick="EstadoR()"></span>
                        </div>
                        <div class="form-group col-lg-3 col-md-3 col-sm-3">
                            <button id="Clientetop_Rentabilidad" type="button" class="btn btn-primary btn-sm">GRAFICAR</button>
                        </div>

                    </div>
                </div>
            </div>
        </div>


         <div class="col-lg-12">
          <div class="row panel-body">
            <div id="Rentabilidad" style="min-width: 310px; height: 400px; margin: 0 auto; display: none"></div>
            </div>
              </div>
            <div class="col-lg-12">
             <div class="text-left Left aligned text">
                <p><strong>Interpretacion:</strong> Se desea que la rentabilidad en encomiendas y/o giros de cada cliente aumente.</p>
            </div>
           </div>
        </div>
      </div>

    <script>
    var Operacion = ""; var Anio = ""; var Mes = ""; var agrupacion = "0"; 
        CargarCombos(Operacion, Anio, Mes);
        Mostar_Grafica("", "", "", "0");
        $(document).ready(function () {

            $("#opc_trimestre").change(function () {
                $("#opc_bimestre").prop("checked", false);
                Validar();
                Mostar_Grafica(Anio, Mes, Operacion, agrupacion);
            });

            $("#opc_bimestre").change(function () {
                $("#opc_trimestre").prop("checked", false);
                Validar();
                Mostar_Grafica(Anio, Mes, Operacion, agrupacion);
            });

            $("#cmbOperacion_general").change(function () {
                Validar();
                CargarCombos(Operacion, Anio, Mes);
            });
            $("#cmbMes_General").change(function () {
                Validar();
                CargarCombos(Operacion, Anio, Mes);
            });

            $("#cmbAnio_General").change(function () {
                Validar();
                CargarCombos(Operacion, Anio, Mes);
            });

            $("#Clientetop_CantidadEncomienda").click(function () {

                Validar();
                Mostar_Grafica(Anio, Mes, Operacion, agrupacion);
            });
        });

        function CargarCombos(Operacion, Anio, Mes) {
            if (Operacion == 0 || Operacion == "") fn_General_Cargar_FiltroOperacion(Anio,Mes);
            if (Anio == 0 || Anio == "") fn_General_Cargar_FiltroAnio(Mes, Operacion);
            if (Mes == 0 || Mes == "") fn_General_Cargar_FiltroMes(Anio, Operacion);
        }

        function Validar() {
            Operacion = $("#cmbOperacion_general").val(); if (Operacion != 0) { Operacion = $("#cmbOperacion_general option:selected").text(); } else { Operacion = ""; }
            Anio = $("#cmbAnio_General").val(); if (Anio != 0) { Anio = $("#cmbAnio_General option:selected").text(); } else { Anio = ""; }
            Mes = $("#cmbMes_General").val(); if (Mes != 0) { Mes = $("#cmbMes_General option:selected").text(); } else { Mes = ""; }
            if ($("#opc_trimestre").prop('checked')) { agrupacion = "3"; }
            if ($("#opc_bimestre").prop('checked')) { agrupacion = "2"; }
            if ($("#opc_bimestre").prop('checked') == false && $("#opc_trimestre").prop('checked') == false) { agrupacion = "0"; }
        }


        function Estado() {
            if (Operacion != 0) $("#cmbOperacion_general").val(0); document.getElementById("cmbOperacion_general").disabled = false;
            if (Anio != 0) $("#cmbAnio_General").val(0); document.getElementById("cmbAnio_General").disabled = false;
            if (Mes != 0) $("#cmbMes_General").val(0); document.getElementById("cmbMes_General").disabled = false;
            $("#opc_trimestre").prop("checked", false);
            $("#opc_bimestre").prop("checked", false);
            Validar();
            CargarCombos(Operacion, Anio, Mes);
            Mostar_Grafica("", "", "", "0");
        }

        function fn_General_Cargar_FiltroMes(Anio, Operacion) {
            var html = '';
            var sucess = function (response) {
                var obj = JSON.parse(response.d);
                var data_html = "";
                if (obj == null) {
                    data_html = "<option value='0'>Seleccione Opción</option>";
                    $("select[id$=cmbMes_General]").html(data_html);
                }
                else {
                    data_html += "<option value='0'>Seleccione Opción</option>";
                    for (var i = 0; i < obj.length; i++) {
                        data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>";
                    }
                    $("select[id$=cmbMes_General]").html(data_html);
                }
            };
            var error = function (xhr, ajaxOptions, thrownError) {
            };
            fn_LlamadoMetodo("ClienteTop.aspx/Encomienda_ClientesTop_CantidaEncomiendas_ddlMes_CS", JSON.stringify({ anio: Anio, operacion: Operacion }), sucess, error);
        }
        function fn_General_Cargar_FiltroAnio(Mes, Operacion) {
            var html = '';
            var sucess = function (response) {
                var obj = JSON.parse(response.d);
                var data_html = "";
                if (obj == null) {
                    data_html = "<option value='0'>Seleccione Opción</option>";
                    $("select[id$=cmbAnio_General]").html(data_html);
                }
                else {
                    data_html += "<option value='0'>Seleccione Opción</option>";
                    for (var i = 0; i < obj.length; i++) {
                        data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>";
                    }
                    $("select[id$=cmbAnio_General]").html(data_html);
                }
            };
            var error = function (xhr, ajaxOptions, thrownError) {
            };
            fn_LlamadoMetodo("ClienteTop.aspx/Encomienda_ClientesTop_CantidaEncomiendas_ddlAño_CS", JSON.stringify({ mes: Mes, operacion: Operacion }), sucess, error);
        }
        function fn_General_Cargar_FiltroOperacion(Anio, Mes) {
            var html = '';
            var sucess = function (response) {
                var obj = JSON.parse(response.d);
                var data_html = "";
                if (obj == null) {
                    data_html = "<option value='0'>Seleccione Opción</option>";
                    $("select[id$=cmbOperacion_general]").html(data_html);
                }
                else {
                    data_html += "<option value='0'>Seleccione Opción</option>";
                    for (var i = 0; i < obj.length; i++) {
                        data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>";
                    }
                    $("select[id$=cmbOperacion_general]").html(data_html);
                }
            };
            var error = function (xhr, ajaxOptions, thrownError) {
            };
            fn_LlamadoMetodo("ClienteTop.aspx/Encomienda_ClientesTop_CantidaEncomiendas_ddlOperacion_CS", JSON.stringify({ anio: Anio, mes: Mes }), sucess, error);
        }
        function Mostar_Grafica(Anio, Mes, Operacion, agrupacion) {
            var sucess = function (response) {
                BarrasChart(response.d.Fecha, response.d.Name, response.d.Series);
                $('#CANTIDADENCOMIENDA').show();
            };
            var error = function (xhr, ajaxOptions, thrownError) {
            };
            fn_LlamadoMetodo("ClienteTop.aspx/G_Encomienda_ClientesTop_CantidaEncomiendas_CS", JSON.stringify({ anio: Anio, mes: Mes, operacion: Operacion, agrupacionF: agrupacion}), sucess, error);
        }

        function BarrasChart(fecha, Name, data) {
            Highcharts.chart('CANTIDADENCOMIENDA', {
                chart: {
                    type: 'column',
                    inverted: true
                },
                title: {
                    text: 'CANTIDAD DE ENCOMIENDA'
                },
                xAxis: {
                    categories: JSON.parse(fecha)
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'TOTAL '
                    },
                  
                    stackLabels: {
                        enabled: true,
                        style: {
                            fontWeight: 'bold',
                            color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                        }
                    }
                },
                legend: {
                    borderWidth: 1,
                    itemDistance: 5,
                    x: -30,
                    verticalAlign: 'top',
                    y: 10,
                    floating: true,
                    backgroundColor: '#FCFFC5',
                    backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
                    borderColor: '#CCC',
                    borderWidth: 1,
                    shadow: false
                },
                tooltip: {
                    headerFormat: '<b>{point.x}</b><br/>',
                    pointFormat: '{series.name}: {point.y:.1f} <br/>Total: {point.stackTotal:.1f} '
                },
                plotOptions: {
                    column: {
                        stacking: 'normal',

                        pointFormat: "Value: {point.y:,.1f} mm"

                    }
                },
                series: JSON.parse(data)

            });
        }
        //Segun Rentablidad


        var ROperacion = ""; var RAnio = ""; var RMes = ""; var Ragrupacion = "0";
        CargarCombosR(ROperacion, RAnio, RMes);
        Mostar_GraficaR("", "", "", "0");
        $(document).ready(function () {

            $("#opc_triRMestre").change(function () {
                $("#opc_biRMestre").prop("checked", false);
                ValidarR();
                Mostar_GraficaR(RAnio, RMes, ROperacion, Ragrupacion);
            });

            $("#opc_biRMestre").change(function () {
                $("#opc_triRMestre").prop("checked", false);
                ValidarR();
                Mostar_GraficaR(RAnio, RMes, ROperacion, Ragrupacion);
            });

            $("#cmbROperacion_general").change(function () {
                ValidarR();
                CargarCombosR(ROperacion, RAnio, RMes);
            });
            $("#cmbRMes_General").change(function () {
                ValidarR();
                CargarCombosR(ROperacion, RAnio, RMes);
            });

            $("#cmbRAnio_General").change(function () {
                ValidarR();
                CargarCombosR(ROperacion, RAnio, RMes);
            });

            $("#Clientetop_Rentabilidad").click(function () {

                ValidarR();
                Mostar_GraficaR(RAnio, RMes, ROperacion, Ragrupacion);
            });
        });

        function CargarCombosR(ROperacion, RAnio, RMes) {
            if (ROperacion == 0 || ROperacion == "") fn_General_Cargar_FiltroROperacion(RAnio, RMes);
            if (RAnio == 0 || RAnio == "") fn_General_Cargar_FiltroRAnio(RMes, ROperacion);
            if (RMes == 0 || RMes == "") fn_General_Cargar_FiltroRMes(RAnio, ROperacion);
        }

        function ValidarR() {
            ROperacion = $("#cmbROperacion_general").val(); if (ROperacion != 0) { ROperacion = $("#cmbROperacion_general option:selected").text(); } else { ROperacion = ""; }
            RAnio = $("#cmbRAnio_General").val(); if (RAnio != 0) { RAnio = $("#cmbRAnio_General option:selected").text(); } else { RAnio = ""; }
            RMes = $("#cmbRMes_General").val(); if (RMes != 0) { RMes = $("#cmbRMes_General option:selected").text(); } else { RMes = ""; }
            if ($("#opc_triRMestre").prop('checked')) { Ragrupacion = "3"; }
            if ($("#opc_biRMestre").prop('checked')) { Ragrupacion = "2"; }
            if ($("#opc_biRMestre").prop('checked') == false && $("#opc_triRMestre").prop('checked') == false) { Ragrupacion = "0"; }
        }


        function EstadoR() {
            if (ROperacion != 0) $("#cmbROperacion_general").val(0); document.getElementById("cmbROperacion_general").disabled = false;
            if (RAnio != 0) $("#cmbRAnio_General").val(0); document.getElementById("cmbRAnio_General").disabled = false;
            if (RMes != 0) $("#cmbRMes_General").val(0); document.getElementById("cmbRMes_General").disabled = false;
            $("#opc_triRMestre").prop("checked", false);
            $("#opc_biRMestre").prop("checked", false);
            ValidarR();
            CargarCombosR(ROperacion, RAnio, RMes);
            Mostar_GraficaR("", "", "", "0");
        }

        function fn_General_Cargar_FiltroRMes(RAnio, ROperacion) {
            var html = '';
            var sucess = function (response) {
                var obj = JSON.parse(response.d);
                var data_html = "";
                if (obj == null) {
                    data_html = "<option value='0'>Seleccione Opción</option>";
                    $("select[id$=cmbRMes_General]").html(data_html);
                }
                else {
                    data_html += "<option value='0'>Seleccione Opción</option>";
                    for (var i = 0; i < obj.length; i++) {
                        data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>";
                    }
                    $("select[id$=cmbRMes_General]").html(data_html);
                }
            };
            var error = function (xhr, ajaxOptions, thrownError) {
            };
            fn_LlamadoMetodo("ClienteTop.aspx/Encomienda_ClientesTop_Rentabilidad_ddlMes_CS", JSON.stringify({ anio: RAnio, operacion: ROperacion }), sucess, error);
        }
        function fn_General_Cargar_FiltroRAnio(RMes, ROperacion) {
            var html = '';
            var sucess = function (response) {
                var obj = JSON.parse(response.d);
                var data_html = "";
                if (obj == null) {
                    data_html = "<option value='0'>Seleccione Opción</option>";
                    $("select[id$=cmbRAnio_General]").html(data_html);
                }
                else {
                    data_html += "<option value='0'>Seleccione Opción</option>";
                    for (var i = 0; i < obj.length; i++) {
                        data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>";
                    }
                    $("select[id$=cmbRAnio_General]").html(data_html);
                }
            };
            var error = function (xhr, ajaxOptions, thrownError) {
            };
            fn_LlamadoMetodo("ClienteTop.aspx/Encomienda_ClientesTop_Rentabilidad_ddlAño_CS", JSON.stringify({ mes: RMes, operacion: ROperacion }), sucess, error);
        }
        function fn_General_Cargar_FiltroROperacion(RAnio, RMes) {
            var html = '';
            var sucess = function (response) {
                var obj = JSON.parse(response.d);
                var data_html = "";
                if (obj == null) {
                    data_html = "<option value='0'>Seleccione Opción</option>";
                    $("select[id$=cmbROperacion_general]").html(data_html);
                }
                else {
                    data_html += "<option value='0'>Seleccione Opción</option>";
                    for (var i = 0; i < obj.length; i++) {
                        data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>";
                    }
                    $("select[id$=cmbROperacion_general]").html(data_html);
                }
            };
            var error = function (xhr, ajaxOptions, thrownError) {
            };
            fn_LlamadoMetodo("ClienteTop.aspx/Encomienda_ClientesTop_Rentabilidad_ddlOperacion_CS", JSON.stringify({ anio: RAnio, mes: RMes }), sucess, error);
        }
        function Mostar_GraficaR(RAnio, RMes, ROperacion, Ragrupacion) {
            var sucess = function (response) {
                BarrasChartR(response.d.Fecha, response.d.Name, response.d.Series);
                $('#Rentabilidad').show();
            };
            var error = function (xhr, ajaxOptions, thrownError) {
            };
            fn_LlamadoMetodo("ClienteTop.aspx/G_Encomienda_ClientesTop_Rentabilidad_CS", JSON.stringify({ anio: RAnio, mes: RMes, operacion: ROperacion, agrupacionF: Ragrupacion }), sucess, error);
        }

        function BarrasChartR(fecha, Name, data) {
            Highcharts.chart('Rentabilidad', {
                chart: {
                    type: 'column',
                    inverted: true
                },
                title: {
                    text: 'SEGUN RENTABILIDAD'
                },
                xAxis: {
                    categories: JSON.parse(fecha)
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'IMPORTE TOTAL '
                    },

                    stackLabels: {
                        enabled: true,
                        style: {
                            fontWeight: 'bold',
                            color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                        }
                    }
                },
                legend: {
                    borderWidth: 1,
                    itemDistance: 5,
                    x: -30,
                    verticalAlign: 'top',
                    y: 10,
                    floating: true,
                    backgroundColor: '#FCFFC5',
                    backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
                    borderColor: '#CCC',
                    borderWidth: 1,
                    shadow: false
                },
                tooltip: {
                    headerFormat: '<b>{point.x}</b><br/>',
                    pointFormat: '{series.name}: {point.y:.1f} <br/>Total: {point.stackTotal:.1f} '
                },
                plotOptions: {
                    column: {
                        stacking: 'normal',

                        pointFormat: "Value: {point.y:,.1f} mm"

                    }
                },
                series: JSON.parse(data)

            });
        }

        </script>
</asp:Content>
