﻿using ENTIDAD.Graficas;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Script.Serialization;
using System.Web.Services;

namespace WebApplication1.Modulo.Area.Encomienda
{
    public partial class VentaSucursal : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        #region Ventas por Sucursal
        public static String ConvertMes(String mes)
        {
            if (mes == "Enero") { mes = "1"; }
            if (mes == "Febrero") { mes = "2"; }
            if (mes == "Marzo") { mes = "3"; }
            if (mes == "Abril") { mes = "4"; }
            if (mes == "Mayo") { mes = "5"; }
            if (mes == "Junio") { mes = "6"; }
            if (mes == "Julio") { mes = "7"; }
            if (mes == "Agosto") { mes = "8"; }
            if (mes == "Septiembre") { mes = "9"; }
            if (mes == "Octubre") { mes = "10"; }
            if (mes == "Noviembre") { mes = "11"; }
            if (mes == "Diciembre") { mes = "12"; }
            return mes;
        }
        [WebMethod]
        public static object Encomienda_VentaSucursal_GeneralMensual_ddlTipo_CS(String mes)
        {
            String GMes;
            GMes = ConvertMes(mes);
            List<String> Tipo = new List<String>();
            JavaScriptSerializer sr = new JavaScriptSerializer();
            DataTable filtro_Tipo = LOGICA.EncomiendaL.Instancia.Encomienda_VentaSucursal_GeneralMensual_ddlTipo_LO(GMes);
            if (filtro_Tipo != null)
            {
                for (int i = 0; i < filtro_Tipo.Rows.Count; i++)
                {
                    if ((filtro_Tipo.Rows[i]["Tipo"].ToString()) == "NULL")
                    {
                        Tipo.Add("NO INDICA");
                    }
                    else
                    {
                        Tipo.Add(filtro_Tipo.Rows[i]["Tipo"].ToString());
                    }

                }
            }
            else { return null; }
            return sr.Serialize(Tipo);
        }
        [WebMethod]
        public static object Encomienda_VentaSucursal_GeneralMensual_ddlMes_CS(String tipo)
        {
            List<String> Mes = new List<String>();
            JavaScriptSerializer sr = new JavaScriptSerializer();
            DataTable filtro_Mes = LOGICA.EncomiendaL.Instancia.Encomienda_VentaSucursal_GeneralMensual_ddlMes_LO(tipo);
            if (filtro_Mes != null)
            {
                for (int i = 0; i < filtro_Mes.Rows.Count; i++)
                {
                    if ((filtro_Mes.Rows[i]["Meses"].ToString()) == "NULL")
                    {
                        Mes.Add("NO INDICA");
                    }
                    else
                    {
                        Mes.Add(filtro_Mes.Rows[i]["Meses"].ToString());
                    }

                }
            }
            else { return null; }
            return sr.Serialize(Mes);
        }
        [WebMethod]
        public static object G_Encomienda_VentaSucursal_GeneralMensual_CS(String mes, String tipo ,String agrupacionF)
        {
            String GMes;
            GMes = ConvertMes(mes);
            int agrupacion = Convert.ToInt32(agrupacionF);
            List<Object> contenedor = new List<Object>();
            List<String> Columnas = new List<String>(); //nombre de marca
            List<Ratio> series = new List<Ratio>();
            DataTable dt = LOGICA.EncomiendaL.Instancia.G_Encomienda_VentaSucursal_GeneralMensual_LO(GMes, tipo);
            LineChart data = new LineChart();
            Decimal val;
            if (dt != null)
            {
                foreach (DataColumn dataColumn in dt.Columns)
                {
                    Columnas.Add(dataColumn.ColumnName);
                }
                int j = 1;

                foreach (DataRow dr in dt.Rows)
                {
                    j = 1;
                    data.Fecha.Add(dr[0].ToString());
                    data.Name = Columnas;

                    while (j < Columnas.Count)
                    {
                        if (data.Series.Count < Columnas.Count - 1)
                            data.Series.Add(new Ratio());
                        string value = dr[j].ToString();
                        val = Convert.ToDecimal(value);
                        value = ((Convert.ToDecimal(value)) / 1000).ToString("N1");
                        data.Series[j - 1].data.Add(Convert.ToDecimal(value));
                        data.Series[j - 1].name = Columnas[j];
                        j++;
                    }
                }
                if (agrupacion == 0)
                {
                    JavaScriptSerializer sr = new JavaScriptSerializer();
                    return new { Fecha = sr.Serialize(data.Fecha), Name = sr.Serialize(data.Name), Series = sr.Serialize(data.Series), };
                }
            }
            //trimestre and bmestre
            String fechaAnio = "";
            Decimal acumulador = 0;
            Decimal total = 0;
            int contadorTrimestre = 1;
            String anioActual = "";
            String nombreAgrupacion = "";
            LineChart data1 = new LineChart();
            if (agrupacion != 0)
            {
                if (agrupacion == 2) { nombreAgrupacion = "Bimestre "; }
                if (agrupacion == 3) { nombreAgrupacion = "Trimestre "; }
                for (int m = 0; m < data.Fecha.Count; m++)
                {


                    if (fechaAnio != anioActual) { contadorTrimestre = 1; }
                    if (m % agrupacion == 0)
                    {
                        data1.Fecha.Add(nombreAgrupacion + (contadorTrimestre).ToString());
                        contadorTrimestre++;
                    }
                    else
                    {
                        if (m == data.Fecha.Count - 1)
                        {
                            data1.Fecha.Add(nombreAgrupacion + (contadorTrimestre).ToString());
                            contadorTrimestre++;
                        }
                    }

                }
                int contadorObjeto = 0;
                for (int i = 0; i < data.Series.Count; i++)
                {
                    data1.Series.Add(new Ratio());
                    for (int j = 0; j < data.Series[i].data.Count; j++)
                    {
                        contadorObjeto = j + 1;
                        var value = data.Series[i].data[j];
                        acumulador = acumulador + value;
                        if (contadorObjeto % agrupacion == 0 && contadorObjeto != 0)
                        {
                            total = acumulador / agrupacion;
                            data1.Series[i].data.Add(Math.Round(total, 1));
                            acumulador = 0;
                            total = 0;
                        }
                        else
                        {
                            if (contadorObjeto == data.Series[i].data.Count)
                            {
                                total = acumulador / agrupacion;
                                data1.Series[i].data.Add(Math.Round(total, 1));
                                acumulador = 0;
                                total = 0;
                            }
                        }

                    }
                    data1.Series[i].name = Columnas[i + 1];
                }
                JavaScriptSerializer sr = new JavaScriptSerializer();
                return new { Fecha = sr.Serialize(data1.Fecha), Name = sr.Serialize(data.Name), Series = sr.Serialize(data1.Series) };
            }
            else { return null; }
        }
        //GIAMPIERE 23/ 08 / 2019
        //General ORIGEN
       
        [WebMethod]
        public static object Encomienda_VentaSucursal_GeneralOrigen_ddlTipo_CS(String anio,String mes)
        {
            String EOMes;
            EOMes = ConvertMes(mes);
            List<String> Tipo = new List<String>();
            JavaScriptSerializer sr = new JavaScriptSerializer();
            DataTable filtro_Tipo = LOGICA.EncomiendaL.Instancia.Encomienda_VentaSucursal_GeneralOrigen_ddlTipo_LO(anio, EOMes);
            if (filtro_Tipo != null)
            {
                for (int i = 0; i < filtro_Tipo.Rows.Count; i++)
                {
                    if ((filtro_Tipo.Rows[i]["Tipo"].ToString()) == "NULL")
                    {
                        Tipo.Add("NO INDICA");
                    }
                    else
                    {
                        Tipo.Add(filtro_Tipo.Rows[i]["Tipo"].ToString());
                    }

                }
            }
            else { return null; }
            return sr.Serialize(Tipo);
        }
        [WebMethod]
        public static object Encomienda_VentaSucursal_GeneralOrigen_ddlMes_CS(String anio,String tipo)
        {
            List<String> Mes = new List<String>();
            JavaScriptSerializer sr = new JavaScriptSerializer();
            DataTable filtro_Mes = LOGICA.EncomiendaL.Instancia.Encomienda_VentaSucursal_GeneralOrigen_ddlMes_LO(anio,tipo);
            if (filtro_Mes != null)
            {
                for (int i = 0; i < filtro_Mes.Rows.Count; i++)
                {
                    if ((filtro_Mes.Rows[i]["Meses"].ToString()) == "NULL")
                    {
                        Mes.Add("NO INDICA");
                    }
                    else
                    {
                        Mes.Add(filtro_Mes.Rows[i]["Meses"].ToString());
                    }

                }
            }
            else { return null; }
            return sr.Serialize(Mes);
        }
        [WebMethod]
        public static object Encomienda_VentaSucursal_GeneralOrigen_ddlAño_CS(String mes, String tipo)
        {
            String EOMes;
            EOMes = ConvertMes(mes);
            List<String> Anio = new List<String>();
            JavaScriptSerializer sr = new JavaScriptSerializer();
            DataTable filtro_Anio = LOGICA.EncomiendaL.Instancia.Encomienda_VentaSucursal_GeneralOrigen_ddlAño_LO(EOMes, tipo);
            if (filtro_Anio != null)
            {
                for (int i = 0; i < filtro_Anio.Rows.Count; i++)
                {
                    if ((filtro_Anio.Rows[i]["Anio"].ToString()) == "NULL")
                    {
                        Anio.Add("NO INDICA");
                    }
                    else
                    {
                        Anio.Add(filtro_Anio.Rows[i]["Anio"].ToString());
                    }

                }
            }
            else { return null; }
            return sr.Serialize(Anio);
        }
        [WebMethod]
        public static object G_Encomienda_VentaSucursal_GeneralOrigen_CS(String anio, String mes, String tipo, String agrupacionF)
        {
            String EOMes;
            EOMes = ConvertMes(mes);
            int agrupacion = Convert.ToInt32(agrupacionF);
            List<Object> contenedor = new List<Object>();
            List<String> Columnas = new List<String>();
            List<String> Col = new List<String>();
            List<Ratio> series = new List<Ratio>();
            DataTable dt = LOGICA.EncomiendaL.Instancia.G_Encomienda_VentaSucursal_GeneralOrigen_LO(anio, EOMes, tipo);
            DataTable tri = LOGICA.EncomiendaL.Instancia.Encomienda_VentaSucursal_GeneralOrigen_AgrupacionTrimestre_LO(anio,tipo);
            DataTable bi = LOGICA.EncomiendaL.Instancia.Encomienda_VentaSucursal_GeneralOrigen_AgrupacionBimestre_LO(anio,tipo);
            LineChart data = new LineChart();
            Ratio ratio = new Ratio();

            if (dt != null)
            {
                if (agrupacion == 0)
                {
                    foreach (DataColumn dataColumn in dt.Columns)
                    {
                       Columnas.Add(dataColumn.ColumnName);
                    }
                    int j = 1;
                    foreach (DataRow dr in dt.Rows)
                    {
                        j = 1;
                       
                        data.Fecha.Add(dr[0].ToString());
                        data.Name = Columnas;
                        while (j < Columnas.Count-1)
                        {
                            if (data.Series.Count < Columnas.Count - 2)
                                data.Series.Add(new Ratio());
                            string value = dr[j].ToString();
                            value = ((Convert.ToDecimal(value))/ 1000).ToString("N1");
                            data.Series[j - 1].data.Add(Convert.ToDecimal(value));
                                data.Series[j - 1].name = Columnas[j];
                                j++;
                          
                        
                         }
                    }
                }
                if (agrupacion == 2)
                {
                    foreach (DataColumn dataColumn in bi.Columns)
                    {
                        Col.Add(dataColumn.ColumnName);
                    }
                    int j = 1;

                    foreach (DataRow dr in bi.Rows)
                    {
                        j = 1;
                        data.Fecha.Add(dr[0].ToString());
                        data.Name = Col;
                        while (j < Col.Count-1)
                        {
                            if (data.Series.Count < Col.Count - 2)
                                data.Series.Add(new Ratio());
                            string value = dr[j].ToString();
                            value = ((Convert.ToDecimal(value)) / 1000).ToString("N1");
                            data.Series[j - 1].data.Add(Convert.ToDecimal(value));
                            data.Series[j - 1].name = Col[j];
                            j++;
                        }
                    }
                }
                if (agrupacion == 3)
                {
                    foreach (DataColumn dataColumn in tri.Columns)
                    {
                        Col.Add(dataColumn.ColumnName);
                    }
                    int j = 1;

                    foreach (DataRow dr in tri.Rows)
                    {
                        j = 1;
                        data.Fecha.Add(dr[0].ToString());
                        data.Name = Col;
                        while (j < Col.Count-1)
                        {
                            if (data.Series.Count < Col.Count - 2)
                                data.Series.Add(new Ratio());
                            string value = dr[j].ToString();
                            value = ((Convert.ToDecimal(value)) / 1000).ToString("N1");
                            data.Series[j - 1].data.Add(Convert.ToDecimal(value));
                            data.Series[j - 1].name = Col[j];
                            j++;
                        }
                    }
                }
                JavaScriptSerializer sr = new JavaScriptSerializer();
                return new { Fecha = sr.Serialize(data.Fecha), Name = sr.Serialize(data.Name), Series = sr.Serialize(data.Series), };
            }
            else { return null; }

        }

        //GIAMPIERE 26/ 08 / 2019
        //General Mensual por Origen
        [WebMethod]
        public static object Encomienda_VentaSucursal_GeneralMensualOrigen_ddlTipo_CS(String mes, String anio)
        {
            String EMOMes;
            EMOMes = ConvertMes(mes);
            List<String> Tipo = new List<String>();
            JavaScriptSerializer sr = new JavaScriptSerializer();
            DataTable filtro_Tipo = LOGICA.EncomiendaL.Instancia.Encomienda_VentaSucursal_GeneralMensualOrigen_ddlTipo_LO(EMOMes, anio);
            if (filtro_Tipo != null)
            {
                for (int i = 0; i < filtro_Tipo.Rows.Count; i++)
                {
                    if ((filtro_Tipo.Rows[i]["Tipo"].ToString()) == "NULL")
                    {
                        Tipo.Add("NO INDICA");
                    }
                    else
                    {
                        Tipo.Add(filtro_Tipo.Rows[i]["Tipo"].ToString());
                    }

                }
            }
            else { return null; }
            return sr.Serialize(Tipo);
        }
        [WebMethod]
        public static object Encomienda_VentaSucursal_GeneralMensualOrigen_ddlAño_CS(String mes, String tipo)
        {
            String EMOMes;
            EMOMes = ConvertMes(mes);
            List<String> Anio = new List<String>();
            JavaScriptSerializer sr = new JavaScriptSerializer();
            DataTable filtro_Anio = LOGICA.EncomiendaL.Instancia.Encomienda_VentaSucursal_GeneralMensualOrigen_ddlAño_LO(EMOMes, tipo);
            if (filtro_Anio != null)
            {
                for (int i = 0; i < filtro_Anio.Rows.Count; i++)
                {
                    if ((filtro_Anio.Rows[i]["Anio"].ToString()) == "NULL")
                    {
                        Anio.Add("NO INDICA");
                    }
                    else
                    {
                        Anio.Add(filtro_Anio.Rows[i]["Anio"].ToString());
                    }

                }
            }
            else { return null; }
            return sr.Serialize(Anio);
        }
        [WebMethod]
        public static object Encomienda_VentaSucursal_GeneralMensualOrigen_ddlMes_CS(String anio, String tipo)
        {
            List<String> Mes = new List<String>();
            JavaScriptSerializer sr = new JavaScriptSerializer();
            DataTable filtro_Mes = LOGICA.EncomiendaL.Instancia.Encomienda_VentaSucursal_GeneralMensualOrigen_ddlMes_LO(anio, tipo);
            if (filtro_Mes != null)
            {
                for (int i = 0; i < filtro_Mes.Rows.Count; i++)
                {
                    if ((filtro_Mes.Rows[i]["Meses"].ToString()) == "NULL")
                    {
                        Mes.Add("NO INDICA");
                    }
                    else
                    {
                        Mes.Add(filtro_Mes.Rows[i]["Meses"].ToString());
                    }

                }
            }
            else { return null; }
            return sr.Serialize(Mes);
        }
        [WebMethod]
        public static object G_Encomienda_VentaSucursal_GeneralMensualOrigen_CS(String anio, String mes, String tipo, String agrupacionF)
        {
            String EMOMes;
            EMOMes = ConvertMes(mes);
            int agrupacion = Convert.ToInt32(agrupacionF);
            Decimal totalNeto = 0;
            List<Object> contenedor = new List<Object>();
            List<String> Columnas = new List<String>(); //nombre de marca
            List<Ratio> series = new List<Ratio>();
            DataTable dt = LOGICA.EncomiendaL.Instancia.G_Encomienda_VentaSucursal_GeneralMensualOrigen_LO(anio, EMOMes, tipo);
            LineChart data = new LineChart();
            Ratio ratio = new Ratio();

            if (dt != null)
            {

                foreach (DataColumn dataColumn in dt.Columns)
                {    
                        Columnas.Add(dataColumn.ColumnName);
                }
                int j = 1;
                foreach (DataRow dr in dt.Rows)
                {
                    j = 1;
                    data.Fecha.Add(dr[0].ToString());
                    data.Name = Columnas;

                    while (j < Columnas.Count)
                    {
                        if (data.Series.Count < Columnas.Count - 1)
                            data.Series.Add(new Ratio());
                        string value = dr[j].ToString();
                        value = ((Convert.ToDecimal(value)) / 1000).ToString("N1");
                        data.Series[j - 1].data.Add(Convert.ToDecimal(value));
                        data.Series[j - 1].name = Columnas[j];
                        j++;
                    }
                }

                if (agrupacion == 0)
                {
                    JavaScriptSerializer sr = new JavaScriptSerializer();
                    return new { Fecha = sr.Serialize(data.Fecha), Name = sr.Serialize(data.Name), Series = sr.Serialize(data.Series), };
                }
            }
            //trimestre and bmestre
            String fechaAnio = "";
            Decimal acumulador = 0;
            Decimal total = 0;
            int contadorTrimestre = 1;
            String anioActual = "";
            String nombreAgrupacion = "";
            LineChart data1 = new LineChart();
            if (agrupacion != 0)
            {
                if (agrupacion == 2) { nombreAgrupacion = "Bimestre "; }
                if (agrupacion == 3) { nombreAgrupacion = "Trimestre "; }
                for (int m = 0; m < data.Fecha.Count; m++)
                {
                    String fecha = ((data.Fecha[m]).Substring((data.Fecha[m]).Length - 4));
                    if (anio != "") { fechaAnio = (anio).ToString(); }
                    else
                    {
                        fechaAnio = fecha;
                    }
                    if (fechaAnio != anioActual) { contadorTrimestre = 1; }
                    if (m % agrupacion == 0)
                    {
                        data1.Fecha.Add(nombreAgrupacion + (contadorTrimestre).ToString() + " del " + fechaAnio);
                        contadorTrimestre++;
                    }
                    else
                    {
                        if (m == data.Fecha.Count - 1)
                        {
                            data1.Fecha.Add(nombreAgrupacion + (contadorTrimestre).ToString() + " del " + fechaAnio);
                            contadorTrimestre++;
                        }
                    }
                    anioActual = fechaAnio;
                }
                int contadorObjeto = 0;
                for (int i = 0; i < data.Series.Count; i++)
                {
                    data1.Series.Add(new Ratio());
                    for (int j = 0; j < data.Series[i].data.Count; j++)
                    {
                        contadorObjeto = j + 1;
                        var value = data.Series[i].data[j];
                        acumulador = acumulador + value;
                        if (contadorObjeto % agrupacion == 0 && contadorObjeto != 0)
                        {
                            total = acumulador / agrupacion;
                            data1.Series[i].data.Add(Math.Round(total, 1));
                            acumulador = 0;
                            total = 0;
                        }
                        else
                        {
                            if (contadorObjeto == data.Series[i].data.Count)
                            {
                                total = acumulador / agrupacion;
                                data1.Series[i].data.Add(Math.Round(total, 1));
                                acumulador = 0;
                                total = 0;
                            }
                        }

                    }
                    data1.Series[i].name = Columnas[i + 1];
                }
                JavaScriptSerializer sr = new JavaScriptSerializer();
                return new { Fecha = sr.Serialize(data1.Fecha), Name = sr.Serialize(data.Name), Series = sr.Serialize(data1.Series) };
            }
            else { return null; }
        }
        #endregion
    }
}