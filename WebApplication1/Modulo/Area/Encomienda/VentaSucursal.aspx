﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Intranet.Master" AutoEventWireup="true" CodeBehind="VentaSucursal.aspx.cs" Inherits="WebApplication1.Modulo.Area.Encomienda.VentaSucursal" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContenedorDashboard" runat="server">


    <h3 class="panel-body" style="color: #283593; margin-top: 0px;">VENTA POR SUCURSAL</h3>

    <div class="text-left Left aligned text" style="margin-left: 18px;">
        <p><strong>Objetivo:</strong> El objetivo del indicador es mostrar la cantidad de ingresos de encomiendas y/o giros en general y en las distintas sucursales de la empresa.</p>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-headline">
                <div class="panel-body">
                    <div class="form-inline">

                        <div class="form-group col-md-2 col-md-3 col-md-6 col-md-6">
                            <label for="formGroupExampleInput">Tipo:</label>
                            <asp:DropDownList ID="cmbTipo_general" ClientIDMode="Static" class="form-control" runat="server" Style="width: 100%;"></asp:DropDownList>
                        </div>
                        <div class="form-group col-md-2 col-md-3 col-md-6 col-md-6">
                             <label for="formGroupExampleInput">Mes:</label>
                          <asp:DropDownList ID="cmbMes_General" ClientIDMode="Static" class="form-control" runat="server" Style="width: 100%;"></asp:DropDownList>
                        </div>
                        <div class="form-group col-md-2 col-md-3 col-md-6 col-md-6">
                            <label for="formGroupExampleInput">Agrupación:</label>
                            <label class="fancy-checkbox">
                                <input type="checkbox" id="opc_trimestre">
                                <span>Trimestre</span>
                            </label>
                            <label class="fancy-checkbox">
                                <input type="checkbox" id="opc_bimestre">
                                <span>Bimestre</span>
                            </label>
                            <br />
                        </div>

                        <div class="form-group col-md-2 col-md-3 col-md-6 col-md-6">
                            <label for="formGroupExampleInput">Limpiar:</label><br />
                            <span class="lnr lnr-magic-wand-limpiar" onclick="Cambiarestado()"></span>
                        </div>
                        <div class="form-group col-lg-2 col-sm-2 col-sm-2">
                            <br />
                            <button id="VentasSucursal_GeneralMensual" type="button" class="btn btn-primary btn-sm">GRAFICAR</button>
                        </div>

                    </div>
                </div>
            </div>
        </div>

       <div class="col-md-12">
          <div class="panel-body">
            <div id="GeneralMensual" style="min-width: 310px; height: 320px; margin: 0 auto; display: none"></div>
           </div>
        </div>
        <div class="col-md-12">
        <div class="text-left Left aligned text">
                <p><strong>Interpretacion:</strong> Se desea que las ventas aumenten año a año</p>
            </div>
            </div>
    </div>
    

  <div class="col-lg-6">
       <div class="row">
            <div class="col-lg-12">
             <div class="panel panel-headline">
                <div class="panel-body">
                    <div class="form-inline">

                         <div class="form-group col-lg-3 col-md-3 col-sm-3">
                            <label for="formGroupExampleInput">Tipo:</label>
                            <asp:DropDownList ID="cmbOTipo_general" ClientIDMode="Static" class="form-control" runat="server" Style="width: 100%;"></asp:DropDownList>
                        </div>
                          <div class="form-group col-lg-3 col-md-3 col-sm-3">
                            <label for="formGroupExampleInput">Año:</label>
                              <asp:DropDownList ID="cmbOAnio_General" ClientIDMode="Static" class="form-control" runat="server" Style="width: 100%;"></asp:DropDownList>
                        </div>
                         <div class="form-group col-lg-3 col-md-3 col-sm-3">
                            <label for="formGroupExampleInput">Mes:</label>
                           <asp:DropDownList ID="cmbOMes_General" ClientIDMode="Static" class="form-control" runat="server" Style="width: 100%;"></asp:DropDownList>
                        </div>

                         <div class="form-group col-lg-3 col-md-3 col-sm-3">
                            <label for="formGroupExampleInput">Agrupación:</label>
                            <label class="fancy-checkbox">
                                <input type="checkbox" id="opc_Otrimestre">
                                <span>Trimestre</span>
                            </label>
                            <label class="fancy-checkbox">
                                <input type="checkbox" id="opc_Obimestre">
                                <span>Bimestre</span>
                            </label>
                            <br />
                        </div>

                        <div class="form-group col-lg-3 col-md-3 col-sm-3">
                            <label for="formGroupExampleInput">Limpiar:</label>
                            <span class="lnr lnr-magic-wand-limpiar" onclick="Estado()"></span>
                        </div>
                        <div class="form-group col-lg-3 col-md-3 col-sm-3">
                            <button id="VentasSucursal_GeneralPorOrigen" type="button" class="btn btn-primary btn-sm">GRAFICAR</button>
                        </div>

                    </div>
                </div>
            </div>
        </div>


         <div class="col-lg-12">
          <div class="row panel-body">
            <div id="GeneralOrigen" style="min-width: 310px; height: 400px; margin: 0 auto; display: none"></div>
            </div>
              </div>
            <div class="col-lg-12">
             <div class="text-left Left aligned text">
                <p><strong>Interpretacion:</strong> Se desea que las ventas aumenten en cada sucursal</p>
            </div>
           </div>
        </div>
      </div>

  <div class="col-lg-6">
       <div class="row">
            <div class="col-lg-12">
             <div class="panel panel-headline">
                <div class="panel-body">
                    <div class="form-inline">
                          <div class="form-group col-lg-3 col-md-3 col-sm-3">
                            <label for="formGroupExampleInput">Tipo:</label>
                            <asp:DropDownList ID="cmbMOTipo_general" ClientIDMode="Static" class="form-control" runat="server" Style="width: 100%;"></asp:DropDownList>
                        </div>
                        <div class="form-group col-lg-3 col-md-3 col-sm-3">
                            <label for="formGroupExampleInput">Año:</label>
                            <asp:DropDownList ID="cmbMOAnio_General" ClientIDMode="Static" class="form-control" runat="server" Style="width: 100%;"></asp:DropDownList>
                        </div>
                         <div class="form-group col-lg-3 col-md-3 col-sm-3">
                            <label for="formGroupExampleInput">Mes:</label>
                          <asp:DropDownList ID="cmbMOMes_General" ClientIDMode="Static" class="form-control" runat="server" Style="width: 100%;"></asp:DropDownList>
                        </div>

                         <div class="form-group col-lg-3 col-md-3 col-sm-3">
                            <label for="formGroupExampleInput">Agrupación:</label>
                            <label class="fancy-checkbox">
                                <input type="checkbox" id="opc_MOtrimestre">
                                <span>Trimestre</span>
                            </label>
                            <label class="fancy-checkbox">
                                <input type="checkbox" id="opc_MObimestre">
                                <span>Bimestre</span>
                            </label>
                            <br />
                        </div>

                         <div class="form-group col-lg-3 col-md-3 col-sm-3">
                            <label for="formGroupExampleInput">Limpiar:</label>
                            <span class="lnr lnr-magic-wand-limpiar" onclick="Estado_MensualOrigen()"></span>
                        </div>
                        <div class="form-group col-lg-3 col-md-3 col-sm-3">
                            <button id="VentasSucursal_General_Mensual_PorOrigen" type="button" class="btn btn-primary btn-sm">GRAFICAR</button>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        
          <div class="col-lg-12">
          <div class="row panel-body">
            <div id="General_Mensual_Origen" style="min-width: 310px; height: 400px; margin: 0 auto; display: none"></div>
        </div>
              </div>
           <div class="col-lg-12">
             <div class="text-left Left aligned text">
                 <br />
                <p><strong>  <br />Interpretacion:</strong> Se desea que las ventas aumenten en cada sucursal</p>
            </div>
                </div>
        </div>

      </div>

    <script>

        var Tipo = ""; var Mes = ""; var agrupacion = "0";
        fn_CargarCombos(Tipo, Mes);
        Cargar_Grafica("", "", "0");
        $(document).ready(function () {

            $("#opc_trimestre").change(function () {
                $("#opc_bimestre").prop("checked", false);
                ValidarCampo();
                Cargar_Grafica(Mes, Tipo, agrupacion);
            });

            $("#opc_bimestre").change(function () {
                $("#opc_trimestre").prop("checked", false);
                ValidarCampo();
                Cargar_Grafica( Mes, Tipo, agrupacion);
            });

            $("#cmbTipo_general").change(function () {
                ValidarCampo();
                fn_CargarCombos(Tipo, Mes);
            });
            $("#cmbMes_General").change(function () {
                ValidarCampo();
                fn_CargarCombos(Tipo, Mes);
            });
            $("#VentasSucursal_GeneralMensual").click(function () {
                ValidarCampo();
                Cargar_Grafica(Mes, Tipo, agrupacion);
            });
        });
        function fn_CargarCombos(Tipo, Mes) {
            if (Tipo == 0 || Tipo == "") fn_General_Cargar_Filtrotipo(Mes);
            if (Mes == 0 || Mes == "") fn_General_Cargar_FiltroMes(Tipo);
        }
        function ValidarCampo() {
            Tipo = $("#cmbTipo_general").val(); if (Tipo != 0) { Tipo = $("#cmbTipo_general option:selected").text(); } else { Tipo = ""; }
            Mes = $("#cmbMes_General").val(); if (Mes != 0) { Mes = $("#cmbMes_General option:selected").text(); } else { Mes = ""; }
            if ($("#opc_trimestre").prop('checked')) { agrupacion = "3"; }
            if ($("#opc_bimestre").prop('checked')) { agrupacion = "2"; }
            if ($("#opc_bimestre").prop('checked') == false && $("#opc_trimestre").prop('checked') == false) { agrupacion = "0"; }
        }


        function Cambiarestado() {
            if (Tipo != 0) $("#cmbTipo_general").val(0); document.getElementById("cmbTipo_general").disabled = false;
            if (Mes != 0) $("#cmbMes_General").val(0); document.getElementById("cmbMes_General").disabled = false;
            $("#opc_trimestre").prop("checked", false);
            $("#opc_bimestre").prop("checked", false);
            ValidarCampo();
            fn_CargarCombos(Tipo, Mes);
            Cargar_Grafica("", "", "0");
        }
        function fn_General_Cargar_FiltroMes(Tipo) {
            var html = '';
            var sucess = function (response) {
                var obj = JSON.parse(response.d);
                var data_html = "";
                if (obj == null) {
                    data_html = "<option value='0'>Seleccione Opción</option>";
                    $("select[id$=cmbMes_General]").html(data_html);
                }
                else {
                    data_html += "<option value='0'>Seleccione Opción</option>";
                    for (var i = 0; i < obj.length; i++) {
                        data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>";
                    }
                    $("select[id$=cmbMes_General]").html(data_html);
                }
            };
            var error = function (xhr, ajaxOptions, thrownError) {
            };
            fn_LlamadoMetodo("VentaSucursal.aspx/Encomienda_VentaSucursal_GeneralMensual_ddlMes_CS", JSON.stringify({ tipo: Tipo }), sucess, error);
        }

        function fn_General_Cargar_Filtrotipo(Mes) {
            var html = '';
            var sucess = function (response) {
                var obj = JSON.parse(response.d);
                var data_html = "";
                if (obj == null) {
                    data_html = "<option value='0'>Seleccione Opción</option>";
                    $("select[id$=cmbTipo_general]").html(data_html);
                }
                else {
                    data_html += "<option value='0'>Seleccione Opción</option>";
                    for (var i = 0; i < obj.length; i++) {
                        data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>";
                    }
                    $("select[id$=cmbTipo_general]").html(data_html);
                }
            };
            var error = function (xhr, ajaxOptions, thrownError) {
            };
            fn_LlamadoMetodo("VentaSucursal.aspx/Encomienda_VentaSucursal_GeneralMensual_ddlTipo_CS", JSON.stringify({ mes: Mes }), sucess, error);
        }
        function Cargar_Grafica( Mes, Tipo, agrupacion) {
            var sucess = function (response) {
                json = response.d.Series
                LineChart(response.d.Fecha, response.d.Name, fn_ValidarANulos(json) );
                $('#GeneralMensual').show();

            };
            var error = function (xhr, ajaxOptions, thrownError) {
                //fn_message('e', 'Ocurrio un error mientras se cargaban los datos.');
            };
            fn_LlamadoMetodo("VentaSucursal.aspx/G_Encomienda_VentaSucursal_GeneralMensual_CS", JSON.stringify({  mes: Mes, tipo: Tipo, agrupacionF: agrupacion }), sucess, error);
        }


        function LineChart(fecha, Name, data) {
            Highcharts.chart('GeneralMensual', {
                chart: {
                    type: 'line'
                },
                title: {
                    text: 'GENERAL MENSUAL'
                },
                subtitle: {
                    text: 'MESES'
                },
                xAxis: {
                    categories: JSON.parse(fecha)
                },
                yAxis: {
                    min: 0,


                    title: {
                        text: 'IMPORTE TOTAL NETO'
                    },
                    labels: {
                        formatter: function () {
                            return (this.value) + ' Mil';
                        }
                    },
                },
                tooltip: {
                    headerFormat: '<b>{point.x}</b><br/>',
                    pointFormat: '{series.name}: {point.y:.1f} Mil'
                },
                plotOptions: {
                    line: {
                        dataLabels: {
                            enabled: true
                        },
                        enableMouseTracking: true
                    }
                },
                series: JSON.parse(data)
            });
        }

        //General por origen

        var OTipo = ""; var OAnio = ""; var OMes = ""; var Oagrupacion = "0"; 
        CargarCombos(OTipo, OAnio, OMes);
        Mostar_Grafica("", "", "", "0");
        $(document).ready(function () {

            $("#opc_Otrimestre").change(function () {
                $("#opc_Obimestre").prop("checked", false);
                Validar();
                Mostar_Grafica(OAnio, OMes, OTipo, Oagrupacion);
            });

            $("#opc_Obimestre").change(function () {
                $("#opc_Otrimestre").prop("checked", false);
                Validar();
                Mostar_Grafica(OAnio, OMes, OTipo, Oagrupacion);
            });

            $("#cmbOTipo_general").change(function () {
                Validar();
                CargarCombos(OTipo, OAnio, OMes);
            });
            $("#cmbOMes_General").change(function () {
                Validar();
                CargarCombos(OTipo, OAnio, OMes);
            });

            $("#cmbOAnio_General").change(function () {
                Validar();
                CargarCombos(OTipo, OAnio, OMes);
            });

            $("#VentasSucursal_GeneralPorOrigen").click(function () {

                Validar();
                Mostar_Grafica(OAnio, OMes, OTipo, Oagrupacion);
            });
        });

        function CargarCombos(OTipo, OAnio, OMes) {
            if (OTipo == 0 || OTipo == "") fn_General_Cargar_FiltroOTipo(OAnio,OMes);
            if (OAnio == 0 || OAnio == "") fn_General_Cargar_FiltroOAnio(OMes, OTipo);
            if (OMes == 0 || OMes == "") fn_General_Cargar_FiltroOMes(OAnio, OTipo);
        }

        function Validar() {
            OTipo = $("#cmbOTipo_general").val(); if (OTipo != 0) { OTipo = $("#cmbOTipo_general option:selected").text(); } else { OTipo = ""; }
            OAnio = $("#cmbOAnio_General").val(); if (OAnio != 0) { OAnio = $("#cmbOAnio_General option:selected").text(); } else { OAnio = ""; }
            OMes = $("#cmbOMes_General").val(); if (OMes != 0) { OMes = $("#cmbOMes_General option:selected").text(); } else { OMes = ""; }
            if ($("#opc_Otrimestre").prop('checked')) { Oagrupacion = "3"; }
            if ($("#opc_Obimestre").prop('checked')) { Oagrupacion = "2"; }
            if ($("#opc_Obimestre").prop('checked') == false && $("#opc_Otrimestre").prop('checked') == false) { Oagrupacion = "0"; }
        }


        function Estado() {
            if (OTipo != 0) $("#cmbOTipo_general").val(0); document.getElementById("cmbOTipo_general").disabled = false;
            if (OAnio != 0) $("#cmbOAnio_General").val(0); document.getElementById("cmbOAnio_General").disabled = false;
            if (OMes != 0) $("#cmbOMes_General").val(0); document.getElementById("cmbOMes_General").disabled = false;
            $("#opc_Otrimestre").prop("checked", false);
            $("#opc_Obimestre").prop("checked", false);
            Validar();
            CargarCombos(OTipo, OAnio, OMes);
            Mostar_Grafica("", "", "", "0");
        }

        function fn_General_Cargar_FiltroOMes(OAnio, OTipo) {
            var html = '';
            var sucess = function (response) {
                var obj = JSON.parse(response.d);
                var data_html = "";
                if (obj == null) {
                    data_html = "<option value='0'>Seleccione Opción</option>";
                    $("select[id$=cmbOMes_General]").html(data_html);
                }
                else {
                    data_html += "<option value='0'>Seleccione Opción</option>";
                    for (var i = 0; i < obj.length; i++) {
                        data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>";
                    }
                    $("select[id$=cmbOMes_General]").html(data_html);
                }
            };
            var error = function (xhr, ajaxOptions, thrownError) {
            };
            fn_LlamadoMetodo("VentaSucursal.aspx/Encomienda_VentaSucursal_GeneralOrigen_ddlMes_CS", JSON.stringify({ anio: OAnio, tipo: OTipo }), sucess, error);
        }
        function fn_General_Cargar_FiltroOAnio(OMes, OTipo) {
            var html = '';
            var sucess = function (response) {
                var obj = JSON.parse(response.d);
                var data_html = "";
                if (obj == null) {
                    data_html = "<option value='0'>Seleccione Opción</option>";
                    $("select[id$=cmbOAnio_General]").html(data_html);
                }
                else {
                    data_html += "<option value='0'>Seleccione Opción</option>";
                    for (var i = 0; i < obj.length; i++) {
                        data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>";
                    }
                    $("select[id$=cmbOAnio_General]").html(data_html);
                }
            };
            var error = function (xhr, ajaxOptions, thrownError) {
            };
            fn_LlamadoMetodo("VentaSucursal.aspx/Encomienda_VentaSucursal_GeneralOrigen_ddlAño_CS", JSON.stringify({ mes: OMes, tipo: OTipo }), sucess, error);
        }
        function fn_General_Cargar_FiltroOTipo(OAnio, OMes) {
            var html = '';
            var sucess = function (response) {
                var obj = JSON.parse(response.d);
                var data_html = "";
                if (obj == null) {
                    data_html = "<option value='0'>Seleccione Opción</option>";
                    $("select[id$=cmbOTipo_general]").html(data_html);
                }
                else {
                    data_html += "<option value='0'>Seleccione Opción</option>";
                    for (var i = 0; i < obj.length; i++) {
                        data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>";
                    }
                    $("select[id$=cmbOTipo_general]").html(data_html);
                }
            };
            var error = function (xhr, ajaxOptions, thrownError) {
            };
            fn_LlamadoMetodo("VentaSucursal.aspx/Encomienda_VentaSucursal_GeneralOrigen_ddlTipo_CS", JSON.stringify({ anio: OAnio, mes: OMes }), sucess, error);
        }
        function Mostar_Grafica(OAnio, OMes, OTipo, Oagrupacion) {
            var sucess = function (response) {
                BarrasChart(response.d.Fecha, response.d.Name, response.d.Series);
                $('#GeneralOrigen').show();
            };
            var error = function (xhr, ajaxOptions, thrownError) {
            };
            fn_LlamadoMetodo("VentaSucursal.aspx/G_Encomienda_VentaSucursal_GeneralOrigen_CS", JSON.stringify({ anio: OAnio, mes: OMes, tipo: OTipo, agrupacionF: Oagrupacion}), sucess, error);
        }

        function BarrasChart(fecha, Name, data) {
            Highcharts.chart('GeneralOrigen', {
                chart: {
                    type: 'column',
                    inverted: true
                },
                title: {
                    text: 'GENERAL POR ORIGEN'
                },
                xAxis: {
                    categories: JSON.parse(fecha)
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Total Neto '
                    },
                    labels: {
                        formatter: function () {
                            return (this.value) + ' Mil';
                        }
                    },
                    stackLabels: {
                        enabled: true,
                        style: {
                            fontWeight: 'bold',
                            color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                        }
                    }
                },
                legend: {
                    borderWidth: 1,
                    itemDistance: 5,
                    x: -30,
                    verticalAlign: 'top',
                    y: 10,
                    floating: true,
                    backgroundColor: '#FCFFC5',
                    backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
                    borderColor: '#CCC',
                    borderWidth: 1,
                    shadow: false
                },
                tooltip: {
                    headerFormat: '<b>{point.x}</b><br/>',
                    pointFormat: '{series.name}: {point.y:.1f} Mil<br/>Total: {point.stackTotal:.1f} Mil'
                },
                plotOptions: {
                    column: {
                        stacking: 'normal',

                        pointFormat: "Value: {point.y:,.1f} mm"

                    }
                },
                series: JSON.parse(data)

            });
        }


        //General Mensual por Origen

        var MOTipo = ""; var MOAnio = ""; var MOMes = ""; var MOagrupacion = "0"; 
        CargarCombos_MensualOrigen(MOTipo, MOAnio, MOMes);
        Mostrar_Grafica_MensualOrigen("2019", "", "", "0");
        $(document).ready(function () {

            $("#opc_MOtrimestre").change(function () {
                $("#opc_MObimestre").prop("checked", false);
                Validar_MensualOrigen();
                Mostrar_Grafica_MensualOrigen(MOAnio, MOMes, MOTipo, MOagrupacion);
            });

            $("#opc_MObimestre").change(function () {
                $("#opc_MOtrimestre").prop("checked", false);
                Validar_MensualOrigen();
                Mostrar_Grafica_MensualOrigen(MOAnio, MOMes, MOTipo, MOagrupacion);
            });

            $("#cmbMOTipo_general").change(function () {
                Validar_MensualOrigen();
                CargarCombos_MensualOrigen(MOTipo, MOAnio, MOMes);
            });
            $("#cmbMOMes_General").change(function () {
                Validar_MensualOrigen();
                CargarCombos_MensualOrigen(MOTipo, MOAnio, MOMes);
            });

            $("#cmbMOAnio_General").change(function () {
                Validar_MensualOrigen();
                CargarCombos_MensualOrigen(MOTipo, MOAnio, MOMes);
            });

            $("#VentasSucursal_General_Mensual_PorOrigen").click(function () {

                Validar_MensualOrigen();
                Mostrar_Grafica_MensualOrigen(MOAnio, MOMes, MOTipo, MOagrupacion);
            });
        });

        function CargarCombos_MensualOrigen(MOTipo, MOAnio, MOMes) {
            if (MOTipo == 0 || MOTipo == "") fn_General_Cargar_FiltroMOTipo(MOMes, MOAnio);
            if (MOAnio == 0 || MOAnio == "") fn_General_Cargar_FiltroMOAnio(MOMes, MOTipo);
            if (MOMes == 0 || MOMes == "") fn_General_Cargar_FiltroMOMes(MOAnio, MOTipo);
        }

        function Validar_MensualOrigen() {
            MOTipo = $("#cmbMOTipo_general").val(); if (MOTipo != 0) { MOTipo = $("#cmbMOTipo_general option:selected").text(); } else { MOTipo = ""; }
            MOAnio = $("#cmbMOAnio_General").val(); if (MOAnio != 0) { MOAnio = $("#cmbMOAnio_General option:selected").text(); } else { MOAnio = ""; }
            MOMes = $("#cmbMOMes_General").val(); if (MOMes != 0) { MOMes = $("#cmbMOMes_General option:selected").text(); } else { MOMes = ""; }
            if ($("#opc_MOtrimestre").prop('checked')) { MOagrupacion = "3"; }
            if ($("#opc_MObimestre").prop('checked')) { MOagrupacion = "2"; }
            if ($("#opc_MObimestre").prop('checked') == false && $("#opc_MOtrimestre").prop('checked') == false) { MOagrupacion = "0"; }
        }
        function Estado_MensualOrigen() {
            if (MOTipo != 0) $("#cmbMOTipo_general").val(0); document.getElementById("cmbMOTipo_general").disabled = false;
            if (MOAnio != 0) $("#cmbMOAnio_General").val(0); document.getElementById("cmbMOAnio_General").disabled = false;
            if (MOMes != 0) $("#cmbMOMes_General").val(0); document.getElementById("cmbMOMes_General").disabled = false;
            $("#opc_MOtrimestre").prop("checked", false);
            $("#opc_MObimestre").prop("checked", false);
            Validar_MensualOrigen();
            CargarCombos_MensualOrigen(MOTipo, MOAnio, MOMes);
            Mostrar_Grafica_MensualOrigen("", "", "", "0");
        }
        function fn_General_Cargar_FiltroMOMes(MOAnio, MOTipo) {
            var html = '';
            var sucess = function (response) {
                var obj = JSON.parse(response.d);
                var data_html = "";
                if (obj == null) {
                    data_html = "<option value='0'>Seleccione Opción</option>";
                    $("select[id$=cmbMOMes_General]").html(data_html);
                }
                else {
                    data_html += "<option value='0'>Seleccione Opción</option>";
                    for (var i = 0; i < obj.length; i++) {
                        data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>";
                    }
                    $("select[id$=cmbMOMes_General]").html(data_html);
                }
            };
            var error = function (xhr, ajaxOptions, thrownError) {
            };
            fn_LlamadoMetodo("VentaSucursal.aspx/Encomienda_VentaSucursal_GeneralMensualOrigen_ddlMes_CS", JSON.stringify({ anio: MOAnio, tipo: MOTipo }), sucess, error);
        }
        function fn_General_Cargar_FiltroMOAnio(MOMes, MOTipo) {
            var html = '';
            var sucess = function (response) {
                var obj = JSON.parse(response.d);
                var data_html = "";
                if (obj == null) {
                    data_html = "<option value='0'>Seleccione Opción</option>";
                    $("select[id$=cmbMOAnio_General]").html(data_html);
                }
                else {
                    data_html += "<option value='0'>Seleccione Opción</option>";
                    for (var i = 0; i < obj.length; i++) {
                        data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>";
                    }
                    $("select[id$=cmbMOAnio_General]").html(data_html);
                }
            };
            var error = function (xhr, ajaxOptions, thrownError) {
            };
            fn_LlamadoMetodo("VentaSucursal.aspx/Encomienda_VentaSucursal_GeneralMensualOrigen_ddlAño_CS", JSON.stringify({ mes: MOMes, tipo: MOTipo }), sucess, error);
        }
        function fn_General_Cargar_FiltroMOTipo(MOMes, MOAnio) {
            var html = '';
            var sucess = function (response) {
                var obj = JSON.parse(response.d);
                var data_html = "";
                if (obj == null) {
                    data_html = "<option value='0'>Seleccione Opción</option>";
                    $("select[id$=cmbMOTipo_general]").html(data_html);
                }
                else {
                    data_html += "<option value='0'>Seleccione Opción</option>";
                    for (var i = 0; i < obj.length; i++) {
                        data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>";
                    }
                    $("select[id$=cmbMOTipo_general]").html(data_html);
                }
            };
            var error = function (xhr, ajaxOptions, thrownError) {
            };
            fn_LlamadoMetodo("VentaSucursal.aspx/Encomienda_VentaSucursal_GeneralMensualOrigen_ddlTipo_CS", JSON.stringify({ mes: MOMes, anio: MOAnio }), sucess, error);
        }
        function Mostrar_Grafica_MensualOrigen(MOAnio, MOMes, MOTipo, MOagrupacion) {
            var sucess = function (response) {
               
                BarrasChart_Mensual_Origen(response.d.Fecha, response.d.Name, response.d.Series);
                $('#General_Mensual_Origen').show();


            };
            var error = function (xhr, ajaxOptions, thrownError) {
            };
            fn_LlamadoMetodo("VentaSucursal.aspx/G_Encomienda_VentaSucursal_GeneralMensualOrigen_CS", JSON.stringify({ anio: MOAnio, mes: MOMes, tipo: MOTipo, agrupacionF: MOagrupacion}), sucess, error);
        }

        function BarrasChart_Mensual_Origen(fecha, Name, data) {
            Highcharts.chart('General_Mensual_Origen', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'GENERAL MENSUAL POR ORIGEN'
                },
                xAxis: {
                    categories: JSON.parse(fecha)
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Total Neto '
                    },
                    labels: {
                        formatter: function () {
                            return (this.value) + ' Mil';
                        }
                    },
                    stackLabels: {
                        enabled: true,
                        
                    }
                },
                legend: {
                    itemDistance: 10,
                     borderColor: '#CCC',
                    navigation: {
                        activeColor: '#3E576F',
                        animation: true,
                    },
                },
                tooltip: {
                    headerFormat: '<b>{point.x}</b><br/>',
                    pointFormat: '{series.name}: {point.y:.1f} Mil<br/>Total: {point.stackTotal:.1f} Mil'
                },
                plotOptions: {
                    column: {
                        stacking: 'normal',
                        dataLabels: {
                            enabled: true,
                            format: '{point.y:.1f}',
                            borderColor: '#FF5722',
                            borderWidth: 8,
                            shadow: false
                        },
                        pointFormat: "Value: {point.y:,.1f} mm"

                    }
                },
                series: JSON.parse(data)

            });
        }
    </script>


</asp:Content>
