﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Intranet.Master" AutoEventWireup="true" CodeBehind="RutaVehiculo.aspx.cs" Inherits="WebApplication1.Modulo.Area.Encomienda.RutaVehiculo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContenedorDashboard" runat="server">


 <!-- **********************************************************************************************************************************************************
      FUNCIONALIDAD  JAVASCRIPT
      *********************************************************************************************************************************************************** -->
    <!--main content start-->
    <script>

        
            //######  MANTENIMIENTO COSTOS - COSTO DEL PERSONAL Y REPUESTOS LINECHART  ######
    
            // TODO 1 : ZONA DE VARIABLES
            var AnioGTV = "";
            var MesGTV = ""; 
            var BimestreGTV='';
            var TrimestreGTV='';
            var agrupacionGTV='';
            var data_html = "";
    
            //TODO 2 : LLAMADA A FUNCIONES YA IMPLEMENTADOS
    
            // !Llama a la funcion que se en carga de llenar los filtros
            CargarCombosGeneralesGTV(AnioGTV, MesGTV, TrimestreGTV,BimestreGTV);
            
            // $("#cmbMes_GTV").val(0); document.getElementById("cmbMes_GTV").disabled = true;
            // $("#cmbTrimestre_GTV").val(0); document.getElementById("cmbTrimestre_GTV").disabled = true;
            // $("#cmbBimestre_GTV").val(0); document.getElementById("cmbBimestre_GTV").disabled = true;

            Grafica_Encomiendas_Personal_RutasVehiculos_GTV("2019", "","","");
    
    
            // TODO 3 : CAPTURAR ELEMENTOS MEDIANTE JQUERY
            $(document).ready(function () {
                 CambiarEstadoBotonesGTV();

                //Camvia el valor de TRIME y deshabilita Mes y Bimestre
                $("#cmbTrimestre_GTV").change(function () {
                    $("#cmbMes_GTV").val(0); document.getElementById("cmbMes_GTV").disabled = true;
                    $("#cmbBimestre_GTV").val(0); document.getElementById("cmbBimestre_GTV").disabled = true;        
                });
                 //Cambia el valor de Bimestre y deshabilita Mes y Trimestre
                $("#cmbBimestre_GTV").change(function () {
                    $("#cmbMes_GTV").val(0); document.getElementById("cmbMes_GTV").disabled = true;
                    $("#cmbTrimestre_GTV").val(0); document.getElementById("cmbTrimestre_GTV").disabled = true;                  
                });

                // Cambiar valor Mes y deshabilitar Trim y Bime
                $("#cmbMes_GTV").change(function () {
                     $("#cmbTrimestre_GTV").val(0); document.getElementById("cmbTrimestre_GTV").disabled = true;
                     $("#cmbBimestre_GTV").val(0); document.getElementById("cmbBimestre_GTV").disabled = true;
                                    
                });

                //JQUERY's Para cargar los combos y validarlos
    
                // !Jquery que carga la sucursal
                $("#cmbAño_GTV").change(function () {
                    validarCombosGTV();
                    CargarCombosGeneralesGTV(AnioGTV, MesGTV, TrimestreGTV,BimestreGTV);
                    $("#cmbMes_GTV").val(0); document.getElementById("cmbMes_GTV").disabled = false;
                    $("#cmbTrimestre_GTV").val(0); document.getElementById("cmbTrimestre_GTV").disabled = false;
                    $("#cmbBimestre_GTV").val(0); document.getElementById("cmbBimestre_GTV").disabled = false;    
                });
    
                //!Jquery que carga el area
                $("#cmbMes_GTV").change(function () {
                    validarCombosGTV();
                    CargarCombosGeneralesGTV(AnioGTV, MesGTV, TrimestreGTV,BimestreGTV);
                    

                });

                $("#cmbTrimestre_GTV").change(function () {
                    validarCombosGTV();
                    CargarCombosGeneralesGTV(AnioGTV, MesGTV, TrimestreGTV,BimestreGTV);
                    

                });
    
                //!Jquery que carga el area
                $("#cmbBimestre_GTV").change(function () {
                    validarCombosGTV();
                    CargarCombosGeneralesGTV(AnioGTV, MesGTV);
                  
                });
    
                 // ! JQUERY que carga la grafica 
                $("#btn_Graficar_RutasVehiculo_GTV").click(function () {
                   
                    validarCombosGTV();
                    Grafica_Encomiendas_Personal_RutasVehiculos_GTV(AnioGTV, MesGTV, TrimestreGTV,BimestreGTV);
                    
                    //Activar los Filtros
                    $("#cmbTrimestre_GTV"); document.getElementById("cmbTrimestre_GTV").disabled = false;
                    $("#cmbBimestre_GTV"); document.getElementById("cmbBimestre_GTV").disabled = false;
                    $("#cmbMes_GTV"); document.getElementById("cmbMes_GTV").disabled = false;
                });
    
                $("#btn_Limpiar_GTV").click(function(){
                    LimpiarDataGTV();
                    validarCombosGTV();
                    Grafica_Encomiendas_Personal_RutasVehiculos_GTV("2019","","","");

                    //Activar los Filtros
                    $("#cmbTrimestre_GTV"); document.getElementById("cmbTrimestre_GTV").disabled = true;
                    $("#cmbBimestre_GTV"); document.getElementById("cmbBimestre_GTV").disabled = true;
                    $("#cmbMes_GTV"); document.getElementById("cmbMes_GTV").disabled = true;
    
                });
            });
    
            // TODO 4 : FUNCIONES PARA INTERACTUAR CON LOS FILTROS (COMBOS)
    
            function CambiarEstadoGTV() {
                if (AnioGTV != 0) $("#cmbAño_GTV").val(0); document.getElementById("cmbAño_GTV").disabled = false;
                if (MesGTV != 0) $("#cmbMes_GTV").val(0); document.getElementById("cmbMes_GTV").disabled = false;
                if (BimestreGTV != 0) $("#cmbTrimestre_GTV").val(0); document.getElementById("cmbTrimestre_GTV").disabled = false;
                if (TrimestreGTV != 0) $("#cmbBimestre_GTV").val(0); document.getElementById("cmbBimestre_GTV").disabled = false;

                validarCombosGTV();
                CargarCombosGeneralesGTV(AnioGTV, MesGTV, TrimestreGTV,BimestreGTV);
            }

             function CambiarEstadoBotonesGTV() {
                if (AnioGTV != 0) $("#cmbAño_GTV").val(0); document.getElementById("cmbAño_GTV").disabled = false;
                if (MesGTV != 0) $("#cmbMes_GTV").val(0); document.getElementById("cmbMes_GTV").disabled = true;
                if (BimestreGTV != 0) $("#cmbTrimestre_GTV").val(0); document.getElementById("cmbTrimestre_GTV").disabled = true;
                if (TrimestreGTV != 0) $("#cmbBimestre_GTV").val(0); document.getElementById("cmbBimestre_GTV").disabled = true;

               
                // Grafica_Encomiendas_Personal_RutasVehiculos_GTV("","","0");
            }
    
            // !Captura el valor seleccionado en el filtro area, subarea y  sucursal 
            function validarCombosGTV() {
                AnioGTV = $("#cmbAño_GTV").val(); if (AnioGTV != 0) { AnioGTV= $("#cmbAño_GTV  option:selected").text(); } else { AnioGTV = ""; }
                MesGTV = $("#cmbMes_GTV").val(); if (MesGTV != 0) { MesGTV = $("#cmbMes_GTV  option:selected").text(); } else { MesGTV = ""; }
                BimestreGTV = $("#cmbBimestre_GTV").val(); if (BimestreGTV != 0) { BimestreGTV= $("#cmbBimestre_GTV  option:selected").text(); } else { BimestreGTV = ""; }
                TrimestreGTV = $("#cmbTrimestre_GTV").val(); if (TrimestreGTV != 0) { TrimestreGTV = $("#cmbTrimestre_GTV  option:selected").text(); } else { TrimestreGTV = ""; }
                // if ($("#opc_trimestreGTV").prop('checked')) { agrupacionGTV = "3";}
                // if ($("#opc_bimestreGTV").prop('checked')) { agrupacionGTV = "2";}
                // if ($("#opc_bimestreGTV").prop('checked') == false && $("#opc_trimestreGTV").prop('checked') == false) {agrupacionGTV = "0";}
                CargarCombosGeneralesGTV();
            }
    
            // ! Funcion que permite cargar datos a los combos
            function CargarCombosGeneralesGTV(AnioGTV, MesGTV,TrimestreGTV, BimestreGTV) {
                if (AnioGTV == 0 || AnioGTV == "") fn_General_Cargar_FiltroAnio_GTV(MesGTV, TrimestreGTV, BimestreGTV);
                if (MesGTV == 0 || MesGTV == "") fn_General_Cargar_FiltroMes_GTV(AnioGTV);
                if (BimestreGTV == 0 || BimestreGTV == "") fn_General_Cargar_FiltroBimestre_GTV(AnioGTV);
                if (TrimestreGTV == 0 || TrimestreGTV == "") fn_General_Cargar_FiltroTrimestre_GTV(AnioGTV);
            }
    
            function LimpiarDataGTV() {
                
                // if(DepartamentoEHGM!=0) $('#cmbDepartamento_EHGM').val(0);
                if(AnioGTV!=0) $('#cmbAño_GTV').val(0);
                if(MesGTV!=0) $('#cmbMes_GTV').val(0);
                if(BimestreGTV!=0) $('#cmbBimestre_GTV').val(0);
                if(TrimestreGTV!=0) $('#cmbTrimestre_GTV').val(0);
                CargarCombosGeneralesGTV();
                Grafica_Encomiendas_Personal_RutasVehiculos_GTV("2019", "","","");

             
            }
    
            //TODO 5: FUNCIONES QUE IMPLEMENTAN LLAMADAS AJAX 
    
            // ! Funcion para cargar los Aniosen el combo Anio
            function fn_General_Cargar_FiltroAnio_GTV(MesGTV, TrimestreGTV, BimestreGTV) {
                var html = '';
                var sucess = function (response) {
                    var obj = JSON.parse(response.d);
                    var data_html = "";
                    if (obj == null) {
                        data_html = "<option value='0'>Seleccionar</option>";
                        $("select[id$=cmbAño_GTV]").html(data_html);
                    }
                    else {
                        data_html += "<option value='0'>Seleccionar</option>";
                        for (var i = 0; i < obj.length; i++) {
                            data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>";
                        }
                        $("select[id$=cmbAño_GTV]").html(data_html);
                    }
                };
                var error = function (xhr, ajaxOptions, thrownError) {
                };
                fn_LlamadoMetodo("RutaVehiculo.aspx/Encomiendas_RutasVehiculos_ListarAño_GTV_CS", JSON.stringify({  Mes: MesGTV, Trimestre:TrimestreGTV, Bimestre:BimestreGTV }), sucess, error);
            }
    
            // ! Funcion que carga los Meses en el combo Meses
            function fn_General_Cargar_FiltroMes_GTV(AnioGTV) {
                var html = '';
                var sucess = function (response) {
                    var obj = JSON.parse(response.d);
                    var data_html = "";
                    if (obj == null) {
                        data_html = "<option value='0'>Seleccionar</option>";
                        $("select[id$=cmbMes_GTV]").html(data_html);
                    }
                    else {
                        data_html += "<option value='0'>Seleccionar</option>";
                        for (var i = 0; i < obj.length; i++) {
                            data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>";
                        }
                        $("select[id$=cmbMes_GTV]").html(data_html);
                    }
                };
                var error = function (xhr, ajaxOptions, thrownError) {
                };
                fn_LlamadoMetodo("RutaVehiculo.aspx/Encomiendas_RutasVehiculos_ListarMes_GTV_CS", JSON.stringify({ Año: AnioGTV }), sucess, error);
            }

            function fn_General_Cargar_FiltroBimestre_GTV(AnioGTV) {
                var html = '';
                var sucess = function (response) {
                    var obj = JSON.parse(response.d);
                    var data_html = "";
                    if (obj == null) {
                        data_html = "<option value='0'>Seleccionar</option>";
                        $("select[id$=cmbBimestre_GTV]").html(data_html);
                    }
                    else {
                        data_html += "<option value='0'>Seleccionar</option>";
                        for (var i = 0; i < obj.length; i++) {
                            data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>";
                        }
                        $("select[id$=cmbBimestre_GTV]").html(data_html);
                    }
                };
                var error = function (xhr, ajaxOptions, thrownError) {
                };
                fn_LlamadoMetodo("RutaVehiculo.aspx/Encomiendas_RutasVehiculos_ListarBimestre_GTV_CS", JSON.stringify({  Año: AnioGTV }), sucess, error);
            }

            function fn_General_Cargar_FiltroTrimestre_GTV(MesGTV) {
                var html = '';
                var sucess = function (response) {
                    var obj = JSON.parse(response.d);
                    var data_html = "";
                    if (obj == null) {
                        data_html = "<option value='0'>Seleccionar</option>";
                        $("select[id$=cmbTrimestre_GTV]").html(data_html);
                    }
                    else {
                        data_html += "<option value='0'>Seleccionar</option>";
                        for (var i = 0; i < obj.length; i++) {
                            data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>";
                        }
                        $("select[id$=cmbTrimestre_GTV]").html(data_html);
                    }
                };
                var error = function (xhr, ajaxOptions, thrownError) {
                };
                fn_LlamadoMetodo("RutaVehiculo.aspx/Encomiendas_RutasVehiculos_ListarTrimestre_GTV_CS", JSON.stringify({  Año: AnioGTV }), sucess, error);
            }
    
        
            // TODO 6: FUNCION QUE IMPLEMENTA LA GRAFICA
                 function  Grafica_Encomiendas_Personal_RutasVehiculos_GTV(AnioGTV, MesGTV, TrimestreGTV,BimestreGTV) {
                 var sucess = function (response) {
                 //fn_Mensaje_Alerta("Confirmacion", "Se esta ejecutando los datos", "", "Alert");
                 //var json=response.d.Series;
                 stacketChar(response.d.Fecha, response.d.Name, fn_ValidarANulos(response.d.Series));
                 $('#Contenedor_RutasVehiculo_GTV').show();
    
                };
                     var error = function (xhr, ajaxOptions, thrownError) {
                };
                     fn_LlamadoMetodo("RutaVehiculo.aspx/Grafica_Encomiendas_RutasVehiculos_GeneralTipoVehiculo_CS", JSON.stringify({ Año:AnioGTV, Mes:MesGTV ,Trimestre:TrimestreGTV, Bimestre:BimestreGTV}), sucess, error);
                }
    
            // TODO : FUNCION HIGHTCHART QUE PERMITE DAR FORMA A LA GRAFICA       
                function stacketChar(fecha,Name,data ){
                    Highcharts.chart('Contenedor_RutasVehiculo_GTV', {
                       chart: {
                            type: 'bar'
                          },
                        title: {
                            text: ' CANTIDAD DE VIAJES SEGUN  RUTA  POR TIPO DE VEHICULO'
                            
                        },
                        subtitle:{
                            text:'RUTA VS TIPO VEHICULO'
                        },
                         xAxis: {
                             categories:JSON.parse(fecha)
                        },
                        yAxis: {
                            min: 0,
                            title: {
                            text: 'CANTIDAD VIAJES',
                        
                         }
                         
                        },
                        legend: {
                            reversed: true
                        },
                        plotOptions: {
                            series: {
                                stacking: 'normal'
                                
                            }
                        },
                    series: 
                    
                    JSON.parse(data)
                });
             }



             // ##### GRAFICA: CANTIDAD VIAJE SEGUN RUTA POR TIEMPO ######    
            
             // TODO 1 : ZONA DE VARIABLES
            var AnioGRT = "";
            var MesGRT = ""; 
            var tipoGRT="";
            var agrupacionGRT='';
            var data_html = "";
    
            //TODO 2 : LLAMADA A FUNCIONES YA IMPLEMENTADOS
    
            // !Llama a la funcion que se en carga de llenar los filtros
            CargarCombosGeneralesGRT(AnioGRT, MesGRT,tipoGRT);
            Grafica_Encomiendas_Personal_RutasVehiculos_GRT("", "","","0");
    
    
            // TODO 3 : CAPTURAR ELEMENTOS MEDIANTE JQUERY
            $(document).ready(function () {
                $("#opc_trimestreGRT").change(function () {
                     $("#opc_bimestreGRT").prop("checked", false);
                    validarCombosGRT();
                    Grafica_Encomiendas_Personal_RutasVehiculos_GRT(AnioGRT,MesGRT,tipoGRT,agrupacionGRT)                
                });

                $("#opc_bimestreGRT").change(function () {
                    $("#opc_trimestreGRT").prop("checked", false);
                    validarCombosGRT();
                    Grafica_Encomiendas_Personal_RutasVehiculos_GRT(AnioGRT,MesGRT,tipoGRT,agrupacionGRT)                
                });

    
                // !Jquery que carga la sucursal
                $("#cmbAño_GRT").change(function () {
                    validarCombosGRT();
                    CargarCombosGeneralesGRT(AnioGRT, MesGRT,tipoGRT);
                });
    
                //!Jquery que carga el area
                $("#cmbMes_GRT").change(function () {
                    validarCombosGRT();
                    CargarCombosGeneralesGRT(AnioGRT, MesGRT,tipoGRT);
                });

                //!Jquery que carga el area
                $("#cmbTipo_GRT").change(function () {
                    validarCombosGRT();
                    CargarCombosGeneralesGRT(AnioGRT, MesGRT,tipoGRT);
                });


              
    
                 // ! JQUERY que carga la grafica 
                $("#btn_Graficar_RutasVehiculo_GRT").click(function () {
                    validarCombosGRT();
                    Grafica_Encomiendas_Personal_RutasVehiculos_GRT(AnioGRT, MesGRT, tipoGRT,agrupacionGRT);
                    
                });
    
                $("#btn_Limpiar_GRT").click(function(){
                    LimpiarDataGRT();
                    validarCombosGRT();
                    Grafica_Encomiendas_Personal_RutasVehiculos_GRT("","","","0");
    
                });
            });
    
            // TODO 4 : FUNCIONES PARA INTERACTUAR CON LOS FILTROS (COMBOS)
    
            function CambiarEstadoGRT() {
                if (AnioGRT != 0) $("#cmbAño_GRT").val(0); document.getElementById("cmbAño_GRT").disabled = false;
                if (MesGRT != 0) $("#cmbMes_GRT").val(0); document.getElementById("cmbMes_GRT").disabled = false;
                if (tipoGRT != 0) $("#cmbTipo_GRT").val(0); document.getElementById("cmbTipo_GRT").disabled = false;

                $("#opc_trimestreGRT").prop("checked", false);
                $("#opc_bimestreGRT").prop("checked", false);
                
                validarCombosGRT();
                CargarCombosGeneralesGRT(AnioGRT, MesGRT,tipoGRT);
                Grafica_Encomiendas_Personal_RutasVehiculos_GRT("","","","0");
            }
    
            // !Captura el valor seleccionado en el filtro area, subarea y  sucursal 
            function validarCombosGRT() {
                AnioGRT = $("#cmbAño_GRT").val(); if (AnioGRT != 0) { AnioGRT= $("#cmbAño_GRT  option:selected").text(); } else { AnioGRT = ""; }
                MesGRT = $("#cmbMes_GRT").val(); if (MesGRT != 0) { MesGRT = $("#cmbMes_GRT  option:selected").text(); } else { MesGRT = ""; }
                tipoGRT = $("#cmbTipo_GRT").val(); if (tipoGRT != 0) { tipoGRT = $("#cmbTipo_GRT  option:selected").text(); } else { tipoGRT = ""; }

                if ($("#opc_trimestreGRT").prop('checked')) { agrupacionGRT = "3";}
                if ($("#opc_bimestreGRT").prop('checked')) { agrupacionGRT = "2";}
                if ($("#opc_bimestreGRT").prop('checked') == false && $("#opc_trimestreGRT").prop('checked') == false) {agrupacionGRT = "0";}

               
            }
    
            // ! Funcion que permite cargar datos a los combos
            function CargarCombosGeneralesGRT(AnioGRT, MesGRT,tipoGRT) {
                if (AnioGRT == 0 || AnioGRT == "") fn_General_Cargar_FiltroAnio_GRT(MesGRT,tipoGRT);
                if (MesGRT == 0 || MesGRT == "") fn_General_Cargar_FiltroMes_GRT(AnioGRT,tipoGRT);
                if (tipoGRT == 0 || tipoGRT == "") fn_General_Cargar_FiltroTipo_GRT(AnioGRT, MesGRT);

            }
    
            function LimpiarDataGRT() {
                
                // if(DepartamentoEHGM!=0) $('#cmbDepartamento_EHGM').val(0);
                if(AnioGRT!=0) $('#cmbAño_GRT').val(0);
                if(MesGRT!=0) $('#cmbMes_GRT').val(0);
                if(tipoGRT!=0) $('#cmbTipo_GRT').val(0);

                CargarCombosGeneralesGRT();
             
            }
    
            //TODO 5: FUNCIONES QUE IMPLEMENTAN LLAMADAS AJAX 
    
            // ! Funcion para cargar los Aniosen el combo Anio
            function fn_General_Cargar_FiltroAnio_GRT(MesGRT, tipoGRT) {
                var html = '';
                var sucess = function (response) {
                    var obj = JSON.parse(response.d);
                    var data_html = "";
                    if (obj == null) {
                        data_html = "<option value='0'>Seleccionar</option>";
                        $("select[id$=cmbAño_GRT]").html(data_html);
                    }
                    else {
                        data_html += "<option value='0'>Seleccionar</option>";
                        for (var i = 0; i < obj.length; i++) {
                            data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>";
                        }
                        $("select[id$=cmbAño_GRT]").html(data_html);
                    }
                };
                var error = function (xhr, ajaxOptions, thrownError) {
                };
                fn_LlamadoMetodo("RutaVehiculo.aspx/Encomiendas_RutasVehiculos_ListarAño_GRT_CS", JSON.stringify({  mes: MesGRT, tipo:tipoGRT }), sucess, error);
            }
    
            // ! Funcion que carga los Meses en el combo Meses
            function fn_General_Cargar_FiltroMes_GRT(AnioGRT,tipoGRT) {
                var html = '';
                var sucess = function (response) {
                    var obj = JSON.parse(response.d);
                    var data_html = "";
                    if (obj == null) {
                        data_html = "<option value='0'>Seleccionar</option>";
                        $("select[id$=cmbMes_GRT]").html(data_html);
                    }
                    else {
                        data_html += "<option value='0'>Seleccionar</option>";
                        for (var i = 0; i < obj.length; i++) {
                            data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>";
                        }
                        $("select[id$=cmbMes_GRT]").html(data_html);
                    }
                };
                var error = function (xhr, ajaxOptions, thrownError) {
                };
                fn_LlamadoMetodo("RutaVehiculo.aspx/Encomiendas_RutasVehiculos_ListarMes_GRT_CS", JSON.stringify({ anio: AnioGRT,tipo:tipoGRT }), sucess, error);
            }

            function fn_General_Cargar_FiltroTipo_GRT(AnioGRT,MesGRT) {
                var html = '';
                var sucess = function (response) {
                    var obj = JSON.parse(response.d);
                    var data_html = "";
                    if (obj == null) {
                        data_html = "<option value='0'>Seleccionar</option>";
                        $("select[id$=cmbTipo_GRT]").html(data_html);
                    }
                    else {
                        data_html += "<option value='0'>Seleccionar</option>";
                        for (var i = 0; i < obj.length; i++) {
                            data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>";
                        }
                        $("select[id$=cmbTipo_GRT]").html(data_html);
                    }
                };
                var error = function (xhr, ajaxOptions, thrownError) {
                };
                fn_LlamadoMetodo("RutaVehiculo.aspx/Encomiendas_RutasVehiculos_ListarTipo_GRT_CS", JSON.stringify({  anio: AnioGRT,mes:MesGRT }), sucess, error);
            }

           
    
        
            // TODO 6: FUNCION QUE IMPLEMENTA LA GRAFICA
                 function  Grafica_Encomiendas_Personal_RutasVehiculos_GRT(AnioGRT, MesGRT, tipoGRT ,agrupacionGRT) {
                 var sucess = function (response) {
                 //fn_Mensaje_Alerta("Confirmacion", "Se esta ejecutando los datos", "", "Alert");
                 //var json=response.d.Series;
                 stacketChar_GRT(response.d.Fecha, response.d.Name, fn_ValidarANulos(response.d.Series));
                 $('#Contenedor_RutasVehiculo_GRT').show();
    
                };
                     var error = function (xhr, ajaxOptions, thrownError) {
                };
                     fn_LlamadoMetodo("RutaVehiculo.aspx/Grafica_Encomiendas_RutasVehiculos_GeneralRutaTiempo_CS", JSON.stringify({ anio:AnioGRT, mes:MesGRT ,tipo:tipoGRT,agrupacionF:agrupacionGRT}), sucess, error);
                }
    
            // TODO : FUNCION HIGHTCHART QUE PERMITE DAR FORMA A LA GRAFICA       
                function stacketChar_GRT(fecha,Name,data ){
                    Highcharts.chart('Contenedor_RutasVehiculo_GRT', {
                       chart: {
                            type: 'bar'
                          },
                        title: {
                            text: ' CANTIDAD DE VIAJES SEGUN  RUTA POR TIEMPO'
                            
                        },
                        subtitle:{
                            text:'RUTA VS TIEMPO'
                        },
                         xAxis: {
                             categories:JSON.parse(fecha)
                        },
                        yAxis: {
                            min: 0,
                            title: {
                            text: 'CANTIDAD VIAJES',
                        
                         }
                         
                        },
                        legend: {
                            reversed: true
                        },
                        plotOptions: {
                            series: {
                                stacking: 'normal'
                            }
                        },
                    series: JSON.parse(data)
                });
             }


            // ##### GRAFICA: CANTIDAD VIAJE RUTA   { GENERAL POR RUTA}   ######  
            
             // TODO 1 : ZONA DE VARIABLES
            var AnioCVR = "";
            var MesCVR = ""; 
            
            var BimestreCVR='';
            var TrimestreCVR='';
            var agrupacionCVR='';
            var data_html = "";
    
            //TODO 2 : LLAMADA A FUNCIONES YA IMPLEMENTADOS
    
            // !Llama a la funcion que se en carga de llenar los filtros
            CargarCombosGeneralesCVR(AnioCVR, MesCVR);
            Grafica_Encomiendas_RutasVehiculos_CVR("","","0");
    
    
            // TODO 3 : CAPTURAR ELEMENTOS MEDIANTE JQUERY
            $(document).ready(function () {

                $("#opc_trimestreCVR"); document.getElementById("opc_trimestreCVR").disabled = true;
                $("#opc_bimestreCVR"); document.getElementById("opc_bimestreCVR").disabled = true;


                $("#opc_trimestreCVR").change(function () {

                     $("#opc_bimestreCVR").prop("checked", false);
                     CambiarMes();
                     validarCombosCVR();
                     Grafica_Encomiendas_RutasVehiculos_CVR(AnioCVR,MesCVR,agrupacionCVR)                
                });

                $("#opc_bimestreCVR").change(function () {
                    $("#opc_trimestreCVR").prop("checked", false);
                    CambiarMes();
                    validarCombosCVR();
                    Grafica_Encomiendas_RutasVehiculos_CVR(AnioCVR,MesCVR,agrupacionCVR)                
                });

    
                // !Jquery que carga la sucursal
                $("#cmbAño_CVR").change(function () {
                    validarCombosCVR();
                    CargarCombosGeneralesCVR(AnioCVR, MesCVR);
                    CambiarMes();
                    $("#opc_trimestreCVR"); document.getElementById("opc_trimestreCVR").disabled = false;
                    $("#opc_bimestreCVR"); document.getElementById("opc_bimestreCVR").disabled = false;

                    
                });
    
                //!Jquery que carga el area
                $("#cmbMes_CVR").change(function () {
                    validarCombosCVR();
                    CargarCombosGeneralesCVR(AnioCVR, MesCVR);
                    $("#opc_trimestreCVR"); document.getElementById("opc_trimestreCVR").disabled = true;
                    $("#opc_bimestreCVR"); document.getElementById("opc_bimestreCVR").disabled = true;
                    $("#opc_bimestreCVR").prop("checked", false);
                    $("#opc_TrimestreCVR").prop("checked", false);
                    
                });

                //!Jquery que carga el area
                $("#cmbTipo_CVR").change(function () {
                    validarCombosCVR();
                    CargarCombosGeneralesCVR(AnioCVR, MesCVR);
                });


              
    
                 // ! JQUERY que carga la grafica 
                $("#btn_Graficar_RutasVehiculo_CVR").click(function () {
                    validarCombosCVR();
                    Grafica_Encomiendas_RutasVehiculos_CVR(AnioCVR, MesCVR,agrupacionCVR);
                    $("#opc_trimestreCVR"); document.getElementById("opc_trimestreCVR").disabled = false;
                    $("#opc_bimestreCVR"); document.getElementById("opc_bimestreCVR").disabled = false;
                    
                });
    
                $("#btn_Limpiar_CVR").click(function(){
                    LimpiarDataCVR();
                    validarCombosCVR();
                    $("#opc_trimestreCVR"); document.getElementById("opc_trimestreCVR").disabled = false;
                    $("#opc_bimestreCVR"); document.getElementById("opc_bimestreCVR").disabled = false;
                    $("#opc_bimestreCVR").prop("checked", false);
                    $("#opc_TrimestreCVR").prop("checked", false);
                    Grafica_Encomiendas_RutasVehiculos_CVR("","","0");
    
                });
            });
    
            // TODO 4 : FUNCIONES PARA INTERACTUAR CON LOS FILTROS (COMBOS)
    
            function CambiarEstadoCVR() {
                if (AnioCVR != 0) $("#cmbAño_CVR").val(0); document.getElementById("cmbAño_CVR").disabled = false;
                if (MesCVR != 0) $("#cmbMes_CVR").val(0); document.getElementById("cmbMes_CVR").disabled = false;

                $("#opc_trimestreCVR").prop("checked", false);
                $("#opc_bimestreCVR").prop("checked", false);
                
                validarCombosCVR();
                CargarCombosGeneralesCVR(AnioCVR, MesCVR);
                Grafica_Encomiendas_RutasVehiculos_CVR("","","0");
            }

            function CambiarMes() {
                // if (AnioCVR != 0) $("#cmbAño_CVR").val(0); document.getElementById("cmbAño_CVR").disabled = false;
                if (MesCVR != 0) $("#cmbMes_CVR").val(0); document.getElementById("cmbMes_CVR").disabled = false;

            }
    
    
            // !Captura el valor seleccionado en el filtro area, subarea y  sucursal 
            function validarCombosCVR() {
                AnioCVR = $("#cmbAño_CVR").val(); if (AnioCVR != 0) { AnioCVR= $("#cmbAño_CVR  option:selected").text(); } else { AnioCVR = ""; }
                MesCVR = $("#cmbMes_CVR").val(); if (MesCVR != 0) { MesCVR = $("#cmbMes_CVR  option:selected").text(); } else { MesCVR = ""; }

                if ($("#opc_trimestreCVR").prop('checked')) { agrupacionCVR = "3";}
                if ($("#opc_bimestreCVR").prop('checked')) { agrupacionCVR = "2";}
                if ($("#opc_bimestreCVR").prop('checked') == false && $("#opc_trimestreCVR").prop('checked') == false) {agrupacionCVR = "0";}

               
            }
    
            // ! Funcion que permite cargar datos a los combos
            function CargarCombosGeneralesCVR(AnioCVR, MesCVR) {
                if (AnioCVR == 0 || AnioCVR == "") fn_General_Cargar_FiltroAnio_CVR(MesCVR);
                if (MesCVR == 0 || MesCVR == "") fn_General_Cargar_FiltroMes_CVR(AnioCVR);

            }
    
            function LimpiarDataCVR() {
                
                // if(DepartamentoEHGM!=0) $('#cmbDepartamento_EHGM').val(0);
                if(AnioCVR!=0) $('#cmbAño_CVR').val(0);
                if(MesCVR!=0) $('#cmbMes_CVR').val(0);

                CargarCombosGeneralesCVR();
             
            }
    
            //TODO 5: FUNCIONES QUE IMPLEMENTAN LLAMADAS AJAX 
    
            // ! Funcion para cargar los Aniosen el combo Anio
            function fn_General_Cargar_FiltroAnio_CVR(MesCVR) {
                var html = '';
                var sucess = function (response) {
                    var obj = JSON.parse(response.d);
                    var data_html = "";
                    if (obj == null) {
                        data_html = "<option value='0'>Seleccionar</option>";
                        $("select[id$=cmbAño_CVR]").html(data_html);
                    }
                    else {
                        data_html += "<option value='0'>Seleccionar</option>";
                        for (var i = 0; i < obj.length; i++) {
                            data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>";
                        }
                        $("select[id$=cmbAño_CVR]").html(data_html);
                    }
                };
                var error = function (xhr, ajaxOptions, thrownError) {
                };
                fn_LlamadoMetodo("RutaVehiculo.aspx/Encomiendas_RutaVehiculos_ListarAño_CVR_CS", JSON.stringify({  Mes: MesCVR}), sucess, error);
            }
    
            // ! Funcion que carga los Meses en el combo Meses
            function fn_General_Cargar_FiltroMes_CVR(AnioCVR,tipoCVR) {
                var html = '';
                var sucess = function (response) {
                    var obj = JSON.parse(response.d);
                    var data_html = "";
                    if (obj == null) {
                        data_html = "<option value='0'>Seleccionar</option>";
                        $("select[id$=cmbMes_CVR]").html(data_html);
                    }
                    else {
                        data_html += "<option value='0'>Seleccionar</option>";
                        for (var i = 0; i < obj.length; i++) {
                            data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>";
                        }
                        $("select[id$=cmbMes_CVR]").html(data_html);
                    }
                };
                var error = function (xhr, ajaxOptions, thrownError) {
                };
                fn_LlamadoMetodo("RutaVehiculo.aspx/Encomiendas_RutaVehiculos_ListarMes_CVR_CS", JSON.stringify({ Año: AnioCVR }), sucess, error);
            }

          
           
    
        
            // TODO 6: FUNCION QUE IMPLEMENTA LA GRAFICA
                 function  Grafica_Encomiendas_RutasVehiculos_CVR(AnioCVR, MesCVR ,agrupacionCVR) {
                 var sucess = function (response) {
                 //fn_Mensaje_Alerta("Confirmacion", "Se esta ejecutando los datos", "", "Alert");
                 //var json=response.d.Series;
                 BarrasChart_CVR(response.d.Fecha, response.d.Name, fn_ValidarANulos(response.d.Series));
                 $('#Contenedor_RutasVehiculo_CVR').show();
    
                };
                     var error = function (xhr, ajaxOptions, thrownError) {
                };
                     fn_LlamadoMetodo("RutaVehiculo.aspx/Grafica_Encomiendas_RutaVehiculos_CantidadViajesRuta_CS", JSON.stringify({ Año:AnioCVR, Mes:MesCVR,agrupacionF:agrupacionCVR}), sucess, error);
                }
    
            // TODO : FUNCION HIGHTCHART QUE PERMITE DAR FORMA A LA GRAFICA       
                function BarrasChart_CVR(fecha,Name,data ){
                    Highcharts.chart('Contenedor_RutasVehiculo_CVR', {
                       chart: {
                            type: 'column'
                          },
                        title: {
                            text: ' CANTIDAD DE VIAJES SEGUN  RUTA  POR TIPO DE VEHICULO'
                            
                        },
                        subtitle:{
                            text:'RUTA VS TIPO VEHICULO'
                        },
                         xAxis: {
                             categories:JSON.parse(fecha)
                        },
                        yAxis: {
                            min: 0,
                            title: {
                            text: 'CANTIDAD VIAJES',
                        
                         }
                         
                        },
                        legend: {
                            reversed: true
                        },
                        plotOptions: {
                            series: {
                                stacking: 'normal'
                            }
                        },
                    series: JSON.parse(data)
                  
                    
                });
             }
    
    </script>
    
    
        <!-- **********************************************************************************************************************************************************
                                                              ESTRUCTURA HTML
          *********************************************************************************************************************************************************** -->
        <!--main content start-->
    
        <!--  GRAFICA  GENERAL RUTA DE VIAJES POR TIPO DE VEHICULO-->
        <!-- <h3><i class="fa fa-angle-right" style="color: #F1C445;"></i> <a>COSTO MANTENIMIENTO</a></h3> -->
        <div class="row panel-body">
            <h3  style="color: #283593; margin-top: 1px; margin-bottom: 10px;">RUTA DE VEHICULOS</h3>
        </div>
    
        <div class="col-lg-12 main-chart" style="  text-align: left !important;">
            <p >  <b>OBJECTIVO:</b> El objetivo del indicador de encomiendas es poder obtener el seguimiento de los viajes realizados. </p>
        </div>
    
    
        <div class="col-lg-12  main-chart" >
            <div class="panel panel-headline" style="margin-bottom: 3px !important;" >
                <div class="panel-body" class="col-lg-6 main-chart" >
                    <div class="grid-container">
    
                        <div class="row">
    
                            <div class="col-md-12 ">
    
                                <!-- OPCIONES-->
                                <div class="col-md-8">
    
                                    <!--Año-->
                                    <div class="col-md-3">
                                        <label for=""> Año:</label>
                                         <asp:DropDownList ID="cmbAño_GTV" ClientIDMode="Static"  class="form-control" runat="server" Style="width: 100%;" ></asp:DropDownList>
    
                                    </div>
    
                                    <!--Mes-->
                                    <div class="col-md-3">
                                        <label for="">Mes:</label>
                                        <asp:DropDownList ID="cmbMes_GTV" ClientIDMode="Static"  class="form-control" runat="server" Style="width: 100%;" > </asp:DropDownList>
                                    </div>

                                    <div class="col-md-3">
                                        <label for=""> Trimestre:</label>
                                        <asp:DropDownList ID="cmbTrimestre_GTV" ClientIDMode="Static"  class="form-control" runat="server" Style="width: 100%;" > </asp:DropDownList>
                                    </div>

                                    <div class="col-md-3" >
                                        <label for=""> Bimestre:</label>
                                        <asp:DropDownList ID="cmbBimestre_GTV" ClientIDMode="Static"  class="form-control" runat="server" Style="width: 100%;" > </asp:DropDownList>
                                    </div>
                                
                                </div>
    
                                <!-- BOTONES-->
                                <div class="col-md-4 ">
                                    <!--GRAFICAR-->
                                    <div class="col-md-6 ">
                                        <button id="btn_Graficar_RutasVehiculo_GTV" type="button" class="btn btn-primary btn-sm">GRAFICAR</button>
                                    </div>
    
                                    <!--LIMPIAR-->
                                    <div class="col-md-6" >
                                        <label>Limpiar:</label>
                                        <span class="lnr lnr-magic-wand-limpiar" id="btn_Limpiar_GTV"> </span>
                                    </div>
                                </div>
                            </div>
    
                        </div>
                    </div>
                </div>
            </div>
    
            <div class="panel panel-headline" style="margin-bottom: 1px !important;">
                <div class="panel-body" class="col-lg-5 main-chart">
                    <!-- Grafica -->
                    <div class="col-md-12">
                        <div id="Contenedor_RutasVehiculo_GTV" style="min-width: 330px; height: 340px; margin: 0 auto; display: none; "></div>
                        <!-- <div class="panel panel-headline ">
                            <span> <b>INTERPRETACIÓN:</b>  Se desea que la cantidad de viajes en cada ruta aumente.</span>
                        </div> -->
                    </div>
    
                </div>
            </div>
            <!-- <div class="panel panel-headline "> -->
                   <span> <b>INTERPRETACIÓN:</b>  Se desea que la cantidad de viajes en cada ruta aumente.</span>
            <!-- </div> -->
    </div>

    <!-- RUTA DE VEHICULOS SEGUN RUTA POR TIEMPO -->
  
        <div class="col-lg-12  main-chart" style="margin-top:20px; ">
            <div class="panel panel-headline" style="margin-bottom: 3px !important;" >
                <div class="panel-body" class="col-lg-6 main-chart" >
                    <div class="grid-container">
    
                        <div class="row">
    
                            <div class="col-md-12 ">
    
                                <!-- OPCIONES-->
                                <div class="col-md-8">
                                    
                                     <!--Año-->
                                     <div class="col-md-4">
                                        <label for=""> Tipo Vehiculo:</label>
                                         <asp:DropDownList ID="cmbTipo_GRT" ClientIDMode="Static"  class="form-control" runat="server" Style="width: 100%;" ></asp:DropDownList>
                                    </div>

                                    <!--Año-->
                                    <div class="col-md-2">
                                        <label for=""> Año:</label>
                                         <asp:DropDownList ID="cmbAño_GRT" ClientIDMode="Static"  class="form-control" runat="server" Style="width: 100%;" ></asp:DropDownList>
    
                                    </div>
    
                                    <!--Mes-->
                                    <div class="col-md-2">
                                        <label for="">Mes:</label>
                                        <asp:DropDownList ID="cmbMes_GRT" ClientIDMode="Static"  class="form-control" runat="server" Style="width: 100%;" > </asp:DropDownList>
                                    </div>
    
                                    <!-- AGRUPACIONES-->
                                    <div class="col-lg-4 col-md-4 col-sm-4 ">
    
                                     <label for="formGroupExampleInput">Agrupacion:</label>
                                        <!--Trimestre-->
                                        <div class="col-md-12">
                                            <label class="fancy-checkbox">
                                                <input type="checkbox" id="opc_trimestreGRT">
                                                 <span> Trimestre </span>
                                            </label>
                                        </div>
    
                                        <div class="col-md-12">
                                            <!--Bimestre-->
                                            <label class="fancy-checkbox">
                                                <input type="checkbox" id="opc_bimestreGRT">
                                                 <span> Bimestre </span>
                                             </label>
                                        </div>
                                    </div>
    
    
                                </div>
    
                                <!-- BOTONES-->
                                <div class="col-md-4 ">
                                    <!--GRAFICAR-->
                                    <div class="col-md-12 ">
                                        <button id="btn_Graficar_RutasVehiculo_GRT" type="button" class="btn btn-primary btn-sm">GRAFICAR</button>
                                    </div>
    
                                    <!--LIMPIAR-->
                                    <div class="col-md-12 " >
                                        <label>Limpiar:</label>
                                        <span class="lnr lnr-magic-wand-limpiar" id="btn_Limpiar_GRT"> </span>
                                    </div>
                                </div>
                            </div>
    
                        </div>
                    </div>
                </div>
            </div>
    
            <div class="panel panel-headline" style="margin-bottom: 1px !important;">
                <div class="panel-body" class="col-lg-5 main-chart">
                    <!-- Grafica -->
                    <div class="col-md-12">
                            <div id="Contenedor_RutasVehiculo_GRT" style="min-width: 310px; height: 350px; margin: 0 auto; display: none; "></div>
                    </div>
    
                </div>
            </div>
            
            <span> <b>INTERPRETACIÓN:</b>   Se desea que la cantidad de viajes en cada ruta aumente.</span>
              
        </div>



        <div class="col-lg-12 container-indicador2" style="margin-top: 20px;">
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-headline">
                        <div class="panel-body children2">
                            <div class="form-inline container-kpi">
        
                                <div class="form-group children2">
                                    <label for="formGroupExampleInput">Año:</label>
                                    <asp:DropDownList ID="cmbAño_CVR" ClientIDMode="Static"  class="form-control" runat="server" Style="width: 100%;" ></asp:DropDownList>

                                </div>
        
                                <div class="form-group children2">
                                    <label for="formGroupExampleInput">Mes:</label>
                                    <asp:DropDownList ID="cmbMes_CVR" ClientIDMode="Static"  class="form-control" runat="server" Style="width: 100%;" ></asp:DropDownList>

                                </div>
        
                               
                                <div class="form-group children2">
                                    <label for="formGroupExampleInput">Agrupación:</label>
                                    <div class="children">
                                        <label class="fancy-checkbox children-group">
                                            <input type="checkbox" id="opc_trimestreCVR">
                                            <span>Trimestre</span>
                                        </label>
        
                                        <label class="fancy-checkbox children-group">
                                            <input type="checkbox" id="opc_bimestreCVR">
                                            <span>Bimestre</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
        
                            <div class="form-inline container-kpi2">
        
                                    <div class="form-group children">
                                        <label for="formGroupExampleInput">Limpiar:</label><br />
                                        <span class="lnr lnr-magic-wand-limpiar" id="btn_Limpiar_CVR"></span>
                                    </div>
        
                                    <div class="form-group children-b ">
                                        <button id="btn_Graficar_RutasVehiculo_CVR" type="button" class="btn btn-primary btn-sm">GRAFICAR</button>
                                    </div>
                            </div>
                        </div>
                    </div>
        
                    <div class="row panel-body">
                            <div id="Contenedor_RutasVehiculo_CVR" style="min-width: 310px; height: 380px; margin: 0 auto; display: none"></div>
                         </div>
                
                        <div class="text-left Left aligned text">
                             <p><strong>Interpretacion:</strong> Se desea que la cantidad de viajes en cada ruta aumente</p>
                    </div>
        
                </div>
            </div>
          
        </div>
    
    



</asp:Content>
