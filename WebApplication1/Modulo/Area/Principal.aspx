﻿<%@ Page Title="" Language="C#" MasterPageFile="/MasterPage/Intranet.Master" AutoEventWireup="true" CodeBehind="Principal.aspx.cs" Inherits="WebApplication1.Modulo.Dashboard" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
        <link href="/Recurso2/HighChart/code/css/themes/sand-signika.css" rel="stylesheet" />
        <script src="/Recurso2/HighChart/code/themes/sand-signika.js"></script>
        <script src="/Recurso2/Base/Modulo/Venta.js"></script>
        <script src="/Recurso2/Base/Modulo/Mantenimiento.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContenedorDashboard" runat="server">
            <div class="row">
            <div class="col-lg-8 col-md-10">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-6">
                        <div id="G_Ventas_VVA_S_P" style="min-width: 310px; max-width: 800px; height: 180px; margin: 0 auto"></div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-6">  
                            <div id="G_Ventas_VVA_P_P" style="min-width: 310px; max-width: 800px; height: 180px; margin: 0 auto"></div>
                    </div>
                </div>

            </div>

            <div class="col-lg-4 col-md-6 col-6">  
                    <div id="Grafica_Productividad_KPI" style="min-width: 310px; max-width: 800px; height: 240px; margin: 0 auto"></div>
            </div>

        </div>

    <div class="row">
            <div class="col-lg-4 col-md-6 col-6">  
                    <div id="G_Falla_KPI_Promedio" style="min-width: 310px; max-width: 800px; height: 200px; margin: 0 auto"></div>
            </div>
    </div>
      <script>
          $(document).ready(function () {
              GraficarVentas_S("G_Ventas_VVA_S_P");
              GraficarVentas_P("G_Ventas_VVA_P_P");
              GraficarDatos_Productividad_KPI("", "Grafica_Productividad_KPI");
              GraficarDatos_PieChart("", "", "", "G_Falla_KPI_Promedio");
          });

          //Dinero Genreado en ventas: Millones de Soles
          function GraficarVentas_S(divGrafica) {

              var sucess = function (response) {
                  Grafica_Ventas_VVA_S(response.d.Marcas, response.d.Data, divGrafica);

              };
              var error = function (xhr, ajaxOptions, thrownError) {
                  //fn_message('e', 'Ocurrio un error mientras se cargaban los datos.');
              };
              fn_LlamadoMetodo("Venta/InformeVenta.aspx/Grafica_Informes_Venta_S", JSON.stringify({}), sucess, error);
          }

          function GraficarVentas_P(divGrafica) {
              var sucess = function (response) {
                  Grafica_Ventas_VVA_P(response.d.Marcas, response.d.Data, divGrafica);
                  //$('#G_Ventas_VVA_P').show();
              };
              var error = function (xhr, ajaxOptions, thrownError) {
                  //fn_message('e', 'Ocurrio un error mientras se cargaban los datos.');
              };
              fn_LlamadoMetodo("Venta/InformeVenta.aspx/Grafica_Informes_Venta_P", JSON.stringify({}), sucess, error);
          }

          //Productividad anual
          function GraficarDatos_Productividad_KPI(anio, idDiv) {
              var sucess = function (response) {
                  var json = JSON.parse(response.d.Series);
                  for (let i = 0; i < json.length; i++) {
                      var x = json[i].data;
                      for (let j = 0; j < x.length; j++) {
                          if (x[j] === 0) {
                              x[j] = null;
                          }
                          else {
                              x[j] = x[j];
                          }
                      }
                  }
                  Graficar_Productividad_KPI(response.d.Fecha, response.d.Name, JSON.stringify(json), idDiv);
              };
              var error = function (xhr, ajaxOptions, thrownError) {
                  //fn_message('e', 'Ocurrio un error mientras se cargaban los datos.');
              };
              fn_LlamadoMetodo("Mantenimiento/Productividad.aspx/Grafica_Productividad_KPI", JSON.stringify({ anio: anio }), sucess, error);
          }

          //Porcentaje Falla del sistema:
          function GraficarDatos_PieChart(filtroMarca, fechaInicio, fechaFin, idDiv) {
              var sucess = function (response) {
                  PieChart(response.d.Series, idDiv);
              };
              var error = function (xhr, ajaxOptions, thrownError) {
                  //fn_message('e', 'Ocurrio un error mientras se cargaban los datos.');
              };
              fn_LlamadoMetodo("Mantenimiento/FallaSistema.aspx/Grafica_Falla_Promedio_Sistema", JSON.stringify({ filtroMarca: filtroMarca, fechaInicio: fechaInicio, fechaFin: fechaFin }), sucess, error);
          }

      </script>



</asp:Content>
