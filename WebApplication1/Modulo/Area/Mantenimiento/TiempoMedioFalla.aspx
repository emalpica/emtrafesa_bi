﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Intranet.Master" AutoEventWireup="true" CodeBehind="TiempoMedioFalla.aspx.cs" Inherits="WebApplication1.Modulo.Area.Mantenimiento.TiempoMedioFalla" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContenedorDashboard" runat="server">

        <script>
        var anio1 = "";var filtroMarca1 = "";  var anio = ""; var mes = "";

            GraficarDatos_Anual("", "");
            GraficarDatos("", "");
            fn_CargarCombos(anio, mes);

        $(document).ready(function () {
            $("#G_Tiempo_Medio_Fallas").click(function () {
                ValidarCampo();
                fn_CargarCombos( anio, mes);
                GraficarDatos(anio, mes);
         
            });
            $("#cmbAnio").change(function () {
                ValidarCampo();
                fn_CargarCombos( anio, mes);
            });
            $("#cmbMes").change(function () {
                ValidarCampo();
                fn_CargarCombos( anio, mes);
            });
            $("#G_Tiempo_Medio_Fallas_Anual").click(function () {
               filtroMarca1 = document.getElementById("ddlMarca1").value; if (filtroMarca1 == 0) { filtroMarca1 = ''; }
               //anio1 = (ddlAnio.value).substr(6, 7); if (anio1 == 0) { anio1 = ''; }
                GraficarDatos_Anual(filtroMarca1, anio1);
            });
         
        });
            function fn_CargarCombos(anio, mes) {
               if (anio == 0 || anio == "") fn_General_Cargar_FiltroAnio( mes);
                if (mes == 0 || mes == "") fn_General_Cargar_FiltroMes( anio);
            }

            function ValidarCampo() {
               anio = $("#cmbAnio").val(); if (anio != 0) { anio = $("#cmbAnio option:selected").text(); } else { anio = ""; }
                mes = $("#cmbMes").val(); if (mes != 0) { mes = $("#cmbMes option:selected").text(); } else { mes = ""; }
            }
            function fn_General_Cargar_FiltroAnio(mes) {
                var html = '';
                var sucess = function (response) {
                    var obj = JSON.parse(response.d);
                    var data_html = "";
                    if (obj == null) {
                        data_html = "<option value='0'>Seleccione Opción</option>";
                        $("select[id$=cmbAnio]").html(data_html);
                    }
                    else {
                        data_html += "<option value='0'>Seleccione Opción</option>";
                        for (var i = 0; i < obj.length; i++) {
                            data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>";
                        }
                        $("select[id$=cmbAnio]").html(data_html);
                    }
                };
                var error = function (xhr, ajaxOptions, thrownError) {
                };
                fn_LlamadoMetodo("TiempoMedioFalla.aspx/Mantenimiemto_Tiempo_Fallas_Fallas_anio_CS", JSON.stringify({ mes: mes }), sucess, error);
            }
            function fn_General_Cargar_FiltroMes(anio) {
                var html = '';
                var sucess = function (response) {
                    var obj = JSON.parse(response.d);
                    var data_html = "";
                    if (obj == null) {
                        data_html = "<option value='0'>Seleccione Opción</option>";
                        $("select[id$=cmbMes]").html(data_html);
                    }
                    else {
                        data_html += "<option value='0'>Seleccione Opción</option>";
                        for (var i = 0; i < obj.length; i++) {
                            data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>";
                        }
                        $("select[id$=cmbMes]").html(data_html);
                    }
                };
                var error = function (xhr, ajaxOptions, thrownError) {
                };
                fn_LlamadoMetodo("TiempoMedioFalla.aspx/Mantenimiemto_Tiempo_Fallas_Fallas_Mes_CS", JSON.stringify({anio: anio }), sucess, error);
            }

        function GraficarDatos_Anual(filtroMarca1, anio1) {
            var sucess = function (response) {
            var json = JSON.parse(response.d.Series);
              for (let i = 0; i < json.length; i++) {
                  var x = json[i].data;
                  for (let j = 0; j < x.length; j++) {
                      if (x[j] === 0) {
                          x[j] = null;
                      }
                      else
                      {
                          x[j] = x[j];
                      }
                  }
              }
                Grafica_TMF_Linea_Anual(response.d.Fecha, response.d.Name, JSON.stringify(json));
            };
            var error = function (xhr, ajaxOptions, thrownError) {
                //fn_message('e', 'Ocurrio un error mientras se cargaban los datos.');
            };
            fn_LlamadoMetodo("TiempoMedioFalla.aspx/Grafica_Tiempo_Medio_Falla_Anual", JSON.stringify({ filtroMarca1: filtroMarca1, anio1:anio1 }), sucess, error);
        }

        function Grafica_TMF_Linea_Anual(fecha, Name, data) {
            Highcharts.chart('Grafica_Tiempo_Medio_Fallas1', {
                chart: {
                    type: 'line'
                },
                title: {
                    text:  'TIEMPO MEDIO ACUMULATIVO ENTRE FALLAS DE LA FLOTA EN GENERAL'
                },
                xAxis: {

                    categories: ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'] //JSON.parse(fecha)
                },
                yAxis: {
                    min: 0,
                    labels: {
                        format: '{value} d '
                    },
                    title: {
                        text: 'DIAS'
                    },
                },
                plotOptions: {
                  line: {
                      dataLabels: {
                          enabled: true,
                          format: '{point.y:.1f} '
                      },
                      enableMouseTracking: true
                  }
              },
                series: JSON.parse(data)
            });
        }

        function GraficarDatos(anio, mes) {
            var sucess = function (response) {
            var json = JSON.parse(response.d.Series);
              for (let i = 0; i < json.length; i++) {
                  var x = json[i].data;
                  for (let j = 0; j < x.length; j++) {
                      if (x[j] === 0) {
                          x[j] = null;
                      }
                      else
                      {
                          x[j] = x[j];
                      }
                  }
              }
                Grafica_TMF_Linea(response.d.Fecha, response.d.Name, JSON.stringify(json));
            };
            var error = function (xhr, ajaxOptions, thrownError) {
                //fn_message('e', 'Ocurrio un error mientras se cargaban los datos.');
            };
            fn_LlamadoMetodo("TiempoMedioFalla.aspx/Grafica_Tiempo_Medio_Falla", JSON.stringify({ anio: anio, mes: mes }), sucess, error);
        }

        function Grafica_TMF_Linea(fecha, Name, data) {
            Highcharts.chart('Grafica_Tiempo_Medio_Fallas', {
                chart: {
                    type: 'line'
                },
                title: {
                    text: 'KPI HISTÓRICO DE TIEMPO MEDIO ACUMULATIVO ENTRE FALLAS '
                },
                xAxis: {
                    categories: JSON.parse(fecha)
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'DIAS'
                    },
                    labels: {
                        format: '{value} d '
                    },
                },
                plotOptions: {
                    line: {
                        dataLabels: {
                            enabled: true
                        },
                        enableMouseTracking: true
                    }
              },
                series: JSON.parse(data)
            });
        }

        </script>

    <h3 class="panel-body" style="COLOR: #283593;margin-top: 0px;">TIEMPO MEDIO ACUMULATIVO ENTRE FALLAS</h3>
        <div class="text-left Left aligned text" style="margin-left: 18px;">
        <p><strong>Objetivo:</strong>El objetivo del indicador de fallas según sistema es mostrar los sistemas de buses que sufren más desperfectos para que se pueda tener un mejor control y seguimiento a los mismos.</p>
    </div>
    <div class="col-lg-6">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-headline">
                <div class="panel-body">
                    <div class="form-inline">
                
                
                        <div class="form-group col-lg-4 col-md-4 col-md-4">
                                <label for="formGroupExampleInput">Año:</label>
                                <asp:DropDownList ID="cmbAnio" ClientIDMode="Static" class="form-control" runat="server" Style="width: 100%;"></asp:DropDownList>
                            </div>
                              <div class="form-group col-lg-4 col-md-4 col-md-4">
                                <label for="formGroupExampleInput">Mes:</label>
                                <asp:DropDownList ID="cmbMes" ClientIDMode="Static" class="form-control" runat="server" Style="width: 100%;"></asp:DropDownList>
                            </div>

                    <div class="form-group col-lg-4 col-md-4 col-sm-12">
                        <div class="col-sm-3 mb">
                            <br />
                            <button id="G_Tiempo_Medio_Fallas" type="button" class="btn btn-primary btn-sm">GRAFICAR</button>
                        </div>
                    </div>
                            </div>
                </div>
            </div>

        </div>
    </div>
        
      <div class="container-indicador">
        <div class="row">
          <div class="col-lg-12">
            <div class="panel panel-headline">
                <div class="panel-body">
                    <div class="form-inline container-kpi">
        <div id="Grafica_Tiempo_Medio_Fallas" style="min-width: 90%; height: 400px; margin: 0 auto;"></div>
                    </div>
                       </div>
                </div>
             </div>
           </div>
          </div>  
        </div>


        <div class="col-lg-6">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-headline">
                <div class="panel-body">
                    <div class="form-inline container-kpi">

<%--                            <div class="form-group col-lg-4 col-md-4 col-sm-12">
                                <label for="formGroupExampleInput">Año:</label>
                                    <asp:DropDownList ID="ddlAnio" ClientIDMode="Static" class="form-control" runat="server" Style="width: 100%;"></asp:DropDownList>
                            </div>--%>

                        <div class="form-group col-lg-4 col-md-4 col-sm-12">
                                <label for="formGroupExampleInput">Marca:</label>

                                    <asp:DropDownList ID="ddlMarca1" ClientIDMode="Static" class="form-control" runat="server" Style="width: 100%;"></asp:DropDownList>

                            </div>


                        <div class="form-group col-lg-3 col-md-3 col-sm-12">

                                <br />
                                <button id="G_Tiempo_Medio_Fallas_Anual" type="button" class="btn btn-primary btn-sm">GRAFICAR</button>

                        </div>


                    </div>
                    </div>
                </div>
            </div>
        </div>


         <div class="container-indicador">
        <div class="row">
          <div class="col-lg-12">
            <div class="panel panel-headline">
                <div class="panel-body">
                    <div class="form-inline container-kpi">
            <div id="Grafica_Tiempo_Medio_Fallas1" style="min-width: 90%; height: 400px; margin: 0 auto;"></div>
                    </div>
               </div>
            </div>
        </div>
    </div>
             </div>

</div>


</asp:Content>
