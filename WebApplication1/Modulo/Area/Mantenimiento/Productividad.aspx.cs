﻿using ENTIDAD.Graficas;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI.WebControls;

namespace WebApplication1.Modulo.Area.Mantenimiento
{
    public partial class Productividad : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Listar_Anio_Productividad();
        }

        [WebMethod]
        public static object Grafica_Productividad_KPI(String anio)
        {
            List<Object> contenedor = new List<Object>();
            List<String> Columnas = new List<String>(); //nombre de marca
            List<Ratio> series = new List<Ratio>();
            DataTable dt = LOGICA.MantenimientoL.Instancia.Grafica_Productividad_KPI(anio);
            LineChart data = new LineChart();
            Ratio ratio = new Ratio();

            if (dt != null)
            {
                foreach (DataColumn dataColumn in dt.Columns)
                {
                    Columnas.Add(dataColumn.ColumnName);
                }
                int j = 1;

                foreach (DataRow dr in dt.Rows)
                {
                    j = 1;
                    data.Fecha.Add(dr[0].ToString());
                    data.Name = Columnas;

                    while (j < Columnas.Count)
                    {
                        if (data.Series.Count < Columnas.Count - 1)
                            data.Series.Add(new Ratio());
                        string value = dr[j].ToString();
                        if (value == null || value == "") { data.Series[j - 1].data.Add(Convert.ToDecimal(0)); }
                        data.Series[j - 1].data.Add(Convert.ToDecimal(value) * 100);
                        data.Series[j - 1].name = Columnas[j];
                        j++;
                    }
                }
                JavaScriptSerializer sr = new JavaScriptSerializer();
                return new { Fecha = sr.Serialize(data.Fecha), Name = sr.Serialize(data.Name), Series = sr.Serialize(data.Series), };
            }
            else { return null; }

        }

        private void Listar_Anio_Productividad()
        {
            ListItem i;
            DataTable filtro_marca = LOGICA.MantenimientoL.Instancia.Listar_Anio_Productividad();
            ddlAnio.DataTextField = "Anio";
            ddlAnio.DataValueField = "Anio";
            ddlAnio.Items.Insert(0, new ListItem("- Seleccione Año -", "0"));
            foreach (DataRow r in filtro_marca.Rows)
            {
                i = new ListItem(r["Anual"].ToString());
                ddlAnio.Items.Add(i);
            }
            ddlAnio.SelectedIndex = 0;
        }

    }
}