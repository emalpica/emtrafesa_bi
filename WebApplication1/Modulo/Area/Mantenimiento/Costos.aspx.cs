﻿using ENTIDAD.Graficas;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI.WebControls;

namespace WebApplication1.Modulo.Area.Mantenimiento
{
    public partial class Costos : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        
        }

        // TODO: ##### GRAFICA KPI COSTOS MONTO TOTAL - LINECHART

        // ** LISTAR EL MES - COSTOS REPUESTOS MANTENIMIENTO

      
         [WebMethod]
        public static object Mantenimiento_CostoPersonalRepuestos_ListarMes_CS(String Anio)
        {
            DataTable filtro_Mes = LOGICA.MantenimientoL.Instancia.Mantenimiento_CostoPersonalRepuestos_ListarMes_LO(Anio);
            JavaScriptSerializer sr = new JavaScriptSerializer();
            List<string> ddlMes = new List<string>();

            if (filtro_Mes != null)
            {
                for (int i = 0; i < filtro_Mes.Rows.Count; i++)
                {
                    if ((filtro_Mes.Rows[i]["Mes"].ToString()) == "NULL")
                    {
                        ddlMes.Add("NO INDICA");
                    }
                    else
                    {
                        ddlMes.Add(filtro_Mes.Rows[i]["Mes"].ToString());
                    }

                }
            }
            else
            {
                return null;
            }
            return sr.Serialize(ddlMes);

        }

        [WebMethod]
        public static object Mantenimiento_CostoPersonalRepuestos_ListarAnio_CS(String Mes){
            
            JavaScriptSerializer sr = new JavaScriptSerializer();
            List<string> ddlAnio = new List<string>();
            DataTable dt_filtro_Anio= LOGICA.MantenimientoL.Instancia.Mantenimiento_CostoPersonalRepuestos_ListarAño_LO(Mes);
            if(dt_filtro_Anio!=null){
                for(int i=0; i<dt_filtro_Anio.Rows.Count; i++){
                    if((dt_filtro_Anio.Rows[i]["Año"].ToString()) == "NULL"){
                        ddlAnio.Add("NO INDICA");
                    }
                    else{
                        ddlAnio.Add(dt_filtro_Anio.Rows[i]["Año"].ToString());
                    }
                }

            }
            else{
                return null;
            }

            return sr.Serialize(ddlAnio);
            
        }

        [WebMethod]
        public static object Grafica_Mantenimiento_CostosPersonalRepuestos_LCH_CS(String Anio, String Mes, String agrupacionF)
        {
           
            int indiceColum = 0;
            int agrupacion = Convert.ToInt32(agrupacionF);
            List<Object> contenedor = new List<Object>();
            List<String> Columnas = new List<String>(); //nombre de marca
            List<Ratio> series = new List<Ratio>();
            DataTable dt = LOGICA.MantenimientoL.Instancia.Grafica_Mantenimiento_CostosPersonalRepuestos_LCH_LO(Anio, Mes);
            LineChart data = new LineChart();

            if (dt != null)
            {
                foreach (DataColumn dataColumn in dt.Columns)
                {
                    Columnas.Add(dataColumn.ColumnName);
                }
                int j = 1;

                foreach (DataRow dr in dt.Rows)
                {
                    j = 1;
                    data.Fecha.Add(dr[0].ToString());
                    data.Name = Columnas;

                    while (j < Columnas.Count)
                    {
                        if (data.Series.Count < Columnas.Count - 1)
                            data.Series.Add(new Ratio());
                        string value = dr[j].ToString();

                        data.Series[j - 1].data.Add(Convert.ToDecimal(value));
                        data.Series[j - 1].name = Columnas[j];
                        j++;
                    }
                }
                if (agrupacion == 0)
                {
                    JavaScriptSerializer sr = new JavaScriptSerializer();
                    return new { Fecha = sr.Serialize(data.Fecha), Name = sr.Serialize(data.Name), Series = sr.Serialize(data.Series), };
                }
            }
            //trimestre and bmestre
            String fechaAnio = "";
            Decimal acumulador = 0;
            Decimal total = 0;
            int contadorTrimestre = 1;
            String anioActual = "";
            String nombreAgrupacion = "";
            LineChart data1 = new LineChart();
            if (agrupacion != 0)
            {
                if (agrupacion == 2) { nombreAgrupacion = "Bimestre "; }
                if (agrupacion == 3) { nombreAgrupacion = "Trimestre "; }
                for (int m = 0; m < data.Fecha.Count; m++)
                {


                    if (fechaAnio != anioActual) { contadorTrimestre = 1; }
                    if (m % agrupacion == 0)
                    {
                        data1.Fecha.Add(nombreAgrupacion + (contadorTrimestre).ToString());
                        contadorTrimestre++;
                    }
                    else
                    {
                        if (m == data.Fecha.Count - 1)
                        {
                            data1.Fecha.Add(nombreAgrupacion + (contadorTrimestre).ToString());
                            contadorTrimestre++;
                        }
                    }

                }

                int contadorObjeto = 0;
                for (int i = 0; i < data.Series.Count; i++)
                {
                    data1.Series.Add(new Ratio());
                    for (int j = 0; j < data.Series[i].data.Count; j++)
                    {
                        contadorObjeto = j + 1;
                        var value = data.Series[i].data[j];
                        acumulador = acumulador + value;
                        if (contadorObjeto % agrupacion == 0 && contadorObjeto != 0)
                        {
                            total = acumulador / agrupacion;
                            data1.Series[i].data.Add(Math.Round(total, 1));
                            acumulador = 0;
                            total = 0;
                        }
                        else
                        {
                            if (contadorObjeto == data.Series[i].data.Count)
                            {
                                total = acumulador / agrupacion;
                                data1.Series[i].data.Add(Math.Round(total, 1));
                                acumulador = 0;
                                total = 0;
                            }
                        }

                    }
                    data1.Series[i].name = Columnas[i + 1];
                }
                JavaScriptSerializer sr = new JavaScriptSerializer();
                return new { Fecha = sr.Serialize(data1.Fecha), Name = sr.Serialize(data.Name), Series = sr.Serialize(data1.Series) };
            }
            else { return null; }
        }

       // TODO: ##### GRAFICA KPI COSTOS MONTO TOTAL - GROUPEDCHART

        // ** LISTAR EL MES - COSTOS REPUESTOS MANTENIMIENTO

      
         [WebMethod]
        public static object Mantenimiento_CostoPersonalRepuestos_ListarMes_GCH_CS(String anio2)
        {
            DataTable filtro_Mes = LOGICA.MantenimientoL.Instancia.Mantenimiento_CostoPersonalRepuestos_ListarMes_GCH_LO(anio2);
            JavaScriptSerializer sr = new JavaScriptSerializer();
            List<string> ddlMes2 = new List<string>();

            if (filtro_Mes != null)
            {
                for (int i = 0; i < filtro_Mes.Rows.Count; i++)
                {
                    if ((filtro_Mes.Rows[i]["Mes"].ToString()) == "NULL")
                    {
                        ddlMes2.Add("NO INDICA");
                    }
                    else
                    {
                        ddlMes2.Add(filtro_Mes.Rows[i]["Mes"].ToString());
                    }

                }
            }
            else
            {
                return null;
            }
            return sr.Serialize(ddlMes2);

        }

        [WebMethod]
        public static object Mantenimiento_CostoPersonalRepuestos_ListarAño_GCH_CS(String mes2){
            
            JavaScriptSerializer sr = new JavaScriptSerializer();
            List<string> ddlAnio2 = new List<string>();
            DataTable dt_filtro_Anio= LOGICA.MantenimientoL.Instancia.Mantenimiento_CostoPersonalRepuestos_ListarAño_GCH_LO(mes2);
            if(dt_filtro_Anio!=null){
                for(int i=0; i<dt_filtro_Anio.Rows.Count; i++){
                    if((dt_filtro_Anio.Rows[i]["Año"].ToString()) == "NULL"){
                        ddlAnio2.Add("NO INDICA");
                    }
                    else{
                        ddlAnio2.Add(dt_filtro_Anio.Rows[i]["Año"].ToString());
                    }
                }

            }
            else{
                return null;
            }

            return sr.Serialize(ddlAnio2);
            
        }

        [WebMethod]
        public static object Grafica_Mantenimiento_Costos_TotalMes_GCH_CS(String anio2, String mes2, String agrupacionF)
        {
           
            int indiceColum = 0;
            int agrupacion = Convert.ToInt32(agrupacionF);
            List<Object> contenedor = new List<Object>();
            List<String> Columnas = new List<String>(); //nombre de marca
            List<Ratio> series = new List<Ratio>();
            DataTable dt = LOGICA.MantenimientoL.Instancia.Grafica_Mantenimiento_Costos_TotalMes_GCH_LO(anio2, mes2);
            LineChart data = new LineChart();

            if (dt != null)
            {
                foreach (DataColumn dataColumn in dt.Columns)
                {
                    Columnas.Add(dataColumn.ColumnName);
                }
                int j = 1;

                foreach (DataRow dr in dt.Rows)
                {
                    j = 1;
                    data.Fecha.Add(dr[0].ToString());
                    data.Name = Columnas;

                    while (j < Columnas.Count)
                    {
                        if (data.Series.Count < Columnas.Count - 1)
                            data.Series.Add(new Ratio());
                        string value = dr[j].ToString();

                        data.Series[j - 1].data.Add(Convert.ToDecimal(value));
                        data.Series[j - 1].name = Columnas[j];
                        j++;
                    }
                }
                if (agrupacion == 0)
                {
                    JavaScriptSerializer sr = new JavaScriptSerializer();
                    return new { Fecha = sr.Serialize(data.Fecha), Name = sr.Serialize(data.Name), Series = sr.Serialize(data.Series), };
                }
            }
            //trimestre and bmestre
            String fechaAnio = "";
            Decimal acumulador = 0;
            Decimal total = 0;
            int contadorTrimestre = 1;
            String anioActual = "";
            String nombreAgrupacion = "";
            LineChart data1 = new LineChart();
            if (agrupacion != 0)
            {
                if (agrupacion == 2) { nombreAgrupacion = "Bimestre "; }
                if (agrupacion == 3) { nombreAgrupacion = "Trimestre "; }
                for (int m = 0; m < data.Fecha.Count; m++)
                {


                    if (fechaAnio != anioActual) { contadorTrimestre = 1; }
                    if (m % agrupacion == 0)
                    {
                        data1.Fecha.Add(nombreAgrupacion + (contadorTrimestre).ToString());
                        contadorTrimestre++;
                    }
                    else
                    {
                        if (m == data.Fecha.Count - 1)
                        {
                            data1.Fecha.Add(nombreAgrupacion + (contadorTrimestre).ToString());
                            contadorTrimestre++;
                        }
                    }

                }

                int contadorObjeto = 0;
                for (int i = 0; i < data.Series.Count; i++)
                {
                    data1.Series.Add(new Ratio());
                    for (int j = 0; j < data.Series[i].data.Count; j++)
                    {
                        contadorObjeto = j + 1;
                        var value = data.Series[i].data[j];
                        acumulador = acumulador + value;
                        if (contadorObjeto % agrupacion == 0 && contadorObjeto != 0)
                        {
                            total = acumulador / agrupacion;
                            data1.Series[i].data.Add(Math.Round(total, 1));
                            acumulador = 0;
                            total = 0;
                        }
                        else
                        {
                            if (contadorObjeto == data.Series[i].data.Count)
                            {
                                total = acumulador / agrupacion;
                                data1.Series[i].data.Add(Math.Round(total, 1));
                                acumulador = 0;
                                total = 0;
                            }
                        }

                    }
                    data1.Series[i].name = Columnas[i + 1];
                }
                JavaScriptSerializer sr = new JavaScriptSerializer();
                return new { Fecha = sr.Serialize(data1.Fecha), Name = sr.Serialize(data.Name), Series = sr.Serialize(data1.Series) };
            }
            else { return null; }
        }


    }
}