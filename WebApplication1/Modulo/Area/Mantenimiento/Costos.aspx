﻿
<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Intranet.Master" AutoEventWireup="true" CodeBehind="Costos.aspx.cs" Inherits="WebApplication1.Modulo.Area.Mantenimiento.Costos" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContenedorDashboard" runat="server">

    <!-- **********************************************************************************************************************************************************
      FUNCIONALIDAD  JAVASCRIPT
      *********************************************************************************************************************************************************** -->
    <!--main content start-->
    <script>

        
        //######  MANTENIMIENTO COSTOS - COSTO DEL PERSONAL Y REPUESTOS LINECHART  ######

        // TODO 1 : ZONA DE VARIABLES
        var AnioLCH = "";
        var MesLCH = ""; 
        var agrupacionLCH='';
        var data_html = "";

        //TODO 2 : LLAMADA A FUNCIONES YA IMPLEMENTADOS

        // !Llama a la funcion que se en carga de llenar los filtros
        CargarCombosGeneralesLCH(AnioLCH, MesLCH);
        Grafica_Mantenimiento_Costos_RepuestosPersonal_LCH("", "", "0");


        // TODO 3 : CAPTURAR ELEMENTOS MEDIANTE JQUERY
        $(document).ready(function () {

            $("#opc_trimestreLCH").change(function () {
                $("#opc_bimestreLCH").prop("checked", false);
                validarCombosLCH();
                Grafica_Mantenimiento_Costos_RepuestosPersonal_LCH(AnioLCH, MesLCH, agrupacionLCH);
                
            });

            $("#opc_bimestreLCH").change(function () {
                $("#opc_trimestreLCH").prop("checked", false);
                validarCombosLCH();
                Grafica_Mantenimiento_Costos_RepuestosPersonal_LCH(AnioLCH, MesLCH, agrupacionLCH);
            });

            // !Jquery que carga la sucursal
            $("#ddlAnio").change(function () {
                validarCombosLCH();
                CargarCombosGeneralesLCH(AnioLCH, MesLCH);
            });

            //!Jquery que carga el area
            $("#ddlMes").change(function () {
                validarCombosLCH();
                CargarCombosGeneralesLCH(AnioLCH, MesLCH);
            });

             // ! JQUERY que carga la grafica 
            $("#btn_Graficar_CostosRepuestos_LCH").click(function () {
                validarCombosLCH();
                Grafica_Mantenimiento_Costos_RepuestosPersonal_LCH(AnioLCH, MesLCH,agrupacionLCH);
                
            });

            $("#btn_Limpiar_LCH").click(function(){
                LimpiarDataLCH();
                validarCombosLCH();
                Grafica_Mantenimiento_Costos_RepuestosPersonal_LCH("","","0");

            });
        });

        // TODO 4 : FUNCIONES PARA INTERACTUAR CON LOS FILTROS (COMBOS)

        function CambiarEstadoLCH() {
            if (AnioLCH != 0) $("#ddlAnio").val(0); document.getElementById("ddlAnio").disabled = false;
            if (MesLCH != 0) $("#ddlMes").val(0); document.getElementById("ddlMes").disabled = false;
            $("#opc_trimestreLCH").prop("checked", false);
            $("#opc_bimestreLCH").prop("checked", false);
           
            validarCombosLCH();
            CargarCombosGeneralesLCH(AnioLCH, MesLCH);
            Grafica_Mantenimiento_Costos_RepuestosPersonal_LCH("", "","0");
            // Grafica_Mantenimiento_Costos_RepuestosPersonal_LCH("","");
        }

        // !Captura el valor seleccionado en el filtro area, subarea y  sucursal 
        function validarCombosLCH() {
            AnioLCH = $("#ddlAnio").val(); if (AnioLCH != 0) { AnioLCH= $("#ddlAnio  option:selected").text(); } else { AnioLCH = ""; }
            MesLCH = $("#ddlMes").val(); if (MesLCH != 0) { MesLCH = $("#ddlMes  option:selected").text(); } else { MesLCH = ""; }
            if ($("#opc_trimestreLCH").prop('checked')) { agrupacionLCH = "3";}
            if ($("#opc_bimestreLCH").prop('checked')) { agrupacionLCH = "2";}
            if ($("#opc_bimestreLCH").prop('checked') == false && $("#opc_trimestreLCH").prop('checked') == false) {agrupacionLCH = "0";}

            //  AreaEHGM = $("#cmbAreaEHGM").val(); if (AreaEHGM != 0) { AreaEHGM = $("#cmbAreaEHGM  option:selected").text(); } else { AreaEHGM = ""; }
            // DepartamentoEHGM = $("#cmbDepartamentoEHGM").val(); if (DepartamentoEHGM != 0) { DepartamentoEHGM = $("#cmbDepartamentoEHGM  option:selected").text(); } else { DepartamentoEHGM = ""; }
        }

        // ! Funcion que permite cargar datos a los combos
        function CargarCombosGeneralesLCH(AnioLCH, MesLCH) {
            if (AnioLCH == 0 || AnioLCH == "") fn_General_Cargar_FiltroAnio_LCH(MesLCH);
            if (MesLCH == 0 || MesLCH == "") fn_General_Cargar_FiltroMes_LCH(AnioLCH);
        }

        function LimpiarDataLCH() {
            
            // if(DepartamentoEHGM!=0) $('#cmbDepartamento_EHGM').val(0);
            if(AnioLCH!=0) $('#ddlAnio').val(0);
            if(MesLCH!=0) $('#ddlMes').val(0);
            CargarCombosGeneralesLCH();
        }


        //TODO 5: FUNCIONES QUE IMPLEMENTAN LLAMADAS AJAX 

        // ! Funcion para cargar los Aniosen el combo Anio
        function fn_General_Cargar_FiltroAnio_LCH(MesLCH) {
            var html = '';
            var sucess = function (response) {
                var obj = JSON.parse(response.d);
                var data_html = "";
                if (obj == null) {
                    data_html = "<option value='0'>Seleccionar</option>";
                    $("select[id$=ddlAnio]").html(data_html);
                }
                else {
                    data_html += "<option value='0'>Seleccionar</option>";
                    for (var i = 0; i < obj.length; i++) {
                        data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>";
                    }
                    $("select[id$=ddlAnio]").html(data_html);
                }
            };
            var error = function (xhr, ajaxOptions, thrownError) {
            };
            fn_LlamadoMetodo("Costos.aspx/Mantenimiento_CostoPersonalRepuestos_ListarAnio_CS", JSON.stringify({  Mes: MesLCH }), sucess, error);
        }

        // ! Funcion que carga los Meses en el combo Meses
        function fn_General_Cargar_FiltroMes_LCH(AnioLCH) {
            var html = '';
            var sucess = function (response) {
                var obj = JSON.parse(response.d);
                var data_html = "";
                if (obj == null) {
                    data_html = "<option value='0'>Seleccionar</option>";
                    $("select[id$=ddlMes]").html(data_html);
                }
                else {
                    data_html += "<option value='0'>Seleccionar</option>";
                    for (var i = 0; i < obj.length; i++) {
                        data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>";
                    }
                    $("select[id$=ddlMes]").html(data_html);
                }
            };
            var error = function (xhr, ajaxOptions, thrownError) {
            };
            fn_LlamadoMetodo("Costos.aspx/Mantenimiento_CostoPersonalRepuestos_ListarMes_CS", JSON.stringify({ Anio: AnioLCH }), sucess, error);
        }

    
        // TODO 6: FUNCION QUE IMPLEMENTA LA GRAFICA
             function  Grafica_Mantenimiento_Costos_RepuestosPersonal_LCH(AnioLCH, MesLCH, agrupacionLCH) {
             var sucess = function (response) {
             //fn_Mensaje_Alerta("Confirmacion", "Se esta ejecutando los datos", "", "Alert");
             //var json=response.d.Series;
             LineChart_G(response.d.Fecha, response.d.Name, fn_ValidarANulos(response.d.Series));
             $('#Grafica_Mantenimiento_Costo_TotalMes_LCH').show();

            };
                 var error = function (xhr, ajaxOptions, thrownError) {
            };
                 fn_LlamadoMetodo("Costos.aspx/Grafica_Mantenimiento_CostosPersonalRepuestos_LCH_CS", JSON.stringify({ Anio:AnioLCH, Mes:MesLCH , agrupacionF:agrupacionLCH}), sucess, error);
            }


        // TODO : FUNCION HIGHTCHART QUE PERMITE DAR FORMA A LA GRAFICA
       function LineChart_G(fecha, Name, data) {
         Highcharts.chart('Grafica_Mantenimiento_Costo_TotalMes_LCH', {
             chart: {
               
                 type: 'line'
             },
             title: {
                 text: 'COSTO DEL DEPARTAMENTO DE MANTENIMIENTO '
             },
             subtitle: {
                 text: 'COSTO TOTAL MENSUAL'
             },
             xAxis: {
                 categories: JSON.parse(fecha)
             },
             yAxis: {
                 min: 0,
                 title: {
                     text: 'MONTO SOLES S/ '
                 }
             },
             plotOptions: {
                 line: {
                     dataLabels: {
                         enabled: true
                     },
                     enableMouseTracking: true
                 }
             },
             series: JSON.parse(data)
         });
     }




     //######  MANTENIMIENTO COSTOS - COSTO DEL PERSONAL Y REPUESTOS GROUPCHART  ######

        // TODO 1 : ZONA DE VARIABLES
        var AnioGCH = "";
        var MesGCH= ""; 
        var agrupacionGCH='';
        var data_html = "";

        //TODO 2 : LLAMADA A FUNCIONES YA IMPLEMENTADOS

        // !Llama a la funcion que se en carga de llenar los filtros
        CargarCombosGeneralesGCH(AnioGCH, MesGCH);
        Grafica_Mantenimiento_Costos_RepuestosPersonal_GCH("", "", "0");


        // TODO 3 : CAPTURAR ELEMENTOS MEDIANTE JQUERY
        $(document).ready(function () {

            $("#opc_trimestreGCH").change(function () {
                $("#opc_bimestreGCH").prop("checked", false);
                validarCombosGCH();
                Grafica_Mantenimiento_Costos_RepuestosPersonal_GCH(AnioGCH, MesGCH, agrupacionGCH);
                
            });

            $("#opc_bimestreGCH").change(function () {
                $("#opc_trimestreGCH").prop("checked", false);
                validarCombosGCH();
                Grafica_Mantenimiento_Costos_RepuestosPersonal_GCH(AnioGCH, MesGCH, agrupacionGCH);
            });

            // !Jquery que carga la sucursal
            $("#ddlAnio2").change(function () {
                validarCombosGCH();
                CargarCombosGeneralesGCH(AnioGCH, MesGCH);
            });

            //!Jquery que carga el area
            $("#ddlMes2").change(function () {
                validarCombosGCH();
                CargarCombosGeneralesGCH(AnioGCH, MesGCH);
            });

             // ! JQUERY que carga la grafica 
            $("#btn_Graficar_CostosRepuestos_GCH").click(function () {
                validarCombosGCH();
                Grafica_Mantenimiento_Costos_RepuestosPersonal_GCH(AnioGCH, MesGCH,agrupacionGCH);
                
            });

            $("#btn_Limpiar_GCH").click(function(){
                LimpiarDataGCH();
                validarCombosGCH();
                Grafica_Mantenimiento_Costos_RepuestosPersonal_GCH("","","0");

            });
        });

        // TODO 4 : FUNCIONES PARA INTERACTUAR CON LOS FILTROS (COMBOS)

        function CambiarEstadoGCH() {
            if (AnioGCH != 0) $("#ddlAnio2").val(0); document.getElementById("ddlAnio2").disabled = false;
            if (MesGCH != 0) $("#ddlMes2").val(0); document.getElementById("ddlMes2").disabled = false;
            $("#opc_trimestreGCH").prop("checked", false);
            $("#opc_bimestreGCH").prop("checked", false);
           
            validarCombosGCH();
            CargarCombosGeneralesGCH(AnioGCH, MesGCH);
            Grafica_Mantenimiento_Costos_RepuestosPersonal_GCH("", "","0");
            // Grafica_Mantenimiento_Costos_RepuestosPersonal_GCH("","");
        }

        // !Captura el valor seleccionado en el filtro area, subarea y  sucursal 
        function validarCombosGCH() {
            AnioGCH = $("#ddlAnio2").val(); if (AnioGCH != 0) { AnioGCH= $("#ddlAnio2  option:selected").text(); } else { AnioGCH = ""; }
            MesGCH = $("#ddlMes2").val(); if (MesGCH != 0) { MesGCH = $("#ddlMes2  option:selected").text(); } else { MesGCH = ""; }
            if ($("#opc_trimestreGCH").prop('checked')) { agrupacionGCH = "3";}
            if ($("#opc_bimestreGCH").prop('checked')) { agrupacionGCH = "2";}
            if ($("#opc_bimestreGCH").prop('checked') == false && $("#opc_trimestreGCH").prop('checked') == false) {agrupacionGCH = "0";}

            //  AreaEHGM = $("#cmbAreaEHGM").val(); if (AreaEHGM != 0) { AreaEHGM = $("#cmbAreaEHGM  option:selected").text(); } else { AreaEHGM = ""; }
            // DepartamentoEHGM = $("#cmbDepartamentoEHGM").val(); if (DepartamentoEHGM != 0) { DepartamentoEHGM = $("#cmbDepartamentoEHGM  option:selected").text(); } else { DepartamentoEHGM = ""; }
        }

        // ! Funcion que permite cargar datos a los combos
        function CargarCombosGeneralesGCH(AnioGCH, MesGCH) {
            if (AnioGCH == 0 || AnioGCH == "") fn_General_Cargar_FiltroAnio_GCH(MesGCH);
            if (MesGCH == 0 || MesGCH == "") fn_General_Cargar_FiltroMes_GCH(AnioGCH);
        }

        function LimpiarDataGCH() {
            
            // if(DepartamentoEHGM!=0) $('#cmbDepartamento_EHGM').val(0);
            if(AnioGCH!=0) $('#ddlAnio2').val(0);
            if(MesGCH!=0) $('#ddlMes2').val(0);
            CargarCombosGeneralesGCH();
        }


        //TODO 5: FUNCIONES QUE IMPLEMENTAN LLAMADAS AJAX 

        // ! Funcion para cargar los Aniosen el combo Anio
        function fn_General_Cargar_FiltroAnio_GCH(MesGCH) {
            var html = '';
            var sucess = function (response) {
                var obj = JSON.parse(response.d);
                var data_html = "";
                if (obj == null) {
                    data_html = "<option value='0'>Seleccionar</option>";
                    $("select[id$=ddlAnio2]").html(data_html);
                }
                else {
                    data_html += "<option value='0'>Seleccionar</option>";
                    for (var i = 0; i < obj.length; i++) {
                        data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>";
                    }
                    $("select[id$=ddlAnio2]").html(data_html);
                }
            };
            var error = function (xhr, ajaxOptions, thrownError) {
            };
            fn_LlamadoMetodo("Costos.aspx/Mantenimiento_CostoPersonalRepuestos_ListarAño_GCH_CS", JSON.stringify({  mes2: MesGCH }), sucess, error);
        }

        // ! Funcion que carga los Meses en el combo Meses
        function fn_General_Cargar_FiltroMes_GCH(AnioGCH) {
            var html = '';
            var sucess = function (response) {
                var obj = JSON.parse(response.d);
                var data_html = "";
                if (obj == null) {
                    data_html = "<option value='0'>Seleccionar</option>";
                    $("select[id$=ddlMes2]").html(data_html);
                }
                else {
                    data_html += "<option value='0'>Seleccionar</option>";
                    for (var i = 0; i < obj.length; i++) {
                        data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>";
                    }
                    $("select[id$=ddlMes2]").html(data_html);
                }
            };
            var error = function (xhr, ajaxOptions, thrownError) {
            };
            fn_LlamadoMetodo("Costos.aspx/Mantenimiento_CostoPersonalRepuestos_ListarMes_GCH_CS", JSON.stringify({ anio2: AnioGCH }), sucess, error);
        }

    
        // TODO 6: FUNCION QUE IMPLEMENTA LA GRAFICA
             function  Grafica_Mantenimiento_Costos_RepuestosPersonal_GCH(AnioGCH, MesGCH, agrupacionGCH) {
             var sucess = function (response) {
             //fn_Mensaje_Alerta("Confirmacion", "Se esta ejecutando los datos", "", "Alert");
             //var json=response.d.Series;
             GroupedChar_G(response.d.Fecha, response.d.Name, fn_ValidarANulos(response.d.Series));
             $('#Grafica_Mantenimiento_Costo_TotalMes_GCH').show();

            };
                 var error = function (xhr, ajaxOptions, thrownError) {
            };
                 fn_LlamadoMetodo("Costos.aspx/Grafica_Mantenimiento_Costos_TotalMes_GCH_CS", JSON.stringify({ anio2:AnioGCH, mes2:MesGCH , agrupacionF:agrupacionGCH}), sucess, error);
            }

        // TODO : FUNCION HIGHTCHART QUE PERMITE DAR FORMA A LA GRAFICA
       function GroupedChar_G(fecha, Name, data) {
            Highcharts.chart('Grafica_Mantenimiento_Costo_TotalMes_GCH', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: ' COSTO DE DEPARTAMENTO DE MANTENIMIENTO'
                },
                subtitle:{
                    text:'COSTO TOTAL MENSUAL'
                },
                xAxis: {
                    categories: JSON.parse(fecha)
                },
                yAxis: {
                    min: 0,
                    title: {    
                        text: 'MONTO EN SOLES S/.'
                    },
                    stackLabels: {
                        enabled: true,
                        style: {
                            fontWeight: 'bold',
                            color: ( // theme
                                Highcharts.defaultOptions.title.style &&
                                Highcharts.defaultOptions.title.style.color
                            ) || 'gray'
                        }
                    }
                },
                legend: {
                    align: 'right',
                    x: -30,
                    verticalAlign: 'bottom',
                    y: 25,
                    floating: true,
                    backgroundColor:
                        Highcharts.defaultOptions.legend.backgroundColor || 'white',
                    borderColor: '#CCC',
                    borderWidth: 1,
                    shadow: false
                },
                tooltip: {
                    headerFormat: '<b>{point.x}</b><br/>',
                    pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
                },
                plotOptions: {
                    column: {
                        stacking: 'normal',
                        dataLabels: {
                            enabled: true
                        }
                    }
                },
                series: JSON.parse(data)
            });
       }

    </script>


    <!-- **********************************************************************************************************************************************************
                                                          ESTRUCTURA HTML
      *********************************************************************************************************************************************************** -->
    <!--main content start-->

    <!-- <h3><i class="fa fa-angle-right" style="color: #F1C445;"></i> <a>COSTO MANTENIMIENTO</a></h3> -->
    
    <!-- LINECHART-->
    <h3 class="panel-body" style="color: #283593; margin-top: 0px;">COSTOS DE MANTENIMIENTO</h3>
    <div id="AlertaMensaje"></div>
    <div class="text-left Left aligned text" style="margin-left: 18px;">
        <p><strong>Objetivo:</strong> El objetivo del KPI de costos de mantenimiento es poder mostrar los costos que genera el area y sus tipos de costos.</p>
    </div>
   
    <div class=" col-lg-6 container-indicador2">
        <div class="col-lg-12">
            <div class="row">
        
            <div class="panel panel-headline">
                <div class="panel-body">
                    <div class="form-inline container-kpi2">
                        
                        <div class="form-group children2">
                            <label for="formGroupExampleInput">Año:</label>
                            <asp:DropDownList ID="ddlAnio" ClientIDMode="Static" class="form-control" runat="server" Style="width: 80%;"></asp:DropDownList>
                        </div>

                        <div class="form-group children2">
                            <label for="formGroupExampleInput">Mes:</label>
                            <asp:DropDownList ID="ddlMes" ClientIDMode="Static" class="form-control" runat="server" Style="width: 80%;"></asp:DropDownList>
                        </div>
                        
                        <div class="form-group children2">
                            <label for="formGroupExampleInput">Agrupación:</label>
                            <div class="children">
                                <label class="fancy-checkbox children-group ">
                                    <input type="checkbox" id="opc_trimestreLCH">
                                    <span>Trimestre</span>
                                </label>
                                
                                <label class="fancy-checkbox children-group">
                                <input type="checkbox" id="opc_bimestreLCH">
                                    <span>Bimestre</span>
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-inline container-kpi2">
                        <div class="form-group children">
                            <label for="formGroupExampleInput">Limpiar:</label><br />
                            <span class="lnr lnr-magic-wand-limpiar" id="btn_Limpiar_LCH"></span>
                        </div>

                        <div class="form-group children-b ">
                            <button id="btn_Graficar_CostosRepuestos_LCH" type="button" class="btn btn-primary btn-sm">GRAFICAR</button>
                        </div>

                    </div>
                </div>
            </div>

            <div class="row panel-body">
                    <div id="Grafica_Mantenimiento_Costo_TotalMes_LCH" style="min-width: 310px; height: 350px; margin: 0 auto; display: none"></div>
                 </div>
        
                <div class="text-left Left aligned text">
                     <p><strong>Interpretacion:</strong> Se desea que los costos sean los mínimos posisbles..</p>
            </div>

        </div>
    </div>
  
</div>


<!--  grafica groupedchart-->
    
<div class="col-lg-6 container-indicador2">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-headline">
                <div class="panel-body children2">
                    <div class="form-inline container-kpi">

                        <div class="form-group children2">
                            <label for="formGroupExampleInput">Año:</label>
                            <asp:DropDownList ID="ddlAnio2" ClientIDMode="Static" class="form-control" runat="server" Style="width: 80%;"></asp:DropDownList>
                        </div>

                        <div class="form-group children2">
                            <label for="formGroupExampleInput">Mes:</label>
                            <asp:DropDownList ID="ddlMes2" ClientIDMode="Static" class="form-control" runat="server" Style="width: 80%;"></asp:DropDownList>
                        </div>

                       
                        <div class="form-group children2">
                            <label for="formGroupExampleInput">Agrupación:</label>
                            <div class="children">
                                <label class="fancy-checkbox children-group">
                                    <input type="checkbox" id="opc_trimestreGCH">
                                    <span>Trimestre</span>
                                </label>

                                <label class="fancy-checkbox children-group">
                                    <input type="checkbox" id="opc_bimestreGCH">
                                    <span>Bimestre</span>
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-inline container-kpi2">

                            <div class="form-group children">
                                <label for="formGroupExampleInput">Limpiar:</label><br />
                                <span class="lnr lnr-magic-wand-limpiar" id="btn_Limpiar_GCH"></span>
                            </div>

                            <div class="form-group children-b ">
                                <button id="btn_Graficar_CostosRepuestos_GCH" type="button" class="btn btn-primary btn-sm">GRAFICAR</button>
                            </div>
                    </div>
                </div>
            </div>

            <div class="row panel-body">
                    <div id="Grafica_Mantenimiento_Costo_TotalMes_GCH" style="min-width: 310px; height: 350px; margin: 0 auto; display: none"></div>
                 </div>
        
                <div class="text-left Left aligned text">
                     <p><strong>Interpretacion:</strong> Se desea que los costos sean los mínimos posisbles..</p>
            </div>

        </div>
    </div>
  
</div>

    
    


     <!-- **********************************************************************************************************************************************************
      DISEÑO CSS  AND FLEXBOX
      *********************************************************************************************************************************************************** -->
    <!--main content start-->


     <style>
        .centrarhijos{
            display:  flex;
            flex-direction: row;
            justify-content: center;
            align-items: center;
        }

    </style>

</asp:Content>
