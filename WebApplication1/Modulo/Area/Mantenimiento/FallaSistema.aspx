﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Intranet.Master" AutoEventWireup="true" CodeBehind="FallaSistema.aspx.cs" Inherits="WebApplication1.Modulo.Area.Mantenimiento.FallaSistema" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="/Recurso2/Base/Modulo/Mantenimiento.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContenedorDashboard" runat="server">
    <style>
    #Grafica_Porcentaje_Fallas_Sistema {
	height: 400px;
	max-width: 800px;
	min-width: 320px;
	margin: 0 auto;
    }
    .highcharts-pie-series .highcharts-point {
	stroke: #EDE;
	stroke-width: 2px;
    }
    .highcharts-pie-series .highcharts-data-label-connector {
	stroke: silver;
	stroke-dasharray: 2, 2;
	stroke-width: 2px;
    }
    </style>

    <script>
        var marca = ""; var tipomantenimiento = ""; var anio = ""; var mes = "";
        var titulo ='';
        fn_CargarCombos(marca, tipomantenimiento, anio, mes, titulo);
        GraficarDatos("", "", "", "", titulo);
 
        $(document).ready(function () {

            $("#cmbmarca").change(function () {
                ValidarCampo();
               fn_CargarCombos(marca, tipomantenimiento, anio, mes);
            });

            $("#cmbTipo_mantenimiento").change(function () {
                ValidarCampo();
                fn_CargarCombos(marca, tipomantenimiento, anio, mes);
            });
            $("#cmbAnio").change(function () {
                ValidarCampo();
                fn_CargarCombos(marca, tipomantenimiento, anio, mes);
            });
            $("#cmbMes").change(function () {
                ValidarCampo();
                fn_CargarCombos(marca, tipomantenimiento, anio, mes);
            });

            
              $("#G_Falla_KPI").click(function () {

                 ValidarCampo();
                 fn_CargarCombos(marca, tipomantenimiento, anio, mes);
                  GraficarDatos(marca, tipomantenimiento, anio, mes, titulo);
              });
        });

        function fn_CargarCombos(marca, tipomantenimiento, anio, mes) {

            if (tipomantenimiento == 0 || tipomantenimiento == "") fn_General_Cargar_Mantenimiento(marca, anio, mes);
            if (marca == 0 || marca == "") fn_General_Cargar_FiltroMarca(tipomantenimiento, anio, mes);
            if (anio == 0 || anio == "") fn_General_Cargar_FiltroAnio(tipomantenimiento, marca, mes);
            if (mes == 0 || mes == "") fn_General_Cargar_FiltroMes(tipomantenimiento, marca, anio);
        }

        function ValidarCampo() {
            marca = $("#cmbmarca").val(); if (marca != 0) { marca = $("#cmbmarca option:selected").text(); } else { marca = ""; }
            tipomantenimiento = $("#cmbTipo_mantenimiento").val(); if (tipomantenimiento != 0) { tipomantenimiento = $("#cmbTipo_mantenimiento option:selected").text(); } else { tipomantenimiento = ""; }
            anio = $("#cmbAnio").val(); if (anio != 0) { anio = $("#cmbAnio option:selected").text(); } else { anio = ""; }
            mes = $("#cmbMes").val(); if (mes != 0) { mes = $("#cmbMes option:selected").text(); } else { mes = ""; }
        }
        function fn_General_Cargar_FiltroAnio(tipomantenimiento, marca, mes) {
            var html = '';
            var sucess = function (response) {
                var obj = JSON.parse(response.d);
                var data_html = "";
                if (obj == null) {
                    data_html = "<option value='0'>Seleccione Opción</option>";
                    $("select[id$=cmbAnio]").html(data_html);
                }
                else {
                    data_html += "<option value='0'>Seleccione Opción</option>";
                    for (var i = 0; i < obj.length; i++) {
                        data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>";
                    }
                    $("select[id$=cmbAnio]").html(data_html);
                }
            };
            var error = function (xhr, ajaxOptions, thrownError) {
            };
            fn_LlamadoMetodo("FallaSistema.aspx/Mantenimiento_FallasSistema_SistemaImplicado_Anio_CS", JSON.stringify({ tipoMantenimiento: tipomantenimiento, marca: marca, mes: mes }), sucess, error);
        }
        function fn_General_Cargar_FiltroMes(tipomantenimiento, marca, anio) {
            var html = '';
            var sucess = function (response) {
                var obj = JSON.parse(response.d);
                var data_html = "";
                if (obj == null) {
                    data_html = "<option value='0'>Seleccione Opción</option>";
                    $("select[id$=cmbMes]").html(data_html);
                }
                else {
                    data_html += "<option value='0'>Seleccione Opción</option>";
                    for (var i = 0; i < obj.length; i++) {
                        data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>";
                    }
                    $("select[id$=cmbMes]").html(data_html);
                }
            };
            var error = function (xhr, ajaxOptions, thrownError) {
            };
            fn_LlamadoMetodo("FallaSistema.aspx/Mantenimiento_FallasSistema_SistemaImplicado_Mes_CS", JSON.stringify({ tipoMantenimiento: tipomantenimiento, marca: marca, anio: anio }), sucess, error);
        }
        function fn_General_Cargar_FiltroMarca(tipomantenimiento, anio, mes) {
            var html = '';
            var sucess = function (response) {
                var obj = JSON.parse(response.d);
                var data_html = "";
                if (obj == null) {
                    data_html = "<option value='0'>Seleccione Opción</option>";
                    $("select[id$=cmbmarca]").html(data_html);
                }
                else {
                    data_html += "<option value='0'>Seleccione Opción</option>";
                    for (var i = 0; i < obj.length; i++) {
                        data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>";
                    }
                    $("select[id$=cmbmarca]").html(data_html);
                }
            };
            var error = function (xhr, ajaxOptions, thrownError) {
            };
            fn_LlamadoMetodo("FallaSistema.aspx/Mantenimiento_FallasSistema_SistemaImplicado_Marca_CS", JSON.stringify({ tipoMantenimiento: tipomantenimiento, anio: anio, mes: mes }), sucess, error);
        }
        function fn_General_Cargar_Mantenimiento(marca, anio, mes) {
            var html = '';
            var sucess = function (response) {
                var obj = JSON.parse(response.d);
                var data_html = "";
                if (obj == null) {
                    data_html = "<option value='0'>Seleccione Opción</option>";
                    $("select[id$=cmbTipo_mantenimiento]").html(data_html);
                }
                else {
                    data_html += "<option value='0'>Seleccione Opción</option>";
                    for (var i = 0; i < obj.length; i++) {
                        data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>";
                    }
                    $("select[id$=cmbTipo_mantenimiento]").html(data_html);
                }
            };
            var error = function (xhr, ajaxOptions, thrownError) {
            };
            fn_LlamadoMetodo("FallaSistema.aspx/Mantenimiento_FallasSistema_SistemaImplicado_TipoMantenimiento_CS", JSON.stringify({ marca: marca, anio: anio, mes: mes }), sucess, error);
        }


        function GraficarDatos(marca, tipomantenimiento, anio, mes, titulo) {
            var sucess = function (response) {
               
                if (tipomantenimiento === "Correctivo" || tipomantenimiento === "Restaurativo") {
                    titulo = "CANTIDAD DE FALLA POR SISTEMA IMPLICADO";
                }
               else if (tipomantenimiento === "Preventivo" ) {
                    titulo = "CANTIDAD DE MANTENIMIENTO POR SISTEMA IMPLICADO";
                }
                else {
                  
                        titulo = "CANTIDAD DE MANTENIMIENTOS POR SISTEMA IMPLICADO";
                    
                }
               
                BarrasChart(response.d.Marcas, response.d.Data, titulo);
                $('#Grafica_Fallas_Sistema').show();
            };
            var error = function (xhr, ajaxOptions, thrownError) {
                //fn_message('e', 'Ocurrio un error mientras se cargaban los datos.');
            };
            fn_LlamadoMetodo("FallaSistema.aspx/Grafica_Falla_Sistema", JSON.stringify({ marca: marca, tipoMantenimiento: tipomantenimiento, anio: anio, mes: mes }), sucess, error);
        }

        function BarrasChart(Marcas, Data, titulo) {
            var chart = Highcharts.chart('Grafica_Fallas_Sistema', {

                title: {
                    text: titulo
                },
                xAxis:
                    [{
                        categories: Marcas
                    }
                        , {
                        title: {
                            text: 'Sistema Implicado'
                        },
                        opposite: true
                    }],
                plotOptions: {
                    series: {
                        stacking: 'normal',
                        dataLabels: {
                            enabled: true,
                            style: {
                                "font-family": "'Open Sans', sans-serif",
                                "-webkit-text-stroke": "2px black",
                                "color": "#ffff",
                                "fontSize": "10px",
                            },
                            format: '{point.y} '
                        }
                    }
                },
                yAxis:
                    [{
                        title: {
                            text: 'Cantidad'
                        }
                    }],
                chart: {
                    inverted: true,
                    polar: false
                },
                series: [{
                    name: 'Cantidad',
                    type: 'column',
                    colorByPoint: true,
                    data: Data,
                    showInLegend: false
                }]
            });
        }


 //Falla
        var Fmarca = ""; var Ftipomantenimiento = ""; var Fanio = " "; var Fmes = " ";
        Ffn_CargarCombos(Fmarca, Ftipomantenimiento, Fanio, Fmes);
        var idDiv = "Grafica_Porcentaje_Fallas_Sistema"; 
        var titulo2 = "";
        GraficarDatos_PieChart("", "", "", "", "Grafica_Porcentaje_Fallas_Sistema", titulo2);
        $(document).ready(function () {

           

            $("#cmbFmarca").change(function () {
                FValidarCampo();
                Ffn_CargarCombos(Fmarca, Ftipomantenimiento, Fanio, Fmes);
            });

            $("#cmbFTipo_mantenimiento").change(function () {
                FValidarCampo();
                Ffn_CargarCombos(Fmarca, Ftipomantenimiento, Fanio, Fmes);
            });

            $("#cmbFAnio").change(function () {
                FValidarCampo();
                Ffn_CargarCombos(Fmarca, Ftipomantenimiento, Fanio, Fmes);
            });
            $("#cmbFMes").change(function () {
                FValidarCampo();
                Ffn_CargarCombos(Fmarca, Ftipomantenimiento, Fanio, Fmes);
            });

            $("#G_Falla_KPI_Promedio").click(function () {

               FValidarCampo();
                Ffn_CargarCombos(Fmarca, Ftipomantenimiento, Fanio, Fmes);
                GraficarDatos_PieChart(Fmarca, Ftipomantenimiento, Fanio, Fmes, idDiv, titulo2);
            });
        });


        function Ffn_CargarCombos(Fmarca, Ftipomantenimiento, Fanio, Fmes) {

            if (Ftipomantenimiento == 0 || Ftipomantenimiento == "") Ffn_General_Cargar_Mantenimiento(Fmarca, Fanio, Fmes);
            if (Fmarca == 0 || Fmarca == "") Ffn_General_Cargar_FiltroMarca(Ftipomantenimiento, Fanio, Fmes);
            if (Fanio == 0 || Fanio == "") Ffn_General_Cargar_FiltroAnio(Ftipomantenimiento, Fmarca, Fmes);
            if (Fmes == 0 || Fmes == "") Ffn_General_Cargar_FiltroMes(Ftipomantenimiento, Fmarca, Fanio);
        }

        function FValidarCampo() {
            Fmarca = $("#cmbFmarca").val(); if (Fmarca != 0) { Fmarca = $("#cmbFmarca option:selected").text(); } else { Fmarca = ""; }
            Ftipomantenimiento = $("#cmbFTipo_mantenimiento").val(); if (Ftipomantenimiento != 0) { Ftipomantenimiento = $("#cmbFTipo_mantenimiento option:selected").text(); } else { Ftipomantenimiento = ""; }
            Fanio = $("#cmbFAnio").val(); if (Fanio != 0) { Fanio = $("#cmbFAnio option:selected").text(); } else { Fanio = ""; }
            Fmes = $("#cmbFMes").val(); if (Fmes != 0) { Fmes = $("#cmbFMes option:selected").text(); } else { Fmes = ""; }
        }
        function Ffn_General_Cargar_FiltroAnio(Ftipomantenimiento, Fmarca, Fmes) {
            var html = '';
            var sucess = function (response) {
                var obj = JSON.parse(response.d);
                var data_html = "";
                if (obj == null) {
                    data_html = "<option value='0'>Seleccione Opción</option>";
                    $("select[id$=cmbFAnio]").html(data_html);
                }
                else {
                    data_html += "<option value='0'>Seleccione Opción</option>";
                    for (var i = 0; i < obj.length; i++) {
                        data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>";
                    }
                    $("select[id$=cmbFAnio]").html(data_html);
                }
            };
            var error = function (xhr, ajaxOptions, thrownError) {
            };
            fn_LlamadoMetodo("FallaSistema.aspx/Mantenimiento_FallasSistema_PorcentajeSistemaImplicado_Anio_CS", JSON.stringify({ tipoMantenimiento: Ftipomantenimiento, marca: Fmarca, mes: Fmes }), sucess, error);
        }
        function Ffn_General_Cargar_FiltroMes(Ftipomantenimiento, Fmarca, Fanio) {
            var html = '';
            var sucess = function (response) {
                var obj = JSON.parse(response.d);
                var data_html = "";
                if (obj == null) {
                    data_html = "<option value='0'>Seleccione Opción</option>";
                    $("select[id$=cmbFMes]").html(data_html);
                }
                else {
                    data_html += "<option value='0'>Seleccione Opción</option>";
                    for (var i = 0; i < obj.length; i++) {
                        data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>";
                    }
                    $("select[id$=cmbFMes]").html(data_html);
                }
            };
            var error = function (xhr, ajaxOptions, thrownError) {
            };
            fn_LlamadoMetodo("FallaSistema.aspx/Mantenimiento_FallasSistema_PorcentajeSistemaImplicado_Mes_CS", JSON.stringify({ tipoMantenimiento: Ftipomantenimiento, marca: Fmarca, anio: Fanio }), sucess, error);
        }

        function Ffn_General_Cargar_FiltroMarca(Ftipomantenimiento, Fanio, Fmes) {
            var html = '';
            var sucess = function (response) {
                var obj = JSON.parse(response.d);
                var data_html = "";
                if (obj == null) {
                    data_html = "<option value='0'>Seleccione Opción</option>";
                    $("select[id$=cmbFmarca]").html(data_html);

                }
                else {
                    data_html += "<option value='0'>Seleccione Opción</option>";
                    for (var i = 0; i < obj.length; i++) {
                        data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>";
                    }
                    $("select[id$=cmbFmarca]").html(data_html);
                }
            };
            var error = function (xhr, ajaxOptions, thrownError) {
            };
            fn_LlamadoMetodo("FallaSistema.aspx/Mantenimiento_FallasSistema_PorcentajeSistemaImplicado_Marca_CS", JSON.stringify({ tipoMantenimiento: Ftipomantenimiento, anio: Fanio, mes: Fmes }), sucess, error);
        }
        function Ffn_General_Cargar_Mantenimiento(Fmarca, Fanio, Fmes) {
            var html = '';
            var sucess = function (response) {
                var obj = JSON.parse(response.d);
                var data_html = "";
                if (obj == null) {
                    data_html = "<option value='0'>Seleccione Opción</option>";
                    $("select[id$=cmbFTipo_mantenimiento]").html(data_html);
                }
                else {
                    data_html += "<option value='0'>Seleccione Opción</option>";
                    for (var i = 0; i < obj.length; i++) {
                        data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>";
                    }
                    $("select[id$=cmbFTipo_mantenimiento]").html(data_html);
                }
            };
            var error = function (xhr, ajaxOptions, thrownError) {
            };
            fn_LlamadoMetodo("FallaSistema.aspx/Mantenimiento_FallasSistema_PorcentajeSistemaImplicado_TipoMantenimiento_CS", JSON.stringify({ marca: Fmarca, anio: Fanio, mes: Fmes }), sucess, error);
        }

        function GraficarDatos_PieChart(Fmarca, Ftipomantenimiento, Fanio, Fmes, idDiv, titulo2) {
            var sucess = function (response) {

                if (Ftipomantenimiento === "Correctivo" || Ftipomantenimiento === "Restaurativo") {

                    titulo2 = "PORCENTAJE DE FALLA DE SISTEMA POR SISTEMA IMPLICADO";
                }
                else if (Ftipomantenimiento === "Preventivo") {

                    titulo2 = "PORCENTAJE DE MANTENIMIENTO POR SISTEMA IMPLICADO";
                }
                else {

                    titulo2 = "PORCENTAJE DE MANTENIMIENTOS POR SISTEMA IMPLICADO";
                }

                PieChart(response.d.Series, idDiv, titulo2);

            };
            var error = function (xhr, ajaxOptions, thrownError) {
                //fn_message('e', 'Ocurrio un error mientras se cargaban los datos.');
            };
            fn_LlamadoMetodo("FallaSistema.aspx/Grafica_Falla_Promedio_Sistema", JSON.stringify({ marca: Fmarca, tipoMantenimiento: Ftipomantenimiento, anio: Fanio, mes: Fmes }), sucess, error);
        }

        
   
    </script>
    <h3 class="panel-body" style="color: #283593; margin-top: 0px;">CANTIDAD DE MANTENIMIENTO POR SISTEMA</h3>
          <div class="text-left Left aligned text" style="margin-left: 18px;">
        <p><strong>Objetivo:</strong>Mostrar los sistemas de buses que sufren más desperfectos para que se pueda tener un mejor control y seguimiento a los mismos.</p>
    </div>
    <div class="col-lg-6">
         
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-headline">
                    <div class="panel-body">
                        <div class="form-inline">
                            <div class="form-group col-lg-4 col-md-4 col-md-4">
                                <label for="formGroupExampleInput">Marca:</label>
                                <asp:DropDownList ID="cmbmarca" ClientIDMode="Static" class="form-control" runat="server" Style="width: 100%;"></asp:DropDownList>
                            </div>
                            <div class="form-group col-lg-4 col-md-4 col-md-4">
                                <label for="formGroupExampleInput">Mantenimiento:</label>
                                <asp:DropDownList ID="cmbTipo_mantenimiento" ClientIDMode="Static" class="form-control" runat="server" Style="width: 100%;"></asp:DropDownList>
                            </div>
                              <div class="form-group col-lg-4 col-md-4 col-md-4">
                                <label for="formGroupExampleInput">Año:</label>
                                <asp:DropDownList ID="cmbAnio" ClientIDMode="Static" class="form-control" runat="server" Style="width: 100%;"></asp:DropDownList>
                            </div>
                              <div class="form-group col-lg-4 col-md-4 col-md-4">
                                <label for="formGroupExampleInput">Mes:</label>
                                <asp:DropDownList ID="cmbMes" ClientIDMode="Static" class="form-control" runat="server" Style="width: 100%;"></asp:DropDownList>
                            </div>
                            <div class="form-group col-lg-4 col-sm-4 col-md-4">
                                <br />
                                <button id="G_Falla_KPI" type="button" class="btn btn-primary btn-sm">GRAFICAR</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <div id="Grafica_Fallas_Sistema" style="min-width: 310px; height: 330px; margin: 0 auto;"></div>
            </div>
        </div>
        <div class="text-left Left aligned text">
            <p><strong>Interpretación:</strong> Se desea que la cantidad de fallas sea la menor posible</p>
        </div>
    </div>
    <div class="col-lg-6">
        
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-headline">
                    <div class="panel-body">
                        <div class="form-inline">
                            <div class="form-group col-lg-4 col-md-4 col-md-4">
                                <label for="formGroupExampleInput">Marca:</label>
                                <asp:DropDownList ID="cmbFmarca" ClientIDMode="Static" class="form-control" runat="server" Style="width: 100%;"></asp:DropDownList>
                            </div>
                                <div class="form-group col-lg-4 col-md-4 col-md-4">
                                <label for="formGroupExampleInput">Mantenimiento:</label>
                                <asp:DropDownList ID="cmbFTipo_mantenimiento" ClientIDMode="Static" class="form-control" runat="server" Style="width: 100%;"></asp:DropDownList>
                            </div>
                             <div class="form-group col-lg-4 col-md-4 col-md-4">
                                <label for="formGroupExampleInput">Año:</label>
                                <asp:DropDownList ID="cmbFAnio" ClientIDMode="Static" class="form-control" runat="server" Style="width: 100%;"></asp:DropDownList>
                            </div>
                              <div class="form-group col-lg-4 col-md-4 col-md-4">
                                <label for="formGroupExampleInput">Mes:</label>
                                <asp:DropDownList ID="cmbFMes" ClientIDMode="Static" class="form-control" runat="server" Style="width: 100%;"></asp:DropDownList>
                            </div>
                            <div class="form-group col-lg-4 col-sm-4 col-md-4">
                                <br />
                                <button id="G_Falla_KPI_Promedio" type="button" class="btn btn-primary btn-sm">GRAFICAR</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <div id="Grafica_Porcentaje_Fallas_Sistema" style="min-width: 310px; height: 330px; margin: 0 auto;"></div>
                </div>
            </div>
        </div>
        <div class="text-left Left aligned text">
            <p><strong>Interpretacion:</strong> Se desea que la cantidad de fallas sea la menor posible.</p>
        </div>
    </div>
 
    <div class="col-lg-12">
        <!-- PANEL HEADLINE -->
        <div class="panel panel-headline">
            <div class="panel-body">
                <p> <b>NOTA: </b> La información mostrada pertenecen a buses de marca Scania, serie FKN, modelo K410.</p>
            </div>
        </div>
        <!-- END PANEL HEADLINE -->
    </div>

    <!--main content end-->

</asp:Content>
