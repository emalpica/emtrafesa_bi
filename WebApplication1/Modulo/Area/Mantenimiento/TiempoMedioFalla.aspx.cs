﻿using ENTIDAD.Graficas;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI.WebControls;

namespace WebApplication1.Modulo.Area.Mantenimiento
{
    public partial class TiempoMedioFalla : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           
            //Cargar_Filtro_Fecha1();
            //Grafica_Tiempo_Promedio_Falla();
            //Cargar_Filtro_Fecha2();
            //Cargar_Filtro_Anio();
            Cargar_Filtro_MarcaA();
        }

        public static String ConvertMes(String mes)
        {
            if (mes == "Enero") { mes = "1"; }
            if (mes == "Febrero") { mes = "2"; }
            if (mes == "Marzo") { mes = "3"; }
            if (mes == "Abril") { mes = "4"; }
            if (mes == "Mayo") { mes = "5"; }
            if (mes == "Junio") { mes = "6"; }
            if (mes == "Julio") { mes = "7"; }
            if (mes == "Agosto") { mes = "8"; }
            if (mes == "Septiembre") { mes = "9"; }
            if (mes == "Octubre") { mes = "10"; }
            if (mes == "Noviembre") { mes = "11"; }
            if (mes == "Diciembre") { mes = "12"; }
            return mes;
        }

        [WebMethod]
        public static object Grafica_Tiempo_Medio_Falla( String anio, String mes)
        {
            String EGMes;
            EGMes = ConvertMes(mes);
            List<Object> contenedor = new List<Object>();
            List<String> Marcas = new List<String>(); //nombre de marca
            List<Ratio> series = new List<Ratio>();
            DataTable dt = LOGICA.MantenimientoL.Instancia.G1_Tiempo_Fallas_Fallas(anio, EGMes);
            LineChart data = new LineChart();
            Ratio ratio = new Ratio();
            Decimal val = 0;

            if (dt != null)
            {
                foreach (DataColumn dataColumn in dt.Columns)
                {
                    Marcas.Add(dataColumn.ColumnName);
                }
                int j = 1;
                foreach (DataRow dr in dt.Rows)
                {
                    j = 1;
                    data.Fecha.Add(dr[0].ToString());
                    data.Name = Marcas;

                    while (j < Marcas.Count)
                    {
                        if (data.Series.Count < Marcas.Count - 1)
                            data.Series.Add(new Ratio());
                        string value = dr[j].ToString();
                        if (value == "" || value == "NULL") { value = "0"; }
                        val = Convert.ToDecimal(value);
                        data.Series[j - 1].data.Add(Math.Round(val,1));
                        data.Series[j - 1].name = Marcas[j];
                        j++;
                    }
                }
                JavaScriptSerializer sr = new JavaScriptSerializer();
                return new { Fecha = sr.Serialize(data.Fecha), Name = sr.Serialize(data.Name), Series = sr.Serialize(data.Series), };
            }
            else { return null; }
        }
        [WebMethod]
        public static object Mantenimiemto_Tiempo_Fallas_Fallas_anio_CS(String mes)
        {
            String EGMes;
            EGMes = ConvertMes(mes);
            List<String> Anio = new List<String>();
            JavaScriptSerializer sr = new JavaScriptSerializer();
            DataTable filtroanio = LOGICA.MantenimientoL.Instancia.Mantenimiemto_Tiempo_Fallas_Fallas_anio_LO( EGMes);
            if (filtroanio != null)
            {
                for (int i = 0; i < filtroanio.Rows.Count; i++)
                {
                    if ((filtroanio.Rows[i]["Año"].ToString()) == "NULL")
                    {
                        Anio.Add("NO INDICA");
                    }
                    else
                    {
                        Anio.Add(filtroanio.Rows[i]["Año"].ToString());
                    }

                }
            }
            else { return null; }
            return sr.Serialize(Anio);
        }
        [WebMethod]
        public static object Mantenimiemto_Tiempo_Fallas_Fallas_Mes_CS(String anio)
        {
            List<String> Mes = new List<String>();
            JavaScriptSerializer sr = new JavaScriptSerializer();
            DataTable filtromes = LOGICA.MantenimientoL.Instancia.Mantenimiemto_Tiempo_Fallas_Fallas_Mes_Lo(anio);
            if (filtromes != null)
            {
                for (int i = 0; i < filtromes.Rows.Count; i++)
                {
                    if ((filtromes.Rows[i]["Meses"].ToString()) == "NULL")
                    {
                        Mes.Add("NO INDICA");
                    }
                    else
                    {
                        Mes.Add(filtromes.Rows[i]["Meses"].ToString());
                    }

                }
            }
            else { return null; }
            return sr.Serialize(Mes);
        }
        //kpi: Tiempo promedio de fallas - Anual
        //private void Cargar_Filtro_Anio()
        //{
        //    ListItem i;
        //    DataTable filtro_marca = LOGICA.MantenimientoL.Instancia.Filtro_Anio_TMF();
        //    ddlAnio.DataTextField = "Anio";
        //    ddlAnio.DataValueField = "Anio";
        //    ddlAnio.Items.Insert(0, new ListItem("Seleccione Año", "0"));
        //    foreach (DataRow r in filtro_marca.Rows)
        //    {
        //        i = new ListItem(r["Anio"].ToString());
        //        ddlAnio.Items.Add(i);
        //    }
        //    ddlAnio.SelectedIndex = 0;
        //}
        private void Cargar_Filtro_MarcaA()
        {
            ListItem i;
            DataTable filtro_marca = LOGICA.MantenimientoL.Instancia.Filtro_Marca_TMFA();
            ddlMarca1.DataTextField = "Marca";
            ddlMarca1.DataValueField = "Marca";
            ddlMarca1.Items.Insert(0, new ListItem("Seleccione Marca", "0"));
            foreach (DataRow r in filtro_marca.Rows)
            {
                i = new ListItem(r["Marca"].ToString());
                ddlMarca1.Items.Add(i);
            }
            ddlMarca1.SelectedIndex = 0;
        }

        [WebMethod]
        public static object Grafica_Tiempo_Medio_Falla_Anual(String filtroMarca1, String anio1)
        {
            List<Object> contenedor = new List<Object>();
            List<String> Columnas = new List<String>(); //nombre de marca
            List<Ratio> series = new List<Ratio>();
            DataTable dt = LOGICA.MantenimientoL.Instancia.Tiempo_Fallas_Fallas_Anual(filtroMarca1, anio1);
            LineChart data = new LineChart();
            Ratio ratio = new Ratio();

            if (dt != null)
            {
                int m = 0;
                foreach (DataColumn dataColumn in dt.Columns)
                {
                    Columnas.Add(dataColumn.ColumnName);
                }
                int j = 1;
                foreach (DataRow dr in dt.Rows)
                {
                    j = 1;
                    data.Fecha.Add(dr[0].ToString());
                    data.Name = Columnas;
                    while (j < Columnas.Count)
                    {
                        if (data.Series.Count < Columnas.Count - 1)
                            data.Series.Add(new Ratio());
                        string value = dr[j].ToString();

                        data.Series[j - 1].data.Add(Convert.ToDecimal(value));
                        data.Series[j - 1].name = Columnas[j];
                        j++;
                    }
                }
                JavaScriptSerializer sr = new JavaScriptSerializer();
                return new { Fecha = sr.Serialize(data.Fecha), Name = sr.Serialize(data.Name), Series = sr.Serialize(data.Series), };
            }
            else { return null; }
        }

    }
}