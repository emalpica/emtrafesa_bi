﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Intranet.Master" AutoEventWireup="true" CodeBehind="Productividad.aspx.cs" Inherits="WebApplication1.Modulo.Area.Mantenimiento.Productividad" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="/Recurso2/Base/Modulo/Mantenimiento.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContenedorDashboard" runat="server">
    <h3 class="panel-body" style="color: #283593; margin-top: 0px;">PRODUCTIVIDAD</h3>
    <div class="text-left Left aligned text" style="margin-left: 18px;">
        <p><strong>Objetivo:</strong> Medir el rendimiento de los colaboradores del área mediante el tiempo que las órdenes de trabajo de mantenimiento.</p>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-headline">
                <div class="panel-body">
                    <div class="form-inline container-kpi">

                        <div class="form-group col-md-2 col-md-3 col-md-3">
                            <label for="formGroupExampleInput">Año:</label>
                            <asp:DropDownList ID="ddlAnio" ClientIDMode="Static" class="form-control" runat="server" Style="width: 100%;"></asp:DropDownList>
                        </div>
                        <div class="form-group col-md-2 col-md-3 col-md-3">
                            <label for="formGroupExampleInput"></label>
                            <br />
                            <button id="G_Productividad" type="button" class="btn btn-primary btn-sm">GRAFICAR</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



        <div class="container-indicador">
               <div class="row">
                <div class="col-lg-12">
                  <div class="panel panel-headline">
                        <div class="panel-body">
                             <div class="form-inline container-kpi">   
                             <div id="Grafica_Productividad_KPI" style="min-width: 90%; height: 300px; margin: 0 auto;"></div>   

                             </div>
                         </div>
                     </div>
                 </div>
                </div>
               </div>



    <div class="text-left Left aligned text">
        <p><strong>Interpretacion:</strong> Se desea que la cantidad de fallas sea la menor posible.</p>
    </div>

            <script>
                var anio = ""; var start = 0; var idDiv = "Grafica_Productividad_KPI"
                GraficarDatos_Productividad_KPI("", idDiv); 
            $(document).ready(function () {

                $("#G_Productividad").click(function () {
                    ValidarFiltros();
                    GraficarDatos_Productividad_KPI(anio, idDiv);      
                });
            });

            function ValidarFiltros() {
                if (ddlAnio.value != 0) { anio = document.getElementById("ddlAnio").value; }
                else { anio = ''; }
            }

            function LimpiarData() {
                anio = "";
            }

            function GraficarDatos_Productividad_KPI(anio, idDiv ) {
                var sucess = function (response) {
                    var json = JSON.parse(response.d.Series);
                    for (let i = 0; i < json.length; i++) {
                        var x = json[i].data;
                        for (let j = 0; j < x.length; j++) {
                            if (x[j] === 0) {
                                x[j] = null;
                            }
                            else {
                                x[j] = x[j];
                            }
                        }
                    }
                    Graficar_Productividad_KPI(response.d.Fecha, response.d.Name, JSON.stringify(json), idDiv );                
                };
                var error = function (xhr, ajaxOptions, thrownError) {
                    //fn_message('e', 'Ocurrio un error mientras se cargaban los datos.');
                };
                fn_LlamadoMetodo("Productividad.aspx/Grafica_Productividad_KPI", JSON.stringify({ anio: anio }), sucess, error);
            }



        </Script>
          
</asp:Content>
