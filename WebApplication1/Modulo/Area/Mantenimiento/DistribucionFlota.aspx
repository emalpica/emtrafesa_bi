﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Intranet.Master" AutoEventWireup="true" CodeBehind="DistribucionFlota.aspx.cs" Inherits="WebApplication1.Modulo.Mantenimiento.DistribucionFlota" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContenedorDashboard" runat="server">



    <h3 class="panel-body" style="color: #283593; margin-top: 0px;">DISPONIBILIDAD DE FLOTA</h3>
    <div id="AlertaMensaje"></div>
    <div class="text-left Left aligned text" style="margin-left: 18px;">
        <p><strong>Objetivo:</strong> El objetivo del KPI de disponibilidad de flota es mostrar la comparación entre el tiempo en trayecto y el tiempo en mantenimiento de la flota.</p>
    </div>
    <div class="container-indicador">
 
            <div >
       <%-- <div class="row">--%>
        <%--<div class="col-lg-12">--%>
            <div class="panel panel-headline">
                <div class="panel-body">
                    <div class="form-inline container-kpi">

                        <div class="form-group children">
                            <label for="formGroupExampleInput">Marca:</label>
                            <asp:DropDownList ID="cmbMarca_general" ClientIDMode="Static" class="form-control" runat="server" Style="width: 100%;"></asp:DropDownList>
                        </div>

                      <%--  <div class="form-group children">
                            <label for="formGroupExampleInput">Modelo:</label>
                            <asp:DropDownList ID="cmbModelo_general" ClientIDMode="Static" class="form-control" runat="server" Style="width: 100%;"></asp:DropDownList>
                        </div>--%>

                        <div class="form-group children">
                            <label for="formGroupExampleInput">Año:</label>
                            <asp:DropDownList ID="cmbAnio_General" ClientIDMode="Static" class="form-control" runat="server" Style="width: 100%;"></asp:DropDownList>
                        </div>

                        <div class="form-group children">
                            <label for="formGroupExampleInput">Mes:</label>
                            <asp:DropDownList ID="cmbMes" ClientIDMode="Static" class="form-control" runat="server" Style="width: 100%;"></asp:DropDownList>
                        </div>
                        <div class="form-group children">
                            <label for="formGroupExampleInput">Agrupación:</label>
                            <div class="children-child">
                            <label class="fancy-checkbox children-group">
                                <input type="checkbox" id="opc_trimestre">
                                <span>Trimestre</span>
                            </label>
                            <label class="fancy-checkbox children-group">
                                <input type="checkbox" id="opc_bimestre">
                                <span>Bimestre</span>
                            </label>
                            </div>
                        </div>


                        <div class="form-group children">
                            <label for="formGroupExampleInput">Limpiar:</label><br />
                            <span class="lnr lnr-magic-wand-limpiar" onclick="fn_Limpiar()"></span>
                        </div>

                        <div class="form-group children children-b">
                            <button id="G_Disponibilidad_Flotas" type="button" class="btn btn-primary btn-sm">GRAFICAR</button>
                        </div>

                    </div>
                </div>
            </div>
     <%--   </div>--%>
    <%--</div>--%>

    <div id="Grafica_Mantenimiento_Disponibilidad_Flotas" class="children-g" style="height: 320px"></div>


   <%--     <div class="text-left Left aligned text">
        <p><strong>Interpretacion:</strong> Se desea que el KPI sea lo más alto posible.</p>
    </div>
    </div>


            <div class="panel panel-headline">
                <div class="panel-body">
                    <div class="form-inline container-kpi">

                        <div class="form-group children">
                            <label for="formGroupExampleInput">Marca:</label>
                            <asp:DropDownList ID="cmbMarca_generalA" ClientIDMode="Static" class="form-control" runat="server" Style="width: 100%;"></asp:DropDownList>
                        </div>

                        <div class="form-group children">
                            <label for="formGroupExampleInput">Modelo:</label>
                            <asp:DropDownList ID="cmbModelo_generalA" ClientIDMode="Static" class="form-control" runat="server" Style="width: 100%;"></asp:DropDownList>
                        </div>

                        <div class="form-group children">
                            <label for="formGroupExampleInput">Anio:</label>
                            <asp:DropDownList ID="cmbAnio_GeneralA" ClientIDMode="Static" class="form-control" runat="server" Style="width: 100%;"></asp:DropDownList>
                        </div>
        

                        <div class="form-group children">
                            <label for="formGroupExampleInput" class="children-child">Mes:</label>
                            <input id="cmbMes_GeneralA" name="DatePickerMonth" class="datepicker" data-date-format="mm/yyyy" style="width: 60px;">
                        </div>

                        <div class="form-group children">
                            <label for="formGroupExampleInput">Agrupación:</label>
                            <div class="children-child">
                            <label class="fancy-checkbox children-group">
                                <input type="checkbox" id="opc_trimestreA">
                                <span>Trimestre</span>
                            </label>
                            <label class="fancy-checkbox children-group">
                                <input type="checkbox" id="opc_bimestreA">
                                <span>Bimestre</span>
                            </label>
                            </div>
                        </div>


                        <div class="form-group children">
                            <label for="formGroupExampleInput">Limpiar:</label><br />
                            <span class="lnr lnr-magic-wand-limpiar" onclick="fn_LimpiarA()"></span>
                        </div>


                        <div class="form-group children-b">
                            <button id="G_Disponibilidd_FlotasA" type="button" class="btn btn-primary btn-sm">GRAFICAR</button>
                        </div>

                    </div>
                </div>
            </div>

        <div id="Grafica_Mantenimiento_Disponibilad_FlotasA" class="children-g" style="height: 300px"></div>

        
        <div class="text-left Left aligned text">
        <p><strong>Interpretacion:</strong> Se desea que el KPI sea lo más alto posible.</p>
    </div>
    </div>--%>

</div>

    <script>
        var marca = ""; var modelo = ""; var anio = ""; var mes = ""; var mes = ""; var filtroanio = ""; var agrupacion = "0"; var bimestre = "0"; var anio = "";
        var marcaA = ""; var modeloA = ""; var anioA = ""; var mesA = ""; var mesA = ""; var filtroanioA = ""; var agrupacionA = "0"; var bimestreA = "0"; var anioA = "";
        var json; var json_convertido; var temp = ""; var tempCmb = "";
        fn_CargarCombos();
        fn_CargarCombosA();
        Cargar_Grafica_General("", "", "2019", "", "0");
        Cargar_Grafica_GeneralA("", "", "2019", "", "0");

        $(document).ready(function () {

            //GRAFICA 1:
            $("#opc_trimestre").change(function () {
                $("#opc_bimestre").prop("checked", false);
                ValidarCampo();
                Cargar_Grafica_General(modelo, marca, anio, mes, agrupacion);
            });
            $("#opc_bimestre").change(function () {
                $("#opc_trimestre").prop("checked", false);
                ValidarCampo();
                Cargar_Grafica_General(modelo, marca, anio, mes, agrupacion);
            });
            $("#cmbAnio_General").change(function () {
                ValidarCampo();
                fn_CargarCombos();
            });
            $("#cmbMarca_general").change(function () {
                ValidarCampo();
                fn_CargarCombos();
            });
            $("#cmbModelo_general").change(function () {
                ValidarCampo();
                fn_CargarCombos();
            });
            $("#cmbMes").change(function () {
                ValidarCampo();
                fn_CargarCombos();
            });
     
            $("#G_Disponibilidad_Flotas").click(function () {
                ValidarCampo();
                Cargar_Grafica_General(modelo, marca, anio, mes, agrupacion);
            });
           
        });

        //GRAFICA 1:
        function ValidarCampo() {
            marca = $("#cmbMarca_general").val(); if (marca != 0) { marca = $("#cmbMarca_general option:selected").text(); } else { marca = ""; }
            //modelo = $("#cmbModelo_general").val(); if (modelo != 0) { modelo = $("#cmbModelo_general option:selected").text(); } else { modelo = ""; }
            anio = $("#cmbAnio_General").val(); if (anio != 0) { anio = $("#cmbAnio_General option:selected").text(); } else { anio = ""; }
            mes = $("#cmbMes").val(); if (mes != 0) { mes = $("#cmbMes option:selected").text(); } else { mes = ""; }
            if ($("#opc_trimestre").prop('checked')) { agrupacion = "3"; }
            if ($("#opc_bimestre").prop('checked')) { agrupacion = "2"; }
            if ($("#opc_bimestre").prop('checked') == false && $("#opc_trimestre").prop('checked') == false) { agrupacion = "0"; }
        }

   


        function fn_Limpiar() {
            if (marca != 0) $("#cmbMarca_general").val(0);
            //if (modelo != 0) $("#cmbModelo_general").val(0);
            if (anio != 0) $("#cmbAnio_General").val(0);
            if (mes != 0) $("#cmbMes").val(0);
            $("#opc_trimestre").prop("checked", false);
            $("#opc_bimestre").prop("checked", false);
            ValidarCampo();
            fn_CargarCombos();
            Cargar_Grafica_General("", "", "2019", "", "0");
        }

        function fn_CargarCombos() {
            fn_General_Cargar_FiltroMarca(modelo, anio, mes, "cmbMarca_general");
            fn_General_Cargar_FiltroModelo(marca, anio, mes, "cmbModelo_general");
            fn_General_Cargar_FiltroAnio(marca, modelo, anio, mes, "cmbAnio_General");
            fn_General_Cargar_FiltroMes(marca, modelo, anio,  "cmbMes");
        }

        function fn_CargarCombosA() {
            fn_General_Cargar_FiltroMarcaA(modeloA, anioA, mesA, "cmbMarca_generalA");
            fn_General_Cargar_FiltroModeloA(marcaA, anioA, mesA, "cmbModelo_generalA");
            fn_General_Cargar_FiltroAnioA(marcaA, modeloA, anioA, mesA, "cmbAnio_GeneralA");
        }

        function fn_LimpiarA() {
            if (marcaA != 0) $("#cmbMarca_generalA").val(0);
            if (modeloA != 0) $("#cmbModelo_generalA").val(0);
            if (anioA != 0) $("#cmbAnio_GeneralA").val(0);
            if (mesA != 0) $("#cmbMes_GeneralA").val(0);
            $("#opc_trimestreA").prop("checked", false);
            $("#opc_bimestreA").prop("checked", false);
            fn_CargarCombos()
            ValidarCampoA();
            Cargar_Grafica_GeneralA("", "", "2019", "", "0");
        }


        function fn_General_Cargar_FiltroMarcaA(modeloA, anioA, mesA, idCm) {
            var html = '';

            var sucess = function (response) {
                var obj = JSON.parse(response.d);
                var data_html = ""; var seleccion = 0;

                if (obj == null) {
                    data_html = "<option value='0'>Seleccione Opcion</option>";
                    $("select[id$=cmbMarca_generalA]").html(data_html);
                }
                else {
                    if (marcaA != 0) {
                        data_html += "<option value='0'>Seleccione Opcion</option>";
                        for (var i = 0; i < obj.length; i++) {
                            if (obj[i] == marcaA) { seleccion = i + 1; data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>"; }
                            else { data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>"; }
                        }
                        $("select[id$=cmbMarca_generalA]").html(data_html);
                        document.getElementById("cmbMarca_generalA").selectedIndex = seleccion;
                    }
                    else {
                        data_html += "<option value='0'>Seleccione Opcion</option>";
                        for (var i = 0; i < obj.length; i++) {
                            data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>";
                        }
                        $("select[id$=cmbMarca_generalA]").html(data_html);
                    }
                }
                temp = "";
            };
            var error = function (xhr, ajaxOptions, thrownError) {
                fn_Mensaje_Alerta("error", " Hubo un error al cargar la marca", "", "AlertaMensaje");
            };
            fn_LlamadoMetodo("DistribucionFlota.aspx/F_Mantenimiento_General_CS_ddMarca", JSON.stringify({ filtroModelo: modeloA, anio: anioA, mes: mesA }), sucess, error);
        }
        function fn_General_Cargar_FiltroModeloA(marcaA, anioA, mesA, idCm) {
            var html = '';
            var sucess = function (response) {
                var obj = JSON.parse(response.d);
                var data_html = ""; var seleccion = 0;
                if (obj == null) {
                    data_html = "<option value='0'>Seleccione Opcion</option>";
                    $("select[id$=cmbModelo_generalA]").html(data_html);
                }
                else {
                    if (modeloA != 0) {
                        data_html += "<option value='0'>Seleccione Opcion</option>";
                        for (var i = 0; i < obj.length; i++) {
                            if (obj[i] == modeloA) { seleccion = i + 1; data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>"; }
                            else { data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>"; }
                        }
                        $("select[id$=cmbModelo_generalA]").html(data_html);
                        document.getElementById("cmbModelo_generalA").selectedIndex = seleccion;
                    }
                    else {
                        data_html += "<option value='0'>Seleccione Opcion</option>";
                        for (var i = 0; i < obj.length; i++) {
                            data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>";
                        }
                        $("select[id$=cmbModelo_generalA]").html(data_html);
                    }
                }
            };
            var error = function (xhr, ajaxOptions, thrownError) {
                fn_Mensaje_Alerta("error", " Hubo un error al cargar el modelo", "", "AlertaMensaje");
            };
            fn_LlamadoMetodo("DistribucionFlota.aspx/F_Mantenimiento_General_CS_ddModelo", JSON.stringify({ filtroMarca: marcaA, anio: anioA, mes: mesA }), sucess, error);
        }
        function fn_General_Cargar_FiltroAnioA(marcaA, modeloA, anioA, mesA, idCm) {
            var html = '';
            var sucess = function (response) {
                var obj = JSON.parse(response.d);
                var data_html = ""; var seleccion = 0;
                if (obj == null) {
                    data_html = "<option value='0'>Seleccione Opcion</option>";
                    $("select[id$=cmbAnio_GeneralA]").html(data_html);
                }
                else {
                    if (anioA != 0) {
                        data_html += "<option value='0'>Seleccione Opcion</option>";
                        for (var i = 0; i < obj.length; i++) {
                            if (obj[i] == anioA) { seleccion = i + 1; data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>"; }
                            else { data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>"; }
                        }
                        $("select[id$=cmbAnio_General]").html(data_html);
                        document.getElementById("cmbAnio_GeneralA").selectedIndex = seleccion;
                    }
                    else {
                        data_html += "<option value='0'>Seleccione Opcion</option>";
                        for (var i = 0; i < obj.length; i++) {
                            data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>";
                        }
                        $("select[id$=cmbAnio_GeneralA]").html(data_html);
                    }
                }
            };
            var error = function (xhr, ajaxOptions, thrownError) {
                fn_Mensaje_Alerta("error", " Hubo un error al cargar el año", "", "AlertaMensaje");
            };
            fn_LlamadoMetodo("DistribucionFlota.aspx/F_Mantenimiento_General_CS_ddAnio", JSON.stringify({ marca: marcaA, filtroModelo: modeloA, anio: anioA, mes: mesA }), sucess, error);
        }
        function Cargar_Grafica_GeneralA(modeloA, marcaA, anioA, mesA, agrupacionA) {
            var sucess = function (response) {
                json = response.d.Series
                LineChartA(response.d.Fecha, response.d.Name, fn_ValidarANulos(json));
                ValidarCampoA();
                //f_Mnensaje_Alerta("Confirmacion", "", "holaa", "AlertaMensaje", html);                
            };
            var error = function (xhr, ajaxOptions, thrownError) {
                //fn_Mensaje_Alerta("error", " Hubo un error al cargar el modelo", "", "AlertaMensaje");
            };
            fn_LlamadoMetodo("DistribucionFlota.aspx/G_Mantenimiento_CantidadGeneral_Modelo_CS", JSON.stringify({ filtroModelo: modeloA, filtroMarca: marcaA, anio: anioA, mes: mesA, agrupacionF: agrupacionA }), sucess, error);
        }
  
        function fn_General_Cargar_FiltroMarca(modelo, anio, mes, idCm) {
            var html = '';
            var sucess = function (response) {
                var obj = JSON.parse(response.d);
                var data_html = ""; var seleccion = 0;
                if (obj == null) {
                    data_html = "<option value='0'>Seleccione Opcion</option>";
                    $("select[id$=" + idCm + "]").html(data_html);
                }
                else {
                    if (marca != 0) {
                        data_html += "<option value='0'>Seleccione Opcion</option>";
                        for (var i = 0; i < obj.length; i++) {
                            if (obj[i] == marca) { seleccion = i + 1; data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>"; }
                            else { data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>"; }
                        }
                        $("select[id$=" + idCm + "]").html(data_html);
                        document.getElementById(idCm).selectedIndex = seleccion;
                    }
                    else {
                        data_html += "<option value='0'>Seleccione Opcion</option>";
                        for (var i = 0; i < obj.length; i++) {
                            data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>";
                        }
                        $("select[id$=" + idCm + "]").html(data_html);
                    }
                }
                temp = "";
            };
            var error = function (xhr, ajaxOptions, thrownError) {
                fn_Mensaje_Alerta("error", " Hubo un error al cargar la marca", "", "AlertaMensaje");
            };
            fn_LlamadoMetodo("DistribucionFlota.aspx/F_Mantenimiento_General_CS_ddMarca", JSON.stringify({ filtroModelo: modelo, anio: anio, mes: mes }), sucess, error);
        }
        function fn_General_Cargar_FiltroMes(marca, modelo, anio, idCm) {
            var html = '';
            var sucess = function (response) {
                var obj = JSON.parse(response.d);
                var data_html = ""; var seleccion = 0;
                if (obj == null) {
                    data_html = "<option value='0'>Seleccione Opcion</option>";
                    $("select[id$=" + idCm + "]").html(data_html);
                }
                else {
                    if (mes != 0) {
                        data_html += "<option value='0'>Seleccione Opcion</option>";
                        for (var i = 0; i < obj.length; i++) {
                            if (obj[i] == mes) { seleccion = i + 1; data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>"; }
                            else { data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>"; }
                        }
                        $("select[id$=" + idCm + "]").html(data_html);
                        document.getElementById(idCm).selectedIndex = seleccion;
                    }
                    else {
                        data_html += "<option value='0'>Seleccione Opcion</option>";
                        for (var i = 0; i < obj.length; i++) {
                            data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>";
                        }
                        $("select[id$=" + idCm + "]").html(data_html);
                    }
                }
            };
            var error = function (xhr, ajaxOptions, thrownError) {
                fn_Mensaje_Alerta("error", " Hubo un error al cargar el modelo", "", "AlertaMensaje");
            };
            fn_LlamadoMetodo("DistribucionFlota.aspx/F_Mantenimiento_DisponibilidadFoltas_CS_ListarMes", JSON.stringify({ marca: marca, filtroModelo: modelo, anio: anio }), sucess, error);
        }
        function fn_General_Cargar_FiltroModelo(marca, anio, mes, idCm) {
            var html = '';
            var sucess = function (response) {
                var obj = JSON.parse(response.d);
                var data_html = ""; var seleccion = 0;
                if (obj == null) {
                    data_html = "<option value='0'>Seleccione Opcion</option>";
                    $("select[id$=" + idCm + "]").html(data_html);
                }
                else {
                    if (modelo != 0) {
                        data_html += "<option value='0'>Seleccione Opcion</option>";
                        for (var i = 0; i < obj.length; i++) {
                            if (obj[i] == modelo) { seleccion = i + 1; data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>"; }
                            else { data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>"; }
                        }
                        $("select[id$=" + idCm + "]").html(data_html);
                        document.getElementById(idCm).selectedIndex = seleccion;
                    }
                    else {
                        data_html += "<option value='0'>Seleccione Opcion</option>";
                        for (var i = 0; i < obj.length; i++) {
                            data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>";
                        }
                        $("select[id$=" + idCm + "]").html(data_html);
                    }
                }
            };
            var error = function (xhr, ajaxOptions, thrownError) {
                fn_Mensaje_Alerta("error", " Hubo un error al cargar el modelo", "", "AlertaMensaje");
            };
            fn_LlamadoMetodo("DistribucionFlota.aspx/F_Mantenimiento_General_CS_ddModelo", JSON.stringify({ filtroMarca: marca, anio: anio, mes: mes }), sucess, error);
        }
        function fn_General_Cargar_FiltroAnio(marca, modelo, anio, mes) {
            var html = '';
            var sucess = function (response) {
                var obj = JSON.parse(response.d);
                var data_html = ""; var seleccion = 0;
                if (obj == null) {
                    data_html = "<option value='0'>Seleccione Opcion</option>";
                    $("select[id$=cmbAnio_General]").html(data_html);
                }
                else {
                    if (anio != 0) {
                        data_html += "<option value='0'>Seleccione Opcion</option>";
                        for (var i = 0; i < obj.length; i++) {
                            if (obj[i] == anio) { seleccion = i + 1; data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>"; }
                            else { data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>"; }
                        }
                        $("select[id$=cmbAnio_General]").html(data_html);
                        document.getElementById("cmbAnio_General").selectedIndex = seleccion;
                    }
                    else {
                        data_html += "<option value='0'>Seleccione Opcion</option>";
                        for (var i = 0; i < obj.length; i++) {
                            data_html += "<option value='" + (i + 1) + "'>" + obj[i] + "</option>";
                        }
                        $("select[id$=cmbAnio_General]").html(data_html);
                    }
                }
            };
            var error = function (xhr, ajaxOptions, thrownError) {
                fn_Mensaje_Alerta("error", " Hubo un error al cargar el año", "", "AlertaMensaje");
            };
            fn_LlamadoMetodo("DistribucionFlota.aspx/F_Mantenimiento_General_CS_ddAnio", JSON.stringify({ marca: marca, filtroModelo: modelo, anio: anio, mes: mes }), sucess, error);
        }
        function Cargar_Grafica_General(modelo, marca, anio, mes, agrupacion) {
            var sucess = function (response) {
                json = response.d.Series
                LineChart(response.d.Fecha, response.d.Name, fn_ValidarANulos(json));
                $('#Grafica_Mantenimiento_Disponibilidad_Flotas').show();
                ValidarCampo();
                //fn_Mensaje_Alerta("Confirmacion", "", "holaa", "AlertaMensaje", html);
            };
            var error = function (xhr, ajaxOptions, thrownError) {
                fn_Mensaje_Alerta("error", " Hubo un error al cargar la gráfica", "", "AlertaMensaje");
            };
            fn_LlamadoMetodo("DistribucionFlota.aspx/Grafica_Distribucion_Flota", JSON.stringify({ filtroModelo: modelo, filtroMarca: marca, anio: anio, mes: mes, agrupacionF: agrupacion }), sucess, error);
        }
        function LineChart(fecha, Name, data) {
            Highcharts.chart('Grafica_Mantenimiento_Disponibilidad_Flotas', {
                chart: {
                    type: 'line'
                },
                title: {
                    text: 'KPI DE DISPONIBILIDAD DE FLOTA DE BUSES'
                },
                xAxis: {
                    categories: JSON.parse(fecha)
                },
                yAxis: {
                    labels: {
                        formatter: function () {
                            return (this.value) + ' %';
                        }
                    },
                    min: 50,
                    max: 100,
                    title: {
                        text: ''
                    }

                },
                tooltip: {
                    headerFormat: '<b>{point.x}</b><br/>',
                    pointFormat: '{series.name}: {point.y:.1f} %  '
                },
                plotOptions: {
                    line: {
                        dataLabels: {
                            enabled: true,
                            format: '{point.y:.1f} %'
                        },
                        enableMouseTracking: true
                    }
                },
                series: JSON.parse(data)
            });
        }

    </script>



</asp:Content>


