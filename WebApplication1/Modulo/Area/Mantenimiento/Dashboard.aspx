﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Intranet.Master" AutoEventWireup="true" CodeBehind="Dashboard.aspx.cs" Inherits="WebApplication1.Modulo.Area.Mantenimiento.Dashboard" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContenedorDashboard" runat="server">

    <script>
      //var filtroMarca = ""; var filtroModelo = ""; var fechaInicio = ""; var fechaFin = ""; var filtroanio="";
        var filtroVacio = "";
      $(document).ready(function () {
          GraficarDatos1(filtroVacio, filtroVacio, filtroVacio, filtroVacio, filtroVacio);
          GraficarDatos(filtroVacio, filtroVacio, filtroVacio, filtroVacio, filtroVacio);
          GraficarDatos_Anual(filtroVacio, filtroVacio);
          GraficarDatos_Productividad_KPI(filtroVacio);
       });

      //KPI: Distribucion de flota
      function GraficarDatos1(filtroVacio, filtroVacio, filtroVacio, filtroVacio, filtroVacio) {
          var sucess = function (response) {
              var json = JSON.parse(response.d.Series);

              for (let i = 0; i < json.length; i++) {
                  var x = json[i].data;
                  for (let j = 0; j < x.length; j++) {
                      if (x[j] === 0) {
                          x[j] = null;
                      }
                  }
              }
              //var json2 = JSON.stringify(json.data);
              LineChart(response.d.Fecha, response.d.Name, JSON.stringify(json));
              $('#Grafica_Mantenimiento_Disponibilidad_Flotas').show();
          };
          var error = function (xhr, ajaxOptions, thrownError) {
              $('#Grafica_Mantenimiento_Disponibilidad_Flotas').toggle();
              //fn_message('e', 'Ocurrio un error mientras se cargaban los datos.');
          };
          fn_LlamadoMetodo("DistribucionFlota.aspx/Grafica_Distribucion_Flota", JSON.stringify({ filtroModelo: filtroVacio, filtroMarca: filtroVacio, fechaInicio: filtroVacio, fechaFin: filtroVacio, filtroanio:filtroVacio}), sucess, error);
      }

      function LineChart(fecha, Name, data) {
          Highcharts.chart('Grafica_Mantenimiento_Disponibilidad_Flotas', {
              chart: {
                  type: 'line'
              },
              title: {
                  text: 'KPI DE DISPONIBILIDAD DE FLOTA MENSUAL POR MARCA'
              },
              //subtitle: {
              //    text: 'Source: WorldClimate.com' },
              xAxis: {
                  categories: JSON.parse(fecha)
              },
              yAxis: {
                  min: 0,
                  title: {
                      text: 'Porcentaje de KPI'
                  }
              },
              plotOptions: {
                  line: {
                      dataLabels: {
                          enabled: true,
                          //format: '{point.y:.1f} %'
                      },
                      enableMouseTracking: false
                  }
              },
              series: JSON.parse(data)
          });
        }

      //KPI: Falla del sistema

      function GraficarDatos(filtroVacio,filtroVacio,filtroVacio,filtroVacio,filtroVacio) {
            var sucess = function (response) {
                BarrasChart(response.d.Marcas, response.d.Data);
                $('#Grafica_Fallas_Sistema').show();
            };
            var error = function (xhr, ajaxOptions, thrownError) {
                //fn_message('e', 'Ocurrio un error mientras se cargaban los datos.');
            };
            fn_LlamadoMetodo("FallaSistema.aspx/Grafica_Falla_Sistema", JSON.stringify({ filtroMarca: filtroVacio, fechaInicioMes:filtroVacio,fechaFinMes:filtroVacio,fechaInicioAnio:filtroVacio,fechaFinAnio:filtroVacio }), sucess, error);
       }

      function BarrasChart(Marcas, Data) { //Name, Series)
         var chart=   Highcharts.chart('Grafica_Fallas_Sistema', {     
            title: {
                text: 'CANTIDAD DE FALLAS'
            },        
            subtitle: {
                text: 'Sistema Implicado'
            },        
                xAxis: {
                    categories: Marcas
             },  
                chart: {
                    inverted: true,
                    polar: false
                },
             series: [{
                name: 'Cantidad:',
                type: 'column',
                colorByPoint: true,
                data: Data, 
                showInLegend: false
            }]              
        });  
        }

      //KPI: Tiempo medio de falla

       function GraficarDatos_Anual(filtroVacio, filtroVacio) {
            var sucess = function (response) {
                Grafica_TMF_Linea_Anual(response.d.Fecha, response.d.Name, response.d.Series);
                $('#Grafica_Tiempo_Medio_Fallas1').show();
            };
            var error = function (xhr, ajaxOptions, thrownError) {
                //fn_message('e', 'Ocurrio un error mientras se cargaban los datos.');
            };
            fn_LlamadoMetodo("TiempoMedioFalla.aspx/Grafica_Tiempo_Medio_Falla_Anual", JSON.stringify({ filtroMarca1: filtroVacio, anio1:filtroVacio }), sucess, error);
        }

       function Grafica_TMF_Linea_Anual(fecha, Name, data) {
            Highcharts.chart('Grafica_Tiempo_Medio_Fallas1', {
                chart: {
                    type: 'line'
                },
                title: {
                    text: 'KPI - TIEMPO MEDIO DE FALLAS POR AÑO'
                },
                //subtitle: {
                //    text: 'Source: WorldClimate.com' },
                xAxis: {
                    categories: JSON.parse(fecha)
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Porcentaje de KPI'
                    }
                },
                plotOptions: {
                    line: {
                        dataLabels: {
                            enabled: true
                        },
                        enableMouseTracking: false
                    }
                },
                series: JSON.parse(data)
            });
        }

        //KPI: Productividad

       function GraficarDatos_Productividad_KPI(filtroVacio) {
                var sucess = function (response) {
                    Graficar_Productividad_KPI(response.d.Fecha, response.d.Name, response.d.Series);
                    $('#Grafica_Productividad_KPI').show();
                   
                };
                var error = function (xhr, ajaxOptions, thrownError) {
                    //fn_message('e', 'Ocurrio un error mientras se cargaban los datos.');
                };
                fn_LlamadoMetodo("Productividad.aspx/Grafica_Productividad_KPI", JSON.stringify({ anio: filtroVacio }), sucess, error);
        }

       function Graficar_Productividad_KPI(fecha, Name, data) {
                Highcharts.chart('Grafica_Productividad_KPI', {
                    chart: {
                        type: 'line'
                    },
                    title: {
                        text: 'TOTAL DE PRODUCTIVIDAD'
                    },
                    subtitle: {
                        text: 'Meses' },
                    xAxis: {
                        categories: JSON.parse(fecha)
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: 'Porcentaje de KPI'
                        }
                    },
                    plotOptions: {
                        line: {
                            dataLabels: {
                                enabled: true
                            },
                            enableMouseTracking: false
                        }
                    },
                    series: JSON.parse(data)
                });
            }

    </script>
    <h3><i class="fa fa-angle-right" style="color: #F1C445;"></i><a>DASHBOARD MANTENIMIENTO</a></h3>
    <hr>
    <div class="col-lg-12 main-chart">
<%--        <div class="btn-group btn-group-justified">
						  <div class="btn-group">
						    <button type="button" id="anio2017" class="btn btn-theme">2017</button>
						  </div>
						  <div class="btn-group">
						    <button type="button" id="anio2018" class="btn btn-theme">2018</button>
						  </div>
						  <div class="btn-group">
						    <button type="button" id="anio2019" class="btn btn-theme">2019</button>
						  </div>
		</div>
        <br />--%>
        <div class="col-lg-12">
            <%--                <div class="form-row">
                    <div class="form-inline">
                    </div>
                </div>--%>
            <div class="row">
                <div class="col-lg-8 col-md-6 col-sm-6">
                <div id="Grafica_Mantenimiento_Disponibilidad_Flotas" style="min-width: 310px; height: 250px; margin: 0 auto; display: none"></div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6">
                 <div id="Grafica_Fallas_Sistema" style="min-width: 310px; height: 250px; margin: 0 auto; display:none"></div>  
                </div>
            </div>

            <br />
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6">
                <div id="Grafica_Tiempo_Medio_Fallas1" style="min-width: 310px; height: 250px; margin: 0 auto; display:none"></div>  
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6">
                 <div id="Grafica_Productividad_KPI" style="min-width: 310px; height: 250px; margin: 0 auto; display:none"></div>  
                </div>
            </div>
        </div>
    </div>


</asp:Content>
