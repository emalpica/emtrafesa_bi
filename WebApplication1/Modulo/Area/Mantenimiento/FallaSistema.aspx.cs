﻿using ENTIDAD.Graficas;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI.WebControls;

namespace WebApplication1.Modulo.Area.Mantenimiento
{
    ////KHJH
    ///ERIKA PRUEBA 2
    /////CAMBIO 3
    public partial class FallaSistema : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
         
        }
        public static String ConvertMes(String mes)
        {
            if (mes == "Enero") { mes = "1"; }
            if (mes == "Febrero") { mes = "2"; }
            if (mes == "Marzo") { mes = "3"; }
            if (mes == "Abril") { mes = "4"; }
            if (mes == "Mayo") { mes = "5"; }
            if (mes == "Junio") { mes = "6"; }
            if (mes == "Julio") { mes = "7"; }
            if (mes == "Agosto") { mes = "8"; }
            if (mes == "Septiembre") { mes = "9"; }
            if (mes == "Octubre") { mes = "10"; }
            if (mes == "Noviembre") { mes = "11"; }
            if (mes == "Diciembre") { mes = "12"; }
            return mes;
        }

        [WebMethod]
        public static object Mantenimiento_FallasSistema_SistemaImplicado_TipoMantenimiento_CS(String marca, String anio, String mes)
        {
            String EGMes;
            EGMes = ConvertMes(mes);
            List<String> tipoMantenimiento = new List<String>();
            JavaScriptSerializer sr = new JavaScriptSerializer();
            DataTable filtro_tipomantenimiento = LOGICA.MantenimientoL.Instancia.Mantenimiento_FallasSistema_SistemaImplicado_TipoMantenimiento_LO(marca, anio, EGMes);

            if (filtro_tipomantenimiento != null)
            {
                for (int i = 0; i < filtro_tipomantenimiento.Rows.Count; i++)
                {
                    if ((filtro_tipomantenimiento.Rows[i]["TipoMantenimiento"].ToString()) == "NULL")
                    {
                        tipoMantenimiento.Add("NO INDICA");
                    }
                    else
                    {
                        tipoMantenimiento.Add(filtro_tipomantenimiento.Rows[i]["TipoMantenimiento"].ToString());
                    }

                }
            }
            else { return null; }
            return sr.Serialize(tipoMantenimiento);
        }

     
        [WebMethod]
        public static object Mantenimiento_FallasSistema_SistemaImplicado_Marca_CS(String tipoMantenimiento, String anio, String mes)
        {
            String EGMes;
            EGMes = ConvertMes(mes);
            List<String> Marca = new List<String>();
            JavaScriptSerializer sr = new JavaScriptSerializer();
            DataTable filtromarca = LOGICA.MantenimientoL.Instancia.Mantenimiento_FallasSistema_SistemaImplicado_Marca_LO(tipoMantenimiento, anio, EGMes);
           if (filtromarca != null)
            {
                for (int i = 0; i < filtromarca.Rows.Count; i++)
                {
                    if ((filtromarca.Rows[i]["Marca"].ToString()) == "NULL")
                    {
                        Marca.Add("NO INDICA");
                    }
                    else
                    {
                        Marca.Add(filtromarca.Rows[i]["Marca"].ToString());
                    }

                }
            }
            else { return null; }
            return sr.Serialize(Marca);
        }
        [WebMethod]
        public static object Mantenimiento_FallasSistema_SistemaImplicado_Anio_CS(String tipoMantenimiento, String marca, String mes)
        {
            String EGMes;
            EGMes = ConvertMes(mes);
            List<String> Anio = new List<String>();
            JavaScriptSerializer sr = new JavaScriptSerializer();
            DataTable filtroanio = LOGICA.MantenimientoL.Instancia.Mantenimiento_FallasSistema_SistemaImplicado_Anio_LO(tipoMantenimiento, marca, EGMes);
            if (filtroanio != null)
            {
                for (int i = 0; i < filtroanio.Rows.Count; i++)
                {
                    if ((filtroanio.Rows[i]["Año"].ToString()) == "NULL")
                    {
                        Anio.Add("NO INDICA");
                    }
                    else
                    {
                        Anio.Add(filtroanio.Rows[i]["Año"].ToString());
                    }

                }
            }
            else { return null; }
            return sr.Serialize(Anio);
        }
        [WebMethod]
        public static object Mantenimiento_FallasSistema_SistemaImplicado_Mes_CS(String tipoMantenimiento, String marca, String anio)
        {
            List<String> Mes = new List<String>();
            JavaScriptSerializer sr = new JavaScriptSerializer();
            DataTable filtromes = LOGICA.MantenimientoL.Instancia.Mantenimiento_FallasSistema_SistemaImplicado_Mes_LO(tipoMantenimiento, marca, anio);
            if (filtromes != null)
            {
                for (int i = 0; i < filtromes.Rows.Count; i++)
                {
                    if ((filtromes.Rows[i]["Meses"].ToString()) == "NULL")
                    {
                        Mes.Add("NO INDICA");
                    }
                    else
                    {
                        Mes.Add(filtromes.Rows[i]["Meses"].ToString());
                    }

                }
            }
            else { return null; }
            return sr.Serialize(Mes);
        }
        [WebMethod]
        public static object Mantenimiento_FallasSistema_PorcentajeSistemaImplicado_TipoMantenimiento_CS(String marca, String anio, String mes)
        {
            String EGMes;
            EGMes = ConvertMes(mes);
            List<String> tipoMantenimiento = new List<String>();
            JavaScriptSerializer sr = new JavaScriptSerializer();
            DataTable filtro_tipomantenimiento = LOGICA.MantenimientoL.Instancia.Mantenimiento_FallasSistema_PorcentajeSistemaImplicado_TipoMantenimiento_LO(marca, anio, EGMes);

            if (filtro_tipomantenimiento != null)
            {
                for (int i = 0; i < filtro_tipomantenimiento.Rows.Count; i++)
                {
                    if ((filtro_tipomantenimiento.Rows[i]["TipoMantenimiento"].ToString()) == "NULL")
                    {
                        tipoMantenimiento.Add("NO INDICA");
                    }
                    else
                    {
                        tipoMantenimiento.Add(filtro_tipomantenimiento.Rows[i]["TipoMantenimiento"].ToString());
                    }

                }
            }
            else { return null; }
            return sr.Serialize(tipoMantenimiento);
        }
        [WebMethod]
        public static object Mantenimiento_FallasSistema_PorcentajeSistemaImplicado_Marca_CS(String tipoMantenimiento, String anio, String mes)
        {
            String EGMes;
            EGMes = ConvertMes(mes);
            List<String> Marca = new List<String>();
            JavaScriptSerializer sr = new JavaScriptSerializer();
            DataTable filtromarca = LOGICA.MantenimientoL.Instancia.Mantenimiento_FallasSistema_PorcentajeSistemaImplicado_Marca_LO(tipoMantenimiento, anio, EGMes);
            if (filtromarca != null)
            {
                for (int i = 0; i < filtromarca.Rows.Count; i++)
                {
                    if ((filtromarca.Rows[i]["Marca"].ToString()) == "NULL")
                    {
                        Marca.Add("NO INDICA");
                    }
                    else
                    {
                        Marca.Add(filtromarca.Rows[i]["Marca"].ToString());
                    }

                }
            }
            else { return null; }
            return sr.Serialize(Marca);
        }
        [WebMethod]
        public static object Mantenimiento_FallasSistema_PorcentajeSistemaImplicado_Anio_CS(String tipoMantenimiento, String marca, String mes)
        {
            String EGMes;
            EGMes = ConvertMes(mes);
            List<String> Anio = new List<String>();
            JavaScriptSerializer sr = new JavaScriptSerializer();
            DataTable filtroanio = LOGICA.MantenimientoL.Instancia.Mantenimiento_FallasSistema_PorcentajeSistemaImplicado_Anio_LO(tipoMantenimiento, marca, EGMes);
            if (filtroanio != null)
            {
                for (int i = 0; i < filtroanio.Rows.Count; i++)
                {
                    if ((filtroanio.Rows[i]["Año"].ToString()) == "NULL")
                    {
                        Anio.Add("NO INDICA");
                    }
                    else
                    {
                        Anio.Add(filtroanio.Rows[i]["Año"].ToString());
                    }

                }
            }
            else { return null; }
            return sr.Serialize(Anio);
        }

        [WebMethod]
        public static object Mantenimiento_FallasSistema_PorcentajeSistemaImplicado_Mes_CS(String tipoMantenimiento, String marca, String anio)
        {
            List<String> Mes = new List<String>();
            JavaScriptSerializer sr = new JavaScriptSerializer();
            DataTable filtromes = LOGICA.MantenimientoL.Instancia.Mantenimiento_FallasSistema_PorcentajeSistemaImplicado_Mes_LO(tipoMantenimiento, marca, anio);
            if (filtromes != null)
            {
                for (int i = 0; i < filtromes.Rows.Count; i++)
                {
                    if ((filtromes.Rows[i]["Meses"].ToString()) == "NULL")
                    {
                        Mes.Add("NO INDICA");
                    }
                    else
                    {
                        Mes.Add(filtromes.Rows[i]["Meses"].ToString());
                    }

                }
            }
            else { return null; }
            return sr.Serialize(Mes);
        }

        [WebMethod]
        public static Object Grafica_Falla_Sistema(String marca, String tipoMantenimiento, String anio, String mes)
        {
            String EGMes;
            EGMes = ConvertMes(mes);
            List<String> Marcas = new List<String>(); //nombre de marca
            List<Decimal> Data = new List<Decimal>();
            DataTable dt = LOGICA.MantenimientoL.Instancia.Listar_Fallas_Sistema(marca, tipoMantenimiento, anio, EGMes);

            if (dt != null)
            {
                int x = 0;
                foreach (DataRow dr in dt.Rows)
                {
                    Data.Add(Convert.ToDecimal((dr[x + 1]).ToString()));
                    Marcas.Add((dr[x]).ToString());
                }

                JavaScriptSerializer sr = new JavaScriptSerializer();
                return new { Marcas, Data, };
            }
            else { return null; }
        }

        [WebMethod]
        public static Object Grafica_Falla_Promedio_Sistema(String marca, String tipoMantenimiento, String anio, String mes)
        {
            String EGMes;
            EGMes = ConvertMes(mes);
            List<Contenido> contenido = new List<Contenido>();
            List<String> columnas = new List<String>(); //nombre de marca
            DataTable dt = LOGICA.MantenimientoL.Instancia.Listar_Fallas__Promedio_Sistema(marca, tipoMantenimiento, anio, EGMes);
            PieChart piechart = new PieChart();
            //piechart = null;
            if (dt != null)
            {
                foreach (DataColumn dataColumn in dt.Columns)
                {
                    columnas.Add(dataColumn.ColumnName);
                }
                piechart.name = "Brands";
                piechart.parametroLine = true;
                int m = 0, j = 0;

                foreach (DataRow dr in dt.Rows)
                {
                    j = 1;
                    while (j < columnas.Count)
                    {
                        j = 1;
                        //piechart.series.Add(new Contenido());
                        //string value = dr[j].ToString();

                        contenido.Add(new Contenido());
                        contenido[m].name = (dr[j - 1].ToString());
                        contenido[m].y = Convert.ToDecimal(dr[j].ToString());
                        contenido[m].sliced = true;
                        contenido[m].selected = true;
                        j++;
                    }
                    m++;
                }
                piechart.data = contenido;
                JavaScriptSerializer sr = new JavaScriptSerializer();
                return new { Series = sr.Serialize(piechart) };
            }
            else { return null; }
        }



    }
}