﻿using ENTIDAD.Graficas;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Script.Serialization;
using System.Web.Services;

namespace WebApplication1.Modulo.Mantenimiento
{
    public partial class DistribucionFlota : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {

        }
        public static String ConvertMes(String mes)
        {
            if (mes == "Enero") { mes = "1"; }
            if (mes == "Febrero") { mes = "2"; }
            if (mes == "Marzo") { mes = "3"; }
            if (mes == "Abril") { mes = "4"; }
            if (mes == "Mayo") { mes = "5"; }
            if (mes == "Junio") { mes = "6"; }
            if (mes == "Julio") { mes = "7"; }
            if (mes == "Agosto") { mes = "8"; }
            if (mes == "Septiembre") { mes = "9"; }
            if (mes == "Octubre") { mes = "10"; }
            if (mes == "Noviembre") { mes = "11"; }
            if (mes == "Diciembre") { mes = "12"; }
            return mes;
        }
        [WebMethod]
        public static object F_Mantenimiento_DisponibilidadFoltas_CS_ListarMes(String marca, String filtroModelo, String anio)
        {
            List<String> Marcas = new List<String>();
            JavaScriptSerializer sr = new JavaScriptSerializer();
            DataTable filtro_marca = LOGICA.MantenimientoL.Instancia.G_Mantenimiento_DisponibilidadFoltas_LO_ListarMes(marca, filtroModelo, anio);
            if (filtro_marca != null)
            {
                for (int i = 0; i < filtro_marca.Rows.Count; i++)
                {
                    if ((filtro_marca.Rows[i]["Meses"].ToString()) == "NULL")
                    {
                        Marcas.Add("NO INDICA");
                    }
                    else
                    {
                        Marcas.Add(filtro_marca.Rows[i]["Meses"].ToString());
                    }

                }
            }
            else { return new { Fecha = "", Name = "", Series = "" }; }
            return sr.Serialize(Marcas);
        }

        [WebMethod]
        public static object F_Mantenimiento_General_CS_ddMarca(String filtroModelo, String anio, String mes)
        {
            String EGMes;
            EGMes = ConvertMes(mes);
            List<String> Marcas = new List<String>();
            JavaScriptSerializer sr = new JavaScriptSerializer();
            DataTable filtro_marca = LOGICA.MantenimientoL.Instancia.G_Mantenimiento_DisponibilidadFoltas_LO_ListarMarca(filtroModelo, anio, EGMes);
            if (filtro_marca != null)
            {
                for (int i = 0; i < filtro_marca.Rows.Count; i++)
                {
                    if ((filtro_marca.Rows[i]["Marca"].ToString()) == "NULL")
                    {
                        Marcas.Add("NO INDICA");
                    }
                    else
                    {
                        Marcas.Add(filtro_marca.Rows[i]["Marca"].ToString());
                    }

                }
            }
            else { return new { Fecha = "", Name = "", Series = "" }; }
            return sr.Serialize(Marcas);
        }
        [WebMethod]
        public static object F_Mantenimiento_General_CS_ddModelo(String filtroMarca, String anio, String mes)
        {
            String EGMes;
            EGMes = ConvertMes(mes);
            List<String> Modelo = new List<String>();
            JavaScriptSerializer sr = new JavaScriptSerializer();
            DataTable filtro_modelo = LOGICA.MantenimientoL.Instancia.G_Mantenimiento_DisponibilidadFoltas_LO_ListarModelo(filtroMarca, anio, EGMes);
            if (filtro_modelo != null)
            {
                for (int i = 0; i < filtro_modelo.Rows.Count; i++)
                {
                    if ((filtro_modelo.Rows[i]["Modelo"].ToString()) == "NULL")
                    {
                        Modelo.Add("NO INDICA");
                    }
                    else
                    {
                        Modelo.Add(filtro_modelo.Rows[i]["Modelo"].ToString());
                    }

                }
            }
            else { return new { Fecha = "", Name = "", Series = "" }; }
            return sr.Serialize(Modelo);
        }
        [WebMethod]
        public static object F_Mantenimiento_General_CS_ddAnio(String marca, String filtroModelo, String anio, String mes)
        {
            String EGMes;
            EGMes = ConvertMes(mes);
            List<String> Anio = new List<String>();
            JavaScriptSerializer sr = new JavaScriptSerializer();
            DataTable dt = LOGICA.MantenimientoL.Instancia.G_Mantenimiento_DisponibilidadFoltas_LO_ListarAnio(marca, filtroModelo, anio, EGMes);
            if (dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if ((dt.Rows[i]["Anio"].ToString()) == "NULL")
                    {
                        Anio.Add("NO INDICA");
                    }
                    else
                    {
                        Anio.Add(dt.Rows[i]["Anio"].ToString());
                    }
                }
            }
            else { return new { Fecha = "", Name = "", Series = "" }; }
            return sr.Serialize(Anio);
        }
        [WebMethod]
        public static object Grafica_Distribucion_Flota(String filtroModelo, String filtroMarca, String anio, String mes, String agrupacionF)
        {
            String EGMes;
            EGMes = ConvertMes(mes);
            Decimal val;
            int agrupacion = Convert.ToInt32(agrupacionF);
            List<Object> contenedor = new List<Object>();
            List<String> Columnas = new List<String>(); //nombre de marca
            List<Ratio> series = new List<Ratio>();
            DataTable dt = LOGICA.MantenimientoL.Instancia.Listar_Disponibilidad_Flotas(filtroModelo, filtroMarca, anio, EGMes);
            LineChart data = new LineChart();

            if (dt != null)
            {
                foreach (DataColumn dataColumn in dt.Columns)
                {
                    Columnas.Add(dataColumn.ColumnName);
                }
                int j = 1;

                foreach (DataRow dr in dt.Rows)
                {
                    j = 1;
                    data.Fecha.Add(dr[0].ToString());
                    data.Name = Columnas;

                    while (j < Columnas.Count)
                    {
                        if (data.Series.Count < Columnas.Count - 1)
                            data.Series.Add(new Ratio());
                        string value = dr[j].ToString();
                        val = (Convert.ToDecimal(value) * 100);
                        data.Series[j - 1].data.Add(Math.Round(val,1));
                        data.Series[j - 1].name = Columnas[j];
                        j++;
                    }
                }
                if (agrupacion == 0)
                {
                    JavaScriptSerializer sr = new JavaScriptSerializer();
                    return new { Fecha = sr.Serialize(data.Fecha), Name = sr.Serialize(data.Name), Series = sr.Serialize(data.Series) };
                }
            }
            String fechaAnio = "";
            Decimal acumulador = 0;
            int contadorTrimestre = 1;
            String anioActual = "";
            String nombreAgrupacion = "";
            LineChart data1 = new LineChart();
            if (agrupacion != 0)
            {
                if (agrupacion == 2) { nombreAgrupacion = "Bimestre "; }
                if (agrupacion == 3) { nombreAgrupacion = "Trimestre "; }

                for (int m = 0; m < data.Fecha.Count; m++)
                {
                    String fecha = ((data.Fecha[m]).Substring((data.Fecha[m]).Length - 4));
                    if (anio != "") { fechaAnio = (anio).ToString(); }
                    else
                    {
                        fechaAnio = fecha;
                    }
                    if (fechaAnio != anioActual) { contadorTrimestre = 1; }

                    if (m % agrupacion == 0)
                    {
                        data1.Fecha.Add(nombreAgrupacion + (contadorTrimestre).ToString() + " del " + fechaAnio);

                        contadorTrimestre++;
                    }
                    else
                    {
                        if (m == data.Fecha.Count - 1)
                        {
                            data1.Fecha.Add(nombreAgrupacion + (contadorTrimestre).ToString() + " del " + fechaAnio);

                            contadorTrimestre++;
                        }
                    }
                    anioActual = fechaAnio;
                }
                int contadorObjeto = 0;
                for (int i = 0; i < data.Series.Count; i++)
                {
                    data1.Series.Add(new Ratio());
                    for (int j = 0; j < data.Series[i].data.Count; j++)
                    {
                        contadorObjeto = j + 1;
                        var value = data.Series[i].data[j];
                        acumulador = acumulador + value;
                        if (contadorObjeto % agrupacion == 0 && contadorObjeto != 0)
                        {
                            data1.Series[i].data.Add(acumulador / agrupacion);
                            acumulador = 0;
                        }
                        else
                        {
                            if (contadorObjeto == data.Series[i].data.Count)
                            {
                                data1.Series[i].data.Add(acumulador / agrupacion);
                                acumulador = 0;
                            }
                        }

                    }
                    data1.Series[i].name = Columnas[i + 1];
                }
                JavaScriptSerializer sr = new JavaScriptSerializer();
                return new { Fecha = sr.Serialize(data1.Fecha), Name = sr.Serialize(data.Name), Series = sr.Serialize(data1.Series) };
            }
            else { return null; }
        }
        //[WebMethod]
        //public static object G_Mantenimiento_CantidadGeneral_Modelo_CS(String filtroModelo, String filtroMarca, String anio, String mes, String agrupacionF)
        //{
        //    int indiceColum = 0;
        //    int agrupacion = Convert.ToInt32(agrupacionF);
        //    List<String> Modelo = new List<String>(); //nombre de marca
        //    List<Ratio> series = new List<Ratio>();
        //    DataTable dt = new DataTable();
        //    dt = LOGICA.MantenimientoL.Instancia.G_Mantenimiento_CantidadGeneral_Modelo_LO(filtroModelo, filtroMarca, anio, mes);
        //    LineChart data = new LineChart();
        //    Ratio ratio = new Ratio();

        //    if (dt != null)
        //    {
        //        Modelo.Add("fecha");
        //        if (filtroMarca != "") { Modelo.Add(filtroMarca.ToString()); }
        //        else
        //        {
        //            DataTable dt_modelo = LOGICA.MantenimientoL.Instancia.G_Mantenimiento_DisponibilidadFoltas_LO_ListarModelo(filtroMarca, anio, mes);
        //            if (dt_modelo != null)
        //            {
        //                foreach (DataRow dm in dt_modelo.Rows)
        //                {
        //                    Modelo.Add(dm[0].ToString());

        //                    indiceColum++;
        //                }
        //            }
        //        }
        //        int j = 1;
        //        int indiceFecha = 0;
        //        foreach (DataRow dr in dt.Rows)
        //        {
        //            j = 1;
        //            data.Fecha.Add(string.Format("{0:Y}", Convert.ToDateTime(dr[indiceFecha])));

        //            //data.Name = Modelo;

        //            while (j < Modelo.Count)
        //            {
        //                if (data.Series.Count < Modelo.Count - 1)
        //                    data.Series.Add(new Ratio());
        //                string value = (dr[j].ToString()).Substring(0, 5);
        //                if (value == null || value == "") { data.Series[j - 1].data.Add(Convert.ToDecimal(0)); }
        //                else
        //                { data.Series[j - 1].data.Add(Convert.ToDecimal(value) * 100); }
        //                data.Series[j - 1].name = Modelo[j];
        //                j++;
        //            }
        //        }
        //        if (agrupacion == 0)
        //        {
        //            JavaScriptSerializer sr = new JavaScriptSerializer();
        //            return new { Fecha = sr.Serialize(data.Fecha), Name = sr.Serialize(Modelo), Series = sr.Serialize(data.Series) };
        //        }
        //    }

        //    String fechaAnio = "";
        //    Decimal acumulador = 0;
        //    int contadorTrimestre = 1;
        //    String anioActual = "";
        //    String nombreAgrupacion = "";
        //    LineChart data1 = new LineChart();
        //    if (agrupacion != 0)
        //    {
        //        if (agrupacion == 2) { nombreAgrupacion = "Bimestre "; }
        //        if (agrupacion == 3) { nombreAgrupacion = "Trimestre "; }

        //        for (int m = 0; m < data.Fecha.Count; m++)
        //        {
        //            String fecha = ((data.Fecha[m]).Substring((data.Fecha[m]).Length - 4));
        //            if (anio != "") { fechaAnio = (anio).ToString(); }
        //            else
        //            {
        //                fechaAnio = fecha;
        //            }
        //            if (fechaAnio != anioActual) { contadorTrimestre = 1; }
                    
        //            if (m % agrupacion == 0)
        //            {
        //                data1.Fecha.Add(nombreAgrupacion + (contadorTrimestre).ToString() + " del " + fechaAnio);
                       
        //                contadorTrimestre++;
        //            }
        //            else
        //            {
        //                if (m == data.Fecha.Count - 1)
        //                {
        //                    data1.Fecha.Add(nombreAgrupacion + (contadorTrimestre).ToString() + " del " + fechaAnio);
                            
        //                    contadorTrimestre++;
        //                }
        //            }
        //            anioActual = fechaAnio;
        //        }
        //        int contadorObjeto = 0;
        //        for (int i = 0; i < data.Series.Count; i++)
        //        {
        //            data1.Series.Add(new Ratio());
        //            for (int j = 0; j < data.Series[i].data.Count; j++)
        //            {
        //                contadorObjeto = j + 1;
        //                var value = data.Series[i].data[j];
        //                acumulador = acumulador + value;
        //                if (contadorObjeto % agrupacion == 0 && contadorObjeto != 0)
        //                {
        //                    data1.Series[i].data.Add(acumulador / agrupacion);
        //                    acumulador = 0;
        //                }
        //                else
        //                {
        //                    if (contadorObjeto == data.Series[i].data.Count)
        //                    {
        //                        data1.Series[i].data.Add(acumulador / agrupacion);
        //                        acumulador = 0;
        //                    }
        //                }

        //            }
        //            data1.Series[i].name = Modelo[i + 1];
        //        }
        //        JavaScriptSerializer sr = new JavaScriptSerializer();
        //        return new { Fecha = sr.Serialize(data1.Fecha), Name = sr.Serialize(data.Name), Series = sr.Serialize(data1.Series) };
        //    }
        //    else { return new { Fecha = "", Name = "", Series = "" }; }
        //}


    }
}