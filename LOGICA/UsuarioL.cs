﻿using DATO;
using System;

namespace LOGICA
{
    public class UsuarioL
    {
        //documento xyz
        #region singleton
        private static readonly UsuarioL _instancia = new UsuarioL();
        public static UsuarioL Instancia
        {
            get { return UsuarioL._instancia; }
        }
        #endregion singleton

        #region metodosUsuario
        public Boolean ValidarUsuario(String usuario, String password)
        {
            Boolean validar = UsuarioD.Instancia.Validar_Usuario(usuario, password);
            return validar;
        }
        #endregion


    }
}
