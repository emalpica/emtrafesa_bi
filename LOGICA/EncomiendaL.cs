﻿using DATO;
using System;
using System.Data;

namespace LOGICA
{
    public class EncomiendaL
    {

        #region singleton
        private static readonly EncomiendaL _instancia = new EncomiendaL();
        public static EncomiendaL Instancia
        {
            get { return EncomiendaL._instancia; }
        }
        #endregion singleton
        //Giampiere
        #region Ventas por Sucursal
        //GIAMPIERE 22/ 08 / 2019
        public DataTable G_Encomienda_VentaSucursal_GeneralMensual_LO(String mes, String tipo)
        {
            DataTable ent = EncomiendaD.Instancia.G_Encomienda_VentaSucursal_GeneralMensual_Dao(mes,tipo);
            return ent;
        }
        //GIAMPIERE 22/ 08 / 2019
        public DataTable Encomienda_VentaSucursal_GeneralMensual_ddlMes_LO( String tipo)
        {
            DataTable ddlMes = EncomiendaD.Instancia.Encomienda_VentaSucursal_GeneralMensual_ddlMes_Dao(tipo);
            return ddlMes;
        }
        //GIAMPIERE 22/ 08 / 2019
        public DataTable Encomienda_VentaSucursal_GeneralMensual_ddlTipo_LO(String mes)
        {
            DataTable ddlTipo = EncomiendaD.Instancia.Encomienda_VentaSucursal_GeneralMensual_ddlTipo_Dao(mes);
            return ddlTipo;
        }
        //GIAMPIERE 23/ 08 / 2019
        //General ORIGEN
        public DataTable G_Encomienda_VentaSucursal_GeneralOrigen_LO(String anio, String mes, String tipo)
        {
            DataTable ent = EncomiendaD.Instancia.G_Encomienda_VentaSucursal_GeneralOrigen_Dao( anio, mes, tipo);
            return ent;
        }
        //GIAMPIERE 27/ 08 / 2019
        public DataTable Encomienda_VentaSucursal_GeneralOrigen_AgrupacionTrimestre_LO(String anio, String tipo)
        {
            DataTable ent = EncomiendaD.Instancia.Encomienda_VentaSucursal_GeneralOrigen_AgrupacionTrimestre_Dao(anio,tipo);
            return ent;
        }
        //GIAMPIERE 27/ 08 / 2019
        public DataTable Encomienda_VentaSucursal_GeneralOrigen_AgrupacionBimestre_LO(String anio, String tipo)
        {
            DataTable ent = EncomiendaD.Instancia.Encomienda_VentaSucursal_GeneralOrigen_AgrupacionBimestre_Dao(anio,tipo);
            return ent;
        }
        //GIAMPIERE 23/ 08 / 2019
        public DataTable Encomienda_VentaSucursal_GeneralOrigen_ddlMes_LO(String mes, String tipo)
        {
            DataTable ddlMes = EncomiendaD.Instancia.Encomienda_VentaSucursal_GeneralOrigen_ddlMes_Dao(mes, tipo);
            return ddlMes;
        }
        //GIAMPIERE 23/ 08 / 2019
        public DataTable Encomienda_VentaSucursal_GeneralOrigen_ddlAño_LO(String mes, String tipo)
        {
            DataTable ddlMes = EncomiendaD.Instancia.Encomienda_VentaSucursal_GeneralOrigen_ddlAño_Dao(mes,tipo);
            return ddlMes;
        }
        //GIAMPIERE 23/ 08 / 2019
        public DataTable Encomienda_VentaSucursal_GeneralOrigen_ddlTipo_LO(String anio, String mes)
        {
            DataTable ddlTipo = EncomiendaD.Instancia.Encomienda_VentaSucursal_GeneralOrigen_ddlTipo_Dao(anio,mes);
            return ddlTipo;
        }
        //GIAMPIERE 26/ 08 / 2019
        //General Mensual por Origen
        public DataTable G_Encomienda_VentaSucursal_GeneralMensualOrigen_LO(String anio, String mes, String tipo)
        {
            DataTable ent = EncomiendaD.Instancia.G_Encomienda_VentaSucursal_GeneralMensualOrigen_Dao(anio, mes, tipo);
            return ent;
        }
        //GIAMPIERE 26/ 08 / 2019
        public DataTable Encomienda_VentaSucursal_GeneralMensualOrigen_ddlMes_LO(String anio, String tipo)
        {
            DataTable ddlMes = EncomiendaD.Instancia.Encomienda_VentaSucursal_GeneralMensualOrigen_ddlMes_Dao(anio, tipo);
            return ddlMes;
        }
        //GIAMPIERE 26/ 08 / 2019
        public DataTable Encomienda_VentaSucursal_GeneralMensualOrigen_ddlAño_LO(String mes, String tipo)
        {
            DataTable ddlAño = EncomiendaD.Instancia.Encomienda_VentaSucursal_GeneralMensualOrigen_ddlAño_Dao(mes, tipo);
            return ddlAño;
        }
        //GIAMPIERE 26/ 08 / 2019
        public DataTable Encomienda_VentaSucursal_GeneralMensualOrigen_ddlTipo_LO(String mes, String anio)
        {
            DataTable ddlTipo = EncomiendaD.Instancia.Encomienda_VentaSucursal_GeneralMensualOrigen_ddlTipo_Dao(mes, anio);
            return ddlTipo;
        }
        #endregion

        #region Cliente top
        //GIAMPIERE 10/ 09 / 2019
        //Cantidad Encomienda
        public DataTable G_Encomienda_ClientesTop_CantidaEncomiendas_LO(String anio, String mes, String operacion)
        {
            DataTable ent = EncomiendaD.Instancia.G_Encomienda_ClientesTop_CantidaEncomiendas_Dao(anio, mes, operacion);
            return ent;
        }
        //GIAMPIERE 10/ 09 / 2019
        public DataTable Encomienda_ClientesTop_CantidaEncomiendas_AgrupacionTrimestre_LO(String anio, String operacion)
        {
            DataTable ent = EncomiendaD.Instancia.Encomienda_ClientesTop_CantidaEncomiendas_AgrupacionTrimestre_Dao(anio, operacion);
            return ent;
        }
        //GIAMPIERE 10/ 09 / 2019
        public DataTable Encomienda_ClientesTop_CantidaEncomiendas_AgrupacionBimestre_LO(String anio, String operacion)
        {
            DataTable ent = EncomiendaD.Instancia.Encomienda_ClientesTop_CantidaEncomiendas_AgrupacionBimestre_Dao(anio, operacion);
            return ent;
        }
        //GIAMPIERE 10/ 09 / 2019
        public DataTable Encomienda_ClientesTop_CantidaEncomiendas_ddlMes_LO(String mes, String operacion)
        {
            DataTable ddlMes = EncomiendaD.Instancia.Encomienda_ClientesTop_CantidaEncomiendas_ddlMes_Dao(mes, operacion);
            return ddlMes;
        }
        //GIAMPIERE 10/ 09 / 2019
        public DataTable Encomienda_ClientesTop_CantidaEncomiendas_ddlAño_LO(String mes, String operacion)
        {
            DataTable ddlMes = EncomiendaD.Instancia.Encomienda_ClientesTop_CantidaEncomiendas_ddlAño_Dao(mes, operacion);
            return ddlMes;
        }

        //GIAMPIERE 10/ 09 / 2019
        public DataTable Encomienda_ClientesTop_CantidaEncomiendas_ddlOperacion_LO(String mes, String anio)
        {
            DataTable ddlTipo = EncomiendaD.Instancia.Encomienda_ClientesTop_CantidaEncomiendas_ddlOperacion_Dao(mes, anio);
            return ddlTipo;
        }
        //Segn Rentabilidad
        //GIAMPIERE 11/ 09 / 2019
        public DataTable G_Encomienda_ClientesTop_Rentabilidad_LO(String anio, String mes, String operacion)
        {
            DataTable ent = EncomiendaD.Instancia.G_Encomienda_ClientesTop_Rentabilidad_Dao(anio, mes, operacion);
            return ent;
        }
        //GIAMPIERE 11/ 09 / 2019
        public DataTable Encomienda_ClientesTop_Rentabilidad_AgrupacionTrimestre_LO(String anio, String operacion)
        {
            DataTable ent = EncomiendaD.Instancia.Encomienda_ClientesTop_Rentabilidad_AgrupacionTrimestre_Dao(anio, operacion);
            return ent;
        }
        //GIAMPIERE 11/ 09 / 2019
        public DataTable Encomienda_ClientesTop_Rentabilidad_AgrupacionBimestre_LO(String anio, String operacion)
        {
            DataTable ent = EncomiendaD.Instancia.Encomienda_ClientesTop_Rentabilidad_AgrupacionBimestre_Dao(anio, operacion);
            return ent;
        }
        //GIAMPIERE 11/ 09 / 2019
        public DataTable Encomienda_ClientesTop_Rentabilidad_ddlMes_LO(String mes, String operacion)
        {
            DataTable ddlMes = EncomiendaD.Instancia.Encomienda_ClientesTop_Rentabilidad_ddlMes_Dao(mes, operacion);
            return ddlMes;
        }
        //GIAMPIERE 11/ 09 / 2019
        public DataTable Encomienda_ClientesTop_Rentabilidad_ddlAño_LO(String mes, String operacion)
        {
            DataTable ddlMes = EncomiendaD.Instancia.Encomienda_ClientesTop_Rentabilidad_ddlAño_Dao(mes, operacion);
            return ddlMes;
        }
        //GIAMPIERE 11/ 09 / 2019
        public DataTable Encomienda_ClientesTop_Rentabilidad_ddlOperacion_LO(String mes, String anio)
        {
            DataTable ddlTipo = EncomiendaD.Instancia.Encomienda_ClientesTop_Rentabilidad_ddlOperacion_Dao(mes, anio);
            return ddlTipo;
        }
        #endregion


        #region SUBREGION_RUTAVEHICULOS
        /* 
            DESARROLLADOR: JORGE BALTODANO
            KPI: ENCOMIENDAS
        */

        // GRAFICA: CANTIDAD VIAJE SEGUN RUTA POR TIPO VEHICULO
        public DataTable Grafica_Encomiendas_RutasVehiculos_GeneralTipoVehiculo_LO(String Año, String Mes, String Trimestre, String Bimestre)
        {
            DataTable dt_grafica_GTV = EncomiendaD.Instancia.Grafica_Encomiendas_RutasVehiculos_GeneralTipoVehiculo_DAO(Año, Mes, Trimestre, Bimestre);
            return dt_grafica_GTV;
        }

        // Listar Año - General por Tipo de Vehiculo
        public DataTable Encomiendas_RutasVehiculos_ListarAño_GTV_LO(String Mes, String Trimestre, String Bimestre)
        {
            DataTable dt_Año = EncomiendaD.Instancia.Encomiendas_RutasVehiculos_ListarAño_GTV_DAO(Mes, Trimestre, Bimestre);
            return dt_Año;
        }

        // Listar Meses - General por Tipo de Vehiculo
        public DataTable Encomiendas_RutasVehiculos_ListarMes_GTV_LO(String Año)
        {
            DataTable dt_Mes = EncomiendaD.Instancia.Encomiendas_RutasVehiculos_ListarMes_GTV_DAO(Año);
            return dt_Mes;
        }

        // Listar Bimestre- General por Tipo de Vehiculo
        public DataTable Encomiendas_RutasVehiculos_ListarBimestre_GTV_LO(String Año)
        {
            DataTable dt_Bimestre = EncomiendaD.Instancia.Encomiendas_RutasVehiculos_ListarBimestre_GTV_DAO(Año);
            return dt_Bimestre;
        }

        // Listar Trimestre - General por Tipo de Vehiculo
        public DataTable Encomiendas_RutasVehiculos_ListarTrimestre_GTV_LO(String Año)
        {
            DataTable dt_Mes = EncomiendaD.Instancia.Encomiendas_RutasVehiculos_ListarTrimestre_GTV_DAO(Año);
            return dt_Mes;
        }


        // ##### GRAFICA: CANTIDAD VIAJE SEGUN RUTA POR TIEMPO ######
        public DataTable Grafica_Encomiendas_RutasVehiculos_GeneralRutaTiempo_LO(String anio, String mes, String tipo)  
        {
            DataTable dt_grafica_GRT = EncomiendaD.Instancia.Grafica_Encomiendas_RutasVehiculos_GeneralRutaTiempo_DAO(anio, mes, tipo);
            return dt_grafica_GRT;
        }

        // Metodo para Trimestre Grafica
         public DataTable Grafica_Encomienda_RutasVehiculos_GeneralRutaTiempo_AgrupacionTrimestre_LO(String anio, String tipo)
        {
            DataTable ent = EncomiendaD.Instancia.Grafica_Encomienda_RutasVehiculos_GeneralRutaTiempo_AgrupacionTrimestre_DAO(anio,tipo);
            return ent;
        }

        // Metodo para Bimestre Grafica 
         public DataTable Grafica_Encomienda_RutasVehiculo_GeneralRutaTiempo_AgrupacionBimestre_LO(String anio, String tipo)
        {
            DataTable ent = EncomiendaD.Instancia.Grafica_Encomienda_RutaVehiculos_GeneralRutaTiempo_AgrupacionBimestre_DAO(anio,tipo);
            return ent;
        }


        // #####################   Metodos para Combos ######################

        public DataTable Encomiendas_RutasVehiculos_ListarAño_GRT_LO(String mes, String tipo)
        {
            DataTable dt_año_GRT = EncomiendaD.Instancia.Encomiendas_RutasVehiculos_ListarAño_GRT_DAO(mes, tipo);
            return dt_año_GRT;
        }

        public DataTable Encomiendas_RutasVehiculos_ListarMes_GRT_LO(String anio, String tipo)
        {
            DataTable dt_mes_GRT = EncomiendaD.Instancia.Encomiendas_RutasVehiculos_ListarMes_GRT_DAO(anio, tipo);
            return dt_mes_GRT;
        }
         
        public DataTable Encomiendas_RutasVehiculos_ListarTipo_GRT_LO(String anio, String mes)
        {
            DataTable dt_tipo_GRT = EncomiendaD.Instancia.Encomiendas_RutasVehiculos_ListarTipo_GRT_DAO(anio, mes);
            return dt_tipo_GRT;
        }

        

        // GRAFICA: CANTIDAD VIAJE SEGUN RUTA 
        public DataTable Grafica_Encomiendas_RutaVehiculos_CantidadViajesRuta_LO(String Año, String Mes)
        {
            DataTable dt_Grafica_CVR = EncomiendaD.Instancia.Grafica_Encomiendas_RutaVehiculos_CantidadViajesRuta_DAO(Año, Mes);
            return dt_Grafica_CVR;
        }

         public DataTable Encomiendas_RutaVehiculos_ListarMes_CVR_LO(String Año)
        {
            DataTable dt_Mes_CVR = EncomiendaD.Instancia.Encomiendas_RutaVehiculos_ListarMes_CVR_DAO(Año);
            return dt_Mes_CVR;
        }

           public DataTable Encomiendas_RutaVehiculos_ListarAño_CVR_LO(String Mes)
        {
            DataTable dt_Año_CVR = EncomiendaD.Instancia.Encomiendas_RutaVehiculos_ListarAño_CVR_DAO(Mes);
            return dt_Año_CVR;
        }

        #endregion

    }
}
