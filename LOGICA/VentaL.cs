﻿using DATO;
using System.Data;


namespace LOGICA
{
    public class VentaL
    {
        #region singleton
        private static readonly VentaL _instancia = new VentaL();
        public static VentaL Instancia
        {
            get { return VentaL._instancia; }
        }
        #endregion singleton

        #region metodosMantenimiento

        public DataTable Listar_Informes_Venta()
        {
            DataTable ent = VentasD.Instancia.Listar_Volumnen_Ventas_Anuales();
            return ent;
        }

        public DataTable Listar_Comparativo_Historico()
        {
            DataTable ent = VentasD.Instancia.Listar_Comparativo_Historico();
            return ent;
        }

        public DataTable Listar_Mayor_Crecimiento_Menual()
        {
            DataTable ent = VentasD.Instancia.Listar_Mayor_Crecimiento_Menual();
            return ent;
        }

        public DataTable Listar_Menor_Crecimiento_Menual()
        {
            DataTable ent = VentasD.Instancia.Listar_Menor_Crecimiento_Menual();
            return ent;
        }

        #endregion
    }
}
