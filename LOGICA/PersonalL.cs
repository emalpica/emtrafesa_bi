﻿using DATO;
using System;
using System.Data;

namespace LOGICA
{
    public class PersonalL
    {
        
        #region singleton
        private static readonly PersonalL _instancia = new PersonalL();
        public static PersonalL Instancia
        {
            get { return PersonalL._instancia; }
        }

        #endregion singleton

        #region Cantidad de Colaboradores
        public DataTable G_Personal_CantidadColaboradores_cantidadGeneral_LO()
        {
            DataTable cantidad = PersonalD.Instancia.G_Personal_CantidadColaboradores_cantidadGeneral_Dao();
            return cantidad;
        }
        //GIAMPIERE  20/ 09 / 2019
        //CANTIDAD POR DEPARTAMENTO
        public DataTable G_Personal_CantidadColaboradores_CantidadArea_LO(String area, String subArea, String sucursal)
        {        //GIAMPIERE  20/ 09 / 2019
            DataTable ent = PersonalD.Instancia.G_Personal_CantidadColaboradores_CantidadArea_Dao(area, subArea, sucursal);
            return ent;
        }
        public DataTable Personal_CantidadColaboradores_CantidadArea_ddlSucursal_LO(String area, String subArea)
        {        //GIAMPIERE  20/ 09 / 2019
            DataTable Sucursal = PersonalD.Instancia.Personal_CantidadColaboradores_CantidadArea_ddlSucursal_Dao(area, subArea);
            return Sucursal;
        }
        public DataTable Personal_CantidadColaboradores_CantidadArea_ddlArea_LO(String sucursal, String subArea)
        {        //GIAMPIERE  20/ 09 / 2019
            DataTable Area = PersonalD.Instancia.Personal_CantidadColaboradores_CantidadArea_ddlArea_Dao(sucursal, subArea);
            return Area;
        }
        public DataTable Personal_CantidadColaboradores_CantidadArea_ddlSubArea_LO(String sucursal, String area)
        {        //GIAMPIERE  20/ 09 / 2019
            DataTable SubArea = PersonalD.Instancia.Personal_CantidadColaboradores_CantidadArea_ddlSubArea_Dao(sucursal, area);
            return SubArea;
        }
        //EVOLUCIÓN HISTÓRICA_EVOLUCIÓN GENERAL MENSUAL
        public DataTable G_Personal_CantidadColaboradores_EvolucionGeneralMensual_LO(String mes, String departamento, String area, String subArea, String sucursal)
        {//GIAMPIERE 12/ 08 / 2019
            DataTable ent = PersonalD.Instancia.G_Personal_CantidadColaboradores_EvolucionGeneralMensual_Dao(mes, departamento, area, subArea, sucursal);
            return ent;
        }
        public DataTable Personal_CantidadColaboradores_EvolucionGeneralMensual_dllSucursal_LO(String mes, String departamento, String area, String subArea)
        {//GIAMPIERE 12/ 08 / 2019
            DataTable ddlSucursal = PersonalD.Instancia.Personal_CantidadColaboradores_EvolucionGeneralMensual_dllSucursal_Dao(mes, departamento, area, subArea);
            return ddlSucursal;
        }
        public DataTable Personal_CantidadColaboradores_EvolucionGeneralMensual_dllDepartamento_LO(String mes, String area, String subArea, String sucursal)
        {//GIAMPIERE 12/ 08 / 2019
            DataTable ddlDepartamento = PersonalD.Instancia.Personal_CantidadColaboradores_EvolucionGeneralMensual_dllDepartamento_Dao(mes, area, subArea, sucursal);
            return ddlDepartamento;
        }
        public DataTable Personal_CantidadColaboradores_EvolucionGeneralMensual_ddlArea_LO(String mes, String departamento, String subArea, String sucursal)
        {//GIAMPIERE 12/ 08 / 2019
            DataTable ddlArea = PersonalD.Instancia.Personal_CantidadColaboradores_EvolucionGeneralMensual_ddlArea_Dao(mes, departamento, subArea, sucursal);
            return ddlArea;
        }
        public DataTable Personal_CantidadColaboradores_EvolucionGeneralMensual_ddlSubArea_LO(String mes, String departamento, String area, String sucursal)
        {//GIAMPIERE 12/ 08 / 2019
            DataTable ddlSubArea = PersonalD.Instancia.Personal_CantidadColaboradores_EvolucionGeneralMensual_ddlSubArea_Dao(mes, departamento, area, sucursal);
            return ddlSubArea;
        }
        public DataTable Personal_CantidadColaboradores_EvolucionGeneralMensual_ddlMes_LO(String departamento, String area, String subArea, String sucursal)
        {//GIAMPIERE 12/ 08 / 2019
            DataTable ddlMes = PersonalD.Instancia.Personal_CantidadColaboradores_EvolucionGeneralMensual_ddlMes_Dao(departamento, area, subArea, sucursal);
            return ddlMes;
        }
        //EVOLUCIÓN HISTÓRICA_EVOLUCIÓN POR ÁREA MENSUAL
        public DataTable G_Personal_CantidadColaboradores_EvolucionAreaMensual_LO(String anio, String mes, String area, String subArea, String sucursal)
        {        //GIAMPIERE 12/ 08 / 2019
            DataTable ent = PersonalD.Instancia.G_Personal_CantidadColaboradores_EvolucionAreaMensual_Dao(anio, mes, area, subArea, sucursal);
            return ent;
        }
        public DataTable Personal_CantidadColaboradores_EvolucionAreaMensual_ddlSucursal_LO(String anio, String mes, String area, String subArea)
        {        //GIAMPIERE 12/ 08 / 2019
            DataTable ddlSucursal = PersonalD.Instancia.Personal_CantidadColaboradores_EvolucionAreaMensual_ddlSucursal_Dao(anio, mes, area, subArea);
            return ddlSucursal;
        }
        public DataTable Personal_CantidadColaboradores_EvolucionAreaMensual_ddlArea_LO(String anio, String mes, String subArea, String sucursal)
        {        //GIAMPIERE 12/ 08 / 2019
            DataTable ddlArea = PersonalD.Instancia.Personal_CantidadColaboradores_EvolucionAreaMensual_ddlArea_Dao(anio, mes, subArea, sucursal);
            return ddlArea;
        }
        public DataTable Personal_CantidadColaboradores_EvolucionAreaMensual_ddlSubArea_LO(String anio, String mes, String area, String sucursal)
        {        //GIAMPIERE 12/ 08 / 2019
            DataTable ddlSubArea = PersonalD.Instancia.Personal_CantidadColaboradores_EvolucionAreaMensual_ddlSubArea_Dao(anio, mes, area, sucursal);
            return ddlSubArea;
        }
        public DataTable Personal_CantidadColaboradores_EvolucionAreaMensual_ddlAnio_LO(String mes, String area, String subArea, String sucursal)
        {        //GIAMPIERE 12/ 08 / 2019
            DataTable ddlAnio = PersonalD.Instancia.Personal_CantidadColaboradores_EvolucionAreaMensual_ddlAnio_Dao(mes, area, subArea, sucursal);
            return ddlAnio;
        }
        public DataTable Personal_CantidadColaboradores_EvolucionAreaMensual_ddlMes_LO(String anio, String area, String subArea, String sucursal)
        {        //GIAMPIERE 12/ 08 / 2019
            DataTable ddlMes = PersonalD.Instancia.Personal_CantidadColaboradores_EvolucionAreaMensual_ddlMes_Dao(anio, area, subArea, sucursal);
            return ddlMes;
        }
        #endregion Cantidad de Colaboradores

        #region Costos Planilla

        public DataTable G_Personal_CostosPlanilla_LO_cantidadGeneral()
        {
            DataTable cantidad = PersonalD.Instancia.G_Personal_CostoPlanilla_DAO_CantidadGeneral();
            return cantidad;
        }

        public DataTable G_Personal_costosPlanilla_LO_cantidadArea(String sucursal, String area, String subarea, String fechaInicio, String fechaFin)
        {
            DataTable ent = PersonalD.Instancia.G_Personal_CostoPlanilla_DAO_CantidadGeneral(sucursal, area, subarea, fechaInicio, fechaFin);
            return ent;
        }

        public DataTable G_Personal_CostoPlanilla_LO_ddlSucursal(String area, String subarea, String fechaInicio, String fechaFin)
        {
            DataTable ent = PersonalD.Instancia.G_Personal_CostoPlanilla_DAO_ddlSucursal(area, subarea, fechaInicio, fechaFin);
            return ent;
        }

        public DataTable G_Personal_CostoPlanilla_LO_ddlSubArea(String area, String sucursal, String fechaInicio, String fechaFin)
        {
            DataTable ent = PersonalD.Instancia.G_Personal_CostoPlanilla_DAO_ddlSubArea(area, sucursal, fechaInicio, fechaFin);
            return ent;
        }

        public DataTable G_Personal_CostoPlanilla_LO_ddlArea(String sucursal, String subarea, String fechaInicio, String fechaFin)
        {
            DataTable ent = PersonalD.Instancia.G_Personal_CostoPlanilla_DAO_ddlArea(sucursal, subarea, fechaInicio, fechaFin);
            return ent;
        }

        //EVOLUCIÓN GENERAL MENSUAL
        public DataTable G_Personal_CostoPlanilla_EvolucionGeneralMensual_LO(String mes, String departamento, String area, String subArea, String sucursal)
        { //GIAMPIERE 09/ 09 / 2019
            DataTable ent = PersonalD.Instancia.G_Personal_CostoPlanilla_EvolucionGeneralMensual_Dao(mes, departamento, area, subArea, sucursal);
            return ent;
        }
        public DataTable Personal_CostoPlanilla_EvolucionGeneralMensual_ddlSucursal_LO(String mes, String departamento, String area, String subArea)
        { //GIAMPIERE 09/ 09 / 2019
            DataTable ddlSucursal = PersonalD.Instancia.Personal_CostoPlanilla_EvolucionGeneralMensual_ddlSucursal_Dao(mes, departamento, area, subArea);
            return ddlSucursal;
        }
        public DataTable Personal_CostoPlanilla_EvolucionGeneralMensual_ddlDepartamento_LO(String mes, String area, String subArea, String sucursal)
        { //GIAMPIERE 09/ 09 / 2019
            DataTable ddlDepartamento = PersonalD.Instancia.Personal_CostoPlanilla_EvolucionGeneralMensual_ddlDepartamento_Dao(mes, area, subArea, sucursal);
            return ddlDepartamento;
        }
        public DataTable Personal_CostoPlanilla_EvolucionGeneralMensual_ddlArea_LO(String mes, String departamento, String subArea, String sucursal)
        { //GIAMPIERE 09/ 09 / 2019
            DataTable ddlArea = PersonalD.Instancia.Personal_CostoPlanilla_EvolucionGeneralMensual_ddlArea_Dao(mes, departamento, subArea, sucursal);
            return ddlArea;
        }
        public DataTable Personal_CostoPlanilla_EvolucionGeneralMensual_ddlSubArea_LO(String mes, String departamento, String area, String sucursal)
        { //GIAMPIERE 09/ 09 / 2019
            DataTable ddlSubArea = PersonalD.Instancia.Personal_CostoPlanilla_EvolucionGeneralMensual_ddlSubArea_Dao(mes, departamento, area, sucursal);
            return ddlSubArea;
        }
        public DataTable Personal_CostoPlanilla_EvolucionGeneralMensual_ddlMes_LO(String departamento, String area, String subArea, String sucursal)
        { //GIAMPIERE 09/ 09 / 2019
            DataTable ddlMes = PersonalD.Instancia.Personal_CostoPlanilla_EvolucionGeneralMensual_ddlMes_Dao(departamento, area, subArea, sucursal);
            return ddlMes;
        }
        //EVOLUCIÓN POR ÁREA MENSUAL

        public DataTable G_Personal_CostoPlanilla_EvolucionAreaMensual_LO(String anio, String mes, String area, String subArea, String sucursal)
        {        //GIAMPIERE 09/ 09 / 2019
            DataTable ent = PersonalD.Instancia.G_Personal_CostoPlanilla_EvolucionAreaMensual_Dao(anio, mes, area, subArea, sucursal);
            return ent;
        }
        public DataTable Personal_CostoPlanilla_EvolucionAreaMensual_ddlSucursal_LO(String anio, String mes, String area, String subArea)
        {        //GIAMPIERE 09/ 09 / 2019
            DataTable ddlSucursal = PersonalD.Instancia.Personal_CostoPlanilla_EvolucionAreaMensual_ddlSucursal_Dao(anio, mes, area, subArea);
            return ddlSucursal;
        }
        public DataTable Personal_CostoPlanilla_EvolucionAreaMensual_ddlArea_LO(String anio, String mes, String subArea, String sucursal)
        {        //GIAMPIERE 09/ 09 / 2019
            DataTable ddlArea = PersonalD.Instancia.Personal_CostoPlanilla_EvolucionAreaMensual_ddlArea_Dao(anio, mes, subArea, sucursal);
            return ddlArea;
        }
        public DataTable Personal_CostoPlanilla_EvolucionAreaMensual_ddlSubArea_LO(String anio, String mes, String area, String sucursal)
        {        //GIAMPIERE 09/ 09 / 2019
            DataTable ddlSubArea = PersonalD.Instancia.Personal_CostoPlanilla_EvolucionAreaMensual_ddlSubArea_Dao(anio, mes, area, sucursal);
            return ddlSubArea;
        }
        public DataTable Personal_CostoPlanilla_EvolucionAreaMensual_ddlAnio_LO(String mes, String area, String subArea, String sucursal)
        {        //GIAMPIERE 09/ 09 / 2019
            DataTable ddlAnio = PersonalD.Instancia.Personal_CostoPlanilla_EvolucionAreaMensual_ddlAnio_Dao(mes, area, subArea, sucursal);
            return ddlAnio;
        }
        public DataTable Personal_CostoPlanilla_EvolucionAreaMensual_ddlMes_LO(String anio, String area, String subArea, String sucursal)
        {        //GIAMPIERE 09/ 09 / 2019
            DataTable ddlMes = PersonalD.Instancia.Personal_CostoPlanilla_EvolucionAreaMensual_ddlMes_Dao(anio, area, subArea, sucursal);
            return ddlMes;
        }
        #endregion

        #region Ausentismo y Descanzo Medico
        //Cantidad Area
        //GIAMPIERE 19/ 09 / 2019
        public DataTable G_Personal_Ausentismo_cantidadGeneral_LO()
        {
            DataTable cantidad = PersonalD.Instancia.G_Personal_Ausentismo_cantidadGeneral_Dao();
            return cantidad;
        }
        //GIAMPIERE 19/ 09 / 2019
        public DataTable G_Personal_Descanso_cantidadGeneral_LO()
        {
            DataTable cantidad = PersonalD.Instancia.G_Personal_Descanso_cantidadGeneral_Dao();
            return cantidad;
        }
        public DataTable Personal_AusentismoyDescanso_Tipo_LO()
        { //GIAMPIERE 17/ 08 / 2019
            DataTable cantidad = PersonalD.Instancia.Personal_AusentismoyDescanso_Tipo_Dao();
            return cantidad;
        }
        public DataTable G_Personal_Ausentismo_CantidadArea_LO(String area, String subArea, String sucursal)
        {        //GIAMPIERE 13/ 08 / 2019
            DataTable Ausentismo = PersonalD.Instancia.G_Personal_Ausentismo_CantidadArea_Dao(area, subArea, sucursal);
            return Ausentismo;
        }
        public DataTable G_Personal_DescansoMedico_CantidadArea_LO(String area, String subArea, String sucursal)
        {        //GIAMPIERE 13/ 08 / 2019
            DataTable DescansoMedico = PersonalD.Instancia.G_Personal_DescansoMedico_CantidadArea_Dao(area, subArea, sucursal);
            return DescansoMedico;
        }
        public DataTable Personal_AusentismoYDescanso_CantidadArea_ddlSucursal_LO(String area, String subArea)
        {        //GIAMPIERE 13/ 08 / 2019
            DataTable Sucursal = PersonalD.Instancia.Personal_AusentismoYDescanso_CantidadArea_ddlSucursal_Dao(area, subArea);
            return Sucursal;
        }
        public DataTable Personal_AusentismoYDescanso_CantidadArea_ddlArea_LO(String sucursal, String subArea)
        {        //GIAMPIERE 13/ 08 / 2019
            DataTable Area = PersonalD.Instancia.Personal_AusentismoYDescanso_CantidadArea_ddlArea_Dao(sucursal, subArea);
            return Area;
        }
        public DataTable Personal_AusentismoYDescanso_CantidadArea_ddlSubArea_LO(String sucursal, String area)
        {        //GIAMPIERE 13/ 08 / 2019
            DataTable SubArea = PersonalD.Instancia.Personal_AusentismoYDescanso_CantidadArea_ddlSubArea_Dao(sucursal, area);
            return SubArea;
        }
        //EVOLUCIÓN GENERAL MENSUAL
        public DataTable G_Personal_Ausentismo_EvolucionGeneralMensual_LO(String mes, String departamento, String area, String subArea, String sucursal)
        { //GIAMPIERE 18/ 09 / 2019
            DataTable ent = PersonalD.Instancia.G_Personal_Ausentismo_EvolucionGeneralMensual_Dao(mes, departamento, area, subArea, sucursal);
            return ent;
        }
        public DataTable G_Personal_Descanso_EvolucionGeneralMensual_LO(String mes, String departamento, String area, String subArea, String sucursal)
        { //GIAMPIERE 18/ 09 / 2019
            DataTable ent = PersonalD.Instancia.G_Personal_Descanso_EvolucionGeneralMensual_Dao(mes, departamento, area, subArea, sucursal);
            return ent;
        }
        public DataTable Personal_AusentismoYDescanso_EvolucionGeneralMensual_ddlSucursal_LO(String mes, String departamento, String area, String subArea)
        { //GIAMPIERE 18/ 09 / 2019
            DataTable ddlSucursal = PersonalD.Instancia.Personal_AusentismoYDescanso_EvolucionGeneralMensual_ddlSucursal_Dao(mes, departamento, area, subArea);
            return ddlSucursal;
        }
        public DataTable Personal_AusentismoYDescanso_EvolucionGeneralMensual_ddlDepartamento_LO(String mes, String area, String subArea, String sucursal)
        { //GIAMPIERE 18/ 09 / 2019
            DataTable ddlDepartamento = PersonalD.Instancia.Personal_AusentismoYDescanso_EvolucionGeneralMensual_ddlDepartamento_Dao(mes, area, subArea, sucursal);
            return ddlDepartamento;
        }
        public DataTable Personal_AusentismoYDescanso_EvolucionGeneralMensual_ddlArea_LO(String mes, String departamento, String subArea, String sucursal)
        { //GIAMPIERE 18/ 09 / 2019
            DataTable ddlArea = PersonalD.Instancia.Personal_AusentismoYDescanso_EvolucionGeneralMensual_ddlArea_Dao(mes, departamento, subArea, sucursal);
            return ddlArea;
        }
        public DataTable Personal_AusentismoYDescanso_EvolucionGeneralMensual_ddlSubArea_LO(String mes, String departamento, String area, String sucursal)
        { //GIAMPIERE 18/ 09 / 2019
            DataTable ddlSubArea = PersonalD.Instancia.Personal_AusentismoYDescanso_EvolucionGeneralMensual_ddlSubArea_Dao(mes, departamento, area, sucursal);
            return ddlSubArea;
        }
        public DataTable Personal_AusentismoYDescanso_EvolucionGeneralMensual_ddlMes_LO(String departamento, String area, String subArea, String sucursal)
        { //GIAMPIERE 18/ 09 / 2019
            DataTable ddlMes = PersonalD.Instancia.Personal_AusentismoYDescanso_EvolucionGeneralMensual_ddlMes_Dao(departamento, area, subArea, sucursal);
            return ddlMes;
        }
        //EVOLUCIÓN POR ÁREA MENSUAL
        public DataTable G_Personal_Ausentismo_EvolucionAreaMensual_LO(String anio, String mes, String area, String subArea, String sucursal)
        {        //GIAMPIERE 18/ 09 / 2019
            DataTable ent = PersonalD.Instancia.G_Personal_Ausentismo_EvolucionAreaMensual_Dao(anio, mes, area, subArea, sucursal);
            return ent;
        }   
        public DataTable G_Personal_Descanso_EvolucionAreaMensual_LO(String anio, String mes, String area, String subArea, String sucursal)
        {        //GIAMPIERE 18/ 09 / 2019
            DataTable ent = PersonalD.Instancia.G_Personal_Descanso_EvolucionAreaMensual_Dao(anio, mes, area, subArea, sucursal);
            return ent;
        }
        public DataTable Personal_AusentismoYDescanso_EvolucionAreaMensual_ddlSucursal_LO(String anio, String mes, String area, String subArea)
        {        //GIAMPIERE 18/ 09 / 2019
            DataTable ddlSucursal = PersonalD.Instancia.Personal_AusentismoYDescanso_EvolucionAreaMensual_ddlSucursal_Dao(anio, mes, area, subArea);
            return ddlSucursal;
        }
        public DataTable Personal_AusentismoYDescanso_EvolucionAreaMensual_ddlArea_LO(String anio, String mes, String subArea, String sucursal)
        {        //GIAMPIERE 18/ 09 / 2019
            DataTable ddlArea = PersonalD.Instancia.Personal_AusentismoYDescanso_EvolucionAreaMensual_ddlArea_Dao(anio, mes, subArea, sucursal);
            return ddlArea;
        }
        public DataTable Personal_AusentismoYDescanso_EvolucionAreaMensual_ddlSubArea_LO(String anio, String mes, String area, String sucursal)
        {        //GIAMPIERE 18/ 09 / 2019
            DataTable ddlSubArea = PersonalD.Instancia.Personal_AusentismoYDescanso_EvolucionAreaMensual_ddlSubArea_Dao(anio, mes, area, sucursal);
            return ddlSubArea;
        }
        public DataTable Personal_AusentismoYDescanso_EvolucionAreaMensual_ddlAnio_LO(String mes, String area, String subArea, String sucursal)
        {        //GIAMPIERE 18/ 09 / 2019
            DataTable ddlAnio = PersonalD.Instancia.Personal_AusentismoYDescanso_EvolucionAreaMensual_ddlAnio_Dao(mes, area, subArea, sucursal);
            return ddlAnio;
        }
        public DataTable Personal_AusentismoYDescanso_EvolucionAreaMensual_ddlMes_LO(String anio, String area, String subArea, String sucursal)
        {        //GIAMPIERE 18/ 09 / 2019
            DataTable ddlMes = PersonalD.Instancia.Personal_AusentismoYDescanso_EvolucionAreaMensual_ddlMes_Dao(anio, area, subArea, sucursal);
            return ddlMes;
        }
        #endregion

        #region Hora Extra
        ////CANTIDAD GENERAL
        public DataTable G_Personal_HoraExtra_cantidadGeneral_LO()
        {
            DataTable cantidad = PersonalD.Instancia.G_Personal_HoraExtra_cantidadGeneral_Dao();
            return cantidad;
        }
        //GIAMPIERE 12/ 08 / 2019
        //CANTIDAD POR DEPARTAMENTO
        public DataTable G_Personal_HoraExtras_CantidadArea_LO(String area, String subArea, String sucursal)
        {        //GIAMPIERE 12/ 08 / 2019
            DataTable ent = PersonalD.Instancia.G_Personal_HoraExtras_CantidadArea_Dao(area, subArea, sucursal);
            return ent;
        }
        public DataTable Personal_HoraExtras_CantidadArea_ddlSucursal_LO(String area, String subArea)
        {        //GIAMPIERE 12/ 08 / 2019
            DataTable Sucursal = PersonalD.Instancia.Personal_HoraExtras_CantidadArea_ddlSucursal_Dao(area, subArea);
            return Sucursal;
        }
        public DataTable Personal_HoraExtras_CantidadArea_ddlArea_LO(String sucursal, String subArea)
        {        //GIAMPIERE 12/ 08 / 2019
            DataTable Area = PersonalD.Instancia.Personal_HoraExtras_CantidadArea_ddlArea_Dao(sucursal, subArea);
            return Area;
        }
        public DataTable Personal_HoraExtras_CantidadArea_ddlSubArea_LO(String sucursal, String area)
        {        //GIAMPIERE 12/ 08 / 2019
            DataTable SubArea = PersonalD.Instancia.Personal_HoraExtras_CantidadArea_ddlSubArea_Dao(sucursal, area);
            return SubArea;
        }

        //EVOLUCIÓN GENERAL MENSUAL
        public DataTable G_Personal_HoraExtras_EvolucionGeneralMensual_LO(String mes, String departamento, String area, String subArea, String sucursal)
        { //GIAMPIERE 12/ 08 / 2019
            DataTable ent = PersonalD.Instancia.G_Personal_HoraExtras_EvolucionGeneralMensual_Dao(mes, departamento, area, subArea, sucursal);
            return ent;
        }
        public DataTable Personal_HoraExtras_EvolucionGeneralMensual_ddlSucursal_LO(String mes, String departamento, String area, String subArea)
        { //GIAMPIERE 12/ 08 / 2019
            DataTable ddlSucursal = PersonalD.Instancia.Personal_HoraExtras_EvolucionGeneralMensual_ddlSucursal_Dao(mes, departamento, area, subArea);
            return ddlSucursal;
        }
        public DataTable Personal_HoraExtras_EvolucionGeneralMensual_ddlDepartamento_LO(String mes, String area, String subArea, String sucursal)
        { //GIAMPIERE 12/ 08 / 2019
            DataTable ddlDepartamento = PersonalD.Instancia.Personal_HoraExtras_EvolucionGeneralMensual_ddlDepartamento_Dao(mes, area, subArea, sucursal);
            return ddlDepartamento;
        }
        public DataTable Personal_HoraExtras_EvolucionGeneralMensual_ddlArea_LO(String mes, String departamento, String subArea, String sucursal)
        { //GIAMPIERE 12/ 08 / 2019
            DataTable ddlArea = PersonalD.Instancia.Personal_HoraExtras_EvolucionGeneralMensual_ddlArea_Dao(mes, departamento, subArea, sucursal);
            return ddlArea;
        }
        public DataTable Personal_HoraExtras_EvolucionGeneralMensual_ddlSubArea_LO(String mes, String departamento, String area, String sucursal)
        { //GIAMPIERE 12/ 08 / 2019
            DataTable ddlSubArea = PersonalD.Instancia.Personal_HoraExtras_EvolucionGeneralMensual_ddlSubArea_Dao(mes, departamento, area, sucursal);
            return ddlSubArea;
        }
        public DataTable Personal_HoraExtras_EvolucionGeneralMensual_ddlMes_LO(String departamento, String area, String subArea, String sucursal)
        { //GIAMPIERE 12/ 08 / 2019
            DataTable ddlMes = PersonalD.Instancia.Personal_HoraExtras_EvolucionGeneralMensual_ddlMes_Dao(departamento, area, subArea, sucursal);
            return ddlMes;
        }
        //EVOLUCIÓN POR ÁREA MENSUAL

        public DataTable G_Personal_HoraExtras_EvolucionAreaMensual_LO(String anio, String mes, String area, String subArea, String sucursal)
        {        //GIAMPIERE 12/ 08 / 2019
            DataTable ent = PersonalD.Instancia.G_Personal_HoraExtras_EvolucionAreaMensual_Dao(anio, mes, area, subArea, sucursal);
            return ent;
        }
        public DataTable Personal_HoraExtras_EvolucionAreaMensual_ddlSucursal_LO(String anio, String mes, String area, String subArea)
        {        //GIAMPIERE 12/ 08 / 2019
            DataTable ddlSucursal = PersonalD.Instancia.Personal_HoraExtras_EvolucionAreaMensual_ddlSucursal_Dao(anio, mes, area, subArea);
            return ddlSucursal;
        }
        public DataTable Personal_HoraExtras_EvolucionAreaMensual_ddlArea_LO(String anio, String mes, String subArea, String sucursal)
        {        //GIAMPIERE 12/ 08 / 2019
            DataTable ddlArea = PersonalD.Instancia.Personal_HoraExtras_EvolucionAreaMensual_ddlArea_Dao(anio, mes, subArea, sucursal);
            return ddlArea;
        }
        public DataTable Personal_HoraExtras_EvolucionAreaMensual_ddlSubArea_LO(String anio, String mes, String area, String sucursal)
        {        //GIAMPIERE 12/ 08 / 2019
            DataTable ddlSubArea = PersonalD.Instancia.Personal_HoraExtras_EvolucionAreaMensual_ddlSubArea_Dao(anio, mes, area, sucursal);
            return ddlSubArea;
        }
        public DataTable Personal_HoraExtras_EvolucionAreaMensual_ddlAnio_LO(String mes, String area, String subArea, String sucursal)
        {        //GIAMPIERE 12/ 08 / 2019
            DataTable ddlAnio = PersonalD.Instancia.Personal_HoraExtras_EvolucionAreaMensual_ddlAnio_Dao(mes, area, subArea, sucursal);
            return ddlAnio;
        }
        public DataTable Personal_HoraExtras_EvolucionAreaMensual_ddlMes_LO(String anio, String area, String subArea, String sucursal)
        {        //GIAMPIERE 12/ 08 / 2019
            DataTable ddlMes = PersonalD.Instancia.Personal_HoraExtras_EvolucionAreaMensual_ddlMes_Dao(anio, area, subArea, sucursal);
            return ddlMes;
        }
        #endregion

        //? ###################################################################### SUSPENSIONES Y AMONESTACIONES ##########################
        #region PERSONAL_SUSPENSIONES_JORGEBALTODANO
       

        //CANTIDAD POR AREA
        
        //JORGE BALTODANO 19/ 09 / 2019
        public DataTable Grafica_Personal_Suspensiones_CantidadSuspensiones_LO()
        {
            DataTable cantidadSuspensiones = PersonalD.Instancia.Grafica_Personal_Suspensiones_CantidadSuspensiones_DAO();
            return cantidadSuspensiones;
        }
        //JORGE BALTODANO 19/ 09 / 2019
        public DataTable Grafica_Personal_Amonestaciones_cantidadAmonestaciones_LO()
        {
            DataTable cantidad = PersonalD.Instancia.Grafica_Personal_Amonestaciones_CantidadAmonestaciones_DAO();
            return cantidad;
        }
        public DataTable Listar_Personal_SuspensionesyAmonestaciones_Tipo_LO()
        { //JORGE BALTODANO 17/ 08 / 2019
            DataTable listarSuspensionesyAmonestaciones = PersonalD.Instancia.Listar_Personal_SuspensionesyAmonestaciones_Tipo_DAO();
            return listarSuspensionesyAmonestaciones;
        }




















        // Listar Sucursal
        public DataTable Personal_Suspensiones_ListarSucursal(String area, String subArea)
        {
            DataTable dt_sucursal = PersonalD.Instancia.Personal_Suspensiones_listarSucursal(area, subArea);
            return dt_sucursal;
        }

        //Listar Areas
        public DataTable Personal_Suspensiones_ListarArea( String sucursal, String subArea)
        {
            DataTable dt_Area = PersonalD.Instancia.Personal_Suspensiones_listarArea( sucursal, subArea);
            return dt_Area;
        }

        //Listar Subarea
        public DataTable Personal_Suspensiones_ListarSubArea( String area, String sucursal)
        {
            DataTable dt_SubArea = PersonalD.Instancia.Personal_Suspensiones_listarSubArea(area, sucursal);
            return dt_SubArea;
        }
        //public DataTable Personal_Suspensiones_ListarAño( String area, String subArea, String sucursal)
        //{
        //    DataTable dt_Año = PersonalD.Instancia.Personal_Suspensiones_listarAño( area, subArea, sucursal);
        //    return dt_Año;
        //}
        //public DataTable Personal_Suspensiones_ListarSTrimestre(String area, String subArea, String sucursal)
        //{
        //    DataTable dt_Trimestre = PersonalD.Instancia.Personal_Suspensiones_listarTrimestre( area, subArea, sucursal);
        //    return dt_Trimestre;
        //}
        
        //GRafica
        public DataTable Grafica_Persona_Suspensiones_Cantidad_Suspensiones_Departamento(String area, String subArea, String sucursal)
        {
            DataTable dt_Grafica_CSP = PersonalD.Instancia.Grafica_Personal_Suspensiones_Cantidad_Suspensiones_Departamento(area, subArea, sucursal);
            return dt_Grafica_CSP;
        }

        /* ####### FUNCION PARA GRAFICAR EVOLUCION HISTORICA GENERAL MENSUAL   ##### */

        //Listar Sucursal
        public DataTable Personal_Suspensiones_ListarSucursal_EHGM_LO(String Anio, String Area, String SubArea, String Departamento, String Mes){
            DataTable dt_sucursal_EHGM=PersonalD.Instancia.Personal_Suspensiones_ListarSucursal_EHGM_DAO( Anio,  Area,  SubArea, Departamento,  Mes);
            return dt_sucursal_EHGM;
        }

          //Listar Departamento
        public DataTable Personal_Suspensiones_ListarDepartamento_EHGM_LO( String Anio, String Area, String SubArea, String Sucursal, String Mes){
            DataTable dt_Departamento_EHGM=PersonalD.Instancia.Personal_Suspensiones_ListarDepartamento_EHGM_DAO( Anio,  Area,  SubArea, Sucursal,  Mes);
            return dt_Departamento_EHGM;
        }

           //Listar Area
        public DataTable Personal_Suspensiones_ListarArea_EHGM_LO( String Anio, String SubArea, String Sucursal, String Departamento,String Mes){
            DataTable dt_Area_EHGM=PersonalD.Instancia.Personal_Suspensiones_ListarArea_EHGM_DAO( Anio,  SubArea,  Sucursal, Departamento, Mes);
            return dt_Area_EHGM;
        }

           //Listar SubArea
        public DataTable Personal_Suspensiones_ListarSubArea_EHGM_LO(String Departamento, String Area, String Sucursal, String Anio, String Mes){
            DataTable dt_SubArea_EHGM=PersonalD.Instancia.Personal_Suspensiones_ListarSubArea_EHGM_DAO(  Departamento, Area, Sucursal,  Anio,  Mes);
            return dt_SubArea_EHGM;
        }

             //Listar Año
        public DataTable Personal_Suspensiones_ListarAño_EHGM_LO(string Departamento, string SubArea, string Area, string Sucursal, string Mes){
            DataTable dt_SubArea_EHGM=PersonalD.Instancia.Personal_Suspensiones_ListarAño_EHGM_DAO(Departamento, SubArea,  Area,  Sucursal,  Mes);
            return dt_SubArea_EHGM;
        }

              //Listar Año
        public DataTable Personal_Suspensiones_ListarMes_EHGM_LO(String Departamento, string SubArea, string Area, string Sucursal, string Año){
            DataTable dt_SubArea_EHGM=PersonalD.Instancia.Personal_Suspensiones_ListarMes_EHGM_DAO( Departamento,  SubArea,  Area,  Sucursal, Año);
            return dt_SubArea_EHGM;
        }

        //GRAFICA 
    






        #endregion

    }
}
