﻿using DATO;
using System;
using System.Data;

namespace LOGICA
{
    public class MantenimientoL
    {
        String tabla_matenimiento = "DTM_Mantenimiento_Disponibilidad_Flotas";

        #region singleton
        private static readonly MantenimientoL _instancia = new MantenimientoL();
        public static MantenimientoL Instancia
        {
            get { return MantenimientoL._instancia; }
        }
        #endregion singleton

        #region metodosMantenimiento

        //Disponibilidad de Flotas:

        public DataTable G_Mantenimiento_CantidadGeneral_Modelo_LO(String modelo, String marca, String anio, String mes)
        {
            DataTable ent = MantenimientoD.Instancia.G_Mantenimiento_DisponibilidadFlota_Modelo_DAO(modelo, marca, anio, mes);
            return ent;
        }

        public DataTable Listar_Disponibilidad_Flotas(String modelo, String marca, String anio, String mes)
        {
            DataTable ent = MantenimientoD.Instancia.Listar_Disponibilidad_Flotas(modelo, marca, anio, mes);
            return ent;
        }

        public DataTable G_Mantenimiento_DisponibilidadFoltas_LO_ListarMarca(String filtroModelo, String anio, String mes)
        {
            DataTable filtro_marca = MantenimientoD.Instancia.G_Mantenimiento_DisponibilidadFlota_DAO_ListarMarca(filtroModelo, anio, mes, tabla_matenimiento);
            return filtro_marca;
        }

        public DataTable G_Mantenimiento_DisponibilidadFoltas_LO_ListarModelo(String filtroMarca, String filtroAnio, String filtroMes)
        {
            DataTable filtro_modelo = MantenimientoD.Instancia.G_Mantenimiento_DisponibilidadFlota_DAO_ListarModelo(filtroMarca, filtroAnio, filtroMes, tabla_matenimiento);
            return filtro_modelo;
        }

        public DataTable G_Mantenimiento_DisponibilidadFoltas_LO_ListarAnio(String marca, String filtroModelo, String filtroAnio, String filtroMes)
        {
            DataTable filtro_modelo = MantenimientoD.Instancia.G_Mantenimiento_DisponibilidadFoltas_DAO_ListarAnio(marca, filtroModelo, filtroMes, tabla_matenimiento);
            return filtro_modelo;
        }

        public DataTable G_Mantenimiento_DisponibilidadFoltas_LO_ListarMes(String marca, String filtroModelo, String filtroAnio)
        {
            DataTable filtro = MantenimientoD.Instancia.G_Mantenimiento_DisponibilidadFoltas_DAO_ListarMes(marca, filtroModelo, filtroAnio, tabla_matenimiento);
            return filtro;
        }

        public DataTable G_Mantenimiento_DisponibilidadFoltas_LO_ListarTrimestre(String filtroModelo, String fechaInicio, String fechaFin, String marca)
        {
            DataTable filtro_modelo = MantenimientoD.Instancia.G_DistribucionFlota_DAO_General(filtroModelo, fechaInicio, fechaFin, marca);
            return filtro_modelo;
        }

        ////kpi: Tiempo promedio de fallas - Histórico
        public DataTable Listar_Fallas_Sistema(String marca, String tipoMantenimiento, String fechaInicio, String fechaFin)
        {
            DataTable ent = MantenimientoD.Instancia.Listar_Falla_Sistema(marca, tipoMantenimiento, fechaInicio, fechaFin);
            return ent;
        }

        public DataTable Listar_Fallas__Promedio_Sistema(String marca, String tipoMantenimiento, String fechaInicio, String fechaFin)
        {
            DataTable ent = MantenimientoD.Instancia.Listar_Falla_Promedio_Sistema(marca, tipoMantenimiento, fechaInicio, fechaFin);
            return ent;
        }

        public DataTable Mantenimiento_FallasSistema_SistemaImplicado_TipoMantenimiento_LO(String marca, String anio, String mes)
        {
            DataTable TipoMantenimiento = MantenimientoD.Instancia.Mantenimiento_FallasSistema_SistemaImplicado_TipoMantenimiento_Dao(marca, anio, mes);
            return TipoMantenimiento;
        }
        public DataTable Mantenimiento_FallasSistema_SistemaImplicado_Marca_LO(String tipoMantenimiento, String anio, String mes)
        {
            DataTable Marca = MantenimientoD.Instancia.Mantenimiento_FallasSistema_SistemaImplicado_Marca_Dao(tipoMantenimiento, anio, mes);
            return Marca;
        }
        public DataTable Mantenimiento_FallasSistema_SistemaImplicado_Anio_LO(String tipoMantenimiento, String marca, String mes)
        {
            DataTable Anio = MantenimientoD.Instancia.Mantenimiento_FallasSistema_SistemaImplicado_Anio_Dao(tipoMantenimiento, marca, mes);
            return Anio;
        }
        public DataTable Mantenimiento_FallasSistema_SistemaImplicado_Mes_LO(String tipoMantenimiento, String marca, String anio)
        {
            DataTable Mes = MantenimientoD.Instancia.Mantenimiento_FallasSistema_SistemaImplicado_Mes_Dao(tipoMantenimiento, marca, anio);
            return Mes;
        }
        public DataTable Mantenimiento_FallasSistema_PorcentajeSistemaImplicado_TipoMantenimiento_LO(String marca, String anio, String mes)
        {
            DataTable TipoMantenimiento = MantenimientoD.Instancia.Mantenimiento_FallasSistema_PorcentajeSistemaImplicado_TipoMantenimiento_Dao(marca, anio, mes);
            return TipoMantenimiento;
        }

        public DataTable Mantenimiento_FallasSistema_PorcentajeSistemaImplicado_Marca_LO(String tipoMantenimiento, String anio, String mes)
        {
            DataTable Marca = MantenimientoD.Instancia.Mantenimiento_FallasSistema_PorcentajeSistemaImplicado_Marca_Dao(tipoMantenimiento, anio, mes);
            return Marca;
        }
        public DataTable Mantenimiento_FallasSistema_PorcentajeSistemaImplicado_Anio_LO(String tipoMantenimiento, String marca, String mes)
        {
            DataTable Marca = MantenimientoD.Instancia.Mantenimiento_FallasSistema_PorcentajeSistemaImplicado_Anio_Dao(tipoMantenimiento, marca, mes);
            return Marca;
        }
        public DataTable Mantenimiento_FallasSistema_PorcentajeSistemaImplicado_Mes_LO(String tipoMantenimiento, String marca, String anio)
        {
            DataTable Marca = MantenimientoD.Instancia.Mantenimiento_FallasSistema_PorcentajeSistemaImplicado_Mes_Dao(tipoMantenimiento, marca, anio);
            return Marca;
        }
        public DataTable Filtro_Fechas_TPF()
        {
            DataTable ent = MantenimientoD.Instancia.Listar_Fecha_TPFS();
            return ent;
        }

        public DataTable G1_Tiempo_Fallas_Fallas(String anio, String mes)
        {
            DataTable ent = MantenimientoD.Instancia.G1_Tiempo_Fallas_Fallas(anio, mes);
            return ent;
        }
        public DataTable Mantenimiemto_Tiempo_Fallas_Fallas_anio_LO( String mes)
        {
            DataTable Mes = MantenimientoD.Instancia.Mantenimiemto_Tiempo_Fallas_Fallas_anio_Dao( mes);
            return Mes;
        }
        public DataTable Mantenimiemto_Tiempo_Fallas_Fallas_Mes_Lo(String anio)
        {
            DataTable Anio = MantenimientoD.Instancia.Mantenimiemto_Tiempo_Fallas_Fallas_Mes_Dao(anio);
            return Anio;
        }

        //kpi: Tiempo promedio de fallas - Anual

        public DataTable Filtro_Anio_TMF()
        {
            DataTable ent = MantenimientoD.Instancia.Listar_Año_TPFS();
            return ent;
        }

        public DataTable Filtro_Marca_TMFA()
        {
            DataTable ent = MantenimientoD.Instancia.Listar_Marca_TPFS();
            return ent;
        }

        public DataTable Tiempo_Fallas_Fallas_Anual(String filtroMarca, String anio)
        {
            DataTable ent = MantenimientoD.Instancia.Tiempo_Fallas_Fallas_Anual(filtroMarca, anio);
            return ent;
        }

        //kpi: Total de Productividad
        public DataTable Grafica_Productividad_KPI(String anio)
        {
            DataTable ent = MantenimientoD.Instancia.Productividad_KPI(anio);
            return ent;
        }

        public DataTable Listar_Anio_Productividad()
        {
            DataTable ent = MantenimientoD.Instancia.Listar_Anio_Productividad();
            return ent;
        }


        //KPI COSTOS EXTERNOS

 //KPI COSTOS EXTERNOS


        // TODO: GRAFICA KPI COSTOS - MONTO TOTAL - LINECHART
         public DataTable Grafica_Mantenimiento_CostosPersonalRepuestos_LCH_LO(String Anio, String Mes)
        {

            DataTable Ddt = MantenimientoD.Instancia.Grafica_Mantenimiento_CostosPersonalRepuestos_LCH_DAO( Anio, Mes);
            return Ddt;

        }
        public DataTable Mantenimiento_CostoPersonalRepuestos_ListarMes_LO(String Anio)
        {
            DataTable Ddt_Mes = MantenimientoD.Instancia.Mantenimiento_CostoPersonalRepuestos_ListarMes_DAO(Anio);
            return Ddt_Mes;

        }
        public DataTable Mantenimiento_CostoPersonalRepuestos_ListarAño_LO(String Mes)
        {
            DataTable dt_Año = MantenimientoD.Instancia.Mantenimiento_CostoPersonalRepuestos_ListarAño_DAO(Mes);
            return dt_Año;
        }

        // TODO: GRAFICA KPI COSTOS - MONTO TOTAL - GROUPEDCHART

        public DataTable Grafica_Mantenimiento_Costos_TotalMes_GCH_LO(String anio2,String mes2)
        {

           DataTable Ddt = MantenimientoD.Instancia.Grafica_Mantenimiento_CostosPersonalRepuestos_GCH_DAO(anio2,mes2);
           return Ddt;

        }

        public DataTable Mantenimiento_CostoPersonalRepuestos_ListarAño_GCH_LO(String mes2)
        {
           DataTable Ddt = MantenimientoD.Instancia.Mantenimiento_CostoPersonalRepuestos_ListarAño_GCH_DAO(mes2);
           return Ddt;

        }

        public DataTable Mantenimiento_CostoPersonalRepuestos_ListarMes_GCH_LO(String anio2)
        {
           DataTable dt_trimestre = MantenimientoD.Instancia.Mantenimiento_CostoPersonalRepuestos_ListarMes_GCH_DAO(anio2);
           return dt_trimestre;
        }

        #endregion
    }
}
