﻿using ENTIDAD.Base;
using System;
using System.Data;
using System.Data.SqlClient;

namespace DATO
{
    public class MantenimientoD
    {

        ConsultaSQL consulta_sql = new ConsultaSQL();

        #region singleton
        private static readonly MantenimientoD _instancia = new MantenimientoD();
        public static MantenimientoD Instancia
        {
            get { return MantenimientoD._instancia; }
        }
        #endregion

        #region Metodos Mantenimiento

        //Disponibilidad de Flotas:

        public DataTable G_Mantenimiento_DisponibilidadFlota_Modelo_DAO(String modelo, String marca, String anio, String mes)
        {
            DataTable dt = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(consulta_sql.G_Mantenimiento_CantidadGeneral_Modelo_SQL(modelo, marca, anio, mes), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt = new DataTable();
                    dt.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt;
        }

        public DataTable Listar_Disponibilidad_Flotas(String modelo, String marca, String anio, String mes)
        {
            DataTable dt = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(consulta_sql.Disponibilidad_Flotas(modelo, marca, anio, mes), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt = new DataTable();
                    dt.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt;
        }

        public DataTable G_Mantenimiento_DisponibilidadFoltas_DAO_ListarAnio(String filtroMarca, String filtroModelo, String filtroMes, String tabla)
        {
            DataTable dt_filtro_anio = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(consulta_sql.F_DisponibilidadFlota_General_ListarAnio_SQL(filtroMarca, filtroModelo, filtroMes, tabla), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_filtro_anio = new DataTable();
                    dt_filtro_anio.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_filtro_anio = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_filtro_anio;
        }

        public DataTable G_Mantenimiento_DisponibilidadFoltas_DAO_ListarMes(String filtroMarca, String filtroModelo, String filtroAnio, String tabla)
        {
            DataTable dt_filtro = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(consulta_sql.F_DisponibilidadFlota_General_ListarMes_SQL(filtroMarca, filtroModelo, filtroAnio, tabla), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_filtro = new DataTable();
                    dt_filtro.Load(dr);
                }
            }
            catch (Exception e)
            {
                dt_filtro = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_filtro;
        }

        public DataTable G_Mantenimiento_DisponibilidadFlota_DAO_ListarMarca(String filtroModelo, String anio, String mes, String tabla)
        {
            DataTable dt_listar_marca = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(consulta_sql.F_Mantenimiento_General_ListarMarca_SQL(filtroModelo, anio, mes, tabla), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_listar_marca = new DataTable();
                    dt_listar_marca.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_listar_marca = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_listar_marca;
        }

        public DataTable G_Mantenimiento_DisponibilidadFlota_DAO_ListarModelo(String filtroMarca, String filtroAnio, String filtroMes, String tabla)
        {
            DataTable dt_listar_modelo = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(consulta_sql.F_Mantenimiento_General_ListarModelo_SQL(filtroMarca, filtroAnio, filtroMes, tabla), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_listar_modelo = new DataTable();
                    dt_listar_modelo.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_listar_modelo = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_listar_modelo;
        }

        public DataTable G_DistribucionFlota_DAO_General(String filtroModelo, String fechaInicio, String fechaFin, String marca)
        {
            DataTable dt_filtro_trimestre = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(consulta_sql.F_DisponibilidadFlota_General_ListarTrimestre_SQL(filtroModelo, fechaInicio, fechaFin, marca), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_filtro_trimestre = new DataTable();
                    dt_filtro_trimestre.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_filtro_trimestre = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_filtro_trimestre;
        }



        //Fallas del Sistema:
        public DataTable Listar_Falla_Sistema(String marca, String tipoMantenimiento, String fechaInicio, String fechaFin)
        {
            DataTable dt = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(consulta_sql.Cantidad_Fallas(marca, tipoMantenimiento, fechaInicio, fechaFin), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt = new DataTable();
                    dt.Load(dr);

                }

            }
            catch (Exception ex)
            {
                dt = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt;
        }

        public DataTable Listar_Falla_Promedio_Sistema(String marca, String tipoMantenimiento, String fechaInicio, String fechaFin)
        {
            DataTable dt = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(consulta_sql.Promedio_Fallas(marca, tipoMantenimiento, fechaInicio, fechaFin), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt = new DataTable();
                    dt.Load(dr);
                }

            }
            catch (Exception ex)
            {
                dt = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt;
        }

        public DataTable Mantenimiento_FallasSistema_SistemaImplicado_TipoMantenimiento_Dao(String marca, String anio, String mes)
        {
            DataTable dt_listar_TipoMantenimiento = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(consulta_sql.Mantenimiento_FallasSistema_SistemaImplicado_TipoMantenimiento_SQL(marca, anio, mes), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_listar_TipoMantenimiento = new DataTable();
                    dt_listar_TipoMantenimiento.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_listar_TipoMantenimiento = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_listar_TipoMantenimiento;
        }


        public DataTable Mantenimiento_FallasSistema_SistemaImplicado_Marca_Dao(String tipoMantenimiento, String anio, String mes)
        {
            DataTable dt_listar_Marca = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(consulta_sql.Mantenimiento_FallasSistema_SistemaImplicado_Marca_SQL(tipoMantenimiento, anio, mes), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_listar_Marca = new DataTable();
                    dt_listar_Marca.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_listar_Marca = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_listar_Marca;
        }
        public DataTable Mantenimiento_FallasSistema_SistemaImplicado_Anio_Dao(String tipoMantenimiento, String marca, String mes)
        {
            DataTable dt_listar_Anio = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(consulta_sql.Mantenimiento_FallasSistema_SistemaImplicado_Anio_SQL(tipoMantenimiento, marca, mes), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_listar_Anio = new DataTable();
                    dt_listar_Anio.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_listar_Anio = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_listar_Anio;
        }
        public DataTable Mantenimiento_FallasSistema_SistemaImplicado_Mes_Dao(String tipoMantenimiento, String marca, String anio)
        {
            DataTable dt_listar_Mes = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(consulta_sql.Mantenimiento_FallasSistema_SistemaImplicado_Mes_SQL(tipoMantenimiento, marca, anio), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_listar_Mes = new DataTable();
                    dt_listar_Mes.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_listar_Mes = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_listar_Mes;
        }
        public DataTable Mantenimiento_FallasSistema_PorcentajeSistemaImplicado_TipoMantenimiento_Dao(String marca, String anio, String mes)
        {
            DataTable dt_listar_TipoMantenimiento = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(consulta_sql.Mantenimiento_FallasSistema_PorcentajeSistemaImplicado_TipoMantenimiento_SQL(marca, anio, mes), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_listar_TipoMantenimiento = new DataTable();
                    dt_listar_TipoMantenimiento.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_listar_TipoMantenimiento = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_listar_TipoMantenimiento;
        }
        public DataTable Mantenimiento_FallasSistema_PorcentajeSistemaImplicado_Marca_Dao(String tipoMantenimiento, String anio, String mes)
        {
            DataTable dt_listar_Marca = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(consulta_sql.Mantenimiento_FallasSistema_PorcentajeSistemaImplicado_Marca_SQL(tipoMantenimiento, anio, mes), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_listar_Marca = new DataTable();
                    dt_listar_Marca.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_listar_Marca = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_listar_Marca;
        }
        public DataTable Mantenimiento_FallasSistema_PorcentajeSistemaImplicado_Anio_Dao(String tipoMantenimiento, String marca, String mes)
        {
            DataTable dt_listar_Marca = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(consulta_sql.Mantenimiento_FallasSistema_PorcentajeSistemaImplicado_Anio_SQL(tipoMantenimiento, marca, mes), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_listar_Marca = new DataTable();
                    dt_listar_Marca.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_listar_Marca = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_listar_Marca;
        }
        public DataTable Mantenimiento_FallasSistema_PorcentajeSistemaImplicado_Mes_Dao(String tipoMantenimiento, String marca, String anio)
        {
            DataTable dt_listar_Marca = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(consulta_sql.Mantenimiento_FallasSistema_PorcentajeSistemaImplicado_Mes_SQL(tipoMantenimiento, marca, anio), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_listar_Marca = new DataTable();
                    dt_listar_Marca.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_listar_Marca = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_listar_Marca;
        }
        //KPI: Tiempo promedio Fallas del Sistema - HISTORICO 
        public DataTable Listar_Fecha_TPFS()
        {
            DataTable dt_filtro_fecha = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(consulta_sql.Filtro_Fecha_TMF, objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_filtro_fecha = new DataTable();
                    dt_filtro_fecha.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_filtro_fecha = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_filtro_fecha;
        }



        public DataTable G1_Tiempo_Fallas_Fallas(String anio, String mes)
        {
            DataTable dt = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(consulta_sql.G1_Tiempo_Fallas_Fallas(anio, mes), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt = new DataTable();
                    dt.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt;
        }
        public DataTable Mantenimiemto_Tiempo_Fallas_Fallas_anio_Dao(String mes)
        {
            DataTable dt_mes = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(consulta_sql.Mantenimiemto_Tiempo_Fallas_Fallas_anio_SQL( mes), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_mes = new DataTable();
                    dt_mes.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_mes = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_mes;
        }
        public DataTable Mantenimiemto_Tiempo_Fallas_Fallas_Mes_Dao(String anio)
        {
            DataTable dt_anio = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(consulta_sql.Mantenimiemto_Tiempo_Fallas_Fallas_Mes_SQL(anio), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_anio = new DataTable();
                    dt_anio.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_anio = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_anio;
        }
        //Tiempo promedio Fallas del Sistema - ANUAL 

        public DataTable Listar_Año_TPFS()
        {
            DataTable dt_filtro_anio = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(consulta_sql.Filtro_Anio_TMF_Anual, objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_filtro_anio = new DataTable();
                    dt_filtro_anio.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_filtro_anio = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_filtro_anio;
        }

        public DataTable Listar_Marca_TPFS()
        {
            DataTable dt_filtro_marca = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(consulta_sql.Filtro_Marca_TMF_Anual, objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_filtro_marca = new DataTable();
                    dt_filtro_marca.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_filtro_marca = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_filtro_marca;
        }

        public DataTable Tiempo_Fallas_Fallas_Anual(String filtroMarca, String anio)
        {
            DataTable dt = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(consulta_sql.Tiempo_Fallas_Fallas_Anual(filtroMarca, anio), objconnection);
                //cmd = new SqlCommand(consulta_sql.Disponibilidad_Flotas(), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt = new DataTable();
                    dt.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt;
        }

        //KPI: PRODUCTIVIDAD

        public DataTable Productividad_KPI(String anio)
        {
            DataTable dt = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(consulta_sql.Productividad_Total_KPI(anio), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt = new DataTable();
                    dt.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt;
        }

        public DataTable Listar_Anio_Productividad()
        {
            DataTable dt_filtro_anio = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(consulta_sql.filtro_anio_productividad_total, objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_filtro_anio = new DataTable();
                    dt_filtro_anio.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_filtro_anio = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_filtro_anio;
        }

        #endregion

      #region KPI_MANTENIMIENTO_COSTOS_REPUESTOS_DAO

        //TODO: Metodo para graficar  KPI COSTOS - LINECHART

        public DataTable Grafica_Mantenimiento_CostosPersonalRepuestos_LCH_DAO(String Anio, String Mes)
        {
            DataTable dt = null;
            SqlCommand cmd = null;
            SqlConnection con = null;
            SqlDataReader dr = null;

            try
            {
                con = Conexion.Conectar();
                cmd = new SqlCommand(consulta_sql.Grafica_Mantenimiento_CostosPersonalRepuestos_LCH_SQL(Anio, Mes), con);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt = new DataTable();
                    dt.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt = null;
            }
            finally
            {
                con.Close();
            }
            return dt;
        }

        // TODO: Filtro de los Meses 
        public DataTable Mantenimiento_CostoPersonalRepuestos_ListarMes_DAO(String Anio)
        {
            DataTable dt_filtro_Mes = null;
            SqlCommand cmd = null;
            SqlConnection con = null;
            SqlDataReader dr = null;
            try
            {
                con = Conexion.Conectar();
                cmd = new SqlCommand(consulta_sql.Mantenimiento_CostoPersonalRepuestos_ListarMes_SQL(Anio), con);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_filtro_Mes = new DataTable();
                    dt_filtro_Mes.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_filtro_Mes = null;
            }
            finally
            {
                con.Close();
            }
            return dt_filtro_Mes;
        }

        // TODO: Filtro de los Año
       public DataTable Mantenimiento_CostoPersonalRepuestos_ListarAño_DAO(String Mes)
        {
            DataTable dt_filtro_Año = null;
            SqlCommand cmd = null;
            SqlConnection con = null;
            SqlDataReader dr = null;
            try
            {
                con = Conexion.Conectar();
                cmd = new SqlCommand(consulta_sql.Mantenimiento_CostoPersonalRepuestos_ListarAño_SQL(Mes), con);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_filtro_Año = new DataTable();
                    dt_filtro_Año.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_filtro_Año = null;
            }
            finally
            {
                con.Close();
            }
            return dt_filtro_Año;
        }
       
       
        // TODO : Metodo para graficar  KPI COSTOS - GROUPEDCHART
        public DataTable Grafica_Mantenimiento_CostosPersonalRepuestos_GCH_DAO(String anio2, String mes2)
        {
           DataTable dt_Grafica_GCH= null;
           SqlCommand cmd = null;
           SqlConnection con = null;
           SqlDataReader dr = null;

           try
           {
               con = Conexion.Conectar();
               cmd = new SqlCommand(consulta_sql.Grafica_Mantenimiento_CostosPersonalRepuestos_GCH_SQL(anio2, mes2), con);
               cmd.CommandType = CommandType.Text;
               dr = cmd.ExecuteReader();
               if (dr.HasRows)
               {
                   dt_Grafica_GCH = new DataTable();
                   dt_Grafica_GCH.Load(dr);
               }
           }
           catch (Exception ex)
           {
               dt_Grafica_GCH = null;
           }
           finally
           {
               con.Close();
           }
           return dt_Grafica_GCH;
        }

        public DataTable Mantenimiento_CostoPersonalRepuestos_ListarAño_GCH_DAO(String mes2)
        {
           DataTable dt_filtro_anio = null;
           SqlCommand cmd = null;
           SqlConnection con = null;
           SqlDataReader dr = null;
           try
           {
               con = Conexion.Conectar();
               cmd = new SqlCommand(consulta_sql.Mantenimiento_CostoPersonalRepuestos_ListarAño_GCH_SQL(mes2), con);
               cmd.CommandType = CommandType.Text;
               dr = cmd.ExecuteReader();
               if (dr.HasRows)
               {
                   dt_filtro_anio = new DataTable();
                   dt_filtro_anio.Load(dr);
               }
           }
           catch (Exception ex)
           {
               dt_filtro_anio= null;
           }
           finally
           {
               con.Close();
           }
           return dt_filtro_anio;
        }

        public DataTable Mantenimiento_CostoPersonalRepuestos_ListarMes_GCH_DAO(String anio2)
        {
           DataTable dt_filtro_Anio2 = null;
           SqlCommand cmd = null;
           SqlConnection con = null;
           SqlDataReader dr = null;
           try
           {
               con = Conexion.Conectar();
               cmd = new SqlCommand(consulta_sql.Mantenimiento_CostoPersonalRepuestos_ListarMes_GCH_SQL(anio2), con);
               cmd.CommandType = CommandType.Text;
               dr = cmd.ExecuteReader();
               if (dr.HasRows)
               {
                   dt_filtro_Anio2 = new DataTable();
                   dt_filtro_Anio2.Load(dr);
               }
           }
           catch (Exception ex)
           {
               dt_filtro_Anio2 = null;
           }
           finally
           {
               con.Close();
           }
           return dt_filtro_Anio2;
        }


        #endregion

    }
}
