﻿using ENTIDAD.Base.Area;
using System;
using System.Data;
using System.Data.SqlClient;

namespace DATO
{
    public class PersonalD
    {

        PersonalSQL personalSQL = new PersonalSQL();


        #region singleton
        private static readonly PersonalD _instancia = new PersonalD();
        public static PersonalD Instancia
        {
            get { return PersonalD._instancia; }
        }
        #endregion


        #region Cantidad de Colaboradores
        //CANTIDAD GENERAL
        public DataTable G_Personal_CantidadColaboradores_cantidadGeneral_Dao()
        {
            DataTable cantidad_general = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(personalSQL.G_Personal_CantidadColaboradores_cantidadGeneral_SQL, objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    cantidad_general = new DataTable();
                    cantidad_general.Load(dr);
                }
            }
            catch (Exception ex)
            {
                cantidad_general = null;
            }
            finally
            {
                objconnection.Close();
            }
            return cantidad_general;
        }
        //GIAMPIERE 20/ 09 / 2019
        //CANTIDAD POR DEPARTAMENTO(Area)
        public DataTable G_Personal_CantidadColaboradores_CantidadArea_Dao(String area, String subArea, String sucursal)
        {        //GIAMPIERE 20/ 09 / 2019
            DataTable dt = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(personalSQL.G_Personal_CantidadColaboradores_CantidadArea_SQL(area, subArea, sucursal), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt = new DataTable();
                    dt.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt;
        }
        public DataTable Personal_CantidadColaboradores_CantidadArea_ddlSucursal_Dao(String area, String subArea)
        {        //GIAMPIERE 20/ 09 / 2019
            DataTable dt_ListarSucursal = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(personalSQL.Personal_CantidadColaboradores_CantidadArea_ddlSucursal_SQL(area, subArea), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_ListarSucursal = new DataTable();
                    dt_ListarSucursal.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_ListarSucursal = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_ListarSucursal;
        }
        public DataTable Personal_CantidadColaboradores_CantidadArea_ddlArea_Dao(String sucursal, String subArea)
        {        //GIAMPIERE 20/ 09 / 2019
            DataTable dt_Area = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(personalSQL.Personal_CantidadColaboradores_CantidadArea_ddlArea_SQL(sucursal, subArea), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_Area = new DataTable();
                    dt_Area.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_Area = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_Area;
        }
        public DataTable Personal_CantidadColaboradores_CantidadArea_ddlSubArea_Dao(String sucursal, String area)
        {        //GIAMPIERE 20/ 09 / 2019
            DataTable dt_SubArea = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(personalSQL.Personal_CantidadColaboradores_CantidadArea_ddlSubArea_SQL(sucursal, area), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_SubArea = new DataTable();
                    dt_SubArea.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_SubArea = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_SubArea;
        }
        //EVOLUCIÓN HISTÓRICA_EVOLUCIÓN GENERAL MENSUAL
        public DataTable G_Personal_CantidadColaboradores_EvolucionGeneralMensual_Dao(String mes, String departamento, String area, String subArea, String sucursal)
        {    //GIAMPIERE 12/ 08 / 2019
            DataTable dt = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(personalSQL.G_Personal_CantidadColaboradores_EvolucionGeneralMensual_SQL(mes, departamento, area, subArea, sucursal), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt = new DataTable();
                    dt.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt;
        }
        public DataTable Personal_CantidadColaboradores_EvolucionGeneralMensual_dllSucursal_Dao(String mes, String departamento, String area, String subArea)
        {     //GIAMPIERE 12/ 08 / 2019
            DataTable dt_listar_sucursal = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(personalSQL.Personal_CantidadColaboradores_EvolucionGeneralMensual_dllSucursal_SQL(mes, departamento, area, subArea), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_listar_sucursal = new DataTable();
                    dt_listar_sucursal.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_listar_sucursal = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_listar_sucursal;
        }
        public DataTable Personal_CantidadColaboradores_EvolucionGeneralMensual_dllDepartamento_Dao(String mes, String area, String subArea, String sucursal)
        {   //GIAMPIERE 12/ 08 / 2019
            DataTable dt_listar_Departamento = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(personalSQL.Personal_CantidadColaboradores_EvolucionGeneralMensual_dllDepartamento_SQL(mes, area, subArea, sucursal), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_listar_Departamento = new DataTable();
                    dt_listar_Departamento.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_listar_Departamento = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_listar_Departamento;
        }
        public DataTable Personal_CantidadColaboradores_EvolucionGeneralMensual_ddlArea_Dao(String mes, String departamento, String subArea, String sucursal)
        { //GIAMPIERE 12/ 08 / 2019
            DataTable dt_listar_Area = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(personalSQL.Personal_CantidadColaboradores_EvolucionGeneralMensual_ddlArea_SQL(mes, departamento, subArea, sucursal), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_listar_Area = new DataTable();
                    dt_listar_Area.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_listar_Area = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_listar_Area;
        }
        public DataTable Personal_CantidadColaboradores_EvolucionGeneralMensual_ddlSubArea_Dao(String mes, String departamento, String area, String sucursal)
        {    //GIAMPIERE 12/ 08 / 2019
            DataTable dt_listar_SubArea = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(personalSQL.Personal_CantidadColaboradores_EvolucionGeneralMensual_ddlSubArea_SQL(mes, departamento, area, sucursal), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_listar_SubArea = new DataTable();
                    dt_listar_SubArea.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_listar_SubArea = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_listar_SubArea;
        }
        public DataTable Personal_CantidadColaboradores_EvolucionGeneralMensual_ddlMes_Dao(String departamento, String area, String subArea, String sucursal)
        {   //GIAMPIERE 12/ 08 / 2019
            DataTable dt_filtro_Mes = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(personalSQL.Personal_CantidadColaboradores_EvolucionGeneralMensual_ddlMes_SQL(departamento, area, subArea, sucursal), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_filtro_Mes = new DataTable();
                    dt_filtro_Mes.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_filtro_Mes = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_filtro_Mes;
        }
        //EVOLUCIÓN HISTÓRICA_EVOLUCIÓN POR ÁREA MENSUAL
        public DataTable G_Personal_CantidadColaboradores_EvolucionAreaMensual_Dao(String anio, String mes, String area, String subArea, String sucursal)
        {        //GIAMPIERE 12/ 08 / 2019
            DataTable dt = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(personalSQL.G_Personal_CantidadColaboradores_EvolucionAreaMensual_SQL(anio, mes, area, subArea, sucursal), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt = new DataTable();
                    dt.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt;
        }
        public DataTable Personal_CantidadColaboradores_EvolucionAreaMensual_ddlSucursal_Dao(String anio, String mes, String area, String subArea)
        {        //GIAMPIERE 12/ 08 / 2019
            DataTable dt_ListaSucursal = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(personalSQL.Personal_CantidadColaboradores_EvolucionAreaMensual_ddlSucursal_SQL(anio, mes, area, subArea), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_ListaSucursal = new DataTable();
                    dt_ListaSucursal.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_ListaSucursal = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_ListaSucursal;
        }
        public DataTable Personal_CantidadColaboradores_EvolucionAreaMensual_ddlArea_Dao(String anio, String mes, String subArea, String sucursal)
        {        //GIAMPIERE 12/ 08 / 2019
            DataTable dt_ListaArea = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(personalSQL.Personal_CantidadColaboradores_EvolucionAreaMensual_ddlArea_SQL(anio, mes, subArea, sucursal), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_ListaArea = new DataTable();
                    dt_ListaArea.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_ListaArea = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_ListaArea;
        }
        public DataTable Personal_CantidadColaboradores_EvolucionAreaMensual_ddlSubArea_Dao(String anio, String mes, String area, String sucursal)
        {        //GIAMPIERRE 12/ 08 / 2019
            DataTable dt_ListaSubArea = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(personalSQL.Personal_CantidadColaboradores_EvolucionAreaMensual_ddlSubArea_SQL(anio, mes, area, sucursal), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_ListaSubArea = new DataTable();
                    dt_ListaSubArea.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_ListaSubArea = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_ListaSubArea;
        }
        public DataTable Personal_CantidadColaboradores_EvolucionAreaMensual_ddlAnio_Dao(String mes, String area, String subArea, String sucursal)
        {        //GIAMPIERE 12/ 08 / 2019
            DataTable dt_ListarAnio = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(personalSQL.Personal_CantidadColaboradores_EvolucionAreaMensual_ddlAnio_SQL(mes, area, subArea, sucursal), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_ListarAnio = new DataTable();
                    dt_ListarAnio.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_ListarAnio = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_ListarAnio;
        }
        public DataTable Personal_CantidadColaboradores_EvolucionAreaMensual_ddlMes_Dao(String anio, String area, String subArea, String sucursal)
        {        //GIAMPIERE 12/ 08 / 2019
            DataTable dt_ListarMes = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(personalSQL.Personal_CantidadColaboradores_EvolucionAreaMensual_ddlMes_SQL(anio, area, subArea, sucursal), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_ListarMes = new DataTable();
                    dt_ListarMes.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_ListarMes = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_ListarMes;
        }

        #endregion

        #region Costos Planillas

        public DataTable G_Personal_CostoPlanilla_DAO_CantidadGeneral()
        {
            DataTable cantidad_general = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(personalSQL.costoPlanilla_cantidadGeneral, objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    cantidad_general = new DataTable();
                    cantidad_general.Load(dr);
                }
            }
            catch (Exception ex)
            {
                cantidad_general = null;
            }
            finally
            {
                objconnection.Close();
            }
            return cantidad_general;
        }

        public DataTable G_Personal_CostoPlanilla_DAO_CantidadGeneral(String sucursal, String area, String subarea, String fechaInicio, String fechaFin)
        {
            DataTable dt_cantidadgeneral = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(personalSQL.G_Personal_CantidadArea_SQLP(sucursal, area, subarea, fechaInicio, fechaFin), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_cantidadgeneral = new DataTable();
                    dt_cantidadgeneral.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_cantidadgeneral = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_cantidadgeneral;
        }

        public DataTable G_Personal_CostoPlanilla_DAO_ddlSucursal(String area, String subarea, String fechaInicio, String fechaFin)
        {
            DataTable dt_cantidadgeneral = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(personalSQL.G_Personal_CostoPlanilla_CantidadArea_ddlSucursal(area, subarea, fechaInicio, fechaFin), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_cantidadgeneral = new DataTable();
                    dt_cantidadgeneral.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_cantidadgeneral = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_cantidadgeneral;
        }

        public DataTable G_Personal_CostoPlanilla_DAO_ddlSubArea(String area, String sucursal, String fechaInicio, String fechaFin)
        {
            DataTable dt_subArea = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(personalSQL.G_Personal_CostoPlanilla_CantidadArea_ddlSubArea(area, sucursal, fechaInicio, fechaFin), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_subArea = new DataTable();
                    dt_subArea.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_subArea = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_subArea;
        }

        public DataTable G_Personal_CostoPlanilla_DAO_ddlArea(String sucursal, String subArea, String fechaInicio, String fechaFin)
        {
            DataTable dt_Area = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(personalSQL.G_Personal_CostoPlanilla_CantidadArea_ddlArea(sucursal, subArea, fechaInicio, fechaFin), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_Area = new DataTable();
                    dt_Area.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_Area = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_Area;
        }

        //EVOLUCIÓN GENERAL MENSUAL
        public DataTable G_Personal_CostoPlanilla_EvolucionGeneralMensual_Dao(String mes, String departamento, String area, String subArea, String sucursal)
        {//GIAMPIERE 09 /09 / 2019
            DataTable dt = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(personalSQL.G_Personal_CostoPlanilla_EvolucionGeneralMensual_SQL(mes, departamento, area, subArea, sucursal), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt = new DataTable();
                    dt.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt;
        }
        public DataTable Personal_CostoPlanilla_EvolucionGeneralMensual_ddlSucursal_Dao(String mes, String departamento, String area, String subArea)
        {//GIAMPIERE 09 /09 / 2019
            DataTable dt_listar_sucursal = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(personalSQL.Personal_CostoPlanilla_EvolucionGeneralMensual_ddlSucursal_SQL(mes, departamento, area, subArea), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_listar_sucursal = new DataTable();
                    dt_listar_sucursal.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_listar_sucursal = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_listar_sucursal;
        }
        public DataTable Personal_CostoPlanilla_EvolucionGeneralMensual_ddlDepartamento_Dao(String mes, String area, String subArea, String sucursal)
        {//GIAMPIERE 09 /09 / 2019
            DataTable dt_listar_Departamento = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(personalSQL.Personal_CostoPlanilla_EvolucionGeneralMensual_ddlDepartamento_SQL(mes, area, subArea, sucursal), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_listar_Departamento = new DataTable();
                    dt_listar_Departamento.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_listar_Departamento = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_listar_Departamento;
        }
        public DataTable Personal_CostoPlanilla_EvolucionGeneralMensual_ddlArea_Dao(String mes, String departamento, String subArea, String sucursal)
        {//GIAMPIERE 09 /09 / 2019
            DataTable dt_listar_Area = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(personalSQL.Personal_CostoPlanilla_EvolucionGeneralMensual_ddlArea_SQL(mes, departamento, subArea, sucursal), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_listar_Area = new DataTable();
                    dt_listar_Area.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_listar_Area = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_listar_Area;
        }
        public DataTable Personal_CostoPlanilla_EvolucionGeneralMensual_ddlSubArea_Dao(String mes, String departamento, String area, String sucursal)
        {//GIAMPIERE 09 /09 / 2019
            DataTable dt_listar_SubArea = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(personalSQL.Personal_CostoPlanilla_EvolucionGeneralMensual_ddlSubArea_SQL(mes, departamento, area, sucursal), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_listar_SubArea = new DataTable();
                    dt_listar_SubArea.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_listar_SubArea = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_listar_SubArea;
        }
        public DataTable Personal_CostoPlanilla_EvolucionGeneralMensual_ddlMes_Dao(String departamento, String area, String subArea, String sucursal)
        {//GIAMPIERE 09 /09 / 2019
            DataTable dt_filtro_Mes = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(personalSQL.Personal_CostoPlanilla_EvolucionGeneralMensual_ddlMes_SQL(departamento, area, subArea, sucursal), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_filtro_Mes = new DataTable();
                    dt_filtro_Mes.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_filtro_Mes = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_filtro_Mes;
        }
        //EVOLUCIÓN POR ÁREA MENSUAL
        public DataTable G_Personal_CostoPlanilla_EvolucionAreaMensual_Dao(String anio, String mes, String area, String subArea, String sucursal)
        {        //GIAMPIERE 09 /09 / 2019
            DataTable dt = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(personalSQL.G_Personal_CostoPlanilla_EvolucionAreaMensual_SQL(anio, mes, area, subArea, sucursal), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt = new DataTable();
                    dt.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt;
        }
        public DataTable Personal_CostoPlanilla_EvolucionAreaMensual_ddlSucursal_Dao(String anio, String mes, String area, String subArea)
        {        //GIAMPIERE 09 /09 / 2019
            DataTable dt_ListaSucursal = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(personalSQL.Personal_CostoPlanilla_EvolucionAreaMensual_ddlSucursal_SQL(anio, mes, area, subArea), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_ListaSucursal = new DataTable();
                    dt_ListaSucursal.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_ListaSucursal = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_ListaSucursal;
        }
        public DataTable Personal_CostoPlanilla_EvolucionAreaMensual_ddlArea_Dao(String anio, String mes, String subArea, String sucursal)
        {        //GIAMPIERE 09 /09 / 2019
            DataTable dt_ListaArea = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(personalSQL.Personal_CostoPlanilla_EvolucionAreaMensual_ddlArea_SQL(anio, mes, subArea, sucursal), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_ListaArea = new DataTable();
                    dt_ListaArea.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_ListaArea = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_ListaArea;
        }
        public DataTable Personal_CostoPlanilla_EvolucionAreaMensual_ddlSubArea_Dao(String anio, String mes, String area, String sucursal)
        {        //GIAMPIERE 09 /09 / 2019
            DataTable dt_ListaSubArea = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(personalSQL.Personal_CostoPlanilla_EvolucionAreaMensual_ddlSubArea_SQL(anio, mes, area, sucursal), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_ListaSubArea = new DataTable();
                    dt_ListaSubArea.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_ListaSubArea = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_ListaSubArea;
        }
        public DataTable Personal_CostoPlanilla_EvolucionAreaMensual_ddlAnio_Dao(String mes, String area, String subArea, String sucursal)
        {        //GIAMPIERE 09 /09 / 2019
            DataTable dt_ListarAnio = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(personalSQL.Personal_CostoPlanilla_EvolucionAreaMensual_ddlAnio_SQL(mes, area, subArea, sucursal), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_ListarAnio = new DataTable();
                    dt_ListarAnio.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_ListarAnio = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_ListarAnio;
        }
        public DataTable Personal_CostoPlanilla_EvolucionAreaMensual_ddlMes_Dao(String anio, String area, String subArea, String sucursal)
        {        //GIAMPIERE 09 /09 / 2019
            DataTable dt_ListarMes = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(personalSQL.Personal_CostoPlanilla_EvolucionAreaMensual_ddlMes_SQL(anio, area, subArea, sucursal), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_ListarMes = new DataTable();
                    dt_ListarMes.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_ListarMes = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_ListarMes;
        }

        #endregion

        #region Ausentismo y Descansos Medicos
        //CANTIDAD GENERAL
        //Giampiere 19/09/2019
        public DataTable G_Personal_Ausentismo_cantidadGeneral_Dao()
        {
            DataTable cantidad_general = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(personalSQL.G_Personal_Ausentismo_cantidadGeneral_SQL, objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    cantidad_general = new DataTable();
                    cantidad_general.Load(dr);
                }
            }
            catch (Exception ex)
            {
                cantidad_general = null;
            }
            finally
            {
                objconnection.Close();
            }
            return cantidad_general;
        }
        //Giampiere 19/09/2019
        public DataTable G_Personal_Descanso_cantidadGeneral_Dao()
        {
            DataTable cantidad_general = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(personalSQL.G_Personal_Descanso_cantidadGeneral_SQL, objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    cantidad_general = new DataTable();
                    cantidad_general.Load(dr);
                }
            }
            catch (Exception ex)
            {
                cantidad_general = null;
            }
            finally
            {
                objconnection.Close();
            }
            return cantidad_general;
        }
        //GIAMPIERE 17/ 08 / 2019
        public DataTable Personal_AusentismoyDescanso_Tipo_Dao()
        {
            DataTable dt_tipo = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(personalSQL.Personal_AusentismoyDescanso_Tipo_SQL, objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_tipo = new DataTable();
                    dt_tipo.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_tipo = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_tipo;
        }
        public DataTable G_Personal_Ausentismo_CantidadArea_Dao(String area, String subArea, String sucursal)
        {        //GIAMPIERE 13/ 09 / 2019
            DataTable dt = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(personalSQL.G_Personal_Ausentismo_CantidadArea_SQL(area, subArea, sucursal), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt = new DataTable();
                    dt.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt;
        }
        public DataTable G_Personal_DescansoMedico_CantidadArea_Dao(String area, String subArea, String sucursal)
        {        //GIAMPIERE 13/ 09 / 2019
            DataTable dt = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(personalSQL.G_Personal_DescansoMedico_CantidadArea_SQL(area, subArea, sucursal), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt = new DataTable();
                    dt.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt;
        }
        public DataTable Personal_AusentismoYDescanso_CantidadArea_ddlSucursal_Dao(String area, String subArea)
        {        //GIAMPIERE 13/ 09 / 2019
            DataTable dt_ListarSucursal = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(personalSQL.Personal_AusentismoYDescanso_CantidadArea_ddlSucursal_SQL(area, subArea), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_ListarSucursal = new DataTable();
                    dt_ListarSucursal.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_ListarSucursal = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_ListarSucursal;
        }
        public DataTable Personal_AusentismoYDescanso_CantidadArea_ddlArea_Dao(String sucursal, String subArea)
        {        //GIAMPIERE 13/ 09 / 2019
            DataTable dt_Area = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(personalSQL.Personal_AusentismoYDescanso_CantidadArea_ddlArea_SQL(sucursal, subArea), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_Area = new DataTable();
                    dt_Area.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_Area = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_Area;
        }
        public DataTable Personal_AusentismoYDescanso_CantidadArea_ddlSubArea_Dao(String sucursal, String area)
        {        //GIAMPIERE 13/ 09 / 2019
            DataTable dt_SubArea = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(personalSQL.Personal_AusentismoYDescanso_CantidadArea_ddlSubArea_SQL(sucursal, area), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_SubArea = new DataTable();
                    dt_SubArea.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_SubArea = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_SubArea;
        }
        //EVOLUCIÓN GENERAL MENSUAL

        public DataTable G_Personal_Ausentismo_EvolucionGeneralMensual_Dao(String mes, String departamento, String area, String subArea, String sucursal)
        {    //GIAMPIERE 18/ 09 / 2019
            DataTable dt = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(personalSQL.G_Personal_Ausentismo_EvolucionGeneralMensual_SQL(mes, departamento, area, subArea, sucursal), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt = new DataTable();
                    dt.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt;
        }
        public DataTable G_Personal_Descanso_EvolucionGeneralMensual_Dao(String mes, String departamento, String area, String subArea, String sucursal)
        {    //GIAMPIERE 18/ 09 / 2019
            DataTable dt = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(personalSQL.G_Personal_Descanso_EvolucionGeneralMensual_SQL(mes, departamento, area, subArea, sucursal), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt = new DataTable();
                    dt.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt;
        }
        public DataTable Personal_AusentismoYDescanso_EvolucionGeneralMensual_ddlSucursal_Dao(String mes, String departamento, String area, String subArea)
        {    //GIAMPIERE 18/ 09 / 2019
            DataTable dt_listar_sucursal = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(personalSQL.Personal_AusentismoYDescanso_EvolucionGeneralMensual_ddlSucursal_SQL(mes, departamento, area, subArea), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_listar_sucursal = new DataTable();
                    dt_listar_sucursal.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_listar_sucursal = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_listar_sucursal;
        }
        public DataTable Personal_AusentismoYDescanso_EvolucionGeneralMensual_ddlDepartamento_Dao(String mes, String area, String subArea, String sucursal)
        {   //GIAMPIERE 18/ 09 / 2019
            DataTable dt_listar_Departamento = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(personalSQL.Personal_AusentismoYDescanso_EvolucionGeneralMensual_ddlDepartamento_SQL(mes, area, subArea, sucursal), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_listar_Departamento = new DataTable();
                    dt_listar_Departamento.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_listar_Departamento = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_listar_Departamento;
        }
        public DataTable Personal_AusentismoYDescanso_EvolucionGeneralMensual_ddlArea_Dao(String mes, String departamento, String subArea, String sucursal)
        {    //GIAMPIERE 18/ 09 / 2019
            DataTable dt_listar_Area = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(personalSQL.Personal_AusentismoYDescanso_EvolucionGeneralMensual_ddlArea_SQL(mes, departamento, subArea, sucursal), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_listar_Area = new DataTable();
                    dt_listar_Area.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_listar_Area = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_listar_Area;
        }
        public DataTable Personal_AusentismoYDescanso_EvolucionGeneralMensual_ddlSubArea_Dao(String mes, String departamento, String area, String sucursal)
        {    //GIAMPIERE 18/ 09 / 2019
            DataTable dt_listar_SubArea = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(personalSQL.Personal_AusentismoYDescanso_EvolucionGeneralMensual_ddlSubArea_SQL(mes, departamento, area, sucursal), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_listar_SubArea = new DataTable();
                    dt_listar_SubArea.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_listar_SubArea = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_listar_SubArea;
        }
        public DataTable Personal_AusentismoYDescanso_EvolucionGeneralMensual_ddlMes_Dao(String departamento, String area, String subArea, String sucursal)
        {    //GIAMPIERE 18/ 09 / 2019
            DataTable dt_filtro_Mes = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(personalSQL.Personal_AusentismoYDescanso_EvolucionGeneralMensual_ddlMes_SQL(departamento, area, subArea, sucursal), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_filtro_Mes = new DataTable();
                    dt_filtro_Mes.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_filtro_Mes = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_filtro_Mes;
        }
        //EVOLUCIÓN POR ÁREA MENSUAL
        public DataTable G_Personal_Ausentismo_EvolucionAreaMensual_Dao(String anio, String mes, String area, String subArea, String sucursal)
        {        //GIAMPIERE 18/ 09 / 2019
            DataTable dt = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(personalSQL.G_Personal_Ausentismo_EvolucionAreaMensual_SQL(anio, mes, area, subArea, sucursal), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt = new DataTable();
                    dt.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt;
        }
        public DataTable G_Personal_Descanso_EvolucionAreaMensual_Dao(String anio, String mes, String area, String subArea, String sucursal)
        {        //GIAMPIERE 18/ 09 / 2019
            DataTable dt = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(personalSQL.G_Personal_Descanso_EvolucionAreaMensual_SQL(anio, mes, area, subArea, sucursal), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt = new DataTable();
                    dt.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt;
        }
        public DataTable Personal_AusentismoYDescanso_EvolucionAreaMensual_ddlSucursal_Dao(String anio, String mes, String area, String subArea)
        {        //GIAMPIERE 18/ 09 / 2019
            DataTable dt_ListaSucursal = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(personalSQL.Personal_AusentismoYDescanso_EvolucionAreaMensual_ddlSucursal_SQL(anio, mes, area, subArea), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_ListaSucursal = new DataTable();
                    dt_ListaSucursal.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_ListaSucursal = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_ListaSucursal;
        }
        public DataTable Personal_AusentismoYDescanso_EvolucionAreaMensual_ddlArea_Dao(String anio, String mes, String subArea, String sucursal)
        {        //GIAMPIERE 18/ 09 / 2019
            DataTable dt_ListaArea = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(personalSQL.Personal_AusentismoYDescanso_EvolucionAreaMensual_ddlArea_SQL(anio, mes, subArea, sucursal), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_ListaArea = new DataTable();
                    dt_ListaArea.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_ListaArea = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_ListaArea;
        }
        public DataTable Personal_AusentismoYDescanso_EvolucionAreaMensual_ddlSubArea_Dao(String anio, String mes, String area, String sucursal)
        {        //GIAMPIERE 18/ 09 / 2019
            DataTable dt_ListaSubArea = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(personalSQL.Personal_AusentismoYDescanso_EvolucionAreaMensual_ddlSubArea_SQL(anio, mes, area, sucursal), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_ListaSubArea = new DataTable();
                    dt_ListaSubArea.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_ListaSubArea = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_ListaSubArea;
        }
        public DataTable Personal_AusentismoYDescanso_EvolucionAreaMensual_ddlAnio_Dao(String mes, String area, String subArea, String sucursal)
        {        //GIAMPIERE 18/ 09 / 2019
            DataTable dt_ListarAnio = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(personalSQL.Personal_AusentismoYDescanso_EvolucionAreaMensual_ddlAnio_SQL(mes, area, subArea, sucursal), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_ListarAnio = new DataTable();
                    dt_ListarAnio.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_ListarAnio = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_ListarAnio;
        }
        public DataTable Personal_AusentismoYDescanso_EvolucionAreaMensual_ddlMes_Dao(String anio, String area, String subArea, String sucursal)
        {        //GIAMPIERE 18/ 09 / 2019
            DataTable dt_ListarMes = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(personalSQL.Personal_AusentismoYDescanso_EvolucionAreaMensual_ddlMes_SQL(anio, area, subArea, sucursal), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_ListarMes = new DataTable();
                    dt_ListarMes.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_ListarMes = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_ListarMes;
        }

        #endregion  Fallos y Descansos Medicos

        #region Hora_Extra
        //CANTIDAD GENERAL
        public DataTable G_Personal_HoraExtra_cantidadGeneral_Dao()
        {
            DataTable cantidad_general = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(personalSQL.G_Personal_HoraExtra_cantidadGeneral_SQL, objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    cantidad_general = new DataTable();
                    cantidad_general.Load(dr);
                }
            }
            catch (Exception ex)
            {
                cantidad_general = null;
            }
            finally
            {
                objconnection.Close();
            }
            return cantidad_general;
        }
        //GIAMPIERE 12/ 08 / 2019
        //CANTIDAD POR DEPARTAMENTO(Area)
        public DataTable G_Personal_HoraExtras_CantidadArea_Dao(String area, String subArea, String sucursal)
        {        //GIAMPIERE 12/ 08 / 2019
            DataTable dt = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(personalSQL.G_Personal_HoraExtras_CantidadArea_SQL(area, subArea, sucursal), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt = new DataTable();
                    dt.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt;
        }
        public DataTable Personal_HoraExtras_CantidadArea_ddlSucursal_Dao(String area, String subArea)
        {        //GIAMPIERE 12/ 08 / 2019
            DataTable dt_ListarSucursal = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(personalSQL.Personal_HoraExtras_CantidadArea_ddlSucursal_SQL(area, subArea), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_ListarSucursal = new DataTable();
                    dt_ListarSucursal.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_ListarSucursal = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_ListarSucursal;
        }
        public DataTable Personal_HoraExtras_CantidadArea_ddlArea_Dao(String sucursal, String subArea)
        {        //GIAMPIERE 12/ 08 / 2019
            DataTable dt_Area = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(personalSQL.Personal_HoraExtras_CantidadArea_ddlArea_SQL(sucursal, subArea), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_Area = new DataTable();
                    dt_Area.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_Area = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_Area;
        }
        public DataTable Personal_HoraExtras_CantidadArea_ddlSubArea_Dao(String sucursal, String area)
        {        //GIAMPIERE 12/ 08 / 2019
            DataTable dt_SubArea = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(personalSQL.Personal_HoraExtras_CantidadArea_ddlSubArea_SQL(sucursal, area), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_SubArea = new DataTable();
                    dt_SubArea.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_SubArea = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_SubArea;
        }
        //EVOLUCIÓN GENERAL MENSUAL
        public DataTable G_Personal_HoraExtras_EvolucionGeneralMensual_Dao(String mes, String departamento, String area, String subArea, String sucursal)
        {    //GIAMPIERE 12/ 08 / 2019
            DataTable dt = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(personalSQL.G_Personal_HoraExtras_EvolucionGeneralMensual_SQL(mes, departamento, area, subArea, sucursal), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt = new DataTable();
                    dt.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt;
        }
        public DataTable Personal_HoraExtras_EvolucionGeneralMensual_ddlSucursal_Dao(String mes, String departamento, String area, String subArea)
        {    //GIAMPIERE 12/ 08 / 2019
            DataTable dt_listar_sucursal = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(personalSQL.Personal_HoraExtras_EvolucionGeneralMensual_ddlSucursal_SQL(mes, departamento, area, subArea), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_listar_sucursal = new DataTable();
                    dt_listar_sucursal.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_listar_sucursal = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_listar_sucursal;
        }
        public DataTable Personal_HoraExtras_EvolucionGeneralMensual_ddlDepartamento_Dao(String mes, String area, String subArea, String sucursal)
        {   //GIAMPIERE 12/ 08 / 2019
            DataTable dt_listar_Departamento = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(personalSQL.Personal_HoraExtras_EvolucionGeneralMensual_ddlDepartamento_SQL(mes, area, subArea, sucursal), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_listar_Departamento = new DataTable();
                    dt_listar_Departamento.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_listar_Departamento = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_listar_Departamento;
        }
        public DataTable Personal_HoraExtras_EvolucionGeneralMensual_ddlArea_Dao(String mes, String departamento, String subArea, String sucursal)
        {    //GIAMPIERE 12/ 08 / 2019
            DataTable dt_listar_Area = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(personalSQL.Personal_HoraExtras_EvolucionGeneralMensual_ddlArea_SQL(mes, departamento, subArea, sucursal), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_listar_Area = new DataTable();
                    dt_listar_Area.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_listar_Area = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_listar_Area;
        }
        public DataTable Personal_HoraExtras_EvolucionGeneralMensual_ddlSubArea_Dao(String mes, String departamento, String area, String sucursal)
        {    //GIAMPIERE 12/ 08 / 2019
            DataTable dt_listar_SubArea = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(personalSQL.Personal_HoraExtras_EvolucionGeneralMensual_ddlSubArea_SQL(mes, departamento, area, sucursal), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_listar_SubArea = new DataTable();
                    dt_listar_SubArea.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_listar_SubArea = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_listar_SubArea;
        }
        public DataTable Personal_HoraExtras_EvolucionGeneralMensual_ddlMes_Dao(String departamento, String area, String subArea, String sucursal)
        {    //GIAMPIERE 12/ 08 / 2019
            DataTable dt_filtro_Mes = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(personalSQL.Personal_HoraExtras_EvolucionGeneralMensual_ddlMes_SQL(departamento, area, subArea, sucursal), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_filtro_Mes = new DataTable();
                    dt_filtro_Mes.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_filtro_Mes = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_filtro_Mes;
        }
        //EVOLUCIÓN POR ÁREA MENSUAL
        public DataTable G_Personal_HoraExtras_EvolucionAreaMensual_Dao(String anio, String mes, String area, String subArea, String sucursal)
        {        //GIAMPIERE 12/ 08 / 2019
            DataTable dt = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(personalSQL.G_Personal_HoraExtras_EvolucionAreaMensual_SQL(anio, mes, area, subArea, sucursal), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt = new DataTable();
                    dt.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt;
        }
        public DataTable Personal_HoraExtras_EvolucionAreaMensual_ddlSucursal_Dao(String anio, String mes, String area, String subArea)
        {        //GIAMPIERE 12/ 08 / 2019
            DataTable dt_ListaSucursal = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(personalSQL.Personal_HoraExtras_EvolucionAreaMensual_ddlSucursal_SQL(anio, mes, area, subArea), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_ListaSucursal = new DataTable();
                    dt_ListaSucursal.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_ListaSucursal = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_ListaSucursal;
        }
        public DataTable Personal_HoraExtras_EvolucionAreaMensual_ddlArea_Dao(String anio, String mes, String subArea, String sucursal)
        {        //GIAMPIERE 12/ 08 / 2019
            DataTable dt_ListaArea = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(personalSQL.Personal_HoraExtras_EvolucionAreaMensual_ddlArea_SQL(anio, mes, subArea, sucursal), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_ListaArea = new DataTable();
                    dt_ListaArea.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_ListaArea = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_ListaArea;
        }
        public DataTable Personal_HoraExtras_EvolucionAreaMensual_ddlSubArea_Dao(String anio, String mes, String area, String sucursal)
        {        //GIAMPIERE 12/ 08 / 2019
            DataTable dt_ListaSubArea = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(personalSQL.Personal_HoraExtras_EvolucionAreaMensual_ddlSubArea_SQL(anio, mes, area, sucursal), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_ListaSubArea = new DataTable();
                    dt_ListaSubArea.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_ListaSubArea = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_ListaSubArea;
        }
        public DataTable Personal_HoraExtras_EvolucionAreaMensual_ddlAnio_Dao(String mes, String area, String subArea, String sucursal)
        {        //GIAMPIERE 12/ 08 / 2019
            DataTable dt_ListarAnio = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(personalSQL.Personal_HoraExtras_EvolucionAreaMensual_ddlAnio_SQL(mes, area, subArea, sucursal), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_ListarAnio = new DataTable();
                    dt_ListarAnio.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_ListarAnio = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_ListarAnio;
        }
        public DataTable Personal_HoraExtras_EvolucionAreaMensual_ddlMes_Dao(String anio, String area, String subArea, String sucursal)
        {        //GIAMPIERE 12/ 08 / 2019
            DataTable dt_ListarMes = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(personalSQL.Personal_HoraExtras_EvolucionAreaMensual_ddlMes_SQL(anio, area, subArea, sucursal), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_ListarMes = new DataTable();
                    dt_ListarMes.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_ListarMes = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_ListarMes;
        }

        #endregion


        //? #################################################   SUSPNESIONES Y AMONESTACIONES ############################################
        #region PERSONAL_SUSPENSIONES_AMONESTACIONES_JORGEBALTODANO
        

        //CANTIDAD GENERAL
        //Jorge Baltodano 19/09/2019
        public DataTable Grafica_Personal_Suspensiones_CantidadSuspensiones_DAO()
        {
            DataTable cantidad_general = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(personalSQL.Grafica_Personal_Suspensiones_CantidadSuspensiones_SQL, objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    cantidad_general = new DataTable();
                    cantidad_general.Load(dr);
                }
            }
            catch (Exception ex)
            {
                cantidad_general = null;
            }
            finally
            {
                objconnection.Close();
            }
            return cantidad_general;
        }
        
        //Jorge Baltodano  19/09/2019
        public DataTable Grafica_Personal_Amonestaciones_CantidadAmonestaciones_DAO()
        {
            DataTable cantidad_general = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(personalSQL.Grafica_Personal_Amonestaciones_CantidadAmonestaciones_SQL, objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    cantidad_general = new DataTable();
                    cantidad_general.Load(dr);
                }
            }
            catch (Exception ex)
            {
                cantidad_general = null;
            }
            finally
            {
                objconnection.Close();
            }
            return cantidad_general;
        }
       
        //Jorge Baltodano 17/ 08 / 2019
        public DataTable Listar_Personal_SuspensionesyAmonestaciones_Tipo_DAO()
        {
            DataTable dt_tipo = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(personalSQL.Consulta_Personal_SuspensionesyAmonestaciones_Tipo_SQL, objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_tipo = new DataTable();
                    dt_tipo.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_tipo = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_tipo;
        } 


        // !* GRAFICA 2:
        public DataTable Personal_Suspensiones_listarSucursal(String area, String subArea)
        {
            DataTable dt_ListaSucursal = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(personalSQL.Personal_Suspensiones_listarSucursal(area, subArea), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_ListaSucursal = new DataTable();
                    dt_ListaSucursal.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_ListaSucursal = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_ListaSucursal;
        }
        public DataTable Personal_Suspensiones_listarArea( String sucursal, String subArea)
        {
            DataTable dt_ListaArea = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(personalSQL.Personal_Suspensiones_ListarArea(sucursal, subArea), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_ListaArea = new DataTable();
                    dt_ListaArea.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_ListaArea = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_ListaArea;
        }
        public DataTable Personal_Suspensiones_listarSubArea( String sucursal, String area)
        {
            DataTable dt_ListaSubArea = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(personalSQL.Personal_Suspensiones_ListarsSubArea( area, sucursal ), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_ListaSubArea = new DataTable();
                    dt_ListaSubArea.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_ListaSubArea = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_ListaSubArea;
        }        

        public DataTable Grafica_Personal_Suspensiones_Cantidad_Suspensiones_Departamento(String area, String subArea, String sucursal)
        {

            DataTable dt = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(personalSQL.Grafica_Personal_Suspensiones_CantidadSuspensionesDepartamento(area, subArea, sucursal), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt = new DataTable();
                    dt.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt;
        }


        /* ####### FUNCION PARA GRAFICAR EVOLUCION HISTORICA GENERAL MENSUAL   ##### */

        // Listar Sucursal 
        public DataTable Personal_Suspensiones_ListarSucursal_EHGM_DAO(String Anio, String Area, String SubArea, String Departamento, String Mes){
            DataTable dt_Sucursal = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(personalSQL.Personal_Suspensiones_ListarSucursal_EHGM_SQL(  Anio, Area, SubArea, Departamento, Mes), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_Sucursal = new DataTable();
                    dt_Sucursal.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_Sucursal = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_Sucursal;
        }

        // Listar Departamento
        public DataTable Personal_Suspensiones_ListarDepartamento_EHGM_DAO( String Anio, String Area, String SubArea, String Sucursal, String Mes){
            DataTable dt_Departamento = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(personalSQL.Personal_Suspensiones_ListarDepartamento_EHGM_SQL(Anio, Area,  SubArea,  Sucursal, Mes), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_Departamento = new DataTable();
                    dt_Departamento.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_Departamento = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_Departamento;
        }

        //Listar Area
        public DataTable Personal_Suspensiones_ListarArea_EHGM_DAO(String Anio, String SubArea, String Sucursal, String Departamento,String Mes){
            DataTable dt_Area = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(personalSQL.Personal_Suspensiones_ListarArea_EHGM_SQL(Anio, SubArea, Sucursal, Departamento, Mes), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_Area = new DataTable();
                    dt_Area.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_Area = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_Area;
        }

        // Listar SubArea
        public DataTable Personal_Suspensiones_ListarSubArea_EHGM_DAO(String Departamento, String Area, String Sucursal, String Anio, String Mes){
            DataTable dt_SubArea = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(personalSQL.Personal_Suspensiones_ListarSubArea_EHGM_SQL( Departamento,  Area,  Sucursal,  Anio,  Mes), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_SubArea = new DataTable();
                    dt_SubArea.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_SubArea = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_SubArea;
        }

        // Listar Año
        public DataTable Personal_Suspensiones_ListarAño_EHGM_DAO(string Departamento, string SubArea, string Area, string Sucursal, string Mes){
            DataTable dt_Año = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(personalSQL.Personal_Suspensiones_ListarAño_EHGM_SQL( Departamento, SubArea, Area, Sucursal,  Mes), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_Año = new DataTable();
                    dt_Año.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_Año = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_Año;
        }

        // Listar Mes
        public DataTable Personal_Suspensiones_ListarMes_EHGM_DAO(String Departamento, string SubArea, string Area, string Sucursal, string Año){
            DataTable dt_Mes = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(personalSQL.Personal_Suspensiones_ListarMes_EHGM_SQL( Departamento, SubArea, Area, Sucursal, Año), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_Mes = new DataTable();
                    dt_Mes.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_Mes = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_Mes;
        }

        // GRAFICA EVOLUCION GENERAL MENSUAL
       

         
     /* ####### FUNCION PARA GRAFICAR EVOLUCION HISTORICA GENERAL MENSUAL   ##### */
      

    }



    #endregion




}