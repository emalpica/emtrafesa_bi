﻿using ENTIDAD.Base.Area;
using System;
using System.Data;
using System.Data.SqlClient;

namespace DATO
{
    public class EncomiendaD
    {
        EncomiendaSQL EncomiendaSQL = new EncomiendaSQL();

        #region singleton
        private static readonly EncomiendaD _instancia = new EncomiendaD();
        public static EncomiendaD Instancia
        {
            get { return EncomiendaD._instancia; }
        }
        #endregion

        #region Ventas por Sucursal
       
        //GIAMPIERE 22/ 08 / 2019
        public DataTable G_Encomienda_VentaSucursal_GeneralMensual_Dao( String mes, String tipo)
        {
            DataTable dt = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(EncomiendaSQL.G_Encomienda_VentaSucursal_GeneralMensual_SQL( mes, tipo), objconnection);
               cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt = new DataTable();
                    dt.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt;
        }
        //GIAMPIERE 22/ 08 / 2019
        public DataTable Encomienda_VentaSucursal_GeneralMensual_ddlMes_Dao(String tipo)
        {      
            DataTable dt_Mes = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(EncomiendaSQL.Encomienda_VentaSucursal_GeneralMensual_ddlMes_SQL( tipo), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_Mes = new DataTable();
                    dt_Mes.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_Mes = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_Mes;
        }
        //GIAMPIERE 22/ 08 / 2019
        public DataTable Encomienda_VentaSucursal_GeneralMensual_ddlTipo_Dao(String Mes)
        {
            DataTable dt_Tipo = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(EncomiendaSQL.Encomienda_VentaSucursal_GeneralMensual_ddlTipo_SQL(Mes), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_Tipo = new DataTable();
                    dt_Tipo.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_Tipo = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_Tipo;
        }
        //General ORIGEN
        public DataTable G_Encomienda_VentaSucursal_GeneralOrigen_Dao(String anio, String mes, String tipo)
        {
            DataTable dt = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(EncomiendaSQL.G_Encomienda_VentaSucursal_GeneralOrigen_SQL(anio, mes, tipo), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt = new DataTable();
                    dt.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt;
        }
        //GIAMPIERE 27/ 08 / 2019
        public DataTable Encomienda_VentaSucursal_GeneralOrigen_AgrupacionTrimestre_Dao(String anio, String tipo)
        {
            DataTable dt = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(EncomiendaSQL.Encomienda_VentaSucursal_GeneralOrigen_AgrupacionTrimestre_SQL(anio,tipo), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt = new DataTable();
                    dt.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt;
        }
        //GIAMPIERE 23/ 08 / 2019
        public DataTable Encomienda_VentaSucursal_GeneralOrigen_AgrupacionBimestre_Dao(String anio, String tipo)
        {
            DataTable dt = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(EncomiendaSQL.Encomienda_VentaSucursal_GeneralOrigen_AgrupacionBimestre_SQL(anio,tipo), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt = new DataTable();
                    dt.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt;
        }
        //GIAMPIERE 23/ 08 / 2019
        public DataTable Encomienda_VentaSucursal_GeneralOrigen_ddlMes_Dao(String anio, String tipo)
        {
            DataTable dt_Mes = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(EncomiendaSQL.Encomienda_VentaSucursal_GeneralOrigen_ddlMes_SQL(anio,tipo), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_Mes = new DataTable();
                    dt_Mes.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_Mes = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_Mes;
        }
        //GIAMPIERE 23/ 08 / 2019
        public DataTable Encomienda_VentaSucursal_GeneralOrigen_ddlAño_Dao(String mes, String tipo)
        {
            DataTable dt_Mes = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(EncomiendaSQL.Encomienda_VentaSucursal_GeneralOrigen_ddlAño_SQL(mes, tipo), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_Mes = new DataTable();
                    dt_Mes.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_Mes = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_Mes;
        }
        //GIAMPIERE 23/ 08 / 2019
        public DataTable Encomienda_VentaSucursal_GeneralOrigen_ddlTipo_Dao(String anio, String mes)
        {
            DataTable dt_Tipo = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(EncomiendaSQL.Encomienda_VentaSucursal_GeneralOrigen_ddlTipo_SQL(anio,mes), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_Tipo = new DataTable();
                    dt_Tipo.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_Tipo = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_Tipo;
        }
        //GIAMPIERE 26/ 08 / 2019
        //General Mensual por ORIGEN
        public DataTable G_Encomienda_VentaSucursal_GeneralMensualOrigen_Dao(String anio, String mes, String tipo)
        {
            DataTable dt = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(EncomiendaSQL.G_Encomienda_VentaSucursal_GeneralMensualOrigen_SQL(anio, mes, tipo), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt = new DataTable();
                    dt.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt;
        }
        //GIAMPIERE 26/ 08 / 2019
        public DataTable Encomienda_VentaSucursal_GeneralMensualOrigen_ddlMes_Dao(String anio, String tipo)
        {
            DataTable dt_Mes = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(EncomiendaSQL.Encomienda_VentaSucursal_GeneralMensualOrigen_ddlMes_SQL(anio, tipo), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_Mes = new DataTable();
                    dt_Mes.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_Mes = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_Mes;
        }
        //GIAMPIERE 26/ 08 / 2019
        public DataTable Encomienda_VentaSucursal_GeneralMensualOrigen_ddlAño_Dao(String mes, String tipo)
        {
            DataTable dt_Año = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(EncomiendaSQL.Encomienda_VentaSucursal_GeneralMensualOrigen_ddlAño_SQL(mes, tipo), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_Año = new DataTable();
                    dt_Año.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_Año = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_Año;
        }
        //GIAMPIERE 26/ 08 / 2019
        public DataTable Encomienda_VentaSucursal_GeneralMensualOrigen_ddlTipo_Dao(String mes, String anio)
        {
            DataTable dt_Tipo = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(EncomiendaSQL.Encomienda_VentaSucursal_GeneralMensualOrigen_ddlTipo_SQL(mes, anio), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_Tipo = new DataTable();
                    dt_Tipo.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_Tipo = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_Tipo;
        }

        #endregion

        #region Clientes Top
        //GIAMPIERE 10/ 09 / 2019
        public DataTable G_Encomienda_ClientesTop_CantidaEncomiendas_Dao(String anio, String mes, String operacion)
        {
            DataTable dt = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(EncomiendaSQL.G_Encomienda_ClientesTop_CantidaEncomiendas_SQL(anio, mes, operacion), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt = new DataTable();
                    dt.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt;
        }
        //GIAMPIERE 10/ 09 / 2019
        public DataTable Encomienda_ClientesTop_CantidaEncomiendas_AgrupacionTrimestre_Dao(String anio, String operacion)
        {
            DataTable dt = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(EncomiendaSQL.Encomienda_ClientesTop_CantidaEncomiendas_AgrupacionTrimestre_SQL(anio, operacion), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt = new DataTable();
                    dt.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt;
        }
        //GIAMPIERE 10/ 09 / 2019
        public DataTable Encomienda_ClientesTop_CantidaEncomiendas_AgrupacionBimestre_Dao(String anio, String operacion)
        {
            DataTable dt = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(EncomiendaSQL.Encomienda_ClientesTop_CantidaEncomiendas_AgrupacionBimestre_SQL(anio, operacion), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt = new DataTable();
                    dt.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt;
        }
        //GIAMPIERE 10/ 09 / 2019
        public DataTable Encomienda_ClientesTop_CantidaEncomiendas_ddlMes_Dao(String anio, String operacion)
        {
            DataTable dt_Mes = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(EncomiendaSQL.Encomienda_ClientesTop_CantidaEncomiendas_ddlMes_SQL(anio, operacion), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_Mes = new DataTable();
                    dt_Mes.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_Mes = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_Mes;
        }
        //GIAMPIERE 10/ 09 / 2019
        public DataTable Encomienda_ClientesTop_CantidaEncomiendas_ddlAño_Dao(String mes, String operacion)
        {
            DataTable dt_Año = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(EncomiendaSQL.Encomienda_ClientesTop_CantidaEncomiendas_ddlAño_SQL(mes, operacion), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_Año = new DataTable();
                    dt_Año.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_Año = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_Año;
        }
        //GIAMPIERE 10/ 09 / 2019
        public DataTable Encomienda_ClientesTop_CantidaEncomiendas_ddlOperacion_Dao(String mes, String anio)
        {
            DataTable dt_Oeracion = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(EncomiendaSQL.Encomienda_ClientesTop_CantidaEncomiendas_ddlOperacion_SQL(mes, anio), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_Oeracion = new DataTable();
                    dt_Oeracion.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_Oeracion = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_Oeracion;
        }

        //Segun Rentabilidad
        //GIAMPIERE 11/ 09 / 2019
        public DataTable G_Encomienda_ClientesTop_Rentabilidad_Dao(String anio, String mes, String operacion)
        {
            DataTable dt = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(EncomiendaSQL.G_Encomienda_ClientesTop_Rentabilidad_SQL(anio, mes, operacion), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt = new DataTable();
                    dt.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt;
        }
        //GIAMPIERE 11/ 09 / 2019
        public DataTable Encomienda_ClientesTop_Rentabilidad_AgrupacionTrimestre_Dao(String anio, String operacion)
        {
            DataTable dt = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(EncomiendaSQL.Encomienda_ClientesTop_Rentabilidad_AgrupacionTrimestre_SQL(anio, operacion), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt = new DataTable();
                    dt.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt;
        }
        //GIAMPIERE 11/ 09 / 2019
        public DataTable Encomienda_ClientesTop_Rentabilidad_AgrupacionBimestre_Dao(String anio, String operacion)
        {
            DataTable dt = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(EncomiendaSQL.Encomienda_ClientesTop_Rentabilidad_AgrupacionBimestre_SQL(anio, operacion), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt = new DataTable();
                    dt.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt;
        }
        //GIAMPIERE 11/ 09 / 2019
        public DataTable Encomienda_ClientesTop_Rentabilidad_ddlMes_Dao(String anio, String operacion)
        {
            DataTable dt_Mes = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(EncomiendaSQL.Encomienda_ClientesTop_Rentabilidad_ddlMes_SQL(anio, operacion), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_Mes = new DataTable();
                    dt_Mes.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_Mes = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_Mes;
        }
        //GIAMPIERE 11/ 09 / 2019
        public DataTable Encomienda_ClientesTop_Rentabilidad_ddlAño_Dao(String mes, String operacion)
        {
            DataTable dt_Año = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(EncomiendaSQL.Encomienda_ClientesTop_Rentabilidad_ddlAño_SQL(mes, operacion), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_Año = new DataTable();
                    dt_Año.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_Año = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_Año;
        }
        //GIAMPIERE 11/ 09 / 2019
        public DataTable Encomienda_ClientesTop_Rentabilidad_ddlOperacion_Dao(String mes, String anio)
        {
            DataTable dt_Oeracion = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(EncomiendaSQL.Encomienda_ClientesTop_Rentabilidad_ddlOperacion_SQL(mes, anio), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_Oeracion = new DataTable();
                    dt_Oeracion.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_Oeracion = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_Oeracion;
        }



        #endregion

        #region SUBREGION_RUTAVEHICULOS
        /* 
            DESARROLLADOR: JORGE BALTODANO
            KPI: ENCOMIENDAS
        */
        // ##### GRAFICA: CANTIDAD VIAJE SEGUN RUTA POR TIEMPO ######

        public DataTable Grafica_Encomiendas_RutasVehiculos_GeneralRutaTiempo_DAO(String anio, String mes, String tipo)
        {
            DataTable dt_Grafica_GRT = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(EncomiendaSQL.Grafica_Encomiendas_RutasVehiculos_GeneralRutaTiempo_SQL(anio, mes, tipo), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_Grafica_GRT = new DataTable();
                    dt_Grafica_GRT.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_Grafica_GRT = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_Grafica_GRT;
        }

         public DataTable Grafica_Encomienda_RutasVehiculos_GeneralRutaTiempo_AgrupacionTrimestre_DAO(String anio, String tipo)
        {
            DataTable dt = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(EncomiendaSQL.Grafica_Encomiendas_RutasVehiculos_GeneralRutaTiempo_AgrupacionTrimestre_SQL(anio,tipo), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt = new DataTable();
                    dt.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt;
        }

         public DataTable Grafica_Encomienda_RutaVehiculos_GeneralRutaTiempo_AgrupacionBimestre_DAO(String anio, String tipo)
        {
            DataTable dt = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(EncomiendaSQL.Grafica_Encomiendas_RutasVehiculos_GeneralRutaTiempo_AgrupacionBimestre_SQL(anio,tipo), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt = new DataTable();
                    dt.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt;
        }




        //Metodos para cargar los combos
        public DataTable Encomiendas_RutasVehiculos_ListarAño_GRT_DAO(String mes, String tipo)
        {
            DataTable dt_GTR_Año = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(EncomiendaSQL.Encomiendas_RutasVehiculos_ListarAño_GRT_SQL(mes, tipo), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_GTR_Año = new DataTable();
                    dt_GTR_Año.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_GTR_Año = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_GTR_Año;
        }

        public DataTable Encomiendas_RutasVehiculos_ListarMes_GRT_DAO(String anio, String tipo)
        {
            DataTable dt_GTR_Mes = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(EncomiendaSQL.Encomiendas_RutasVehiculos_ListarMes_GRT_SQL(anio, tipo), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_GTR_Mes = new DataTable();
                    dt_GTR_Mes.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_GTR_Mes = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_GTR_Mes;
        }

        public DataTable Encomiendas_RutasVehiculos_ListarTipo_GRT_DAO(String anio, String mes)
        {
            DataTable dt_GTR_Tipo = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(EncomiendaSQL.Encomiendas_RutaVehiculos_ListarTipo_GTR_SQL(anio, mes), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_GTR_Tipo = new DataTable();
                    dt_GTR_Tipo.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_GTR_Tipo = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_GTR_Tipo;
        }
        
        
        // GRAFICA: CANTIDAD VIAJE SEGUN RUTA POR TIPO VEHICULO
        public DataTable Grafica_Encomiendas_RutasVehiculos_GeneralTipoVehiculo_DAO(String Año, String Mes, String Trimestre, String Bimestre)
        {
            DataTable dt_Grafica_GTV = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(EncomiendaSQL.Grafica_Encomiendas_RutasVehiculos_GeneralTipoVehiculo_SQL(Año, Mes, Trimestre, Bimestre), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_Grafica_GTV = new DataTable();
                    dt_Grafica_GTV.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_Grafica_GTV = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_Grafica_GTV;
        }


        // Listar Año - General por Tipo de Vehiculo
        public DataTable Encomiendas_RutasVehiculos_ListarAño_GTV_DAO(String Mes, String Trimestre, String Bimestre)
        {
            DataTable dt_Año = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(EncomiendaSQL.Encomiendas_RutasVehiculos_ListarAño_GTV_SQL(Mes, Trimestre, Bimestre), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_Año = new DataTable();
                    dt_Año.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_Año = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_Año;
        }

        //Listar Mes - General por Tipo de Vehiculo
        public DataTable Encomiendas_RutasVehiculos_ListarMes_GTV_DAO(String Año)
        {
            DataTable dt_Mes = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(EncomiendaSQL.Encomiendas_RutasVehiculos_ListarMes_GTV_SQL(Año), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_Mes = new DataTable();
                    dt_Mes.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_Mes = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_Mes;
        }

        //Listar Bimestre - General por Tipo de Vehiculo
        public DataTable Encomiendas_RutasVehiculos_ListarBimestre_GTV_DAO(String Año)
        {

            DataTable dt_Bimestre = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(EncomiendaSQL.Encomiendas_RutasVehiculos_ListarBimestre_GTV_SQL(Año), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_Bimestre = new DataTable();
                    dt_Bimestre.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_Bimestre = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_Bimestre;
        }

        //Listar Bimestre - General por Tipo de Vehiculo
        public DataTable Encomiendas_RutasVehiculos_ListarTrimestre_GTV_DAO(String Año)
        {

            DataTable dt_Trimestre = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(EncomiendaSQL.Encomiendas_RutasVehiculos_ListarTrimestre_GTV_SQL(Año), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_Trimestre = new DataTable();
                    dt_Trimestre.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_Trimestre = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_Trimestre;
        }


        
        // GRAFICA: CANTIDAD VIAJE SEGUN RUTA 

        public DataTable Grafica_Encomiendas_RutaVehiculos_CantidadViajesRuta_DAO(String Año, String Mes)
        {

            DataTable dt_Grafica = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(EncomiendaSQL.Grafica_Encomiendas_RutaVehiculos_CantidadViajesRuta_SQL(Año,Mes), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_Grafica = new DataTable();
                    dt_Grafica.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_Grafica = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_Grafica;
        }

         public DataTable Encomiendas_RutaVehiculos_ListarMes_CVR_DAO(String Año)
        {

            DataTable dt_Mes = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(EncomiendaSQL.Encomiendas_RutaVehiculos_ListarMes_CVR_SQL(Año), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_Mes = new DataTable();
                    dt_Mes.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_Mes = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_Mes;
        }

          public DataTable Encomiendas_RutaVehiculos_ListarAño_CVR_DAO(String Mes)
        {

            DataTable dt_Año = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(EncomiendaSQL.Encomiendas_RutaVehiculos_ListarAño_CVR_SQL(Mes), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt_Año = new DataTable();
                    dt_Año.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt_Año = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt_Año;
        }

        #endregion


    }
}
