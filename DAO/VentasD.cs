﻿using ENTIDAD.Base.Area;
using System;
using System.Data;
using System.Data.SqlClient;

namespace DATO
{
    public class VentasD
    {

        VentaSQL consulta_sql = new VentaSQL();

        #region singleton
        private static readonly VentasD _instancia = new VentasD();
        public static VentasD Instancia
        {
            get { return VentasD._instancia; }
        }
        #endregion

        #region Metodos Mantenimiento

        //Informes de Venta:
        public DataTable Listar_Volumnen_Ventas_Anuales()
        {
            DataTable dt = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(consulta_sql.Volumen_Ventas_Anuales, objconnection);
                //cmd = new SqlCommand(consulta_sql.Disponibilidad_Flotas(), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt = new DataTable();
                    dt.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt;
        }

        public DataTable Listar_Comparativo_Historico()
        {
            DataTable dt = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(consulta_sql.Comparativo_Historico, objconnection);
                //cmd = new SqlCommand(consulta_sql.Disponibilidad_Flotas(), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt = new DataTable();
                    dt.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt;
        }

        public DataTable Listar_Mayor_Crecimiento_Menual()
        {
            DataTable dt = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(consulta_sql.Rank_MCM, objconnection);
                //cmd = new SqlCommand(consulta_sql.Disponibilidad_Flotas(), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt = new DataTable();
                    dt.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt;
        }

        public DataTable Listar_Menor_Crecimiento_Menual()
        {
            DataTable dt = null;
            SqlCommand cmd = null;
            SqlConnection objconnection = null;
            SqlDataReader dr = null;
            try
            {
                objconnection = Conexion.Conectar();
                cmd = new SqlCommand(consulta_sql.Rank_NCM, objconnection);
                //cmd = new SqlCommand(consulta_sql.Disponibilidad_Flotas(), objconnection);
                cmd.CommandType = CommandType.Text;
                dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dt = new DataTable();
                    dt.Load(dr);
                }
            }
            catch (Exception ex)
            {
                dt = null;
            }
            finally
            {
                objconnection.Close();
            }
            return dt;
        }

        #endregion

    }
}
