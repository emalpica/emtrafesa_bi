﻿using System;

namespace ENTIDAD
{
    public class TipoUsuario
    {
        //Tipo: 1-Super administrador  2-Administrador  3-Gestion 4-Control
        public int idTipoUsuario { get; set; }
        public String nombre { get; set; }
    }
}
