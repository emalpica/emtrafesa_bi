﻿using System;

namespace ENTIDAD.Base
{
    public class Trimeste
    {
        //TRIMESTRES:
        //holi A2
        //T1:Enero-Febrero-Marzo   T2:Abril-Mayo-Junio   T3:Julio-Agosto-Septiembre   T4:Octubre-Noviembre-Diciembre 
        public int idTrimestre { get; set; }
        public String M3 { get; set; }
        public String M6 { get; set; }
        public String M9 { get; set; }
        public String M12 { get; set; }
        public String estado { get; set; }

        public Trimeste()
        {
            M3 = "Trimestre 1";
            M6 = "Trimestre 2";
            M9 = "Trimestre 3";
            M12 = "Trimestre 4";
        }

        //public List<String> Fomato_Meses(List<String> lista)
        //{ 
        //    List<Regex> lst_Regex = new List<Regex>(lista);

        //    for(int lst = 0;  lst in lst_Regex)
        //    {
        //        var resutl = lst_Regex[lst].Match("January");
        //        //if (lst_Regex[lst].Match("January"))
        //        //{  }
        //    }
        //        return lista;
        //}

    }
}
