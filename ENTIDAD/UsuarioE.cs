﻿using System;

namespace ENTIDAD
{
    public class UsuarioE
    {
        public int idUsuario { get; set; }
        public String nombres { get; set; }
        public String apellidos { get; set; }
        public String usuario { get; set; }
        public String contrasena { get; set; }
        public int estado { get; set; }
        public TipoUsuario tipo { get; set; }
        public UsuarioE()
        {
            nombres = "Gerencia Emtrafesa";
            usuario = "gerencia";
            contrasena = "Emtrafesa123";
            tipo.idTipoUsuario = 2;

        }
    }


}
