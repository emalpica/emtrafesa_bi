﻿using System;

namespace ENTIDAD.Base.Area
{
    public class PersonalSQL
    {
        String consulta;

        //GIAMPIERE PARIMANGO
        #region Cantidad de Colaboradores
        public string G_Personal_CantidadColaboradores_cantidadGeneral_SQL =

       " declare @Mes date, @primerDiaMes date, @ultimaDiaMes date  " +
         " IF(getdate()= (select EOMONTH(getdate()) as UltDiaMesActual)) " +
         " begin set @Mes=	(SELECT CONVERT (date, GETDATE()))	 end ELSE begin " +
         " set @Mes= (select EOMONTH(getdate(),-1) as UltDiaMesAnterior)  end " +
         " set @primerDiaMes= dateadd(d,1, EOMONTH(@Mes,-1)) " +
         " set @ultimaDiaMes = EOMONTH(@Mes) " +
         " select sum(Cantidad) as Cantidad  from [Personal].[DTM_Personal_Colaboradores] " +
         " where Fecha between @primerDiaMes and @ultimaDiaMes  and  KeyPersonalColaboradores  in(select max(KeyPersonalHoraExtra) " +
         " from [Personal].[DTM_Personal_Horas_Extra] Where Fecha between @primerDiaMes and @ultimaDiaMes " +
         " group by Mes,Sucursal ,Departamento,Area,SubArea) ";
        public String G_Personal_CantidadColaboradores_CantidadArea_SQL(String area, String subArea, String sucursal)
        {        //GIAMPIERE 20/ 09 / 2019
            consulta =
            " declare  @Area nvarchar(max),@mesActual varchar(max),@mes varchar(max),@anio varchar(max), @SubArea nvarchar(max), @Sucursal nvarchar(max),@consulta nvarchar(max), @filtro nvarchar(max) " +
             " set @Area='" + area + "';set @SubArea='" + subArea + "'; set @Sucursal= '" + sucursal + "';set @consulta=''; set @filtro= ''; " +
             " IF(getdate() = (select EOMONTH(getdate()))) begin set @mesActual = (SELECT CONVERT(date, GETDATE())) end ELSe begin set @mesActual = (select EOMONTH(getdate(), -1) ) end set @mes = MONTH(@mesActual); set @anio = YEAR(@mesActual); " +
             " if ( @Area != '' and @SubArea = '' and @Sucursal = '')  set @filtro= ' Where  Mes='''+@mes+''' and Anual='''+@anio+''' and Area='''+@Area+''' ' " +
             " if ( @Area != '' and @SubArea = '' and @Sucursal != '') set @filtro= '  Where  Mes='''+@mes+''' and Anual='''+@anio+''' and Area='''+@Area+'''  and Sucursal='''+@Sucursal+''' ' " +
             " if ( @Area != '' and @SubArea != '' and @Sucursal = '') set @filtro= '  Where  Mes='''+@mes+''' and Anual='''+@anio+'''  and Area='''+@Area+'''  and SubArea='''+@SubArea+''' ' " +
             " if ( @Area != '' and @SubArea != '' and @Sucursal != '') set @filtro= '  Where  Mes='''+@mes+''' and Anual='''+@anio+''' and Area='''+@Area+'''  and SubArea='''+@SubArea+''' and Sucursal='''+@Sucursal+''' ' " +
             " if ( @Area = '' and @SubArea = '' and @Sucursal = '') set @filtro= '  Where  Mes='''+@mes+''' and Anual='''+@anio+'''   ' " +
             " if ( @Area = '' and @SubArea = '' and @Sucursal != '') set @filtro= '  Where  Mes='''+@mes+''' and Anual='''+@anio+''' and Sucursal='''+@Sucursal+''' ' " +
             " if ( @Area = '' and @SubArea != '' and @Sucursal = '') set @filtro= '  Where  Mes='''+@mes+''' and Anual='''+@anio+''' and SubArea='''+@SubArea+''' ' " +
             " if ( @Area = '' and @SubArea != '' and @Sucursal != '') set @filtro= '  Where  Mes='''+@mes+''' and Anual='''+@anio+''' and SubArea='''+@SubArea+''' and Sucursal='''+@Sucursal+''' ' " +
             " set @consulta=' select Departamento, sum(Cantidad) as Cantidad  from [Personal].[DTM_Personal_Colaboradores] '+@filtro+'  and KeyPersonalColaboradores in(select max(KeyPersonalColaboradores) from [Personal].[DTM_Personal_Colaboradores] '+@filtro+'  group by Sucursal ,Departamento,Area,SubArea) group by Departamento' " +
             " execute(@consulta)";
            return consulta;
        }

        public String Personal_CantidadColaboradores_CantidadArea_ddlSucursal_SQL(String area, String subArea)
        {        //GIAMPIERE 20/ 09 / 2019
            consulta =
        " declare  @Area nvarchar(max), @SubArea nvarchar(max) " +
        " set @Area='" + area + "';set @SubArea='" + subArea + "'; " +
        " if ( @Area = '' and @SubArea = '' ) Select distinct Sucursal  from [Personal].[DTM_Personal_Colaboradores]" +
        " if ( @Area != '' and @SubArea = '' ) Select distinct Sucursal  from [Personal].[DTM_Personal_Colaboradores] Where   Area=@Area " +
        " if ( @Area = '' and @SubArea != '' ) Select distinct Sucursal  from [Personal].[DTM_Personal_Colaboradores] Where SubArea=@SubArea" +
        " if ( @Area != '' and @SubArea != '') Select distinct Sucursal  from [Personal].[DTM_Personal_Colaboradores] Where Area=@Area and SubArea=@SubArea";

            return consulta;
        }

        public String Personal_CantidadColaboradores_CantidadArea_ddlArea_SQL(String sucursal, String subArea)
        {        //GIAMPIERE 20/ 09 / 2019
            consulta =
         " declare  @Sucursal nvarchar(max), @SubArea nvarchar(max)" +
         " set @SubArea='" + subArea + "'; set @Sucursal= '" + sucursal + "';" +
         " if (@SubArea = '' and @Sucursal = '')  Select distinct Area  from [Personal].[DTM_Personal_Colaboradores]" +
         " if (@SubArea = '' and @Sucursal != '')  Select distinct Area  from [Personal].[DTM_Personal_Colaboradores] where Sucursal=@Sucursal" +
         " if (@SubArea != '' and @Sucursal = '') Select distinct Area  from [Personal].[DTM_Personal_Colaboradores] where SubArea=@SubArea" +
         " if (@SubArea != '' and @Sucursal != '') Select distinct Area  from [Personal].[DTM_Personal_Colaboradores] where Sucursal=@Sucursal and SubArea=@SubArea";

            return consulta;
        }
        public String Personal_CantidadColaboradores_CantidadArea_ddlSubArea_SQL(String sucursal, String area)
        {        //GIAMPIERE 20/ 09 / 2019
            consulta =
         "  declare  @Sucursal nvarchar(max), @Area  nvarchar(max)" +
          " set @Area='" + area + "'; set @Sucursal= '" + sucursal + "';" +
        "  if (@Area  = '' and @Sucursal = '')  Select distinct SubArea  from [Personal].[DTM_Personal_Colaboradores]" +
        "  if (@Area  = '' and @Sucursal != '')  Select distinct SubArea  from [Personal].[DTM_Personal_Colaboradores] where Sucursal=@Sucursal" +
        "  if (@Area  != '' and @Sucursal = '') Select distinct SubArea  from [Personal].[DTM_Personal_Colaboradores] where Area=@Area " +
        "  if (@Area  != '' and @Sucursal != '') Select distinct SubArea  from [Personal].[DTM_Personal_Colaboradores] where Sucursal=@Sucursal and Area=@Area ";
            return consulta;
        }
        //EVOLUCIÓN GENERAL MENSUAL
        //GIAMPIERE 12/ 08 / 2019
        public String G_Personal_CantidadColaboradores_EvolucionGeneralMensual_SQL(String mes, String departamento, String area, String subArea, String sucursal)
        {
            consulta =
          " declare @Departamento varchar(20), @Area nvarchar(max), @Mes nvarchar(3),@SubArea nvarchar(max),@Sucursal nvarchar(max), @filtro  NVARCHAR(MAX), @cols NVARCHAR(MAX), @null NVARCHAR(MAX), @consulta NVARCHAR(MAX); " +
          " set @Departamento= '" + departamento + "'; set @Area='" + area + "';set @SubArea='" + subArea + "'; set @Sucursal= '" + sucursal + "'; set @Mes='" + mes + "'; set @filtro =''; set @consulta =''; " +
          " select @cols = STUFF((SELECT Distinct ',' + QUOTENAME(Anual)  from [Personal].[DTM_Personal_Colaboradores] group by ',' + QUOTENAME(Anual)Order by ',' + QUOTENAME(Anual) FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)')  ,1,1,'') " +
          " select @null = STUFF((SELECT Distinct ', isnull(' + QUOTENAME(Anual)+',0) as'  + QUOTENAME(Anual) from [Personal].[DTM_Personal_Colaboradores]  group by ', isnull(' + QUOTENAME(Anual)+',0) as'  + QUOTENAME(Anual) Order by ', isnull(' + QUOTENAME(Anual)+',0) as'  + QUOTENAME(Anual)   FOR XML PATH(''), TYPE  ).value('.', 'NVARCHAR(MAX)')   ,1,1,'') " +
          " if  (@Mes = '' and @Departamento = ''  and @Area = '' and @SubArea = '' and @Sucursal = ''  )  	  set @consulta= 'SELECT Meses,' + @null + '  from  ( select Mes, DATENAME(MONTH,Fecha) as Meses,Anual,Cantidad from [Personal].[DTM_Personal_Colaboradores] where  KeyPersonalColaboradores in(select max(KeyPersonalColaboradores) from [Personal].[DTM_Personal_Colaboradores]  group by  Mes ,Anual,Sucursal ,Departamento,Area,SubArea)   ) x  pivot ( sum(Cantidad) for Anual in (' + @cols + ') ) p  order by Mes' " +
          " else " +
          " begin " +
          " if  (@Mes != '' and @Departamento != '' and @Area != '' and @SubArea != '' and @Sucursal != '' )  	 set @filtro = ' Sucursal = '''+@Sucursal+''' and Departamento = '''+@Departamento+''' and Area = '''+@Area+''' and SubArea = '''+@SubArea+'''  and Mes = '''+@Mes+'''  ' " +
          " if  (@Mes != '' and @Departamento != ''  and @Area = '' and @SubArea = '' and @Sucursal = ''  )  	  set @filtro = ' Departamento = '''+@Departamento+'''  and Mes = '''+@Mes+''' ' " +
          " if  (@Mes != '' and @Departamento != ''  and @Area != '' and @SubArea  = '' and @Sucursal != '' )  	 set @filtro = ' Sucursal = '''+@Sucursal+''' and Departamento = '''+@Departamento+''' and Area='''+@Area+'''  and Mes = '''+@Mes+''' '" +
          " if  (@Mes != '' and @Departamento != '' and @Area != '' and @SubArea != '' and @Sucursal = '')  	 set @filtro = ' Departamento = '''+@Departamento+''' and Area = '''+@Area+''' and SubArea = '''+@SubArea+'''  and Mes = '''+@Mes+''' ' " +
          " if  (@Mes != '' and @Departamento != ''  and @Area = '' and @SubArea  != '' and @Sucursal != '' )  	  set @filtro = ' Sucursal = '''+@Sucursal+''' and Departamento = '''+@Departamento+''' and SubArea = '''+@SubArea+'''  and Mes = '''+@Mes+''' ' " +
          " if  (@Mes != '' and @Departamento != '' and @Area = '' and @SubArea = '' and @Sucursal != '')  	 set @filtro = ' Sucursal = '''+@Sucursal+''' and Departamento = '''+@Departamento+''' and Mes = '''+@Mes+'''   ' " +
          " if  (@Mes != '' and @Departamento != ''  and @Area = '' and @SubArea  != '' and @Sucursal = '' )  	  set @filtro = ' Departamento = '''+@Departamento+''' and SubArea = '''+@SubArea+'''  and Mes = '''+@Mes+'''  ' " +
          " if  (@Mes != '' and @Departamento != '' and @Area != '' and @SubArea = '' and @Sucursal = '')  	 set @filtro = '  Departamento = '''+@Departamento+''' and Area = '''+@Area+'''  and Mes = '''+@Mes+''' ' " +
          " if  (@Mes != '' and @Departamento = '' and @Area != '' and @SubArea != '' and @Sucursal != '' )  	 set @filtro = '  Sucursal = '''+@Sucursal+'''  and Area = '''+@Area+''' and SubArea = '''+@SubArea+'''  and Mes = '''+@Mes+''' ' " +
          " if  (@Mes != '' and @Departamento = ''  and @Area = '' and @SubArea = '' and @Sucursal = ''  )  	  set @filtro = ' Mes = '''+@Mes+''' ' " +
          " if  (@Mes != '' and @Departamento = ''  and @Area != '' and @SubArea  = '' and @Sucursal != '' )  	 set @filtro = ' Sucursal = '''+@Sucursal+''' and Area='''+@Area+''' and Mes = '''+@Mes+'''  ' " +
          " if  (@Mes != '' and @Departamento = '' and @Area != '' and @SubArea != '' and @Sucursal = '')  	 set @filtro = ' Area = '''+@Area+''' and SubArea = '''+@SubArea+'''  and Mes = '''+@Mes+'''   ' " +
          " if  (@Mes != '' and @Departamento = ''  and @Area = '' and @SubArea  != '' and @Sucursal != '' )  	  set @filtro = '  Sucursal = '''+@Sucursal+'''  and SubArea = '''+@SubArea+'''  and Mes = '''+@Mes+''' ' " +
          " if  (@Mes != '' and @Departamento = '' and @Area = '' and @SubArea = '' and @Sucursal != '')  	 set @filtro = ' Sucursal = '''+@Sucursal+'''  and Mes = '''+@Mes+'''  ' " +
          " if  (@Mes != '' and @Departamento = ''  and @Area = '' and @SubArea  != '' and @Sucursal = '' )  	  set @filtro = '  SubArea = '''+@SubArea+'''  and Mes = '''+@Mes+''' ' " +
          " if  (@Mes != '' and @Departamento = '' and @Area != '' and @SubArea = '' and @Sucursal = '')  	 set @filtro = ' Area = '''+@Area+'''  and Mes = '''+@Mes+''' ' " +
          " if  (@Mes = '' and @Departamento != '' and @Area != '' and @SubArea != '' and @Sucursal != '' )  	 set @filtro = '  Sucursal = '''+@Sucursal+''' and Departamento = '''+@Departamento+''' and Area = '''+@Area+''' and SubArea = '''+@SubArea+''' ' " +
          " if  (@Mes = '' and @Departamento != ''  and @Area = '' and @SubArea = '' and @Sucursal = ''  )  	  set @filtro = '  Departamento = '''+@Departamento+'''  ' " +
          " if  (@Mes = '' and @Departamento != ''  and @Area != '' and @SubArea  = '' and @Sucursal != '' )  	 set @filtro = '  Sucursal = '''+@Sucursal+''' and Departamento = '''+@Departamento+''' and Area='''+@Area+'''   ' " +
          " if  (@Mes = '' and @Departamento != '' and @Area != '' and @SubArea != '' and @Sucursal = '')  	 set @filtro = '  Departamento = '''+@Departamento+''' and Area = '''+@Area+''' and SubArea = '''+@SubArea+'''   ' " +
          " if  (@Mes = '' and @Departamento != ''  and @Area = '' and @SubArea  != '' and @Sucursal != '' )  	  set @filtro = '  Sucursal = '''+@Sucursal+''' and Departamento = '''+@Departamento+''' and SubArea = '''+@SubArea+'''  ' " +
          " if  (@Mes = '' and @Departamento != '' and @Area = '' and @SubArea = '' and @Sucursal != '')  	 set @filtro = ' Sucursal = '''+@Sucursal+''' and Departamento = '''+@Departamento+'''' " +
          " if  (@Mes = '' and @Departamento != ''  and @Area = '' and @SubArea  != '' and @Sucursal = '' )  	  set @filtro = '  Departamento = '''+@Departamento+''' and SubArea = '''+@SubArea+'''' " +
          " if  (@Mes = '' and @Departamento != '' and @Area != '' and @SubArea = '' and @Sucursal = '')  	 set @filtro = '  Departamento = '''+@Departamento+''' and Area = '''+@Area+''' ' " +
          " if  (@Mes = '' and @Departamento = '' and @Area != '' and @SubArea != '' and @Sucursal != '' )  	 set @filtro = '  Sucursal = '''+@Sucursal+'''  and Area = '''+@Area+''' and SubArea = '''+@SubArea+'''  ' " +
          " if  (@Mes = '' and @Departamento = ''  and @Area != '' and @SubArea  = '' and @Sucursal != '' )  	 set @filtro = '  Sucursal = '''+@Sucursal+''' and Area='''+@Area+'''  '  " +
          " if  (@Mes = '' and @Departamento = '' and @Area != '' and @SubArea != '' and @Sucursal = '')  	 set @filtro = '  Area = '''+@Area+''' and SubArea = '''+@SubArea+''' '  " +
          " if  (@Mes = '' and @Departamento = ''  and @Area = '' and @SubArea  != '' and @Sucursal != '' )  	  set @filtro = '  Sucursal = '''+@Sucursal+'''  and SubArea = '''+@SubArea+'''  ' " +
          " if  (@Mes = '' and @Departamento = '' and @Area = '' and @SubArea = '' and @Sucursal != '')  	 set @filtro = ' Sucursal = '''+@Sucursal+''' ' " +
          " if  (@Mes = '' and @Departamento = ''  and @Area = '' and @SubArea  != '' and @Sucursal = '' )  	  set @filtro = '  SubArea = '''+@SubArea+''' ' " +
          " if  (@Mes = '' and @Departamento = '' and @Area != '' and @SubArea = '' and @Sucursal = '')  	 set @filtro = ' Area = '''+@Area+'''  ' " +
          " set @consulta= 'SELECT Meses,' + @null + '  from  ( select Mes, DATENAME(MONTH,Fecha) as Meses,Anual,Cantidad from [Personal].[DTM_Personal_Colaboradores] where '+@filtro+' and KeyPersonalColaboradores in(select max(KeyPersonalColaboradores) from [Personal].[DTM_Personal_Colaboradores] where '+@filtro+'  group by  Mes ,Anual,Sucursal ,Departamento,Area,SubArea)   ) x  pivot ( sum(Cantidad) for Anual in (' + @cols + ') ) p  order by Mes' " +
          " end " +
          " execute(@consulta) ";

            return consulta;
        }
        public String Personal_CantidadColaboradores_EvolucionGeneralMensual_dllSucursal_SQL(String mes, String departamento, String area, String subArea)
        {        //GIAMPIERE 12/ 08 / 2019
            consulta =
                " declare  @Area nvarchar(max),@SubArea nvarchar(max),@Departamento nvarchar(max),@Mes nvarchar(max),@Consulta nvarchar(max),@Filtro nvarchar(max),@select nvarchar(max);" +
          " set @Departamento= '" + departamento + "'; set @Area='" + area + "';set @SubArea='" + subArea + "'; set @Mes='" + mes + "';set @Consulta= ''; set @Filtro= ''; set @select= ''; " +
          " set @Consulta= N'select distinct Sucursal from[Personal].[DTM_Personal_Colaboradores]' " +
          " if (@Mes = ''  and @Area = '' and @SubArea = '' and @Departamento = ''  )  set @Filtro=  @Consulta " +
          " if (@Mes != ''  and @Area != '' and @SubArea != '' and @Departamento != '' )   set @Filtro =  @Consulta+ ' where Departamento = '''+@Departamento+''' and  Area = '''+@Area+''' and SubArea = '''+@SubArea+''' and Mes='''+@Mes+'''  '" +
          " if (@Mes != ''  and @Area != '' and @SubArea  = '' and @Departamento != '' ) set @Filtro =  @Consulta+' where Departamento = '''+@Departamento+''' and  Area = '''+@Area+'''  and Mes='''+@Mes+'''  ' " +
          " if (@Mes != ''  and @Area != '' and @SubArea != '' and @Departamento = '')  	set @Filtro =  @Consulta+' where Area = '''+@Area+''' and SubArea = '''+@SubArea+''' and Mes='''+@Mes+'''  ' " +
          " if (@Mes != ''  and @Area != '' and @SubArea = '' and @Departamento = '')  set @Filtro =  @Consulta+' where Area = '''+@Area+'''  and Mes='''+@Mes+''' ' " +
          " if (@Mes != ''  and @Area = '' and @SubArea = '' and @Departamento = ''  )  	  set @Filtro =  @Consulta+' where Mes='''+@Mes+'''  ' " +
          " if (@Mes != ''  and @Area = '' and @SubArea  != '' and @Departamento != '' )  	set @Filtro =  @Consulta+' where Departamento = '''+@Departamento+''' and SubArea = '''+@SubArea+''' and Mes='''+@Mes+''' ' " +
          " if (@Mes != ''  and @Area = '' and @SubArea = '' and @Departamento != '')  	set @Filtro =  @Consulta+' where Departamento = '''+@Departamento+''' and Mes='''+@Mes+'''  ' " +
          " if (@Mes != ''  and @Area = '' and @SubArea  != '' and @Departamento = '' )  	set @Filtro =  @Consulta+' where SubArea = '''+@SubArea+''' and Mes='''+@Mes+'''  ' " +
          " if (@Mes = ''  and @Area != '' and @SubArea != '' and @Departamento != '' )   set @Filtro =  @Consulta+' where Departamento = '''+@Departamento+''' and  Area = '''+@Area+''' and SubArea = '''+@SubArea+''' ' " +
          " if (@Mes = ''  and @Area != '' and @SubArea  = '' and @Departamento != '' ) set @Filtro =  @Consulta+' where Departamento = '''+@Departamento+''' and  Area = '''+@Area+'''   ' " +
          " if (@Mes = ''  and @Area != '' and @SubArea != '' and @Departamento = '')  	set @Filtro =  @Consulta+' where Area = '''+@Area+''' and SubArea = '''+@SubArea+'''  ' " +
          " if (@Mes = ''  and @Area != '' and @SubArea = '' and @Departamento = '')  set @Filtro =  @Consulta+' where Area = '''+@Area+'''  ' " +
          " if (@Mes = ''  and @Area = '' and @SubArea  != '' and @Departamento != '' )  	set @Filtro =  @Consulta+' where Departamento = '''+@Departamento+''' and SubArea = '''+@SubArea+'''  ' " +
          " if (@Mes = ''  and @Area = '' and @SubArea = '' and @Departamento != '')  	set @Filtro =  @Consulta+' where Departamento = '''+@Departamento+'''   ' " +
          " if (@Mes = ''  and @Area = '' and @SubArea  != '' and @Departamento = '' )  	set @Filtro =  @Consulta+' where SubArea = '''+@SubArea+'''  ' " +
          " set @Consulta=  @Filtro +' order by Sucursal' " +
          " execute(@Consulta)";
            return consulta;
        }
        public String Personal_CantidadColaboradores_EvolucionGeneralMensual_dllDepartamento_SQL(String mes, String area, String subArea, String sucursal)
        {        //GIAMPIERE 12/ 08 / 2019
            consulta =
                " declare @Area nvarchar(max),@SubArea nvarchar(max),@Sucursal nvarchar(max),@Mes nvarchar(max),@Consulta nvarchar(max),@Filtro nvarchar(max),@select nvarchar(max);" +
          " set @Area='" + area + "';set @SubArea='" + subArea + "'; set @Sucursal= '" + sucursal + "'; set @Mes='" + mes + "'; set @Consulta= ''; set @Filtro= ''; set @select= ''; " +
          " set @Consulta= N'select distinct Departamento from[Personal].[DTM_Personal_Colaboradores]'" +
          " if (@Mes = ''  and @Area = '' and @SubArea = '' and @Sucursal = ''  )  set @Filtro=  @Consulta " +
          " if (@Mes != ''  and @Area != '' and @SubArea != '' and @Sucursal != '' )   set @Filtro =  @Consulta+ ' where Sucursal = '''+@Sucursal+''' and  Area = '''+@Area+''' and SubArea = '''+@SubArea+''' and Mes='''+@Mes+'''  ' " +
          " if (@Mes != ''  and @Area != '' and @SubArea  = '' and @Sucursal != '' ) set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and  Area = '''+@Area+'''  and Mes='''+@Mes+'''  '" +
          " if (@Mes != ''  and @Area != '' and @SubArea != '' and @Sucursal = '')  	set @Filtro =  @Consulta+' where Area = '''+@Area+''' and SubArea = '''+@SubArea+''' and Mes='''+@Mes+'''  ' " +
          " if (@Mes != ''  and @Area != '' and @SubArea = '' and @Sucursal = '')  set @Filtro =  @Consulta+' where Area = '''+@Area+'''  and Mes='''+@Mes+''' '  " +
          " if (@Mes != ''  and @Area = '' and @SubArea = '' and @Sucursal = ''  )  	  set @Filtro =  @Consulta+' where Mes='''+@Mes+'''  ' " +
          " if (@Mes != ''  and @Area = '' and @SubArea  != '' and @Sucursal != '' )  	set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and SubArea = '''+@SubArea+''' and Mes='''+@Mes+''' '  " +
          " if (@Mes != ''  and @Area = '' and @SubArea = '' and @Sucursal != '')  	set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and Mes='''+@Mes+'''  ' " +
          " if (@Mes != ''  and @Area = '' and @SubArea  != '' and @Sucursal = '' )  	set @Filtro =  @Consulta+' where SubArea = '''+@SubArea+''' and Mes='''+@Mes+'''  '" +
          " if (@Mes = ''  and @Area != '' and @SubArea != '' and @Sucursal != '' )   set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and  Area = '''+@Area+''' and SubArea = '''+@SubArea+''' ' " +
          " if (@Mes = ''  and @Area != '' and @SubArea  = '' and @Sucursal != '' ) set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and  Area = '''+@Area+'''   '" +
          " if (@Mes = ''  and @Area != '' and @SubArea != '' and @Sucursal = '')  	set @Filtro =  @Consulta+' where Area = '''+@Area+''' and SubArea = '''+@SubArea+'''  '" +
          " if (@Mes = ''  and @Area != '' and @SubArea = '' and @Sucursal = '')  set @Filtro =  @Consulta+' where Area = '''+@Area+'''  '" +
          " if (@Mes = ''  and @Area = '' and @SubArea  != '' and @Sucursal != '' )  	set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and SubArea = '''+@SubArea+'''  ' " +
          " if (@Mes = ''  and @Area = '' and @SubArea = '' and @Sucursal != '')  	set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+'''   ' " +
          " if (@Mes = ''  and @Area = '' and @SubArea  != '' and @Sucursal = '' )  	set @Filtro =  @Consulta+' where SubArea = '''+@SubArea+'''  '" +
          " set @Consulta=  @Filtro +' order by Departamento'" +
          " execute(@Consulta);";
            return consulta;
        }
        public String Personal_CantidadColaboradores_EvolucionGeneralMensual_ddlArea_SQL(String mes, String departamento, String subArea, String sucursal)
        {        //GIAMPIERE 12/ 08 / 2019
            consulta =
                 " declare  @SubArea nvarchar(max),@Departamento nvarchar(max),@Sucursal nvarchar(max),@Mes nvarchar(max),@Consulta nvarchar(max),@Filtro nvarchar(max),@select nvarchar(max);" +
          " set @Departamento= '" + departamento + "'; set @SubArea='" + subArea + "'; set @Sucursal= '" + sucursal + "'; set @Mes='" + mes + "'; set @Consulta= ''; set @Filtro= ''; set @select= ''; " +
          " set @Consulta= N'select distinct Area from[Personal].[DTM_Personal_Colaboradores]'" +
          " if (@Mes = ''  and @SubArea = '' and @Departamento = '' and @Sucursal = ''  )  set @Filtro=  @Consulta " +
          " if (@Mes != ''  and @SubArea != '' and @Departamento != '' and @Sucursal != '' )   set @Filtro =  @Consulta+ ' where Sucursal = '''+@Sucursal+''' and  SubArea = '''+@SubArea+''' and Departamento = '''+@Departamento+''' and Mes='''+@Mes+'''  ' " +
          " if (@Mes != ''  and @SubArea != '' and @Departamento  = '' and @Sucursal != '' ) set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and  SubArea = '''+@SubArea+'''  and Mes='''+@Mes+'''  '" +
          " if (@Mes != ''  and @SubArea != '' and @Departamento != '' and @Sucursal = '')  	set @Filtro =  @Consulta+' where SubArea = '''+@SubArea+''' and Departamento = '''+@Departamento+''' and Mes='''+@Mes+'''  ' " +
          " if (@Mes != ''  and @SubArea != '' and @Departamento = '' and @Sucursal = '')  set @Filtro =  @Consulta+' where SubArea = '''+@SubArea+'''  and Mes='''+@Mes+''' '  " +
          " if (@Mes != ''  and @SubArea = '' and @Departamento = '' and @Sucursal = ''  )  	  set @Filtro =  @Consulta+' where Mes='''+@Mes+'''  ' " +
          " if (@Mes != ''  and @SubArea = '' and @Departamento  != '' and @Sucursal != '' )  	set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and Departamento = '''+@Departamento+''' and Mes='''+@Mes+''' '  " +
          " if (@Mes != ''  and @SubArea = '' and @Departamento = '' and @Sucursal != '')  	set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and Mes='''+@Mes+'''  ' " +
          " if (@Mes != ''  and @SubArea = '' and @Departamento  != '' and @Sucursal = '' )  	set @Filtro =  @Consulta+' where Departamento = '''+@Departamento+''' and Mes='''+@Mes+'''  '" +
          " if (@Mes = ''  and @SubArea != '' and @Departamento != '' and @Sucursal != '' )   set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and  SubArea = '''+@SubArea+''' and Departamento = '''+@Departamento+''' ' " +
          " if (@Mes = ''  and @SubArea != '' and @Departamento  = '' and @Sucursal != '' ) set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and  SubArea = '''+@SubArea+'''   '" +
          " if (@Mes = ''  and @SubArea != '' and @Departamento != '' and @Sucursal = '')  	set @Filtro =  @Consulta+' where SubArea = '''+@SubArea+''' and Departamento = '''+@Departamento+'''  '" +
          " if (@Mes = ''  and @SubArea != '' and @Departamento = '' and @Sucursal = '')  set @Filtro =  @Consulta+' where SubArea = '''+@SubArea+'''  '" +
          " if (@Mes = ''  and @SubArea = '' and @Departamento  != '' and @Sucursal != '' )  	set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and Departamento = '''+@Departamento+'''  ' " +
          " if (@Mes = ''  and @SubArea = '' and @Departamento = '' and @Sucursal != '')  	set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+'''   ' " +
          " if (@Mes = ''  and @SubArea = '' and @Departamento  != '' and @Sucursal = '' )  	set @Filtro =  @Consulta+' where Departamento = '''+@Departamento+'''  '" +
          " set @Consulta=  @Filtro +' order by Area'" +
          " execute(@Consulta)";
            return consulta;
        }
        public String Personal_CantidadColaboradores_EvolucionGeneralMensual_ddlSubArea_SQL(String mes, String departamento, String area, String sucursal)
        {        //GIAMPIERE 12/ 08 / 2019
            consulta =
                " declare @Area nvarchar(max),@Departamento nvarchar(max),@Sucursal nvarchar(max),@Mes nvarchar(max),@Consulta nvarchar(max),@Filtro nvarchar(max),@select nvarchar(max);" +
         " set @Departamento= '" + departamento + "'; set @Area='" + area + "'; set @Sucursal= '" + sucursal + "'; set @Mes='" + mes + "'; set @Consulta= ''; set @Filtro= ''; set @select= ''; " +
          " set @Consulta= N'select distinct SubArea from[Personal].[DTM_Personal_Colaboradores]'" +
          " if (@Mes = ''  and @Area = '' and @Departamento = '' and @Sucursal = ''  )  set @Filtro=  @Consulta " +
          " if (@Mes != ''  and @Area != '' and @Departamento != '' and @Sucursal != '' )   set @Filtro =  @Consulta+ ' where Sucursal = '''+@Sucursal+''' and  Area = '''+@Area+''' and Departamento = '''+@Departamento+''' and Mes='''+@Mes+'''  ' " +
          " if (@Mes != ''  and @Area != '' and @Departamento  = '' and @Sucursal != '' ) set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and  Area = '''+@Area+'''  and Mes='''+@Mes+'''  '" +
          " if (@Mes != ''  and @Area != '' and @Departamento != '' and @Sucursal = '')  	set @Filtro =  @Consulta+' where Area = '''+@Area+''' and Departamento = '''+@Departamento+''' and Mes='''+@Mes+'''  ' " +
          " if (@Mes != ''  and @Area != '' and @Departamento = '' and @Sucursal = '')  set @Filtro =  @Consulta+' where Area = '''+@Area+'''  and Mes='''+@Mes+''' '  " +
          " if (@Mes != ''  and @Area = '' and @Departamento = '' and @Sucursal = ''  )  	  set @Filtro =  @Consulta+' where Mes='''+@Mes+'''  ' " +
          " if (@Mes != ''  and @Area = '' and @Departamento  != '' and @Sucursal != '' )  	set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and Departamento = '''+@Departamento+''' and Mes='''+@Mes+''' '  " +
          " if (@Mes != ''  and @Area = '' and @Departamento = '' and @Sucursal != '')  	set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and Mes='''+@Mes+'''  ' " +
          " if (@Mes != ''  and @Area = '' and @Departamento  != '' and @Sucursal = '' )  	set @Filtro =  @Consulta+' where Departamento = '''+@Departamento+''' and Mes='''+@Mes+'''  '" +
          " if (@Mes = ''  and @Area != '' and @Departamento != '' and @Sucursal != '' )   set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and  Area = '''+@Area+''' and Departamento = '''+@Departamento+''' ' " +
          " if (@Mes = ''  and @Area != '' and @Departamento  = '' and @Sucursal != '' ) set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and  Area = '''+@Area+'''   '" +
          " if (@Mes = ''  and @Area != '' and @Departamento != '' and @Sucursal = '')  	set @Filtro =  @Consulta+' where Area = '''+@Area+''' and Departamento = '''+@Departamento+'''  '" +
          " if (@Mes = ''  and @Area != '' and @Departamento = '' and @Sucursal = '')  set @Filtro =  @Consulta+' where Area = '''+@Area+'''  '" +
          " if (@Mes = ''  and @Area = '' and @Departamento  != '' and @Sucursal != '' )  	set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and Departamento = '''+@Departamento+'''  ' " +
          " if (@Mes = ''  and @Area = '' and @Departamento = '' and @Sucursal != '')  	set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+'''   ' " +
          " if (@Mes = ''  and @Area = '' and @Departamento  != '' and @Sucursal = '' )  	set @Filtro =  @Consulta+' where Departamento = '''+@Departamento+'''  '" +
          " set @Consulta=  @Filtro +' order by SubArea'" +
          " execute(@Consulta)";
            return consulta;
        }
        public String Personal_CantidadColaboradores_EvolucionGeneralMensual_ddlMes_SQL(String departamento, String area, String subArea, String sucursal)
        { //GIAMPIERE 12/ 08 / 2019
            consulta =
          " declare @SubArea nvarchar(max),@Departamento nvarchar(max),@Sucursal nvarchar(max),@Area nvarchar(max),@Consulta nvarchar(max),@Filtro nvarchar(max),@select nvarchar(max);" +
          " set @Departamento= '" + departamento + "'; set @Area='" + area + "';set @SubArea='" + subArea + "'; set @Sucursal= '" + sucursal + "'; set @Consulta= ''; set @Filtro= ''; set @select= ''; " +
          " set @Consulta= N'select distinct DATENAME(MONTH,Fecha) as Meses ,Mes from[Personal].[DTM_Personal_Colaboradores]'" +
          " if (@Area = ''  and @SubArea = '' and @Departamento = '' and @Sucursal = ''  )  set @Filtro=  @Consulta " +
          " if (@Area != ''  and @SubArea != '' and @Departamento != '' and @Sucursal != '' )   set @Filtro =  @Consulta+ ' where Sucursal = '''+@Sucursal+''' and  SubArea = '''+@SubArea+''' and Departamento = '''+@Departamento+''' and Area='''+@Area+'''  ' " +
          " if (@Area != ''  and @SubArea != '' and @Departamento  = '' and @Sucursal != '' ) set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and  SubArea = '''+@SubArea+'''  and Area='''+@Area+'''  '" +
          " if (@Area != ''  and @SubArea != '' and @Departamento != '' and @Sucursal = '')  	set @Filtro =  @Consulta+' where SubArea = '''+@SubArea+''' and Departamento = '''+@Departamento+''' and Area='''+@Area+'''  ' " +
          " if (@Area != ''  and @SubArea != '' and @Departamento = '' and @Sucursal = '')  set @Filtro =  @Consulta+' where SubArea = '''+@SubArea+'''  and Area='''+@Area+''' '  " +
          " if (@Area != ''  and @SubArea = '' and @Departamento = '' and @Sucursal = ''  )  	  set @Filtro =  @Consulta+' where Area='''+@Area+'''  ' " +
          " if (@Area != ''  and @SubArea = '' and @Departamento  != '' and @Sucursal != '' )  	set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and Departamento = '''+@Departamento+''' and Area='''+@Area+''' '  " +
          " if (@Area != ''  and @SubArea = '' and @Departamento = '' and @Sucursal != '')  	set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and Area='''+@Area+'''  ' " +
          " if (@Area != ''  and @SubArea = '' and @Departamento  != '' and @Sucursal = '' )  	set @Filtro =  @Consulta+' where Departamento = '''+@Departamento+''' and Area='''+@Area+'''  '" +
          " if (@Area = ''  and @SubArea != '' and @Departamento != '' and @Sucursal != '' )   set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and  SubArea = '''+@SubArea+''' and Departamento = '''+@Departamento+''' ' " +
          " if (@Area = ''  and @SubArea != '' and @Departamento  = '' and @Sucursal != '' ) set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and  SubArea = '''+@SubArea+'''   '" +
          " if (@Area = ''  and @SubArea != '' and @Departamento != '' and @Sucursal = '')  	set @Filtro =  @Consulta+' where SubArea = '''+@SubArea+''' and Departamento = '''+@Departamento+'''  '" +
          " if (@Area = ''  and @SubArea != '' and @Departamento = '' and @Sucursal = '')  set @Filtro =  @Consulta+' where SubArea = '''+@SubArea+'''  '" +
          " if (@Area = ''  and @SubArea = '' and @Departamento  != '' and @Sucursal != '' )  	set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and Departamento = '''+@Departamento+'''  ' " +
          " if (@Area = ''  and @SubArea = '' and @Departamento = '' and @Sucursal != '')  	set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+'''   ' " +
          " if (@Area = ''  and @SubArea = '' and @Departamento  != '' and @Sucursal = '' )  	set @Filtro =  @Consulta+' where Departamento = '''+@Departamento+'''  '" +
          " set @Consulta=  @Filtro +' order by Mes'" +
          " execute(@Consulta)";
            return consulta;
        }
        //EVOLUCIÓN POR ÁREA MENSUAL
        public String G_Personal_CantidadColaboradores_EvolucionAreaMensual_SQL(String anio, String mes, String area, String subArea, String sucursal)
        {        //GIAMPIERE 12/ 08 / 2019
            consulta =
                   " declare @Departamento varchar(max),@Año nvarchar(max), @Area nvarchar(max), @Mes nvarchar(3),@SubArea nvarchar(max),@Sucursal nvarchar(max), @filtro  NVARCHAR(MAX), @cols NVARCHAR(MAX), @null  NVARCHAR(MAX), @consulta  NVARCHAR(MAX); " +
            " set @Año='" + anio + "'; set @Area='" + area + "';set @SubArea='" + subArea + "'; set @Sucursal= '" + sucursal + "'; set @Mes='" + mes + "';set @consulta ='';set @filtro =''; " +
            " select @cols = STUFF((SELECT Distinct ',' + QUOTENAME(Departamento)  from  [Personal].[DTM_Personal_Colaboradores] group by ',' + QUOTENAME(Departamento)Order by ',' + QUOTENAME(Departamento) FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)')  ,1,1,'')  " +
            " select @null = STUFF((SELECT Distinct ', isnull(' + QUOTENAME(Departamento)+',0) as'  + QUOTENAME(Departamento) from  [Personal].[DTM_Personal_Colaboradores]  group by ', isnull(' + QUOTENAME(Departamento)+',0) as'  + QUOTENAME(Departamento) Order by ', isnull(' + QUOTENAME(Departamento)+',0) as'  + QUOTENAME(Departamento)   FOR XML PATH(''), TYPE  ).value('.', 'NVARCHAR(MAX)')   ,1,1,'')  " +
            " if  (@Mes = '' and @Año = ''  and @Area = '' and @SubArea = '' and @Sucursal = ''  )  	  set @consulta = 'SELECT Meses,' + @null + '  from  ( select Mes,DATENAME(MONTH,Fecha) as Meses, Departamento,Cantidad from [Personal].[DTM_Personal_Colaboradores]   where KeyPersonalColaboradores in(select max(KeyPersonalColaboradores) from [Personal].[DTM_Personal_Colaboradores] group by Mes ,Anual, Departamento,Area,SubArea,Sucursal) )  x  pivot ( sum(Cantidad) for Departamento in (' + @cols + ') ) p  order by Mes ' " +
            " else " +
            " begin " +
            " if  (@Mes != '' and @Año != '' and @Area != '' and @SubArea != '' and @Sucursal != '' )  	 set @filtro = ' Sucursal = '''+@Sucursal+''' and Anual = '''+@Año+''' and Area = '''+@Area+''' and SubArea = '''+@SubArea+'''  and Mes = '''+@Mes+''' ' " +
            " if  (@Mes != '' and @Año != ''  and @Area = '' and @SubArea = '' and @Sucursal = ''  )  	  set @filtro = ' Anual = '''+@Año+'''  and Mes = '''+@Mes+''' ' " +
            " if  (@Mes != '' and @Año != ''  and @Area != '' and @SubArea  = '' and @Sucursal != '' )  	 set @filtro = ' Sucursal = '''+@Sucursal+''' and Anual = '''+@Año+''' and Area='''+@Area+'''  and Mes = '''+@Mes+''' ' " +
            " if  (@Mes != '' and @Año != '' and @Area != '' and @SubArea != '' and @Sucursal = '')  	 set @filtro = ' Anual = '''+@Año+''' and Area = '''+@Area+''' and SubArea = '''+@SubArea+'''  and Mes = '''+@Mes+''' ' " +
            " if  (@Mes != '' and @Año != ''  and @Area = '' and @SubArea  != '' and @Sucursal != '' )  	  set @filtro = '  Sucursal = '''+@Sucursal+''' and Anual = '''+@Año+''' and SubArea = '''+@SubArea+'''  and Mes = '''+@Mes+'''' " +
            " if  (@Mes != '' and @Año != '' and @Area = '' and @SubArea = '' and @Sucursal != '')  	 set @filtro = ' Sucursal = '''+@Sucursal+''' and Anual = '''+@Año+''' and Mes = '''+@Mes+'''  ' " +
            " if  (@Mes != '' and @Año != ''  and @Area = '' and @SubArea  != '' and @Sucursal = '' )  	  set @filtro = '  Anual = '''+@Año+''' and SubArea = '''+@SubArea+'''  and Mes = '''+@Mes+''' ' " +
            " if  (@Mes != '' and @Año != '' and @Area != '' and @SubArea = '' and @Sucursal = '')  	 set @filtro = ' Anual = '''+@Año+''' and Area = '''+@Area+'''  and Mes = '''+@Mes+'''' " +
            " if  (@Mes != '' and @Año = '' and @Area != '' and @SubArea != '' and @Sucursal != '' )  	 set @filtro = ' Sucursal = '''+@Sucursal+'''  and Area = '''+@Area+''' and SubArea = '''+@SubArea+'''  and Mes = '''+@Mes+''' ' " +
            " if  (@Mes != '' and @Año = ''  and @Area = '' and @SubArea = '' and @Sucursal = ''  )  	  set @filtro = ' Mes = '''+@Mes+''' ' " +
            " if  (@Mes != '' and @Año = ''  and @Area != '' and @SubArea  = '' and @Sucursal != '' )  	 set @filtro = ' Sucursal = '''+@Sucursal+''' and Area='''+@Area+''' and Mes = '''+@Mes+''' ' " +
            " if  (@Mes != '' and @Año = '' and @Area != '' and @SubArea != '' and @Sucursal = '')  	 set @filtro = ' Area = '''+@Area+''' and SubArea = '''+@SubArea+'''  and Mes = '''+@Mes+''' ' " +
            " if  (@Mes != '' and @Año = ''  and @Area = '' and @SubArea  != '' and @Sucursal != '' )  	  set @filtro = '  Sucursal = '''+@Sucursal+'''  and SubArea = '''+@SubArea+'''  and Mes = '''+@Mes+''' ' " +
            " if  (@Mes != '' and @Año = '' and @Area = '' and @SubArea = '' and @Sucursal != '')  	 set @filtro = ' Sucursal = '''+@Sucursal+'''  and Mes = '''+@Mes+''' ' " +
            " if  (@Mes != '' and @Año = ''  and @Area = '' and @SubArea  != '' and @Sucursal = '' )  	  set @filtro = '  SubArea = '''+@SubArea+'''  and Mes = '''+@Mes+''' ' " +
            " if  (@Mes != '' and @Año = '' and @Area != '' and @SubArea = '' and @Sucursal = '')  	 set @filtro = 'Area = '''+@Area+'''  and Mes = '''+@Mes+''' ' " +
            " if  (@Mes = '' and @Año != '' and @Area != '' and @SubArea != '' and @Sucursal != '' )  	 set @filtro = ' Sucursal = '''+@Sucursal+''' and Anual = '''+@Año+''' and Area = '''+@Area+''' and SubArea = '''+@SubArea+''' ' " +
            " if  (@Mes = '' and @Año != ''  and @Area = '' and @SubArea = '' and @Sucursal = ''  )  	  set @filtro = ' Anual = '''+@Año+'''' " +
            " if  (@Mes = '' and @Año != ''  and @Area != '' and @SubArea  = '' and @Sucursal != '' )  	 set @filtro = ' Sucursal = '''+@Sucursal+''' and Anual = '''+@Año+''' and Area='''+@Area+''' ' " +
            " if  (@Mes = '' and @Año != '' and @Area != '' and @SubArea != '' and @Sucursal = '')  	 set @filtro = ' Anual = '''+@Año+''' and Area = '''+@Area+''' and SubArea = '''+@SubArea+''' '" +
            " if  (@Mes = '' and @Año != ''  and @Area = '' and @SubArea  != '' and @Sucursal != '' )  	  set @filtro = '  Sucursal = '''+@Sucursal+''' and Anual = '''+@Año+''' and SubArea = '''+@SubArea+''' ' " +
            " if  (@Mes = '' and @Año != '' and @Area = '' and @SubArea = '' and @Sucursal != '')  	 set @filtro = ' Sucursal = '''+@Sucursal+''' and Anual = '''+@Año+''' ' " +
            " if  (@Mes = '' and @Año != ''  and @Area = '' and @SubArea  != '' and @Sucursal = '' )  	  set @filtro = '  Anual = '''+@Año+''' and SubArea = '''+@SubArea+''' ' " +
            " if  (@Mes = '' and @Año != '' and @Area != '' and @SubArea = '' and @Sucursal = '')  	 set @filtro = ' Anual = '''+@Año+''' and Area = '''+@Area+''' ' " +
            " if  (@Mes = '' and @Año = '' and @Area != '' and @SubArea != '' and @Sucursal != '' )  	 set @filtro = ' Sucursal = '''+@Sucursal+'''  and Area = '''+@Area+''' and SubArea = '''+@SubArea+''' ' " +
            " if  (@Mes = '' and @Año = ''  and @Area != '' and @SubArea  = '' and @Sucursal != '' )  	 set @filtro = ' Sucursal = '''+@Sucursal+''' and Area='''+@Area+''' ' " +
            " if  (@Mes = '' and @Año = '' and @Area != '' and @SubArea != '' and @Sucursal = '')  	 set @filtro = ' Area = '''+@Area+''' and SubArea = '''+@SubArea+''' ' " +
            " if  (@Mes = '' and @Año = ''  and @Area = '' and @SubArea  != '' and @Sucursal != '' )  	  set @filtro = '  Sucursal = '''+@Sucursal+'''  and SubArea = '''+@SubArea+''' ' " +
            " if  (@Mes = '' and @Año = '' and @Area = '' and @SubArea = '' and @Sucursal != '')  	 set @filtro = ' Sucursal = '''+@Sucursal+''' ' " +
            " if  (@Mes = '' and @Año = ''  and @Area = '' and @SubArea  != '' and @Sucursal = '' )  	  set @filtro = '  SubArea = '''+@SubArea+''' ' " +
            " if  (@Mes = '' and @Año = '' and @Area != '' and @SubArea = '' and @Sucursal = '')  	 set @filtro = 'Area = '''+@Area+''' ' " +
            " set @consulta = 'SELECT Meses,' + @null + '  from  ( select Mes,DATENAME(MONTH,Fecha) as Meses, Departamento,Cantidad from [Personal].[DTM_Personal_Colaboradores]   where ' +@filtro+ ' and KeyPersonalColaboradores in(select max(KeyPersonalColaboradores) from [Personal].[DTM_Personal_Colaboradores] where ' +@filtro+ '  group by Mes ,Anual, Departamento,Area,SubArea,Sucursal) )  x  pivot ( sum(Cantidad) for Departamento in (' + @cols + ') ) p  order by Mes ' " +
            " end " +
            " execute(@consulta) ";
            return consulta;
        }
        public String Personal_CantidadColaboradores_EvolucionAreaMensual_ddlSucursal_SQL(String anio, String mes, String area, String subArea)
        {        //GIAMPIERE 12/ 08 / 2019
            consulta =
                 " declare @Año varchar(20), @Area nvarchar(max),@SubArea nvarchar(max),@Mes nvarchar(max),@Consulta nvarchar(max),@filtro nvarchar(max),@select nvarchar(max); " +
            " set @Año='" + anio + "'; set @Area='" + area + "';set @SubArea='" + subArea + "'; set @Mes='" + mes + "';set @Consulta= ''; set @filtro= ''; set @select= ''; " +
            " set @select= N'select distinct Sucursal from[Personal].[DTM_Personal_Colaboradores]' " +
            " if ( @Año != '' and @Area != '' and @SubArea != '' and @Mes != '' )   set @filtro = @select+  ' where  Mes = '''+@Mes+''' and Anual = '''+@Año+''' and Area = '''+@Area+''' and SubArea = '''+@SubArea+''' ' " +
            " if ( @Año != '' and @Area != '' and @SubArea  = '' and @Mes != '' ) set @filtro = @select+  ' where  Mes = '''+@Mes+''' and Anual = '''+@Año+''' and Area = '''+@Area+''' ' " +
            " if ( @Año != '' and @Area != '' and @SubArea != '' and @Mes = '')  	set @filtro = @select+  ' where  Anual = '''+@Año+''' and Area = '''+@Area+''' and SubArea = '''+@SubArea+''' ' " +
            " if ( @Año != '' and @Area != '' and @SubArea = '' and @Mes = '')  set @filtro = @select+  ' where  Anual = '''+@Año+''' and Area = '''+@Area+''' ' " +
            " if ( @Año != '' and @Area = '' and @SubArea = '' and @Mes = ''  )  	  set @filtro = @select+  ' where Anual = '''+@Año+'''   ' " +
            " if ( @Año != '' and @Area = '' and @SubArea  != '' and @Mes != '' )  	set @filtro = @select+  ' where  Mes = '''+@Mes+''' and Anual = '''+@Año+'''  and SubArea = '''+@SubArea+''' ' " +
            " if ( @Año != '' and @Area = '' and @SubArea = '' and @Mes != '')  	set @filtro = @select+  ' where  Mes = '''+@Mes+''' and Anual = '''+@Año+'''   ' " +
            " if ( @Año != '' and @Area = '' and @SubArea  != '' and @Mes = '' )  	set @filtro = @select+  ' where  Anual = '''+@Año+''' and SubArea = '''+@SubArea+'''  '  " +
            " if ( @Año = '' and @Area != '' and @SubArea != '' and @Mes != '' )   set @filtro = @select+  ' where  Mes = '''+@Mes+''' and  Area = '''+@Area+''' and SubArea = '''+@SubArea+'''  ' " +
            " if ( @Año = '' and @Area != '' and @SubArea  = '' and @Mes != '' ) set @filtro = @select+  ' where  Mes = '''+@Mes+''' and  Area = '''+@Area+''' ' " +
            " if ( @Año = '' and @Area != '' and @SubArea != '' and @Mes = '')  	set @filtro = @select+  ' where   Area = '''+@Area+''' and SubArea = '''+@SubArea+''' ' " +
            " if ( @Año = '' and @Area != '' and @SubArea = '' and @Mes = '')  set @filtro = @select+  ' where   Area = '''+@Area+''' ' " +
            " if ( @Año = '' and @Area = '' and @SubArea = '' and @Mes = ''  )  	  set @filtro = @select+  '' " +
            " if ( @Año = '' and @Area = '' and @SubArea  != '' and @Mes != '' )  	set @filtro = @select+  ' where  Mes = '''+@Mes+''' and SubArea = '''+@SubArea+''' ' " +
            " if ( @Año = '' and @Area = '' and @SubArea = '' and @Mes != '')  	set @filtro = @select+  ' where  Mes = '''+@Mes+'''    ' " +
            " if ( @Año = '' and @Area = '' and @SubArea  != '' and @Mes = '' )  	set @filtro = @select+  ' where   SubArea = '''+@SubArea+''' ' " +
            " set @Consulta= @filtro+ 'order by Sucursal'; " +
            " execute(@Consulta) ";
            return consulta;
        }
        public String Personal_CantidadColaboradores_EvolucionAreaMensual_ddlArea_SQL(String anio, String mes, String subArea, String sucursal)
        {        //GIAMPIERE 12/ 08 / 2019
            consulta =
                 " declare @Año varchar(5), @SubArea nvarchar(max),@Sucursal nvarchar(max),@Mes nvarchar(max),@Consulta nvarchar(max),@filtro nvarchar(max),@select nvarchar(max); " +
            " set @Año='" + anio + "'; set @SubArea='" + subArea + "'; set @Sucursal= '" + sucursal + "'; set @Mes='" + mes + "';set @Consulta= ''; set @filtro= ''; set @select= ''; " +
            " set @select= N'select distinct Area from [Personal].[DTM_Personal_Colaboradores]' " +
            " if ( @Año != '' and @SubArea != '' and @Sucursal != '' and @Mes != '' )   set @filtro = @select+  ' where  Mes = '''+@Mes+''' and Anual = '''+@Año+''' and SubArea = '''+@SubArea+''' and Sucursal = '''+@Sucursal+''' ' " +
            " if ( @Año != '' and @SubArea != '' and @Sucursal  = '' and @Mes != '' ) set @filtro = @select+  ' where  Mes = '''+@Mes+''' and Anual = '''+@Año+''' and SubArea = '''+@SubArea+''' ' " +
            " if ( @Año != '' and @SubArea != '' and @Sucursal != '' and @Mes = '')  	set @filtro = @select+  ' where  Anual = '''+@Año+''' and SubArea = '''+@SubArea+''' and Sucursal = '''+@Sucursal+''' ' " +
            " if ( @Año != '' and @SubArea != '' and @Sucursal = '' and @Mes = '')  set @filtro = @select+  ' where  Anual = '''+@Año+''' and SubArea = '''+@SubArea+''' ' " +
            " if ( @Año != '' and @SubArea = '' and @Sucursal = '' and @Mes = ''  )  	  set @filtro = @select+  ' where Anual = '''+@Año+'''   ' " +
            " if ( @Año != '' and @SubArea = '' and @Sucursal  != '' and @Mes != '' )  	set @filtro = @select+  ' where  Mes = '''+@Mes+''' and Anual = '''+@Año+'''  and Sucursal = '''+@Sucursal+''' ' " +
            " if ( @Año != '' and @SubArea = '' and @Sucursal = '' and @Mes != '')  	set @filtro = @select+  ' where  Mes = '''+@Mes+''' and Anual = '''+@Año+'''   ' " +
            " if ( @Año != '' and @SubArea = '' and @Sucursal  != '' and @Mes = '' )  	set @filtro = @select+  ' where  Anual = '''+@Año+''' and Sucursal = '''+@Sucursal+'''  '  " +
            " if ( @Año = '' and @SubArea != '' and @Sucursal != '' and @Mes != '' )   set @filtro = @select+  ' where  Mes = '''+@Mes+''' and  SubArea = '''+@SubArea+''' and Sucursal = '''+@Sucursal+'''  ' " +
            " if ( @Año = '' and @SubArea != '' and @Sucursal  = '' and @Mes != '' ) set @filtro = @select+  ' where  Mes = '''+@Mes+''' and  SubArea = '''+@SubArea+''' ' " +
            " if ( @Año = '' and @SubArea != '' and @Sucursal != '' and @Mes = '')  	set @filtro = @select+  ' where   SubArea = '''+@SubArea+''' and Sucursal = '''+@Sucursal+''' ' " +
            " if ( @Año = '' and @SubArea != '' and @Sucursal = '' and @Mes = '')  set @filtro = @select+  ' where   SubArea = '''+@SubArea+''' ' " +
            " if ( @Año = '' and @SubArea = '' and @Sucursal = '' and @Mes = ''  )  	  set @filtro = @select+  '' " +
            " if ( @Año = '' and @SubArea = '' and @Sucursal  != '' and @Mes != '' )  	set @filtro = @select+  ' where  Mes = '''+@Mes+''' and Sucursal = '''+@Sucursal+''' ' " +
            " if ( @Año = '' and @SubArea = '' and @Sucursal = '' and @Mes != '')  	set @filtro = @select+  ' where  Mes = '''+@Mes+'''    ' " +
            " if ( @Año = '' and @SubArea = '' and @Sucursal  != '' and @Mes = '' )  	set @filtro = @select+  ' where   Sucursal = '''+@Sucursal+''' ' " +
            " set @Consulta= @filtro+ 'order by Area'; " +
            " execute(@Consulta) ";
            return consulta;
        }
        public String Personal_CantidadColaboradores_EvolucionAreaMensual_ddlSubArea_SQL(String anio, String mes, String area, String sucursal)
        {        //GIAMPIERE 12/ 08 / 2019
            consulta =
                   " declare @Año varchar(20), @Area nvarchar(max),@Sucursal nvarchar(max),@Mes nvarchar(max),@Consulta nvarchar(max),@filtro nvarchar(max),@select nvarchar(max); " +
            " set @Año='" + anio + "'; set @Area='" + area + "';set @Sucursal= '" + sucursal + "'; set @Mes='" + mes + "';set @Consulta= ''; set @filtro= ''; set @select= ''; " +
            " set @select= N'select distinct SubArea from [Personal].[DTM_Personal_Colaboradores]' " +
            " if ( @Año != '' and @Area != '' and @Sucursal != '' and @Mes != '' )   set @filtro = @select+  ' where  Mes = '''+@Mes+''' and Anual = '''+@Año+''' and Area = '''+@Area+''' and Sucursal = '''+@Sucursal+''' ' " +
            " if ( @Año != '' and @Area != '' and @Sucursal  = '' and @Mes != '' ) set @filtro = @select+  ' where  Mes = '''+@Mes+''' and Anual = '''+@Año+''' and Area = '''+@Area+''' ' " +
            " if ( @Año != '' and @Area != '' and @Sucursal != '' and @Mes = '')  	set @filtro = @select+  ' where  Anual = '''+@Año+''' and Area = '''+@Area+''' and Sucursal = '''+@Sucursal+''' ' " +
            " if ( @Año != '' and @Area != '' and @Sucursal = '' and @Mes = '')  set @filtro = @select+  ' where  Anual = '''+@Año+''' and Area = '''+@Area+''' ' " +
            " if ( @Año != '' and @Area = '' and @Sucursal = '' and @Mes = ''  )  	  set @filtro = @select+  ' where Anual = '''+@Año+'''   ' " +
            " if ( @Año != '' and @Area = '' and @Sucursal  != '' and @Mes != '' )  	set @filtro = @select+  ' where  Mes = '''+@Mes+''' and Anual = '''+@Año+'''  and Sucursal = '''+@Sucursal+''' ' " +
            " if ( @Año != '' and @Area = '' and @Sucursal = '' and @Mes != '')  	set @filtro = @select+  ' where  Mes = '''+@Mes+''' and Anual = '''+@Año+'''   ' " +
            " if ( @Año != '' and @Area = '' and @Sucursal  != '' and @Mes = '' )  	set @filtro = @select+  ' where  Anual = '''+@Año+''' and Sucursal = '''+@Sucursal+'''  '  " +
            " if ( @Año = '' and @Area != '' and @Sucursal != '' and @Mes != '' )   set @filtro = @select+  ' where  Mes = '''+@Mes+''' and  Area = '''+@Area+''' and Sucursal = '''+@Sucursal+'''  ' " +
            " if ( @Año = '' and @Area != '' and @Sucursal  = '' and @Mes != '' ) set @filtro = @select+  ' where  Mes = '''+@Mes+''' and  Area = '''+@Area+''' ' " +
            " if ( @Año = '' and @Area != '' and @Sucursal != '' and @Mes = '')  	set @filtro = @select+  ' where   Area = '''+@Area+''' and Sucursal = '''+@Sucursal+''' ' " +
            " if ( @Año = '' and @Area != '' and @Sucursal = '' and @Mes = '')  set @filtro = @select+  ' where   Area = '''+@Area+''' ' " +
            " if ( @Año = '' and @Area = '' and @Sucursal = '' and @Mes = ''  )  	  set @filtro = @select+  '' " +
            " if ( @Año = '' and @Area = '' and @Sucursal  != '' and @Mes != '' )  	set @filtro = @select+  ' where  Mes = '''+@Mes+''' and Sucursal = '''+@Sucursal+''' ' " +
            " if ( @Año = '' and @Area = '' and @Sucursal = '' and @Mes != '')  	set @filtro = @select+  ' where  Mes = '''+@Mes+'''    ' " +
            " if ( @Año = '' and @Area = '' and @Sucursal  != '' and @Mes = '' )  	set @filtro = @select+  ' where   Sucursal = '''+@Sucursal+''' ' " +
            " set @Consulta= @filtro+ 'order by SubArea'; " +
            " execute(@Consulta) ";

            return consulta;
        }
        public String Personal_CantidadColaboradores_EvolucionAreaMensual_ddlAnio_SQL(String mes, String area, String subArea, String sucursal)
        {//GIAMPIERE 12/ 08 / 2019
            consulta =
                         " declare @Mes varchar(5), @SubArea nvarchar(max),@Sucursal nvarchar(max),@Area nvarchar(max),@Consulta nvarchar(max),@filtro nvarchar(max),@select nvarchar(max); " +
            " set @Area='" + area + "';set @SubArea='" + subArea + "'; set @Sucursal= '" + sucursal + "'; set @Mes='" + mes + "';set @Consulta= ''; set @filtro= ''; set @select= ''; " +
            " set @select= N'select distinct Anual as Anio from [Personal].[DTM_Personal_Colaboradores]' " +
            " if ( @Mes != '' and @SubArea != '' and @Sucursal != '' and @Area != '' )   set @filtro = @select+  ' where  Area = '''+@Area+''' and Mes = '''+@Mes+''' and SubArea = '''+@SubArea+''' and Sucursal = '''+@Sucursal+''' ' " +
            " if ( @Mes != '' and @SubArea != '' and @Sucursal  = '' and @Area != '' ) set @filtro = @select+  ' where  Area = '''+@Area+''' and Mes = '''+@Mes+''' and SubArea = '''+@SubArea+''' ' " +
            " if ( @Mes != '' and @SubArea != '' and @Sucursal != '' and @Area = '')  	set @filtro = @select+  ' where  Mes = '''+@Mes+''' and SubArea = '''+@SubArea+''' and Sucursal = '''+@Sucursal+''' ' " +
            " if ( @Mes != '' and @SubArea != '' and @Sucursal = '' and @Area = '')  set @filtro = @select+  ' where  Mes = '''+@Mes+''' and SubArea = '''+@SubArea+''' ' " +
            " if ( @Mes != '' and @SubArea = '' and @Sucursal = '' and @Area = ''  )  	  set @filtro = @select+  ' where Mes = '''+@Mes+'''   ' " +
            " if ( @Mes != '' and @SubArea = '' and @Sucursal  != '' and @Area != '' )  	set @filtro = @select+  ' where  Area = '''+@Area+''' and Mes = '''+@Mes+'''  and Sucursal = '''+@Sucursal+''' ' " +
            " if ( @Mes != '' and @SubArea = '' and @Sucursal = '' and @Area != '')  	set @filtro = @select+  ' where  Area = '''+@Area+''' and Mes = '''+@Mes+'''   ' " +
            " if ( @Mes != '' and @SubArea = '' and @Sucursal  != '' and @Area = '' )  	set @filtro = @select+  ' where  Mes = '''+@Mes+''' and Sucursal = '''+@Sucursal+'''  '  " +
            " if ( @Mes = '' and @SubArea != '' and @Sucursal != '' and @Area != '' )   set @filtro = @select+  ' where  Area = '''+@Area+''' and  SubArea = '''+@SubArea+''' and Sucursal = '''+@Sucursal+'''  ' " +
            " if ( @Mes = '' and @SubArea != '' and @Sucursal  = '' and @Area != '' ) set @filtro = @select+  ' where  Area = '''+@Area+''' and  SubArea = '''+@SubArea+''' ' " +
            " if ( @Mes = '' and @SubArea != '' and @Sucursal != '' and @Area = '')  	set @filtro = @select+  ' where   SubArea = '''+@SubArea+''' and Sucursal = '''+@Sucursal+''' ' " +
            " if ( @Mes = '' and @SubArea != '' and @Sucursal = '' and @Area = '')  set @filtro = @select+  ' where   SubArea = '''+@SubArea+''' ' " +
            " if ( @Mes = '' and @SubArea = '' and @Sucursal = '' and @Area = ''  )  	  set @filtro = @select+  '' " +
            " if ( @Mes = '' and @SubArea = '' and @Sucursal  != '' and @Area != '' )  	set @filtro = @select+  ' where  Area = '''+@Area+''' and Sucursal = '''+@Sucursal+''' ' " +
            " if ( @Mes = '' and @SubArea = '' and @Sucursal = '' and @Area != '')  	set @filtro = @select+  ' where  Area = '''+@Area+'''    ' " +
            " if ( @Mes = '' and @SubArea = '' and @Sucursal  != '' and @Area = '' )  	set @filtro = @select+  ' where   Sucursal = '''+@Sucursal+''' ' " +
            " set @Consulta= @filtro+ 'order by Anual'; " +
            " execute(@Consulta) ";


            return consulta;
        }
        public String Personal_CantidadColaboradores_EvolucionAreaMensual_ddlMes_SQL(String anio, String area, String subArea, String sucursal)
        {//GIAMPIERE 12/ 08 / 2019
            consulta =
                " declare @Año varchar(5), @SubArea nvarchar(max),@Sucursal nvarchar(max),@Area nvarchar(max),@Consulta nvarchar(max),@filtro nvarchar(max),@select nvarchar(max); " +
            " set @Año='" + anio + "'; set @Area='" + area + "';set @SubArea='" + subArea + "'; set @Sucursal= '" + sucursal + "'; set @Consulta= ''; set @filtro= ''; set @select= ''; " +
            " set @select= N'select distinct DATENAME(MONTH,Fecha) as Meses ,Mes from [Personal].[DTM_Personal_Colaboradores]' " +
            " if ( @Año != '' and @SubArea != '' and @Sucursal != '' and @Area != '' )   set @filtro = @select+  ' where  Area = '''+@Area+''' and Anual = '''+@Año+''' and SubArea = '''+@SubArea+''' and Sucursal = '''+@Sucursal+''' ' " +
            " if ( @Año != '' and @SubArea != '' and @Sucursal  = '' and @Area != '' ) set @filtro = @select+  ' where  Area = '''+@Area+''' and Anual = '''+@Año+''' and SubArea = '''+@SubArea+''' ' " +
            " if ( @Año != '' and @SubArea != '' and @Sucursal != '' and @Area = '')  	set @filtro = @select+  ' where  Anual = '''+@Año+''' and SubArea = '''+@SubArea+''' and Sucursal = '''+@Sucursal+''' ' " +
            " if ( @Año != '' and @SubArea != '' and @Sucursal = '' and @Area = '')  set @filtro = @select+  ' where  Anual = '''+@Año+''' and SubArea = '''+@SubArea+''' ' " +
            " if ( @Año != '' and @SubArea = '' and @Sucursal = '' and @Area = ''  )  	  set @filtro = @select+  ' where Anual = '''+@Año+'''   ' " +
            " if ( @Año != '' and @SubArea = '' and @Sucursal  != '' and @Area != '' )  	set @filtro = @select+  ' where  Area = '''+@Area+''' and Anual = '''+@Año+'''  and Sucursal = '''+@Sucursal+''' ' " +
            " if ( @Año != '' and @SubArea = '' and @Sucursal = '' and @Area != '')  	set @filtro = @select+  ' where  Area = '''+@Area+''' and Anual = '''+@Año+'''   ' " +
            " if ( @Año != '' and @SubArea = '' and @Sucursal  != '' and @Area = '' )  	set @filtro = @select+  ' where  Anual = '''+@Año+''' and Sucursal = '''+@Sucursal+'''  '  " +
            " if ( @Año = '' and @SubArea != '' and @Sucursal != '' and @Area != '' )   set @filtro = @select+  ' where  Area = '''+@Area+''' and  SubArea = '''+@SubArea+''' and Sucursal = '''+@Sucursal+'''  ' " +
            " if ( @Año = '' and @SubArea != '' and @Sucursal  = '' and @Area != '' ) set @filtro = @select+  ' where  Area = '''+@Area+''' and  SubArea = '''+@SubArea+''' ' " +
            " if ( @Año = '' and @SubArea != '' and @Sucursal != '' and @Area = '')  	set @filtro = @select+  ' where   SubArea = '''+@SubArea+''' and Sucursal = '''+@Sucursal+''' ' " +
            " if ( @Año = '' and @SubArea != '' and @Sucursal = '' and @Area = '')  set @filtro = @select+  ' where   SubArea = '''+@SubArea+''' ' " +
            " if ( @Año = '' and @SubArea = '' and @Sucursal = '' and @Area = ''  )  	  set @filtro = @select+  '' " +
            " if ( @Año = '' and @SubArea = '' and @Sucursal  != '' and @Area != '' )  	set @filtro = @select+  ' where  Area = '''+@Area+''' and Sucursal = '''+@Sucursal+''' ' " +
            " if ( @Año = '' and @SubArea = '' and @Sucursal = '' and @Area != '')  	set @filtro = @select+  ' where  Area = '''+@Area+'''    ' " +
            " if ( @Año = '' and @SubArea = '' and @Sucursal  != '' and @Area = '' )  	set @filtro = @select+  ' where   Sucursal = '''+@Sucursal+''' ' " +
            " set @Consulta= @filtro+ 'order by Mes'; " +
            " execute(@Consulta) ";

            return consulta;
        }
        #endregion   //MODULO: CANTIDAD DE COLABORADORES
        #region Ausentismos y descanzos medicos
        //GIAMPIERE 19/ 08 / 2019
        //Cantidad Area
        public string G_Personal_Ausentismo_cantidadGeneral_SQL =

                " declare @Mes date, @primerDiaMes date, @ultimaDiaMes date  " +
                " IF(getdate()= (select EOMONTH(getdate()) as UltDiaMesActual))  " +
                " begin set @Mes=	(SELECT CONVERT (date, GETDATE()))	 end ELSE begin" +
                " set @Mes= (select EOMONTH(getdate(),-1) as UltDiaMesAnterior)  end " +
                " set @primerDiaMes= dateadd(d,1, EOMONTH(@Mes,-1)) " +
                " set @ultimaDiaMes = EOMONTH(@Mes) " +
                " select sum(TotalInasistencias) as Cantidad  from [Personal].[DTM_Personal_Ausentismos] " +
                " where Fecha between @primerDiaMes and @ultimaDiaMes  and  KeyPersonalAusentismos in(select max(KeyPersonalAusentismos) " +
                " from [Personal].[DTM_Personal_Ausentismos] Where Fecha between @primerDiaMes and @ultimaDiaMes " +
                " group by Mes,Sucursal ,Departamento,Area,SubArea) ";

        //GIAMPIERE 17/ 08 / 2019
        public string G_Personal_Descanso_cantidadGeneral_SQL =

                " declare @Mes date, @primerDiaMes date, @ultimaDiaMes date   " +
                " IF(getdate()= (select EOMONTH(getdate()) as UltDiaMesActual)) " +
                " begin set @Mes=	(SELECT CONVERT (date, GETDATE()))	 end ELSE begin " +
                " set @Mes= (select EOMONTH(getdate(),-1) as UltDiaMesAnterior)  end " +
                " set @primerDiaMes= dateadd(d,1, EOMONTH(@Mes,-1)) " +
                " set @ultimaDiaMes = EOMONTH(@Mes) " +
                " select sum(TotalDescansos) as Cantidad  from [Personal].[DTM_Personal_Ausentismos] " +
                " where Fecha between @primerDiaMes and @ultimaDiaMes  and  KeyPersonalAusentismos in(select max(KeyPersonalAusentismos) " +
                " from [Personal].[DTM_Personal_Ausentismos] Where Fecha between @primerDiaMes and @ultimaDiaMes " +
                " group by Mes,Sucursal ,Departamento,Area,SubArea)";

        //GIAMPIERE 17/ 08 / 2019
        public string Personal_AusentismoyDescanso_Tipo_SQL =

        " CREATE TABLE #temp (Tipo varchar(max))" +
       " INSERT INTO #temp VALUES ('Faltas') " +
       " INSERT INTO #temp VALUES ('Descansos Medicos') " +
       " select Tipo from #temp  drop table #temp ";
        public String G_Personal_Ausentismo_CantidadArea_SQL(String area, String subArea, String sucursal)
        {        //GIAMPIERE 13/ 09 / 2019
            consulta =
         " declare  @Area nvarchar(max),@mesActual varchar(max),@mes varchar(max),@anio varchar(max), @SubArea nvarchar(max), @Sucursal nvarchar(max),@consulta nvarchar(max), @filtro nvarchar(max) " +
         " set @Area='" + area + "';set @SubArea='" + subArea + "'; set @Sucursal= '" + sucursal + "';set @consulta=''; set @filtro= ''; " +
         " IF(getdate() = (select EOMONTH(getdate()))) begin set @mesActual = (SELECT CONVERT(date, GETDATE())) end ELSe begin set @mesActual = (select EOMONTH(getdate(), -1) ) end set @mes = MONTH(@mesActual); set @anio = YEAR(@mesActual); " +
         " if ( @Area != '' and @SubArea = '' and @Sucursal = '')  set @filtro= ' Where  Mes='''+@mes+''' and Anual='''+@anio+''' and Area='''+@Area+''' '   " +
         " if ( @Area != '' and @SubArea = '' and @Sucursal != '') set @filtro= '  Where  Mes='''+@mes+''' and Anual='''+@anio+''' and Area='''+@Area+'''  and Sucursal='''+@Sucursal+''' '  " +
         " if ( @Area != '' and @SubArea != '' and @Sucursal = '') set @filtro= '  Where  Mes='''+@mes+''' and Anual='''+@anio+'''  and Area='''+@Area+'''  and SubArea='''+@SubArea+''' ' " +
         " if ( @Area != '' and @SubArea != '' and @Sucursal != '') set @filtro= '  Where  Mes='''+@mes+''' and Anual='''+@anio+''' and Area='''+@Area+'''  and SubArea='''+@SubArea+''' and Sucursal='''+@Sucursal+''' ' " +
         " if ( @Area = '' and @SubArea = '' and @Sucursal = '') set @filtro= '  Where  Mes='''+@mes+''' and Anual='''+@anio+'''   ' " +
         " if ( @Area = '' and @SubArea = '' and @Sucursal != '') set @filtro= '  Where  Mes='''+@mes+''' and Anual='''+@anio+''' and Sucursal='''+@Sucursal+''' ' " +
         " if ( @Area = '' and @SubArea != '' and @Sucursal = '') set @filtro= '  Where  Mes='''+@mes+''' and Anual='''+@anio+''' and SubArea='''+@SubArea+''' ' " +
         " if ( @Area = '' and @SubArea != '' and @Sucursal != '') set @filtro= '  Where  Mes='''+@mes+''' and Anual='''+@anio+''' and SubArea='''+@SubArea+''' and Sucursal='''+@Sucursal+''' ' " +
         " set @consulta=' select Departamento, sum( TotalInasistencias) as Total  from [Personal].[DTM_Personal_Ausentismos] '+@filtro+'  and KeyPersonalAusentismos in(select max(KeyPersonalAusentismos) from [Personal].[DTM_Personal_Ausentismos] '+@filtro+'  group by Sucursal ,Departamento,Area,SubArea) group by Departamento' " +
         " execute(@consulta)";
            return consulta;
        }
        public String G_Personal_DescansoMedico_CantidadArea_SQL(String area, String subArea, String sucursal)
        {        //GIAMPIERE 13/ 09 / 2019
           consulta =
         " declare  @Area nvarchar(max),@mesActual varchar(max),@mes varchar(max),@anio varchar(max), @SubArea nvarchar(max), @Sucursal nvarchar(max),@consulta nvarchar(max), @filtro nvarchar(max) " +
         " set @Area='" + area + "';set @SubArea='" + subArea + "'; set @Sucursal= '" + sucursal + "';set @consulta=''; set @filtro= ''; " +
         " IF(getdate() = (select EOMONTH(getdate()))) begin set @mesActual = (SELECT CONVERT(date, GETDATE())) end ELSe begin set @mesActual = (select EOMONTH(getdate(), -1) ) end set @mes = MONTH(@mesActual); set @anio = YEAR(@mesActual); " +
         " if ( @Area != '' and @SubArea = '' and @Sucursal = '')  set @filtro= ' Where  Mes='''+@mes+''' and Anual='''+@anio+''' and Area='''+@Area+''' '   " +
         " if ( @Area != '' and @SubArea = '' and @Sucursal != '') set @filtro= '  Where  Mes='''+@mes+''' and Anual='''+@anio+''' and Area='''+@Area+'''  and Sucursal='''+@Sucursal+''' '  " +
         " if ( @Area != '' and @SubArea != '' and @Sucursal = '') set @filtro= '  Where  Mes='''+@mes+''' and Anual='''+@anio+'''  and Area='''+@Area+'''  and SubArea='''+@SubArea+''' ' " +
         " if ( @Area != '' and @SubArea != '' and @Sucursal != '') set @filtro= '  Where  Mes='''+@mes+''' and Anual='''+@anio+''' and Area='''+@Area+'''  and SubArea='''+@SubArea+''' and Sucursal='''+@Sucursal+''' ' " +
         " if ( @Area = '' and @SubArea = '' and @Sucursal = '') set @filtro= '  Where  Mes='''+@mes+''' and Anual='''+@anio+'''   ' " +
         " if ( @Area = '' and @SubArea = '' and @Sucursal != '') set @filtro= '  Where  Mes='''+@mes+''' and Anual='''+@anio+''' and Sucursal='''+@Sucursal+''' ' " +
         " if ( @Area = '' and @SubArea != '' and @Sucursal = '') set @filtro= '  Where  Mes='''+@mes+''' and Anual='''+@anio+''' and SubArea='''+@SubArea+''' ' " +
         " if ( @Area = '' and @SubArea != '' and @Sucursal != '') set @filtro= '  Where  Mes='''+@mes+''' and Anual='''+@anio+''' and SubArea='''+@SubArea+''' and Sucursal='''+@Sucursal+''' ' " +
         " set @consulta=' select Departamento, sum( TotalDescansos) as Total  from [Personal].[DTM_Personal_Ausentismos] '+@filtro+'  and KeyPersonalAusentismos in(select max(KeyPersonalAusentismos) from [Personal].[DTM_Personal_Ausentismos] '+@filtro+'  group by Sucursal ,Departamento,Area,SubArea) group by Departamento' " +
         " execute(@consulta)";
            return consulta;
        }
        public String Personal_AusentismoYDescanso_CantidadArea_ddlSucursal_SQL(String area, String subArea)
        {        //GIAMPIERE 13/ 09 / 2019
            consulta =
        " declare  @Area nvarchar(max), @SubArea nvarchar(max) " +
        " set @Area='" + area + "';set @SubArea='" + subArea + "'; " +
        " if ( @Area = '' and @SubArea = '' ) Select distinct Sucursal  from [Personal].[DTM_Personal_Ausentismos]" +
        " if ( @Area != '' and @SubArea = '' ) Select distinct Sucursal  from [Personal].[DTM_Personal_Ausentismos] Where   Area=@Area " +
        " if ( @Area = '' and @SubArea != '' ) Select distinct Sucursal  from [Personal].[DTM_Personal_Ausentismos] Where SubArea=@SubArea" +
        " if ( @Area != '' and @SubArea != '') Select distinct Sucursal  from [Personal].[DTM_Personal_Ausentismos] Where Area=@Area and SubArea=@SubArea";

            return consulta;
        }

        public String Personal_AusentismoYDescanso_CantidadArea_ddlArea_SQL(String sucursal, String subArea)
        {        //GIAMPIERE 13/ 09 / 2019
            consulta =
         " declare  @Sucursal nvarchar(max), @SubArea nvarchar(max)" +
         " set @SubArea='" + subArea + "'; set @Sucursal= '" + sucursal + "';" +
         " if (@SubArea = '' and @Sucursal = '')  Select distinct Area  from [Personal].[DTM_Personal_Ausentismos]" +
         " if (@SubArea = '' and @Sucursal != '')  Select distinct Area  from [Personal].[DTM_Personal_Ausentismos] where Sucursal=@Sucursal" +
         " if (@SubArea != '' and @Sucursal = '') Select distinct Area  from [Personal].[DTM_Personal_Ausentismos] where SubArea=@SubArea" +
         " if (@SubArea != '' and @Sucursal != '') Select distinct Area  from [Personal].[DTM_Personal_Ausentismos] where Sucursal=@Sucursal and SubArea=@SubArea";

            return consulta;
        }
        public String Personal_AusentismoYDescanso_CantidadArea_ddlSubArea_SQL(String sucursal, String area)
        {        //GIAMPIERE 13/ 09 / 2019
            consulta =
         "  declare  @Sucursal nvarchar(max), @Area  nvarchar(max)" +
          " set @Area='" + area + "'; set @Sucursal= '" + sucursal + "';" +
        "  if (@Area  = '' and @Sucursal = '')  Select distinct SubArea  from [Personal].[DTM_Personal_Ausentismos]" +
        "  if (@Area  = '' and @Sucursal != '')  Select distinct SubArea  from [Personal].[DTM_Personal_Ausentismos] where Sucursal=@Sucursal" +
        "  if (@Area  != '' and @Sucursal = '') Select distinct SubArea  from [Personal].[DTM_Personal_Ausentismos] where Area=@Area " +
        "  if (@Area  != '' and @Sucursal != '') Select distinct SubArea  from [Personal].[DTM_Personal_Ausentismos] where Sucursal=@Sucursal and Area=@Area ";
            return consulta;
        }

        //EVOLUCIÓN GENERAL MENSUAL
        public String G_Personal_Ausentismo_EvolucionGeneralMensual_SQL(String mes, String departamento, String area, String subArea, String sucursal)
        {        //GIAMPIERE 18/ 09 / 2019
            consulta =
                " declare @Departamento varchar(20), @Area nvarchar(max), @Mes nvarchar(3),@SubArea nvarchar(max),@Sucursal nvarchar(max), @filtro  NVARCHAR(MAX), @cols NVARCHAR(MAX), @null NVARCHAR(MAX), @consulta NVARCHAR(MAX); " +
          " set @Departamento= '" + departamento + "'; set @Area='" + area + "';set @SubArea='" + subArea + "'; set @Sucursal= '" + sucursal + "'; set @Mes='" + mes + "'; set @filtro =''; set @consulta =''; " +
          " select @cols = STUFF((SELECT Distinct ',' + QUOTENAME(Anual)  from [Personal].[DTM_Personal_Ausentismos] group by ',' + QUOTENAME(Anual)Order by ',' + QUOTENAME(Anual) FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)')  ,1,1,'') " +
           " select @null = STUFF((SELECT Distinct ', isnull(' + QUOTENAME(Anual)+',0) as'  + QUOTENAME(Anual) from [Personal].[DTM_Personal_Ausentismos]  group by ', isnull(' + QUOTENAME(Anual)+',0) as'  + QUOTENAME(Anual) Order by ', isnull(' + QUOTENAME(Anual)+',0) as'  + QUOTENAME(Anual)   FOR XML PATH(''), TYPE  ).value('.', 'NVARCHAR(MAX)')   ,1,1,'') " +
           " if  (@Mes = '' and @Departamento = ''  and @Area = '' and @SubArea = '' and @Sucursal = ''  )  	  set @consulta= 'SELECT Meses,' + @null + '  from  ( select Mes, DATENAME(MONTH,Fecha) as Meses,Anual,TotalInasistencias from [Personal].[DTM_Personal_Ausentismos] where  KeyPersonalAusentismos in(select max(KeyPersonalAusentismos) from [Personal].[DTM_Personal_Ausentismos]  group by  Mes ,Anual,Sucursal ,Departamento,Area,SubArea)   ) x  pivot ( sum(TotalInasistencias) for Anual in (' + @cols + ') ) p  order by Mes' " +
           " else  " +
           " begin " +
           " if  (@Mes != '' and @Departamento != '' and @Area != '' and @SubArea != '' and @Sucursal != '' )  	 set @filtro = ' Sucursal = '''+@Sucursal+''' and Departamento = '''+@Departamento+''' and Area = '''+@Area+''' and SubArea = '''+@SubArea+'''  and Mes = '''+@Mes+'''  ' " +
           " if  (@Mes != '' and @Departamento != ''  and @Area = '' and @SubArea = '' and @Sucursal = ''  )  	 set @filtro = ' Departamento = '''+@Departamento+'''  and Mes = '''+@Mes+''' ' " +
           " if  (@Mes != '' and @Departamento != ''  and @Area != '' and @SubArea  = '' and @Sucursal != '' )   set @filtro = ' Sucursal = '''+@Sucursal+''' and Departamento = '''+@Departamento+''' and Area='''+@Area+'''  and Mes = '''+@Mes+''' ' " +
           " if  (@Mes != '' and @Departamento != '' and @Area != '' and @SubArea != '' and @Sucursal = '')  	 set @filtro = ' Departamento = '''+@Departamento+''' and Area = '''+@Area+''' and SubArea = '''+@SubArea+'''  and Mes = '''+@Mes+''' ' " +
           " if  (@Mes != '' and @Departamento != ''  and @Area = '' and @SubArea  != '' and @Sucursal != '' )   set @filtro = ' Sucursal = '''+@Sucursal+''' and Departamento = '''+@Departamento+''' and SubArea = '''+@SubArea+'''  and Mes = '''+@Mes+''' ' " +
           " if  (@Mes != '' and @Departamento != '' and @Area = '' and @SubArea = '' and @Sucursal != '')  	 set @filtro = ' Sucursal = '''+@Sucursal+''' and Departamento = '''+@Departamento+''' and Mes = '''+@Mes+'''   ' " +
           " if  (@Mes != '' and @Departamento != ''  and @Area = '' and @SubArea  != '' and @Sucursal = '' )  	  set @filtro = ' Departamento = '''+@Departamento+''' and SubArea = '''+@SubArea+'''  and Mes = '''+@Mes+'''  ' " +
           " if  (@Mes != '' and @Departamento != '' and @Area != '' and @SubArea = '' and @Sucursal = '')  	 set @filtro = '  Departamento = '''+@Departamento+''' and Area = '''+@Area+'''  and Mes = '''+@Mes+''' ' " +
           " if  (@Mes != '' and @Departamento = '' and @Area != '' and @SubArea != '' and @Sucursal != '' )  	 set @filtro = '  Sucursal = '''+@Sucursal+'''  and Area = '''+@Area+''' and SubArea = '''+@SubArea+'''  and Mes = '''+@Mes+''' ' " +
           " if  (@Mes != '' and @Departamento = ''  and @Area = '' and @SubArea = '' and @Sucursal = ''  )  	  set @filtro = ' Mes = '''+@Mes+''' ' " +
           " if  (@Mes != '' and @Departamento = ''  and @Area != '' and @SubArea  = '' and @Sucursal != '' )  	 set @filtro = ' Sucursal = '''+@Sucursal+''' and Area='''+@Area+''' and Mes = '''+@Mes+'''  ' " +
           " if  (@Mes != '' and @Departamento = '' and @Area != '' and @SubArea != '' and @Sucursal = '')  	 set @filtro = ' Area = '''+@Area+''' and SubArea = '''+@SubArea+'''  and Mes = '''+@Mes+'''   ' " +
           " if  (@Mes != '' and @Departamento = ''  and @Area = '' and @SubArea  != '' and @Sucursal != '' )  	  set @filtro = '  Sucursal = '''+@Sucursal+'''  and SubArea = '''+@SubArea+'''  and Mes = '''+@Mes+''' ' " +
           " if  (@Mes != '' and @Departamento = '' and @Area = '' and @SubArea = '' and @Sucursal != '')  	 set @filtro = ' Sucursal = '''+@Sucursal+'''  and Mes = '''+@Mes+'''  ' " +
           " if  (@Mes != '' and @Departamento = ''  and @Area = '' and @SubArea  != '' and @Sucursal = '' )  	  set @filtro = '  SubArea = '''+@SubArea+'''  and Mes = '''+@Mes+''' '  " +
           " if  (@Mes != '' and @Departamento = '' and @Area != '' and @SubArea = '' and @Sucursal = '')  	 set @filtro = ' Area = '''+@Area+'''  and Mes = '''+@Mes+''' ' " +
           " if  (@Mes = '' and @Departamento != '' and @Area != '' and @SubArea != '' and @Sucursal != '' )  	 set @filtro = '  Sucursal = '''+@Sucursal+''' and Departamento = '''+@Departamento+''' and Area = '''+@Area+''' and SubArea = '''+@SubArea+''' ' " +
           " if  (@Mes = '' and @Departamento != ''  and @Area = '' and @SubArea = '' and @Sucursal = ''  )  	  set @filtro = '  Departamento = '''+@Departamento+'''  ' " +
           " if  (@Mes = '' and @Departamento != ''  and @Area != '' and @SubArea  = '' and @Sucursal != '' )  	 set @filtro = '  Sucursal = '''+@Sucursal+''' and Departamento = '''+@Departamento+''' and Area='''+@Area+'''   ' " +
           " if  (@Mes = '' and @Departamento != '' and @Area != '' and @SubArea != '' and @Sucursal = '')  	 set @filtro = '  Departamento = '''+@Departamento+''' and Area = '''+@Area+''' and SubArea = '''+@SubArea+'''   '  " +
           " if  (@Mes = '' and @Departamento != ''  and @Area = '' and @SubArea  != '' and @Sucursal != '' )  	  set @filtro = '  Sucursal = '''+@Sucursal+''' and Departamento = '''+@Departamento+''' and SubArea = '''+@SubArea+'''  ' " +
           " if  (@Mes = '' and @Departamento != '' and @Area = '' and @SubArea = '' and @Sucursal != '')  	 set @filtro = ' Sucursal = '''+@Sucursal+''' and Departamento = '''+@Departamento+''''  " +
           " if  (@Mes = '' and @Departamento != ''  and @Area = '' and @SubArea  != '' and @Sucursal = '' )  	  set @filtro = '  Departamento = '''+@Departamento+''' and SubArea = '''+@SubArea+'''' " +
           " if  (@Mes = '' and @Departamento != '' and @Area != '' and @SubArea = '' and @Sucursal = '')  	 set @filtro = '  Departamento = '''+@Departamento+''' and Area = '''+@Area+''' ' " +
           " if  (@Mes = '' and @Departamento = '' and @Area != '' and @SubArea != '' and @Sucursal != '' )  	 set @filtro = '  Sucursal = '''+@Sucursal+'''  and Area = '''+@Area+''' and SubArea = '''+@SubArea+'''  ' " +
           " if  (@Mes = '' and @Departamento = ''  and @Area != '' and @SubArea  = '' and @Sucursal != '' )  	 set @filtro = '  Sucursal = '''+@Sucursal+''' and Area='''+@Area+'''  ' " +
           " if  (@Mes = '' and @Departamento = '' and @Area != '' and @SubArea != '' and @Sucursal = '')  	 set @filtro = '  Area = '''+@Area+''' and SubArea = '''+@SubArea+''' '  " +
           " if  (@Mes = '' and @Departamento = ''  and @Area = '' and @SubArea  != '' and @Sucursal != '' )  	  set @filtro = '  Sucursal = '''+@Sucursal+'''  and SubArea = '''+@SubArea+'''  ' " +
           " if  (@Mes = '' and @Departamento = '' and @Area = '' and @SubArea = '' and @Sucursal != '')  	 set @filtro = ' Sucursal = '''+@Sucursal+''' '" +
           " if  (@Mes = '' and @Departamento = ''  and @Area = '' and @SubArea  != '' and @Sucursal = '' )  	  set @filtro = '  SubArea = '''+@SubArea+''' ' " +
           " if  (@Mes = '' and @Departamento = '' and @Area != '' and @SubArea = '' and @Sucursal = '')  	 set @filtro = ' Area = '''+@Area+'''  ' " +
           " set @consulta= 'SELECT Meses,' + @null + '  from  ( select Mes, DATENAME(MONTH,Fecha) as Meses,Anual,TotalInasistencias from [Personal].[DTM_Personal_Ausentismos] where '+@filtro+' and KeyPersonalAusentismos in(select max(KeyPersonalAusentismos) from [Personal].[DTM_Personal_Ausentismos] where '+@filtro+'  group by  Mes ,Anual,Sucursal ,Departamento,Area,SubArea)   ) x  pivot ( sum(TotalInasistencias) for Anual in (' + @cols + ') ) p  order by Mes' " +
           " end " +
           " execute(@consulta) ";
            return consulta;
        }
        public String G_Personal_Descanso_EvolucionGeneralMensual_SQL(String mes, String departamento, String area, String subArea, String sucursal)
        {        //GIAMPIERE 18/ 09 / 2019
            consulta =
                " declare @Departamento varchar(20), @Area nvarchar(max), @Mes nvarchar(3),@SubArea nvarchar(max),@Sucursal nvarchar(max), @filtro  NVARCHAR(MAX), @cols NVARCHAR(MAX), @null NVARCHAR(MAX), @consulta NVARCHAR(MAX); " +
          " set @Departamento= '" + departamento + "'; set @Area='" + area + "';set @SubArea='" + subArea + "'; set @Sucursal= '" + sucursal + "'; set @Mes='" + mes + "'; set @filtro =''; set @consulta =''; " +
          " select @cols = STUFF((SELECT Distinct ',' + QUOTENAME(Anual)  from [Personal].[DTM_Personal_Ausentismos] group by ',' + QUOTENAME(Anual)Order by ',' + QUOTENAME(Anual) FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)')  ,1,1,'') " +
           " select @null = STUFF((SELECT Distinct ', isnull(' + QUOTENAME(Anual)+',0) as'  + QUOTENAME(Anual) from [Personal].[DTM_Personal_Ausentismos]  group by ', isnull(' + QUOTENAME(Anual)+',0) as'  + QUOTENAME(Anual) Order by ', isnull(' + QUOTENAME(Anual)+',0) as'  + QUOTENAME(Anual)   FOR XML PATH(''), TYPE  ).value('.', 'NVARCHAR(MAX)')   ,1,1,'') " +
           " if  (@Mes = '' and @Departamento = ''  and @Area = '' and @SubArea = '' and @Sucursal = ''  )  	  set @consulta= 'SELECT Meses,' + @null + '  from  ( select Mes, DATENAME(MONTH,Fecha) as Meses,Anual,TotalDescansos from [Personal].[DTM_Personal_Ausentismos] where  KeyPersonalAusentismos in(select max(KeyPersonalAusentismos) from [Personal].[DTM_Personal_Ausentismos]  group by  Mes ,Anual,Sucursal ,Departamento,Area,SubArea)   ) x  pivot ( sum(TotalDescansos) for Anual in (' + @cols + ') ) p  order by Mes' " +
           " else  " +
           " begin " +
           " if  (@Mes != '' and @Departamento != '' and @Area != '' and @SubArea != '' and @Sucursal != '' )  	 set @filtro = ' Sucursal = '''+@Sucursal+''' and Departamento = '''+@Departamento+''' and Area = '''+@Area+''' and SubArea = '''+@SubArea+'''  and Mes = '''+@Mes+'''  ' " +
           " if  (@Mes != '' and @Departamento != ''  and @Area = '' and @SubArea = '' and @Sucursal = ''  )  	 set @filtro = ' Departamento = '''+@Departamento+'''  and Mes = '''+@Mes+''' ' " +
           " if  (@Mes != '' and @Departamento != ''  and @Area != '' and @SubArea  = '' and @Sucursal != '' )   set @filtro = ' Sucursal = '''+@Sucursal+''' and Departamento = '''+@Departamento+''' and Area='''+@Area+'''  and Mes = '''+@Mes+''' ' " +
           " if  (@Mes != '' and @Departamento != '' and @Area != '' and @SubArea != '' and @Sucursal = '')  	 set @filtro = ' Departamento = '''+@Departamento+''' and Area = '''+@Area+''' and SubArea = '''+@SubArea+'''  and Mes = '''+@Mes+''' ' " +
           " if  (@Mes != '' and @Departamento != ''  and @Area = '' and @SubArea  != '' and @Sucursal != '' )   set @filtro = ' Sucursal = '''+@Sucursal+''' and Departamento = '''+@Departamento+''' and SubArea = '''+@SubArea+'''  and Mes = '''+@Mes+''' ' " +
           " if  (@Mes != '' and @Departamento != '' and @Area = '' and @SubArea = '' and @Sucursal != '')  	 set @filtro = ' Sucursal = '''+@Sucursal+''' and Departamento = '''+@Departamento+''' and Mes = '''+@Mes+'''   ' " +
           " if  (@Mes != '' and @Departamento != ''  and @Area = '' and @SubArea  != '' and @Sucursal = '' )  	  set @filtro = ' Departamento = '''+@Departamento+''' and SubArea = '''+@SubArea+'''  and Mes = '''+@Mes+'''  ' " +
           " if  (@Mes != '' and @Departamento != '' and @Area != '' and @SubArea = '' and @Sucursal = '')  	 set @filtro = '  Departamento = '''+@Departamento+''' and Area = '''+@Area+'''  and Mes = '''+@Mes+''' ' " +
           " if  (@Mes != '' and @Departamento = '' and @Area != '' and @SubArea != '' and @Sucursal != '' )  	 set @filtro = '  Sucursal = '''+@Sucursal+'''  and Area = '''+@Area+''' and SubArea = '''+@SubArea+'''  and Mes = '''+@Mes+''' ' " +
           " if  (@Mes != '' and @Departamento = ''  and @Area = '' and @SubArea = '' and @Sucursal = ''  )  	  set @filtro = ' Mes = '''+@Mes+''' ' " +
           " if  (@Mes != '' and @Departamento = ''  and @Area != '' and @SubArea  = '' and @Sucursal != '' )  	 set @filtro = ' Sucursal = '''+@Sucursal+''' and Area='''+@Area+''' and Mes = '''+@Mes+'''  ' " +
           " if  (@Mes != '' and @Departamento = '' and @Area != '' and @SubArea != '' and @Sucursal = '')  	 set @filtro = ' Area = '''+@Area+''' and SubArea = '''+@SubArea+'''  and Mes = '''+@Mes+'''   ' " +
           " if  (@Mes != '' and @Departamento = ''  and @Area = '' and @SubArea  != '' and @Sucursal != '' )  	  set @filtro = '  Sucursal = '''+@Sucursal+'''  and SubArea = '''+@SubArea+'''  and Mes = '''+@Mes+''' ' " +
           " if  (@Mes != '' and @Departamento = '' and @Area = '' and @SubArea = '' and @Sucursal != '')  	 set @filtro = ' Sucursal = '''+@Sucursal+'''  and Mes = '''+@Mes+'''  ' " +
           " if  (@Mes != '' and @Departamento = ''  and @Area = '' and @SubArea  != '' and @Sucursal = '' )  	  set @filtro = '  SubArea = '''+@SubArea+'''  and Mes = '''+@Mes+''' '  " +
           " if  (@Mes != '' and @Departamento = '' and @Area != '' and @SubArea = '' and @Sucursal = '')  	 set @filtro = ' Area = '''+@Area+'''  and Mes = '''+@Mes+''' ' " +
           " if  (@Mes = '' and @Departamento != '' and @Area != '' and @SubArea != '' and @Sucursal != '' )  	 set @filtro = '  Sucursal = '''+@Sucursal+''' and Departamento = '''+@Departamento+''' and Area = '''+@Area+''' and SubArea = '''+@SubArea+''' ' " +
           " if  (@Mes = '' and @Departamento != ''  and @Area = '' and @SubArea = '' and @Sucursal = ''  )  	  set @filtro = '  Departamento = '''+@Departamento+'''  ' " +
           " if  (@Mes = '' and @Departamento != ''  and @Area != '' and @SubArea  = '' and @Sucursal != '' )  	 set @filtro = '  Sucursal = '''+@Sucursal+''' and Departamento = '''+@Departamento+''' and Area='''+@Area+'''   ' " +
           " if  (@Mes = '' and @Departamento != '' and @Area != '' and @SubArea != '' and @Sucursal = '')  	 set @filtro = '  Departamento = '''+@Departamento+''' and Area = '''+@Area+''' and SubArea = '''+@SubArea+'''   '  " +
           " if  (@Mes = '' and @Departamento != ''  and @Area = '' and @SubArea  != '' and @Sucursal != '' )  	  set @filtro = '  Sucursal = '''+@Sucursal+''' and Departamento = '''+@Departamento+''' and SubArea = '''+@SubArea+'''  ' " +
           " if  (@Mes = '' and @Departamento != '' and @Area = '' and @SubArea = '' and @Sucursal != '')  	 set @filtro = ' Sucursal = '''+@Sucursal+''' and Departamento = '''+@Departamento+''''  " +
           " if  (@Mes = '' and @Departamento != ''  and @Area = '' and @SubArea  != '' and @Sucursal = '' )  	  set @filtro = '  Departamento = '''+@Departamento+''' and SubArea = '''+@SubArea+'''' " +
           " if  (@Mes = '' and @Departamento != '' and @Area != '' and @SubArea = '' and @Sucursal = '')  	 set @filtro = '  Departamento = '''+@Departamento+''' and Area = '''+@Area+''' ' " +
           " if  (@Mes = '' and @Departamento = '' and @Area != '' and @SubArea != '' and @Sucursal != '' )  	 set @filtro = '  Sucursal = '''+@Sucursal+'''  and Area = '''+@Area+''' and SubArea = '''+@SubArea+'''  ' " +
           " if  (@Mes = '' and @Departamento = ''  and @Area != '' and @SubArea  = '' and @Sucursal != '' )  	 set @filtro = '  Sucursal = '''+@Sucursal+''' and Area='''+@Area+'''  ' " +
           " if  (@Mes = '' and @Departamento = '' and @Area != '' and @SubArea != '' and @Sucursal = '')  	 set @filtro = '  Area = '''+@Area+''' and SubArea = '''+@SubArea+''' '  " +
           " if  (@Mes = '' and @Departamento = ''  and @Area = '' and @SubArea  != '' and @Sucursal != '' )  	  set @filtro = '  Sucursal = '''+@Sucursal+'''  and SubArea = '''+@SubArea+'''  ' " +
           " if  (@Mes = '' and @Departamento = '' and @Area = '' and @SubArea = '' and @Sucursal != '')  	 set @filtro = ' Sucursal = '''+@Sucursal+''' '" +
           " if  (@Mes = '' and @Departamento = ''  and @Area = '' and @SubArea  != '' and @Sucursal = '' )  	  set @filtro = '  SubArea = '''+@SubArea+''' ' " +
           " if  (@Mes = '' and @Departamento = '' and @Area != '' and @SubArea = '' and @Sucursal = '')  	 set @filtro = ' Area = '''+@Area+'''  ' " +
           " set @consulta= 'SELECT Meses,' + @null + '  from  ( select Mes, DATENAME(MONTH,Fecha) as Meses,Anual,TotalDescansos from [Personal].[DTM_Personal_Ausentismos] where '+@filtro+' and KeyPersonalAusentismos in(select max(KeyPersonalAusentismos) from [Personal].[DTM_Personal_Ausentismos] where '+@filtro+'  group by  Mes ,Anual,Sucursal ,Departamento,Area,SubArea)   ) x  pivot ( sum(TotalDescansos) for Anual in (' + @cols + ') ) p  order by Mes' " +
           " end " +
           " execute(@consulta) ";
            return consulta;
        }
        public String Personal_AusentismoYDescanso_EvolucionGeneralMensual_ddlSucursal_SQL(String mes, String departamento, String area, String subArea)
        {        //GIAMPIERE 18/ 09 / 2019
            consulta =
                " declare @Area nvarchar(max),@SubArea nvarchar(max),@Departamento nvarchar(max),@Mes nvarchar(max),@Consulta nvarchar(max),@Filtro nvarchar(max),@select nvarchar(max);" +
          " set @Departamento= '" + departamento + "'; set @Area='" + area + "';set @SubArea='" + subArea + "'; set @Mes='" + mes + "';set @Consulta= ''; set @Filtro= ''; set @select= ''; " +
          " set @Consulta= N'select distinct Sucursal from[Personal].[DTM_Personal_Ausentismos]' " +
          " if (@Mes = ''  and @Area = '' and @SubArea = '' and @Departamento = ''  )  set @Filtro=  @Consulta " +
          " if (@Mes != ''  and @Area != '' and @SubArea != '' and @Departamento != '' )   set @Filtro =  @Consulta+ ' where Departamento = '''+@Departamento+''' and  Area = '''+@Area+''' and SubArea = '''+@SubArea+''' and Mes='''+@Mes+'''  '" +
          " if (@Mes != ''  and @Area != '' and @SubArea  = '' and @Departamento != '' ) set @Filtro =  @Consulta+' where Departamento = '''+@Departamento+''' and  Area = '''+@Area+'''  and Mes='''+@Mes+'''  ' " +
          " if (@Mes != ''  and @Area != '' and @SubArea != '' and @Departamento = '')  	set @Filtro =  @Consulta+' where Area = '''+@Area+''' and SubArea = '''+@SubArea+''' and Mes='''+@Mes+'''  ' " +
          " if (@Mes != ''  and @Area != '' and @SubArea = '' and @Departamento = '')  set @Filtro =  @Consulta+' where Area = '''+@Area+'''  and Mes='''+@Mes+''' ' " +
          " if (@Mes != ''  and @Area = '' and @SubArea = '' and @Departamento = ''  )  	  set @Filtro =  @Consulta+' where Mes='''+@Mes+'''  ' " +
          " if (@Mes != ''  and @Area = '' and @SubArea  != '' and @Departamento != '' )  	set @Filtro =  @Consulta+' where Departamento = '''+@Departamento+''' and SubArea = '''+@SubArea+''' and Mes='''+@Mes+''' ' " +
          " if (@Mes != ''  and @Area = '' and @SubArea = '' and @Departamento != '')  	set @Filtro =  @Consulta+' where Departamento = '''+@Departamento+''' and Mes='''+@Mes+'''  ' " +
          " if (@Mes != ''  and @Area = '' and @SubArea  != '' and @Departamento = '' )  	set @Filtro =  @Consulta+' where SubArea = '''+@SubArea+''' and Mes='''+@Mes+'''  ' " +
          " if (@Mes = ''  and @Area != '' and @SubArea != '' and @Departamento != '' )   set @Filtro =  @Consulta+' where Departamento = '''+@Departamento+''' and  Area = '''+@Area+''' and SubArea = '''+@SubArea+''' ' " +
          " if (@Mes = ''  and @Area != '' and @SubArea  = '' and @Departamento != '' ) set @Filtro =  @Consulta+' where Departamento = '''+@Departamento+''' and  Area = '''+@Area+'''   ' " +
          " if (@Mes = ''  and @Area != '' and @SubArea != '' and @Departamento = '')  	set @Filtro =  @Consulta+' where Area = '''+@Area+''' and SubArea = '''+@SubArea+'''  ' " +
          " if (@Mes = ''  and @Area != '' and @SubArea = '' and @Departamento = '')  set @Filtro =  @Consulta+' where Area = '''+@Area+'''  ' " +
          " if (@Mes = ''  and @Area = '' and @SubArea  != '' and @Departamento != '' )  	set @Filtro =  @Consulta+' where Departamento = '''+@Departamento+''' and SubArea = '''+@SubArea+'''  ' " +
          " if (@Mes = ''  and @Area = '' and @SubArea = '' and @Departamento != '')  	set @Filtro =  @Consulta+' where Departamento = '''+@Departamento+'''   ' " +
          " if (@Mes = ''  and @Area = '' and @SubArea  != '' and @Departamento = '' )  	set @Filtro =  @Consulta+' where SubArea = '''+@SubArea+'''  ' " +
          " set @Consulta=  @Filtro +' order by Sucursal' " +
          " execute(@Consulta)";
            return consulta;
        }
        public String Personal_AusentismoYDescanso_EvolucionGeneralMensual_ddlDepartamento_SQL(String mes, String area, String subArea, String sucursal)
        {        //GIAMPIERE 18/ 09 / 2019
            consulta =
          " declare @Area nvarchar(max),@SubArea nvarchar(max),@Sucursal nvarchar(max),@Mes nvarchar(max),@Consulta nvarchar(max),@Filtro nvarchar(max),@select nvarchar(max);" +
          " set @Area='" + area + "';set @SubArea='" + subArea + "'; set @Sucursal= '" + sucursal + "'; set @Mes='" + mes + "'; set @Consulta= ''; set @Filtro= ''; set @select= ''; " +
          " set @Consulta= N'select distinct Departamento from[Personal].[DTM_Personal_Ausentismos]'" +
          " if (@Mes = ''  and @Area = '' and @SubArea = '' and @Sucursal = ''  )  set @Filtro=  @Consulta " +
          " if (@Mes != ''  and @Area != '' and @SubArea != '' and @Sucursal != '' )   set @Filtro =  @Consulta+ ' where Sucursal = '''+@Sucursal+''' and  Area = '''+@Area+''' and SubArea = '''+@SubArea+''' and Mes='''+@Mes+'''  ' " +
          " if (@Mes != ''  and @Area != '' and @SubArea  = '' and @Sucursal != '' ) set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and  Area = '''+@Area+'''  and Mes='''+@Mes+'''  '" +
          " if (@Mes != ''  and @Area != '' and @SubArea != '' and @Sucursal = '')  	set @Filtro =  @Consulta+' where Area = '''+@Area+''' and SubArea = '''+@SubArea+''' and Mes='''+@Mes+'''  ' " +
          " if (@Mes != ''  and @Area != '' and @SubArea = '' and @Sucursal = '')  set @Filtro =  @Consulta+' where Area = '''+@Area+'''  and Mes='''+@Mes+''' '  " +
          " if (@Mes != ''  and @Area = '' and @SubArea = '' and @Sucursal = ''  )  	  set @Filtro =  @Consulta+' where Mes='''+@Mes+'''  ' " +
          " if (@Mes != ''  and @Area = '' and @SubArea  != '' and @Sucursal != '' )  	set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and SubArea = '''+@SubArea+''' and Mes='''+@Mes+''' '  " +
          " if (@Mes != ''  and @Area = '' and @SubArea = '' and @Sucursal != '')  	set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and Mes='''+@Mes+'''  ' " +
          " if (@Mes != ''  and @Area = '' and @SubArea  != '' and @Sucursal = '' )  	set @Filtro =  @Consulta+' where SubArea = '''+@SubArea+''' and Mes='''+@Mes+'''  '" +
          " if (@Mes = ''  and @Area != '' and @SubArea != '' and @Sucursal != '' )   set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and  Area = '''+@Area+''' and SubArea = '''+@SubArea+''' ' " +
          " if (@Mes = ''  and @Area != '' and @SubArea  = '' and @Sucursal != '' ) set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and  Area = '''+@Area+'''   '" +
          " if (@Mes = ''  and @Area != '' and @SubArea != '' and @Sucursal = '')  	set @Filtro =  @Consulta+' where Area = '''+@Area+''' and SubArea = '''+@SubArea+'''  '" +
          " if (@Mes = ''  and @Area != '' and @SubArea = '' and @Sucursal = '')  set @Filtro =  @Consulta+' where Area = '''+@Area+'''  '" +
          " if (@Mes = ''  and @Area = '' and @SubArea  != '' and @Sucursal != '' )  	set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and SubArea = '''+@SubArea+'''  ' " +
          " if (@Mes = ''  and @Area = '' and @SubArea = '' and @Sucursal != '')  	set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+'''   ' " +
          " if (@Mes = ''  and @Area = '' and @SubArea  != '' and @Sucursal = '' )  	set @Filtro =  @Consulta+' where SubArea = '''+@SubArea+'''  '" +
          " set @Consulta=  @Filtro +' order by Departamento'" +
          " execute(@Consulta);";
            return consulta;
        }
        public String Personal_AusentismoYDescanso_EvolucionGeneralMensual_ddlArea_SQL(String mes, String departamento, String subArea, String sucursal)
        {        //GIAMPIERE 18/ 09 / 2019
            consulta =
          " declare @SubArea nvarchar(max),@Departamento nvarchar(max),@Sucursal nvarchar(max),@Mes nvarchar(max),@Consulta nvarchar(max),@Filtro nvarchar(max),@select nvarchar(max);" +
          " set @Departamento= '" + departamento + "';set @SubArea='" + subArea + "'; set @Sucursal= '" + sucursal + "'; set @Mes='" + mes + "'; set @Consulta= ''; set @Filtro= ''; set @select= ''; " +
          " set @Consulta= N'select distinct Area from[Personal].[DTM_Personal_Ausentismos]'" +
          " if (@Mes = ''  and @SubArea = '' and @Departamento = '' and @Sucursal = ''  )  set @Filtro=  @Consulta " +
          " if (@Mes != ''  and @SubArea != '' and @Departamento != '' and @Sucursal != '' )   set @Filtro =  @Consulta+ ' where Sucursal = '''+@Sucursal+''' and  SubArea = '''+@SubArea+''' and Departamento = '''+@Departamento+''' and Mes='''+@Mes+'''  ' " +
          " if (@Mes != ''  and @SubArea != '' and @Departamento  = '' and @Sucursal != '' ) set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and  SubArea = '''+@SubArea+'''  and Mes='''+@Mes+'''  '" +
          " if (@Mes != ''  and @SubArea != '' and @Departamento != '' and @Sucursal = '')  	set @Filtro =  @Consulta+' where SubArea = '''+@SubArea+''' and Departamento = '''+@Departamento+''' and Mes='''+@Mes+'''  ' " +
          " if (@Mes != ''  and @SubArea != '' and @Departamento = '' and @Sucursal = '')  set @Filtro =  @Consulta+' where SubArea = '''+@SubArea+'''  and Mes='''+@Mes+''' '  " +
          " if (@Mes != ''  and @SubArea = '' and @Departamento = '' and @Sucursal = ''  )  	  set @Filtro =  @Consulta+' where Mes='''+@Mes+'''  ' " +
          " if (@Mes != ''  and @SubArea = '' and @Departamento  != '' and @Sucursal != '' )  	set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and Departamento = '''+@Departamento+''' and Mes='''+@Mes+''' '  " +
          " if (@Mes != ''  and @SubArea = '' and @Departamento = '' and @Sucursal != '')  	set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and Mes='''+@Mes+'''  ' " +
          " if (@Mes != ''  and @SubArea = '' and @Departamento  != '' and @Sucursal = '' )  	set @Filtro =  @Consulta+' where Departamento = '''+@Departamento+''' and Mes='''+@Mes+'''  '" +
          " if (@Mes = ''  and @SubArea != '' and @Departamento != '' and @Sucursal != '' )   set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and  SubArea = '''+@SubArea+''' and Departamento = '''+@Departamento+''' ' " +
          " if (@Mes = ''  and @SubArea != '' and @Departamento  = '' and @Sucursal != '' ) set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and  SubArea = '''+@SubArea+'''   '" +
          " if (@Mes = ''  and @SubArea != '' and @Departamento != '' and @Sucursal = '')  	set @Filtro =  @Consulta+' where SubArea = '''+@SubArea+''' and Departamento = '''+@Departamento+'''  '" +
          " if (@Mes = ''  and @SubArea != '' and @Departamento = '' and @Sucursal = '')  set @Filtro =  @Consulta+' where SubArea = '''+@SubArea+'''  '" +
          " if (@Mes = ''  and @SubArea = '' and @Departamento  != '' and @Sucursal != '' )  	set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and Departamento = '''+@Departamento+'''  ' " +
          " if (@Mes = ''  and @SubArea = '' and @Departamento = '' and @Sucursal != '')  	set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+'''   ' " +
          " if (@Mes = ''  and @SubArea = '' and @Departamento  != '' and @Sucursal = '' )  	set @Filtro =  @Consulta+' where Departamento = '''+@Departamento+'''  '" +
          " set @Consulta=  @Filtro +' order by Area'" +
          " execute(@Consulta)";

            return consulta;
        }
        public String Personal_AusentismoYDescanso_EvolucionGeneralMensual_ddlSubArea_SQL(String mes, String departamento, String area, String sucursal)
        {        //GIAMPIERE 18/ 09 / 2019
            consulta =
          " declare @Area nvarchar(max),@Departamento nvarchar(max),@Sucursal nvarchar(max),@Mes nvarchar(max),@Consulta nvarchar(max),@Filtro nvarchar(max),@select nvarchar(max);" +
          " set @Departamento= '" + departamento + "'; set @Area='" + area + "';set @Sucursal= '" + sucursal + "'; set @Mes='" + mes + "'; set @Consulta= ''; set @Filtro= ''; set @select= ''; " +
          " set @Consulta= N'select distinct SubArea from[Personal].[DTM_Personal_Ausentismos]'" +
          " if (@Mes = ''  and @Area = '' and @Departamento = '' and @Sucursal = ''  )  set @Filtro=  @Consulta " +
          " if (@Mes != ''  and @Area != '' and @Departamento != '' and @Sucursal != '' )   set @Filtro =  @Consulta+ ' where Sucursal = '''+@Sucursal+''' and  Area = '''+@Area+''' and Departamento = '''+@Departamento+''' and Mes='''+@Mes+'''  ' " +
          " if (@Mes != ''  and @Area != '' and @Departamento  = '' and @Sucursal != '' ) set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and  Area = '''+@Area+'''  and Mes='''+@Mes+'''  '" +
          " if (@Mes != ''  and @Area != '' and @Departamento != '' and @Sucursal = '')  	set @Filtro =  @Consulta+' where Area = '''+@Area+''' and Departamento = '''+@Departamento+''' and Mes='''+@Mes+'''  ' " +
          " if (@Mes != ''  and @Area != '' and @Departamento = '' and @Sucursal = '')  set @Filtro =  @Consulta+' where Area = '''+@Area+'''  and Mes='''+@Mes+''' '  " +
          " if (@Mes != ''  and @Area = '' and @Departamento = '' and @Sucursal = ''  )  	  set @Filtro =  @Consulta+' where Mes='''+@Mes+'''  ' " +
          " if (@Mes != ''  and @Area = '' and @Departamento  != '' and @Sucursal != '' )  	set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and Departamento = '''+@Departamento+''' and Mes='''+@Mes+''' '  " +
          " if (@Mes != ''  and @Area = '' and @Departamento = '' and @Sucursal != '')  	set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and Mes='''+@Mes+'''  ' " +
          " if (@Mes != ''  and @Area = '' and @Departamento  != '' and @Sucursal = '' )  	set @Filtro =  @Consulta+' where Departamento = '''+@Departamento+''' and Mes='''+@Mes+'''  '" +
          " if (@Mes = ''  and @Area != '' and @Departamento != '' and @Sucursal != '' )   set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and  Area = '''+@Area+''' and Departamento = '''+@Departamento+''' ' " +
          " if (@Mes = ''  and @Area != '' and @Departamento  = '' and @Sucursal != '' ) set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and  Area = '''+@Area+'''   '" +
          " if (@Mes = ''  and @Area != '' and @Departamento != '' and @Sucursal = '')  	set @Filtro =  @Consulta+' where Area = '''+@Area+''' and Departamento = '''+@Departamento+'''  '" +
          " if (@Mes = ''  and @Area != '' and @Departamento = '' and @Sucursal = '')  set @Filtro =  @Consulta+' where Area = '''+@Area+'''  '" +
          " if (@Mes = ''  and @Area = '' and @Departamento  != '' and @Sucursal != '' )  	set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and Departamento = '''+@Departamento+'''  ' " +
          " if (@Mes = ''  and @Area = '' and @Departamento = '' and @Sucursal != '')  	set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+'''   ' " +
          " if (@Mes = ''  and @Area = '' and @Departamento  != '' and @Sucursal = '' )  	set @Filtro =  @Consulta+' where Departamento = '''+@Departamento+'''  '" +
          " set @Consulta=  @Filtro +' order by SubArea'" +
          " execute(@Consulta)";
            return consulta;

        }
        public String Personal_AusentismoYDescanso_EvolucionGeneralMensual_ddlMes_SQL(String departamento, String area, String subArea, String sucursal)
        {   //GIAMPIERE 18/ 09 / 2019
            consulta =
          " declare @SubArea nvarchar(max),@Departamento nvarchar(max),@Sucursal nvarchar(max),@Area nvarchar(max),@Consulta nvarchar(max),@Filtro nvarchar(max),@select nvarchar(max);" +
         " set @Departamento= '" + departamento + "'; set @Area='" + area + "';set @SubArea='" + subArea + "'; set @Sucursal= '" + sucursal + "';set @Consulta= ''; set @Filtro= ''; set @select= ''; " +
          " set @Consulta= N'select distinct DATENAME(MONTH,Fecha) as Meses ,Mes from[Personal].[DTM_Personal_Ausentismos]'" +
          " if (@Area = ''  and @SubArea = '' and @Departamento = '' and @Sucursal = ''  )  set @Filtro=  @Consulta " +
          " if (@Area != ''  and @SubArea != '' and @Departamento != '' and @Sucursal != '' )   set @Filtro =  @Consulta+ ' where Sucursal = '''+@Sucursal+''' and  SubArea = '''+@SubArea+''' and Departamento = '''+@Departamento+''' and Area='''+@Area+'''  ' " +
          " if (@Area != ''  and @SubArea != '' and @Departamento  = '' and @Sucursal != '' ) set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and  SubArea = '''+@SubArea+'''  and Area='''+@Area+'''  '" +
          " if (@Area != ''  and @SubArea != '' and @Departamento != '' and @Sucursal = '')  	set @Filtro =  @Consulta+' where SubArea = '''+@SubArea+''' and Departamento = '''+@Departamento+''' and Area='''+@Area+'''  ' " +
          " if (@Area != ''  and @SubArea != '' and @Departamento = '' and @Sucursal = '')  set @Filtro =  @Consulta+' where SubArea = '''+@SubArea+'''  and Area='''+@Area+''' '  " +
          " if (@Area != ''  and @SubArea = '' and @Departamento = '' and @Sucursal = ''  )  	  set @Filtro =  @Consulta+' where Area='''+@Area+'''  ' " +
          " if (@Area != ''  and @SubArea = '' and @Departamento  != '' and @Sucursal != '' )  	set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and Departamento = '''+@Departamento+''' and Area='''+@Area+''' '  " +
          " if (@Area != ''  and @SubArea = '' and @Departamento = '' and @Sucursal != '')  	set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and Area='''+@Area+'''  ' " +
          " if (@Area != ''  and @SubArea = '' and @Departamento  != '' and @Sucursal = '' )  	set @Filtro =  @Consulta+' where Departamento = '''+@Departamento+''' and Area='''+@Area+'''  '" +
          " if (@Area = ''  and @SubArea != '' and @Departamento != '' and @Sucursal != '' )   set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and  SubArea = '''+@SubArea+''' and Departamento = '''+@Departamento+''' ' " +
          " if (@Area = ''  and @SubArea != '' and @Departamento  = '' and @Sucursal != '' ) set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and  SubArea = '''+@SubArea+'''   '" +
          " if (@Area = ''  and @SubArea != '' and @Departamento != '' and @Sucursal = '')  	set @Filtro =  @Consulta+' where SubArea = '''+@SubArea+''' and Departamento = '''+@Departamento+'''  '" +
          " if (@Area = ''  and @SubArea != '' and @Departamento = '' and @Sucursal = '')  set @Filtro =  @Consulta+' where SubArea = '''+@SubArea+'''  '" +
          " if (@Area = ''  and @SubArea = '' and @Departamento  != '' and @Sucursal != '' )  	set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and Departamento = '''+@Departamento+'''  ' " +
          " if (@Area = ''  and @SubArea = '' and @Departamento = '' and @Sucursal != '')  	set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+'''   ' " +
          " if (@Area = ''  and @SubArea = '' and @Departamento  != '' and @Sucursal = '' )  	set @Filtro =  @Consulta+' where Departamento = '''+@Departamento+'''  '" +
          " set @Consulta=  @Filtro +' order by Mes'" +
          " execute(@Consulta)";
            return consulta;
        }
        //EVOLUCIÓN POR ÁREA MENSUAL
        public String G_Personal_Ausentismo_EvolucionAreaMensual_SQL(String anio, String mes, String area, String subArea, String sucursal)
        {        //GIAMPIERE 18/ 09 / 2019
            consulta =
                " declare @Departamento varchar(max),@Año nvarchar(max), @Area nvarchar(max), @Mes nvarchar(3),@SubArea nvarchar(max),@Sucursal nvarchar(max), @filtro  NVARCHAR(MAX), @cols NVARCHAR(MAX), @null  NVARCHAR(MAX), @consulta  NVARCHAR(MAX); " +
            " set @Año='" + anio + "'; set @Area='" + area + "';set @SubArea='" + subArea + "'; set @Sucursal= '" + sucursal + "'; set @Mes='" + mes + "';set @consulta ='';set @filtro =''; " +
            " select @cols = STUFF((SELECT Distinct ',' + QUOTENAME(Departamento)  from  [Personal].[DTM_Personal_Ausentismos] group by ',' + QUOTENAME(Departamento)Order by ',' + QUOTENAME(Departamento) FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)')  ,1,1,'') " +
            " select @null = STUFF((SELECT Distinct ', isnull(' + QUOTENAME(Departamento)+',0) as'  + QUOTENAME(Departamento) from  [Personal].[DTM_Personal_Ausentismos]  group by ', isnull(' + QUOTENAME(Departamento)+',0) as'  + QUOTENAME(Departamento) Order by ', isnull(' + QUOTENAME(Departamento)+',0) as'  + QUOTENAME(Departamento)   FOR XML PATH(''), TYPE  ).value('.', 'NVARCHAR(MAX)')   ,1,1,'')  " +
            " if  (@Mes = '' and @Año = ''  and @Area = '' and @SubArea = '' and @Sucursal = ''  )  	  set @consulta = 'SELECT Meses,' + @null + '  from  ( select Mes,DATENAME(MONTH,Fecha) as Meses, Departamento,TotalInasistencias from [Personal].[DTM_Personal_Ausentismos]   where KeyPersonalAusentismos in(select max(KeyPersonalAusentismos) from [Personal].[DTM_Personal_Ausentismos] group by Mes ,Anual, Departamento,Area,SubArea,Sucursal) )  x  pivot ( sum(TotalInasistencias) for Departamento in (' + @cols + ') ) p  order by Mes ' " +
            " else " +
            " begin " +
            " if  (@Mes != '' and @Año != '' and @Area != '' and @SubArea != '' and @Sucursal != '' )  	 set @filtro = ' Sucursal = '''+@Sucursal+''' and Anual = '''+@Año+''' and Area = '''+@Area+''' and SubArea = '''+@SubArea+'''  and Mes = '''+@Mes+''' ' " +
            " if  (@Mes != '' and @Año != ''  and @Area = '' and @SubArea = '' and @Sucursal = ''  )  	  set @filtro = ' Anual = '''+@Año+'''  and Mes = '''+@Mes+''' ' " +
            " if  (@Mes != '' and @Año != ''  and @Area != '' and @SubArea  = '' and @Sucursal != '' )  	 set @filtro = ' Sucursal = '''+@Sucursal+''' and Anual = '''+@Año+''' and Area='''+@Area+'''  and Mes = '''+@Mes+''' ' " +
            " if  (@Mes != '' and @Año != '' and @Area != '' and @SubArea != '' and @Sucursal = '')  	 set @filtro = ' Anual = '''+@Año+''' and Area = '''+@Area+''' and SubArea = '''+@SubArea+'''  and Mes = '''+@Mes+''' ' " +
            " if  (@Mes != '' and @Año != ''  and @Area = '' and @SubArea  != '' and @Sucursal != '' )  	  set @filtro = '  Sucursal = '''+@Sucursal+''' and Anual = '''+@Año+''' and SubArea = '''+@SubArea+'''  and Mes = '''+@Mes+'''' " +
            " if  (@Mes != '' and @Año != '' and @Area = '' and @SubArea = '' and @Sucursal != '')  	 set @filtro = ' Sucursal = '''+@Sucursal+''' and Anual = '''+@Año+''' and Mes = '''+@Mes+'''  ' " +
            " if  (@Mes != '' and @Año != ''  and @Area = '' and @SubArea  != '' and @Sucursal = '' )  	  set @filtro = '  Anual = '''+@Año+''' and SubArea = '''+@SubArea+'''  and Mes = '''+@Mes+''' ' " +
            " if  (@Mes != '' and @Año != '' and @Area != '' and @SubArea = '' and @Sucursal = '')  	 set @filtro = ' Anual = '''+@Año+''' and Area = '''+@Area+'''  and Mes = '''+@Mes+'''' " +
            " if  (@Mes != '' and @Año = '' and @Area != '' and @SubArea != '' and @Sucursal != '' )  	 set @filtro = ' Sucursal = '''+@Sucursal+'''  and Area = '''+@Area+''' and SubArea = '''+@SubArea+'''  and Mes = '''+@Mes+''' ' " +
            " if  (@Mes != '' and @Año = ''  and @Area = '' and @SubArea = '' and @Sucursal = ''  )  	  set @filtro = ' Mes = '''+@Mes+''' ' " +
            " if  (@Mes != '' and @Año = ''  and @Area != '' and @SubArea  = '' and @Sucursal != '' )  	 set @filtro = ' Sucursal = '''+@Sucursal+''' and Area='''+@Area+''' and Mes = '''+@Mes+''' ' " +
            " if  (@Mes != '' and @Año = '' and @Area != '' and @SubArea != '' and @Sucursal = '')  	 set @filtro = ' Area = '''+@Area+''' and SubArea = '''+@SubArea+'''  and Mes = '''+@Mes+''' ' " +
            " if  (@Mes != '' and @Año = ''  and @Area = '' and @SubArea  != '' and @Sucursal != '' )  	  set @filtro = '  Sucursal = '''+@Sucursal+'''  and SubArea = '''+@SubArea+'''  and Mes = '''+@Mes+''' ' " +
            " if  (@Mes != '' and @Año = '' and @Area = '' and @SubArea = '' and @Sucursal != '')  	 set @filtro = ' Sucursal = '''+@Sucursal+'''  and Mes = '''+@Mes+''' ' " +
            " if  (@Mes != '' and @Año = ''  and @Area = '' and @SubArea  != '' and @Sucursal = '' )  	  set @filtro = '  SubArea = '''+@SubArea+'''  and Mes = '''+@Mes+''' ' " +
            " if  (@Mes != '' and @Año = '' and @Area != '' and @SubArea = '' and @Sucursal = '')  	 set @filtro = 'Area = '''+@Area+'''  and Mes = '''+@Mes+''' '" +
            " if  (@Mes = '' and @Año != '' and @Area != '' and @SubArea != '' and @Sucursal != '' )  	 set @filtro = ' Sucursal = '''+@Sucursal+''' and Anual = '''+@Año+''' and Area = '''+@Area+''' and SubArea = '''+@SubArea+''' ' " +
            " if  (@Mes = '' and @Año != ''  and @Area = '' and @SubArea = '' and @Sucursal = ''  )  	  set @filtro = ' Anual = '''+@Año+''''" +
            " if  (@Mes = '' and @Año != ''  and @Area != '' and @SubArea  = '' and @Sucursal != '' )  	 set @filtro = ' Sucursal = '''+@Sucursal+''' and Anual = '''+@Año+''' and Area='''+@Area+''' ' " +
            " if  (@Mes = '' and @Año != '' and @Area != '' and @SubArea != '' and @Sucursal = '')  	 set @filtro = ' Anual = '''+@Año+''' and Area = '''+@Area+''' and SubArea = '''+@SubArea+''' ' " +
            " if  (@Mes = '' and @Año != ''  and @Area = '' and @SubArea  != '' and @Sucursal != '' )  	  set @filtro = '  Sucursal = '''+@Sucursal+''' and Anual = '''+@Año+''' and SubArea = '''+@SubArea+''' ' " +
            " if  (@Mes = '' and @Año != '' and @Area = '' and @SubArea = '' and @Sucursal != '')  	 set @filtro = ' Sucursal = '''+@Sucursal+''' and Anual = '''+@Año+''' ' " +
            " if  (@Mes = '' and @Año != ''  and @Area = '' and @SubArea  != '' and @Sucursal = '' )  	  set @filtro = '  Anual = '''+@Año+''' and SubArea = '''+@SubArea+''' ' " +
            " if  (@Mes = '' and @Año != '' and @Area != '' and @SubArea = '' and @Sucursal = '')  	 set @filtro = ' Anual = '''+@Año+''' and Area = '''+@Area+''' ' " +
            " if  (@Mes = '' and @Año = '' and @Area != '' and @SubArea != '' and @Sucursal != '' )  	 set @filtro = ' Sucursal = '''+@Sucursal+'''  and Area = '''+@Area+''' and SubArea = '''+@SubArea+''' ' " +
            " if  (@Mes = '' and @Año = ''  and @Area != '' and @SubArea  = '' and @Sucursal != '' )  	 set @filtro = ' Sucursal = '''+@Sucursal+''' and Area='''+@Area+''' ' " +
            " if  (@Mes = '' and @Año = '' and @Area != '' and @SubArea != '' and @Sucursal = '')  	 set @filtro = ' Area = '''+@Area+''' and SubArea = '''+@SubArea+''' ' " +
            " if  (@Mes = '' and @Año = ''  and @Area = '' and @SubArea  != '' and @Sucursal != '' )  	  set @filtro = '  Sucursal = '''+@Sucursal+'''  and SubArea = '''+@SubArea+''' ' " +
            " if  (@Mes = '' and @Año = '' and @Area = '' and @SubArea = '' and @Sucursal != '')  	 set @filtro = ' Sucursal = '''+@Sucursal+''' ' " +
            " if  (@Mes = '' and @Año = ''  and @Area = '' and @SubArea  != '' and @Sucursal = '' )  	  set @filtro = '  SubArea = '''+@SubArea+''' ' " +
            " if  (@Mes = '' and @Año = '' and @Area != '' and @SubArea = '' and @Sucursal = '')  	 set @filtro = 'Area = '''+@Area+''' ' " +
            " set @consulta = 'SELECT Meses,' + @null + '  from  ( select Mes,DATENAME(MONTH,Fecha) as Meses, Departamento,TotalInasistencias from [Personal].[DTM_Personal_Ausentismos]   where ' +@filtro+ ' and KeyPersonalAusentismos in(select max(KeyPersonalAusentismos) from [Personal].[DTM_Personal_Ausentismos] where ' +@filtro+ '  group by Mes ,Anual, Departamento,Area,SubArea,Sucursal) )  x  pivot ( sum(TotalInasistencias) for Departamento in (' + @cols + ') ) p  order by Mes ' " +
            " end " +
            " execute(@consulta)";
            return consulta;  
        }   

        public String G_Personal_Descanso_EvolucionAreaMensual_SQL(String anio, String mes, String area, String subArea, String sucursal)
        {        //GIAMPIERE 18/ 09 / 2019
            consulta =
                " declare @Departamento varchar(max),@Año nvarchar(max), @Area nvarchar(max), @Mes nvarchar(3),@SubArea nvarchar(max),@Sucursal nvarchar(max), @filtro  NVARCHAR(MAX), @cols NVARCHAR(MAX), @null  NVARCHAR(MAX), @consulta  NVARCHAR(MAX); " +
            " set @Año='" + anio + "'; set @Area='" + area + "';set @SubArea='" + subArea + "'; set @Sucursal= '" + sucursal + "'; set @Mes='" + mes + "';set @consulta ='';set @filtro =''; " +
            " select @cols = STUFF((SELECT Distinct ',' + QUOTENAME(Departamento)  from  [Personal].[DTM_Personal_Ausentismos] group by ',' + QUOTENAME(Departamento)Order by ',' + QUOTENAME(Departamento) FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)')  ,1,1,'') " +
            " select @null = STUFF((SELECT Distinct ', isnull(' + QUOTENAME(Departamento)+',0) as'  + QUOTENAME(Departamento) from  [Personal].[DTM_Personal_Ausentismos]  group by ', isnull(' + QUOTENAME(Departamento)+',0) as'  + QUOTENAME(Departamento) Order by ', isnull(' + QUOTENAME(Departamento)+',0) as'  + QUOTENAME(Departamento)   FOR XML PATH(''), TYPE  ).value('.', 'NVARCHAR(MAX)')   ,1,1,'')  " +
            " if  (@Mes = '' and @Año = ''  and @Area = '' and @SubArea = '' and @Sucursal = ''  )  	  set @consulta = 'SELECT Meses,' + @null + '  from  ( select Mes,DATENAME(MONTH,Fecha) as Meses, Departamento,TotalDescansos from [Personal].[DTM_Personal_Ausentismos]   where KeyPersonalAusentismos in(select max(KeyPersonalAusentismos) from [Personal].[DTM_Personal_Ausentismos] group by Mes ,Anual, Departamento,Area,SubArea,Sucursal) )  x  pivot ( sum(TotalDescansos) for Departamento in (' + @cols + ') ) p  order by Mes ' " +
            " else " +
            " begin " +
            " if  (@Mes != '' and @Año != '' and @Area != '' and @SubArea != '' and @Sucursal != '' )  	 set @filtro = ' Sucursal = '''+@Sucursal+''' and Anual = '''+@Año+''' and Area = '''+@Area+''' and SubArea = '''+@SubArea+'''  and Mes = '''+@Mes+''' ' " +
            " if  (@Mes != '' and @Año != ''  and @Area = '' and @SubArea = '' and @Sucursal = ''  )  	  set @filtro = ' Anual = '''+@Año+'''  and Mes = '''+@Mes+''' ' " +
            " if  (@Mes != '' and @Año != ''  and @Area != '' and @SubArea  = '' and @Sucursal != '' )  	 set @filtro = ' Sucursal = '''+@Sucursal+''' and Anual = '''+@Año+''' and Area='''+@Area+'''  and Mes = '''+@Mes+''' ' " +
            " if  (@Mes != '' and @Año != '' and @Area != '' and @SubArea != '' and @Sucursal = '')  	 set @filtro = ' Anual = '''+@Año+''' and Area = '''+@Area+''' and SubArea = '''+@SubArea+'''  and Mes = '''+@Mes+''' ' " +
            " if  (@Mes != '' and @Año != ''  and @Area = '' and @SubArea  != '' and @Sucursal != '' )  	  set @filtro = '  Sucursal = '''+@Sucursal+''' and Anual = '''+@Año+''' and SubArea = '''+@SubArea+'''  and Mes = '''+@Mes+'''' " +
            " if  (@Mes != '' and @Año != '' and @Area = '' and @SubArea = '' and @Sucursal != '')  	 set @filtro = ' Sucursal = '''+@Sucursal+''' and Anual = '''+@Año+''' and Mes = '''+@Mes+'''  ' " +
            " if  (@Mes != '' and @Año != ''  and @Area = '' and @SubArea  != '' and @Sucursal = '' )  	  set @filtro = '  Anual = '''+@Año+''' and SubArea = '''+@SubArea+'''  and Mes = '''+@Mes+''' ' " +
            " if  (@Mes != '' and @Año != '' and @Area != '' and @SubArea = '' and @Sucursal = '')  	 set @filtro = ' Anual = '''+@Año+''' and Area = '''+@Area+'''  and Mes = '''+@Mes+'''' " +
            " if  (@Mes != '' and @Año = '' and @Area != '' and @SubArea != '' and @Sucursal != '' )  	 set @filtro = ' Sucursal = '''+@Sucursal+'''  and Area = '''+@Area+''' and SubArea = '''+@SubArea+'''  and Mes = '''+@Mes+''' ' " +
            " if  (@Mes != '' and @Año = ''  and @Area = '' and @SubArea = '' and @Sucursal = ''  )  	  set @filtro = ' Mes = '''+@Mes+''' ' " +
            " if  (@Mes != '' and @Año = ''  and @Area != '' and @SubArea  = '' and @Sucursal != '' )  	 set @filtro = ' Sucursal = '''+@Sucursal+''' and Area='''+@Area+''' and Mes = '''+@Mes+''' ' " +
            " if  (@Mes != '' and @Año = '' and @Area != '' and @SubArea != '' and @Sucursal = '')  	 set @filtro = ' Area = '''+@Area+''' and SubArea = '''+@SubArea+'''  and Mes = '''+@Mes+''' ' " +
            " if  (@Mes != '' and @Año = ''  and @Area = '' and @SubArea  != '' and @Sucursal != '' )  	  set @filtro = '  Sucursal = '''+@Sucursal+'''  and SubArea = '''+@SubArea+'''  and Mes = '''+@Mes+''' ' " +
            " if  (@Mes != '' and @Año = '' and @Area = '' and @SubArea = '' and @Sucursal != '')  	 set @filtro = ' Sucursal = '''+@Sucursal+'''  and Mes = '''+@Mes+''' ' " +
            " if  (@Mes != '' and @Año = ''  and @Area = '' and @SubArea  != '' and @Sucursal = '' )  	  set @filtro = '  SubArea = '''+@SubArea+'''  and Mes = '''+@Mes+''' ' " +
            " if  (@Mes != '' and @Año = '' and @Area != '' and @SubArea = '' and @Sucursal = '')  	 set @filtro = 'Area = '''+@Area+'''  and Mes = '''+@Mes+''' '" +
            " if  (@Mes = '' and @Año != '' and @Area != '' and @SubArea != '' and @Sucursal != '' )  	 set @filtro = ' Sucursal = '''+@Sucursal+''' and Anual = '''+@Año+''' and Area = '''+@Area+''' and SubArea = '''+@SubArea+''' ' " +
            " if  (@Mes = '' and @Año != ''  and @Area = '' and @SubArea = '' and @Sucursal = ''  )  	  set @filtro = ' Anual = '''+@Año+''''" +
            " if  (@Mes = '' and @Año != ''  and @Area != '' and @SubArea  = '' and @Sucursal != '' )  	 set @filtro = ' Sucursal = '''+@Sucursal+''' and Anual = '''+@Año+''' and Area='''+@Area+''' ' " +
            " if  (@Mes = '' and @Año != '' and @Area != '' and @SubArea != '' and @Sucursal = '')  	 set @filtro = ' Anual = '''+@Año+''' and Area = '''+@Area+''' and SubArea = '''+@SubArea+''' ' " +
            " if  (@Mes = '' and @Año != ''  and @Area = '' and @SubArea  != '' and @Sucursal != '' )  	  set @filtro = '  Sucursal = '''+@Sucursal+''' and Anual = '''+@Año+''' and SubArea = '''+@SubArea+''' ' " +
            " if  (@Mes = '' and @Año != '' and @Area = '' and @SubArea = '' and @Sucursal != '')  	 set @filtro = ' Sucursal = '''+@Sucursal+''' and Anual = '''+@Año+''' ' " +
            " if  (@Mes = '' and @Año != ''  and @Area = '' and @SubArea  != '' and @Sucursal = '' )  	  set @filtro = '  Anual = '''+@Año+''' and SubArea = '''+@SubArea+''' ' " +
            " if  (@Mes = '' and @Año != '' and @Area != '' and @SubArea = '' and @Sucursal = '')  	 set @filtro = ' Anual = '''+@Año+''' and Area = '''+@Area+''' ' " +
            " if  (@Mes = '' and @Año = '' and @Area != '' and @SubArea != '' and @Sucursal != '' )  	 set @filtro = ' Sucursal = '''+@Sucursal+'''  and Area = '''+@Area+''' and SubArea = '''+@SubArea+''' ' " +
            " if  (@Mes = '' and @Año = ''  and @Area != '' and @SubArea  = '' and @Sucursal != '' )  	 set @filtro = ' Sucursal = '''+@Sucursal+''' and Area='''+@Area+''' ' " +
            " if  (@Mes = '' and @Año = '' and @Area != '' and @SubArea != '' and @Sucursal = '')  	 set @filtro = ' Area = '''+@Area+''' and SubArea = '''+@SubArea+''' ' " +
            " if  (@Mes = '' and @Año = ''  and @Area = '' and @SubArea  != '' and @Sucursal != '' )  	  set @filtro = '  Sucursal = '''+@Sucursal+'''  and SubArea = '''+@SubArea+''' ' " +
            " if  (@Mes = '' and @Año = '' and @Area = '' and @SubArea = '' and @Sucursal != '')  	 set @filtro = ' Sucursal = '''+@Sucursal+''' ' " +
            " if  (@Mes = '' and @Año = ''  and @Area = '' and @SubArea  != '' and @Sucursal = '' )  	  set @filtro = '  SubArea = '''+@SubArea+''' ' " +
            " if  (@Mes = '' and @Año = '' and @Area != '' and @SubArea = '' and @Sucursal = '')  	 set @filtro = 'Area = '''+@Area+''' ' " +
            " set @consulta = 'SELECT Meses,' + @null + '  from  ( select Mes,DATENAME(MONTH,Fecha) as Meses, Departamento,TotalDescansos from [Personal].[DTM_Personal_Ausentismos]   where ' +@filtro+ ' and KeyPersonalAusentismos in(select max(KeyPersonalAusentismos) from [Personal].[DTM_Personal_Ausentismos] where ' +@filtro+ '  group by Mes ,Anual, Departamento,Area,SubArea,Sucursal) )  x  pivot ( sum(TotalDescansos) for Departamento in (' + @cols + ') ) p  order by Mes ' " +
            " end " +
            " execute(@consulta)";
            return consulta;
        }       
        public String Personal_AusentismoYDescanso_EvolucionAreaMensual_ddlSucursal_SQL(String anio, String mes, String area, String subArea)
        {        //GIAMPIERE 18/ 09 / 2019
            consulta =
                 " declare @Año varchar(20), @Area nvarchar(max),@SubArea nvarchar(max),@Mes nvarchar(max),@Consulta nvarchar(max),@filtro nvarchar(max),@select nvarchar(max); " +
            " set @Año='" + anio + "'; set @Area='" + area + "';set @SubArea='" + subArea + "'; set @Mes='" + mes + "';set @Consulta= ''; set @filtro= ''; set @select= ''; " +
            " set @select= N'select distinct Sucursal from [Personal].[DTM_Personal_Ausentismos]' " +
            " if ( @Año != '' and @Area != '' and @SubArea != '' and @Mes != '' )   set @filtro = @select+  ' where  Mes = '''+@Mes+''' and Anual = '''+@Año+''' and Area = '''+@Area+''' and SubArea = '''+@SubArea+''' ' " +
            " if ( @Año != '' and @Area != '' and @SubArea  = '' and @Mes != '' ) set @filtro = @select+  ' where  Mes = '''+@Mes+''' and Anual = '''+@Año+''' and Area = '''+@Area+''' ' " +
            " if ( @Año != '' and @Area != '' and @SubArea != '' and @Mes = '')  	set @filtro = @select+  ' where  Anual = '''+@Año+''' and Area = '''+@Area+''' and SubArea = '''+@SubArea+''' ' " +
            " if ( @Año != '' and @Area != '' and @SubArea = '' and @Mes = '')  set @filtro = @select+  ' where  Anual = '''+@Año+''' and Area = '''+@Area+''' ' " +
            " if ( @Año != '' and @Area = '' and @SubArea = '' and @Mes = ''  )  	  set @filtro = @select+  ' where Anual = '''+@Año+'''   ' " +
            " if ( @Año != '' and @Area = '' and @SubArea  != '' and @Mes != '' )  	set @filtro = @select+  ' where  Mes = '''+@Mes+''' and Anual = '''+@Año+'''  and SubArea = '''+@SubArea+''' ' " +
            " if ( @Año != '' and @Area = '' and @SubArea = '' and @Mes != '')  	set @filtro = @select+  ' where  Mes = '''+@Mes+''' and Anual = '''+@Año+'''   ' " +
            " if ( @Año != '' and @Area = '' and @SubArea  != '' and @Mes = '' )  	set @filtro = @select+  ' where  Anual = '''+@Año+''' and SubArea = '''+@SubArea+'''  '  " +
            " if ( @Año = '' and @Area != '' and @SubArea != '' and @Mes != '' )   set @filtro = @select+  ' where  Mes = '''+@Mes+''' and  Area = '''+@Area+''' and SubArea = '''+@SubArea+'''  ' " +
            " if ( @Año = '' and @Area != '' and @SubArea  = '' and @Mes != '' ) set @filtro = @select+  ' where  Mes = '''+@Mes+''' and  Area = '''+@Area+''' ' " +
            " if ( @Año = '' and @Area != '' and @SubArea != '' and @Mes = '')  	set @filtro = @select+  ' where   Area = '''+@Area+''' and SubArea = '''+@SubArea+''' ' " +
            " if ( @Año = '' and @Area != '' and @SubArea = '' and @Mes = '')  set @filtro = @select+  ' where   Area = '''+@Area+''' ' " +
            " if ( @Año = '' and @Area = '' and @SubArea = '' and @Mes = ''  )  	  set @filtro = @select+  '' " +
            " if ( @Año = '' and @Area = '' and @SubArea  != '' and @Mes != '' )  	set @filtro = @select+  ' where  Mes = '''+@Mes+''' and SubArea = '''+@SubArea+''' ' " +
            " if ( @Año = '' and @Area = '' and @SubArea = '' and @Mes != '')  	set @filtro = @select+  ' where  Mes = '''+@Mes+'''    ' " +
            " if ( @Año = '' and @Area = '' and @SubArea  != '' and @Mes = '' )  	set @filtro = @select+  ' where   SubArea = '''+@SubArea+''' ' " +
            " set @Consulta= @filtro+ 'order by Sucursal'; " +
            " execute(@Consulta) ";
            return consulta;
        }
        public String Personal_AusentismoYDescanso_EvolucionAreaMensual_ddlArea_SQL(String anio, String mes, String subArea, String sucursal)
        {        //GIAMPIERE 18/ 09 / 2019
            consulta =
                 " declare @Año varchar(5), @SubArea nvarchar(max),@Sucursal nvarchar(max),@Mes nvarchar(max),@Consulta nvarchar(max),@filtro nvarchar(max),@select nvarchar(max); " +
            " set @Año='" + anio + "'; set @SubArea='" + subArea + "'; set @Sucursal= '" + sucursal + "'; set @Mes='" + mes + "';set @Consulta= ''; set @filtro= ''; set @select= ''; " +
            " set @select= N'select distinct Area from [Personal].[DTM_Personal_Ausentismos]' " +
            " if ( @Año != '' and @SubArea != '' and @Sucursal != '' and @Mes != '' )   set @filtro = @select+  ' where  Mes = '''+@Mes+''' and Anual = '''+@Año+''' and SubArea = '''+@SubArea+''' and Sucursal = '''+@Sucursal+''' ' " +
            " if ( @Año != '' and @SubArea != '' and @Sucursal  = '' and @Mes != '' ) set @filtro = @select+  ' where  Mes = '''+@Mes+''' and Anual = '''+@Año+''' and SubArea = '''+@SubArea+''' ' " +
            " if ( @Año != '' and @SubArea != '' and @Sucursal != '' and @Mes = '')  	set @filtro = @select+  ' where  Anual = '''+@Año+''' and SubArea = '''+@SubArea+''' and Sucursal = '''+@Sucursal+''' ' " +
            " if ( @Año != '' and @SubArea != '' and @Sucursal = '' and @Mes = '')  set @filtro = @select+  ' where  Anual = '''+@Año+''' and SubArea = '''+@SubArea+''' ' " +
            " if ( @Año != '' and @SubArea = '' and @Sucursal = '' and @Mes = ''  )  	  set @filtro = @select+  ' where Anual = '''+@Año+'''   ' " +
            " if ( @Año != '' and @SubArea = '' and @Sucursal  != '' and @Mes != '' )  	set @filtro = @select+  ' where  Mes = '''+@Mes+''' and Anual = '''+@Año+'''  and Sucursal = '''+@Sucursal+''' ' " +
            " if ( @Año != '' and @SubArea = '' and @Sucursal = '' and @Mes != '')  	set @filtro = @select+  ' where  Mes = '''+@Mes+''' and Anual = '''+@Año+'''   ' " +
            " if ( @Año != '' and @SubArea = '' and @Sucursal  != '' and @Mes = '' )  	set @filtro = @select+  ' where  Anual = '''+@Año+''' and Sucursal = '''+@Sucursal+'''  '  " +
            " if ( @Año = '' and @SubArea != '' and @Sucursal != '' and @Mes != '' )   set @filtro = @select+  ' where  Mes = '''+@Mes+''' and  SubArea = '''+@SubArea+''' and Sucursal = '''+@Sucursal+'''  ' " +
            " if ( @Año = '' and @SubArea != '' and @Sucursal  = '' and @Mes != '' ) set @filtro = @select+  ' where  Mes = '''+@Mes+''' and  SubArea = '''+@SubArea+''' ' " +
            " if ( @Año = '' and @SubArea != '' and @Sucursal != '' and @Mes = '')  	set @filtro = @select+  ' where   SubArea = '''+@SubArea+''' and Sucursal = '''+@Sucursal+''' ' " +
            " if ( @Año = '' and @SubArea != '' and @Sucursal = '' and @Mes = '')  set @filtro = @select+  ' where   SubArea = '''+@SubArea+''' ' " +
            " if ( @Año = '' and @SubArea = '' and @Sucursal = '' and @Mes = ''  )  	  set @filtro = @select+  '' " +
            " if ( @Año = '' and @SubArea = '' and @Sucursal  != '' and @Mes != '' )  	set @filtro = @select+  ' where  Mes = '''+@Mes+''' and Sucursal = '''+@Sucursal+''' ' " +
            " if ( @Año = '' and @SubArea = '' and @Sucursal = '' and @Mes != '')  	set @filtro = @select+  ' where  Mes = '''+@Mes+'''    ' " +
            " if ( @Año = '' and @SubArea = '' and @Sucursal  != '' and @Mes = '' )  	set @filtro = @select+  ' where   Sucursal = '''+@Sucursal+''' ' " +
            " set @Consulta= @filtro+ 'order by Area'; " +
            " execute(@Consulta) ";
            return consulta;
        }
        public String Personal_AusentismoYDescanso_EvolucionAreaMensual_ddlSubArea_SQL(String anio, String mes, String area, String sucursal)
        {        //GIAMPIERE 18/ 09 / 2019
            consulta =
                  " declare @Año varchar(20), @Area nvarchar(max),@Sucursal nvarchar(max),@Mes nvarchar(max),@Consulta nvarchar(max),@filtro nvarchar(max),@select nvarchar(max); " +
            " set @Año='" + anio + "'; set @Area='" + area + "';set @Sucursal= '" + sucursal + "'; set @Mes='" + mes + "';set @Consulta= ''; set @filtro= ''; set @select= ''; " +
            " set @select= N'select distinct SubArea from [Personal].[DTM_Personal_Ausentismos]' " +
            " if ( @Año != '' and @Area != '' and @Sucursal != '' and @Mes != '' )   set @filtro = @select+  ' where  Mes = '''+@Mes+''' and Anual = '''+@Año+''' and Area = '''+@Area+''' and Sucursal = '''+@Sucursal+''' ' " +
            " if ( @Año != '' and @Area != '' and @Sucursal  = '' and @Mes != '' ) set @filtro = @select+  ' where  Mes = '''+@Mes+''' and Anual = '''+@Año+''' and Area = '''+@Area+''' ' " +
            " if ( @Año != '' and @Area != '' and @Sucursal != '' and @Mes = '')  	set @filtro = @select+  ' where  Anual = '''+@Año+''' and Area = '''+@Area+''' and Sucursal = '''+@Sucursal+''' ' " +
            " if ( @Año != '' and @Area != '' and @Sucursal = '' and @Mes = '')  set @filtro = @select+  ' where  Anual = '''+@Año+''' and Area = '''+@Area+''' ' " +
            " if ( @Año != '' and @Area = '' and @Sucursal = '' and @Mes = ''  )  	  set @filtro = @select+  ' where Anual = '''+@Año+'''   ' " +
            " if ( @Año != '' and @Area = '' and @Sucursal  != '' and @Mes != '' )  	set @filtro = @select+  ' where  Mes = '''+@Mes+''' and Anual = '''+@Año+'''  and Sucursal = '''+@Sucursal+''' ' " +
            " if ( @Año != '' and @Area = '' and @Sucursal = '' and @Mes != '')  	set @filtro = @select+  ' where  Mes = '''+@Mes+''' and Anual = '''+@Año+'''   ' " +
            " if ( @Año != '' and @Area = '' and @Sucursal  != '' and @Mes = '' )  	set @filtro = @select+  ' where  Anual = '''+@Año+''' and Sucursal = '''+@Sucursal+'''  '  " +
            " if ( @Año = '' and @Area != '' and @Sucursal != '' and @Mes != '' )   set @filtro = @select+  ' where  Mes = '''+@Mes+''' and  Area = '''+@Area+''' and Sucursal = '''+@Sucursal+'''  ' " +
            " if ( @Año = '' and @Area != '' and @Sucursal  = '' and @Mes != '' ) set @filtro = @select+  ' where  Mes = '''+@Mes+''' and  Area = '''+@Area+''' ' " +
            " if ( @Año = '' and @Area != '' and @Sucursal != '' and @Mes = '')  	set @filtro = @select+  ' where   Area = '''+@Area+''' and Sucursal = '''+@Sucursal+''' ' " +
            " if ( @Año = '' and @Area != '' and @Sucursal = '' and @Mes = '')  set @filtro = @select+  ' where   Area = '''+@Area+''' ' " +
            " if ( @Año = '' and @Area = '' and @Sucursal = '' and @Mes = ''  )  	  set @filtro = @select+  '' " +
            " if ( @Año = '' and @Area = '' and @Sucursal  != '' and @Mes != '' )  	set @filtro = @select+  ' where  Mes = '''+@Mes+''' and Sucursal = '''+@Sucursal+''' ' " +
            " if ( @Año = '' and @Area = '' and @Sucursal = '' and @Mes != '')  	set @filtro = @select+  ' where  Mes = '''+@Mes+'''    ' " +
            " if ( @Año = '' and @Area = '' and @Sucursal  != '' and @Mes = '' )  	set @filtro = @select+  ' where   Sucursal = '''+@Sucursal+''' ' " +
            " set @Consulta= @filtro+ 'order by SubArea'; " +
            " execute(@Consulta) ";

            return consulta;
        }
        public String Personal_AusentismoYDescanso_EvolucionAreaMensual_ddlAnio_SQL(String mes, String area, String subArea, String sucursal)
        {//GIAMPIERE 18/ 09 / 2019
            consulta =
            " declare @Mes varchar(5), @SubArea nvarchar(max),@Sucursal nvarchar(max),@Area nvarchar(max),@Consulta nvarchar(max),@filtro nvarchar(max),@select nvarchar(max); " +
            " set @Area='" + area + "';set @SubArea='" + subArea + "'; set @Sucursal= '" + sucursal + "'; set @Mes='" + mes + "';set @Consulta= ''; set @filtro= ''; set @select= ''; " +
            " set @select= N'select distinct Anual as Anio from [Personal].[DTM_Personal_Ausentismos]' " +
            " if ( @Mes != '' and @SubArea != '' and @Sucursal != '' and @Area != '' )   set @filtro = @select+  ' where  Area = '''+@Area+''' and Mes = '''+@Mes+''' and SubArea = '''+@SubArea+''' and Sucursal = '''+@Sucursal+''' ' " +
            " if ( @Mes != '' and @SubArea != '' and @Sucursal  = '' and @Area != '' ) set @filtro = @select+  ' where  Area = '''+@Area+''' and Mes = '''+@Mes+''' and SubArea = '''+@SubArea+''' ' " +
            " if ( @Mes != '' and @SubArea != '' and @Sucursal != '' and @Area = '')  	set @filtro = @select+  ' where  Mes = '''+@Mes+''' and SubArea = '''+@SubArea+''' and Sucursal = '''+@Sucursal+''' ' " +
            " if ( @Mes != '' and @SubArea != '' and @Sucursal = '' and @Area = '')  set @filtro = @select+  ' where  Mes = '''+@Mes+''' and SubArea = '''+@SubArea+''' ' " +
            " if ( @Mes != '' and @SubArea = '' and @Sucursal = '' and @Area = ''  )  	  set @filtro = @select+  ' where Mes = '''+@Mes+'''   ' " +
            " if ( @Mes != '' and @SubArea = '' and @Sucursal  != '' and @Area != '' )  	set @filtro = @select+  ' where  Area = '''+@Area+''' and Mes = '''+@Mes+'''  and Sucursal = '''+@Sucursal+''' ' " +
            " if ( @Mes != '' and @SubArea = '' and @Sucursal = '' and @Area != '')  	set @filtro = @select+  ' where  Area = '''+@Area+''' and Mes = '''+@Mes+'''   ' " +
            " if ( @Mes != '' and @SubArea = '' and @Sucursal  != '' and @Area = '' )  	set @filtro = @select+  ' where  Mes = '''+@Mes+''' and Sucursal = '''+@Sucursal+'''  '  " +
            " if ( @Mes = '' and @SubArea != '' and @Sucursal != '' and @Area != '' )   set @filtro = @select+  ' where  Area = '''+@Area+''' and  SubArea = '''+@SubArea+''' and Sucursal = '''+@Sucursal+'''  ' " +
            " if ( @Mes = '' and @SubArea != '' and @Sucursal  = '' and @Area != '' ) set @filtro = @select+  ' where  Area = '''+@Area+''' and  SubArea = '''+@SubArea+''' ' " +
            " if ( @Mes = '' and @SubArea != '' and @Sucursal != '' and @Area = '')  	set @filtro = @select+  ' where   SubArea = '''+@SubArea+''' and Sucursal = '''+@Sucursal+''' ' " +
            " if ( @Mes = '' and @SubArea != '' and @Sucursal = '' and @Area = '')  set @filtro = @select+  ' where   SubArea = '''+@SubArea+''' ' " +
            " if ( @Mes = '' and @SubArea = '' and @Sucursal = '' and @Area = ''  )  	  set @filtro = @select+  '' " +
            " if ( @Mes = '' and @SubArea = '' and @Sucursal  != '' and @Area != '' )  	set @filtro = @select+  ' where  Area = '''+@Area+''' and Sucursal = '''+@Sucursal+''' ' " +
            " if ( @Mes = '' and @SubArea = '' and @Sucursal = '' and @Area != '')  	set @filtro = @select+  ' where  Area = '''+@Area+'''    ' " +
            " if ( @Mes = '' and @SubArea = '' and @Sucursal  != '' and @Area = '' )  	set @filtro = @select+  ' where   Sucursal = '''+@Sucursal+''' ' " +
            " set @Consulta= @filtro+ 'order by Anual'; " +
            " execute(@Consulta) ";
            return consulta;
        }
        public String Personal_AusentismoYDescanso_EvolucionAreaMensual_ddlMes_SQL(String anio, String area, String subArea, String sucursal)
        {//GIAMPIERE 18/ 09 / 2019
            consulta =
                 " declare @Año varchar(5), @SubArea nvarchar(max),@Sucursal nvarchar(max),@Area nvarchar(max),@Consulta nvarchar(max),@filtro nvarchar(max),@select nvarchar(max); " +
            " set @Año='" + anio + "'; set @Area='" + area + "';set @SubArea='" + subArea + "'; set @Sucursal= '" + sucursal + "';set @Consulta= ''; set @filtro= ''; set @select= ''; " +
            " set @select= N'select distinct DATENAME(MONTH,Fecha) as Meses ,Mes from [Personal].[DTM_Personal_Ausentismos]' " +
            " if ( @Año != '' and @SubArea != '' and @Sucursal != '' and @Area != '' )   set @filtro = @select+  ' where  Area = '''+@Area+''' and Anual = '''+@Año+''' and SubArea = '''+@SubArea+''' and Sucursal = '''+@Sucursal+''' ' " +
            " if ( @Año != '' and @SubArea != '' and @Sucursal  = '' and @Area != '' ) set @filtro = @select+  ' where  Area = '''+@Area+''' and Anual = '''+@Año+''' and SubArea = '''+@SubArea+''' ' " +
            " if ( @Año != '' and @SubArea != '' and @Sucursal != '' and @Area = '')  	set @filtro = @select+  ' where  Anual = '''+@Año+''' and SubArea = '''+@SubArea+''' and Sucursal = '''+@Sucursal+''' ' " +
            " if ( @Año != '' and @SubArea != '' and @Sucursal = '' and @Area = '')  set @filtro = @select+  ' where  Anual = '''+@Año+''' and SubArea = '''+@SubArea+''' ' " +
            " if ( @Año != '' and @SubArea = '' and @Sucursal = '' and @Area = ''  )  	  set @filtro = @select+  ' where Anual = '''+@Año+'''   ' " +
            " if ( @Año != '' and @SubArea = '' and @Sucursal  != '' and @Area != '' )  	set @filtro = @select+  ' where  Area = '''+@Area+''' and Anual = '''+@Año+'''  and Sucursal = '''+@Sucursal+''' ' " +
            " if ( @Año != '' and @SubArea = '' and @Sucursal = '' and @Area != '')  	set @filtro = @select+  ' where  Area = '''+@Area+''' and Anual = '''+@Año+'''   ' " +
            " if ( @Año != '' and @SubArea = '' and @Sucursal  != '' and @Area = '' )  	set @filtro = @select+  ' where  Anual = '''+@Año+''' and Sucursal = '''+@Sucursal+'''  '  " +
            " if ( @Año = '' and @SubArea != '' and @Sucursal != '' and @Area != '' )   set @filtro = @select+  ' where  Area = '''+@Area+''' and  SubArea = '''+@SubArea+''' and Sucursal = '''+@Sucursal+'''  ' " +
            " if ( @Año = '' and @SubArea != '' and @Sucursal  = '' and @Area != '' ) set @filtro = @select+  ' where  Area = '''+@Area+''' and  SubArea = '''+@SubArea+''' ' " +
            " if ( @Año = '' and @SubArea != '' and @Sucursal != '' and @Area = '')  	set @filtro = @select+  ' where   SubArea = '''+@SubArea+''' and Sucursal = '''+@Sucursal+''' ' " +
            " if ( @Año = '' and @SubArea != '' and @Sucursal = '' and @Area = '')  set @filtro = @select+  ' where   SubArea = '''+@SubArea+''' ' " +
            " if ( @Año = '' and @SubArea = '' and @Sucursal = '' and @Area = ''  )  	  set @filtro = @select+  '' " +
            " if ( @Año = '' and @SubArea = '' and @Sucursal  != '' and @Area != '' )  	set @filtro = @select+  ' where  Area = '''+@Area+''' and Sucursal = '''+@Sucursal+''' ' " +
            " if ( @Año = '' and @SubArea = '' and @Sucursal = '' and @Area != '')  	set @filtro = @select+  ' where  Area = '''+@Area+'''    ' " +
            " if ( @Año = '' and @SubArea = '' and @Sucursal  != '' and @Area = '' )  	set @filtro = @select+  ' where   Sucursal = '''+@Sucursal+''' ' " +
            " set @Consulta= @filtro+ 'order by Mes'; " +
            " execute(@Consulta) ";

            return consulta;
        }
        #endregion 
        #region Costos de Planilla

        public string costoPlanilla_cantidadGeneral =

        //KPI: PERSONAL - GENERAL -CANTIDAD DE TRABAJADORES GENERAL
        //ERIKA MALPICA -19 / 07 / 2019
        " declare @Mes date, @primerDiaMes date, @ultimaDiaMes date" +
        " IF(getdate()= (select EOMONTH(getdate()) as UltDiaMesActual))" +
        " begin set @Mes=	(SELECT CONVERT (date, GETDATE()))	 end" +
        " ELSE" +
        " begin" +
        " set @Mes= (select EOMONTH(getdate(),-1) as UltDiaMesAnterior) " +
        " end" +
        " set @primerDiaMes= dateadd(d,1, EOMONTH(@Mes,-1))" +
        " set @ultimaDiaMes = EOMONTH(@Mes)" +
        " select sum(dtm.Total) as Cantidad from Personal.DTM_Personal_Planilla dtm" +
        " where dtm.Fecha between @primerDiaMes and @ultimaDiaMes";

        public String G_Personal_CantidadArea_SQLP(String sucursal, String area, String subarea, String fechaInicio, String fechaFin)
        {
            consulta =
            " declare @sucursal varchar(max),@departamento varchar(max),@area varchar(max),@subarea varchar(max), @fechaInicio varchar(max), @fechaFin varchar(max), @consulta varchar(max), @mes varchar(max)" +
            " set @sucursal = '" + sucursal + "'; set @departamento = ''; set @area = '" + area + "'; set @subarea = '" + subarea + "'; set @fechaInicio = '" + fechaInicio + "'; set @fechaFin = '" + fechaFin + "'" +
            " IF(@fechaInicio = '' and @fechaFin = '') BEGIN" +
            " IF(getdate() = (select EOMONTH(getdate()))) begin set @Mes = (SELECT CONVERT(date, GETDATE())) end" +
            " ELSE begin set @Mes = (select EOMONTH(getdate(), -1) ) end" +
            " set @fechaInicio = dateadd(d, 1, EOMONTH(@Mes, -1))  set @fechaFin = EOMONTH(@Mes)" +
            " END" +
            " set @consulta = ' select dtm.Departamento,(sum(dtm.Total)/1000) as Cantidad from Personal.DTM_Personal_Planilla dtm where '" +
            " if (@sucursal != '' and @area = '' and @subarea = '' and(@fechaInicio != '' and @fechaFin != '')) set @consulta = @consulta + ' dtm.Sucursal=''' + @sucursal + ''' and'" +
            " if (@sucursal = '' and @area != '' and @subarea = '' and(@fechaInicio != '' and @fechaFin != '')) set @consulta = @consulta + ' dtm.Area=''' + @area + ''' and'" +
            " if (@sucursal = '' and @area = '' and @subarea != '' and(@fechaInicio != '' and @fechaFin != '')) set @consulta = @consulta + ' dtm.SubArea=''' + @subarea + ''' and'" +
            " if (@sucursal != '' and @area!= '' and @subarea = '' and(@fechaInicio != '' and @fechaFin != '')) set @consulta = @consulta + ' dtm.Sucursal=''' + @sucursal + ''' and' + ' dtm.Area=''' + @area + ''' and'" +
            " if (@sucursal != '' and @area!= '' and @subarea!= '' and(@fechaInicio != '' and @fechaFin != '')) set @consulta = @consulta + ' dtm.Sucursal=''' + @sucursal + ''' and' + ' dtm.Area=''' + @area + ''' and' + ' dtm.SubArea=''' + @subarea + ''' and'" +
            " if (@sucursal != '' and @area = '' and @subarea!= '' and(@fechaInicio != '' and @fechaFin != '')) set @consulta = @consulta + ' dtm.Sucursal=''' + @sucursal + ''' and' + ' dtm.SubArea=''' + @subarea + ''' and'" +
            " if (@sucursal = '' and @area!= '' and @subarea!= '' and(@fechaInicio != '' and @fechaFin != '')) set @consulta = @consulta + ' dtm.Area=''' + @area + ''' and' + ' dtm.SubArea=''' + @subarea + ''' and'" +
            " set @consulta = @consulta + '  dtm.Fecha between ''' + @fechaInicio + ''' and ''' + @fechaFin + ''' group by dtm.Departamento order by sum(dtm.CantidadTrabajadores)'" +
            " exec(@consulta)";

            return consulta;
        }

        public String G_Personal_CostoPlanilla_CantidadArea_ddlSucursal(String area, String subarea, String fechaInicio, String fechaFin)
        {
            //KPI: PERSONAL - GENERAL -FILTRO SUCURSAL
            //ERIKA MALPICA -26 / 07 / 2019
            consulta =
            " declare @area varchar(max),@subarea varchar(max), @fechaInicio varchar(max), @fechaFin varchar(max), @consulta varchar(max), @mesActual varchar(max),@mes varchar(max),@anio varchar(max),@select varchar(max)" +
            " set @area = '" + area + "'; set @subarea = '" + subarea + "'; set @fechaInicio = '" + fechaInicio + "'; set @fechaFin = '" + fechaFin + "'; set @consulta = ' '" +
            " set @select = N'select distinct dtm.Sucursal as Sucursal from Personal.DTM_Personal_Planilla dtm '" +
            " IF(getdate() = (select EOMONTH(getdate()))) begin set @mesActual = (SELECT CONVERT(date, GETDATE())) end" +
            " ELSe begin set @mesActual = (select EOMONTH(getdate(), -1) ) end" +
            " set @mes = MONTH(@mesActual); set @anio = YEAR(@mesActual);" +
            " if (@area != '' and @subarea = '' and(@fechaInicio = '' and @fechaFin = '')) set @consulta = @consulta + ' and dtm.Area=''' + @area + ''''" +
            " if (@area = '' and @subarea!= '' and(@fechaInicio = '' and @fechaFin = '')) set @consulta = @consulta + ' and dtm.SubArea=''' + @subarea + ''''" +
            " if (@area = '' and @subarea = '' and(@fechaInicio != '' and @fechaFin != '')) set @consulta = @consulta + ' and dtm.Fecha between''' + @fechaInicio + ''' and ''' + @fechaFin + ''''" +
            " if (@area != '' and @subarea!= '' and(@fechaInicio != '' and @fechaFin != '')) set @consulta = @consulta + ' and dtm.Area=''' + @area + '''' + ' and dtm.SubArea=''' + @subarea + '''' + ' and dtm.Fecha between''' + @fechaInicio + ''' and ''' + @fechaFin + ''''" +
            " if (@area = '' and @subarea = '' and(@fechaInicio != '' and @fechaFin != '')) set @consulta = @consulta + ' and  dtm.Fecha between''' + @fechaInicio + ''' and ''' + @fechaFin + ''''" +
            " if (@area != '' and @subarea!= '' and(@fechaInicio = '' and @fechaFin = '')) set @consulta = @consulta + ' and dtm.Area=''' + @area + '''' + ' and dtm.SubArea=''' + @subarea + ''''" +
            " if (@area != '' and @subarea!= '' and(@fechaInicio != '' and @fechaFin != '')) set @consulta = @consulta + ' and dtm.Area=''' + @area + '''' + ' and dtm.SubArea=''' + @subarea + '''' + ' and dtm.Fecha between''' + @fechaInicio + ''' and ''' + @fechaFin + ''''" +
            " if (@area = '' and @subarea!= '' and(@fechaInicio != '' and @fechaFin != '')) set @consulta = @consulta + ' and dtm.SubArea=''' + @subarea + '''' + ' and dtm.Fecha between''' + @fechaInicio + ''' and ''' + @fechaFin + ''''" +
            " set @consulta = @select + 'where dtm.Mes=''' + @mes + ''' and dtm.Anual=''' + @anio + ''' ' + @consulta + ' order by dtm.Sucursal asc';" +
            " exec(@consulta)";
            
            return consulta;
        }

        public String G_Personal_CostoPlanilla_CantidadArea_ddlArea(String sucursal, String subarea, String fechaInicio, String fechaFin)
        {
            //KPI: PERSONAL - GENERAL -FILTRO AREA
            //ERIKA MALPICA -25 / 07 / 2019
            consulta =
            " declare @sucursal varchar(max),@subarea varchar(max), @fechaInicio varchar(max), @fechaFin varchar(max), @consulta varchar(max), @select varchar(max), @mesActual varchar(max),@mes varchar(max),@anio varchar(max)" +
            " set @sucursal = '" + sucursal + "'; set @subarea = '" + subarea + "'; set @fechaInicio = '" + fechaInicio + "'; set @fechaFin = '" + fechaFin + "';" +
            " set @select = N'select distinct dtm.Area as Area from Personal.DTM_Personal_Planilla dtm ' set @consulta = ' '" +
            " IF(getdate() = (select EOMONTH(getdate()))) begin set @mesActual = (SELECT CONVERT(date, GETDATE())) end" +
            " ELSe begin set @mesActual = (select EOMONTH(getdate(), -1) ) end" +
            " set @mes = MONTH(@mesActual); set @anio = YEAR(@mesActual);" +
            " if (@sucursal = '' and @subarea = '' and(@fechaInicio = '' and @fechaFin = '')) set @consulta = ' '" +
            " if (@sucursal != '' and @subarea = '' and(@fechaInicio = '' and @fechaFin = '')) set @consulta = @consulta + ' and dtm.Sucursal=''' + @sucursal + ''''" +
            " if (@sucursal = '' and @subarea!= '' and(@fechaInicio = '' and @fechaFin = '')) set @consulta = @consulta + ' and dtm.SubArea=''' + @subarea + ''''" +
            " if (@sucursal = '' and @subarea = '' and(@fechaInicio != '' and @fechaFin != '')) set @consulta = @consulta + ' and dtm.Fecha between''' + @fechaInicio + ''' and ''' + @fechaFin + ''''" +
            " if (@sucursal != '' and @subarea!= '' and(@fechaInicio = '' and @fechaFin = '')) set @consulta = @consulta + ' and dtm.Sucursal=''' + @sucursal + '''' + ' and dtm.SubArea=''' + @subarea + ''''" +
            " if (@sucursal != '' and @subarea!= '' and(@fechaInicio != '' and @fechaFin != '')) set @consulta = @consulta + ' and dtm.Sucursal=''' + @sucursal + '''' + ' and dtm.SubArea=''' + @subarea + '''' + ' and dtm.Fecha between''' + @fechaInicio + ''' and ''' + @fechaFin + ''''" +
            " if (@sucursal = '' and @subarea = '' and(@fechaInicio != '' and @fechaFin != '')) set @consulta = @consulta + ' and  dtm.Fecha between''' + @fechaInicio + ''' and ''' + @fechaFin + ''''" +
            " if (@sucursal != '' and @subarea!= '' and(@fechaInicio = '' and @fechaFin = '')) set @consulta = @consulta + ' and dtm.Sucursal=''' + @sucursal + '''' + 'and dtm.SubArea=''' + @subarea + ''''" +
            " if (@sucursal != '' and @subarea!= '' and(@fechaInicio != '' and @fechaFin != '')) set @consulta = @consulta + ' and dtm.Sucursal=''' + @sucursal + '''' + 'and dtm.SubArea=''' + @subarea + '''' + ' and dtm.Fecha between''' + @fechaInicio + ''' and ''' + @fechaFin + ''''" +
            " if (@sucursal = '' and @subarea!= '' and(@fechaInicio != '' and @fechaFin != '')) set @consulta = @consulta + ' and dtm.SubArea=''' + @subarea + '''' + ' and dtm.Fecha between''' + @fechaInicio + ''' and ''' + @fechaFin + ''''" +
            " set @consulta = @select + 'where dtm.Mes=' + @mes + 'and dtm.Anual=''' + @anio + ''' ' + @consulta + ' order by dtm.Area asc';" +
            " exec(@consulta)";
            return consulta;
        }

        public String G_Personal_CostoPlanilla_CantidadArea_ddlSubArea(String sucursal, String area, String fechaInicio, String fechaFin)
        {
            //KPI: PERSONAL - GENERAL -FILTRO AREA
            //ERIKA MALPICA -26 / 07 / 2019
            consulta =
            " declare @area varchar(max),@sucursal varchar(max), @fechaInicio varchar(max), @fechaFin varchar(max), @consulta varchar(max), @mesActual varchar(max),@mes varchar(max),@anio varchar(max),@select varchar(max)" +
            " set @area = '" + area + "'; set @sucursal = '" + sucursal + "'; set @fechaInicio = '" + fechaInicio + "'; set @fechaFin = '" + fechaFin + "'; set @consulta = ' '" +
            " set @select = N'select distinct dtm.SubArea as SubArea from Personal.DTM_Personal_Planilla dtm '" +
            " IF(getdate() = (select EOMONTH(getdate()))) begin set @mesActual = (SELECT CONVERT(date, GETDATE())) end" +
            " ELSe begin set @mesActual = (select EOMONTH(getdate(), -1) ) end" +
            " set @mes = MONTH(@mesActual); set @anio = YEAR(@mesActual);" +
            " if (@area != '' and @sucursal = '' and(@fechaInicio = '' and @fechaFin = '')) set @consulta = @consulta + ' and dtm.Area=''' + @area + ''''" +
            " if (@area = '' and @sucursal!= '' and(@fechaInicio = '' and @fechaFin = '')) set @consulta = @consulta + ' and dtm.Sucursal=''' + @sucursal + ''''" +
            " if (@area = '' and @sucursal = '' and(@fechaInicio != '' and @fechaFin != '')) set @consulta = @consulta + ' and dtm.Fecha between''' + @fechaInicio + ''' and ''' + @fechaFin + ''''" +
            " if (@area != '' and @sucursal!= '' and(@fechaInicio != '' and @fechaFin != '')) set @consulta = @consulta + ' and dtm.Area=''' + @area + '''' + ' and dtm.Sucursal=''' + @sucursal + '''' + ' and dtm.Fecha between''' + @fechaInicio + ''' and ''' + @fechaFin + ''''" +
            " if (@area = '' and @sucursal = '' and(@fechaInicio != '' and @fechaFin != '')) set @consulta = @consulta + ' and  dtm.Fecha between''' + @fechaInicio + ''' and ''' + @fechaFin + ''''" +
            " if (@area != '' and @sucursal!= '' and(@fechaInicio = '' and @fechaFin = '')) set @consulta = @consulta + ' and dtm.Area=''' + @area + '''' + ' and dtm.Sucursal=''' + @sucursal + ''''" +
            " if (@area != '' and @sucursal!= '' and(@fechaInicio != '' and @fechaFin != '')) set @consulta = @consulta + ' and dtm.Area=''' + @area + '''' + ' and dtm.Sucursal=''' + @sucursal + '''' + ' and dtm.Fecha between''' + @fechaInicio + ''' and ''' + @fechaFin + ''''" +
            " if (@area = '' and @sucursal!= '' and(@fechaInicio != '' and @fechaFin != '')) set @consulta = @consulta + ' and dtm.Sucursal=''' + @sucursal + '''' + ' and dtm.Fecha between''' + @fechaInicio + ''' and ''' + @fechaFin + ''''" +
            " set @consulta = @select + 'where dtm.Mes=''' + @mes + ''' and dtm.Anual=''' + @anio + ''' ' + @consulta + ' order by dtm.SubArea asc';" +
            " exec(@consulta)";
            return consulta;

        }

        //EVOLUCIÓN GENERAL MENSUAL
        //GIAMPIERE 09/ 09 / 2019
        public String G_Personal_CostoPlanilla_EvolucionGeneralMensual_SQL(String mes, String departamento, String area, String subArea, String sucursal)
        {        //GIAMPIERE 09/ 09 / 2019
            consulta =
                " declare @Departamento varchar(20), @Area nvarchar(max), @Mes nvarchar(3),@SubArea nvarchar(max),@Sucursal nvarchar(max), @filtro  NVARCHAR(MAX), @cols NVARCHAR(MAX), @null NVARCHAR(MAX), @consulta NVARCHAR(MAX); " +
          " set @Departamento= '" + departamento + "'; set @Area='" + area + "';set @SubArea='" + subArea + "'; set @Sucursal= '" + sucursal + "'; set @Mes='" + mes + "'; set @filtro =''; set @consulta =''; " +
          " select @cols = STUFF((SELECT Distinct ',' + QUOTENAME(Anual)  from Personal.DTM_Personal_Planilla group by ',' + QUOTENAME(Anual)Order by ',' + QUOTENAME(Anual) FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)')  ,1,1,'') " +
           " select @null = STUFF((SELECT Distinct ', isnull(' + QUOTENAME(Anual)+',0) as'  + QUOTENAME(Anual) from Personal.DTM_Personal_Planilla  group by ', isnull(' + QUOTENAME(Anual)+',0) as'  + QUOTENAME(Anual) Order by ', isnull(' + QUOTENAME(Anual)+',0) as'  + QUOTENAME(Anual)   FOR XML PATH(''), TYPE  ).value('.', 'NVARCHAR(MAX)')   ,1,1,'') " +
           " if  (@Mes = '' and @Departamento = ''  and @Area = '' and @SubArea = '' and @Sucursal = ''  )  	  set @consulta= 'SELECT Meses,' + @null + '  from  ( select Mes, DATENAME(MONTH,Fecha) as Meses,Anual,Total from Personal.DTM_Personal_Planilla where  KeyPersonalPlanilla in(select max(KeyPersonalPlanilla) from Personal.DTM_Personal_Planilla  group by  Mes ,Anual,Sucursal ,Departamento,Area,SubArea)   ) x  pivot ( sum(Total) for Anual in (' + @cols + ') ) p  order by Mes' " +
           " else  " +
           " begin " +
           " if  (@Mes != '' and @Departamento != '' and @Area != '' and @SubArea != '' and @Sucursal != '' )  	 set @filtro = ' Sucursal = '''+@Sucursal+''' and Departamento = '''+@Departamento+''' and Area = '''+@Area+''' and SubArea = '''+@SubArea+'''  and Mes = '''+@Mes+'''  ' " +
           " if  (@Mes != '' and @Departamento != ''  and @Area = '' and @SubArea = '' and @Sucursal = ''  )  	 set @filtro = ' Departamento = '''+@Departamento+'''  and Mes = '''+@Mes+''' ' " +
           " if  (@Mes != '' and @Departamento != ''  and @Area != '' and @SubArea  = '' and @Sucursal != '' )   set @filtro = ' Sucursal = '''+@Sucursal+''' and Departamento = '''+@Departamento+''' and Area='''+@Area+'''  and Mes = '''+@Mes+''' ' " +
           " if  (@Mes != '' and @Departamento != '' and @Area != '' and @SubArea != '' and @Sucursal = '')  	 set @filtro = ' Departamento = '''+@Departamento+''' and Area = '''+@Area+''' and SubArea = '''+@SubArea+'''  and Mes = '''+@Mes+''' ' " +
           " if  (@Mes != '' and @Departamento != ''  and @Area = '' and @SubArea  != '' and @Sucursal != '' )   set @filtro = ' Sucursal = '''+@Sucursal+''' and Departamento = '''+@Departamento+''' and SubArea = '''+@SubArea+'''  and Mes = '''+@Mes+''' ' " +
           " if  (@Mes != '' and @Departamento != '' and @Area = '' and @SubArea = '' and @Sucursal != '')  	 set @filtro = ' Sucursal = '''+@Sucursal+''' and Departamento = '''+@Departamento+''' and Mes = '''+@Mes+'''   ' " +
           " if  (@Mes != '' and @Departamento != ''  and @Area = '' and @SubArea  != '' and @Sucursal = '' )  	  set @filtro = ' Departamento = '''+@Departamento+''' and SubArea = '''+@SubArea+'''  and Mes = '''+@Mes+'''  ' " +
           " if  (@Mes != '' and @Departamento != '' and @Area != '' and @SubArea = '' and @Sucursal = '')  	 set @filtro = '  Departamento = '''+@Departamento+''' and Area = '''+@Area+'''  and Mes = '''+@Mes+''' ' " +
           " if  (@Mes != '' and @Departamento = '' and @Area != '' and @SubArea != '' and @Sucursal != '' )  	 set @filtro = '  Sucursal = '''+@Sucursal+'''  and Area = '''+@Area+''' and SubArea = '''+@SubArea+'''  and Mes = '''+@Mes+''' ' " +
           " if  (@Mes != '' and @Departamento = ''  and @Area = '' and @SubArea = '' and @Sucursal = ''  )  	  set @filtro = ' Mes = '''+@Mes+''' ' " +
           " if  (@Mes != '' and @Departamento = ''  and @Area != '' and @SubArea  = '' and @Sucursal != '' )  	 set @filtro = ' Sucursal = '''+@Sucursal+''' and Area='''+@Area+''' and Mes = '''+@Mes+'''  ' " +
           " if  (@Mes != '' and @Departamento = '' and @Area != '' and @SubArea != '' and @Sucursal = '')  	 set @filtro = ' Area = '''+@Area+''' and SubArea = '''+@SubArea+'''  and Mes = '''+@Mes+'''   ' " +
           " if  (@Mes != '' and @Departamento = ''  and @Area = '' and @SubArea  != '' and @Sucursal != '' )  	  set @filtro = '  Sucursal = '''+@Sucursal+'''  and SubArea = '''+@SubArea+'''  and Mes = '''+@Mes+''' ' " +
           " if  (@Mes != '' and @Departamento = '' and @Area = '' and @SubArea = '' and @Sucursal != '')  	 set @filtro = ' Sucursal = '''+@Sucursal+'''  and Mes = '''+@Mes+'''  ' " +
           " if  (@Mes != '' and @Departamento = ''  and @Area = '' and @SubArea  != '' and @Sucursal = '' )  	  set @filtro = '  SubArea = '''+@SubArea+'''  and Mes = '''+@Mes+''' '  " +
           " if  (@Mes != '' and @Departamento = '' and @Area != '' and @SubArea = '' and @Sucursal = '')  	 set @filtro = ' Area = '''+@Area+'''  and Mes = '''+@Mes+''' ' " +
           " if  (@Mes = '' and @Departamento != '' and @Area != '' and @SubArea != '' and @Sucursal != '' )  	 set @filtro = '  Sucursal = '''+@Sucursal+''' and Departamento = '''+@Departamento+''' and Area = '''+@Area+''' and SubArea = '''+@SubArea+''' ' " +
           " if  (@Mes = '' and @Departamento != ''  and @Area = '' and @SubArea = '' and @Sucursal = ''  )  	  set @filtro = '  Departamento = '''+@Departamento+'''  ' " +
           " if  (@Mes = '' and @Departamento != ''  and @Area != '' and @SubArea  = '' and @Sucursal != '' )  	 set @filtro = '  Sucursal = '''+@Sucursal+''' and Departamento = '''+@Departamento+''' and Area='''+@Area+'''   ' " +
           " if  (@Mes = '' and @Departamento != '' and @Area != '' and @SubArea != '' and @Sucursal = '')  	 set @filtro = '  Departamento = '''+@Departamento+''' and Area = '''+@Area+''' and SubArea = '''+@SubArea+'''   '  " +
           " if  (@Mes = '' and @Departamento != ''  and @Area = '' and @SubArea  != '' and @Sucursal != '' )  	  set @filtro = '  Sucursal = '''+@Sucursal+''' and Departamento = '''+@Departamento+''' and SubArea = '''+@SubArea+'''  ' " +
           " if  (@Mes = '' and @Departamento != '' and @Area = '' and @SubArea = '' and @Sucursal != '')  	 set @filtro = ' Sucursal = '''+@Sucursal+''' and Departamento = '''+@Departamento+''''  " +
           " if  (@Mes = '' and @Departamento != ''  and @Area = '' and @SubArea  != '' and @Sucursal = '' )  	  set @filtro = '  Departamento = '''+@Departamento+''' and SubArea = '''+@SubArea+'''' " +
           " if  (@Mes = '' and @Departamento != '' and @Area != '' and @SubArea = '' and @Sucursal = '')  	 set @filtro = '  Departamento = '''+@Departamento+''' and Area = '''+@Area+''' ' " +
           " if  (@Mes = '' and @Departamento = '' and @Area != '' and @SubArea != '' and @Sucursal != '' )  	 set @filtro = '  Sucursal = '''+@Sucursal+'''  and Area = '''+@Area+''' and SubArea = '''+@SubArea+'''  ' " +
           " if  (@Mes = '' and @Departamento = ''  and @Area != '' and @SubArea  = '' and @Sucursal != '' )  	 set @filtro = '  Sucursal = '''+@Sucursal+''' and Area='''+@Area+'''  ' " +
           " if  (@Mes = '' and @Departamento = '' and @Area != '' and @SubArea != '' and @Sucursal = '')  	 set @filtro = '  Area = '''+@Area+''' and SubArea = '''+@SubArea+''' '  " +
           " if  (@Mes = '' and @Departamento = ''  and @Area = '' and @SubArea  != '' and @Sucursal != '' )  	  set @filtro = '  Sucursal = '''+@Sucursal+'''  and SubArea = '''+@SubArea+'''  ' " +
           " if  (@Mes = '' and @Departamento = '' and @Area = '' and @SubArea = '' and @Sucursal != '')  	 set @filtro = ' Sucursal = '''+@Sucursal+''' '" +
           " if  (@Mes = '' and @Departamento = ''  and @Area = '' and @SubArea  != '' and @Sucursal = '' )  	  set @filtro = '  SubArea = '''+@SubArea+''' ' " +
           " if  (@Mes = '' and @Departamento = '' and @Area != '' and @SubArea = '' and @Sucursal = '')  	 set @filtro = ' Area = '''+@Area+'''  ' " +
           " set @consulta= 'SELECT Meses,' + @null + '  from  ( select Mes, DATENAME(MONTH,Fecha) as Meses,Anual,Total from Personal.DTM_Personal_Planilla where '+@filtro+' and KeyPersonalPlanilla in(select max(KeyPersonalPlanilla) from Personal.DTM_Personal_Planilla where '+@filtro+'  group by  Mes ,Anual,Sucursal ,Departamento,Area,SubArea)   ) x  pivot ( sum(Total) for Anual in (' + @cols + ') ) p  order by Mes' " +
           " end " +
           " execute(@consulta) ";

            return consulta;
        }
        public String Personal_CostoPlanilla_EvolucionGeneralMensual_ddlSucursal_SQL(String mes, String departamento, String area, String subArea)
        {        //GIAMPIERE 09/ 09 / 2019
            consulta =
                " declare @Area nvarchar(max),@SubArea nvarchar(max),@Departamento nvarchar(max),@Mes nvarchar(max),@Consulta nvarchar(max),@Filtro nvarchar(max),@select nvarchar(max);" +
          " set @Departamento= '" + departamento + "'; set @Area='" + area + "';set @SubArea='" + subArea + "'; set @Mes='" + mes + "';set @Consulta= ''; set @Filtro= ''; set @select= ''; " +
          " set @Consulta= N'select distinct Sucursal from Personal.DTM_Personal_Planilla' " +
          " if (@Mes = ''  and @Area = '' and @SubArea = '' and @Departamento = ''  )  set @Filtro=  @Consulta " +
          " if (@Mes != ''  and @Area != '' and @SubArea != '' and @Departamento != '' )   set @Filtro =  @Consulta+ ' where Departamento = '''+@Departamento+''' and  Area = '''+@Area+''' and SubArea = '''+@SubArea+''' and Mes='''+@Mes+'''  '" +
          " if (@Mes != ''  and @Area != '' and @SubArea  = '' and @Departamento != '' ) set @Filtro =  @Consulta+' where Departamento = '''+@Departamento+''' and  Area = '''+@Area+'''  and Mes='''+@Mes+'''  ' " +
          " if (@Mes != ''  and @Area != '' and @SubArea != '' and @Departamento = '')  	set @Filtro =  @Consulta+' where Area = '''+@Area+''' and SubArea = '''+@SubArea+''' and Mes='''+@Mes+'''  ' " +
          " if (@Mes != ''  and @Area != '' and @SubArea = '' and @Departamento = '')  set @Filtro =  @Consulta+' where Area = '''+@Area+'''  and Mes='''+@Mes+''' ' " +
          " if (@Mes != ''  and @Area = '' and @SubArea = '' and @Departamento = ''  )  	  set @Filtro =  @Consulta+' where Mes='''+@Mes+'''  ' " +
          " if (@Mes != ''  and @Area = '' and @SubArea  != '' and @Departamento != '' )  	set @Filtro =  @Consulta+' where Departamento = '''+@Departamento+''' and SubArea = '''+@SubArea+''' and Mes='''+@Mes+''' ' " +
          " if (@Mes != ''  and @Area = '' and @SubArea = '' and @Departamento != '')  	set @Filtro =  @Consulta+' where Departamento = '''+@Departamento+''' and Mes='''+@Mes+'''  ' " +
          " if (@Mes != ''  and @Area = '' and @SubArea  != '' and @Departamento = '' )  	set @Filtro =  @Consulta+' where SubArea = '''+@SubArea+''' and Mes='''+@Mes+'''  ' " +
          " if (@Mes = ''  and @Area != '' and @SubArea != '' and @Departamento != '' )   set @Filtro =  @Consulta+' where Departamento = '''+@Departamento+''' and  Area = '''+@Area+''' and SubArea = '''+@SubArea+''' ' " +
          " if (@Mes = ''  and @Area != '' and @SubArea  = '' and @Departamento != '' ) set @Filtro =  @Consulta+' where Departamento = '''+@Departamento+''' and  Area = '''+@Area+'''   ' " +
          " if (@Mes = ''  and @Area != '' and @SubArea != '' and @Departamento = '')  	set @Filtro =  @Consulta+' where Area = '''+@Area+''' and SubArea = '''+@SubArea+'''  ' " +
          " if (@Mes = ''  and @Area != '' and @SubArea = '' and @Departamento = '')  set @Filtro =  @Consulta+' where Area = '''+@Area+'''  ' " +
          " if (@Mes = ''  and @Area = '' and @SubArea  != '' and @Departamento != '' )  	set @Filtro =  @Consulta+' where Departamento = '''+@Departamento+''' and SubArea = '''+@SubArea+'''  ' " +
          " if (@Mes = ''  and @Area = '' and @SubArea = '' and @Departamento != '')  	set @Filtro =  @Consulta+' where Departamento = '''+@Departamento+'''   ' " +
          " if (@Mes = ''  and @Area = '' and @SubArea  != '' and @Departamento = '' )  	set @Filtro =  @Consulta+' where SubArea = '''+@SubArea+'''  ' " +
          " set @Consulta=  @Filtro +' order by Sucursal' " +
          " execute(@Consulta)";
            return consulta;
        }
        public String Personal_CostoPlanilla_EvolucionGeneralMensual_ddlDepartamento_SQL(String mes, String area, String subArea, String sucursal)
        {        //GIAMPIERE 09/ 09 / 2019
            consulta =
          " declare @Area nvarchar(max),@SubArea nvarchar(max),@Sucursal nvarchar(max),@Mes nvarchar(max),@Consulta nvarchar(max),@Filtro nvarchar(max),@select nvarchar(max);" +
          " set @Area='" + area + "';set @SubArea='" + subArea + "'; set @Sucursal= '" + sucursal + "'; set @Mes='" + mes + "'; set @Consulta= ''; set @Filtro= ''; set @select= ''; " +
          " set @Consulta= N'select distinct Departamento from Personal.DTM_Personal_Planilla'" +
          " if (@Mes = ''  and @Area = '' and @SubArea = '' and @Sucursal = ''  )  set @Filtro=  @Consulta " +
          " if (@Mes != ''  and @Area != '' and @SubArea != '' and @Sucursal != '' )   set @Filtro =  @Consulta+ ' where Sucursal = '''+@Sucursal+''' and  Area = '''+@Area+''' and SubArea = '''+@SubArea+''' and Mes='''+@Mes+'''  ' " +
          " if (@Mes != ''  and @Area != '' and @SubArea  = '' and @Sucursal != '' ) set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and  Area = '''+@Area+'''  and Mes='''+@Mes+'''  '" +
          " if (@Mes != ''  and @Area != '' and @SubArea != '' and @Sucursal = '')  	set @Filtro =  @Consulta+' where Area = '''+@Area+''' and SubArea = '''+@SubArea+''' and Mes='''+@Mes+'''  ' " +
          " if (@Mes != ''  and @Area != '' and @SubArea = '' and @Sucursal = '')  set @Filtro =  @Consulta+' where Area = '''+@Area+'''  and Mes='''+@Mes+''' '  " +
          " if (@Mes != ''  and @Area = '' and @SubArea = '' and @Sucursal = ''  )  	  set @Filtro =  @Consulta+' where Mes='''+@Mes+'''  ' " +
          " if (@Mes != ''  and @Area = '' and @SubArea  != '' and @Sucursal != '' )  	set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and SubArea = '''+@SubArea+''' and Mes='''+@Mes+''' '  " +
          " if (@Mes != ''  and @Area = '' and @SubArea = '' and @Sucursal != '')  	set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and Mes='''+@Mes+'''  ' " +
          " if (@Mes != ''  and @Area = '' and @SubArea  != '' and @Sucursal = '' )  	set @Filtro =  @Consulta+' where SubArea = '''+@SubArea+''' and Mes='''+@Mes+'''  '" +
          " if (@Mes = ''  and @Area != '' and @SubArea != '' and @Sucursal != '' )   set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and  Area = '''+@Area+''' and SubArea = '''+@SubArea+''' ' " +
          " if (@Mes = ''  and @Area != '' and @SubArea  = '' and @Sucursal != '' ) set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and  Area = '''+@Area+'''   '" +
          " if (@Mes = ''  and @Area != '' and @SubArea != '' and @Sucursal = '')  	set @Filtro =  @Consulta+' where Area = '''+@Area+''' and SubArea = '''+@SubArea+'''  '" +
          " if (@Mes = ''  and @Area != '' and @SubArea = '' and @Sucursal = '')  set @Filtro =  @Consulta+' where Area = '''+@Area+'''  '" +
          " if (@Mes = ''  and @Area = '' and @SubArea  != '' and @Sucursal != '' )  	set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and SubArea = '''+@SubArea+'''  ' " +
          " if (@Mes = ''  and @Area = '' and @SubArea = '' and @Sucursal != '')  	set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+'''   ' " +
          " if (@Mes = ''  and @Area = '' and @SubArea  != '' and @Sucursal = '' )  	set @Filtro =  @Consulta+' where SubArea = '''+@SubArea+'''  '" +
          " set @Consulta=  @Filtro +' order by Departamento'" +
          " execute(@Consulta);";
            return consulta;
        }
        public String Personal_CostoPlanilla_EvolucionGeneralMensual_ddlArea_SQL(String mes, String departamento, String subArea, String sucursal)
        {        //GIAMPIERE 09/ 09 / 2019
            consulta =
          " declare @SubArea nvarchar(max),@Departamento nvarchar(max),@Sucursal nvarchar(max),@Mes nvarchar(max),@Consulta nvarchar(max),@Filtro nvarchar(max),@select nvarchar(max);" +
          " set @Departamento= '" + departamento + "';set @SubArea='" + subArea + "'; set @Sucursal= '" + sucursal + "'; set @Mes='" + mes + "'; set @Consulta= ''; set @Filtro= ''; set @select= ''; " +
          " set @Consulta= N'select distinct Area from Personal.DTM_Personal_Planilla'" +
          " if (@Mes = ''  and @SubArea = '' and @Departamento = '' and @Sucursal = ''  )  set @Filtro=  @Consulta " +
          " if (@Mes != ''  and @SubArea != '' and @Departamento != '' and @Sucursal != '' )   set @Filtro =  @Consulta+ ' where Sucursal = '''+@Sucursal+''' and  SubArea = '''+@SubArea+''' and Departamento = '''+@Departamento+''' and Mes='''+@Mes+'''  ' " +
          " if (@Mes != ''  and @SubArea != '' and @Departamento  = '' and @Sucursal != '' ) set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and  SubArea = '''+@SubArea+'''  and Mes='''+@Mes+'''  '" +
          " if (@Mes != ''  and @SubArea != '' and @Departamento != '' and @Sucursal = '')  	set @Filtro =  @Consulta+' where SubArea = '''+@SubArea+''' and Departamento = '''+@Departamento+''' and Mes='''+@Mes+'''  ' " +
          " if (@Mes != ''  and @SubArea != '' and @Departamento = '' and @Sucursal = '')  set @Filtro =  @Consulta+' where SubArea = '''+@SubArea+'''  and Mes='''+@Mes+''' '  " +
          " if (@Mes != ''  and @SubArea = '' and @Departamento = '' and @Sucursal = ''  )  	  set @Filtro =  @Consulta+' where Mes='''+@Mes+'''  ' " +
          " if (@Mes != ''  and @SubArea = '' and @Departamento  != '' and @Sucursal != '' )  	set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and Departamento = '''+@Departamento+''' and Mes='''+@Mes+''' '  " +
          " if (@Mes != ''  and @SubArea = '' and @Departamento = '' and @Sucursal != '')  	set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and Mes='''+@Mes+'''  ' " +
          " if (@Mes != ''  and @SubArea = '' and @Departamento  != '' and @Sucursal = '' )  	set @Filtro =  @Consulta+' where Departamento = '''+@Departamento+''' and Mes='''+@Mes+'''  '" +
          " if (@Mes = ''  and @SubArea != '' and @Departamento != '' and @Sucursal != '' )   set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and  SubArea = '''+@SubArea+''' and Departamento = '''+@Departamento+''' ' " +
          " if (@Mes = ''  and @SubArea != '' and @Departamento  = '' and @Sucursal != '' ) set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and  SubArea = '''+@SubArea+'''   '" +
          " if (@Mes = ''  and @SubArea != '' and @Departamento != '' and @Sucursal = '')  	set @Filtro =  @Consulta+' where SubArea = '''+@SubArea+''' and Departamento = '''+@Departamento+'''  '" +
          " if (@Mes = ''  and @SubArea != '' and @Departamento = '' and @Sucursal = '')  set @Filtro =  @Consulta+' where SubArea = '''+@SubArea+'''  '" +
          " if (@Mes = ''  and @SubArea = '' and @Departamento  != '' and @Sucursal != '' )  	set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and Departamento = '''+@Departamento+'''  ' " +
          " if (@Mes = ''  and @SubArea = '' and @Departamento = '' and @Sucursal != '')  	set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+'''   ' " +
          " if (@Mes = ''  and @SubArea = '' and @Departamento  != '' and @Sucursal = '' )  	set @Filtro =  @Consulta+' where Departamento = '''+@Departamento+'''  '" +
          " set @Consulta=  @Filtro +' order by Area'" +
          " execute(@Consulta)";

            return consulta;
        }
        public String Personal_CostoPlanilla_EvolucionGeneralMensual_ddlSubArea_SQL(String mes, String departamento, String area, String sucursal)
        {        //GIAMPIERE 09/ 09 / 2019
            consulta =
          " declare @Area nvarchar(max),@Departamento nvarchar(max),@Sucursal nvarchar(max),@Mes nvarchar(max),@Consulta nvarchar(max),@Filtro nvarchar(max),@select nvarchar(max);" +
          " set @Departamento= '" + departamento + "'; set @Area='" + area + "';set @Sucursal= '" + sucursal + "'; set @Mes='" + mes + "'; set @Consulta= ''; set @Filtro= ''; set @select= ''; " +
          " set @Consulta= N'select distinct SubArea from Personal.DTM_Personal_Planilla'" +
          " if (@Mes = ''  and @Area = '' and @Departamento = '' and @Sucursal = ''  )  set @Filtro=  @Consulta " +
          " if (@Mes != ''  and @Area != '' and @Departamento != '' and @Sucursal != '' )   set @Filtro =  @Consulta+ ' where Sucursal = '''+@Sucursal+''' and  Area = '''+@Area+''' and Departamento = '''+@Departamento+''' and Mes='''+@Mes+'''  ' " +
          " if (@Mes != ''  and @Area != '' and @Departamento  = '' and @Sucursal != '' ) set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and  Area = '''+@Area+'''  and Mes='''+@Mes+'''  '" +
          " if (@Mes != ''  and @Area != '' and @Departamento != '' and @Sucursal = '')  	set @Filtro =  @Consulta+' where Area = '''+@Area+''' and Departamento = '''+@Departamento+''' and Mes='''+@Mes+'''  ' " +
          " if (@Mes != ''  and @Area != '' and @Departamento = '' and @Sucursal = '')  set @Filtro =  @Consulta+' where Area = '''+@Area+'''  and Mes='''+@Mes+''' '  " +
          " if (@Mes != ''  and @Area = '' and @Departamento = '' and @Sucursal = ''  )  	  set @Filtro =  @Consulta+' where Mes='''+@Mes+'''  ' " +
          " if (@Mes != ''  and @Area = '' and @Departamento  != '' and @Sucursal != '' )  	set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and Departamento = '''+@Departamento+''' and Mes='''+@Mes+''' '  " +
          " if (@Mes != ''  and @Area = '' and @Departamento = '' and @Sucursal != '')  	set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and Mes='''+@Mes+'''  ' " +
          " if (@Mes != ''  and @Area = '' and @Departamento  != '' and @Sucursal = '' )  	set @Filtro =  @Consulta+' where Departamento = '''+@Departamento+''' and Mes='''+@Mes+'''  '" +
          " if (@Mes = ''  and @Area != '' and @Departamento != '' and @Sucursal != '' )   set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and  Area = '''+@Area+''' and Departamento = '''+@Departamento+''' ' " +
          " if (@Mes = ''  and @Area != '' and @Departamento  = '' and @Sucursal != '' ) set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and  Area = '''+@Area+'''   '" +
          " if (@Mes = ''  and @Area != '' and @Departamento != '' and @Sucursal = '')  	set @Filtro =  @Consulta+' where Area = '''+@Area+''' and Departamento = '''+@Departamento+'''  '" +
          " if (@Mes = ''  and @Area != '' and @Departamento = '' and @Sucursal = '')  set @Filtro =  @Consulta+' where Area = '''+@Area+'''  '" +
          " if (@Mes = ''  and @Area = '' and @Departamento  != '' and @Sucursal != '' )  	set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and Departamento = '''+@Departamento+'''  ' " +
          " if (@Mes = ''  and @Area = '' and @Departamento = '' and @Sucursal != '')  	set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+'''   ' " +
          " if (@Mes = ''  and @Area = '' and @Departamento  != '' and @Sucursal = '' )  	set @Filtro =  @Consulta+' where Departamento = '''+@Departamento+'''  '" +
          " set @Consulta=  @Filtro +' order by SubArea'" +
          " execute(@Consulta)";
            return consulta;

        }
        public String Personal_CostoPlanilla_EvolucionGeneralMensual_ddlMes_SQL(String departamento, String area, String subArea, String sucursal)
        {   //GIAMPIERE 09/ 09 / 2019
            consulta =
          " declare @SubArea nvarchar(max),@Departamento nvarchar(max),@Sucursal nvarchar(max),@Area nvarchar(max),@Consulta nvarchar(max),@Filtro nvarchar(max),@select nvarchar(max);" +
         " set @Departamento= '" + departamento + "'; set @Area='" + area + "';set @SubArea='" + subArea + "'; set @Sucursal= '" + sucursal + "';set @Consulta= ''; set @Filtro= ''; set @select= ''; " +
          " set @Consulta= N'select distinct DATENAME(MONTH,Fecha) as Meses ,Mes from Personal.DTM_Personal_Planilla'" +
          " if (@Area = ''  and @SubArea = '' and @Departamento = '' and @Sucursal = ''  )  set @Filtro=  @Consulta " +
          " if (@Area != ''  and @SubArea != '' and @Departamento != '' and @Sucursal != '' )   set @Filtro =  @Consulta+ ' where Sucursal = '''+@Sucursal+''' and  SubArea = '''+@SubArea+''' and Departamento = '''+@Departamento+''' and Area='''+@Area+'''  ' " +
          " if (@Area != ''  and @SubArea != '' and @Departamento  = '' and @Sucursal != '' ) set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and  SubArea = '''+@SubArea+'''  and Area='''+@Area+'''  '" +
          " if (@Area != ''  and @SubArea != '' and @Departamento != '' and @Sucursal = '')  	set @Filtro =  @Consulta+' where SubArea = '''+@SubArea+''' and Departamento = '''+@Departamento+''' and Area='''+@Area+'''  ' " +
          " if (@Area != ''  and @SubArea != '' and @Departamento = '' and @Sucursal = '')  set @Filtro =  @Consulta+' where SubArea = '''+@SubArea+'''  and Area='''+@Area+''' '  " +
          " if (@Area != ''  and @SubArea = '' and @Departamento = '' and @Sucursal = ''  )  	  set @Filtro =  @Consulta+' where Area='''+@Area+'''  ' " +
          " if (@Area != ''  and @SubArea = '' and @Departamento  != '' and @Sucursal != '' )  	set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and Departamento = '''+@Departamento+''' and Area='''+@Area+''' '  " +
          " if (@Area != ''  and @SubArea = '' and @Departamento = '' and @Sucursal != '')  	set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and Area='''+@Area+'''  ' " +
          " if (@Area != ''  and @SubArea = '' and @Departamento  != '' and @Sucursal = '' )  	set @Filtro =  @Consulta+' where Departamento = '''+@Departamento+''' and Area='''+@Area+'''  '" +
          " if (@Area = ''  and @SubArea != '' and @Departamento != '' and @Sucursal != '' )   set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and  SubArea = '''+@SubArea+''' and Departamento = '''+@Departamento+''' ' " +
          " if (@Area = ''  and @SubArea != '' and @Departamento  = '' and @Sucursal != '' ) set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and  SubArea = '''+@SubArea+'''   '" +
          " if (@Area = ''  and @SubArea != '' and @Departamento != '' and @Sucursal = '')  	set @Filtro =  @Consulta+' where SubArea = '''+@SubArea+''' and Departamento = '''+@Departamento+'''  '" +
          " if (@Area = ''  and @SubArea != '' and @Departamento = '' and @Sucursal = '')  set @Filtro =  @Consulta+' where SubArea = '''+@SubArea+'''  '" +
          " if (@Area = ''  and @SubArea = '' and @Departamento  != '' and @Sucursal != '' )  	set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and Departamento = '''+@Departamento+'''  ' " +
          " if (@Area = ''  and @SubArea = '' and @Departamento = '' and @Sucursal != '')  	set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+'''   ' " +
          " if (@Area = ''  and @SubArea = '' and @Departamento  != '' and @Sucursal = '' )  	set @Filtro =  @Consulta+' where Departamento = '''+@Departamento+'''  '" +
          " set @Consulta=  @Filtro +' order by Mes'" +
          " execute(@Consulta)";
            return consulta;
        }
        //EVOLUCIÓN POR ÁREA MENSUAL
        //GIAMPIERE 09/ 09 / 2019
        public String G_Personal_CostoPlanilla_EvolucionAreaMensual_SQL(String anio, String mes, String area, String subArea, String sucursal)
        {        //GIAMPIERE 09/ 09 / 2019
            consulta =
                " declare @Departamento varchar(max),@Año nvarchar(max), @Area nvarchar(max), @Mes nvarchar(3),@SubArea nvarchar(max),@Sucursal nvarchar(max), @filtro  NVARCHAR(MAX), @cols NVARCHAR(MAX), @null  NVARCHAR(MAX), @consulta  NVARCHAR(MAX); " +
            " set @Año='" + anio + "'; set @Area='" + area + "';set @SubArea='" + subArea + "'; set @Sucursal= '" + sucursal + "'; set @Mes='" + mes + "';set @consulta ='';set @filtro =''; " +
            " select @cols = STUFF((SELECT Distinct ',' + QUOTENAME(Departamento)  from  Personal.DTM_Personal_Planilla group by ',' + QUOTENAME(Departamento)Order by ',' + QUOTENAME(Departamento) FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)')  ,1,1,'') " +
            " select @null = STUFF((SELECT Distinct ', isnull(' + QUOTENAME(Departamento)+',0) as'  + QUOTENAME(Departamento) from  Personal.DTM_Personal_Planilla  group by ', isnull(' + QUOTENAME(Departamento)+',0) as'  + QUOTENAME(Departamento) Order by ', isnull(' + QUOTENAME(Departamento)+',0) as'  + QUOTENAME(Departamento)   FOR XML PATH(''), TYPE  ).value('.', 'NVARCHAR(MAX)')   ,1,1,'')  " +
            " if  (@Mes = '' and @Año = ''  and @Area = '' and @SubArea = '' and @Sucursal = ''  )  	  set @consulta = 'SELECT Meses,' + @null + '  from  ( select Mes,DATENAME(MONTH,Fecha) as Meses, Departamento,Total from Personal.DTM_Personal_Planilla   where KeyPersonalPlanilla in(select max(KeyPersonalPlanilla) from Personal.DTM_Personal_Planilla group by Mes ,Anual, Departamento,Area,SubArea,Sucursal) )  x  pivot ( sum(Total) for Departamento in (' + @cols + ') ) p  order by Mes ' " +
            " else " +
            " begin " +
            " if  (@Mes != '' and @Año != '' and @Area != '' and @SubArea != '' and @Sucursal != '' )  	 set @filtro = ' Sucursal = '''+@Sucursal+''' and Anual = '''+@Año+''' and Area = '''+@Area+''' and SubArea = '''+@SubArea+'''  and Mes = '''+@Mes+''' ' " +
            " if  (@Mes != '' and @Año != ''  and @Area = '' and @SubArea = '' and @Sucursal = ''  )  	  set @filtro = ' Anual = '''+@Año+'''  and Mes = '''+@Mes+''' ' " +
            " if  (@Mes != '' and @Año != ''  and @Area != '' and @SubArea  = '' and @Sucursal != '' )  	 set @filtro = ' Sucursal = '''+@Sucursal+''' and Anual = '''+@Año+''' and Area='''+@Area+'''  and Mes = '''+@Mes+''' ' " +
            " if  (@Mes != '' and @Año != '' and @Area != '' and @SubArea != '' and @Sucursal = '')  	 set @filtro = ' Anual = '''+@Año+''' and Area = '''+@Area+''' and SubArea = '''+@SubArea+'''  and Mes = '''+@Mes+''' ' " +
            " if  (@Mes != '' and @Año != ''  and @Area = '' and @SubArea  != '' and @Sucursal != '' )  	  set @filtro = '  Sucursal = '''+@Sucursal+''' and Anual = '''+@Año+''' and SubArea = '''+@SubArea+'''  and Mes = '''+@Mes+'''' " +
            " if  (@Mes != '' and @Año != '' and @Area = '' and @SubArea = '' and @Sucursal != '')  	 set @filtro = ' Sucursal = '''+@Sucursal+''' and Anual = '''+@Año+''' and Mes = '''+@Mes+'''  ' " +
            " if  (@Mes != '' and @Año != ''  and @Area = '' and @SubArea  != '' and @Sucursal = '' )  	  set @filtro = '  Anual = '''+@Año+''' and SubArea = '''+@SubArea+'''  and Mes = '''+@Mes+''' ' " +
            " if  (@Mes != '' and @Año != '' and @Area != '' and @SubArea = '' and @Sucursal = '')  	 set @filtro = ' Anual = '''+@Año+''' and Area = '''+@Area+'''  and Mes = '''+@Mes+'''' " +
            " if  (@Mes != '' and @Año = '' and @Area != '' and @SubArea != '' and @Sucursal != '' )  	 set @filtro = ' Sucursal = '''+@Sucursal+'''  and Area = '''+@Area+''' and SubArea = '''+@SubArea+'''  and Mes = '''+@Mes+''' ' " +
            " if  (@Mes != '' and @Año = ''  and @Area = '' and @SubArea = '' and @Sucursal = ''  )  	  set @filtro = ' Mes = '''+@Mes+''' ' " +
            " if  (@Mes != '' and @Año = ''  and @Area != '' and @SubArea  = '' and @Sucursal != '' )  	 set @filtro = ' Sucursal = '''+@Sucursal+''' and Area='''+@Area+''' and Mes = '''+@Mes+''' ' " +
            " if  (@Mes != '' and @Año = '' and @Area != '' and @SubArea != '' and @Sucursal = '')  	 set @filtro = ' Area = '''+@Area+''' and SubArea = '''+@SubArea+'''  and Mes = '''+@Mes+''' ' " +
            " if  (@Mes != '' and @Año = ''  and @Area = '' and @SubArea  != '' and @Sucursal != '' )  	  set @filtro = '  Sucursal = '''+@Sucursal+'''  and SubArea = '''+@SubArea+'''  and Mes = '''+@Mes+''' ' " +
            " if  (@Mes != '' and @Año = '' and @Area = '' and @SubArea = '' and @Sucursal != '')  	 set @filtro = ' Sucursal = '''+@Sucursal+'''  and Mes = '''+@Mes+''' ' " +
            " if  (@Mes != '' and @Año = ''  and @Area = '' and @SubArea  != '' and @Sucursal = '' )  	  set @filtro = '  SubArea = '''+@SubArea+'''  and Mes = '''+@Mes+''' ' " +
            " if  (@Mes != '' and @Año = '' and @Area != '' and @SubArea = '' and @Sucursal = '')  	 set @filtro = 'Area = '''+@Area+'''  and Mes = '''+@Mes+''' '" +
            " if  (@Mes = '' and @Año != '' and @Area != '' and @SubArea != '' and @Sucursal != '' )  	 set @filtro = ' Sucursal = '''+@Sucursal+''' and Anual = '''+@Año+''' and Area = '''+@Area+''' and SubArea = '''+@SubArea+''' ' " +
            " if  (@Mes = '' and @Año != ''  and @Area = '' and @SubArea = '' and @Sucursal = ''  )  	  set @filtro = ' Anual = '''+@Año+''''" +
            " if  (@Mes = '' and @Año != ''  and @Area != '' and @SubArea  = '' and @Sucursal != '' )  	 set @filtro = ' Sucursal = '''+@Sucursal+''' and Anual = '''+@Año+''' and Area='''+@Area+''' ' " +
            " if  (@Mes = '' and @Año != '' and @Area != '' and @SubArea != '' and @Sucursal = '')  	 set @filtro = ' Anual = '''+@Año+''' and Area = '''+@Area+''' and SubArea = '''+@SubArea+''' ' " +
            " if  (@Mes = '' and @Año != ''  and @Area = '' and @SubArea  != '' and @Sucursal != '' )  	  set @filtro = '  Sucursal = '''+@Sucursal+''' and Anual = '''+@Año+''' and SubArea = '''+@SubArea+''' ' " +
            " if  (@Mes = '' and @Año != '' and @Area = '' and @SubArea = '' and @Sucursal != '')  	 set @filtro = ' Sucursal = '''+@Sucursal+''' and Anual = '''+@Año+''' ' " +
            " if  (@Mes = '' and @Año != ''  and @Area = '' and @SubArea  != '' and @Sucursal = '' )  	  set @filtro = '  Anual = '''+@Año+''' and SubArea = '''+@SubArea+''' ' " +
            " if  (@Mes = '' and @Año != '' and @Area != '' and @SubArea = '' and @Sucursal = '')  	 set @filtro = ' Anual = '''+@Año+''' and Area = '''+@Area+''' ' " +
            " if  (@Mes = '' and @Año = '' and @Area != '' and @SubArea != '' and @Sucursal != '' )  	 set @filtro = ' Sucursal = '''+@Sucursal+'''  and Area = '''+@Area+''' and SubArea = '''+@SubArea+''' ' " +
            " if  (@Mes = '' and @Año = ''  and @Area != '' and @SubArea  = '' and @Sucursal != '' )  	 set @filtro = ' Sucursal = '''+@Sucursal+''' and Area='''+@Area+''' ' " +
            " if  (@Mes = '' and @Año = '' and @Area != '' and @SubArea != '' and @Sucursal = '')  	 set @filtro = ' Area = '''+@Area+''' and SubArea = '''+@SubArea+''' ' " +
            " if  (@Mes = '' and @Año = ''  and @Area = '' and @SubArea  != '' and @Sucursal != '' )  	  set @filtro = '  Sucursal = '''+@Sucursal+'''  and SubArea = '''+@SubArea+''' ' " +
            " if  (@Mes = '' and @Año = '' and @Area = '' and @SubArea = '' and @Sucursal != '')  	 set @filtro = ' Sucursal = '''+@Sucursal+''' ' " +
            " if  (@Mes = '' and @Año = ''  and @Area = '' and @SubArea  != '' and @Sucursal = '' )  	  set @filtro = '  SubArea = '''+@SubArea+''' ' " +
            " if  (@Mes = '' and @Año = '' and @Area != '' and @SubArea = '' and @Sucursal = '')  	 set @filtro = 'Area = '''+@Area+''' ' " +
            " set @consulta = 'SELECT Meses,' + @null + '  from  ( select Mes,DATENAME(MONTH,Fecha) as Meses, Departamento,Total from Personal.DTM_Personal_Planilla   where ' +@filtro+ ' and KeyPersonalPlanilla in(select max(KeyPersonalPlanilla) from Personal.DTM_Personal_Planilla where ' +@filtro+ '  group by Mes ,Anual, Departamento,Area,SubArea,Sucursal) )  x  pivot ( sum(Total) for Departamento in (' + @cols + ') ) p  order by Mes ' " +
            " end " +
            " execute(@consulta)";
            return consulta;
        }
        public String Personal_CostoPlanilla_EvolucionAreaMensual_ddlSucursal_SQL(String anio, String mes, String area, String subArea)
        {        //GIAMPIERE 09/ 09 / 2019
            consulta =
                 " declare @Año varchar(20), @Area nvarchar(max),@SubArea nvarchar(max),@Mes nvarchar(max),@Consulta nvarchar(max),@filtro nvarchar(max),@select nvarchar(max); " +
            " set @Año='" + anio + "'; set @Area='" + area + "';set @SubArea='" + subArea + "'; set @Mes='" + mes + "';set @Consulta= ''; set @filtro= ''; set @select= ''; " +
            " set @select= N'select distinct Sucursal from Personal.DTM_Personal_Planilla ' " +
            " if ( @Año != '' and @Area != '' and @SubArea != '' and @Mes != '' )   set @filtro = @select+  ' where  Mes = '''+@Mes+''' and Anual = '''+@Año+''' and Area = '''+@Area+''' and SubArea = '''+@SubArea+''' ' " +
            " if ( @Año != '' and @Area != '' and @SubArea  = '' and @Mes != '' ) set @filtro = @select+  ' where  Mes = '''+@Mes+''' and Anual = '''+@Año+''' and Area = '''+@Area+''' ' " +
            " if ( @Año != '' and @Area != '' and @SubArea != '' and @Mes = '')  	set @filtro = @select+  ' where  Anual = '''+@Año+''' and Area = '''+@Area+''' and SubArea = '''+@SubArea+''' ' " +
            " if ( @Año != '' and @Area != '' and @SubArea = '' and @Mes = '')  set @filtro = @select+  ' where  Anual = '''+@Año+''' and Area = '''+@Area+''' ' " +
            " if ( @Año != '' and @Area = '' and @SubArea = '' and @Mes = ''  )  	  set @filtro = @select+  ' where Anual = '''+@Año+'''   ' " +
            " if ( @Año != '' and @Area = '' and @SubArea  != '' and @Mes != '' )  	set @filtro = @select+  ' where  Mes = '''+@Mes+''' and Anual = '''+@Año+'''  and SubArea = '''+@SubArea+''' ' " +
            " if ( @Año != '' and @Area = '' and @SubArea = '' and @Mes != '')  	set @filtro = @select+  ' where  Mes = '''+@Mes+''' and Anual = '''+@Año+'''   ' " +
            " if ( @Año != '' and @Area = '' and @SubArea  != '' and @Mes = '' )  	set @filtro = @select+  ' where  Anual = '''+@Año+''' and SubArea = '''+@SubArea+'''  '  " +
            " if ( @Año = '' and @Area != '' and @SubArea != '' and @Mes != '' )   set @filtro = @select+  ' where  Mes = '''+@Mes+''' and  Area = '''+@Area+''' and SubArea = '''+@SubArea+'''  ' " +
            " if ( @Año = '' and @Area != '' and @SubArea  = '' and @Mes != '' ) set @filtro = @select+  ' where  Mes = '''+@Mes+''' and  Area = '''+@Area+''' ' " +
            " if ( @Año = '' and @Area != '' and @SubArea != '' and @Mes = '')  	set @filtro = @select+  ' where   Area = '''+@Area+''' and SubArea = '''+@SubArea+''' ' " +
            " if ( @Año = '' and @Area != '' and @SubArea = '' and @Mes = '')  set @filtro = @select+  ' where   Area = '''+@Area+''' ' " +
            " if ( @Año = '' and @Area = '' and @SubArea = '' and @Mes = ''  )  	  set @filtro = @select+  '' " +
            " if ( @Año = '' and @Area = '' and @SubArea  != '' and @Mes != '' )  	set @filtro = @select+  ' where  Mes = '''+@Mes+''' and SubArea = '''+@SubArea+''' ' " +
            " if ( @Año = '' and @Area = '' and @SubArea = '' and @Mes != '')  	set @filtro = @select+  ' where  Mes = '''+@Mes+'''    ' " +
            " if ( @Año = '' and @Area = '' and @SubArea  != '' and @Mes = '' )  	set @filtro = @select+  ' where   SubArea = '''+@SubArea+''' ' " +
            " set @Consulta= @filtro+ 'order by Sucursal'; " +
            " execute(@Consulta) ";
            return consulta;
        }
        public String Personal_CostoPlanilla_EvolucionAreaMensual_ddlArea_SQL(String anio, String mes, String subArea, String sucursal)
        {        //GIAMPIERE 09/ 09 / 2019
            consulta =
                 " declare @Año varchar(5), @SubArea nvarchar(max),@Sucursal nvarchar(max),@Mes nvarchar(max),@Consulta nvarchar(max),@filtro nvarchar(max),@select nvarchar(max); " +
            " set @Año='" + anio + "'; set @SubArea='" + subArea + "'; set @Sucursal= '" + sucursal + "'; set @Mes='" + mes + "';set @Consulta= ''; set @filtro= ''; set @select= ''; " +
            " set @select= N'select distinct Area from Personal.DTM_Personal_Planilla ' " +
            " if ( @Año != '' and @SubArea != '' and @Sucursal != '' and @Mes != '' )   set @filtro = @select+  ' where  Mes = '''+@Mes+''' and Anual = '''+@Año+''' and SubArea = '''+@SubArea+''' and Sucursal = '''+@Sucursal+''' ' " +
            " if ( @Año != '' and @SubArea != '' and @Sucursal  = '' and @Mes != '' ) set @filtro = @select+  ' where  Mes = '''+@Mes+''' and Anual = '''+@Año+''' and SubArea = '''+@SubArea+''' ' " +
            " if ( @Año != '' and @SubArea != '' and @Sucursal != '' and @Mes = '')  	set @filtro = @select+  ' where  Anual = '''+@Año+''' and SubArea = '''+@SubArea+''' and Sucursal = '''+@Sucursal+''' ' " +
            " if ( @Año != '' and @SubArea != '' and @Sucursal = '' and @Mes = '')  set @filtro = @select+  ' where  Anual = '''+@Año+''' and SubArea = '''+@SubArea+''' ' " +
            " if ( @Año != '' and @SubArea = '' and @Sucursal = '' and @Mes = ''  )  	  set @filtro = @select+  ' where Anual = '''+@Año+'''   ' " +
            " if ( @Año != '' and @SubArea = '' and @Sucursal  != '' and @Mes != '' )  	set @filtro = @select+  ' where  Mes = '''+@Mes+''' and Anual = '''+@Año+'''  and Sucursal = '''+@Sucursal+''' ' " +
            " if ( @Año != '' and @SubArea = '' and @Sucursal = '' and @Mes != '')  	set @filtro = @select+  ' where  Mes = '''+@Mes+''' and Anual = '''+@Año+'''   ' " +
            " if ( @Año != '' and @SubArea = '' and @Sucursal  != '' and @Mes = '' )  	set @filtro = @select+  ' where  Anual = '''+@Año+''' and Sucursal = '''+@Sucursal+'''  '  " +
            " if ( @Año = '' and @SubArea != '' and @Sucursal != '' and @Mes != '' )   set @filtro = @select+  ' where  Mes = '''+@Mes+''' and  SubArea = '''+@SubArea+''' and Sucursal = '''+@Sucursal+'''  ' " +
            " if ( @Año = '' and @SubArea != '' and @Sucursal  = '' and @Mes != '' ) set @filtro = @select+  ' where  Mes = '''+@Mes+''' and  SubArea = '''+@SubArea+''' ' " +
            " if ( @Año = '' and @SubArea != '' and @Sucursal != '' and @Mes = '')  	set @filtro = @select+  ' where   SubArea = '''+@SubArea+''' and Sucursal = '''+@Sucursal+''' ' " +
            " if ( @Año = '' and @SubArea != '' and @Sucursal = '' and @Mes = '')  set @filtro = @select+  ' where   SubArea = '''+@SubArea+''' ' " +
            " if ( @Año = '' and @SubArea = '' and @Sucursal = '' and @Mes = ''  )  	  set @filtro = @select+  '' " +
            " if ( @Año = '' and @SubArea = '' and @Sucursal  != '' and @Mes != '' )  	set @filtro = @select+  ' where  Mes = '''+@Mes+''' and Sucursal = '''+@Sucursal+''' ' " +
            " if ( @Año = '' and @SubArea = '' and @Sucursal = '' and @Mes != '')  	set @filtro = @select+  ' where  Mes = '''+@Mes+'''    ' " +
            " if ( @Año = '' and @SubArea = '' and @Sucursal  != '' and @Mes = '' )  	set @filtro = @select+  ' where   Sucursal = '''+@Sucursal+''' ' " +
            " set @Consulta= @filtro+ 'order by Area'; " +
            " execute(@Consulta) ";
            return consulta;
        }
        public String Personal_CostoPlanilla_EvolucionAreaMensual_ddlSubArea_SQL(String anio, String mes, String area, String sucursal)
        {        //GIAMPIERE 09/ 09 / 2019
            consulta =
                  " declare @Año varchar(20), @Area nvarchar(max),@Sucursal nvarchar(max),@Mes nvarchar(max),@Consulta nvarchar(max),@filtro nvarchar(max),@select nvarchar(max); " +
            " set @Año='" + anio + "'; set @Area='" + area + "';set @Sucursal= '" + sucursal + "'; set @Mes='" + mes + "';set @Consulta= ''; set @filtro= ''; set @select= ''; " +
            " set @select= N'select distinct SubArea from Personal.DTM_Personal_Planilla ' " +
            " if ( @Año != '' and @Area != '' and @Sucursal != '' and @Mes != '' )   set @filtro = @select+  ' where  Mes = '''+@Mes+''' and Anual = '''+@Año+''' and Area = '''+@Area+''' and Sucursal = '''+@Sucursal+''' ' " +
            " if ( @Año != '' and @Area != '' and @Sucursal  = '' and @Mes != '' ) set @filtro = @select+  ' where  Mes = '''+@Mes+''' and Anual = '''+@Año+''' and Area = '''+@Area+''' ' " +
            " if ( @Año != '' and @Area != '' and @Sucursal != '' and @Mes = '')  	set @filtro = @select+  ' where  Anual = '''+@Año+''' and Area = '''+@Area+''' and Sucursal = '''+@Sucursal+''' ' " +
            " if ( @Año != '' and @Area != '' and @Sucursal = '' and @Mes = '')  set @filtro = @select+  ' where  Anual = '''+@Año+''' and Area = '''+@Area+''' ' " +
            " if ( @Año != '' and @Area = '' and @Sucursal = '' and @Mes = ''  )  	  set @filtro = @select+  ' where Anual = '''+@Año+'''   ' " +
            " if ( @Año != '' and @Area = '' and @Sucursal  != '' and @Mes != '' )  	set @filtro = @select+  ' where  Mes = '''+@Mes+''' and Anual = '''+@Año+'''  and Sucursal = '''+@Sucursal+''' ' " +
            " if ( @Año != '' and @Area = '' and @Sucursal = '' and @Mes != '')  	set @filtro = @select+  ' where  Mes = '''+@Mes+''' and Anual = '''+@Año+'''   ' " +
            " if ( @Año != '' and @Area = '' and @Sucursal  != '' and @Mes = '' )  	set @filtro = @select+  ' where  Anual = '''+@Año+''' and Sucursal = '''+@Sucursal+'''  '  " +
            " if ( @Año = '' and @Area != '' and @Sucursal != '' and @Mes != '' )   set @filtro = @select+  ' where  Mes = '''+@Mes+''' and  Area = '''+@Area+''' and Sucursal = '''+@Sucursal+'''  ' " +
            " if ( @Año = '' and @Area != '' and @Sucursal  = '' and @Mes != '' ) set @filtro = @select+  ' where  Mes = '''+@Mes+''' and  Area = '''+@Area+''' ' " +
            " if ( @Año = '' and @Area != '' and @Sucursal != '' and @Mes = '')  	set @filtro = @select+  ' where   Area = '''+@Area+''' and Sucursal = '''+@Sucursal+''' ' " +
            " if ( @Año = '' and @Area != '' and @Sucursal = '' and @Mes = '')  set @filtro = @select+  ' where   Area = '''+@Area+''' ' " +
            " if ( @Año = '' and @Area = '' and @Sucursal = '' and @Mes = ''  )  	  set @filtro = @select+  '' " +
            " if ( @Año = '' and @Area = '' and @Sucursal  != '' and @Mes != '' )  	set @filtro = @select+  ' where  Mes = '''+@Mes+''' and Sucursal = '''+@Sucursal+''' ' " +
            " if ( @Año = '' and @Area = '' and @Sucursal = '' and @Mes != '')  	set @filtro = @select+  ' where  Mes = '''+@Mes+'''    ' " +
            " if ( @Año = '' and @Area = '' and @Sucursal  != '' and @Mes = '' )  	set @filtro = @select+  ' where   Sucursal = '''+@Sucursal+''' ' " +
            " set @Consulta= @filtro+ 'order by SubArea'; " +
            " execute(@Consulta) ";

            return consulta;
        }
        public String Personal_CostoPlanilla_EvolucionAreaMensual_ddlAnio_SQL(String mes, String area, String subArea, String sucursal)
        {//GIAMPIERE 09/ 09 / 2019
            consulta =
            " declare @Mes varchar(5), @SubArea nvarchar(max),@Sucursal nvarchar(max),@Area nvarchar(max),@Consulta nvarchar(max),@filtro nvarchar(max),@select nvarchar(max); " +
            " set @Area='" + area + "';set @SubArea='" + subArea + "'; set @Sucursal= '" + sucursal + "'; set @Mes='" + mes + "';set @Consulta= ''; set @filtro= ''; set @select= ''; " +
            " set @select= N'select distinct Anual as Anio from Personal.DTM_Personal_Planilla ' " +
            " if ( @Mes != '' and @SubArea != '' and @Sucursal != '' and @Area != '' )   set @filtro = @select+  ' where  Area = '''+@Area+''' and Mes = '''+@Mes+''' and SubArea = '''+@SubArea+''' and Sucursal = '''+@Sucursal+''' ' " +
            " if ( @Mes != '' and @SubArea != '' and @Sucursal  = '' and @Area != '' ) set @filtro = @select+  ' where  Area = '''+@Area+''' and Mes = '''+@Mes+''' and SubArea = '''+@SubArea+''' ' " +
            " if ( @Mes != '' and @SubArea != '' and @Sucursal != '' and @Area = '')  	set @filtro = @select+  ' where  Mes = '''+@Mes+''' and SubArea = '''+@SubArea+''' and Sucursal = '''+@Sucursal+''' ' " +
            " if ( @Mes != '' and @SubArea != '' and @Sucursal = '' and @Area = '')  set @filtro = @select+  ' where  Mes = '''+@Mes+''' and SubArea = '''+@SubArea+''' ' " +
            " if ( @Mes != '' and @SubArea = '' and @Sucursal = '' and @Area = ''  )  	  set @filtro = @select+  ' where Mes = '''+@Mes+'''   ' " +
            " if ( @Mes != '' and @SubArea = '' and @Sucursal  != '' and @Area != '' )  	set @filtro = @select+  ' where  Area = '''+@Area+''' and Mes = '''+@Mes+'''  and Sucursal = '''+@Sucursal+''' ' " +
            " if ( @Mes != '' and @SubArea = '' and @Sucursal = '' and @Area != '')  	set @filtro = @select+  ' where  Area = '''+@Area+''' and Mes = '''+@Mes+'''   ' " +
            " if ( @Mes != '' and @SubArea = '' and @Sucursal  != '' and @Area = '' )  	set @filtro = @select+  ' where  Mes = '''+@Mes+''' and Sucursal = '''+@Sucursal+'''  '  " +
            " if ( @Mes = '' and @SubArea != '' and @Sucursal != '' and @Area != '' )   set @filtro = @select+  ' where  Area = '''+@Area+''' and  SubArea = '''+@SubArea+''' and Sucursal = '''+@Sucursal+'''  ' " +
            " if ( @Mes = '' and @SubArea != '' and @Sucursal  = '' and @Area != '' ) set @filtro = @select+  ' where  Area = '''+@Area+''' and  SubArea = '''+@SubArea+''' ' " +
            " if ( @Mes = '' and @SubArea != '' and @Sucursal != '' and @Area = '')  	set @filtro = @select+  ' where   SubArea = '''+@SubArea+''' and Sucursal = '''+@Sucursal+''' ' " +
            " if ( @Mes = '' and @SubArea != '' and @Sucursal = '' and @Area = '')  set @filtro = @select+  ' where   SubArea = '''+@SubArea+''' ' " +
            " if ( @Mes = '' and @SubArea = '' and @Sucursal = '' and @Area = ''  )  	  set @filtro = @select+  '' " +
            " if ( @Mes = '' and @SubArea = '' and @Sucursal  != '' and @Area != '' )  	set @filtro = @select+  ' where  Area = '''+@Area+''' and Sucursal = '''+@Sucursal+''' ' " +
            " if ( @Mes = '' and @SubArea = '' and @Sucursal = '' and @Area != '')  	set @filtro = @select+  ' where  Area = '''+@Area+'''    ' " +
            " if ( @Mes = '' and @SubArea = '' and @Sucursal  != '' and @Area = '' )  	set @filtro = @select+  ' where   Sucursal = '''+@Sucursal+''' ' " +
            " set @Consulta= @filtro+ 'order by Anual'; " +
            " execute(@Consulta) ";
            return consulta;
        }
        public String Personal_CostoPlanilla_EvolucionAreaMensual_ddlMes_SQL(String anio, String area, String subArea, String sucursal)
        {//GIAMPIERE 09/ 09 / 2019
            consulta =
                 " declare @Año varchar(5), @SubArea nvarchar(max),@Sucursal nvarchar(max),@Area nvarchar(max),@Consulta nvarchar(max),@filtro nvarchar(max),@select nvarchar(max); " +
            " set @Año='" + anio + "'; set @Area='" + area + "';set @SubArea='" + subArea + "'; set @Sucursal= '" + sucursal + "';set @Consulta= ''; set @filtro= ''; set @select= ''; " +
            " set @select= N'select distinct DATENAME(MONTH,Fecha) as Meses ,Mes from Personal.DTM_Personal_Planilla ' " +
            " if ( @Año != '' and @SubArea != '' and @Sucursal != '' and @Area != '' )   set @filtro = @select+  ' where  Area = '''+@Area+''' and Anual = '''+@Año+''' and SubArea = '''+@SubArea+''' and Sucursal = '''+@Sucursal+''' ' " +
            " if ( @Año != '' and @SubArea != '' and @Sucursal  = '' and @Area != '' ) set @filtro = @select+  ' where  Area = '''+@Area+''' and Anual = '''+@Año+''' and SubArea = '''+@SubArea+''' ' " +
            " if ( @Año != '' and @SubArea != '' and @Sucursal != '' and @Area = '')  	set @filtro = @select+  ' where  Anual = '''+@Año+''' and SubArea = '''+@SubArea+''' and Sucursal = '''+@Sucursal+''' ' " +
            " if ( @Año != '' and @SubArea != '' and @Sucursal = '' and @Area = '')  set @filtro = @select+  ' where  Anual = '''+@Año+''' and SubArea = '''+@SubArea+''' ' " +
            " if ( @Año != '' and @SubArea = '' and @Sucursal = '' and @Area = ''  )  	  set @filtro = @select+  ' where Anual = '''+@Año+'''   ' " +
            " if ( @Año != '' and @SubArea = '' and @Sucursal  != '' and @Area != '' )  	set @filtro = @select+  ' where  Area = '''+@Area+''' and Anual = '''+@Año+'''  and Sucursal = '''+@Sucursal+''' ' " +
            " if ( @Año != '' and @SubArea = '' and @Sucursal = '' and @Area != '')  	set @filtro = @select+  ' where  Area = '''+@Area+''' and Anual = '''+@Año+'''   ' " +
            " if ( @Año != '' and @SubArea = '' and @Sucursal  != '' and @Area = '' )  	set @filtro = @select+  ' where  Anual = '''+@Año+''' and Sucursal = '''+@Sucursal+'''  '  " +
            " if ( @Año = '' and @SubArea != '' and @Sucursal != '' and @Area != '' )   set @filtro = @select+  ' where  Area = '''+@Area+''' and  SubArea = '''+@SubArea+''' and Sucursal = '''+@Sucursal+'''  ' " +
            " if ( @Año = '' and @SubArea != '' and @Sucursal  = '' and @Area != '' ) set @filtro = @select+  ' where  Area = '''+@Area+''' and  SubArea = '''+@SubArea+''' ' " +
            " if ( @Año = '' and @SubArea != '' and @Sucursal != '' and @Area = '')  	set @filtro = @select+  ' where   SubArea = '''+@SubArea+''' and Sucursal = '''+@Sucursal+''' ' " +
            " if ( @Año = '' and @SubArea != '' and @Sucursal = '' and @Area = '')  set @filtro = @select+  ' where   SubArea = '''+@SubArea+''' ' " +
            " if ( @Año = '' and @SubArea = '' and @Sucursal = '' and @Area = ''  )  	  set @filtro = @select+  '' " +
            " if ( @Año = '' and @SubArea = '' and @Sucursal  != '' and @Area != '' )  	set @filtro = @select+  ' where  Area = '''+@Area+''' and Sucursal = '''+@Sucursal+''' ' " +
            " if ( @Año = '' and @SubArea = '' and @Sucursal = '' and @Area != '')  	set @filtro = @select+  ' where  Area = '''+@Area+'''    ' " +
            " if ( @Año = '' and @SubArea = '' and @Sucursal  != '' and @Area = '' )  	set @filtro = @select+  ' where   Sucursal = '''+@Sucursal+''' ' " +
            " set @Consulta= @filtro+ 'order by Mes'; " +
            " execute(@Consulta) ";


            return consulta;
        }

        #endregion
        #region HorasExtras
        //GIAMPIERE 12/ 08 / 2019
        //#region HorasExtras
        //CANTIDAD GENERAL
        public string G_Personal_HoraExtra_cantidadGeneral_SQL =

        " declare @Mes date, @primerDiaMes date, @ultimaDiaMes date   " +
        " IF(getdate()= (select EOMONTH(getdate()) as UltDiaMesActual)) " +
        " begin set @Mes=	(SELECT CONVERT (date, GETDATE()))	 end ELSE begin" +
        " set @Mes= (select EOMONTH(getdate(),-1) as UltDiaMesAnterior)  end" +
        " set @primerDiaMes= dateadd(d,1, EOMONTH(@Mes,-1))" +
        " set @ultimaDiaMes = EOMONTH(@Mes)" +
        " select sum(CantidaHorasExtra) as Cantidad  from [Personal].[DTM_Personal_Horas_Extra] " +
        " where Fecha between @primerDiaMes and @ultimaDiaMes  and  KeyPersonalHoraExtra in(select max(KeyPersonalHoraExtra)" +
        " from [Personal].[DTM_Personal_Horas_Extra] Where Fecha between @primerDiaMes and @ultimaDiaMes" +
        " group by Mes,Sucursal ,Departamento,Area,SubArea)";

        //CANTIDAD POR DEPARTAMENTO
        public String G_Personal_HoraExtras_CantidadArea_SQL(String area, String subArea, String sucursal)
        {        //GIAMPIERE 12/ 08 / 2019
            consulta =
            " declare  @Area nvarchar(max),@mesActual varchar(max),@mes varchar(max),@anio varchar(max), @SubArea nvarchar(max), @Sucursal nvarchar(max),@consulta nvarchar(max), @filtro nvarchar(max) " +
             " set @Area='" + area + "';set @SubArea='" + subArea + "'; set @Sucursal= '" + sucursal + "';set @consulta=''; set @filtro= ''; " +
             " IF(getdate() = (select EOMONTH(getdate()))) begin set @mesActual = (SELECT CONVERT(date, GETDATE())) end ELSe begin set @mesActual = (select EOMONTH(getdate(), -1) ) end set @mes = MONTH(@mesActual); set @anio = YEAR(@mesActual); " +
             " if ( @Area != '' and @SubArea = '' and @Sucursal = '')  set @filtro= ' Where  Mes='''+@mes+''' and Anual='''+@anio+''' and Area='''+@Area+''' ' " +
             " if ( @Area != '' and @SubArea = '' and @Sucursal != '') set @filtro= '  Where  Mes='''+@mes+''' and Anual='''+@anio+''' and Area='''+@Area+'''  and Sucursal='''+@Sucursal+''' ' " +
             " if ( @Area != '' and @SubArea != '' and @Sucursal = '') set @filtro= '  Where  Mes='''+@mes+''' and Anual='''+@anio+'''  and Area='''+@Area+'''  and SubArea='''+@SubArea+''' ' " +
             " if ( @Area != '' and @SubArea != '' and @Sucursal != '') set @filtro= '  Where  Mes='''+@mes+''' and Anual='''+@anio+''' and Area='''+@Area+'''  and SubArea='''+@SubArea+''' and Sucursal='''+@Sucursal+''' ' " +
             " if ( @Area = '' and @SubArea = '' and @Sucursal = '') set @filtro= '  Where  Mes='''+@mes+''' and Anual='''+@anio+'''   ' " +
             " if ( @Area = '' and @SubArea = '' and @Sucursal != '') set @filtro= '  Where  Mes='''+@mes+''' and Anual='''+@anio+''' and Sucursal='''+@Sucursal+''' ' " +
             " if ( @Area = '' and @SubArea != '' and @Sucursal = '') set @filtro= '  Where  Mes='''+@mes+''' and Anual='''+@anio+''' and SubArea='''+@SubArea+''' ' " +
             " if ( @Area = '' and @SubArea != '' and @Sucursal != '') set @filtro= '  Where  Mes='''+@mes+''' and Anual='''+@anio+''' and SubArea='''+@SubArea+''' and Sucursal='''+@Sucursal+''' ' " +
             " set @consulta=' select Departamento, sum(CantidaHorasExtra) as Cantidad  from [Personal].[DTM_Personal_Horas_Extra] '+@filtro+'  and KeyPersonalHoraExtra in(select max(KeyPersonalHoraExtra) from [Personal].[DTM_Personal_Horas_Extra] '+@filtro+'  group by Sucursal ,Departamento,Area,SubArea) group by Departamento' " +
             " execute(@consulta)";
            return consulta;
        }

        public String Personal_HoraExtras_CantidadArea_ddlSucursal_SQL(String area, String subArea)
        {        //GIAMPIERE 12/ 08 / 2019
            consulta =
        " declare  @Area nvarchar(max), @SubArea nvarchar(max) " +
        " set @Area='" + area + "';set @SubArea='" + subArea + "'; " +
        " if ( @Area = '' and @SubArea = '' ) Select distinct Sucursal  from [Personal].[DTM_Personal_Horas_Extra]" +
        " if ( @Area != '' and @SubArea = '' ) Select distinct Sucursal  from [Personal].[DTM_Personal_Horas_Extra] Where   Area=@Area " +
        " if ( @Area = '' and @SubArea != '' ) Select distinct Sucursal  from [Personal].[DTM_Personal_Horas_Extra] Where SubArea=@SubArea" +
        " if ( @Area != '' and @SubArea != '') Select distinct Sucursal  from [Personal].[DTM_Personal_Horas_Extra] Where Area=@Area and SubArea=@SubArea";

            return consulta;
        }

        public String Personal_HoraExtras_CantidadArea_ddlArea_SQL(String sucursal, String subArea)
        {        //GIAMPIERE 12/ 08 / 2019
            consulta =
         " declare  @Sucursal nvarchar(max), @SubArea nvarchar(max)" +
         " set @SubArea='" + subArea + "'; set @Sucursal= '" + sucursal + "';" +
         " if (@SubArea = '' and @Sucursal = '')  Select distinct Area  from [Personal].[DTM_Personal_Horas_Extra]" +
         " if (@SubArea = '' and @Sucursal != '')  Select distinct Area  from [Personal].[DTM_Personal_Horas_Extra] where Sucursal=@Sucursal" +
         " if (@SubArea != '' and @Sucursal = '') Select distinct Area  from [Personal].[DTM_Personal_Horas_Extra] where SubArea=@SubArea" +
         " if (@SubArea != '' and @Sucursal != '') Select distinct Area  from [Personal].[DTM_Personal_Horas_Extra] where Sucursal=@Sucursal and SubArea=@SubArea";

            return consulta;
        }
        public String Personal_HoraExtras_CantidadArea_ddlSubArea_SQL(String sucursal, String area)
        {        //GIAMPIERE 12/ 08 / 2019
            consulta =
         "  declare  @Sucursal nvarchar(max), @Area  nvarchar(max)" +
          " set @Area='" + area + "'; set @Sucursal= '" + sucursal + "';" +
        "  if (@Area  = '' and @Sucursal = '')  Select distinct SubArea  from [Personal].[DTM_Personal_Horas_Extra]" +
        "  if (@Area  = '' and @Sucursal != '')  Select distinct SubArea  from [Personal].[DTM_Personal_Horas_Extra] where Sucursal=@Sucursal" +
        "  if (@Area  != '' and @Sucursal = '') Select distinct SubArea  from [Personal].[DTM_Personal_Horas_Extra] where Area=@Area " +
        "  if (@Area  != '' and @Sucursal != '') Select distinct SubArea  from [Personal].[DTM_Personal_Horas_Extra] where Sucursal=@Sucursal and Area=@Area ";
            return consulta;
        }
        //EVOLUCIÓN GENERAL MENSUAL
        public String G_Personal_HoraExtras_EvolucionGeneralMensual_SQL(String mes, String departamento, String area, String subArea, String sucursal)
        {        //GIAMPIERE 12/ 08 / 2019
            consulta =
                " declare @Departamento varchar(20), @Area nvarchar(max), @Mes nvarchar(3),@SubArea nvarchar(max),@Sucursal nvarchar(max), @filtro  NVARCHAR(MAX), @cols NVARCHAR(MAX), @null NVARCHAR(MAX), @consulta NVARCHAR(MAX); " +
          " set @Departamento= '" + departamento + "'; set @Area='" + area + "';set @SubArea='" + subArea + "'; set @Sucursal= '" + sucursal + "'; set @Mes='" + mes + "'; set @filtro =''; set @consulta =''; " +
          " select @cols = STUFF((SELECT Distinct ',' + QUOTENAME(Anual)  from [Personal].[DTM_Personal_Horas_Extra] group by ',' + QUOTENAME(Anual)Order by ',' + QUOTENAME(Anual) FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)')  ,1,1,'') " +
           " select @null = STUFF((SELECT Distinct ', isnull(' + QUOTENAME(Anual)+',0) as'  + QUOTENAME(Anual) from [Personal].[DTM_Personal_Horas_Extra]  group by ', isnull(' + QUOTENAME(Anual)+',0) as'  + QUOTENAME(Anual) Order by ', isnull(' + QUOTENAME(Anual)+',0) as'  + QUOTENAME(Anual)   FOR XML PATH(''), TYPE  ).value('.', 'NVARCHAR(MAX)')   ,1,1,'') " +
           " if  (@Mes = '' and @Departamento = ''  and @Area = '' and @SubArea = '' and @Sucursal = ''  )  	  set @consulta= 'SELECT Meses,' + @null + '  from  ( select Mes, DATENAME(MONTH,Fecha) as Meses,Anual,CantidaHorasExtra from [Personal].[DTM_Personal_Horas_Extra] where  KeyPersonalHoraExtra in(select max(KeyPersonalHoraExtra) from [Personal].[DTM_Personal_Horas_Extra]  group by  Mes ,Anual,Sucursal ,Departamento,Area,SubArea)   ) x  pivot ( sum(CantidaHorasExtra) for Anual in (' + @cols + ') ) p  order by Mes' " +
           " else  " +
           " begin " +
           " if  (@Mes != '' and @Departamento != '' and @Area != '' and @SubArea != '' and @Sucursal != '' )  	 set @filtro = ' Sucursal = '''+@Sucursal+''' and Departamento = '''+@Departamento+''' and Area = '''+@Area+''' and SubArea = '''+@SubArea+'''  and Mes = '''+@Mes+'''  ' " +
           " if  (@Mes != '' and @Departamento != ''  and @Area = '' and @SubArea = '' and @Sucursal = ''  )  	 set @filtro = ' Departamento = '''+@Departamento+'''  and Mes = '''+@Mes+''' ' " +
           " if  (@Mes != '' and @Departamento != ''  and @Area != '' and @SubArea  = '' and @Sucursal != '' )   set @filtro = ' Sucursal = '''+@Sucursal+''' and Departamento = '''+@Departamento+''' and Area='''+@Area+'''  and Mes = '''+@Mes+''' ' " +
           " if  (@Mes != '' and @Departamento != '' and @Area != '' and @SubArea != '' and @Sucursal = '')  	 set @filtro = ' Departamento = '''+@Departamento+''' and Area = '''+@Area+''' and SubArea = '''+@SubArea+'''  and Mes = '''+@Mes+''' ' " +
           " if  (@Mes != '' and @Departamento != ''  and @Area = '' and @SubArea  != '' and @Sucursal != '' )   set @filtro = ' Sucursal = '''+@Sucursal+''' and Departamento = '''+@Departamento+''' and SubArea = '''+@SubArea+'''  and Mes = '''+@Mes+''' ' " +
           " if  (@Mes != '' and @Departamento != '' and @Area = '' and @SubArea = '' and @Sucursal != '')  	 set @filtro = ' Sucursal = '''+@Sucursal+''' and Departamento = '''+@Departamento+''' and Mes = '''+@Mes+'''   ' " +
           " if  (@Mes != '' and @Departamento != ''  and @Area = '' and @SubArea  != '' and @Sucursal = '' )  	  set @filtro = ' Departamento = '''+@Departamento+''' and SubArea = '''+@SubArea+'''  and Mes = '''+@Mes+'''  ' " +
           " if  (@Mes != '' and @Departamento != '' and @Area != '' and @SubArea = '' and @Sucursal = '')  	 set @filtro = '  Departamento = '''+@Departamento+''' and Area = '''+@Area+'''  and Mes = '''+@Mes+''' ' " +
           " if  (@Mes != '' and @Departamento = '' and @Area != '' and @SubArea != '' and @Sucursal != '' )  	 set @filtro = '  Sucursal = '''+@Sucursal+'''  and Area = '''+@Area+''' and SubArea = '''+@SubArea+'''  and Mes = '''+@Mes+''' ' " +
           " if  (@Mes != '' and @Departamento = ''  and @Area = '' and @SubArea = '' and @Sucursal = ''  )  	  set @filtro = ' Mes = '''+@Mes+''' ' " +
           " if  (@Mes != '' and @Departamento = ''  and @Area != '' and @SubArea  = '' and @Sucursal != '' )  	 set @filtro = ' Sucursal = '''+@Sucursal+''' and Area='''+@Area+''' and Mes = '''+@Mes+'''  ' " +
           " if  (@Mes != '' and @Departamento = '' and @Area != '' and @SubArea != '' and @Sucursal = '')  	 set @filtro = ' Area = '''+@Area+''' and SubArea = '''+@SubArea+'''  and Mes = '''+@Mes+'''   ' " +
           " if  (@Mes != '' and @Departamento = ''  and @Area = '' and @SubArea  != '' and @Sucursal != '' )  	  set @filtro = '  Sucursal = '''+@Sucursal+'''  and SubArea = '''+@SubArea+'''  and Mes = '''+@Mes+''' ' " +
           " if  (@Mes != '' and @Departamento = '' and @Area = '' and @SubArea = '' and @Sucursal != '')  	 set @filtro = ' Sucursal = '''+@Sucursal+'''  and Mes = '''+@Mes+'''  ' " +
           " if  (@Mes != '' and @Departamento = ''  and @Area = '' and @SubArea  != '' and @Sucursal = '' )  	  set @filtro = '  SubArea = '''+@SubArea+'''  and Mes = '''+@Mes+''' '  " +
           " if  (@Mes != '' and @Departamento = '' and @Area != '' and @SubArea = '' and @Sucursal = '')  	 set @filtro = ' Area = '''+@Area+'''  and Mes = '''+@Mes+''' ' " +
           " if  (@Mes = '' and @Departamento != '' and @Area != '' and @SubArea != '' and @Sucursal != '' )  	 set @filtro = '  Sucursal = '''+@Sucursal+''' and Departamento = '''+@Departamento+''' and Area = '''+@Area+''' and SubArea = '''+@SubArea+''' ' " +
           " if  (@Mes = '' and @Departamento != ''  and @Area = '' and @SubArea = '' and @Sucursal = ''  )  	  set @filtro = '  Departamento = '''+@Departamento+'''  ' " +
           " if  (@Mes = '' and @Departamento != ''  and @Area != '' and @SubArea  = '' and @Sucursal != '' )  	 set @filtro = '  Sucursal = '''+@Sucursal+''' and Departamento = '''+@Departamento+''' and Area='''+@Area+'''   ' " +
           " if  (@Mes = '' and @Departamento != '' and @Area != '' and @SubArea != '' and @Sucursal = '')  	 set @filtro = '  Departamento = '''+@Departamento+''' and Area = '''+@Area+''' and SubArea = '''+@SubArea+'''   '  " +
           " if  (@Mes = '' and @Departamento != ''  and @Area = '' and @SubArea  != '' and @Sucursal != '' )  	  set @filtro = '  Sucursal = '''+@Sucursal+''' and Departamento = '''+@Departamento+''' and SubArea = '''+@SubArea+'''  ' " +
           " if  (@Mes = '' and @Departamento != '' and @Area = '' and @SubArea = '' and @Sucursal != '')  	 set @filtro = ' Sucursal = '''+@Sucursal+''' and Departamento = '''+@Departamento+''''  " +
           " if  (@Mes = '' and @Departamento != ''  and @Area = '' and @SubArea  != '' and @Sucursal = '' )  	  set @filtro = '  Departamento = '''+@Departamento+''' and SubArea = '''+@SubArea+'''' " +
           " if  (@Mes = '' and @Departamento != '' and @Area != '' and @SubArea = '' and @Sucursal = '')  	 set @filtro = '  Departamento = '''+@Departamento+''' and Area = '''+@Area+''' ' " +
           " if  (@Mes = '' and @Departamento = '' and @Area != '' and @SubArea != '' and @Sucursal != '' )  	 set @filtro = '  Sucursal = '''+@Sucursal+'''  and Area = '''+@Area+''' and SubArea = '''+@SubArea+'''  ' " +
           " if  (@Mes = '' and @Departamento = ''  and @Area != '' and @SubArea  = '' and @Sucursal != '' )  	 set @filtro = '  Sucursal = '''+@Sucursal+''' and Area='''+@Area+'''  ' " +
           " if  (@Mes = '' and @Departamento = '' and @Area != '' and @SubArea != '' and @Sucursal = '')  	 set @filtro = '  Area = '''+@Area+''' and SubArea = '''+@SubArea+''' '  " +
           " if  (@Mes = '' and @Departamento = ''  and @Area = '' and @SubArea  != '' and @Sucursal != '' )  	  set @filtro = '  Sucursal = '''+@Sucursal+'''  and SubArea = '''+@SubArea+'''  ' " +
           " if  (@Mes = '' and @Departamento = '' and @Area = '' and @SubArea = '' and @Sucursal != '')  	 set @filtro = ' Sucursal = '''+@Sucursal+''' '" +
           " if  (@Mes = '' and @Departamento = ''  and @Area = '' and @SubArea  != '' and @Sucursal = '' )  	  set @filtro = '  SubArea = '''+@SubArea+''' ' " +
           " if  (@Mes = '' and @Departamento = '' and @Area != '' and @SubArea = '' and @Sucursal = '')  	 set @filtro = ' Area = '''+@Area+'''  ' " +
           " set @consulta= 'SELECT Meses,' + @null + '  from  ( select Mes, DATENAME(MONTH,Fecha) as Meses,Anual,CantidaHorasExtra from [Personal].[DTM_Personal_Horas_Extra] where '+@filtro+' and KeyPersonalHoraExtra in(select max(KeyPersonalHoraExtra) from [Personal].[DTM_Personal_Horas_Extra] where '+@filtro+'  group by  Mes ,Anual,Sucursal ,Departamento,Area,SubArea)   ) x  pivot ( sum(CantidaHorasExtra) for Anual in (' + @cols + ') ) p  order by Mes' " +
           " end " +
           " execute(@consulta) ";

            return consulta;
        }
        public String Personal_HoraExtras_EvolucionGeneralMensual_ddlSucursal_SQL(String mes, String departamento, String area, String subArea)
        {        //GIAMPIERE 12/ 08 / 2019
            consulta =
                " declare @Area nvarchar(max),@SubArea nvarchar(max),@Departamento nvarchar(max),@Mes nvarchar(max),@Consulta nvarchar(max),@Filtro nvarchar(max),@select nvarchar(max);" +
          " set @Departamento= '" + departamento + "'; set @Area='" + area + "';set @SubArea='" + subArea + "'; set @Mes='" + mes + "';set @Consulta= ''; set @Filtro= ''; set @select= ''; " +
          " set @Consulta= N'select distinct Sucursal from[Personal].[DTM_Personal_Horas_Extra]' " +
          " if (@Mes = ''  and @Area = '' and @SubArea = '' and @Departamento = ''  )  set @Filtro=  @Consulta " +
          " if (@Mes != ''  and @Area != '' and @SubArea != '' and @Departamento != '' )   set @Filtro =  @Consulta+ ' where Departamento = '''+@Departamento+''' and  Area = '''+@Area+''' and SubArea = '''+@SubArea+''' and Mes='''+@Mes+'''  '" +
          " if (@Mes != ''  and @Area != '' and @SubArea  = '' and @Departamento != '' ) set @Filtro =  @Consulta+' where Departamento = '''+@Departamento+''' and  Area = '''+@Area+'''  and Mes='''+@Mes+'''  ' " +
          " if (@Mes != ''  and @Area != '' and @SubArea != '' and @Departamento = '')  	set @Filtro =  @Consulta+' where Area = '''+@Area+''' and SubArea = '''+@SubArea+''' and Mes='''+@Mes+'''  ' " +
          " if (@Mes != ''  and @Area != '' and @SubArea = '' and @Departamento = '')  set @Filtro =  @Consulta+' where Area = '''+@Area+'''  and Mes='''+@Mes+''' ' " +
          " if (@Mes != ''  and @Area = '' and @SubArea = '' and @Departamento = ''  )  	  set @Filtro =  @Consulta+' where Mes='''+@Mes+'''  ' " +
          " if (@Mes != ''  and @Area = '' and @SubArea  != '' and @Departamento != '' )  	set @Filtro =  @Consulta+' where Departamento = '''+@Departamento+''' and SubArea = '''+@SubArea+''' and Mes='''+@Mes+''' ' " +
          " if (@Mes != ''  and @Area = '' and @SubArea = '' and @Departamento != '')  	set @Filtro =  @Consulta+' where Departamento = '''+@Departamento+''' and Mes='''+@Mes+'''  ' " +
          " if (@Mes != ''  and @Area = '' and @SubArea  != '' and @Departamento = '' )  	set @Filtro =  @Consulta+' where SubArea = '''+@SubArea+''' and Mes='''+@Mes+'''  ' " +
          " if (@Mes = ''  and @Area != '' and @SubArea != '' and @Departamento != '' )   set @Filtro =  @Consulta+' where Departamento = '''+@Departamento+''' and  Area = '''+@Area+''' and SubArea = '''+@SubArea+''' ' " +
          " if (@Mes = ''  and @Area != '' and @SubArea  = '' and @Departamento != '' ) set @Filtro =  @Consulta+' where Departamento = '''+@Departamento+''' and  Area = '''+@Area+'''   ' " +
          " if (@Mes = ''  and @Area != '' and @SubArea != '' and @Departamento = '')  	set @Filtro =  @Consulta+' where Area = '''+@Area+''' and SubArea = '''+@SubArea+'''  ' " +
          " if (@Mes = ''  and @Area != '' and @SubArea = '' and @Departamento = '')  set @Filtro =  @Consulta+' where Area = '''+@Area+'''  ' " +
          " if (@Mes = ''  and @Area = '' and @SubArea  != '' and @Departamento != '' )  	set @Filtro =  @Consulta+' where Departamento = '''+@Departamento+''' and SubArea = '''+@SubArea+'''  ' " +
          " if (@Mes = ''  and @Area = '' and @SubArea = '' and @Departamento != '')  	set @Filtro =  @Consulta+' where Departamento = '''+@Departamento+'''   ' " +
          " if (@Mes = ''  and @Area = '' and @SubArea  != '' and @Departamento = '' )  	set @Filtro =  @Consulta+' where SubArea = '''+@SubArea+'''  ' " +
          " set @Consulta=  @Filtro +' order by Sucursal' " +
          " execute(@Consulta)";
            return consulta;
        }
        public String Personal_HoraExtras_EvolucionGeneralMensual_ddlDepartamento_SQL(String mes, String area, String subArea, String sucursal)
        {        //GIAMPIERE 12/ 08 / 2019
            consulta =
          " declare @Area nvarchar(max),@SubArea nvarchar(max),@Sucursal nvarchar(max),@Mes nvarchar(max),@Consulta nvarchar(max),@Filtro nvarchar(max),@select nvarchar(max);" +
          " set @Area='" + area + "';set @SubArea='" + subArea + "'; set @Sucursal= '" + sucursal + "'; set @Mes='" + mes + "'; set @Consulta= ''; set @Filtro= ''; set @select= ''; " +
          " set @Consulta= N'select distinct Departamento from[Personal].[DTM_Personal_Horas_Extra]'" +
          " if (@Mes = ''  and @Area = '' and @SubArea = '' and @Sucursal = ''  )  set @Filtro=  @Consulta " +
          " if (@Mes != ''  and @Area != '' and @SubArea != '' and @Sucursal != '' )   set @Filtro =  @Consulta+ ' where Sucursal = '''+@Sucursal+''' and  Area = '''+@Area+''' and SubArea = '''+@SubArea+''' and Mes='''+@Mes+'''  ' " +
          " if (@Mes != ''  and @Area != '' and @SubArea  = '' and @Sucursal != '' ) set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and  Area = '''+@Area+'''  and Mes='''+@Mes+'''  '" +
          " if (@Mes != ''  and @Area != '' and @SubArea != '' and @Sucursal = '')  	set @Filtro =  @Consulta+' where Area = '''+@Area+''' and SubArea = '''+@SubArea+''' and Mes='''+@Mes+'''  ' " +
          " if (@Mes != ''  and @Area != '' and @SubArea = '' and @Sucursal = '')  set @Filtro =  @Consulta+' where Area = '''+@Area+'''  and Mes='''+@Mes+''' '  " +
          " if (@Mes != ''  and @Area = '' and @SubArea = '' and @Sucursal = ''  )  	  set @Filtro =  @Consulta+' where Mes='''+@Mes+'''  ' " +
          " if (@Mes != ''  and @Area = '' and @SubArea  != '' and @Sucursal != '' )  	set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and SubArea = '''+@SubArea+''' and Mes='''+@Mes+''' '  " +
          " if (@Mes != ''  and @Area = '' and @SubArea = '' and @Sucursal != '')  	set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and Mes='''+@Mes+'''  ' " +
          " if (@Mes != ''  and @Area = '' and @SubArea  != '' and @Sucursal = '' )  	set @Filtro =  @Consulta+' where SubArea = '''+@SubArea+''' and Mes='''+@Mes+'''  '" +
          " if (@Mes = ''  and @Area != '' and @SubArea != '' and @Sucursal != '' )   set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and  Area = '''+@Area+''' and SubArea = '''+@SubArea+''' ' " +
          " if (@Mes = ''  and @Area != '' and @SubArea  = '' and @Sucursal != '' ) set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and  Area = '''+@Area+'''   '" +
          " if (@Mes = ''  and @Area != '' and @SubArea != '' and @Sucursal = '')  	set @Filtro =  @Consulta+' where Area = '''+@Area+''' and SubArea = '''+@SubArea+'''  '" +
          " if (@Mes = ''  and @Area != '' and @SubArea = '' and @Sucursal = '')  set @Filtro =  @Consulta+' where Area = '''+@Area+'''  '" +
          " if (@Mes = ''  and @Area = '' and @SubArea  != '' and @Sucursal != '' )  	set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and SubArea = '''+@SubArea+'''  ' " +
          " if (@Mes = ''  and @Area = '' and @SubArea = '' and @Sucursal != '')  	set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+'''   ' " +
          " if (@Mes = ''  and @Area = '' and @SubArea  != '' and @Sucursal = '' )  	set @Filtro =  @Consulta+' where SubArea = '''+@SubArea+'''  '" +
          " set @Consulta=  @Filtro +' order by Departamento'" +
          " execute(@Consulta);";
            return consulta;
        }
        public String Personal_HoraExtras_EvolucionGeneralMensual_ddlArea_SQL(String mes, String departamento, String subArea, String sucursal)
        {        //GIAMPIERE 12/ 08 / 2019
            consulta =
          " declare @SubArea nvarchar(max),@Departamento nvarchar(max),@Sucursal nvarchar(max),@Mes nvarchar(max),@Consulta nvarchar(max),@Filtro nvarchar(max),@select nvarchar(max);" +
          " set @Departamento= '" + departamento + "';set @SubArea='" + subArea + "'; set @Sucursal= '" + sucursal + "'; set @Mes='" + mes + "'; set @Consulta= ''; set @Filtro= ''; set @select= ''; " +
          " set @Consulta= N'select distinct Area from[Personal].[DTM_Personal_Horas_Extra]'" +
          " if (@Mes = ''  and @SubArea = '' and @Departamento = '' and @Sucursal = ''  )  set @Filtro=  @Consulta " +
          " if (@Mes != ''  and @SubArea != '' and @Departamento != '' and @Sucursal != '' )   set @Filtro =  @Consulta+ ' where Sucursal = '''+@Sucursal+''' and  SubArea = '''+@SubArea+''' and Departamento = '''+@Departamento+''' and Mes='''+@Mes+'''  ' " +
          " if (@Mes != ''  and @SubArea != '' and @Departamento  = '' and @Sucursal != '' ) set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and  SubArea = '''+@SubArea+'''  and Mes='''+@Mes+'''  '" +
          " if (@Mes != ''  and @SubArea != '' and @Departamento != '' and @Sucursal = '')  	set @Filtro =  @Consulta+' where SubArea = '''+@SubArea+''' and Departamento = '''+@Departamento+''' and Mes='''+@Mes+'''  ' " +
          " if (@Mes != ''  and @SubArea != '' and @Departamento = '' and @Sucursal = '')  set @Filtro =  @Consulta+' where SubArea = '''+@SubArea+'''  and Mes='''+@Mes+''' '  " +
          " if (@Mes != ''  and @SubArea = '' and @Departamento = '' and @Sucursal = ''  )  	  set @Filtro =  @Consulta+' where Mes='''+@Mes+'''  ' " +
          " if (@Mes != ''  and @SubArea = '' and @Departamento  != '' and @Sucursal != '' )  	set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and Departamento = '''+@Departamento+''' and Mes='''+@Mes+''' '  " +
          " if (@Mes != ''  and @SubArea = '' and @Departamento = '' and @Sucursal != '')  	set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and Mes='''+@Mes+'''  ' " +
          " if (@Mes != ''  and @SubArea = '' and @Departamento  != '' and @Sucursal = '' )  	set @Filtro =  @Consulta+' where Departamento = '''+@Departamento+''' and Mes='''+@Mes+'''  '" +
          " if (@Mes = ''  and @SubArea != '' and @Departamento != '' and @Sucursal != '' )   set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and  SubArea = '''+@SubArea+''' and Departamento = '''+@Departamento+''' ' " +
          " if (@Mes = ''  and @SubArea != '' and @Departamento  = '' and @Sucursal != '' ) set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and  SubArea = '''+@SubArea+'''   '" +
          " if (@Mes = ''  and @SubArea != '' and @Departamento != '' and @Sucursal = '')  	set @Filtro =  @Consulta+' where SubArea = '''+@SubArea+''' and Departamento = '''+@Departamento+'''  '" +
          " if (@Mes = ''  and @SubArea != '' and @Departamento = '' and @Sucursal = '')  set @Filtro =  @Consulta+' where SubArea = '''+@SubArea+'''  '" +
          " if (@Mes = ''  and @SubArea = '' and @Departamento  != '' and @Sucursal != '' )  	set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and Departamento = '''+@Departamento+'''  ' " +
          " if (@Mes = ''  and @SubArea = '' and @Departamento = '' and @Sucursal != '')  	set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+'''   ' " +
          " if (@Mes = ''  and @SubArea = '' and @Departamento  != '' and @Sucursal = '' )  	set @Filtro =  @Consulta+' where Departamento = '''+@Departamento+'''  '" +
          " set @Consulta=  @Filtro +' order by Area'" +
          " execute(@Consulta)";

            return consulta;
        }
        public String Personal_HoraExtras_EvolucionGeneralMensual_ddlSubArea_SQL(String mes, String departamento, String area, String sucursal)
        {        //GIAMPIERE 12/ 08 / 2019
            consulta =
          " declare @Area nvarchar(max),@Departamento nvarchar(max),@Sucursal nvarchar(max),@Mes nvarchar(max),@Consulta nvarchar(max),@Filtro nvarchar(max),@select nvarchar(max);" +
          " set @Departamento= '" + departamento + "'; set @Area='" + area + "';set @Sucursal= '" + sucursal + "'; set @Mes='" + mes + "'; set @Consulta= ''; set @Filtro= ''; set @select= ''; " +
          " set @Consulta= N'select distinct SubArea from[Personal].[DTM_Personal_Horas_Extra]'" +
          " if (@Mes = ''  and @Area = '' and @Departamento = '' and @Sucursal = ''  )  set @Filtro=  @Consulta " +
          " if (@Mes != ''  and @Area != '' and @Departamento != '' and @Sucursal != '' )   set @Filtro =  @Consulta+ ' where Sucursal = '''+@Sucursal+''' and  Area = '''+@Area+''' and Departamento = '''+@Departamento+''' and Mes='''+@Mes+'''  ' " +
          " if (@Mes != ''  and @Area != '' and @Departamento  = '' and @Sucursal != '' ) set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and  Area = '''+@Area+'''  and Mes='''+@Mes+'''  '" +
          " if (@Mes != ''  and @Area != '' and @Departamento != '' and @Sucursal = '')  	set @Filtro =  @Consulta+' where Area = '''+@Area+''' and Departamento = '''+@Departamento+''' and Mes='''+@Mes+'''  ' " +
          " if (@Mes != ''  and @Area != '' and @Departamento = '' and @Sucursal = '')  set @Filtro =  @Consulta+' where Area = '''+@Area+'''  and Mes='''+@Mes+''' '  " +
          " if (@Mes != ''  and @Area = '' and @Departamento = '' and @Sucursal = ''  )  	  set @Filtro =  @Consulta+' where Mes='''+@Mes+'''  ' " +
          " if (@Mes != ''  and @Area = '' and @Departamento  != '' and @Sucursal != '' )  	set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and Departamento = '''+@Departamento+''' and Mes='''+@Mes+''' '  " +
          " if (@Mes != ''  and @Area = '' and @Departamento = '' and @Sucursal != '')  	set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and Mes='''+@Mes+'''  ' " +
          " if (@Mes != ''  and @Area = '' and @Departamento  != '' and @Sucursal = '' )  	set @Filtro =  @Consulta+' where Departamento = '''+@Departamento+''' and Mes='''+@Mes+'''  '" +
          " if (@Mes = ''  and @Area != '' and @Departamento != '' and @Sucursal != '' )   set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and  Area = '''+@Area+''' and Departamento = '''+@Departamento+''' ' " +
          " if (@Mes = ''  and @Area != '' and @Departamento  = '' and @Sucursal != '' ) set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and  Area = '''+@Area+'''   '" +
          " if (@Mes = ''  and @Area != '' and @Departamento != '' and @Sucursal = '')  	set @Filtro =  @Consulta+' where Area = '''+@Area+''' and Departamento = '''+@Departamento+'''  '" +
          " if (@Mes = ''  and @Area != '' and @Departamento = '' and @Sucursal = '')  set @Filtro =  @Consulta+' where Area = '''+@Area+'''  '" +
          " if (@Mes = ''  and @Area = '' and @Departamento  != '' and @Sucursal != '' )  	set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and Departamento = '''+@Departamento+'''  ' " +
          " if (@Mes = ''  and @Area = '' and @Departamento = '' and @Sucursal != '')  	set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+'''   ' " +
          " if (@Mes = ''  and @Area = '' and @Departamento  != '' and @Sucursal = '' )  	set @Filtro =  @Consulta+' where Departamento = '''+@Departamento+'''  '" +
          " set @Consulta=  @Filtro +' order by SubArea'" +
          " execute(@Consulta)";
            return consulta;

        }
        public String Personal_HoraExtras_EvolucionGeneralMensual_ddlMes_SQL(String departamento, String area, String subArea, String sucursal)
        {   //GIAMPIERE 12/ 08 / 2019
            consulta =
          " declare @SubArea nvarchar(max),@Departamento nvarchar(max),@Sucursal nvarchar(max),@Area nvarchar(max),@Consulta nvarchar(max),@Filtro nvarchar(max),@select nvarchar(max);" +
         " set @Departamento= '" + departamento + "'; set @Area='" + area + "';set @SubArea='" + subArea + "'; set @Sucursal= '" + sucursal + "';set @Consulta= ''; set @Filtro= ''; set @select= ''; " +
          " set @Consulta= N'select distinct DATENAME(MONTH,Fecha) as Meses ,Mes from[Personal].[DTM_Personal_Horas_Extra]'" +
          " if (@Area = ''  and @SubArea = '' and @Departamento = '' and @Sucursal = ''  )  set @Filtro=  @Consulta " +
          " if (@Area != ''  and @SubArea != '' and @Departamento != '' and @Sucursal != '' )   set @Filtro =  @Consulta+ ' where Sucursal = '''+@Sucursal+''' and  SubArea = '''+@SubArea+''' and Departamento = '''+@Departamento+''' and Area='''+@Area+'''  ' " +
          " if (@Area != ''  and @SubArea != '' and @Departamento  = '' and @Sucursal != '' ) set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and  SubArea = '''+@SubArea+'''  and Area='''+@Area+'''  '" +
          " if (@Area != ''  and @SubArea != '' and @Departamento != '' and @Sucursal = '')  	set @Filtro =  @Consulta+' where SubArea = '''+@SubArea+''' and Departamento = '''+@Departamento+''' and Area='''+@Area+'''  ' " +
          " if (@Area != ''  and @SubArea != '' and @Departamento = '' and @Sucursal = '')  set @Filtro =  @Consulta+' where SubArea = '''+@SubArea+'''  and Area='''+@Area+''' '  " +
          " if (@Area != ''  and @SubArea = '' and @Departamento = '' and @Sucursal = ''  )  	  set @Filtro =  @Consulta+' where Area='''+@Area+'''  ' " +
          " if (@Area != ''  and @SubArea = '' and @Departamento  != '' and @Sucursal != '' )  	set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and Departamento = '''+@Departamento+''' and Area='''+@Area+''' '  " +
          " if (@Area != ''  and @SubArea = '' and @Departamento = '' and @Sucursal != '')  	set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and Area='''+@Area+'''  ' " +
          " if (@Area != ''  and @SubArea = '' and @Departamento  != '' and @Sucursal = '' )  	set @Filtro =  @Consulta+' where Departamento = '''+@Departamento+''' and Area='''+@Area+'''  '" +
          " if (@Area = ''  and @SubArea != '' and @Departamento != '' and @Sucursal != '' )   set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and  SubArea = '''+@SubArea+''' and Departamento = '''+@Departamento+''' ' " +
          " if (@Area = ''  and @SubArea != '' and @Departamento  = '' and @Sucursal != '' ) set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and  SubArea = '''+@SubArea+'''   '" +
          " if (@Area = ''  and @SubArea != '' and @Departamento != '' and @Sucursal = '')  	set @Filtro =  @Consulta+' where SubArea = '''+@SubArea+''' and Departamento = '''+@Departamento+'''  '" +
          " if (@Area = ''  and @SubArea != '' and @Departamento = '' and @Sucursal = '')  set @Filtro =  @Consulta+' where SubArea = '''+@SubArea+'''  '" +
          " if (@Area = ''  and @SubArea = '' and @Departamento  != '' and @Sucursal != '' )  	set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+''' and Departamento = '''+@Departamento+'''  ' " +
          " if (@Area = ''  and @SubArea = '' and @Departamento = '' and @Sucursal != '')  	set @Filtro =  @Consulta+' where Sucursal = '''+@Sucursal+'''   ' " +
          " if (@Area = ''  and @SubArea = '' and @Departamento  != '' and @Sucursal = '' )  	set @Filtro =  @Consulta+' where Departamento = '''+@Departamento+'''  '" +
          " set @Consulta=  @Filtro +' order by Mes'" +
          " execute(@Consulta)";
            return consulta;
        }
        //EVOLUCIÓN POR ÁREA MENSUAL
        public String G_Personal_HoraExtras_EvolucionAreaMensual_SQL(String anio, String mes, String area, String subArea, String sucursal)
        {        //GIAMPIERE 12/ 08 / 2019
            consulta =
                " declare @Departamento varchar(max),@Año nvarchar(max), @Area nvarchar(max), @Mes nvarchar(3),@SubArea nvarchar(max),@Sucursal nvarchar(max), @filtro  NVARCHAR(MAX), @cols NVARCHAR(MAX), @null  NVARCHAR(MAX), @consulta  NVARCHAR(MAX); " +
            " set @Año='" + anio + "'; set @Area='" + area + "';set @SubArea='" + subArea + "'; set @Sucursal= '" + sucursal + "'; set @Mes='" + mes + "';set @consulta ='';set @filtro =''; " +
            " select @cols = STUFF((SELECT Distinct ',' + QUOTENAME(Departamento)  from  [Personal].[DTM_Personal_Horas_Extra] group by ',' + QUOTENAME(Departamento)Order by ',' + QUOTENAME(Departamento) FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)')  ,1,1,'') " +
            " select @null = STUFF((SELECT Distinct ', isnull(' + QUOTENAME(Departamento)+',0) as'  + QUOTENAME(Departamento) from  [Personal].[DTM_Personal_Horas_Extra]  group by ', isnull(' + QUOTENAME(Departamento)+',0) as'  + QUOTENAME(Departamento) Order by ', isnull(' + QUOTENAME(Departamento)+',0) as'  + QUOTENAME(Departamento)   FOR XML PATH(''), TYPE  ).value('.', 'NVARCHAR(MAX)')   ,1,1,'')  " +
            " if  (@Mes = '' and @Año = ''  and @Area = '' and @SubArea = '' and @Sucursal = ''  )  	  set @consulta = 'SELECT Meses,' + @null + '  from  ( select Mes,DATENAME(MONTH,Fecha) as Meses, Departamento,CantidaHorasExtra from [Personal].[DTM_Personal_Horas_Extra]   where KeyPersonalHoraExtra in(select max(KeyPersonalHoraExtra) from [Personal].[DTM_Personal_Horas_Extra] group by Mes ,Anual, Departamento,Area,SubArea,Sucursal) )  x  pivot ( sum(CantidaHorasExtra) for Departamento in (' + @cols + ') ) p  order by Mes ' " +
            " else " +
            " begin " +
            " if  (@Mes != '' and @Año != '' and @Area != '' and @SubArea != '' and @Sucursal != '' )  	 set @filtro = ' Sucursal = '''+@Sucursal+''' and Anual = '''+@Año+''' and Area = '''+@Area+''' and SubArea = '''+@SubArea+'''  and Mes = '''+@Mes+''' ' " +
            " if  (@Mes != '' and @Año != ''  and @Area = '' and @SubArea = '' and @Sucursal = ''  )  	  set @filtro = ' Anual = '''+@Año+'''  and Mes = '''+@Mes+''' ' " +
            " if  (@Mes != '' and @Año != ''  and @Area != '' and @SubArea  = '' and @Sucursal != '' )  	 set @filtro = ' Sucursal = '''+@Sucursal+''' and Anual = '''+@Año+''' and Area='''+@Area+'''  and Mes = '''+@Mes+''' ' " +
            " if  (@Mes != '' and @Año != '' and @Area != '' and @SubArea != '' and @Sucursal = '')  	 set @filtro = ' Anual = '''+@Año+''' and Area = '''+@Area+''' and SubArea = '''+@SubArea+'''  and Mes = '''+@Mes+''' ' " +
            " if  (@Mes != '' and @Año != ''  and @Area = '' and @SubArea  != '' and @Sucursal != '' )  	  set @filtro = '  Sucursal = '''+@Sucursal+''' and Anual = '''+@Año+''' and SubArea = '''+@SubArea+'''  and Mes = '''+@Mes+'''' " +
            " if  (@Mes != '' and @Año != '' and @Area = '' and @SubArea = '' and @Sucursal != '')  	 set @filtro = ' Sucursal = '''+@Sucursal+''' and Anual = '''+@Año+''' and Mes = '''+@Mes+'''  ' " +
            " if  (@Mes != '' and @Año != ''  and @Area = '' and @SubArea  != '' and @Sucursal = '' )  	  set @filtro = '  Anual = '''+@Año+''' and SubArea = '''+@SubArea+'''  and Mes = '''+@Mes+''' ' " +
            " if  (@Mes != '' and @Año != '' and @Area != '' and @SubArea = '' and @Sucursal = '')  	 set @filtro = ' Anual = '''+@Año+''' and Area = '''+@Area+'''  and Mes = '''+@Mes+'''' " +
            " if  (@Mes != '' and @Año = '' and @Area != '' and @SubArea != '' and @Sucursal != '' )  	 set @filtro = ' Sucursal = '''+@Sucursal+'''  and Area = '''+@Area+''' and SubArea = '''+@SubArea+'''  and Mes = '''+@Mes+''' ' " +
            " if  (@Mes != '' and @Año = ''  and @Area = '' and @SubArea = '' and @Sucursal = ''  )  	  set @filtro = ' Mes = '''+@Mes+''' ' " +
            " if  (@Mes != '' and @Año = ''  and @Area != '' and @SubArea  = '' and @Sucursal != '' )  	 set @filtro = ' Sucursal = '''+@Sucursal+''' and Area='''+@Area+''' and Mes = '''+@Mes+''' ' " +
            " if  (@Mes != '' and @Año = '' and @Area != '' and @SubArea != '' and @Sucursal = '')  	 set @filtro = ' Area = '''+@Area+''' and SubArea = '''+@SubArea+'''  and Mes = '''+@Mes+''' ' " +
            " if  (@Mes != '' and @Año = ''  and @Area = '' and @SubArea  != '' and @Sucursal != '' )  	  set @filtro = '  Sucursal = '''+@Sucursal+'''  and SubArea = '''+@SubArea+'''  and Mes = '''+@Mes+''' ' " +
            " if  (@Mes != '' and @Año = '' and @Area = '' and @SubArea = '' and @Sucursal != '')  	 set @filtro = ' Sucursal = '''+@Sucursal+'''  and Mes = '''+@Mes+''' ' " +
            " if  (@Mes != '' and @Año = ''  and @Area = '' and @SubArea  != '' and @Sucursal = '' )  	  set @filtro = '  SubArea = '''+@SubArea+'''  and Mes = '''+@Mes+''' ' " +
            " if  (@Mes != '' and @Año = '' and @Area != '' and @SubArea = '' and @Sucursal = '')  	 set @filtro = 'Area = '''+@Area+'''  and Mes = '''+@Mes+''' '" +
            " if  (@Mes = '' and @Año != '' and @Area != '' and @SubArea != '' and @Sucursal != '' )  	 set @filtro = ' Sucursal = '''+@Sucursal+''' and Anual = '''+@Año+''' and Area = '''+@Area+''' and SubArea = '''+@SubArea+''' ' " +
            " if  (@Mes = '' and @Año != ''  and @Area = '' and @SubArea = '' and @Sucursal = ''  )  	  set @filtro = ' Anual = '''+@Año+''''" +
            " if  (@Mes = '' and @Año != ''  and @Area != '' and @SubArea  = '' and @Sucursal != '' )  	 set @filtro = ' Sucursal = '''+@Sucursal+''' and Anual = '''+@Año+''' and Area='''+@Area+''' ' " +
            " if  (@Mes = '' and @Año != '' and @Area != '' and @SubArea != '' and @Sucursal = '')  	 set @filtro = ' Anual = '''+@Año+''' and Area = '''+@Area+''' and SubArea = '''+@SubArea+''' ' " +
            " if  (@Mes = '' and @Año != ''  and @Area = '' and @SubArea  != '' and @Sucursal != '' )  	  set @filtro = '  Sucursal = '''+@Sucursal+''' and Anual = '''+@Año+''' and SubArea = '''+@SubArea+''' ' " +
            " if  (@Mes = '' and @Año != '' and @Area = '' and @SubArea = '' and @Sucursal != '')  	 set @filtro = ' Sucursal = '''+@Sucursal+''' and Anual = '''+@Año+''' ' " +
            " if  (@Mes = '' and @Año != ''  and @Area = '' and @SubArea  != '' and @Sucursal = '' )  	  set @filtro = '  Anual = '''+@Año+''' and SubArea = '''+@SubArea+''' ' " +
            " if  (@Mes = '' and @Año != '' and @Area != '' and @SubArea = '' and @Sucursal = '')  	 set @filtro = ' Anual = '''+@Año+''' and Area = '''+@Area+''' ' " +
            " if  (@Mes = '' and @Año = '' and @Area != '' and @SubArea != '' and @Sucursal != '' )  	 set @filtro = ' Sucursal = '''+@Sucursal+'''  and Area = '''+@Area+''' and SubArea = '''+@SubArea+''' ' " +
            " if  (@Mes = '' and @Año = ''  and @Area != '' and @SubArea  = '' and @Sucursal != '' )  	 set @filtro = ' Sucursal = '''+@Sucursal+''' and Area='''+@Area+''' ' " +
            " if  (@Mes = '' and @Año = '' and @Area != '' and @SubArea != '' and @Sucursal = '')  	 set @filtro = ' Area = '''+@Area+''' and SubArea = '''+@SubArea+''' ' " +
            " if  (@Mes = '' and @Año = ''  and @Area = '' and @SubArea  != '' and @Sucursal != '' )  	  set @filtro = '  Sucursal = '''+@Sucursal+'''  and SubArea = '''+@SubArea+''' ' " +
            " if  (@Mes = '' and @Año = '' and @Area = '' and @SubArea = '' and @Sucursal != '')  	 set @filtro = ' Sucursal = '''+@Sucursal+''' ' " +
            " if  (@Mes = '' and @Año = ''  and @Area = '' and @SubArea  != '' and @Sucursal = '' )  	  set @filtro = '  SubArea = '''+@SubArea+''' ' " +
            " if  (@Mes = '' and @Año = '' and @Area != '' and @SubArea = '' and @Sucursal = '')  	 set @filtro = 'Area = '''+@Area+''' ' " +
            " set @consulta = 'SELECT Meses,' + @null + '  from  ( select Mes,DATENAME(MONTH,Fecha) as Meses, Departamento,CantidaHorasExtra from [Personal].[DTM_Personal_Horas_Extra]   where ' +@filtro+ ' and KeyPersonalHoraExtra in(select max(KeyPersonalHoraExtra) from [Personal].[DTM_Personal_Horas_Extra] where ' +@filtro+ '  group by Mes ,Anual, Departamento,Area,SubArea,Sucursal) )  x  pivot ( sum(CantidaHorasExtra) for Departamento in (' + @cols + ') ) p  order by Mes ' " +
            " end " +
            " execute(@consulta)";
            return consulta;
        }
        public String Personal_HoraExtras_EvolucionAreaMensual_ddlSucursal_SQL(String anio, String mes, String area, String subArea)
        {        //GIAMPIERE 12/ 08 / 2019
            consulta =
                 " declare @Año varchar(20), @Area nvarchar(max),@SubArea nvarchar(max),@Mes nvarchar(max),@Consulta nvarchar(max),@filtro nvarchar(max),@select nvarchar(max); " +
            " set @Año='" + anio + "'; set @Area='" + area + "';set @SubArea='" + subArea + "'; set @Mes='" + mes + "';set @Consulta= ''; set @filtro= ''; set @select= ''; " +
            " set @select= N'select distinct Sucursal from [Personal].[DTM_Personal_Horas_Extra]' " +
            " if ( @Año != '' and @Area != '' and @SubArea != '' and @Mes != '' )   set @filtro = @select+  ' where  Mes = '''+@Mes+''' and Anual = '''+@Año+''' and Area = '''+@Area+''' and SubArea = '''+@SubArea+''' ' " +
            " if ( @Año != '' and @Area != '' and @SubArea  = '' and @Mes != '' ) set @filtro = @select+  ' where  Mes = '''+@Mes+''' and Anual = '''+@Año+''' and Area = '''+@Area+''' ' " +
            " if ( @Año != '' and @Area != '' and @SubArea != '' and @Mes = '')  	set @filtro = @select+  ' where  Anual = '''+@Año+''' and Area = '''+@Area+''' and SubArea = '''+@SubArea+''' ' " +
            " if ( @Año != '' and @Area != '' and @SubArea = '' and @Mes = '')  set @filtro = @select+  ' where  Anual = '''+@Año+''' and Area = '''+@Area+''' ' " +
            " if ( @Año != '' and @Area = '' and @SubArea = '' and @Mes = ''  )  	  set @filtro = @select+  ' where Anual = '''+@Año+'''   ' " +
            " if ( @Año != '' and @Area = '' and @SubArea  != '' and @Mes != '' )  	set @filtro = @select+  ' where  Mes = '''+@Mes+''' and Anual = '''+@Año+'''  and SubArea = '''+@SubArea+''' ' " +
            " if ( @Año != '' and @Area = '' and @SubArea = '' and @Mes != '')  	set @filtro = @select+  ' where  Mes = '''+@Mes+''' and Anual = '''+@Año+'''   ' " +
            " if ( @Año != '' and @Area = '' and @SubArea  != '' and @Mes = '' )  	set @filtro = @select+  ' where  Anual = '''+@Año+''' and SubArea = '''+@SubArea+'''  '  " +
            " if ( @Año = '' and @Area != '' and @SubArea != '' and @Mes != '' )   set @filtro = @select+  ' where  Mes = '''+@Mes+''' and  Area = '''+@Area+''' and SubArea = '''+@SubArea+'''  ' " +
            " if ( @Año = '' and @Area != '' and @SubArea  = '' and @Mes != '' ) set @filtro = @select+  ' where  Mes = '''+@Mes+''' and  Area = '''+@Area+''' ' " +
            " if ( @Año = '' and @Area != '' and @SubArea != '' and @Mes = '')  	set @filtro = @select+  ' where   Area = '''+@Area+''' and SubArea = '''+@SubArea+''' ' " +
            " if ( @Año = '' and @Area != '' and @SubArea = '' and @Mes = '')  set @filtro = @select+  ' where   Area = '''+@Area+''' ' " +
            " if ( @Año = '' and @Area = '' and @SubArea = '' and @Mes = ''  )  	  set @filtro = @select+  '' " +
            " if ( @Año = '' and @Area = '' and @SubArea  != '' and @Mes != '' )  	set @filtro = @select+  ' where  Mes = '''+@Mes+''' and SubArea = '''+@SubArea+''' ' " +
            " if ( @Año = '' and @Area = '' and @SubArea = '' and @Mes != '')  	set @filtro = @select+  ' where  Mes = '''+@Mes+'''    ' " +
            " if ( @Año = '' and @Area = '' and @SubArea  != '' and @Mes = '' )  	set @filtro = @select+  ' where   SubArea = '''+@SubArea+''' ' " +
            " set @Consulta= @filtro+ 'order by Sucursal'; " +
            " execute(@Consulta) ";
            return consulta;
        }
        public String Personal_HoraExtras_EvolucionAreaMensual_ddlArea_SQL(String anio, String mes, String subArea, String sucursal)
        {        //GIAMPIERE 12/ 08 / 2019
            consulta =
                 " declare @Año varchar(5), @SubArea nvarchar(max),@Sucursal nvarchar(max),@Mes nvarchar(max),@Consulta nvarchar(max),@filtro nvarchar(max),@select nvarchar(max); " +
            " set @Año='" + anio + "'; set @SubArea='" + subArea + "'; set @Sucursal= '" + sucursal + "'; set @Mes='" + mes + "';set @Consulta= ''; set @filtro= ''; set @select= ''; " +
            " set @select= N'select distinct Area from [Personal].[DTM_Personal_Horas_Extra]' " +
            " if ( @Año != '' and @SubArea != '' and @Sucursal != '' and @Mes != '' )   set @filtro = @select+  ' where  Mes = '''+@Mes+''' and Anual = '''+@Año+''' and SubArea = '''+@SubArea+''' and Sucursal = '''+@Sucursal+''' ' " +
            " if ( @Año != '' and @SubArea != '' and @Sucursal  = '' and @Mes != '' ) set @filtro = @select+  ' where  Mes = '''+@Mes+''' and Anual = '''+@Año+''' and SubArea = '''+@SubArea+''' ' " +
            " if ( @Año != '' and @SubArea != '' and @Sucursal != '' and @Mes = '')  	set @filtro = @select+  ' where  Anual = '''+@Año+''' and SubArea = '''+@SubArea+''' and Sucursal = '''+@Sucursal+''' ' " +
            " if ( @Año != '' and @SubArea != '' and @Sucursal = '' and @Mes = '')  set @filtro = @select+  ' where  Anual = '''+@Año+''' and SubArea = '''+@SubArea+''' ' " +
            " if ( @Año != '' and @SubArea = '' and @Sucursal = '' and @Mes = ''  )  	  set @filtro = @select+  ' where Anual = '''+@Año+'''   ' " +
            " if ( @Año != '' and @SubArea = '' and @Sucursal  != '' and @Mes != '' )  	set @filtro = @select+  ' where  Mes = '''+@Mes+''' and Anual = '''+@Año+'''  and Sucursal = '''+@Sucursal+''' ' " +
            " if ( @Año != '' and @SubArea = '' and @Sucursal = '' and @Mes != '')  	set @filtro = @select+  ' where  Mes = '''+@Mes+''' and Anual = '''+@Año+'''   ' " +
            " if ( @Año != '' and @SubArea = '' and @Sucursal  != '' and @Mes = '' )  	set @filtro = @select+  ' where  Anual = '''+@Año+''' and Sucursal = '''+@Sucursal+'''  '  " +
            " if ( @Año = '' and @SubArea != '' and @Sucursal != '' and @Mes != '' )   set @filtro = @select+  ' where  Mes = '''+@Mes+''' and  SubArea = '''+@SubArea+''' and Sucursal = '''+@Sucursal+'''  ' " +
            " if ( @Año = '' and @SubArea != '' and @Sucursal  = '' and @Mes != '' ) set @filtro = @select+  ' where  Mes = '''+@Mes+''' and  SubArea = '''+@SubArea+''' ' " +
            " if ( @Año = '' and @SubArea != '' and @Sucursal != '' and @Mes = '')  	set @filtro = @select+  ' where   SubArea = '''+@SubArea+''' and Sucursal = '''+@Sucursal+''' ' " +
            " if ( @Año = '' and @SubArea != '' and @Sucursal = '' and @Mes = '')  set @filtro = @select+  ' where   SubArea = '''+@SubArea+''' ' " +
            " if ( @Año = '' and @SubArea = '' and @Sucursal = '' and @Mes = ''  )  	  set @filtro = @select+  '' " +
            " if ( @Año = '' and @SubArea = '' and @Sucursal  != '' and @Mes != '' )  	set @filtro = @select+  ' where  Mes = '''+@Mes+''' and Sucursal = '''+@Sucursal+''' ' " +
            " if ( @Año = '' and @SubArea = '' and @Sucursal = '' and @Mes != '')  	set @filtro = @select+  ' where  Mes = '''+@Mes+'''    ' " +
            " if ( @Año = '' and @SubArea = '' and @Sucursal  != '' and @Mes = '' )  	set @filtro = @select+  ' where   Sucursal = '''+@Sucursal+''' ' " +
            " set @Consulta= @filtro+ 'order by Area'; " +
            " execute(@Consulta) ";
            return consulta;
        }
        public String Personal_HoraExtras_EvolucionAreaMensual_ddlSubArea_SQL(String anio, String mes, String area, String sucursal)
        {        //GIAMPIERE 12/ 08 / 2019
            consulta =
                  " declare @Año varchar(20), @Area nvarchar(max),@Sucursal nvarchar(max),@Mes nvarchar(max),@Consulta nvarchar(max),@filtro nvarchar(max),@select nvarchar(max); " +
            " set @Año='" + anio + "'; set @Area='" + area + "';set @Sucursal= '" + sucursal + "'; set @Mes='" + mes + "';set @Consulta= ''; set @filtro= ''; set @select= ''; " +
            " set @select= N'select distinct SubArea from [Personal].[DTM_Personal_Horas_Extra]' " +
            " if ( @Año != '' and @Area != '' and @Sucursal != '' and @Mes != '' )   set @filtro = @select+  ' where  Mes = '''+@Mes+''' and Anual = '''+@Año+''' and Area = '''+@Area+''' and Sucursal = '''+@Sucursal+''' ' " +
            " if ( @Año != '' and @Area != '' and @Sucursal  = '' and @Mes != '' ) set @filtro = @select+  ' where  Mes = '''+@Mes+''' and Anual = '''+@Año+''' and Area = '''+@Area+''' ' " +
            " if ( @Año != '' and @Area != '' and @Sucursal != '' and @Mes = '')  	set @filtro = @select+  ' where  Anual = '''+@Año+''' and Area = '''+@Area+''' and Sucursal = '''+@Sucursal+''' ' " +
            " if ( @Año != '' and @Area != '' and @Sucursal = '' and @Mes = '')  set @filtro = @select+  ' where  Anual = '''+@Año+''' and Area = '''+@Area+''' ' " +
            " if ( @Año != '' and @Area = '' and @Sucursal = '' and @Mes = ''  )  	  set @filtro = @select+  ' where Anual = '''+@Año+'''   ' " +
            " if ( @Año != '' and @Area = '' and @Sucursal  != '' and @Mes != '' )  	set @filtro = @select+  ' where  Mes = '''+@Mes+''' and Anual = '''+@Año+'''  and Sucursal = '''+@Sucursal+''' ' " +
            " if ( @Año != '' and @Area = '' and @Sucursal = '' and @Mes != '')  	set @filtro = @select+  ' where  Mes = '''+@Mes+''' and Anual = '''+@Año+'''   ' " +
            " if ( @Año != '' and @Area = '' and @Sucursal  != '' and @Mes = '' )  	set @filtro = @select+  ' where  Anual = '''+@Año+''' and Sucursal = '''+@Sucursal+'''  '  " +
            " if ( @Año = '' and @Area != '' and @Sucursal != '' and @Mes != '' )   set @filtro = @select+  ' where  Mes = '''+@Mes+''' and  Area = '''+@Area+''' and Sucursal = '''+@Sucursal+'''  ' " +
            " if ( @Año = '' and @Area != '' and @Sucursal  = '' and @Mes != '' ) set @filtro = @select+  ' where  Mes = '''+@Mes+''' and  Area = '''+@Area+''' ' " +
            " if ( @Año = '' and @Area != '' and @Sucursal != '' and @Mes = '')  	set @filtro = @select+  ' where   Area = '''+@Area+''' and Sucursal = '''+@Sucursal+''' ' " +
            " if ( @Año = '' and @Area != '' and @Sucursal = '' and @Mes = '')  set @filtro = @select+  ' where   Area = '''+@Area+''' ' " +
            " if ( @Año = '' and @Area = '' and @Sucursal = '' and @Mes = ''  )  	  set @filtro = @select+  '' " +
            " if ( @Año = '' and @Area = '' and @Sucursal  != '' and @Mes != '' )  	set @filtro = @select+  ' where  Mes = '''+@Mes+''' and Sucursal = '''+@Sucursal+''' ' " +
            " if ( @Año = '' and @Area = '' and @Sucursal = '' and @Mes != '')  	set @filtro = @select+  ' where  Mes = '''+@Mes+'''    ' " +
            " if ( @Año = '' and @Area = '' and @Sucursal  != '' and @Mes = '' )  	set @filtro = @select+  ' where   Sucursal = '''+@Sucursal+''' ' " +
            " set @Consulta= @filtro+ 'order by SubArea'; " +
            " execute(@Consulta) ";

            return consulta;
        }
        public String Personal_HoraExtras_EvolucionAreaMensual_ddlAnio_SQL(String mes, String area, String subArea, String sucursal)
        {//GIAMPIERE 12/ 08 / 2019
            consulta =
            " declare @Mes varchar(5), @SubArea nvarchar(max),@Sucursal nvarchar(max),@Area nvarchar(max),@Consulta nvarchar(max),@filtro nvarchar(max),@select nvarchar(max); " +
            " set @Area='" + area + "';set @SubArea='" + subArea + "'; set @Sucursal= '" + sucursal + "'; set @Mes='" + mes + "';set @Consulta= ''; set @filtro= ''; set @select= ''; " +
            " set @select= N'select distinct Anual as Anio from [Personal].[DTM_Personal_Horas_Extra]' " +
            " if ( @Mes != '' and @SubArea != '' and @Sucursal != '' and @Area != '' )   set @filtro = @select+  ' where  Area = '''+@Area+''' and Mes = '''+@Mes+''' and SubArea = '''+@SubArea+''' and Sucursal = '''+@Sucursal+''' ' " +
            " if ( @Mes != '' and @SubArea != '' and @Sucursal  = '' and @Area != '' ) set @filtro = @select+  ' where  Area = '''+@Area+''' and Mes = '''+@Mes+''' and SubArea = '''+@SubArea+''' ' " +
            " if ( @Mes != '' and @SubArea != '' and @Sucursal != '' and @Area = '')  	set @filtro = @select+  ' where  Mes = '''+@Mes+''' and SubArea = '''+@SubArea+''' and Sucursal = '''+@Sucursal+''' ' " +
            " if ( @Mes != '' and @SubArea != '' and @Sucursal = '' and @Area = '')  set @filtro = @select+  ' where  Mes = '''+@Mes+''' and SubArea = '''+@SubArea+''' ' " +
            " if ( @Mes != '' and @SubArea = '' and @Sucursal = '' and @Area = ''  )  	  set @filtro = @select+  ' where Mes = '''+@Mes+'''   ' " +
            " if ( @Mes != '' and @SubArea = '' and @Sucursal  != '' and @Area != '' )  	set @filtro = @select+  ' where  Area = '''+@Area+''' and Mes = '''+@Mes+'''  and Sucursal = '''+@Sucursal+''' ' " +
            " if ( @Mes != '' and @SubArea = '' and @Sucursal = '' and @Area != '')  	set @filtro = @select+  ' where  Area = '''+@Area+''' and Mes = '''+@Mes+'''   ' " +
            " if ( @Mes != '' and @SubArea = '' and @Sucursal  != '' and @Area = '' )  	set @filtro = @select+  ' where  Mes = '''+@Mes+''' and Sucursal = '''+@Sucursal+'''  '  " +
            " if ( @Mes = '' and @SubArea != '' and @Sucursal != '' and @Area != '' )   set @filtro = @select+  ' where  Area = '''+@Area+''' and  SubArea = '''+@SubArea+''' and Sucursal = '''+@Sucursal+'''  ' " +
            " if ( @Mes = '' and @SubArea != '' and @Sucursal  = '' and @Area != '' ) set @filtro = @select+  ' where  Area = '''+@Area+''' and  SubArea = '''+@SubArea+''' ' " +
            " if ( @Mes = '' and @SubArea != '' and @Sucursal != '' and @Area = '')  	set @filtro = @select+  ' where   SubArea = '''+@SubArea+''' and Sucursal = '''+@Sucursal+''' ' " +
            " if ( @Mes = '' and @SubArea != '' and @Sucursal = '' and @Area = '')  set @filtro = @select+  ' where   SubArea = '''+@SubArea+''' ' " +
            " if ( @Mes = '' and @SubArea = '' and @Sucursal = '' and @Area = ''  )  	  set @filtro = @select+  '' " +
            " if ( @Mes = '' and @SubArea = '' and @Sucursal  != '' and @Area != '' )  	set @filtro = @select+  ' where  Area = '''+@Area+''' and Sucursal = '''+@Sucursal+''' ' " +
            " if ( @Mes = '' and @SubArea = '' and @Sucursal = '' and @Area != '')  	set @filtro = @select+  ' where  Area = '''+@Area+'''    ' " +
            " if ( @Mes = '' and @SubArea = '' and @Sucursal  != '' and @Area = '' )  	set @filtro = @select+  ' where   Sucursal = '''+@Sucursal+''' ' " +
            " set @Consulta= @filtro+ 'order by Anual'; " +
            " execute(@Consulta) ";
            return consulta;
        }
        public String Personal_HoraExtras_EvolucionAreaMensual_ddlMes_SQL(String anio, String area, String subArea, String sucursal)
        {//GIAMPIERE 12/ 08 / 2019
            consulta =
                 " declare @Año varchar(5), @SubArea nvarchar(max),@Sucursal nvarchar(max),@Area nvarchar(max),@Consulta nvarchar(max),@filtro nvarchar(max),@select nvarchar(max); " +
            " set @Año='" + anio + "'; set @Area='" + area + "';set @SubArea='" + subArea + "'; set @Sucursal= '" + sucursal + "';set @Consulta= ''; set @filtro= ''; set @select= ''; " +
            " set @select= N'select distinct DATENAME(MONTH,Fecha) as Meses ,Mes from [Personal].[DTM_Personal_Horas_Extra]' " +
            " if ( @Año != '' and @SubArea != '' and @Sucursal != '' and @Area != '' )   set @filtro = @select+  ' where  Area = '''+@Area+''' and Anual = '''+@Año+''' and SubArea = '''+@SubArea+''' and Sucursal = '''+@Sucursal+''' ' " +
            " if ( @Año != '' and @SubArea != '' and @Sucursal  = '' and @Area != '' ) set @filtro = @select+  ' where  Area = '''+@Area+''' and Anual = '''+@Año+''' and SubArea = '''+@SubArea+''' ' " +
            " if ( @Año != '' and @SubArea != '' and @Sucursal != '' and @Area = '')  	set @filtro = @select+  ' where  Anual = '''+@Año+''' and SubArea = '''+@SubArea+''' and Sucursal = '''+@Sucursal+''' ' " +
            " if ( @Año != '' and @SubArea != '' and @Sucursal = '' and @Area = '')  set @filtro = @select+  ' where  Anual = '''+@Año+''' and SubArea = '''+@SubArea+''' ' " +
            " if ( @Año != '' and @SubArea = '' and @Sucursal = '' and @Area = ''  )  	  set @filtro = @select+  ' where Anual = '''+@Año+'''   ' " +
            " if ( @Año != '' and @SubArea = '' and @Sucursal  != '' and @Area != '' )  	set @filtro = @select+  ' where  Area = '''+@Area+''' and Anual = '''+@Año+'''  and Sucursal = '''+@Sucursal+''' ' " +
            " if ( @Año != '' and @SubArea = '' and @Sucursal = '' and @Area != '')  	set @filtro = @select+  ' where  Area = '''+@Area+''' and Anual = '''+@Año+'''   ' " +
            " if ( @Año != '' and @SubArea = '' and @Sucursal  != '' and @Area = '' )  	set @filtro = @select+  ' where  Anual = '''+@Año+''' and Sucursal = '''+@Sucursal+'''  '  " +
            " if ( @Año = '' and @SubArea != '' and @Sucursal != '' and @Area != '' )   set @filtro = @select+  ' where  Area = '''+@Area+''' and  SubArea = '''+@SubArea+''' and Sucursal = '''+@Sucursal+'''  ' " +
            " if ( @Año = '' and @SubArea != '' and @Sucursal  = '' and @Area != '' ) set @filtro = @select+  ' where  Area = '''+@Area+''' and  SubArea = '''+@SubArea+''' ' " +
            " if ( @Año = '' and @SubArea != '' and @Sucursal != '' and @Area = '')  	set @filtro = @select+  ' where   SubArea = '''+@SubArea+''' and Sucursal = '''+@Sucursal+''' ' " +
            " if ( @Año = '' and @SubArea != '' and @Sucursal = '' and @Area = '')  set @filtro = @select+  ' where   SubArea = '''+@SubArea+''' ' " +
            " if ( @Año = '' and @SubArea = '' and @Sucursal = '' and @Area = ''  )  	  set @filtro = @select+  '' " +
            " if ( @Año = '' and @SubArea = '' and @Sucursal  != '' and @Area != '' )  	set @filtro = @select+  ' where  Area = '''+@Area+''' and Sucursal = '''+@Sucursal+''' ' " +
            " if ( @Año = '' and @SubArea = '' and @Sucursal = '' and @Area != '')  	set @filtro = @select+  ' where  Area = '''+@Area+'''    ' " +
            " if ( @Año = '' and @SubArea = '' and @Sucursal  != '' and @Area = '' )  	set @filtro = @select+  ' where   Sucursal = '''+@Sucursal+''' ' " +
            " set @Consulta= @filtro+ 'order by Mes'; " +
            " execute(@Consulta) ";

            return consulta;
        }

        #endregion

       
        #region SUSPENSIONES_AMONESTACIONES

         

        //BALTODANO QUISPE JORGE 19/ 08 / 2019
        // CANTIDAD DE SUSPENSIONES  GRAFICA 1
        public string Grafica_Personal_Suspensiones_CantidadSuspensiones_SQL =

                "  declare @Mes date, @primerDiaMes date, @ultimaDiaMes date  "+
                "  IF(getdate()= (select EOMONTH(getdate()) as UltDiaMesActual))  "+
                "  begin set @Mes=	(SELECT CONVERT (date, GETDATE()))	 end ELSE begin"+
                "  set @Mes= (select EOMONTH(getdate(),-1) as UltDiaMesAnterior)  end "+
                "  set @primerDiaMes= dateadd(d,1, EOMONTH(@Mes,-1)) "+
                "  set @ultimaDiaMes = EOMONTH(@Mes) "+
                "  select isnull(sum(Cantidad_Suspenciones) ,0)as Cantidad  from [Personal].[DTM_Personal_Suspensiones] "+
                "  where Fecha between @primerDiaMes and @ultimaDiaMes  and  KeyPersonalSuspenciones in(select max(KeyPersonalSuspenciones) "+
                "  from [Personal].[DTM_Personal_Suspensiones] Where Fecha between @primerDiaMes and @ultimaDiaMes "+
                "  group by Mes,Sucursal ,Departamento,Area,SubArea) ";

        //BALTODANO JORGE 17/ 08 / 2019
        public string Grafica_Personal_Amonestaciones_CantidadAmonestaciones_SQL =

                "  declare @Mes date, @primerDiaMes date, @ultimaDiaMes date  "+
                "  IF(getdate()= (select EOMONTH(getdate()) as UltDiaMesActual))  "+
                "  begin set @Mes=	(SELECT CONVERT (date, GETDATE()))	 end ELSE begin"+
                "  set @Mes= (select EOMONTH(getdate(),-1) as UltDiaMesAnterior)  end "+
                "  set @primerDiaMes= dateadd(d,1, EOMONTH(@Mes,-1)) "+
                "  set @ultimaDiaMes = EOMONTH(@Mes) "+
                "  select isnull(sum(Cantidad_Incumplimiento) ,0)as Cantidad  from [Personal].[DTM_Personal_Suspensiones] "+
                "  where Fecha between @primerDiaMes and @ultimaDiaMes  and  KeyPersonalSuspenciones in(select max(KeyPersonalSuspenciones) "+
                "  from [Personal].[DTM_Personal_Suspensiones] Where Fecha between @primerDiaMes and @ultimaDiaMes "+
                "  group by Mes,Sucursal ,Departamento,Area,SubArea) ";

        //BALTODANO JORGE 17/ 08 / 2019
        public string Consulta_Personal_SuspensionesyAmonestaciones_Tipo_SQL =

        " CREATE TABLE #temp (Tipo varchar(max))" +
       " INSERT INTO #temp VALUES ('Suspensiones') " +
       " INSERT INTO #temp VALUES ('Amonestaciones') " +
       " select Tipo from #temp  drop table #temp ";

        
        //? FUNCIONES PARA GRAFICAR CANTIDAD DE SUSPENSIONES POR DEPARTAMENTO 

        // ** Funcion para Consultar Sucursal
        public String Personal_Suspensiones_listarSucursal(String area, String subArea)
        {
            consulta =
                "   declare  @area varchar(max), @subArea varchar(max) " +
                "    set @area='" + area + "';set @subArea='" + subArea + "'; " +
                "   if ( @area = '' and @subArea = '' ) Select distinct Sucursal  from [Personal].[DTM_Personal_Suspensiones]" +
                "   if ( @area != '' and @subArea = '' ) Select distinct Sucursal  from [Personal].[DTM_Personal_Suspensiones] Where   area=@area " +
                "   if ( @area = '' and @subArea != '' ) Select distinct Sucursal  from [Personal].[DTM_Personal_Suspensiones] Where subArea=@subArea" +
                "    if ( @area != '' and @subArea != '') Select distinct Sucursal  from [Personal].[DTM_Personal_Suspensiones] Where area=@area and subArea=@subArea";

            return consulta;
        }


        // ** Funcion para consultar el Area
        public String Personal_Suspensiones_ListarArea(String sucursal, String subArea)
        {
            consulta =
            "   declare  @Sucursal varchar(max), @SubArea varchar(max)" +
            "  set @SubArea='" + subArea + "'; set @Sucursal= '" + sucursal + "';" +
            "  if (@SubArea = '' and @Sucursal = '')  Select distinct Area  from [Personal].[DTM_Personal_Suspensiones] " +
            "   if (@SubArea = '' and @Sucursal != '')  Select distinct Area  from [Personal].[DTM_Personal_Suspensiones] where Sucursal=@Sucursal" +
            "   if (@SubArea != '' and @Sucursal = '') Select distinct Area  from [Personal].[DTM_Personal_Suspensiones] where SubArea=@SubArea " +
            "   if (@SubArea != '' and @Sucursal != '') Select distinct Area  from [Personal].[DTM_Personal_Suspensiones] where Sucursal=@Sucursal and SubArea=@SubArea";
            return consulta;
        }


        // ** Funcion para consultar subArea
        public String Personal_Suspensiones_ListarsSubArea(String sucursal, String area)
        {
            consulta =
            "    declare  @Sucursal varchar(max), @Area  varchar(max)" +
            "   set @Area='" + area + "'; set @Sucursal= '" + sucursal + "';" +
            "   if (@Area  = '' and @Sucursal = '')  Select distinct SubArea  from [Personal].[DTM_Personal_Suspensiones]" +
            "   if (@Area  = '' and @Sucursal != '')  Select distinct SubArea  from [Personal].[DTM_Personal_Suspensiones] where Sucursal=@Sucursal  " +
            "   if (@Area  != '' and @Sucursal = '') Select distinct SubArea  from [Personal].[DTM_Personal_Suspensiones] where Area=@Area  " +
            "   if (@Area  != '' and @Sucursal != '') Select distinct SubArea  from [Personal].[DTM_Personal_Suspensiones] where Sucursal=@Sucursal and Area=@Area ";

            return consulta;
        }

        //** Funcion para consultar el año
        public String Personal_Suspensiones_ListarAño(String subArea, String sucursal, String area)
        {

            consulta =
            "  declare  @Area nvarchar(max),@mesActual varchar(max),@mes varchar(max),@anio varchar(max), @SubArea nvarchar(max), @Sucursal nvarchar(max) " +
            "  set @Area='" + area + "';set @SubArea='" + subArea + "'; set @Sucursal= ' " + sucursal + "';" +
            "  IF(getdate() = (select EOMONTH(getdate()))) begin set @mesActual = (SELECT CONVERT(date, GETDATE())) end ELSe begin set @mesActual = (select EOMONTH(getdate(), -1) ) end set @mes = MONTH(@mesActual); set @anio = YEAR(@mesActual);" +
            "  if ( @Area != '' and @SubArea = '' and @Sucursal = '') select Departamento, sum(Cantidad_Suspenciones) as Cantidad  from [Personal].[DTM_Personal_Suspensiones]  Where  Mes=@mes and Año=@anio and Area=@Area and KeyPersonalSuspenciones in(select max(KeyPersonalSuspenciones) from [Personal].[DTM_Personal_Suspensiones]  where Mes=@mes and Año=@anio and Area=@Area group by Sucursal ,Departamento,Area,SubArea) group by Departamento" +
            "  if ( @Area != '' and @SubArea = '' and @Sucursal != '') select Departamento, sum(Cantidad_Suspenciones) as Cantidad  from [Personal].[DTM_Personal_Suspensiones]  Where  Mes=@mes and Año=@anio and Area=@Area and Sucursal=@Sucursal  and KeyPersonalSuspenciones in(select max(KeyPersonalSuspenciones) from [Personal].[DTM_Personal_Suspensiones]  where Mes=@mes and Año=@anio and Area=@Area and Sucursal=@Sucursal group by Sucursal ,Departamento,Area,SubArea) group by Departamento" +
            "  if ( @Area != '' and @SubArea != '' and @Sucursal = '') select Departamento, sum(Cantidad_Suspenciones) as Cantidad  from [Personal].[DTM_Personal_Suspensiones]  Where  Mes=@mes and Año=@anio  and Area=@Area and SubArea=@SubArea and KeyPersonalSuspenciones in(select max(KeyPersonalSuspenciones) from [Personal].[DTM_Personal_Suspensiones]  where Mes=@mes and Año=@anio and Area=@Area and SubArea=@SubArea group by Sucursal ,Departamento,Area,SubArea) group by Departamento" +
            "  if ( @Area != '' and @SubArea != '' and @Sucursal != '') select Departamento, sum(Cantidad_Suspenciones) as Cantidad  from [Personal].[DTM_Personal_Suspensiones]  Where  Mes=@mes and Año=@anio and Area=@Area and SubArea=@SubArea and Sucursal=@Sucursal and KeyPersonalSuspenciones in(select max(KeyPersonalSuspenciones) from [Personal].[DTM_Personal_Suspensiones]  where Mes=@mes and Año=@anio and Area=@Area and SubArea=@SubArea and Sucursal=@Sucursal group by Sucursal ,Departamento,Area,SubArea) group by Departamento" +
            "  if ( @Area = '' and @SubArea = '' and @Sucursal = '') select Departamento, sum(Cantidad_Suspenciones) as Cantidad  from [Personal].[DTM_Personal_Suspensiones]  Where  Mes=@mes and Año=@anio  and KeyPersonalSuspenciones in(select max(KeyPersonalSuspenciones) from [Personal].[DTM_Personal_Suspensiones]  where Mes=@mes and Año=@anio  group by Sucursal ,Departamento,Area,SubArea) group by Departamento" +
            "  if ( @Area = '' and @SubArea = '' and @Sucursal != '') select Departamento, sum(Cantidad_Suspenciones) as Cantidad  from [Personal].[DTM_Personal_Suspensiones]  Where  Mes=@mes and Año=@anio and Sucursal=@Sucursal  and KeyPersonalSuspenciones in(select max(KeyPersonalSuspenciones) from [Personal].[DTM_Personal_Suspensiones]  where Mes=@mes and Año=@anio and Sucursal=@Sucursal group by Sucursal ,Departamento,Area,SubArea) group by Departamento" +
            "  if ( @Area = '' and @SubArea != '' and @Sucursal = '') select Departamento, sum(Cantidad_Suspenciones) as Cantidad  from [Personal].[DTM_Personal_Suspensiones]  Where  Mes=@mes and Año=@anio and SubArea=@SubArea and KeyPersonalSuspenciones in(select max(KeyPersonalSuspenciones) from [Personal].[DTM_Personal_Suspensiones]  where Mes=@mes and Año=@anio and SubArea=@SubArea group by Sucursal ,Departamento,Area,SubArea) group by Departamento " +
            "  if ( @Area = '' and @SubArea != '' and @Sucursal != '') select Departamento, sum(Cantidad_Suspenciones) as Cantidad  from [Personal].[DTM_Personal_Suspensiones]  Where  Mes=@mes and Año=@anio and SubArea=@SubArea and Sucursal=@Sucursal and KeyPersonalSuspenciones in(select max(KeyPersonalSuspenciones) from [Personal].[DTM_Personal_Suspensiones]  where Mes=@mes and Año=@anio and SubArea=@SubArea and Sucursal=@Sucursal group by Sucursal ,Departamento,Area,SubArea) group by Departamento";

            return consulta;

        }



        // ** Funcion para Graficar La cantidad de suspensiones por Departamento
        public String Grafica_Personal_Suspensiones_CantidadSuspensionesDepartamento(String area, String subArea, String sucursal)
        {
            consulta =
               "  declare  @area varchar(max),@mesActual varchar(max),@mes varchar(max),@anio varchar(max), @subArea nvarchar(max), @sucursal varchar(max)  " +
                "  set @area = '" + area + "'; set @subArea = '" + subArea + "'; set @sucursal = '" + sucursal + "'; " +
                "  IF(getdate() = (select EOMONTH(getdate()))) begin set @mesActual = (SELECT CONVERT(date, GETDATE())) end ELSe begin set @mesActual = (select EOMONTH(getdate(), -1) ) end set @mes = MONTH(@mesActual); set @anio = YEAR(@mesActual);" +
                "   if (@area != '' and @subArea = '' and @sucursal = '') select Departamento, sum(Cantidad_Suspenciones  ) as Cantidad  from[Personal].[DTM_Personal_Suspensiones]  Where  Area = @area and KeyPersonalSuspenciones in(select max(KeyPersonalSuspenciones) from[Personal].[DTM_Personal_Suspensiones]  where  Area = @area group by Sucursal ,Departamento,Area,SubArea) group by Departamento " +
                "  if (@area != '' and @subArea = '' and @sucursal != '') select Departamento, sum(Cantidad_Suspenciones) as Cantidad  from[Personal].[DTM_Personal_Suspensiones]  Where  Area = @area and Sucursal = @sucursal  and KeyPersonalSuspenciones in(select max(KeyPersonalSuspenciones) from[Personal].[DTM_Personal_Suspensiones]  where  Area = @area and Sucursal = @sucursal group by Sucursal ,Departamento,Area,SubArea) group by Departamento" +
                "  if (@area != '' and @subArea != '' and @sucursal = '') select Departamento, sum(Cantidad_Suspenciones) as Cantidad  from[Personal].[DTM_Personal_Suspensiones] Where  Area = @area and SubArea = @subArea and KeyPersonalSuspenciones in(select max(KeyPersonalSuspenciones) from[Personal].[DTM_Personal_Suspensiones]  where  Area = @area and SubArea = @subArea group by Sucursal ,Departamento,Area,SubArea) group by Departamento" +
                "  if (@area != '' and @subArea != '' and @sucursal != '') select Departamento, sum( Cantidad_Suspenciones) as Cantidad  from[Personal].[DTM_Personal_Suspensiones]  Where  Area = @area and SubArea = @subArea and Sucursal = @sucursal and KeyPersonalSuspenciones in(select max(KeyPersonalSuspenciones) from[Personal].[DTM_Personal_Suspensiones]  where  Area = @area and SubArea = @subArea and Sucursal = @sucursal group by Sucursal ,Departamento,Area,SubArea) group by Departamento " +
                " if (@area = '' and @subArea = '' and @sucursal = '') select Departamento, sum(Cantidad_Suspenciones)  as Cantidad  from[Personal].[DTM_Personal_Suspensiones]  Where KeyPersonalSuspenciones in(select max(KeyPersonalSuspenciones) from[Personal].[DTM_Personal_Suspensiones]   group by Sucursal ,Departamento,Area,SubArea) group by Departamento " +
                "  if (@area = '' and @subArea = '' and @sucursal != '') select Departamento, sum(Cantidad_Suspenciones) as Cantidad  from[Personal].[DTM_Personal_Suspensiones]  Where  Sucursal = @sucursal  and KeyPersonalSuspenciones in(select max(KeyPersonalSuspenciones) from[Personal].[DTM_Personal_Suspensiones]  where  Sucursal = @sucursal group by Sucursal ,Departamento,Area,SubArea) group by Departamento " +
                "  if (@area = '' and @subArea != '' and @sucursal = '') select Departamento, sum(Cantidad_Suspenciones ) as Cantidad  from[Personal].[DTM_Personal_Suspensiones]  Where  SubArea = @subArea and KeyPersonalSuspenciones in(select max(KeyPersonalSuspenciones) from[Personal].[DTM_Personal_Suspensiones]  where  SubArea = @subArea group by Sucursal ,Departamento,Area,SubArea) group by Departamento" +
                "    if (@area = '' and @subArea != '' and @sucursal != '') select Departamento, sum(Cantidad_Suspenciones) as Cantidad  from[Personal].[DTM_Personal_Suspensiones]  Where  SubArea = @subArea and Sucursal = @sucursal and KeyPersonalSuspenciones in(select max(KeyPersonalSuspenciones) from[Personal].[DTM_Personal_Suspensiones]  where  SubArea = @subArea and Sucursal = @sucursal group by Sucursal ,Departamento,Area,SubArea) group by Departamento";

            return consulta;
        }


        #endregion

        

        #region SubRegion_Suspensiones_Evolucion_GeneralMensual

        /* ####### FUNCION PARA GRAFICAR EVOLUCION HISTORICA GENERAL MENSUAL   ##### */

        // ** Listar Sucursal 
        public String Personal_Suspensiones_ListarSucursal_EHGM_SQL(String Anio, String Area, String SubArea, String Departamento, String Mes)
        {
            consulta =
            "   declare @Año varchar(20), @Area nvarchar(max),@SubArea nvarchar(max),@Departamento nvarchar(max),@Mes nvarchar(max);" +
            "   set @Año= '"+Anio+"'; set @Area='"+Area+"';set @SubArea='"+SubArea+"'; set @Departamento= '"+Departamento+"'; set @Mes= '"+Mes+"'; " +
            "  if (@Mes != '' and @Año != '' and @Area != '' and @SubArea != '' and @Departamento != '' )   select distinct Sucursal from[Personal].[DTM_Personal_Suspensiones] where  Departamento = @Departamento and Año = @Año and Area = @Area and SubArea = @SubArea and Mes=@Mes order by Sucursal " +
            "  if (@Mes != '' and @Año != '' and @Area != '' and @SubArea  = '' and @Departamento != '' ) select distinct Sucursal from[Personal].[DTM_Personal_Suspensiones] where  Departamento = @Departamento and Año = @Año and Area = @Area and Mes=@Mes order by Sucursal " +
            "  if (@Mes != '' and @Año != '' and @Area != '' and @SubArea != '' and @Departamento = '')  	select distinct Sucursal from[Personal].[DTM_Personal_Suspensiones] where  Año = @Año and Area = @Area and SubArea = @SubArea  and Mes=@Mes order by Sucursal " +
            "  if (@Mes != '' and @Año != '' and @Area != '' and @SubArea = '' and @Departamento = '')  select distinct Sucursal from[Personal].[DTM_Personal_Suspensiones] where  Año = @Año and Area = @Area and Mes=@Mes order by Sucursal " +
            "  if (@Mes != '' and @Año != '' and @Area = '' and @SubArea = '' and @Departamento = ''  )  	  select distinct Sucursal from[Personal].[DTM_Personal_Suspensiones] where Año = @Año  and Mes=@Mes order by Sucursal " +
            "  if (@Mes != '' and @Año != '' and @Area = '' and @SubArea  != '' and @Departamento != '' )  	select distinct Sucursal from[Personal].[DTM_Personal_Suspensiones] where  Departamento = @Departamento and Año = @Año  and SubArea = @SubArea and Mes=@Mes order by Sucursal " +
            "  if (@Mes != '' and @Año != '' and @Area = '' and @SubArea = '' and @Departamento != '')  	select distinct Sucursal from[Personal].[DTM_Personal_Suspensiones] where  Departamento = @Departamento and Año = @Año and Mes=@Mes order by Sucursal " +
            "  if (@Mes != '' and @Año != '' and @Area = '' and @SubArea  != '' and @Departamento = '' )  	select distinct Sucursal from[Personal].[DTM_Personal_Suspensiones] where  Año = @Año and SubArea = @SubArea and Mes=@Mes order by Sucursal " +
            "  if (@Mes != '' and @Año = '' and @Area != '' and @SubArea != '' and @Departamento != '' )   select distinct Sucursal from[Personal].[DTM_Personal_Suspensiones] where  Departamento = @Departamento and  Area = @Area and SubArea = @SubArea and Mes=@Mes order by Sucursal " +
            "  if (@Mes != '' and @Año = '' and @Area != '' and @SubArea  = '' and @Departamento != '' ) select distinct Sucursal from[Personal].[DTM_Personal_Suspensiones] where  Departamento = @Departamento and  Area = @Area  and Mes=@Mes order by Sucursal " +
            "  if (@Mes != '' and @Año = '' and @Area != '' and @SubArea != '' and @Departamento = '')  	select distinct Sucursal from[Personal].[DTM_Personal_Suspensiones] where   Area = @Area and SubArea = @SubArea and Mes=@Mes order by Sucursal " +
            "  if (@Mes != '' and @Año = '' and @Area != '' and @SubArea = '' and @Departamento = '')  select distinct Sucursal from[Personal].[DTM_Personal_Suspensiones] where   Area = @Area  and Mes=@Mes order by Sucursal " +
            "  if (@Mes != '' and @Año = '' and @Area = '' and @SubArea = '' and @Departamento = ''  )  	  select distinct Sucursal from[Personal].[DTM_Personal_Suspensiones] where  Mes=@Mes order by Sucursal " +
            "  if (@Mes != '' and @Año = '' and @Area = '' and @SubArea  != '' and @Departamento != '' )  	select distinct Sucursal from[Personal].[DTM_Personal_Suspensiones] where  Departamento = @Departamento and SubArea = @SubArea and Mes=@Mes order by Sucursal " +
            "  if (@Mes != '' and @Año = '' and @Area = '' and @SubArea = '' and @Departamento != '')  	select distinct Sucursal from[Personal].[DTM_Personal_Suspensiones] where  Departamento = @Departamento and Mes=@Mes  order by Sucursal " +
            "  if (@Mes != '' and @Año = '' and @Area = '' and @SubArea  != '' and @Departamento = '' )  	select distinct Sucursal from[Personal].[DTM_Personal_Suspensiones] where   SubArea = @SubArea and Mes=@Mes order by Sucursal " +
            "  if (@Mes = '' and @Año != '' and @Area != '' and @SubArea != '' and @Departamento != '' )   select distinct Sucursal from[Personal].[DTM_Personal_Suspensiones] where  Departamento = @Departamento and Año = @Año and Area = @Area and SubArea = @SubArea order by Sucursal " +
            "  if (@Mes = '' and @Año != '' and @Area != '' and @SubArea  = '' and @Departamento != '' ) select distinct Sucursal from[Personal].[DTM_Personal_Suspensiones] where  Departamento = @Departamento and Año = @Año and Area = @Area  order by Sucursal " +
            "  if (@Mes = '' and @Año != '' and @Area != '' and @SubArea != '' and @Departamento = '')  	select distinct Sucursal from[Personal].[DTM_Personal_Suspensiones] where  Año = @Año and Area = @Area and SubArea = @SubArea order by Sucursal " +
            "  if (@Mes = '' and @Año != '' and @Area != '' and @SubArea = '' and @Departamento = '')  select distinct Sucursal from[Personal].[DTM_Personal_Suspensiones] where  Año = @Año and Area = @Area order by Sucursal " +
            "  if (@Mes = '' and @Año != '' and @Area = '' and @SubArea = '' and @Departamento = ''  )  	  select distinct Sucursal from[Personal].[DTM_Personal_Suspensiones] where Año = @Año  order by Sucursal " +
            "  if (@Mes = '' and @Año != '' and @Area = '' and @SubArea  != '' and @Departamento != '' )  	select distinct Sucursal from[Personal].[DTM_Personal_Suspensiones] where  Departamento = @Departamento and Año = @Año  and SubArea = @SubArea order by Sucursal " +
            "  if (@Mes = '' and @Año != '' and @Area = '' and @SubArea = '' and @Departamento != '')  	select distinct Sucursal from[Personal].[DTM_Personal_Suspensiones] where  Departamento = @Departamento and Año = @Año  order by Sucursal " +
            "  if (@Mes = '' and @Año != '' and @Area = '' and @SubArea  != '' and @Departamento = '' )  	select distinct Sucursal from[Personal].[DTM_Personal_Suspensiones] where  Año = @Año and SubArea = @SubArea order by Sucursal " +
            "  if (@Mes = '' and @Año = '' and @Area != '' and @SubArea != '' and @Departamento != '' )   select distinct Sucursal from[Personal].[DTM_Personal_Suspensiones] where  Departamento = @Departamento and  Area = @Area and SubArea = @SubArea order by Sucursal " +
            "  if (@Mes = '' and @Año = '' and @Area != '' and @SubArea  = '' and @Departamento != '' ) select distinct Sucursal from[Personal].[DTM_Personal_Suspensiones] where  Departamento = @Departamento and  Area = @Area  order by Sucursal " +
            "  if (@Mes = '' and @Año = '' and @Area != '' and @SubArea != '' and @Departamento = '')  	select distinct Sucursal from[Personal].[DTM_Personal_Suspensiones] where   Area = @Area and SubArea = @SubArea order by Sucursal " +
            "  if (@Mes = '' and @Año = '' and @Area != '' and @SubArea = '' and @Departamento = '')  select distinct Sucursal from[Personal].[DTM_Personal_Suspensiones] where   Area = @Area order by Sucursal " +
            "  if (@Mes = '' and @Año = '' and @Area = '' and @SubArea = '' and @Departamento = ''  )  	  select distinct Sucursal from[Personal].[DTM_Personal_Suspensiones]  order by Sucursal " +
            "  if (@Mes = '' and @Año = '' and @Area = '' and @SubArea  != '' and @Departamento != '' )  	select distinct Sucursal from[Personal].[DTM_Personal_Suspensiones] where  Departamento = @Departamento and SubArea = @SubArea order by Sucursal " +
            "  if (@Mes = '' and @Año = '' and @Area = '' and @SubArea = '' and @Departamento != '')  	select distinct Sucursal from[Personal].[DTM_Personal_Suspensiones] where  Departamento = @Departamento   order by Sucursal " +
            "  if (@Mes = '' and @Año = '' and @Area = '' and @SubArea  != '' and @Departamento = '' )  	select distinct Sucursal from[Personal].[DTM_Personal_Suspensiones] where   SubArea = @SubArea order by Sucursal " ;

            return consulta;

        }

        // ** Listar Departamento
        public String Personal_Suspensiones_ListarDepartamento_EHGM_SQL(String Anio, String Area, String SubArea, String Sucursal, String Mes)
        {

            consulta =
                " declare @Año varchar(20), @Area nvarchar(max),@SubArea nvarchar(max),@Sucursal nvarchar(max),@Mes nvarchar(max);" +
                "  set @Año= '"+Anio+"'; set @Area='"+Area+"';set @SubArea='"+SubArea+"'; set @Sucursal= '"+Sucursal+"'; set @Mes= '"+Mes+"'; " +
                "  if (@Mes != '' and @Año != '' and @Area != '' and @SubArea != '' and @Sucursal != '' )   select distinct Departamento from[Personal].[DTM_Personal_Suspensiones] where  Sucursal = @Sucursal and Año = @Año and Area = @Area and SubArea = @SubArea and Mes=@Mes order by Departamento " +
                "  if (@Mes != '' and @Año != '' and @Area != '' and @SubArea  = '' and @Sucursal != '' ) select distinct Departamento from[Personal].[DTM_Personal_Suspensiones] where  Sucursal = @Sucursal and Año = @Año and Area = @Area and Mes=@Mes order by Departamento " +
                "  if (@Mes != '' and @Año != '' and @Area != '' and @SubArea != '' and @Sucursal = '')  	select distinct Departamento from[Personal].[DTM_Personal_Suspensiones] where  Año = @Año and Area = @Area and SubArea = @SubArea  and Mes=@Mes order by Departamento " +
                "   if (@Mes != '' and @Año != '' and @Area != '' and @SubArea = '' and @Sucursal = '')  select distinct Departamento from[Personal].[DTM_Personal_Suspensiones] where  Año = @Año and Area = @Area and Mes=@Mes order by Departamento " +
                "  if (@Mes != '' and @Año != '' and @Area = '' and @SubArea = '' and @Sucursal = ''  )  	  select distinct Departamento from[Personal].[DTM_Personal_Suspensiones] where Año = @Año  and Mes=@Mes order by Departamento " +
                "  if (@Mes != '' and @Año != '' and @Area = '' and @SubArea  != '' and @Sucursal != '' )  	select distinct Departamento from[Personal].[DTM_Personal_Suspensiones] where  Sucursal = @Sucursal and Año = @Año  and SubArea = @SubArea and Mes=@Mes order by Departamento " +
                "  if (@Mes != '' and @Año != '' and @Area = '' and @SubArea = '' and @Sucursal != '')  	select distinct Departamento from[Personal].[DTM_Personal_Suspensiones] where  Sucursal = @Sucursal and Año = @Año and Mes=@Mes order by Departamento " +
                "  if (@Mes != '' and @Año != '' and @Area = '' and @SubArea  != '' and @Sucursal = '' )  	select distinct Departamento from[Personal].[DTM_Personal_Suspensiones] where  Año = @Año and SubArea = @SubArea and Mes=@Mes order by Departamento " +
                "  if (@Mes != '' and @Año = '' and @Area != '' and @SubArea != '' and @Sucursal != '' )   select distinct Departamento from[Personal].[DTM_Personal_Suspensiones] where  Sucursal = @Sucursal and  Area = @Area and SubArea = @SubArea and Mes=@Mes order by Departamento " +
                "  if (@Mes != '' and @Año = '' and @Area != '' and @SubArea  = '' and @Sucursal != '' ) select distinct Departamento from[Personal].[DTM_Personal_Suspensiones] where  Sucursal = @Sucursal and  Area = @Area  and Mes=@Mes order by Departamento " +
                "  if (@Mes != '' and @Año = '' and @Area != '' and @SubArea != '' and @Sucursal = '')  	select distinct Departamento from[Personal].[DTM_Personal_Suspensiones] where   Area = @Area and SubArea = @SubArea and Mes=@Mes order by Departamento " +
                "  if (@Mes != '' and @Año = '' and @Area != '' and @SubArea = '' and @Sucursal = '')  select distinct Departamento from[Personal].[DTM_Personal_Suspensiones] where   Area = @Area  and Mes=@Mes order by Departamento " +
                "  if (@Mes != '' and @Año = '' and @Area = '' and @SubArea = '' and @Sucursal = ''  )  	  select distinct Departamento from[Personal].[DTM_Personal_Suspensiones] where  Mes=@Mes order by Departamento " +
                "  if (@Mes != '' and @Año = '' and @Area = '' and @SubArea  != '' and @Sucursal != '' )  	select distinct Departamento from[Personal].[DTM_Personal_Suspensiones] where  Sucursal = @Sucursal and SubArea = @SubArea and Mes=@Mes order by Departamento " +
                "  if (@Mes != '' and @Año = '' and @Area = '' and @SubArea = '' and @Sucursal != '')  	select distinct Departamento from[Personal].[DTM_Personal_Suspensiones] where  Sucursal = @Sucursal and Mes=@Mes  order by Departamento " +
                "  if (@Mes != '' and @Año = '' and @Area = '' and @SubArea  != '' and @Sucursal = '' )  	select distinct Departamento from[Personal].[DTM_Personal_Suspensiones] where   SubArea = @SubArea and Mes=@Mes order by Departamento " +
                "  if (@Mes = '' and @Año != '' and @Area != '' and @SubArea != '' and @Sucursal != '' )   select distinct Departamento from[Personal].[DTM_Personal_Suspensiones] where  Sucursal = @Sucursal and Año = @Año and Area = @Area and SubArea = @SubArea order by Departamento " +
                "  if (@Mes = '' and @Año != '' and @Area != '' and @SubArea  = '' and @Sucursal != '' ) select distinct Departamento from[Personal].[DTM_Personal_Suspensiones] where  Sucursal = @Sucursal and Año = @Año and Area = @Area  order by Departamento " +
                "  if (@Mes = '' and @Año != '' and @Area != '' and @SubArea != '' and @Sucursal = '')  	select distinct Departamento from[Personal].[DTM_Personal_Suspensiones] where  Año = @Año and Area = @Area and SubArea = @SubArea order by Departamento " +
                "  if (@Mes = '' and @Año != '' and @Area != '' and @SubArea = '' and @Sucursal = '')  select distinct Departamento from[Personal].[DTM_Personal_Suspensiones] where  Año = @Año and Area = @Area order by Departamento " +
                "  if (@Mes = '' and @Año != '' and @Area = '' and @SubArea = '' and @Sucursal = ''  )  	  select distinct Departamento from[Personal].[DTM_Personal_Suspensiones] where Año = @Año  order by Departamento " +
                "  if (@Mes = '' and @Año != '' and @Area = '' and @SubArea  != '' and @Sucursal != '' )  	select distinct Departamento from[Personal].[DTM_Personal_Suspensiones] where  Sucursal = @Sucursal and Año = @Año  and SubArea = @SubArea order by Departamento " +
                "  if (@Mes = '' and @Año != '' and @Area = '' and @SubArea = '' and @Sucursal != '')  	select distinct Departamento from[Personal].[DTM_Personal_Suspensiones] where  Sucursal = @Sucursal and Año = @Año  order by Departamento " +
                "  if (@Mes = '' and @Año != '' and @Area = '' and @SubArea  != '' and @Sucursal = '' )  	select distinct Departamento from[Personal].[DTM_Personal_Suspensiones] where  Año = @Año and SubArea = @SubArea order by Departamento " +
                "  if (@Mes = '' and @Año = '' and @Area != '' and @SubArea != '' and @Sucursal != '' )   select distinct Departamento from[Personal].[DTM_Personal_Suspensiones] where  Sucursal = @Sucursal and  Area = @Area and SubArea = @SubArea order by Departamento " +
                "  if (@Mes = '' and @Año = '' and @Area != '' and @SubArea  = '' and @Sucursal != '' ) select distinct Departamento from[Personal].[DTM_Personal_Suspensiones] where  Sucursal = @Sucursal and  Area = @Area  order by Departamento " +
                "  if (@Mes = '' and @Año = '' and @Area != '' and @SubArea != '' and @Sucursal = '')  	select distinct Departamento from[Personal].[DTM_Personal_Suspensiones] where   Area = @Area and SubArea = @SubArea order by Departamento " +
                "  if (@Mes = '' and @Año = '' and @Area != '' and @SubArea = '' and @Sucursal = '')  select distinct Departamento from[Personal].[DTM_Personal_Suspensiones] where   Area = @Area order by Departamento " +
                "  if (@Mes = '' and @Año = '' and @Area = '' and @SubArea = '' and @Sucursal = ''  )  	  select distinct Departamento from[Personal].[DTM_Personal_Suspensiones]  order by Departamento " +
                "  if (@Mes = '' and @Año = '' and @Area = '' and @SubArea  != '' and @Sucursal != '' )  	select distinct Departamento from[Personal].[DTM_Personal_Suspensiones] where  Sucursal = @Sucursal and SubArea = @SubArea order by Departamento " +
                "  if (@Mes = '' and @Año = '' and @Area = '' and @SubArea = '' and @Sucursal != '')  	select distinct Departamento from[Personal].[DTM_Personal_Suspensiones] where  Sucursal = @Sucursal   order by Departamento " +
                "  if (@Mes = '' and @Año = '' and @Area = '' and @SubArea  != '' and @Sucursal = '' )  	select distinct Departamento from[Personal].[DTM_Personal_Suspensiones] where   SubArea = @SubArea order by Departamento ";
                            

            return consulta;

        }

        // ** Listar Area
        public String Personal_Suspensiones_ListarArea_EHGM_SQL(String Anio, String SubArea, String Sucursal, String Departamento,String Mes)
        {
            consulta =
            "  declare @Año varchar(20), @SubArea nvarchar(max),@Sucursal nvarchar(max),@Departamento nvarchar(max),@Mes nvarchar(max);" +
            "  set @Año= '"+Anio+"'; set @SubArea='"+SubArea+" ';set @Sucursal=' "+Sucursal+"'; set @Departamento= '"+Departamento+"'; set @Mes= '"+Mes+" '; " +
            " if (@Mes != '' and @Año != '' and @SubArea != '' and @Sucursal != '' and @Departamento != '' )   select distinct Area from[Personal].[DTM_Personal_Suspensiones] where  Departamento = @Departamento and Año = @Año and SubArea = @SubArea and Sucursal = @Sucursal and Mes=@Mes order by Area  "+  
            "  if (@Mes != '' and @Año != '' and @SubArea != '' and @Sucursal  = '' and @Departamento != '' ) select distinct Area from[Personal].[DTM_Personal_Suspensiones] where  Departamento = @Departamento and Año = @Año and SubArea = @SubArea and Mes=@Mes order by Area "+
            "  if (@Mes != '' and @Año != '' and @SubArea != '' and @Sucursal != '' and @Departamento = '')  	select distinct Area from[Personal].[DTM_Personal_Suspensiones] where  Año = @Año and SubArea = @SubArea and Sucursal = @Sucursal  and Mes=@Mes order by Area "+
            "  if (@Mes != '' and @Año != '' and @SubArea != '' and @Sucursal = '' and @Departamento = '')  select distinct Area from[Personal].[DTM_Personal_Suspensiones] where  Año = @Año and SubArea = @SubArea and Mes=@Mes order by Area "+
            "  if (@Mes != '' and @Año != '' and @SubArea = '' and @Sucursal = '' and @Departamento = ''  )  	  select distinct Area from[Personal].[DTM_Personal_Suspensiones] where Año = @Año  and Mes=@Mes order by Area "+
            "  if (@Mes != '' and @Año != '' and @SubArea = '' and @Sucursal  != '' and @Departamento != '' )  	select distinct Area from[Personal].[DTM_Personal_Suspensiones] where  Departamento = @Departamento and Año = @Año  and Sucursal = @Sucursal and Mes=@Mes order by Area "+
            "  if (@Mes != '' and @Año != '' and @SubArea = '' and @Sucursal = '' and @Departamento != '')  	select distinct Area from[Personal].[DTM_Personal_Suspensiones] where  Departamento = @Departamento and Año = @Año and Mes=@Mes order by Area "+
            "  if (@Mes != '' and @Año != '' and @SubArea = '' and @Sucursal  != '' and @Departamento = '' )  	select distinct Area from[Personal].[DTM_Personal_Suspensiones] where  Año = @Año and Sucursal = @Sucursal and Mes=@Mes order by Area "+
            "  if (@Mes != '' and @Año = '' and @SubArea != '' and @Sucursal != '' and @Departamento != '' )   select distinct Area from[Personal].[DTM_Personal_Suspensiones] where  Departamento = @Departamento and  SubArea = @SubArea and Sucursal = @Sucursal and Mes=@Mes order by Area "+
            "  if (@Mes != '' and @Año = '' and @SubArea != '' and @Sucursal  = '' and @Departamento != '' ) select distinct Area from[Personal].[DTM_Personal_Suspensiones] where  Departamento = @Departamento and  SubArea = @SubArea  and Mes=@Mes order by Area "+
            "  if (@Mes != '' and @Año = '' and @SubArea != '' and @Sucursal != '' and @Departamento = '')  	select distinct Area from[Personal].[DTM_Personal_Suspensiones] where   SubArea = @SubArea and Sucursal = @Sucursal and Mes=@Mes order by Area "+
            "  if (@Mes != '' and @Año = '' and @SubArea != '' and @Sucursal = '' and @Departamento = '')  select distinct Area from[Personal].[DTM_Personal_Suspensiones] where   SubArea = @SubArea  and Mes=@Mes order by Area "+
            "  if (@Mes != '' and @Año = '' and @SubArea = '' and @Sucursal = '' and @Departamento = ''  )  	  select distinct Area from[Personal].[DTM_Personal_Suspensiones] where  Mes=@Mes order by Area "+
            "  if (@Mes != '' and @Año = '' and @SubArea = '' and @Sucursal  != '' and @Departamento != '' )  	select distinct Area from[Personal].[DTM_Personal_Suspensiones] where  Departamento = @Departamento and Sucursal = @Sucursal and Mes=@Mes order by Area "+
            "  if (@Mes != '' and @Año = '' and @SubArea = '' and @Sucursal = '' and @Departamento != '')  	select distinct Area from[Personal].[DTM_Personal_Suspensiones] where  Departamento = @Departamento and Mes=@Mes  order by Area "+
            "  if (@Mes != '' and @Año = '' and @SubArea = '' and @Sucursal  != '' and @Departamento = '' )  	select distinct Area from[Personal].[DTM_Personal_Suspensiones] where   Sucursal = @Sucursal and Mes=@Mes order by Area "+
            "  if (@Mes = '' and @Año != '' and @SubArea != '' and @Sucursal != '' and @Departamento != '' )   select distinct Area from[Personal].[DTM_Personal_Suspensiones] where  Departamento = @Departamento and Año = @Año and SubArea = @SubArea and Sucursal = @Sucursal order by Area "+
            "  if (@Mes = '' and @Año != '' and @SubArea != '' and @Sucursal  = '' and @Departamento != '' ) select distinct Area from[Personal].[DTM_Personal_Suspensiones] where  Departamento = @Departamento and Año = @Año and SubArea = @SubArea  order by Area "+
            "  if (@Mes = '' and @Año != '' and @SubArea != '' and @Sucursal != '' and @Departamento = '')  	select distinct Area from[Personal].[DTM_Personal_Suspensiones] where  Año = @Año and SubArea = @SubArea and Sucursal = @Sucursal order by Area "+
            "  if (@Mes = '' and @Año != '' and @SubArea != '' and @Sucursal = '' and @Departamento = '')  select distinct Area from[Personal].[DTM_Personal_Suspensiones] where  Año = @Año and SubArea = @SubArea order by Area "+
            "  if (@Mes = '' and @Año != '' and @SubArea = '' and @Sucursal = '' and @Departamento = ''  )  	  select distinct Area from[Personal].[DTM_Personal_Suspensiones] where Año = @Año  order by Area "+
            "  if (@Mes = '' and @Año != '' and @SubArea = '' and @Sucursal  != '' and @Departamento != '' )  	select distinct Area from[Personal].[DTM_Personal_Suspensiones] where  Departamento = @Departamento and Año = @Año  and Sucursal = @Sucursal order by Area "+
            "  if (@Mes = '' and @Año != '' and @SubArea = '' and @Sucursal = '' and @Departamento != '')  	select distinct Area from[Personal].[DTM_Personal_Suspensiones] where  Departamento = @Departamento and Año = @Año  order by Area "+
            "  if (@Mes = '' and @Año != '' and @SubArea = '' and @Sucursal  != '' and @Departamento = '' )  	select distinct Area from[Personal].[DTM_Personal_Suspensiones] where  Año = @Año and Sucursal = @Sucursal order by Area "+
            "  if (@Mes = '' and @Año = '' and @SubArea != '' and @Sucursal != '' and @Departamento != '' )   select distinct Area from[Personal].[DTM_Personal_Suspensiones] where  Departamento = @Departamento and  SubArea = @SubArea and Sucursal = @Sucursal order by Area "+
            "  if (@Mes = '' and @Año = '' and @SubArea != '' and @Sucursal  = '' and @Departamento != '' ) select distinct Area from[Personal].[DTM_Personal_Suspensiones] where  Departamento = @Departamento and  SubArea = @SubArea  order by Area "+
            "  if (@Mes = '' and @Año = '' and @SubArea != '' and @Sucursal != '' and @Departamento = '')  	select distinct Area from[Personal].[DTM_Personal_Suspensiones] where   SubArea = @SubArea and Sucursal = @Sucursal order by Area "+
            "  if (@Mes = '' and @Año = '' and @SubArea != '' and @Sucursal = '' and @Departamento = '')  select distinct Area from[Personal].[DTM_Personal_Suspensiones] where   SubArea = @SubArea order by Area "+
            "  if (@Mes = '' and @Año = '' and @SubArea = '' and @Sucursal = '' and @Departamento = ''  )  	  select distinct Area from[Personal].[DTM_Personal_Suspensiones]  order by Area "+
            "  if (@Mes = '' and @Año = '' and @SubArea = '' and @Sucursal  != '' and @Departamento != '' )  	select distinct Area from[Personal].[DTM_Personal_Suspensiones] where  Departamento = @Departamento and Sucursal = @Sucursal order by Area "+
            "  if (@Mes = '' and @Año = '' and @SubArea = '' and @Sucursal = '' and @Departamento != '')  	select distinct Area from[Personal].[DTM_Personal_Suspensiones] where  Departamento = @Departamento   order by Area "+
            "  if (@Mes = '' and @Año = '' and @SubArea = '' and @Sucursal  != '' and @Departamento = '' )  	select distinct Area from[Personal].[DTM_Personal_Suspensiones] where   Sucursal = @Sucursal order by Area ";
            
            return consulta;
        }

        // ** Listar SubArea
        public String Personal_Suspensiones_ListarSubArea_EHGM_SQL(String Departamento, String Area, String Sucursal, String Anio, String Mes)
        {
            consulta =
             "  declare @Año varchar(20), @Area nvarchar(max),@Sucursal nvarchar(max),@Departamento nvarchar(max),@Mes nvarchar(max); "+
            "  set @Año= '"+Anio+"'; set @Area='" +Area+ "'; set @Sucursal='" +Sucursal+ "'; set @Departamento= ' "+Departamento+ " '; set @Mes= '"+Mes+" ';"+
            "  if (@Mes != '' and @Año != '' and @Area != '' and @Sucursal != '' and @Departamento != '' )   select distinct SubArea from[Personal].[DTM_Personal_Suspensiones] where  Departamento = @Departamento and Año = @Año and Area = @Area and Sucursal = @Sucursal and Mes=@Mes order by SubArea "+
            "  if (@Mes != '' and @Año != '' and @Area != '' and @Sucursal  = '' and @Departamento != '' ) select distinct SubArea from[Personal].[DTM_Personal_Suspensiones] where  Departamento = @Departamento and Año = @Año and Area = @Area and Mes=@Mes order by SubArea "+
            "  if (@Mes != '' and @Año != '' and @Area != '' and @Sucursal != '' and @Departamento = '')  	select distinct SubArea from[Personal].[DTM_Personal_Suspensiones] where  Año = @Año and Area = @Area and Sucursal = @Sucursal  and Mes=@Mes order by SubArea "+
            "  if (@Mes != '' and @Año != '' and @Area != '' and @Sucursal = '' and @Departamento = '')  select distinct SubArea from[Personal].[DTM_Personal_Suspensiones] where  Año = @Año and Area = @Area and Mes=@Mes order by SubArea "+
            "  if (@Mes != '' and @Año != '' and @Area = '' and @Sucursal = '' and @Departamento = ''  )  	  select distinct SubArea from[Personal].[DTM_Personal_Suspensiones] where Año = @Año  and Mes=@Mes order by SubArea "+
            "  if (@Mes != '' and @Año != '' and @Area = '' and @Sucursal  != '' and @Departamento != '' )  	select distinct SubArea from[Personal].[DTM_Personal_Suspensiones] where  Departamento = @Departamento and Año = @Año  and Sucursal = @Sucursal and Mes=@Mes order by SubArea "+
            "  if (@Mes != '' and @Año != '' and @Area = '' and @Sucursal = '' and @Departamento != '')  	select distinct SubArea from[Personal].[DTM_Personal_Suspensiones] where  Departamento = @Departamento and Año = @Año and Mes=@Mes order by SubArea "+
            "  if (@Mes != '' and @Año != '' and @Area = '' and @Sucursal  != '' and @Departamento = '' )  	select distinct SubArea from[Personal].[DTM_Personal_Suspensiones] where  Año = @Año and Sucursal = @Sucursal and Mes=@Mes order by SubArea "+
            "  if (@Mes != '' and @Año = '' and @Area != '' and @Sucursal != '' and @Departamento != '' )   select distinct SubArea from[Personal].[DTM_Personal_Suspensiones] where  Departamento = @Departamento and  Area = @Area and Sucursal = @Sucursal and Mes=@Mes order by SubArea "+
            "  if (@Mes != '' and @Año = '' and @Area != '' and @Sucursal  = '' and @Departamento != '' ) select distinct SubArea from[Personal].[DTM_Personal_Suspensiones] where  Departamento = @Departamento and  Area = @Area  and Mes=@Mes order by SubArea "+
            "  if (@Mes != '' and @Año = '' and @Area != '' and @Sucursal != '' and @Departamento = '')  	select distinct SubArea from[Personal].[DTM_Personal_Suspensiones] where   Area = @Area and Sucursal = @Sucursal and Mes=@Mes order by SubArea "+
            "  if (@Mes != '' and @Año = '' and @Area != '' and @Sucursal = '' and @Departamento = '')  select distinct SubArea from[Personal].[DTM_Personal_Suspensiones] where   Area = @Area  and Mes=@Mes order by SubArea "+
            "  if (@Mes != '' and @Año = '' and @Area = '' and @Sucursal = '' and @Departamento = ''  )  	  select distinct SubArea from[Personal].[DTM_Personal_Suspensiones] where  Mes=@Mes order by SubArea "+
            "   if (@Mes != '' and @Año = '' and @Area = '' and @Sucursal  != '' and @Departamento != '' )  	select distinct SubArea from[Personal].[DTM_Personal_Suspensiones] where  Departamento = @Departamento and Sucursal = @Sucursal and Mes=@Mes order by SubArea "+
            "  if (@Mes != '' and @Año = '' and @Area = '' and @Sucursal = '' and @Departamento != '')  	select distinct SubArea from[Personal].[DTM_Personal_Suspensiones] where  Departamento = @Departamento and Mes=@Mes  order by SubArea "+
            "  if (@Mes != '' and @Año = '' and @Area = '' and @Sucursal  != '' and @Departamento = '' )  	select distinct SubArea from[Personal].[DTM_Personal_Suspensiones] where   Sucursal = @Sucursal and Mes=@Mes order by SubArea "+
            "  if (@Mes = '' and @Año != '' and @Area != '' and @Sucursal != '' and @Departamento != '' )   select distinct SubArea from[Personal].[DTM_Personal_Suspensiones] where  Departamento = @Departamento and Año = @Año and Area = @Area and Sucursal = @Sucursal order by SubArea "+
            "  if (@Mes = '' and @Año != '' and @Area != '' and @Sucursal  = '' and @Departamento != '' ) select distinct SubArea from[Personal].[DTM_Personal_Suspensiones] where  Departamento = @Departamento and Año = @Año and Area = @Area  order by SubArea "+
            "  if (@Mes = '' and @Año != '' and @Area != '' and @Sucursal != '' and @Departamento = '')  	select distinct SubArea from[Personal].[DTM_Personal_Suspensiones] where  Año = @Año and Area = @Area and Sucursal = @Sucursal order by SubArea "+
            "  if (@Mes = '' and @Año != '' and @Area != '' and @Sucursal = '' and @Departamento = '')  select distinct SubArea from[Personal].[DTM_Personal_Suspensiones] where  Año = @Año and Area = @Area order by SubArea "+
            "  if (@Mes = '' and @Año != '' and @Area = '' and @Sucursal = '' and @Departamento = ''  )  	  select distinct SubArea from[Personal].[DTM_Personal_Suspensiones] where Año = @Año  order by SubArea "+
            "  if (@Mes = '' and @Año != '' and @Area = '' and @Sucursal  != '' and @Departamento != '' )  	select distinct SubArea from[Personal].[DTM_Personal_Suspensiones] where  Departamento = @Departamento and Año = @Año  and Sucursal = @Sucursal order by SubArea "+
            "  if (@Mes = '' and @Año != '' and @Area = '' and @Sucursal = '' and @Departamento != '')  	select distinct SubArea from[Personal].[DTM_Personal_Suspensiones] where  Departamento = @Departamento and Año = @Año  order by SubArea "+
            "  if (@Mes = '' and @Año != '' and @Area = '' and @Sucursal  != '' and @Departamento = '' )  	select distinct SubArea from[Personal].[DTM_Personal_Suspensiones] where  Año = @Año and Sucursal = @Sucursal order by SubArea "+
            "  if (@Mes = '' and @Año = '' and @Area != '' and @Sucursal != '' and @Departamento != '' )   select distinct SubArea from[Personal].[DTM_Personal_Suspensiones] where  Departamento = @Departamento and  Area = @Area and Sucursal = @Sucursal order by SubArea "+
            "  if (@Mes = '' and @Año = '' and @Area != '' and @Sucursal  = '' and @Departamento != '' ) select distinct SubArea from[Personal].[DTM_Personal_Suspensiones] where  Departamento = @Departamento and  Area = @Area  order by SubArea "+
        "  if (@Mes = '' and @Año = '' and @Area != '' and @Sucursal != '' and @Departamento = '')  	select distinct SubArea from[Personal].[DTM_Personal_Suspensiones] where   Area = @Area and Sucursal = @Sucursal order by SubArea "+
            "  if (@Mes = '' and @Año = '' and @Area != '' and @Sucursal = '' and @Departamento = '')  select distinct SubArea from[Personal].[DTM_Personal_Suspensiones] where   Area = @Area order by SubArea "+
            "  if (@Mes = '' and @Año = '' and @Area = '' and @Sucursal = '' and @Departamento = ''  )  	  select distinct SubArea from[Personal].[DTM_Personal_Suspensiones]  order by SubArea "+
            "  if (@Mes = '' and @Año = '' and @Area = '' and @Sucursal  != '' and @Departamento != '' )  	select distinct SubArea from[Personal].[DTM_Personal_Suspensiones] where  Departamento = @Departamento and Sucursal = @Sucursal order by SubArea "+
            "  if (@Mes = '' and @Año = '' and @Area = '' and @Sucursal = '' and @Departamento != '')  	select distinct SubArea from[Personal].[DTM_Personal_Suspensiones] where  Departamento = @Departamento   order by SubArea "+
            "  if (@Mes = '' and @Año = '' and @Area = '' and @Sucursal  != '' and @Departamento = '' )  	select distinct SubArea from[Personal].[DTM_Personal_Suspensiones] where   Sucursal = @Sucursal order by SubArea ";

            return consulta;
        }

        // ** Listar Año
         public String Personal_Suspensiones_ListarAño_EHGM_SQL(String Departamento, String SubArea, String Area,String Sucursal, String Mes )
        {

            consulta=
                "  declare @Departamento varchar(20), @Area nvarchar(max),@SubArea nvarchar(max),@Sucursal nvarchar(max),@Mes nvarchar(max);" +
                "  set @Departamento= ' "+Departamento+"'; set @Area='"+Area+"';set @SubArea='"+SubArea+"'; set @Sucursal= '"+Sucursal+" '; set @Mes= '"+Mes+"'; " +
                "  if (@Mes != '' and @Departamento != '' and @Area != '' and @SubArea != '' and @Sucursal != '' )   select distinct Año as Anio from[Personal].[DTM_Personal_Suspensiones] where  Sucursal = @Sucursal and Departamento = @Departamento and Area = @Area and SubArea = @SubArea and Mes=@Mes order by Año " +
                "  if (@Mes != '' and @Departamento != '' and @Area != '' and @SubArea  = '' and @Sucursal != '' ) select distinct Año as Anio from[Personal].[DTM_Personal_Suspensiones] where  Sucursal = @Sucursal and Departamento = @Departamento and Area = @Area and Mes=@Mes order by Año " +
                "  if (@Mes != '' and @Departamento != '' and @Area != '' and @SubArea != '' and @Sucursal = '')  	select distinct Año as Anio from[Personal].[DTM_Personal_Suspensiones] where  Departamento = @Departamento and Area = @Area and SubArea = @SubArea  and Mes=@Mes order by Año " +
                "  if (@Mes != '' and @Departamento != '' and @Area != '' and @SubArea = '' and @Sucursal = '')  select distinct Año as Anio from[Personal].[DTM_Personal_Suspensiones] where  Departamento = @Departamento and Area = @Area and Mes=@Mes order by Año " +
                "  if (@Mes != '' and @Departamento != '' and @Area = '' and @SubArea = '' and @Sucursal = ''  )  	  select distinct Año as Anio from[Personal].[DTM_Personal_Suspensiones] where Departamento = @Departamento  and Mes=@Mes order by Año " +
                "  if (@Mes != '' and @Departamento != '' and @Area = '' and @SubArea  != '' and @Sucursal != '' )  	select distinct Año as Anio from[Personal].[DTM_Personal_Suspensiones] where  Sucursal = @Sucursal and Departamento = @Departamento  and SubArea = @SubArea and Mes=@Mes order by Año " +
                "  if (@Mes != '' and @Departamento != '' and @Area = '' and @SubArea = '' and @Sucursal != '')  	select distinct Año as Anio from[Personal].[DTM_Personal_Suspensiones] where  Sucursal = @Sucursal and Departamento = @Departamento and Mes=@Mes order by Año " +
                "  if (@Mes != '' and @Departamento != '' and @Area = '' and @SubArea  != '' and @Sucursal = '' )  	select distinct Año as Anio from[Personal].[DTM_Personal_Suspensiones] where  Departamento = @Departamento and SubArea = @SubArea and Mes=@Mes order by Año " +
                "  if (@Mes != '' and @Departamento = '' and @Area != '' and @SubArea != '' and @Sucursal != '' )   select distinct Año as Anio from[Personal].[DTM_Personal_Suspensiones] where  Sucursal = @Sucursal and  Area = @Area and SubArea = @SubArea and Mes=@Mes order by Año " +
                "  if (@Mes != '' and @Departamento = '' and @Area != '' and @SubArea  = '' and @Sucursal != '' ) select distinct Año as Anio from[Personal].[DTM_Personal_Suspensiones] where  Sucursal = @Sucursal and  Area = @Area  and Mes=@Mes order by Año " +
                "  if (@Mes != '' and @Departamento = '' and @Area != '' and @SubArea != '' and @Sucursal = '')  	select distinct Año as Anio from[Personal].[DTM_Personal_Suspensiones] where   Area = @Area and SubArea = @SubArea and Mes=@Mes order by Año " +
                "  if (@Mes != '' and @Departamento = '' and @Area != '' and @SubArea = '' and @Sucursal = '')  select distinct Año as Anio from[Personal].[DTM_Personal_Suspensiones] where   Area = @Area  and Mes=@Mes order by Año " +
                "  if (@Mes != '' and @Departamento = '' and @Area = '' and @SubArea = '' and @Sucursal = ''  )  	  select distinct Año as Anio from[Personal].[DTM_Personal_Suspensiones] where  Mes=@Mes order by Año " +
                "  if (@Mes != '' and @Departamento = '' and @Area = '' and @SubArea  != '' and @Sucursal != '' )  	select distinct Año as Anio from[Personal].[DTM_Personal_Suspensiones] where  Sucursal = @Sucursal and SubArea = @SubArea and Mes=@Mes order by Año " +
                "  if (@Mes != '' and @Departamento = '' and @Area = '' and @SubArea = '' and @Sucursal != '')  	select distinct Año as Anio from[Personal].[DTM_Personal_Suspensiones] where  Sucursal = @Sucursal and Mes=@Mes  order by Año " +
                "  if (@Mes != '' and @Departamento = '' and @Area = '' and @SubArea  != '' and @Sucursal = '' )  	select distinct Año as Anio from[Personal].[DTM_Personal_Suspensiones] where   SubArea = @SubArea and Mes=@Mes order by Año " +
                "  if (@Mes = '' and @Departamento != '' and @Area != '' and @SubArea != '' and @Sucursal != '' )   select distinct Año as Anio from[Personal].[DTM_Personal_Suspensiones] where  Sucursal = @Sucursal and Departamento = @Departamento and Area = @Area and SubArea = @SubArea order by Año " +
                "  if (@Mes = '' and @Departamento != '' and @Area != '' and @SubArea  = '' and @Sucursal != '' ) select distinct Año as Anio from[Personal].[DTM_Personal_Suspensiones] where  Sucursal = @Sucursal and Departamento = @Departamento and Area = @Area  order by Año " +
                "  if (@Mes = '' and @Departamento != '' and @Area != '' and @SubArea != '' and @Sucursal = '')  	select distinct Año as Anio from[Personal].[DTM_Personal_Suspensiones] where  Departamento = @Departamento and Area = @Area and SubArea = @SubArea order by Año " +
                "  if (@Mes = '' and @Departamento != '' and @Area != '' and @SubArea = '' and @Sucursal = '')  select distinct Año as Anio from[Personal].[DTM_Personal_Suspensiones] where  Departamento = @Departamento and Area = @Area order by Año " +
                "  if (@Mes = '' and @Departamento != '' and @Area = '' and @SubArea = '' and @Sucursal = ''  )  	  select distinct Año as Anio from[Personal].[DTM_Personal_Suspensiones] where Departamento = @Departamento  order by Año " +
                "  if (@Mes = '' and @Departamento != '' and @Area = '' and @SubArea  != '' and @Sucursal != '' )  	select distinct Año as Anio from[Personal].[DTM_Personal_Suspensiones] where  Sucursal = @Sucursal and Departamento = @Departamento  and SubArea = @SubArea order by Año " +
                "  if (@Mes = '' and @Departamento != '' and @Area = '' and @SubArea = '' and @Sucursal != '')  	select distinct Año as Anio from[Personal].[DTM_Personal_Suspensiones] where  Sucursal = @Sucursal and Departamento = @Departamento  order by Año" +
                "  if (@Mes = '' and @Departamento != '' and @Area = '' and @SubArea  != '' and @Sucursal = '' )  	select distinct Año as Anio from[Personal].[DTM_Personal_Suspensiones] where  Departamento = @Departamento and SubArea = @SubArea order by Año " +
                "  if (@Mes = '' and @Departamento = '' and @Area != '' and @SubArea != '' and @Sucursal != '' )   select distinct Año as Anio from[Personal].[DTM_Personal_Suspensiones] where  Sucursal = @Sucursal and  Area = @Area and SubArea = @SubArea order by Año " +
                "  if (@Mes = '' and @Departamento = '' and @Area != '' and @SubArea  = '' and @Sucursal != '' ) select distinct Año as Anio from[Personal].[DTM_Personal_Suspensiones] where  Sucursal = @Sucursal and  Area = @Area  order by Año " +
                "  if (@Mes = '' and @Departamento = '' and @Area != '' and @SubArea != '' and @Sucursal = '')  	select distinct Año as Anio from[Personal].[DTM_Personal_Suspensiones] where   Area = @Area and SubArea = @SubArea order by Año " +
                "  if (@Mes = '' and @Departamento = '' and @Area != '' and @SubArea = '' and @Sucursal = '')  select distinct Año as Anio from[Personal].[DTM_Personal_Suspensiones] where   Area = @Area order by Año " +
                "  if (@Mes = '' and @Departamento = '' and @Area = '' and @SubArea = '' and @Sucursal = ''  )  	  select distinct Año as Anio from[Personal].[DTM_Personal_Suspensiones]  order by Año " +
                "  if (@Mes = '' and @Departamento = '' and @Area = '' and @SubArea  != '' and @Sucursal != '' )  	select distinct Año as Anio from[Personal].[DTM_Personal_Suspensiones] where  Sucursal = @Sucursal and SubArea = @SubArea order by Año " +
                "  if (@Mes = '' and @Departamento = '' and @Area = '' and @SubArea = '' and @Sucursal != '')  	select distinct Año as Anio from[Personal].[DTM_Personal_Suspensiones] where  Sucursal = @Sucursal   order by Año " +
                " if (@Mes = '' and @Departamento = '' and @Area = '' and @SubArea  != '' and @Sucursal = '' )  	select distinct Año as Anio from[Personal].[DTM_Personal_Suspensiones] where   SubArea = @SubArea order by Año";

                return consulta;
        }

        // ** Listar Mes
         public String Personal_Suspensiones_ListarMes_EHGM_SQL(String Departamento, String SubArea, String Area,String Sucursal, String Año )
        {
            consulta=
                "  declare @Departamento varchar(20), @Area nvarchar(max),@SubArea nvarchar(max),@Sucursal nvarchar(max),@Año nvarchar(max); "+
                "  set @Departamento= '"+Departamento+"'; set @Area='"+Area+"';set @SubArea='"+SubArea+"'; set @Sucursal= '"+Sucursal+"'; set @Año= '"+Año+"'; "+
                "  if (@Año != '' and @Departamento != '' and @Area != '' and @SubArea != '' and @Sucursal != '' )   select distinct Mes from[Personal].[DTM_Personal_Colaboradores] where  Sucursal = @Sucursal and Departamento = @Departamento and Area = @Area and SubArea = @SubArea and Anual=@Año order by Mes "+
                "  if (@Año != '' and @Departamento != '' and @Area != '' and @SubArea  = '' and @Sucursal != '' ) select distinct Mes from[Personal].[DTM_Personal_Colaboradores] where  Sucursal = @Sucursal and Departamento = @Departamento and Area = @Area and Anual=@Año order by Mes "+
                "  if (@Año != '' and @Departamento != '' and @Area != '' and @SubArea != '' and @Sucursal = '')  	select distinct Mes from[Personal].[DTM_Personal_Colaboradores] where  Departamento = @Departamento and Area = @Area and SubArea = @SubArea  and Anual=@Año order by Mes "+
                "  if (@Año != '' and @Departamento != '' and @Area != '' and @SubArea = '' and @Sucursal = '')  select distinct Mes from[Personal].[DTM_Personal_Colaboradores] where  Departamento = @Departamento and Area = @Area and Anual=@Año order by Mes "+
                "  if (@Año != '' and @Departamento != '' and @Area = '' and @SubArea = '' and @Sucursal = ''  )  	  select distinct Mes from[Personal].[DTM_Personal_Colaboradores] where Departamento = @Departamento  and Anual=@Año order by Mes "+
                "  if (@Año != '' and @Departamento != '' and @Area = '' and @SubArea  != '' and @Sucursal != '' )  	select distinct Mes from[Personal].[DTM_Personal_Colaboradores] where  Sucursal = @Sucursal and Departamento = @Departamento  and SubArea = @SubArea and Anual=@Año order by Mes "+
                "  if (@Año != '' and @Departamento != '' and @Area = '' and @SubArea = '' and @Sucursal != '')  	select distinct Mes from[Personal].[DTM_Personal_Colaboradores] where  Sucursal = @Sucursal and Departamento = @Departamento and Anual=@Año order by Mes "+
                "  if (@Año != '' and @Departamento != '' and @Area = '' and @SubArea  != '' and @Sucursal = '' )  	select distinct Mes from[Personal].[DTM_Personal_Colaboradores] where  Departamento = @Departamento and SubArea = @SubArea and Anual=@Año order by Mes "+
                "  if (@Año != '' and @Departamento = '' and @Area != '' and @SubArea != '' and @Sucursal != '' )   select distinct Mes from[Personal].[DTM_Personal_Colaboradores] where  Sucursal = @Sucursal and  Area = @Area and SubArea = @SubArea and Anual=@Año order by Mes "+
                "  if (@Año != '' and @Departamento = '' and @Area != '' and @SubArea  = '' and @Sucursal != '' ) select distinct Mes from[Personal].[DTM_Personal_Colaboradores] where  Sucursal = @Sucursal and  Area = @Area  and Anual=@Año order by Mes "+
                "  if (@Año != '' and @Departamento = '' and @Area != '' and @SubArea != '' and @Sucursal = '')  	select distinct Mes from[Personal].[DTM_Personal_Colaboradores] where   Area = @Area and SubArea = @SubArea and Anual=@Año order by Mes "+
                "  if (@Año != '' and @Departamento = '' and @Area != '' and @SubArea = '' and @Sucursal = '')  select distinct Mes from[Personal].[DTM_Personal_Colaboradores] where   Area = @Area  and Anual=@Año order by Mes "+
                "  if (@Año != '' and @Departamento = '' and @Area = '' and @SubArea = '' and @Sucursal = ''  )  	  select distinct Mes from[Personal].[DTM_Personal_Colaboradores] where  Anual=@Año order by Mes "+
                "  if (@Año != '' and @Departamento = '' and @Area = '' and @SubArea  != '' and @Sucursal != '' )  	select distinct Mes from[Personal].[DTM_Personal_Colaboradores] where  Sucursal = @Sucursal and SubArea = @SubArea and Anual=@Año order by Mes "+
                "  if (@Año != '' and @Departamento = '' and @Area = '' and @SubArea = '' and @Sucursal != '')  	select distinct Mes from[Personal].[DTM_Personal_Colaboradores] where  Sucursal = @Sucursal and Anual=@Año  order by Mes "+
                "  if (@Año != '' and @Departamento = '' and @Area = '' and @SubArea  != '' and @Sucursal = '' )  	select distinct Mes from[Personal].[DTM_Personal_Colaboradores] where   SubArea = @SubArea and Anual=@Año order by Mes "+
                "  if (@Año = '' and @Departamento != '' and @Area != '' and @SubArea != '' and @Sucursal != '' )   select distinct Mes from[Personal].[DTM_Personal_Colaboradores] where  Sucursal = @Sucursal and Departamento = @Departamento and Area = @Area and SubArea = @SubArea order by Mes "+
                "  if (@Año = '' and @Departamento != '' and @Area != '' and @SubArea  = '' and @Sucursal != '' ) select distinct Mes from[Personal].[DTM_Personal_Colaboradores] where  Sucursal = @Sucursal and Departamento = @Departamento and Area = @Area  order by Mes "+
                "  if (@Año = '' and @Departamento != '' and @Area != '' and @SubArea != '' and @Sucursal = '')  	select distinct Mes from[Personal].[DTM_Personal_Colaboradores] where  Departamento = @Departamento and Area = @Area and SubArea = @SubArea order by Mes "+
                "  if (@Año = '' and @Departamento != '' and @Area != '' and @SubArea = '' and @Sucursal = '')  select distinct Mes from[Personal].[DTM_Personal_Colaboradores] where  Departamento = @Departamento and Area = @Area order by Mes "+
                "  if (@Año = '' and @Departamento != '' and @Area = '' and @SubArea = '' and @Sucursal = ''  )  	  select distinct Mes from[Personal].[DTM_Personal_Colaboradores] where Departamento = @Departamento  order by Mes "+
                "  if (@Año = '' and @Departamento != '' and @Area = '' and @SubArea  != '' and @Sucursal != '' )  	select distinct Mes from[Personal].[DTM_Personal_Colaboradores] where  Sucursal = @Sucursal and Departamento = @Departamento  and SubArea = @SubArea order by Mes "+
                "  if (@Año = '' and @Departamento != '' and @Area = '' and @SubArea = '' and @Sucursal != '')  	select distinct Mes from[Personal].[DTM_Personal_Colaboradores] where  Sucursal = @Sucursal and Departamento = @Departamento  order by Mes "+
                "  if (@Año = '' and @Departamento != '' and @Area = '' and @SubArea  != '' and @Sucursal = '' )  	select distinct Mes from[Personal].[DTM_Personal_Colaboradores] where  Departamento = @Departamento and SubArea = @SubArea order by Mes "+
                "  if (@Año = '' and @Departamento = '' and @Area != '' and @SubArea != '' and @Sucursal != '' )   select distinct Mes from[Personal].[DTM_Personal_Colaboradores] where  Sucursal = @Sucursal and  Area = @Area and SubArea = @SubArea order by Mes "+
                "  if (@Año = '' and @Departamento = '' and @Area != '' and @SubArea  = '' and @Sucursal != '' ) select distinct Mes from[Personal].[DTM_Personal_Colaboradores] where  Sucursal = @Sucursal and  Area = @Area  order by Mes "+
                "  if (@Año = '' and @Departamento = '' and @Area != '' and @SubArea != '' and @Sucursal = '')  	select distinct Mes from[Personal].[DTM_Personal_Colaboradores] where   Area = @Area and SubArea = @SubArea order by Mes "+
                "  if (@Año = '' and @Departamento = '' and @Area != '' and @SubArea = '' and @Sucursal = '')  select distinct Mes from[Personal].[DTM_Personal_Colaboradores] where   Area = @Area order by Mes "+
                "  if (@Año = '' and @Departamento = '' and @Area = '' and @SubArea = '' and @Sucursal = ''  )  	  select distinct Mes from[Personal].[DTM_Personal_Colaboradores]  order by Mes "+
                "  if (@Año = '' and @Departamento = '' and @Area = '' and @SubArea  != '' and @Sucursal != '' )  	select distinct Mes from[Personal].[DTM_Personal_Colaboradores] where  Sucursal = @Sucursal and SubArea = @SubArea order by Mes "+
                "  if (@Año = '' and @Departamento = '' and @Area = '' and @SubArea = '' and @Sucursal != '')  	select distinct Mes from[Personal].[DTM_Personal_Colaboradores] where  Sucursal = @Sucursal   order by Mes "+
                "  if (@Año = '' and @Departamento = '' and @Area = '' and @SubArea  != '' and @Sucursal = '' )  	select distinct Mes from[Personal].[DTM_Personal_Colaboradores] where   SubArea = @SubArea order by Mes ";



            return consulta;

        }


        //  GRAFICAR EVOLUCION HISTORICA GENERAL MENSUAL */
      

        #endregion


    


    }
}
