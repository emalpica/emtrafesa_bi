﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ENTIDAD.Base.Area
{
    public class EncomiendaSQL
    {
        String consulta;

        #region Venta por Sucursal
        //GIAMPIERE 22/ 08 / 2019
        //General Mensual
        public String G_Encomienda_VentaSucursal_GeneralMensual_SQL(String mes, String tipo)
        {
            consulta =
        " declare  @Tipo nvarchar(max), @Mes nvarchar(max),@query  AS NVARCHAR(MAX), @cols NVARCHAR(MAX), @null NVARCHAR(MAX), @filtro NVARCHAR(MAX), @consulta  NVARCHAR(MAX); " +
        " set @Tipo='" + tipo + "';set @Mes= '" + mes + "';set @filtro='';set @consulta= '';" +
        " select @cols = STUFF((SELECT Distinct ',' + QUOTENAME(Anual)  from [Encomienda].[DTM_Encomienda_Ventas_Surcursal] group by ',' + QUOTENAME(Anual) Order by ',' + QUOTENAME(Anual) FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)')  ,1,1,'') " +
        " select @null = STUFF((SELECT Distinct ', isnull(' + QUOTENAME(Anual)+',0) as'  + QUOTENAME(Anual) from [Encomienda].[DTM_Encomienda_Ventas_Surcursal]group by ', isnull(' + QUOTENAME(Anual)+',0) as'  + QUOTENAME(Anual)  Order by ', isnull(' + QUOTENAME(Anual)+',0) as'  + QUOTENAME(Anual)   FOR XML PATH(''), TYPE  ).value('.', 'NVARCHAR(MAX)')   ,1,1,'') " +
        " if (@Mes = '' and @Tipo = ''  ) set @consulta = 'SELECT Meses,' + @null + '  from ( select Mes, DATENAME(MONTH,Fecha) as Meses,Anual,ImporteTotalNeto from [Encomienda].[DTM_Encomienda_Ventas_Surcursal] )  x  pivot ( sum(ImporteTotalNeto) for Anual in (' + @cols + ') ) p  order by Mes '  " +
        " else begin " +
        " if (@Mes != '' and @Tipo != '')set @filtro = ' where  Tipo = '''+@Tipo+''' and Mes = '''+@Mes+'''  ' " +
        " if (@Mes != '' and @Tipo = ''  ) set @filtro = ' where  Mes = '''+@Mes+'''  '" +
        " if (@Mes = '' and @Tipo != '')set @filtro = '  where  Tipo = '''+@Tipo+''' ' " +
        " set @consulta='SELECT Meses,' + @null + '  from ( select Mes, DATENAME(MONTH,Fecha) as Meses,Anual,ImporteTotalNeto from [Encomienda].[DTM_Encomienda_Ventas_Surcursal] '+@filtro+ ' )  x  pivot ( sum(ImporteTotalNeto) for Anual in (' + @cols + ') ) p  order by Mes ' " +
        " end " +
        " execute(@consulta)";


            return consulta;
        }
        //GIAMPIERE 22/ 08 / 2019
        public String Encomienda_VentaSucursal_GeneralMensual_ddlMes_SQL(String tipo)
        {
            consulta =
         " declare  @Tipo nvarchar(max),@Año varchar(max)" +
        " set @Tipo='" + tipo + "'; " +
        " if (@Tipo != '')   select distinct DATENAME(MONTH,Fecha) as Meses ,Mes from [Encomienda].[DTM_Encomienda_Ventas_Surcursal] Where Tipo=@Tipo order by Mes" +
        " if (@Tipo = '' ) select distinct DATENAME(MONTH,Fecha) as Meses ,Mes from [Encomienda].[DTM_Encomienda_Ventas_Surcursal] order by Mes"; ;

            return consulta;
        }
        //GIAMPIERE 22/ 08 / 2019
        public String Encomienda_VentaSucursal_GeneralMensual_ddlTipo_SQL(String mes)
        {
            consulta =
        " declare  @Año nvarchar(max),@Mes varchar(max);" +
        " set @Mes= '" + mes + "';" +
        " if ( @Mes != '') select distinct Tipo from [Encomienda].[DTM_Encomienda_Ventas_Surcursal] Where Mes=@Mes order by Tipo" +
        " if ( @Mes = '') select distinct Tipo from [Encomienda].[DTM_Encomienda_Ventas_Surcursal] order by Tipo";

            return consulta;
        }
        //GIAMPIERE 22/ 08 / 2019
        //General por Origen
        public String G_Encomienda_VentaSucursal_GeneralOrigen_SQL(String anio, String mes, String tipo)
        {
            consulta =
           " declare @Mes nvarchar(max),@Año nvarchar(max),@Tipo nvarchar(max), @query  NVARCHAR(MAX), @cols  NVARCHAR(MAX), @null  NVARCHAR(MAX), @filtro NVARCHAR(MAX), @consulta  NVARCHAR(MAX), @cols2  NVARCHAR(MAX), @null2  NVARCHAR(MAX), @select NVARCHAR(MAX), @from NVARCHAR(MAX) " +
           "  set @Tipo='" + tipo + "';set @Año= '" + anio + "';set @Mes= '" + mes + "';set @filtro='';set @consulta= '';" +
           " select @cols = STUFF((SELECT Distinct ',' + QUOTENAME(Anual)  from  [Encomienda].[DTM_Encomienda_Ventas_Surcursal]  group by ',' + QUOTENAME(Anual)Order by ',' + QUOTENAME(Anual)  FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)')  ,1,1,'')  " +
           " select @null = STUFF((SELECT Distinct ', isnull(' + QUOTENAME(Anual)+',0) as'  + QUOTENAME(Anual) from  [Encomienda].[DTM_Encomienda_Ventas_Surcursal]   group by ', isnull(' + QUOTENAME(Anual)+',0) as'  + QUOTENAME(Anual) Order by ', isnull(' + QUOTENAME(Anual)+',0) as'  + QUOTENAME(Anual)  FOR XML PATH(''), TYPE  ).value('.', 'NVARCHAR(MAX)')   ,1,1,'')  " +
           " select @cols2 = STUFF((SELECT Distinct ',' + QUOTENAME(Anual)  from  [Encomienda].[DTM_Encomienda_Ventas_Surcursal]  where Anual=@Año group by ',' + QUOTENAME(Anual)Order by ',' + QUOTENAME(Anual)  FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)')  ,1,1,'') " +
           " select @null2 = STUFF((SELECT Distinct ', isnull(' + QUOTENAME(Anual)+',0) as'  + QUOTENAME(Anual) from  [Encomienda].[DTM_Encomienda_Ventas_Surcursal]  where Anual=@Año group by ', isnull(' + QUOTENAME(Anual)+',0) as'  + QUOTENAME(Anual) Order by ', isnull(' + QUOTENAME(Anual)+',0) as'  + QUOTENAME(Anual)   FOR XML PATH(''), TYPE  ).value('.', 'NVARCHAR(MAX)')   ,1,1,'') " +
           " select @select= ',(Select sum(ImporteTotalNeto) as total  from [Encomienda].[DTM_Encomienda_Ventas_Surcursal] b where b.Origen=p.Origen   '" +
           " select @from= ' group by Origen) as total   from  ( select  Origen,Anual,ImporteTotalNeto  from [Encomienda].[DTM_Encomienda_Ventas_Surcursal]  ' " +
           " if (@Año!= '' and @Mes = '' and @Tipo = ''  )   set @consulta = 'SELECT top 10 isnull(Origen,0) as Origen,' + @null2 + ' ' + @select + '  and Anual='''+@Año+''' ' + @from + '  )x  pivot (  sum(ImporteTotalNeto) for Anual in (' + @cols2 + ') )  p order by total desc'  " +
           " if (@Año!= '' and @Mes != '' and @Tipo != '' ) set @consulta = 'SELECT top 10 isnull(Origen,0) as Origen,' + @null2 + ' ' + @select + ' and Tipo = '''+@Tipo+'''  and Mes='''+@Mes+'''  and Anual='''+@Año+''' ' + @from + ' where  Tipo = '''+@Tipo+'''  and Mes='''+@Mes+''' )x  pivot (  sum(ImporteTotalNeto) for Anual in (' + @cols + ') )  p order by total desc'   " +
           " if (@Año!= '' and @Mes != '' and @Tipo = '')  set @consulta = 'SELECT top 10 isnull(Origen,0) as Origen,' + @null2 + ' ' + @select + ' and Mes ='''+@Mes+'''  and Anual='''+@Año+''' ' + @from + ' where Mes ='''+@Mes+''' )x  pivot (  sum(ImporteTotalNeto) for Anual in (' + @cols2 + ') )  p order by total desc'   " +
           " if (@Año!= '' and @Mes = '' and @Tipo != '')  set @consulta = 'SELECT top 10 isnull(Origen,0) as Origen,' + @null2 + ' ' + @select + ' and Tipo = '''+@Tipo+'''  and Anual='''+@Año+''' ' + @from + ' where Tipo = '''+@Tipo+'''  )x  pivot (  sum(ImporteTotalNeto) for Anual in (' + @cols2 + ') )  p order by total desc' " +  
           " if (@Año= '' and @Mes != '' and @Tipo != '' ) set @consulta = 'SELECT top 10 isnull(Origen,0) as Origen,' + @null + ' ' + @select + ' and  Tipo = '''+@Tipo+'''  and Mes='''+@Mes+'''  and Anual='''+@Año+''' ' + @from + ' where  Tipo = '''+@Tipo+'''  and Mes='''+@Mes+''' )x  pivot (  sum(ImporteTotalNeto) for Anual in (' + @cols + ') )  p order by total desc'  " +
           " if (@Año= '' and @Mes != '' and @Tipo = '')  set @consulta = 'SELECT top 10 isnull(Origen,0) as Origen,' + @null + ' ' + @select + ' and Mes = '''+@Mes+'''' + @from + ' where Mes = '''+@Mes+''' )x  pivot (  sum(ImporteTotalNeto) for Anual in (' + @cols + ') )  p order by total desc' " +
           " if (@Año= '' and @Mes = '' and @Tipo != '') set @consulta = 'SELECT top 10 isnull(Origen,0) as Origen,' + @null + ' ' + @select + ' and Tipo = '''+@Tipo+'''  ' + @from + ' where Tipo = '''+@Tipo+'''   )x  pivot (  sum(ImporteTotalNeto) for Anual in (' + @cols + ') )  p order by total desc' " +  
           " if (@Año= '' and @Mes = '' and @Tipo = ''  ) set @consulta = 'SELECT top 10 isnull(Origen,0) as Origen,' + @null + ' ' + @select + ' ' + @from + ' ) x  pivot (  sum(ImporteTotalNeto) for Anual in (' + @cols + ') )  p order by total desc' " +
           " execute(@consulta) ";

            return consulta;
        }
        //GIAMPIERE 27/ 08 / 2019
        public String Encomienda_VentaSucursal_GeneralOrigen_AgrupacionTrimestre_SQL(String anio, String tipo)
        {
            consulta =
          " declare  @Mes nvarchar(max),@Año nvarchar(max),@Tipo nvarchar(max)" +
          " set @Tipo='" + tipo + "'; set @Año= '" + anio + "';" +
          "  if (@Año = '' and @Tipo = '' ) select top 10  Origen,Trimestre1=isnull((select  sum(ImporteTotalNeto)/3  from[Encomienda].[DTM_Encomienda_Ventas_Surcursal] b where Mes between'1' and  '3' and a.Origen=b.Origen ),0),Trimestre2=isnull((select  sum(ImporteTotalNeto)/3  from[Encomienda].[DTM_Encomienda_Ventas_Surcursal] b where Mes between'4' and  '6' and a.Origen=b.Origen ),0),Trimestre3=isnull((select  sum(ImporteTotalNeto)/3  from[Encomienda].[DTM_Encomienda_Ventas_Surcursal] b where Mes between'7' and  '9' and a.Origen=b.Origen ),0),Trimestre4=isnull((select  sum(ImporteTotalNeto)/3  from[Encomienda].[DTM_Encomienda_Ventas_Surcursal] b where Mes between'10' and  '12' and a.Origen=b.Origen ),0),(Select sum(ImporteTotalNeto)/3 as total  from [Encomienda].[DTM_Encomienda_Ventas_Surcursal] c where c.Origen=a.Origen )as t from [Encomienda].[DTM_Encomienda_Ventas_Surcursal] a group by Origen order by t desc " +
          "  if (@Año = '' and @Tipo != '') select top 10  Origen,Trimestre1=isnull((select  sum(ImporteTotalNeto)/3  from[Encomienda].[DTM_Encomienda_Ventas_Surcursal] b where Mes between'1' and  '3' and a.Origen=b.Origen and  Tipo=@Tipo ),0),Trimestre2=isnull((select  sum(ImporteTotalNeto)/3  from[Encomienda].[DTM_Encomienda_Ventas_Surcursal] b where Mes between'4' and  '6' and a.Origen=b.Origen and  Tipo=@Tipo),0),Trimestre3=isnull((select  sum(ImporteTotalNeto)/3  from[Encomienda].[DTM_Encomienda_Ventas_Surcursal] b where Mes between'7' and  '9' and a.Origen=b.Origen and  Tipo=@Tipo ),0),Trimestre4=isnull((select  sum(ImporteTotalNeto)/3  from[Encomienda].[DTM_Encomienda_Ventas_Surcursal] b where Mes between'10' and  '12' and a.Origen=b.Origen and  Tipo=@Tipo ),0),(Select sum(ImporteTotalNeto)/3 as total  from [Encomienda].[DTM_Encomienda_Ventas_Surcursal] c where c.Origen=a.Origen and  Tipo=@Tipo )as t from [Encomienda].[DTM_Encomienda_Ventas_Surcursal] a group by Origen order by t desc " +
          "  if (@Año != '' and @Tipo = '' ) select top 10 Origen,Trimestre1=isnull((select  sum(ImporteTotalNeto)/3  from[Encomienda].[DTM_Encomienda_Ventas_Surcursal] b where Mes between'1' and  '3' and a.Origen=b.Origen and  Anual=@Año ),0),Trimestre2=isnull((select  sum(ImporteTotalNeto)/3  from[Encomienda].[DTM_Encomienda_Ventas_Surcursal] b where Mes between'4' and  '6' and a.Origen=b.Origen and  Anual=@Año),0),Trimestre3=isnull((select  sum(ImporteTotalNeto)/3  from[Encomienda].[DTM_Encomienda_Ventas_Surcursal] b where Mes between'7' and  '9' and a.Origen=b.Origen and  Anual=@Año ),0),Trimestre4=isnull((select  sum(ImporteTotalNeto)/3  from[Encomienda].[DTM_Encomienda_Ventas_Surcursal] b where Mes between'10' and  '12' and a.Origen=b.Origen and  Anual=@Año ),0),(Select sum(ImporteTotalNeto)/3 as total  from [Encomienda].[DTM_Encomienda_Ventas_Surcursal] c where c.Origen=a.Origen and Anual=@Año)as t from [Encomienda].[DTM_Encomienda_Ventas_Surcursal] a group by Origen order by t desc " +
          "  if (@Año != '' and @Tipo != '') select top 10 Origen,Trimestre1=isnull((select  sum(ImporteTotalNeto)/3  from[Encomienda].[DTM_Encomienda_Ventas_Surcursal] b where Mes between'1' and  '3' and a.Origen=b.Origen and  Tipo=@Tipo and Anual=@Año  ),0),Trimestre2=isnull((select  sum(ImporteTotalNeto)/3  from[Encomienda].[DTM_Encomienda_Ventas_Surcursal] b where Mes between'4' and  '6' and a.Origen=b.Origen and  Tipo=@Tipo and Anual=@Año ),0),Trimestre3=isnull((select  sum(ImporteTotalNeto)/3  from[Encomienda].[DTM_Encomienda_Ventas_Surcursal] b where Mes between'7' and  '9' and a.Origen=b.Origen and  Tipo=@Tipo and Anual=@Año  ),0),Trimestre4=isnull((select  sum(ImporteTotalNeto)/3  from[Encomienda].[DTM_Encomienda_Ventas_Surcursal] b where Mes between'10' and  '12' and a.Origen=b.Origen and  Tipo=@Tipo and Anual=@Año  ),0),(Select sum(ImporteTotalNeto)/3 as total  from [Encomienda].[DTM_Encomienda_Ventas_Surcursal] c where c.Origen=a.Origen  and  Tipo=@Tipo and Anual=@Año )as t from [Encomienda].[DTM_Encomienda_Ventas_Surcursal] a group by Origen order by t desc ";

            return consulta;
        }
        //GIAMPIERE 27/ 08 / 2019
        public String Encomienda_VentaSucursal_GeneralOrigen_AgrupacionBimestre_SQL(String anio,String tipo)
        {
            consulta =
            " declare  @Año nvarchar(max),@Tipo nvarchar(max)" +
            " set @Tipo='" + tipo + "'; set @Año= '" + anio + "';" +
            "  if (@Año = '' and @Tipo = '' ) select top 10  Origen,Bimestre1=isnull((select  sum(ImporteTotalNeto)/2  from[Encomienda].[DTM_Encomienda_Ventas_Surcursal] b where Mes between'1' and  '2' and a.Origen=b.Origen ),0),Bimestre2=isnull((select  sum(ImporteTotalNeto)/2  from[Encomienda].[DTM_Encomienda_Ventas_Surcursal] b where Mes between'3' and  '4' and a.Origen=b.Origen ),0),Bimestre3=isnull((select  sum(ImporteTotalNeto)/2  from[Encomienda].[DTM_Encomienda_Ventas_Surcursal] b where Mes between'5' and  '6' and a.Origen=b.Origen ),0),Bimestre4=isnull((select  sum(ImporteTotalNeto)/2  from[Encomienda].[DTM_Encomienda_Ventas_Surcursal] b where Mes between'7' and  '8' and a.Origen=b.Origen ),0),Bimestre5=isnull((select  sum(ImporteTotalNeto)/2  from[Encomienda].[DTM_Encomienda_Ventas_Surcursal] b where Mes between'9' and  '10' and a.Origen=b.Origen ),0),Bimestre6=isnull((select  sum(ImporteTotalNeto)/2  from[Encomienda].[DTM_Encomienda_Ventas_Surcursal] b where Mes between'11' and  '12' and a.Origen=b.Origen ),0),(Select sum(ImporteTotalNeto)/2 as total  from [Encomienda].[DTM_Encomienda_Ventas_Surcursal] c where c.Origen=a.Origen )as t from [Encomienda].[DTM_Encomienda_Ventas_Surcursal] a group by Origen order by t desc      " +
            "  if (@Año = '' and @Tipo != '') select top 10  Origen ,Bimestre1=isnull((select  sum(ImporteTotalNeto)/2  from[Encomienda].[DTM_Encomienda_Ventas_Surcursal] b where Mes between'1' and  '2' and a.Origen=b.Origen and  Tipo=@Tipo ),0),Bimestre2=isnull((select  sum(ImporteTotalNeto)/2  from[Encomienda].[DTM_Encomienda_Ventas_Surcursal] b where Mes between'3' and  '4' and a.Origen=b.Origen and  Tipo=@Tipo ),0),Bimestre3=isnull((select  sum(ImporteTotalNeto)/2  from[Encomienda].[DTM_Encomienda_Ventas_Surcursal] b where Mes between'5' and  '6' and a.Origen=b.Origen and  Tipo=@Tipo ),0),Bimestre4=isnull((select  sum(ImporteTotalNeto)/2  from[Encomienda].[DTM_Encomienda_Ventas_Surcursal] b where Mes between'7' and  '8' and a.Origen=b.Origen and  Tipo=@Tipo ),0),Bimestre5=isnull((select  sum(ImporteTotalNeto)/2  from[Encomienda].[DTM_Encomienda_Ventas_Surcursal] b where Mes between'9' and  '10' and a.Origen=b.Origen and  Tipo=@Tipo ),0),Bimestre6=isnull((select  sum(ImporteTotalNeto)/2  from[Encomienda].[DTM_Encomienda_Ventas_Surcursal] b where Mes between'11' and  '12' and a.Origen=b.Origen and  Tipo=@Tipo  ),0),(Select sum(ImporteTotalNeto)/2 as total  from [Encomienda].[DTM_Encomienda_Ventas_Surcursal] c where c.Origen=a.Origen and  Tipo=@Tipo )as t from [Encomienda].[DTM_Encomienda_Ventas_Surcursal] a group by Origen order by t desc " +
            "  if (@Año != '' and @Tipo = '' ) select top 10  Origen ,Bimestre1=isnull((select  sum(ImporteTotalNeto)/2  from[Encomienda].[DTM_Encomienda_Ventas_Surcursal] b where Mes between'1' and  '2' and a.Origen=b.Origen and  Anual=@Año ),0),Bimestre2=isnull((select  sum(ImporteTotalNeto)/2  from[Encomienda].[DTM_Encomienda_Ventas_Surcursal] b where Mes between'3' and  '4' and a.Origen=b.Origen and  Anual=@Año ),0),Bimestre3=isnull((select  sum(ImporteTotalNeto)/2  from[Encomienda].[DTM_Encomienda_Ventas_Surcursal] b where Mes between'5' and  '6' and a.Origen=b.Origen and  Anual=@Año ),0),Bimestre4=isnull((select  sum(ImporteTotalNeto)/2  from[Encomienda].[DTM_Encomienda_Ventas_Surcursal] b where Mes between'7' and  '8' and a.Origen=b.Origen and  Anual=@Año ),0),Bimestre5=isnull((select  sum(ImporteTotalNeto)/2  from[Encomienda].[DTM_Encomienda_Ventas_Surcursal] b where Mes between'9' and  '10' and a.Origen=b.Origen and  Anual=@Año ),0),Bimestre6=isnull((select  sum(ImporteTotalNeto)/2  from[Encomienda].[DTM_Encomienda_Ventas_Surcursal] b where Mes between'11' and  '12' and a.Origen=b.Origen and  Anual=@Año  ),0),(Select sum(ImporteTotalNeto)/2 as total  from [Encomienda].[DTM_Encomienda_Ventas_Surcursal] c where c.Origen=a.Origen and  Anual=@Año )as t from [Encomienda].[DTM_Encomienda_Ventas_Surcursal] a group by Origen order by t desc  " +
            "  if (@Año != '' and @Tipo != '') select top 10  Origen ,Bimestre1=isnull((select  sum(ImporteTotalNeto)/2  from[Encomienda].[DTM_Encomienda_Ventas_Surcursal] b where Mes between'1' and  '2' and a.Origen=b.Origen and  Tipo=@Tipo and Anual=@Año   ),0),Bimestre2=isnull((select  sum(ImporteTotalNeto)/2  from[Encomienda].[DTM_Encomienda_Ventas_Surcursal] b where Mes between'3' and  '4' and a.Origen=b.Origen and  Tipo=@Tipo and Anual=@Año   ),0),Bimestre3=isnull((select  sum(ImporteTotalNeto)/2  from[Encomienda].[DTM_Encomienda_Ventas_Surcursal] b where Mes between'5' and  '6' and a.Origen=b.Origen and  Tipo=@Tipo and Anual=@Año   ),0),Bimestre4=isnull((select  sum(ImporteTotalNeto)/2  from[Encomienda].[DTM_Encomienda_Ventas_Surcursal] b where Mes between'7' and  '8' and a.Origen=b.Origen and  Tipo=@Tipo and Anual=@Año   ),0),Bimestre5=isnull((select  sum(ImporteTotalNeto)/2  from[Encomienda].[DTM_Encomienda_Ventas_Surcursal] b where Mes between'9' and  '10' and a.Origen=b.Origen and  Tipo=@Tipo and Anual=@Año   ),0),Bimestre6=isnull((select  sum(ImporteTotalNeto)/2  from[Encomienda].[DTM_Encomienda_Ventas_Surcursal] b where Mes between'11' and  '12' and a.Origen=b.Origen and  Tipo=@Tipo and Anual=@Año    ),0),(Select sum(ImporteTotalNeto)/2 as total  from [Encomienda].[DTM_Encomienda_Ventas_Surcursal] c where c.Origen=a.Origen and  Tipo=@Tipo and Anual=@Año   )as t from [Encomienda].[DTM_Encomienda_Ventas_Surcursal] a group by Origen order by t desc ";
            return consulta;
        }
        //GIAMPIERE 23/ 08 / 2019
        public String Encomienda_VentaSucursal_GeneralOrigen_ddlMes_SQL(String anio ,String tipo)
        {
            consulta =
         " declare  @Tipo nvarchar(max),@Año varchar(max)" +
            " set @Tipo='" + tipo + "'; set @Año= '" + anio + "';" +
             "  if ( @Año  != '' and @Tipo = '' ) select distinct DATENAME(MONTH,Fecha) as Meses ,Mes from [Encomienda].[DTM_Encomienda_Ventas_Surcursal] Where Anual=@Año order by Mes" +
             "  if ( @Año  != '' and @Tipo != '' ) select distinct DATENAME(MONTH,Fecha) as Meses ,Mes from [Encomienda].[DTM_Encomienda_Ventas_Surcursal] Where Tipo=@Tipo and Anual=@Año order by Mes" +
             "  if ( @Año = '' and @Tipo != '')   select distinct DATENAME(MONTH,Fecha) as Meses ,Mes from [Encomienda].[DTM_Encomienda_Ventas_Surcursal] Where Tipo=@Tipo order by Mes" +
             "  if ( @Año = '' and @Tipo = ''  ) select distinct DATENAME(MONTH,Fecha) as Meses ,Mes from [Encomienda].[DTM_Encomienda_Ventas_Surcursal] order by Mes"; ;


            return consulta;
        }
        //GIAMPIERE 23/ 08 / 2019
        public String Encomienda_VentaSucursal_GeneralOrigen_ddlAño_SQL(String mes, String tipo)
        {
            consulta =
            "  declare  @Tipo nvarchar(max),@Mes varchar(max)" +
              " set @Tipo='" + tipo + "';set @Mes= '" + mes + "';" +
             "  if ( @Mes  != '' and @Tipo = '' ) select distinct Anual as Anio from [Encomienda].[DTM_Encomienda_Ventas_Surcursal] Where Mes=@Mes order by Anual" +
             "  if ( @Mes  != '' and @Tipo != '' ) select distinct Anual as Anio from [Encomienda].[DTM_Encomienda_Ventas_Surcursal] Where Tipo=@Tipo and Mes=@Mes order by Anual" +
             "  if ( @Mes = '' and @Tipo != '')   select distinct Anual as Anio from [Encomienda].[DTM_Encomienda_Ventas_Surcursal] Where Tipo=@Tipo order by Anual" +
             "  if ( @Mes = '' and @Tipo = ''  ) select distinct Anual as Anio from [Encomienda].[DTM_Encomienda_Ventas_Surcursal]  order by Anual";
            return consulta;
        }
        //GIAMPIERE 23/ 08 / 2019
        public String Encomienda_VentaSucursal_GeneralOrigen_ddlTipo_SQL(String anio,String mes)
        {
            consulta =
              " declare  @Año nvarchar(max),@Mes varchar(max);" +
              " set @Año= '" + anio + "';set @Mes= '" + mes + "';" +
              " if ( @Mes  != '' and @Año = '' ) select distinct Tipo from [Encomienda].[DTM_Encomienda_Ventas_Surcursal] Where Mes=@Mes order by Tipo" +
              " if ( @Mes  != '' and @Año != '' ) select distinct Tipo from [Encomienda].[DTM_Encomienda_Ventas_Surcursal] Where Anual=@Año and Mes=@Mes order by Tipo" +
              " if ( @Mes = '' and @Año != '')   select distinct Tipo from [Encomienda].[DTM_Encomienda_Ventas_Surcursal] Where Anual=@Año order by Tipo" +
              " if ( @Mes = '' and @Año = ''  ) select distinct Tipo from [Encomienda].[DTM_Encomienda_Ventas_Surcursal] order by Tipo";


            return consulta;
        }
        //GIAMPIERE 26/ 08 / 2019
        //General Mensual por Origen
        public String G_Encomienda_VentaSucursal_GeneralMensualOrigen_SQL(String anio, String mes, String tipo)
        {
            consulta =
             " declare @Departamento varchar(max), @Mes nvarchar(max),@Año nvarchar(max),@Tipo nvarchar(max), @query  AS NVARCHAR(MAX), @cols AS NVARCHAR(MAX), @null  AS NVARCHAR(MAX), @cols2 AS NVARCHAR(MAX), @null2  AS NVARCHAR(MAX)" +
            " set @Tipo='" + tipo + "'; set @Año= '" + anio + "';set @Mes= '" + mes + "';" +
            " select @cols = STUFF((SELECT Distinct top 10 ',' + QUOTENAME(Origen)  from [Encomienda].[DTM_Encomienda_Ventas_Surcursal] group by ',' + QUOTENAME(Origen)Order by ',' + QUOTENAME(Origen) desc FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)')  ,1,1,'')  " +
            " select @null = STUFF((SELECT Distinct top 10', isnull(' + QUOTENAME(Origen)+',0) as'  + QUOTENAME(Origen) from [Encomienda].[DTM_Encomienda_Ventas_Surcursal]  group by ', isnull(' + QUOTENAME(Origen)+',0) as'  + QUOTENAME(Origen) Order by ', isnull(' + QUOTENAME(Origen)+',0) as'  + QUOTENAME(Origen) desc  FOR XML PATH(''), TYPE  ).value('.', 'NVARCHAR(MAX)')   ,1,1,'') " +
            " if ( @Mes != '' and @Año = '' and @Tipo != '' )  	 set @query = 'SELECT concat(Meses,'' de '',Anual) as MesAño,' + @null + '  from  ( select Mes,DATENAME(MONTH,Fecha) as Meses,Anual,Origen,ImporteTotalNeto from [Encomienda].[DTM_Encomienda_Ventas_Surcursal] where  Tipo = '''+@Tipo+''' and Mes='''+@Mes+'''  ) x  pivot ( sum(ImporteTotalNeto) for Origen in (' + @cols + ') ) p  order by Anual' " +
            " if ( @Mes != '' and @Año = '' and @Tipo = '')  	 set @query = 'SELECT concat(Meses,'' de '',Anual) as MesAño,' + @null + '  from  ( select Mes,DATENAME(MONTH,Fecha) as Meses,Anual,Origen,ImporteTotalNeto from [Encomienda].[DTM_Encomienda_Ventas_Surcursal] where Mes = '''+@Mes+''' ) x  pivot ( sum(ImporteTotalNeto) for Origen in (' + @cols + ') ) p order by Anual' " +
            " if ( @Mes = '' and @Año = '' and @Tipo = ''  )  	  set @query = 'SELECT concat(Meses,'' de '',Anual) as MesAño,' + @null + '  from  ( select Mes,DATENAME(MONTH,Fecha) as Meses,Anual,Origen,ImporteTotalNeto from [Encomienda].[DTM_Encomienda_Ventas_Surcursal] ) x  pivot ( sum(ImporteTotalNeto) for Origen in (' + @cols + ') ) p order by Anual' " +
            " if ( @Mes = '' and @Año = '' and @Tipo != '')  	 set @query = 'SELECT concat(Meses,'' de '',Anual) as MesAño,' + @null + '  from  ( select Mes,DATENAME(MONTH,Fecha) as Meses,Anual,Origen,ImporteTotalNeto from [Encomienda].[DTM_Encomienda_Ventas_Surcursal] where Tipo = '''+@Tipo+''' ) x  pivot ( sum(ImporteTotalNeto) for Origen in (' + @cols + ') ) p order by Anual' " +
            " if ( @Mes != '' and @Año != '' and @Tipo != '' )  	 set @query = 'SELECT concat(Meses,'' de '',Anual) as MesAño,' + @null + '  from  ( select Mes, DATENAME(MONTH,Fecha) as Meses,Anual,Origen,ImporteTotalNeto from [Encomienda].[DTM_Encomienda_Ventas_Surcursal] where  Tipo = '''+@Tipo+''' and Anual= '''+@Año+''' and Mes='''+@Mes+''' ) x  pivot ( sum(ImporteTotalNeto) for Origen in (' + @cols + ') ) p order by Anual' " +
            " if ( @Mes != '' and @Año != '' and @Tipo = '')  	 set @query = 'SELECT concat(Meses,'' de '',Anual) as MesAño,' + @null + '  from  ( select Mes, DATENAME(MONTH,Fecha) as Meses,Anual,Origen,ImporteTotalNeto from [Encomienda].[DTM_Encomienda_Ventas_Surcursal] where Mes = '''+@Mes+''' and Anual= '''+@Año+''' ) x  pivot ( sum(ImporteTotalNeto) for Origen in (' + @cols + ') ) p order by Anual' " +
            " if ( @Mes = '' and @Año != '' and @Tipo = ''  )  	  set @query = 'SELECT concat(Meses,'' de '',Anual) as MesAño,' + @null + '  from  ( select Mes, DATENAME(MONTH,Fecha) as Meses,Anual,Origen,ImporteTotalNeto from [Encomienda].[DTM_Encomienda_Ventas_Surcursal] where  Anual= '''+@Año+'''  ) x  pivot ( sum(ImporteTotalNeto) for Origen in (' + @cols + ') ) p order by Mes' " +
            " if ( @Mes = '' and @Año != '' and @Tipo != '')  	 set @query = 'SELECT concat(Meses,'' de '',Anual) as MesAño,' + @null + '  from  ( select  Mes, DATENAME(MONTH,Fecha) as Meses,Anual,Origen,ImporteTotalNeto from [Encomienda].[DTM_Encomienda_Ventas_Surcursal] where Tipo = '''+@Tipo+'''and Anual= '''+@Año+'''  ) x  pivot ( sum(ImporteTotalNeto) for Origen in (' + @cols + ') ) p order by Mes ' " +
             " execute(@query)";
            return consulta;
        }
        //GIAMPIERE 26/ 08 / 2019
        public String Encomienda_VentaSucursal_GeneralMensualOrigen_ddlMes_SQL(String anio, String tipo)
        {
            consulta =
              "  declare  @Tipo nvarchar(max),@Año varchar(max)" +
              " set @Tipo='" + tipo + "'; set @Año= '" + anio + "';" +
             "  if ( @Año  != '' and @Tipo = '' ) select distinct DATENAME(MONTH,Fecha) as Meses ,Mes from [Encomienda].[DTM_Encomienda_Ventas_Surcursal] Where Anual=@Año order by Mes" +
             "  if ( @Año  != '' and @Tipo != '' ) select distinct DATENAME(MONTH,Fecha) as Meses ,Mes from [Encomienda].[DTM_Encomienda_Ventas_Surcursal] Where Tipo=@Tipo and Anual=@Año order by Mes" +
             "  if ( @Año = '' and @Tipo != '')   select distinct DATENAME(MONTH,Fecha) as Meses ,Mes from [Encomienda].[DTM_Encomienda_Ventas_Surcursal] Where Tipo=@Tipo order by Mes" +
             "  if ( @Año = '' and @Tipo = ''  ) select distinct DATENAME(MONTH,Fecha) as Meses ,Mes from [Encomienda].[DTM_Encomienda_Ventas_Surcursal] order by Mes"; ;

            return consulta;
        }
        //GIAMPIERE 26/ 08 / 2019
        public String Encomienda_VentaSucursal_GeneralMensualOrigen_ddlAño_SQL(String mes, String tipo)
        {
            consulta =
            "  declare  @Tipo nvarchar(max),@Mes varchar(max)" +
              " set @Tipo='" + tipo + "';set @Mes= '" + mes + "';" +
             "  if ( @Mes  != '' and @Tipo = '' ) select distinct Anual as Anio from [Encomienda].[DTM_Encomienda_Ventas_Surcursal] Where Mes=@Mes order by Anual" +
             "  if ( @Mes  != '' and @Tipo != '' ) select distinct Anual as Anio from [Encomienda].[DTM_Encomienda_Ventas_Surcursal] Where Tipo=@Tipo and Mes=@Mes order by Anual" +
             "  if ( @Mes = '' and @Tipo != '')   select distinct Anual as Anio from [Encomienda].[DTM_Encomienda_Ventas_Surcursal] Where Tipo=@Tipo order by Anual" +
             "  if ( @Mes = '' and @Tipo = ''  ) select distinct Anual as Anio from [Encomienda].[DTM_Encomienda_Ventas_Surcursal]  order by Anual";
            return consulta;
        }
        //GIAMPIERE 26/ 08 / 2019
        public String Encomienda_VentaSucursal_GeneralMensualOrigen_ddlTipo_SQL(String mes, String anio)
        {
            consulta =
              " declare  @Año nvarchar(max),@Mes varchar(max);" +
              " set @Año= '" + anio + "';set @Mes= '" + mes + "';" +
              " if ( @Mes  != '' and @Año = '' ) select distinct Tipo from [Encomienda].[DTM_Encomienda_Ventas_Surcursal] Where Mes=@Mes order by Tipo" +
              " if ( @Mes  != '' and @Año != '' ) select distinct Tipo from [Encomienda].[DTM_Encomienda_Ventas_Surcursal] Where Anual=@Año and Mes=@Mes order by Tipo" +
              " if ( @Mes = '' and @Año != '')   select distinct Tipo from [Encomienda].[DTM_Encomienda_Ventas_Surcursal] Where Anual=@Año order by Tipo" +
              " if ( @Mes = '' and @Año = ''  ) select distinct Tipo from [Encomienda].[DTM_Encomienda_Ventas_Surcursal] order by Tipo";
            return consulta;
        }
        #endregion

        #region Clientes Top
        //GIAMPIERE 10/ 09 / 2019
        //Cantidad Encomiendas
        public String G_Encomienda_ClientesTop_CantidaEncomiendas_SQL(String anio, String mes, String operacion)
        {
            consulta =
       " declare @Mes nvarchar(max),@Año nvarchar(max),@Operacion nvarchar(max), @query  NVARCHAR(MAX), @cols  NVARCHAR(MAX), @null  NVARCHAR(MAX), @filtro NVARCHAR(MAX), @consulta  NVARCHAR(MAX), @cols2  NVARCHAR(MAX), @null2  NVARCHAR(MAX), @select NVARCHAR(MAX), @from NVARCHAR(MAX) " +
       " set @Operacion='" + operacion + "';set @Año= '" + anio + "';set @Mes= '" + mes + "';set @filtro='';set @consulta= '';" +
       " select @cols = STUFF((SELECT Distinct ',' + QUOTENAME(Anual)  from  [Encomienda].[DTM_Encomienda_Cliente_Top]  group by ',' + QUOTENAME(Anual)Order by ',' + QUOTENAME(Anual) desc FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)')  ,1,1,'') " +
       " select @null = STUFF((SELECT Distinct ', isnull(' + QUOTENAME(Anual)+',0) as'  + QUOTENAME(Anual) from  [Encomienda].[DTM_Encomienda_Cliente_Top]   group by ', isnull(' + QUOTENAME(Anual)+',0) as'  + QUOTENAME(Anual) Order by ', isnull(' + QUOTENAME(Anual)+',0) as'  + QUOTENAME(Anual) desc  FOR XML PATH(''), TYPE  ).value('.', 'NVARCHAR(MAX)')   ,1,1,'')  " +
       " select @cols2 = STUFF((SELECT Distinct ',' + QUOTENAME(Anual)  from  [Encomienda].[DTM_Encomienda_Cliente_Top]  where Anual=@Año group by ',' + QUOTENAME(Anual)Order by ',' + QUOTENAME(Anual) desc FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)')  ,1,1,'') " +
       " select @null2 = STUFF((SELECT Distinct ', isnull(' + QUOTENAME(Anual)+',0) as'  + QUOTENAME(Anual) from  [Encomienda].[DTM_Encomienda_Cliente_Top]  where Anual=@Año group by ', isnull(' + QUOTENAME(Anual)+',0) as'  + QUOTENAME(Anual) Order by ', isnull(' + QUOTENAME(Anual)+',0) as'  + QUOTENAME(Anual) desc  FOR XML PATH(''), TYPE  ).value('.', 'NVARCHAR(MAX)')   ,1,1,'')" +
       " select @select= ',(Select sum(CantidadOperaciones) as total  from [Encomienda].[DTM_Encomienda_Cliente_Top] b where b.Cliente=p.Cliente   '" +
       " select @from= ' group by Cliente) as total   from  ( select  Cliente,Anual,CantidadOperaciones  from [Encomienda].[DTM_Encomienda_Cliente_Top]  '" +
       " if (@Año!= '' and @Mes = '' and @Operacion = ''  )   set @consulta = 'SELECT top 10 isnull(Cliente,0) as Cliente,' + @null2 + ' ' + @select + '  and Anual='''+@Año+''' ' + @from + '  )x  pivot (  sum(CantidadOperaciones) for Anual in (' + @cols2 + ') )  p order by total desc' " +
       " if (@Año!= '' and @Mes != '' and @Operacion != '' ) set @consulta = 'SELECT top 10 isnull(Cliente,0) as Cliente,' + @null2 + ' ' + @select + ' and Operacion = '''+@Operacion+'''  and Mes='''+@Mes+'''  and Anual='''+@Año+''' ' + @from + ' where  Operacion = '''+@Operacion+'''  and Mes='''+@Mes+''' )x  pivot (  sum(CantidadOperaciones) for Anual in (' + @cols + ') )  p order by total desc'  " +
       " if (@Año!= '' and @Mes != '' and @Operacion = '')  set @consulta = 'SELECT top 10 isnull(Cliente,0) as Cliente,' + @null2 + ' ' + @select + ' and Mes ='''+@Mes+'''  and Anual='''+@Año+''' ' + @from + ' where Mes ='''+@Mes+''' )x  pivot (  sum(CantidadOperaciones) for Anual in (' + @cols2 + ') )  p order by total desc'  " +
       " if (@Año!= '' and @Mes = '' and @Operacion != '')  set @consulta = 'SELECT top 10 isnull(Cliente,0) as Cliente,' + @null2 + ' ' + @select + ' and Operacion = '''+@Operacion+'''  and Anual='''+@Año+''' ' + @from + ' where Operacion = '''+@Operacion+'''  )x  pivot (  sum(CantidadOperaciones) for Anual in (' + @cols2 + ') )  p order by total desc'  " +
       " if (@Año= '' and @Mes != '' and @Operacion != '' ) set @consulta = 'SELECT top 10 isnull(Cliente,0) as Cliente,' + @null + ' ' + @select + ' and  Operacion = '''+@Operacion+'''  and Mes='''+@Mes+'''  and Anual='''+@Año+''' ' + @from + ' where  Operacion = '''+@Operacion+'''  and Mes='''+@Mes+''' )x  pivot (  sum(CantidadOperaciones) for Anual in (' + @cols + ') )  p order by total desc' " +
       " if (@Año= '' and @Mes != '' and @Operacion = '')  set @consulta = 'SELECT top 10 isnull(Cliente,0) as Cliente,' + @null + ' ' + @select + ' and Mes = '''+@Mes+'''' + @from + ' where Mes = '''+@Mes+''' )x  pivot (  sum(CantidadOperaciones) for Anual in (' + @cols + ') )  p order by total desc' " +
       " if (@Año= '' and @Mes = '' and @Operacion != '') set @consulta = 'SELECT top 10 isnull(Cliente,0) as Cliente,' + @null + ' ' + @select + ' and Operacion = '''+@Operacion+'''  ' + @from + ' where Operacion = '''+@Operacion+'''   )x  pivot (  sum(CantidadOperaciones) for Anual in (' + @cols + ') )  p order by total desc'  " +
       " if (@Año= '' and @Mes = '' and @Operacion = ''  ) set @consulta = 'SELECT top 10 isnull(Cliente,0) as Cliente,' + @null + ' ' + @select + ' ' + @from + ' ) x  pivot (  sum(CantidadOperaciones) for Anual in (' + @cols + ') )  p order by total desc' " +
       " execute(@consulta)";
            return consulta;
        }
        //GIAMPIERE 10/ 09 / 2019
        public String Encomienda_ClientesTop_CantidaEncomiendas_AgrupacionTrimestre_SQL(String anio, String operacion)
        {
            consulta =
            " declare  @Mes nvarchar(max),@Año nvarchar(max),@Operacion nvarchar(max)" +
            " set @Operacion='" + operacion + "'; set @Año= '" + anio + "';" +
           " if (@Año = '' and @Operacion = '' ) select top 10  Cliente,Trimestre1=isnull((select  sum(CantidadOperaciones)/3  from[Encomienda].[DTM_Encomienda_Cliente_Top] b where Mes between'1' and  '3' and a.Cliente=b.Cliente ),0),Trimestre2=isnull((select  sum(CantidadOperaciones)/3  from[Encomienda].[DTM_Encomienda_Cliente_Top] b where Mes between'4' and  '6' and a.Cliente=b.Cliente ),0),Trimestre3=isnull((select  sum(CantidadOperaciones)/3  from[Encomienda].[DTM_Encomienda_Cliente_Top] b where Mes between'7' and  '9' and a.Cliente=b.Cliente ),0),Trimestre4=isnull((select  sum(CantidadOperaciones)/3  from[Encomienda].[DTM_Encomienda_Cliente_Top] b where Mes between'10' and  '12' and a.Cliente=b.Cliente ),0),(Select sum(CantidadOperaciones)/3 as total  from [Encomienda].[DTM_Encomienda_Cliente_Top] c where c.Cliente=a.Cliente )as t from [Encomienda].[DTM_Encomienda_Cliente_Top] a group by Cliente order by t desc " +
           " if (@Año = '' and @Operacion != '') select top 10  Cliente,Trimestre1=isnull((select  sum(CantidadOperaciones)/3  from[Encomienda].[DTM_Encomienda_Cliente_Top] b where Mes between'1' and  '3' and a.Cliente=b.Cliente and  Operacion=@Operacion ),0),Trimestre2=isnull((select  sum(CantidadOperaciones)/3  from[Encomienda].[DTM_Encomienda_Cliente_Top] b where Mes between'4' and  '6' and a.Cliente=b.Cliente and  Operacion=@Operacion),0),Trimestre3=isnull((select  sum(CantidadOperaciones)/3  from[Encomienda].[DTM_Encomienda_Cliente_Top] b where Mes between'7' and  '9' and a.Cliente=b.Cliente and  Operacion=@Operacion ),0),Trimestre4=isnull((select  sum(CantidadOperaciones)/3  from[Encomienda].[DTM_Encomienda_Cliente_Top] b where Mes between'10' and  '12' and a.Cliente=b.Cliente and  Operacion=@Operacion ),0),(Select sum(CantidadOperaciones)/3 as total  from [Encomienda].[DTM_Encomienda_Cliente_Top] c where c.Cliente=a.Cliente and  Operacion=@Operacion )as t from [Encomienda].[DTM_Encomienda_Cliente_Top] a group by Cliente order by t desc " +
           " if (@Año != '' and @Operacion = '' ) select top 10 Cliente,Trimestre1=isnull((select  sum(CantidadOperaciones)/3  from[Encomienda].[DTM_Encomienda_Cliente_Top] b where Mes between'1' and  '3' and a.Cliente=b.Cliente and  Anual=@Año ),0),Trimestre2=isnull((select  sum(CantidadOperaciones)/3  from[Encomienda].[DTM_Encomienda_Cliente_Top] b where Mes between'4' and  '6' and a.Cliente=b.Cliente and  Anual=@Año),0),Trimestre3=isnull((select  sum(CantidadOperaciones)/3  from[Encomienda].[DTM_Encomienda_Cliente_Top] b where Mes between'7' and  '9' and a.Cliente=b.Cliente and  Anual=@Año ),0),Trimestre4=isnull((select  sum(CantidadOperaciones)/3  from[Encomienda].[DTM_Encomienda_Cliente_Top] b where Mes between'10' and  '12' and a.Cliente=b.Cliente and  Anual=@Año ),0),(Select sum(CantidadOperaciones)/3 as total  from [Encomienda].[DTM_Encomienda_Cliente_Top] c where c.Cliente=a.Cliente and Anual=@Año)as t from [Encomienda].[DTM_Encomienda_Cliente_Top] a group by Cliente order by t desc " +
           " if (@Año != '' and @Operacion != '') select top 10 Cliente,Trimestre1=isnull((select  sum(CantidadOperaciones)/3  from[Encomienda].[DTM_Encomienda_Cliente_Top] b where Mes between'1' and  '3' and a.Cliente=b.Cliente and  Operacion=@Operacion and Anual=@Año  ),0),Trimestre2=isnull((select  sum(CantidadOperaciones)/3  from[Encomienda].[DTM_Encomienda_Cliente_Top] b where Mes between'4' and  '6' and a.Cliente=b.Cliente and  Operacion=@Operacion and Anual=@Año ),0),Trimestre3=isnull((select  sum(CantidadOperaciones)/3  from[Encomienda].[DTM_Encomienda_Cliente_Top] b where Mes between'7' and  '9' and a.Cliente=b.Cliente and  Operacion=@Operacion and Anual=@Año  ),0),Trimestre4=isnull((select  sum(CantidadOperaciones)/3  from[Encomienda].[DTM_Encomienda_Cliente_Top] b where Mes between'10' and  '12' and a.Cliente=b.Cliente and  Operacion=@Operacion and Anual=@Año  ),0),(Select sum(CantidadOperaciones)/3 as total  from [Encomienda].[DTM_Encomienda_Cliente_Top] c where c.Cliente=a.Cliente  and  Operacion=@Operacion and Anual=@Año )as t from [Encomienda].[DTM_Encomienda_Cliente_Top] a group by Cliente order by t desc ";
            return consulta;
        }
        //GIAMPIERE 10/ 09 / 2019
        public String Encomienda_ClientesTop_CantidaEncomiendas_AgrupacionBimestre_SQL(String anio, String operacion)
        {
            consulta =
           " declare  @Mes nvarchar(max),@Año nvarchar(max),@Operacion nvarchar(max)" +
         " set @Operacion='" + operacion + "'; set @Año= '" + anio + "';" +
         " if (@Año = '' and @Operacion = '' ) select top 10  Cliente,Bimestre1=isnull((select  sum(CantidadOperaciones)/2  from[Encomienda].[DTM_Encomienda_Cliente_Top] b where Mes between'1' and  '2' and a.Cliente=b.Cliente ),0),Bimestre2=isnull((select  sum(CantidadOperaciones)/2  from[Encomienda].[DTM_Encomienda_Cliente_Top] b where Mes between'3' and  '4' and a.Cliente=b.Cliente ),0),Bimestre3=isnull((select  sum(CantidadOperaciones)/2  from[Encomienda].[DTM_Encomienda_Cliente_Top] b where Mes between'5' and  '6' and a.Cliente=b.Cliente ),0),Bimestre4=isnull((select  sum(CantidadOperaciones)/2  from[Encomienda].[DTM_Encomienda_Cliente_Top] b where Mes between'7' and  '8' and a.Cliente=b.Cliente ),0),Bimestre5=isnull((select  sum(CantidadOperaciones)/2  from[Encomienda].[DTM_Encomienda_Cliente_Top] b where Mes between'9' and  '10' and a.Cliente=b.Cliente ),0),Bimestre6=isnull((select  sum(CantidadOperaciones)/2  from[Encomienda].[DTM_Encomienda_Cliente_Top] b where Mes between'11' and  '12' and a.Cliente=b.Cliente ),0),(Select sum(CantidadOperaciones)/2 as total  from [Encomienda].[DTM_Encomienda_Cliente_Top] c where c.Cliente=a.Cliente )as t from [Encomienda].[DTM_Encomienda_Cliente_Top] a group by Cliente order by t desc      " +
         " if (@Año = '' and @Operacion != '') select top 10  Cliente ,Bimestre1=isnull((select  sum(CantidadOperaciones)/2  from[Encomienda].[DTM_Encomienda_Cliente_Top] b where Mes between'1' and  '2' and a.Cliente=b.Cliente and  Operacion=@Operacion ),0),Bimestre2=isnull((select  sum(CantidadOperaciones)/2  from[Encomienda].[DTM_Encomienda_Cliente_Top] b where Mes between'3' and  '4' and a.Cliente=b.Cliente and  Operacion=@Operacion ),0),Bimestre3=isnull((select  sum(CantidadOperaciones)/2  from[Encomienda].[DTM_Encomienda_Cliente_Top] b where Mes between'5' and  '6' and a.Cliente=b.Cliente and  Operacion=@Operacion ),0),Bimestre4=isnull((select  sum(CantidadOperaciones)/2  from[Encomienda].[DTM_Encomienda_Cliente_Top] b where Mes between'7' and  '8' and a.Cliente=b.Cliente and  Operacion=@Operacion ),0),Bimestre5=isnull((select  sum(CantidadOperaciones)/2  from[Encomienda].[DTM_Encomienda_Cliente_Top] b where Mes between'9' and  '10' and a.Cliente=b.Cliente and  Operacion=@Operacion ),0),Bimestre6=isnull((select  sum(CantidadOperaciones)/2  from[Encomienda].[DTM_Encomienda_Cliente_Top] b where Mes between'11' and  '12' and a.Cliente=b.Cliente and  Operacion=@Operacion  ),0),(Select sum(CantidadOperaciones)/2 as total  from [Encomienda].[DTM_Encomienda_Cliente_Top] c where c.Cliente=a.Cliente and  Operacion=@Operacion )as t from [Encomienda].[DTM_Encomienda_Cliente_Top] a group by Cliente order by t desc " +
         " if (@Año != '' and @Operacion = '' ) select top 10  Cliente ,Bimestre1=isnull((select  sum(CantidadOperaciones)/2  from[Encomienda].[DTM_Encomienda_Cliente_Top] b where Mes between'1' and  '2' and a.Cliente=b.Cliente and  Anual=@Año ),0),Bimestre2=isnull((select  sum(CantidadOperaciones)/2  from[Encomienda].[DTM_Encomienda_Cliente_Top] b where Mes between'3' and  '4' and a.Cliente=b.Cliente and  Anual=@Año ),0),Bimestre3=isnull((select  sum(CantidadOperaciones)/2  from[Encomienda].[DTM_Encomienda_Cliente_Top] b where Mes between'5' and  '6' and a.Cliente=b.Cliente and  Anual=@Año ),0),Bimestre4=isnull((select  sum(CantidadOperaciones)/2  from[Encomienda].[DTM_Encomienda_Cliente_Top] b where Mes between'7' and  '8' and a.Cliente=b.Cliente and  Anual=@Año ),0),Bimestre5=isnull((select  sum(CantidadOperaciones)/2  from[Encomienda].[DTM_Encomienda_Cliente_Top] b where Mes between'9' and  '10' and a.Cliente=b.Cliente and  Anual=@Año ),0),Bimestre6=isnull((select  sum(CantidadOperaciones)/2  from[Encomienda].[DTM_Encomienda_Cliente_Top] b where Mes between'11' and  '12' and a.Cliente=b.Cliente and  Anual=@Año  ),0),(Select sum(CantidadOperaciones)/2 as total  from [Encomienda].[DTM_Encomienda_Cliente_Top] c where c.Cliente=a.Cliente and  Anual=@Año )as t from [Encomienda].[DTM_Encomienda_Cliente_Top] a group by Cliente order by t desc " +
         " if (@Año != '' and @Operacion != '') select top 10  Cliente ,Bimestre1=isnull((select  sum(CantidadOperaciones)/2  from[Encomienda].[DTM_Encomienda_Cliente_Top] b where Mes between'1' and  '2' and a.Cliente=b.Cliente and  Operacion=@Operacion and Anual=@Año   ),0),Bimestre2=isnull((select  sum(CantidadOperaciones)/2  from[Encomienda].[DTM_Encomienda_Cliente_Top] b where Mes between'3' and  '4' and a.Cliente=b.Cliente and  Operacion=@Operacion and Anual=@Año   ),0),Bimestre3=isnull((select  sum(CantidadOperaciones)/2  from[Encomienda].[DTM_Encomienda_Cliente_Top] b where Mes between'5' and  '6' and a.Cliente=b.Cliente and  Operacion=@Operacion and Anual=@Año   ),0),Bimestre4=isnull((select  sum(CantidadOperaciones)/2  from[Encomienda].[DTM_Encomienda_Cliente_Top] b where Mes between'7' and  '8' and a.Cliente=b.Cliente and  Operacion=@Operacion and Anual=@Año   ),0),Bimestre5=isnull((select  sum(CantidadOperaciones)/2  from[Encomienda].[DTM_Encomienda_Cliente_Top] b where Mes between'9' and  '10' and a.Cliente=b.Cliente and  Operacion=@Operacion and Anual=@Año   ),0),Bimestre6=isnull((select  sum(CantidadOperaciones)/2  from[Encomienda].[DTM_Encomienda_Cliente_Top] b where Mes between'11' and  '12' and a.Cliente=b.Cliente and  Operacion=@Operacion and Anual=@Año    ),0),(Select sum(CantidadOperaciones)/2 as total  from [Encomienda].[DTM_Encomienda_Cliente_Top] c where c.Cliente=a.Cliente and  Operacion=@Operacion and Anual=@Año   )as t from [Encomienda].[DTM_Encomienda_Cliente_Top] a group by Cliente order by t desc ";

            return consulta;
        }
        //GIAMPIERE 10/ 09 / 2019
        public String Encomienda_ClientesTop_CantidaEncomiendas_ddlMes_SQL(String anio, String operacion)
        {
            consulta =
             "  declare  @Operacion nvarchar(max),@Año varchar(max)" +
             " set @Operacion='" + operacion + "'; set @Año= '" + anio + "';" +
             "  if ( @Año  != '' and @Operacion = '' ) select distinct DATENAME(MONTH,Fecha) as Meses ,Mes from [Encomienda].[DTM_Encomienda_Cliente_Top] Where Anual=@Año order by Mes" +
             "  if ( @Año  != '' and @Operacion != '' ) select distinct DATENAME(MONTH,Fecha) as Meses ,Mes from [Encomienda].[DTM_Encomienda_Cliente_Top] Where Operacion=@Operacion and Anual=@Año order by Mes" +
             "  if ( @Año = '' and @Operacion != '')   select distinct DATENAME(MONTH,Fecha) as Meses ,Mes from [Encomienda].[DTM_Encomienda_Cliente_Top] Where Operacion=@Operacion order by Mes" +
             "  if ( @Año = '' and @Operacion = ''  ) select distinct DATENAME(MONTH,Fecha) as Meses ,Mes from [Encomienda].[DTM_Encomienda_Cliente_Top] order by Mes"; ;

            return consulta;
        }
        //GIAMPIERE 10/ 09 / 2019
        public String Encomienda_ClientesTop_CantidaEncomiendas_ddlAño_SQL(String mes, String operacion)
        {
            consulta =
              "  declare  @Operacion nvarchar(max),@Mes varchar(max)" +
             " set @Operacion='" + operacion + "';set @Mes= '" + mes + "';" +
             "  if ( @Mes  != '' and @Operacion= '' ) select distinct Anual as Anio from [Encomienda].[DTM_Encomienda_Cliente_Top] Where Mes=@Mes order by Anual" +
             "  if ( @Mes  != '' and @Operacion!= '' ) select distinct Anual as Anio from [Encomienda].[DTM_Encomienda_Cliente_Top] Where Operacion=@Operacion and Mes=@Mes order by Anual" +
             "  if ( @Mes = '' and @Operacion!= '')   select distinct Anual as Anio from [Encomienda].[DTM_Encomienda_Cliente_Top] Where Operacion=@Operacion order by Anual" +
             "  if ( @Mes = '' and @Operacion= ''  ) select distinct Anual as Anio from [Encomienda].[DTM_Encomienda_Cliente_Top]  order by Anual";

            return consulta;
        }
        //GIAMPIERE 10/ 09 / 2019
        public String Encomienda_ClientesTop_CantidaEncomiendas_ddlOperacion_SQL(String mes, String anio)
        {
            consulta =
                " declare  @Año nvarchar(max),@Mes varchar(max);" +
              " set @Año= '" + anio + "';set @Mes= '" + mes + "';" +
              " if ( @Mes  != '' and @Año = '' ) select distinct Operacion from [Encomienda].[DTM_Encomienda_Cliente_Top] Where Mes=@Mes order by Operacion" +
              " if ( @Mes  != '' and @Año != '' ) select distinct Operacion from [Encomienda].[DTM_Encomienda_Cliente_Top] Where Anual=@Año and Mes=@Mes order by Operacion" +
              " if ( @Mes = '' and @Año != '')   select distinct Operacion from [Encomienda].[DTM_Encomienda_Cliente_Top] Where Anual=@Año order by Operacion" +
              " if ( @Mes = '' and @Año = ''  ) select distinct Operacion from [Encomienda].[DTM_Encomienda_Cliente_Top] order by Operacion";
            return consulta;
        }
        //Segun Rentabilidad
        //GIAMPIERE 11/ 09 / 2019
        public String G_Encomienda_ClientesTop_Rentabilidad_SQL(String anio, String mes, String operacion)
        {
            consulta =
       " declare @Mes nvarchar(max),@Año nvarchar(max),@Operacion nvarchar(max), @query  NVARCHAR(MAX), @cols  NVARCHAR(MAX), @null  NVARCHAR(MAX), @filtro NVARCHAR(MAX), @consulta  NVARCHAR(MAX), @cols2  NVARCHAR(MAX), @null2  NVARCHAR(MAX), @select NVARCHAR(MAX), @from NVARCHAR(MAX) " +
       " set @Operacion='" + operacion + "';set @Año= '" + anio + "';set @Mes= '" + mes + "';set @filtro='';set @consulta= '';" +
       " select @cols = STUFF((SELECT Distinct ',' + QUOTENAME(Anual)  from  [Encomienda].[DTM_Encomienda_Cliente_Top]  group by ',' + QUOTENAME(Anual)Order by ',' + QUOTENAME(Anual) desc FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)')  ,1,1,'') " +
       " select @null = STUFF((SELECT Distinct ', isnull(' + QUOTENAME(Anual)+',0) as'  + QUOTENAME(Anual) from  [Encomienda].[DTM_Encomienda_Cliente_Top]   group by ', isnull(' + QUOTENAME(Anual)+',0) as'  + QUOTENAME(Anual) Order by ', isnull(' + QUOTENAME(Anual)+',0) as'  + QUOTENAME(Anual) desc  FOR XML PATH(''), TYPE  ).value('.', 'NVARCHAR(MAX)')   ,1,1,'')  " +
       " select @cols2 = STUFF((SELECT Distinct ',' + QUOTENAME(Anual)  from  [Encomienda].[DTM_Encomienda_Cliente_Top]  where Anual=@Año group by ',' + QUOTENAME(Anual)Order by ',' + QUOTENAME(Anual) desc FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)')  ,1,1,'') " +
       " select @null2 = STUFF((SELECT Distinct ', isnull(' + QUOTENAME(Anual)+',0) as'  + QUOTENAME(Anual) from  [Encomienda].[DTM_Encomienda_Cliente_Top]  where Anual=@Año group by ', isnull(' + QUOTENAME(Anual)+',0) as'  + QUOTENAME(Anual) Order by ', isnull(' + QUOTENAME(Anual)+',0) as'  + QUOTENAME(Anual) desc  FOR XML PATH(''), TYPE  ).value('.', 'NVARCHAR(MAX)')   ,1,1,'')" +
       " select @select= ',(Select sum(ImporteTotalNeto) as total  from [Encomienda].[DTM_Encomienda_Cliente_Top] b where b.Cliente=p.Cliente   '" +
       " select @from= ' group by Cliente) as total   from  ( select  Cliente,Anual,ImporteTotalNeto  from [Encomienda].[DTM_Encomienda_Cliente_Top]  '" +
       " if (@Año!= '' and @Mes = '' and @Operacion = ''  )   set @consulta = 'SELECT top 10 isnull(Cliente,0) as Cliente,' + @null2 + ' ' + @select + '  and Anual='''+@Año+''' ' + @from + '  )x  pivot (  sum(ImporteTotalNeto) for Anual in (' + @cols2 + ') )  p order by total desc' " +
       " if (@Año!= '' and @Mes != '' and @Operacion != '' ) set @consulta = 'SELECT top 10 isnull(Cliente,0) as Cliente,' + @null2 + ' ' + @select + ' and Operacion = '''+@Operacion+'''  and Mes='''+@Mes+'''  and Anual='''+@Año+''' ' + @from + ' where  Operacion = '''+@Operacion+'''  and Mes='''+@Mes+''' )x  pivot (  sum(ImporteTotalNeto) for Anual in (' + @cols + ') )  p order by total desc'  " +
       " if (@Año!= '' and @Mes != '' and @Operacion = '')  set @consulta = 'SELECT top 10 isnull(Cliente,0) as Cliente,' + @null2 + ' ' + @select + ' and Mes ='''+@Mes+'''  and Anual='''+@Año+''' ' + @from + ' where Mes ='''+@Mes+''' )x  pivot (  sum(ImporteTotalNeto) for Anual in (' + @cols2 + ') )  p order by total desc'  " +
       " if (@Año!= '' and @Mes = '' and @Operacion != '')  set @consulta = 'SELECT top 10 isnull(Cliente,0) as Cliente,' + @null2 + ' ' + @select + ' and Operacion = '''+@Operacion+'''  and Anual='''+@Año+''' ' + @from + ' where Operacion = '''+@Operacion+'''  )x  pivot (  sum(ImporteTotalNeto) for Anual in (' + @cols2 + ') )  p order by total desc'  " +
       " if (@Año= '' and @Mes != '' and @Operacion != '' ) set @consulta = 'SELECT top 10 isnull(Cliente,0) as Cliente,' + @null + ' ' + @select + ' and  Operacion = '''+@Operacion+'''  and Mes='''+@Mes+'''  and Anual='''+@Año+''' ' + @from + ' where  Operacion = '''+@Operacion+'''  and Mes='''+@Mes+''' )x  pivot (  sum(ImporteTotalNeto) for Anual in (' + @cols + ') )  p order by total desc' " +
       " if (@Año= '' and @Mes != '' and @Operacion = '')  set @consulta = 'SELECT top 10 isnull(Cliente,0) as Cliente,' + @null + ' ' + @select + ' and Mes = '''+@Mes+'''' + @from + ' where Mes = '''+@Mes+''' )x  pivot (  sum(ImporteTotalNeto) for Anual in (' + @cols + ') )  p order by total desc' " +
       " if (@Año= '' and @Mes = '' and @Operacion != '') set @consulta = 'SELECT top 10 isnull(Cliente,0) as Cliente,' + @null + ' ' + @select + ' and Operacion = '''+@Operacion+'''  ' + @from + ' where Operacion = '''+@Operacion+'''   )x  pivot (  sum(ImporteTotalNeto) for Anual in (' + @cols + ') )  p order by total desc'  " +
       " if (@Año= '' and @Mes = '' and @Operacion = ''  ) set @consulta = 'SELECT top 10 isnull(Cliente,0) as Cliente,' + @null + ' ' + @select + ' ' + @from + ' ) x  pivot (  sum(ImporteTotalNeto) for Anual in (' + @cols + ') )  p order by total desc' " +
       " execute(@consulta)";
            return consulta;
        }
        //GIAMPIERE 11/ 09 / 2019
        public String Encomienda_ClientesTop_Rentabilidad_AgrupacionTrimestre_SQL(String anio, String operacion)
        {
            consulta =
            " declare  @Mes nvarchar(max),@Año nvarchar(max),@Operacion nvarchar(max)" +
            " set @Operacion='" + operacion + "'; set @Año= '" + anio + "';" +
           " if (@Año = '' and @Operacion = '' ) select top 10  Cliente,Trimestre1=isnull((select  sum(ImporteTotalNeto)/3  from[Encomienda].[DTM_Encomienda_Cliente_Top] b where Mes between'1' and  '3' and a.Cliente=b.Cliente ),0),Trimestre2=isnull((select  sum(ImporteTotalNeto)/3  from[Encomienda].[DTM_Encomienda_Cliente_Top] b where Mes between'4' and  '6' and a.Cliente=b.Cliente ),0),Trimestre3=isnull((select  sum(ImporteTotalNeto)/3  from[Encomienda].[DTM_Encomienda_Cliente_Top] b where Mes between'7' and  '9' and a.Cliente=b.Cliente ),0),Trimestre4=isnull((select  sum(ImporteTotalNeto)/3  from[Encomienda].[DTM_Encomienda_Cliente_Top] b where Mes between'10' and  '12' and a.Cliente=b.Cliente ),0),(Select sum(ImporteTotalNeto)/3 as total  from [Encomienda].[DTM_Encomienda_Cliente_Top] c where c.Cliente=a.Cliente )as t from [Encomienda].[DTM_Encomienda_Cliente_Top] a group by Cliente order by t desc " +
           " if (@Año = '' and @Operacion != '') select top 10  Cliente,Trimestre1=isnull((select  sum(ImporteTotalNeto)/3  from[Encomienda].[DTM_Encomienda_Cliente_Top] b where Mes between'1' and  '3' and a.Cliente=b.Cliente and  Operacion=@Operacion ),0),Trimestre2=isnull((select  sum(ImporteTotalNeto)/3  from[Encomienda].[DTM_Encomienda_Cliente_Top] b where Mes between'4' and  '6' and a.Cliente=b.Cliente and  Operacion=@Operacion),0),Trimestre3=isnull((select  sum(ImporteTotalNeto)/3  from[Encomienda].[DTM_Encomienda_Cliente_Top] b where Mes between'7' and  '9' and a.Cliente=b.Cliente and  Operacion=@Operacion ),0),Trimestre4=isnull((select  sum(ImporteTotalNeto)/3  from[Encomienda].[DTM_Encomienda_Cliente_Top] b where Mes between'10' and  '12' and a.Cliente=b.Cliente and  Operacion=@Operacion ),0),(Select sum(ImporteTotalNeto)/3 as total  from [Encomienda].[DTM_Encomienda_Cliente_Top] c where c.Cliente=a.Cliente and  Operacion=@Operacion )as t from [Encomienda].[DTM_Encomienda_Cliente_Top] a group by Cliente order by t desc " +
           " if (@Año != '' and @Operacion = '' ) select top 10 Cliente,Trimestre1=isnull((select  sum(ImporteTotalNeto)/3  from[Encomienda].[DTM_Encomienda_Cliente_Top] b where Mes between'1' and  '3' and a.Cliente=b.Cliente and  Anual=@Año ),0),Trimestre2=isnull((select  sum(ImporteTotalNeto)/3  from[Encomienda].[DTM_Encomienda_Cliente_Top] b where Mes between'4' and  '6' and a.Cliente=b.Cliente and  Anual=@Año),0),Trimestre3=isnull((select  sum(ImporteTotalNeto)/3  from[Encomienda].[DTM_Encomienda_Cliente_Top] b where Mes between'7' and  '9' and a.Cliente=b.Cliente and  Anual=@Año ),0),Trimestre4=isnull((select  sum(ImporteTotalNeto)/3  from[Encomienda].[DTM_Encomienda_Cliente_Top] b where Mes between'10' and  '12' and a.Cliente=b.Cliente and  Anual=@Año ),0),(Select sum(ImporteTotalNeto)/3 as total  from [Encomienda].[DTM_Encomienda_Cliente_Top] c where c.Cliente=a.Cliente and Anual=@Año)as t from [Encomienda].[DTM_Encomienda_Cliente_Top] a group by Cliente order by t desc " +
           " if (@Año != '' and @Operacion != '') select top 10 Cliente,Trimestre1=isnull((select  sum(ImporteTotalNeto)/3  from[Encomienda].[DTM_Encomienda_Cliente_Top] b where Mes between'1' and  '3' and a.Cliente=b.Cliente and  Operacion=@Operacion and Anual=@Año  ),0),Trimestre2=isnull((select  sum(ImporteTotalNeto)/3  from[Encomienda].[DTM_Encomienda_Cliente_Top] b where Mes between'4' and  '6' and a.Cliente=b.Cliente and  Operacion=@Operacion and Anual=@Año ),0),Trimestre3=isnull((select  sum(ImporteTotalNeto)/3  from[Encomienda].[DTM_Encomienda_Cliente_Top] b where Mes between'7' and  '9' and a.Cliente=b.Cliente and  Operacion=@Operacion and Anual=@Año  ),0),Trimestre4=isnull((select  sum(ImporteTotalNeto)/3  from[Encomienda].[DTM_Encomienda_Cliente_Top] b where Mes between'10' and  '12' and a.Cliente=b.Cliente and  Operacion=@Operacion and Anual=@Año  ),0),(Select sum(ImporteTotalNeto)/3 as total  from [Encomienda].[DTM_Encomienda_Cliente_Top] c where c.Cliente=a.Cliente  and  Operacion=@Operacion and Anual=@Año )as t from [Encomienda].[DTM_Encomienda_Cliente_Top] a group by Cliente order by t desc ";
            return consulta;
        }
        //GIAMPIERE 11/ 09 / 2019
        public String Encomienda_ClientesTop_Rentabilidad_AgrupacionBimestre_SQL(String anio, String operacion)
        {
            consulta =
         " declare  @Mes nvarchar(max),@Año nvarchar(max),@Operacion nvarchar(max)" +
         " set @Operacion='" + operacion + "'; set @Año= '" + anio + "';" +
         " if (@Año = '' and @Operacion = '' ) select top 10  Cliente,Bimestre1=isnull((select  sum(ImporteTotalNeto)/2  from[Encomienda].[DTM_Encomienda_Cliente_Top] b where Mes between'1' and  '2' and a.Cliente=b.Cliente ),0),Bimestre2=isnull((select  sum(ImporteTotalNeto)/2  from[Encomienda].[DTM_Encomienda_Cliente_Top] b where Mes between'3' and  '4' and a.Cliente=b.Cliente ),0),Bimestre3=isnull((select  sum(ImporteTotalNeto)/2  from[Encomienda].[DTM_Encomienda_Cliente_Top] b where Mes between'5' and  '6' and a.Cliente=b.Cliente ),0),Bimestre4=isnull((select  sum(ImporteTotalNeto)/2  from[Encomienda].[DTM_Encomienda_Cliente_Top] b where Mes between'7' and  '8' and a.Cliente=b.Cliente ),0),Bimestre5=isnull((select  sum(ImporteTotalNeto)/2  from[Encomienda].[DTM_Encomienda_Cliente_Top] b where Mes between'9' and  '10' and a.Cliente=b.Cliente ),0),Bimestre6=isnull((select  sum(ImporteTotalNeto)/2  from[Encomienda].[DTM_Encomienda_Cliente_Top] b where Mes between'11' and  '12' and a.Cliente=b.Cliente ),0),(Select sum(ImporteTotalNeto)/2 as total  from [Encomienda].[DTM_Encomienda_Cliente_Top] c where c.Cliente=a.Cliente )as t from [Encomienda].[DTM_Encomienda_Cliente_Top] a group by Cliente order by t desc      " +
         " if (@Año = '' and @Operacion != '') select top 10  Cliente ,Bimestre1=isnull((select  sum(ImporteTotalNeto)/2  from[Encomienda].[DTM_Encomienda_Cliente_Top] b where Mes between'1' and  '2' and a.Cliente=b.Cliente and  Operacion=@Operacion ),0),Bimestre2=isnull((select  sum(ImporteTotalNeto)/2  from[Encomienda].[DTM_Encomienda_Cliente_Top] b where Mes between'3' and  '4' and a.Cliente=b.Cliente and  Operacion=@Operacion ),0),Bimestre3=isnull((select  sum(ImporteTotalNeto)/2  from[Encomienda].[DTM_Encomienda_Cliente_Top] b where Mes between'5' and  '6' and a.Cliente=b.Cliente and  Operacion=@Operacion ),0),Bimestre4=isnull((select  sum(ImporteTotalNeto)/2  from[Encomienda].[DTM_Encomienda_Cliente_Top] b where Mes between'7' and  '8' and a.Cliente=b.Cliente and  Operacion=@Operacion ),0),Bimestre5=isnull((select  sum(ImporteTotalNeto)/2  from[Encomienda].[DTM_Encomienda_Cliente_Top] b where Mes between'9' and  '10' and a.Cliente=b.Cliente and  Operacion=@Operacion ),0),Bimestre6=isnull((select  sum(ImporteTotalNeto)/2  from[Encomienda].[DTM_Encomienda_Cliente_Top] b where Mes between'11' and  '12' and a.Cliente=b.Cliente and  Operacion=@Operacion  ),0),(Select sum(ImporteTotalNeto)/2 as total  from [Encomienda].[DTM_Encomienda_Cliente_Top] c where c.Cliente=a.Cliente and  Operacion=@Operacion )as t from [Encomienda].[DTM_Encomienda_Cliente_Top] a group by Cliente order by t desc " +
         " if (@Año != '' and @Operacion = '' ) select top 10  Cliente ,Bimestre1=isnull((select  sum(ImporteTotalNeto)/2  from[Encomienda].[DTM_Encomienda_Cliente_Top] b where Mes between'1' and  '2' and a.Cliente=b.Cliente and  Anual=@Año ),0),Bimestre2=isnull((select  sum(ImporteTotalNeto)/2  from[Encomienda].[DTM_Encomienda_Cliente_Top] b where Mes between'3' and  '4' and a.Cliente=b.Cliente and  Anual=@Año ),0),Bimestre3=isnull((select  sum(ImporteTotalNeto)/2  from[Encomienda].[DTM_Encomienda_Cliente_Top] b where Mes between'5' and  '6' and a.Cliente=b.Cliente and  Anual=@Año ),0),Bimestre4=isnull((select  sum(ImporteTotalNeto)/2  from[Encomienda].[DTM_Encomienda_Cliente_Top] b where Mes between'7' and  '8' and a.Cliente=b.Cliente and  Anual=@Año ),0),Bimestre5=isnull((select  sum(ImporteTotalNeto)/2  from[Encomienda].[DTM_Encomienda_Cliente_Top] b where Mes between'9' and  '10' and a.Cliente=b.Cliente and  Anual=@Año ),0),Bimestre6=isnull((select  sum(ImporteTotalNeto)/2  from[Encomienda].[DTM_Encomienda_Cliente_Top] b where Mes between'11' and  '12' and a.Cliente=b.Cliente and  Anual=@Año  ),0),(Select sum(ImporteTotalNeto)/2 as total  from [Encomienda].[DTM_Encomienda_Cliente_Top] c where c.Cliente=a.Cliente and  Anual=@Año )as t from [Encomienda].[DTM_Encomienda_Cliente_Top] a group by Cliente order by t desc " +
         " if (@Año != '' and @Operacion != '') select top 10  Cliente ,Bimestre1=isnull((select  sum(ImporteTotalNeto)/2  from[Encomienda].[DTM_Encomienda_Cliente_Top] b where Mes between'1' and  '2' and a.Cliente=b.Cliente and  Operacion=@Operacion and Anual=@Año   ),0),Bimestre2=isnull((select  sum(ImporteTotalNeto)/2  from[Encomienda].[DTM_Encomienda_Cliente_Top] b where Mes between'3' and  '4' and a.Cliente=b.Cliente and  Operacion=@Operacion and Anual=@Año   ),0),Bimestre3=isnull((select  sum(ImporteTotalNeto)/2  from[Encomienda].[DTM_Encomienda_Cliente_Top] b where Mes between'5' and  '6' and a.Cliente=b.Cliente and  Operacion=@Operacion and Anual=@Año   ),0),Bimestre4=isnull((select  sum(ImporteTotalNeto)/2  from[Encomienda].[DTM_Encomienda_Cliente_Top] b where Mes between'7' and  '8' and a.Cliente=b.Cliente and  Operacion=@Operacion and Anual=@Año   ),0),Bimestre5=isnull((select  sum(ImporteTotalNeto)/2  from[Encomienda].[DTM_Encomienda_Cliente_Top] b where Mes between'9' and  '10' and a.Cliente=b.Cliente and  Operacion=@Operacion and Anual=@Año   ),0),Bimestre6=isnull((select  sum(ImporteTotalNeto)/2  from[Encomienda].[DTM_Encomienda_Cliente_Top] b where Mes between'11' and  '12' and a.Cliente=b.Cliente and  Operacion=@Operacion and Anual=@Año    ),0),(Select sum(ImporteTotalNeto)/2 as total  from [Encomienda].[DTM_Encomienda_Cliente_Top] c where c.Cliente=a.Cliente and  Operacion=@Operacion and Anual=@Año   )as t from [Encomienda].[DTM_Encomienda_Cliente_Top] a group by Cliente order by t desc ";
            return consulta;
        }
        //GIAMPIERE 11/ 09 / 2019
        public String Encomienda_ClientesTop_Rentabilidad_ddlMes_SQL(String anio, String operacion)
        {
            consulta =
             "  declare  @Operacion nvarchar(max),@Año varchar(max)" +
             " set @Operacion='" + operacion + "'; set @Año= '" + anio + "';" +
             "  if ( @Año  != '' and @Operacion = '' ) select distinct DATENAME(MONTH,Fecha) as Meses ,Mes from [Encomienda].[DTM_Encomienda_Cliente_Top] Where Anual=@Año order by Mes" +
             "  if ( @Año  != '' and @Operacion != '' ) select distinct DATENAME(MONTH,Fecha) as Meses ,Mes from [Encomienda].[DTM_Encomienda_Cliente_Top] Where Operacion=@Operacion and Anual=@Año order by Mes" +
             "  if ( @Año = '' and @Operacion != '')   select distinct DATENAME(MONTH,Fecha) as Meses ,Mes from [Encomienda].[DTM_Encomienda_Cliente_Top] Where Operacion=@Operacion order by Mes" +
             "  if ( @Año = '' and @Operacion = ''  ) select distinct DATENAME(MONTH,Fecha) as Meses ,Mes from [Encomienda].[DTM_Encomienda_Cliente_Top] order by Mes"; ;

            return consulta;
        }
        //GIAMPIERE 11/ 09 / 2019
        public String Encomienda_ClientesTop_Rentabilidad_ddlAño_SQL(String mes, String operacion)
        {
            consulta =
              "  declare  @Operacion nvarchar(max),@Mes varchar(max)" +
             " set @Operacion='" + operacion + "';set @Mes= '" + mes + "';" +
             "  if ( @Mes  != '' and @Operacion= '' ) select distinct Anual as Anio from [Encomienda].[DTM_Encomienda_Cliente_Top] Where Mes=@Mes order by Anual" +
             "  if ( @Mes  != '' and @Operacion!= '' ) select distinct Anual as Anio from [Encomienda].[DTM_Encomienda_Cliente_Top] Where Operacion=@Operacion and Mes=@Mes order by Anual" +
             "  if ( @Mes = '' and @Operacion!= '')   select distinct Anual as Anio from [Encomienda].[DTM_Encomienda_Cliente_Top] Where Operacion=@Operacion order by Anual" +
             "  if ( @Mes = '' and @Operacion= ''  ) select distinct Anual as Anio from [Encomienda].[DTM_Encomienda_Cliente_Top]  order by Anual";

            return consulta;
        }
        //GIAMPIERE 11/ 09 / 2019
        public String Encomienda_ClientesTop_Rentabilidad_ddlOperacion_SQL(String mes, String anio)
        {
            consulta =
                " declare  @Año nvarchar(max),@Mes varchar(max);" +
              " set @Año= '" + anio + "';set @Mes= '" + mes + "';" +
              " if ( @Mes  != '' and @Año = '' ) select distinct Operacion from [Encomienda].[DTM_Encomienda_Cliente_Top] Where Mes=@Mes order by Operacion" +
              " if ( @Mes  != '' and @Año != '' ) select distinct Operacion from [Encomienda].[DTM_Encomienda_Cliente_Top] Where Anual=@Año and Mes=@Mes order by Operacion" +
              " if ( @Mes = '' and @Año != '')   select distinct Operacion from [Encomienda].[DTM_Encomienda_Cliente_Top] Where Anual=@Año order by Operacion" +
              " if ( @Mes = '' and @Año = ''  ) select distinct Operacion from [Encomienda].[DTM_Encomienda_Cliente_Top] order by Operacion";
            return consulta;
        }

        #endregion

        #region RUTAVEHICULOS
        /* 
            DESARROLLADOR: JORGE BALTODANO
            KPI: ENCOMIENDAS
        */

        // GRAFICA: CANTIDAD VIAJE SEGUN RUTA POR TIPO VEHICULO
        public String Grafica_Encomiendas_RutasVehiculos_GeneralTipoVehiculo_SQL(String Año, String Mes, String Trimestre, String Bimestre){
            consulta=
                " declare @Mes nvarchar(max),@Año nvarchar(max),@Bimestre nvarchar(max),@Trimestre nvarchar(max), @select NVARCHAR(MAX),@query NVARCHAR(MAX), @cols NVARCHAR(MAX), @cols2 NVARCHAR(MAX), @null NVARCHAR(MAX), @filtro NVARCHAR(MAX), @consulta NVARCHAR(MAX); " +
                " set @Trimestre = '"+Trimestre+"'; set @Año = '"+Año+"'; set @Mes = '"+Mes+"'; set @Bimestre = '"+Bimestre+"'; set @filtro = ''; set @consulta = ''; set @select = ''; " +
                " select @cols = STUFF((SELECT Distinct top 10 ',' + QUOTENAME(TipoVehiculo)  from[Encomienda].[DTM_Encomienda_Rutas_Vehiculos] group by ',' + QUOTENAME(TipoVehiculo)Order by ',' + QUOTENAME(TipoVehiculo) desc FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'),1,1,'')   " +
                " select @null = STUFF((SELECT Distinct top 10', isnull(' + QUOTENAME(TipoVehiculo) + ',0) as' + QUOTENAME(TipoVehiculo) from[Encomienda].[DTM_Encomienda_Rutas_Vehiculos]  group by ', isnull(' + QUOTENAME(TipoVehiculo) + ',0) as' + QUOTENAME(TipoVehiculo) Order by ', isnull(' + QUOTENAME(TipoVehiculo) + ',0) as' + QUOTENAME(TipoVehiculo) desc  FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'),1,1,'')  " +
                " select @cols2 = STUFF((SELECT Distinct top 10 '+' + QUOTENAME(TipoVehiculo)  from[Encomienda].[DTM_Encomienda_Rutas_Vehiculos] group by '+' + QUOTENAME(TipoVehiculo)Order by '+' + QUOTENAME(TipoVehiculo) desc FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'),1,1,'')   " +
                " if (@Año = '' and @Mes = '' and @Trimestre = ''  and @Bimestre = '') set @consulta = 'SELECT top 10 CONCAT(Origen,'' - '',Destino) as Origen,' + @null + ' INTO #tmpCantidadViajes  from  ( select  Origen,Destino,TipoVehiculo,CantidadViajes from [Encomienda].[DTM_Encomienda_Rutas_Vehiculos]    ) x  pivot ( sum(CantidadViajes) for TipoVehiculo in (' + @cols + ') ) p order  by Origen desc " +
                " select Origen, '+@cols+' from(select Origen, '+@cols+', SUM('+@cols2+') as Total from #tmpCantidadViajes group by Origen, '+@cols+' )  x  group by Origen, '+@cols+', Total order by Total DESC  '  " +
                " else begin " +
                " if (@Año != '' and @Mes = '' and @Trimestre = '' and @Bimestre = ''  ) set @filtro = '  where  Anual = ''' + @Año + ''' ' " +
                " if (@Año != '' and @Mes != '' and @Trimestre = '' and @Bimestre = '' ) set @filtro = '  where  Anual = ''' + @Año + ''' and  Mes = ''' + @Mes + ''' ' " +
                " if (@Año != '' and @Mes = '' and @Trimestre != '' and @Bimestre = '')  set @filtro = '  where Anual = ''' + @Año + ''' and DATEPART(qq, Fecha) = ''' + @Trimestre + '''  ' " +
                " if (@Año != '' and @Mes = '' and @Trimestre = '' and @Bimestre!= '')  set @filtro = '  where Anual = ''' + @Año + ''' and (CAST((MONTH(Fecha)+1)/2 AS CHAR(1)))= ''' + @Bimestre + ''' ' " +
                " set @consulta = ' SELECT TOP 10 CONCAT(Origen,'' - '',Destino) as Origen,' + @null + ' INTO #tmpCantidadViajes  from     (  select  Origen,Destino,TipoVehiculo,CantidadViajes  from [Encomienda].[DTM_Encomienda_Rutas_Vehiculos]  ' + @filtro + '   ) x  pivot ( sum(CantidadViajes) for TipoVehiculo in (' + @cols + ') ) p order  by  Origen desc  " +
                " select Origen, '+@cols+' from(select Origen, '+@cols+', SUM('+@cols2+') as Total from #tmpCantidadViajes group by Origen, '+@cols+' )  x " +
                " group by Origen, '+@cols+', Total order by Total DESC  ' " +
                " end " +
                " execute(@consulta) ";
            return consulta;
        }

        
        // Listar Año - General por Tipo de Vehiculo
        public String Encomiendas_RutasVehiculos_ListarAño_GTV_SQL(String Mes, String Trimestre, String Bimestre){
           
           consulta=
            "  declare @Mes varchar(20),@Trimestre varchar(20),@Bimestre varchar(20) ,@Consulta nvarchar(max),@filtro nvarchar(max),@select nvarchar(max);  "+
            "  set @Mes='"+Mes+"';set @Trimestre='"+Trimestre+"';set @Bimestre='"+Bimestre+"';  set @Consulta= ''; set @filtro= ''; set @select= ''; "+
            "  set @select= N'select distinct Anual as Año from [Encomienda].[DTM_Encomienda_Rutas_Vehiculos] ' "+
            "  if( @Mes='' and @Bimestre='' and @Trimestre='' )        set @filtro = @select "+
            "  if (@Mes != '' and @Bimestre='' and @Trimestre=''  )    set @filtro = @select+  ' where  Mes = '''+@Mes+'''' "+
            "  if(@Mes != '' and @Bimestre!='' and @Trimestre='' )		set @filtro = @select+  ' where  Mes = '''+@Mes+''''  + ' and CAST((MONTH(Fecha)+1)/2 AS CHAR(1))  =''' +@Bimestre+''''"+ 
            "  if(@Mes != '' and @Bimestre!='' and @Trimestre!='' )	set @filtro = @select+  ' where  Mes = '''+@Mes+''''  + ' and CAST((MONTH(Fecha)+1)/2 AS CHAR(1))  =''' +@Bimestre+'''' + ' and  datepart(qq,Fecha)  =''' +@Trimestre+'''' "+
            "  if(@Mes='' and @Bimestre!='' and @Trimestre!='' )		set @filtro = @select+  ' where  CAST((MONTH(Fecha)+1)/2 AS CHAR(1))  =''' +@Bimestre+'''' + ' and  datepart(qq,Fecha)  =''' +@Trimestre+'''' "+
            "  if(@Mes='' and @Bimestre='' and @Trimestre!='' )		set @filtro = @select+  ' where  datepart(qq,Fecha)  =''' +@Trimestre+'''' "+
            "  if(@Mes='' and @Bimestre!='' and @Trimestre='' )	    set @filtro = @select+  ' where  CAST((MONTH(Fecha)+1)/2 AS CHAR(1))  =''' +@Bimestre+'''' "+
            "  set @Consulta= @filtro+ ' order by Anual'; "+
            "  exec sp_sqlexec @Consulta "; 
            return consulta;
          
        }

        
        // Listar Mes - General por Tipo de Vehiculo
        public String Encomiendas_RutasVehiculos_ListarMes_GTV_SQL(String Año){
           
           consulta=
            " declare @Año varchar(20),@Consulta nvarchar(max),@filtro nvarchar(max),@select nvarchar(max); "+
            " set @Año='"+Año+"';  set @Consulta= ''; set @filtro= ''; set @select= ''; "+
            " set @select= N'select distinct Mes as Meses from [Encomienda].[DTM_Encomienda_Rutas_Vehiculos] ' "+
            " if( @Año=''  )  set @filtro = @select "+
            " if (@Año !='')    set @filtro = @select+  ' where  Anual = '''+@Año+''''  "+
            " set @Consulta= @filtro+ ' order by Meses'; "+
            " exec sp_sqlexec @Consulta ";
           return consulta;
          
          }

        
        // Listar Bimestre - General por Tipo de Vehiculo
          public String Encomiendas_RutasVehiculos_ListarBimestre_GTV_SQL(String Año){
           
           consulta=
            "  declare @Año varchar(20),@Consulta nvarchar(max),@filtro nvarchar(max),@select nvarchar(max); " +
            "  set @Año='"+Año+"';  set @Consulta= ''; set @filtro= ''; set @select= ''; " +
            "  set @select= N'select distinct (CAST((MONTH(Fecha)+1)/2 AS CHAR(1))) as Bimestre from [Encomienda].[DTM_Encomienda_Rutas_Vehiculos] ' " +
            "  if( @Año=''  )  set @filtro = @select " +
            "  if (@Año !='')    set @filtro = @select+  ' where  Anual = '''+@Año+'''' " + 
            "  set @Consulta= @filtro+ ' order by Bimestre'; " +
            "  exec sp_sqlexec @Consulta " ;

           return consulta;
          
          }

        // Listar Trimestre - General por Tipo de Vehiculo
          public String Encomiendas_RutasVehiculos_ListarTrimestre_GTV_SQL(String Año){
           
           consulta=
            "  declare @Año varchar(20),@Consulta nvarchar(max),@filtro nvarchar(max),@select nvarchar(max);  "+
            "  set @Año='"+Año+"';  set @Consulta= ''; set @filtro= ''; set @select= ''; "+
            "  set @select= N'select distinct Datepart(qq, Fecha) as Trimestre from [Encomienda].[DTM_Encomienda_Rutas_Vehiculos] ' "+
            "  if( @Año=''  )  set @filtro = @select "+
            "  if (@Año !='')    set @filtro = @select+  ' where  Anual = '''+@Año+''''  "+
            "  set @Consulta= @filtro+ ' order by Trimestre'; "+
            "  exec sp_sqlexec @Consulta";


           return consulta;
          }

          
          // ##### GRAFICA 2 :  CANTIDAD VIAJE SEGUN RUTA POR TIEMPO ######
          public String Grafica_Encomiendas_RutasVehiculos_GeneralRutaTiempo_SQL(String anio, String mes, String tipo){
            consulta=
            " declare @Mes nvarchar(max),@Año nvarchar(max),@TipoVehiculo nvarchar(max), @query  NVARCHAR(MAX), @cols  NVARCHAR(MAX), @null  NVARCHAR(MAX), @filtro NVARCHAR(MAX), @consulta  NVARCHAR(MAX), @cols2  NVARCHAR(MAX), @null2  NVARCHAR(MAX), @select NVARCHAR(MAX), @from NVARCHAR(MAX) "+
            " set @TipoVehiculo='"+tipo+"';set @Año= '"+anio+"';set @Mes= '"+mes+"';set @filtro='';set @consulta= '';"+
           " select @cols = STUFF((SELECT Distinct ',' + QUOTENAME(Anual)  from  [Encomienda].[DTM_Encomienda_Rutas_Vehiculos]  group by ',' + QUOTENAME(Anual)Order by ',' + QUOTENAME(Anual) asc FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)')  ,1,1,'')  "+
           " select @null = STUFF((SELECT Distinct ', isnull(' + QUOTENAME(Anual)+',0) as'  + QUOTENAME(Anual) from  [Encomienda].[DTM_Encomienda_Rutas_Vehiculos]   group by ', isnull(' + QUOTENAME(Anual)+',0) as'  + QUOTENAME(Anual) Order by ', isnull(' + QUOTENAME(Anual)+',0) as'  + QUOTENAME(Anual) asc  FOR XML PATH(''), TYPE  ).value('.', 'NVARCHAR(MAX)')   ,1,1,'')  "+
           " select @cols2 = STUFF((SELECT Distinct ',' + QUOTENAME(Anual)  from  [Encomienda].[DTM_Encomienda_Rutas_Vehiculos]  where Anual=@Año group by ',' + QUOTENAME(Anual)Order by ',' + QUOTENAME(Anual) desc FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)')  ,1,1,'') "+
           " select @null2 = STUFF((SELECT Distinct ', isnull(' + QUOTENAME(Anual)+',0) as'  + QUOTENAME(Anual) from  [Encomienda].[DTM_Encomienda_Rutas_Vehiculos]  where Anual=@Año group by ', isnull(' + QUOTENAME(Anual)+',0) as'  + QUOTENAME(Anual) Order by ', isnull(' + QUOTENAME(Anual)+',0) as'  + QUOTENAME(Anual) desc  FOR XML PATH(''), TYPE  ).value('.', 'NVARCHAR(MAX)')   ,1,1,'')  "+
           " select @select= ',(Select sum(CantidadViajes) as total  from [Encomienda].[DTM_Encomienda_Rutas_Vehiculos] b where b.Origen=p.Origen and b.Destino=p.Destino '"+
           " select @from= ' group by Origen, Destino) as total   from  ( select  Origen,Destino,Anual,CantidadViajes  from [Encomienda].[DTM_Encomienda_Rutas_Vehiculos]  ' "+
           " if (@Año!= '' and @Mes = '' and @TipoVehiculo = ''  )   set @consulta = 'SELECT top 10 isnull(concat(Origen,''-'',Destino),0) as Origen ,' + @null2 + ' ' + @select + '  and Anual='''+@Año+''' ' + @from + '  )x  pivot (  sum(CantidadViajes) for Anual in (' + @cols2 + ') )  p order by total desc'  "+
           " if (@Año!= '' and @Mes != '' and @TipoVehiculo != '' ) set @consulta = 'SELECT top 10 isnull(concat(Origen,''-'',Destino),0) as Origen,' + @null2 + ' ' + @select + ' and TipoVehiculo = '''+@TipoVehiculo+'''  and Mes='''+@Mes+'''  and Anual='''+@Año+''' ' + @from + ' where  TipoVehiculo = '''+@TipoVehiculo+'''  and Mes='''+@Mes+''' )x  pivot (  sum(CantidadViajes) for Anual in (' + @cols + ') )  p order by total desc'   "+
           " if (@Año!= '' and @Mes != '' and @TipoVehiculo = '')  set @consulta = 'SELECT top 10 isnull(concat(Origen,''-'',Destino),0) as Origen,' + @null2 + ' ' + @select + ' and Mes ='''+@Mes+'''  and Anual='''+@Año+''' ' + @from + ' where Mes ='''+@Mes+''' )x  pivot (  sum(CantidadViajes) for Anual in (' + @cols2 + ') )  p order by total desc'   "+
           " if (@Año!= '' and @Mes = '' and @TipoVehiculo != '')  set @consulta = 'SELECT top 10 isnull(concat(Origen,''-'',Destino),0) as Origen,' + @null2 + ' ' + @select + ' and TipoVehiculo = '''+@TipoVehiculo+'''  and Anual='''+@Año+''' ' + @from + ' where TipoVehiculo = '''+@TipoVehiculo+'''  )x  pivot (  sum(CantidadViajes) for Anual in (' + @cols2 + ') )  p order by total desc' "+
           " if (@Año= '' and @Mes != '' and @TipoVehiculo != '' ) set @consulta = 'SELECT top 10 isnull(concat(Origen,''-'',Destino),0) as Origen,' + @null + ' ' + @select + ' and  TipoVehiculo = '''+@TipoVehiculo+'''  and Mes='''+@Mes+'''  and Anual='''+@Año+''' ' + @from + ' where  TipoVehiculo = '''+@TipoVehiculo+'''  and Mes='''+@Mes+''' )x  pivot (  sum(CantidadViajes) for Anual in (' + @cols + ') )  p order by total desc'  "+
           " if (@Año= '' and @Mes != '' and @TipoVehiculo = '')  set @consulta = 'SELECT top 10 isnull(concat(Origen,''-'',Destino),0) as Origen,' + @null + ' ' + @select + ' and Mes = '''+@Mes+'''' + @from + ' where Mes = '''+@Mes+''' )x  pivot (  sum(CantidadViajes) for Anual in (' + @cols + ') )  p order by total desc' "+
           " if (@Año= '' and @Mes = '' and @TipoVehiculo != '') set @consulta = 'SELECT top 10 isnull(concat(Origen,''-'',Destino),0) as Origen,' + @null + ' ' + @select + ' and TipoVehiculo = '''+@TipoVehiculo+'''  ' + @from + ' where TipoVehiculo = '''+@TipoVehiculo+'''   )x  pivot (  sum(CantidadViajes) for Anual in (' + @cols + ') )  p order by total desc' "+
           " if (@Año= '' and @Mes = '' and @TipoVehiculo = ''  ) set @consulta = 'SELECT top 10 isnull(concat(Origen,''-'',Destino),0) as Origen,' + @null + ' ' + @select + ' ' + @from + ' ) x  pivot (  sum(CantidadViajes) for Anual in (' + @cols + ') )  p order by total desc' "+
           " execute(@consulta) ";

            return consulta;
        }

        public String Grafica_Encomiendas_RutasVehiculos_GeneralRutaTiempo_AgrupacionTrimestre_SQL(String anio, String tipo){
            consulta=
               " declare  @Mes nvarchar(max),@Año nvarchar(max),@TipoVehiculo nvarchar(max) "+
               " set @TipoVehiculo='"+tipo+"'; set @Año= '"+anio+"'; "+
               " if (@Año = '' and @TipoVehiculo = '' ) select top 10  concat(Origen,'-',Destino) as Origen,Trimestre1=isnull((select  sum(CantidadViajes)/3  from[Encomienda].[DTM_Encomienda_Rutas_Vehiculos] b where Mes between'1' and  '3' and a.Origen=b.Origen and a.Destino=b.Destino group by Origen, Destino),0) ,Trimestre2=isnull((select  sum(CantidadViajes)/3  from[Encomienda].[DTM_Encomienda_Rutas_Vehiculos] b where Mes between'4' and  '6' and a.Origen=b.Origen and a.Destino=b.Destino group by Origen, Destino),0),Trimestre3=isnull((select  sum(CantidadViajes)/3  from[Encomienda].[DTM_Encomienda_Rutas_Vehiculos] b where Mes between'7' and  '9' and a.Origen=b.Origen and a.Destino=b.Destino group by Origen, Destino),0),Trimestre4=isnull((select  sum(CantidadViajes)/3  from[Encomienda].[DTM_Encomienda_Rutas_Vehiculos] b where Mes between'10' and  '12' and a.Origen=b.Origen and  a.Destino=b.Destino group by Origen, Destino),0),(Select sum(CantidadViajes)/3 as total  from [Encomienda].[DTM_Encomienda_Rutas_Vehiculos] c where c.Origen=a.Origen  and c.Destino=a.Destino group by Origen, Destino)as t from [Encomienda].[DTM_Encomienda_Rutas_Vehiculos] a group by Origen,Destino order by t desc "+
                " if (@Año = '' and @TipoVehiculo != '') select top 10  concat(Origen,'-',Destino) as Origen,Trimestre1=isnull((select  sum(CantidadViajes)/3  from[Encomienda].[DTM_Encomienda_Rutas_Vehiculos] b where Mes between'1' and  '3' and a.Origen=b.Origen and a.Destino=b.Destino  and  TipoVehiculo=@TipoVehiculo group by Origen, Destino ),0),Trimestre2=isnull((select  sum(CantidadViajes)/3  from[Encomienda].[DTM_Encomienda_Rutas_Vehiculos] b where Mes between'4' and  '6' and a.Origen=b.Origen and a.Destino=b.Destino and  TipoVehiculo=@TipoVehiculo group by Origen, Destino),0),Trimestre3=isnull((select  sum(CantidadViajes)/3  from[Encomienda].[DTM_Encomienda_Rutas_Vehiculos] b where Mes between'7' and  '9' and a.Origen=b.Origen and a.Destino=b.Destino and  TipoVehiculo=@TipoVehiculo group by Origen, Destino),0),Trimestre4=isnull((select  sum(CantidadViajes)/3  from[Encomienda].[DTM_Encomienda_Rutas_Vehiculos] b where Mes between'10' and  '12' and a.Origen=b.Origen  and a.Destino=b.Destino and  TipoVehiculo=@TipoVehiculo group by Origen, Destino),0),(Select sum(CantidadViajes)/3 as total  from [Encomienda].[DTM_Encomienda_Rutas_Vehiculos] c where c.Origen=a.Origen and c.Destino=a.Destino and  TipoVehiculo=@TipoVehiculo group by Origen, Destino)as t from [Encomienda].[DTM_Encomienda_Rutas_Vehiculos] a group by Origen ,Destino order by t desc "+
                " if (@Año != '' and @TipoVehiculo = '' ) select top 10 concat(Origen,'-',Destino) as Origen,Trimestre1=isnull((select  sum(CantidadViajes)/3  from[Encomienda].[DTM_Encomienda_Rutas_Vehiculos] b where Mes between'1' and  '3' and a.Origen=b.Origen and a.Destino=b.Destino and Anual=@Año  group by Origen, Destino),0),Trimestre2=isnull((select  sum(CantidadViajes)/3  from[Encomienda].[DTM_Encomienda_Rutas_Vehiculos] b where Mes between'4' and  '6' and a.Origen=b.Origen and a.Destino=b.Destino and  Anual=@Año),0),Trimestre3=isnull((select  sum(CantidadViajes)/3  from[Encomienda].[DTM_Encomienda_Rutas_Vehiculos] b where Mes between'7' and  '9' and a.Origen=b.Origen and a.Destino=b.Destino and  Anual=@Año ),0),Trimestre4=isnull((select  sum(CantidadViajes)/3  from[Encomienda].[DTM_Encomienda_Rutas_Vehiculos] b where Mes between'10' and  '12' and a.Origen=b.Origen  and a.Destino=b.Destino and  Anual=@Año ),0),(Select sum(CantidadViajes)/3 as total  from [Encomienda].[DTM_Encomienda_Rutas_Vehiculos] c where c.Origen=a.Origen and c.Destino=a.Destino and Anual=@Año)as t from [Encomienda].[DTM_Encomienda_Rutas_Vehiculos] a group by Origen,Destino order by t desc "+
                " if (@Año != '' and @TipoVehiculo != '') select top 10 concat(Origen,'-',Destino) as Origen,Trimestre1=isnull((select  sum(CantidadViajes)/3  from[Encomienda].[DTM_Encomienda_Rutas_Vehiculos] b where Mes between'1' and  '3' and a.Origen=b.Origen and a.Destino=b.Destino and  TipoVehiculo=@TipoVehiculo and Anual=@Año   group by Origen, Destino),0),Trimestre2=isnull((select  sum(CantidadViajes)/3  from[Encomienda].[DTM_Encomienda_Rutas_Vehiculos] b where Mes between'4' and  '6' and a.Origen=b.Origen and a.Destino=b.Destino and  TipoVehiculo=@TipoVehiculo and Anual=@Año group by Origen, Destino),0),Trimestre3=isnull((select  sum(CantidadViajes)/3  from[Encomienda].[DTM_Encomienda_Rutas_Vehiculos] b where Mes between'7' and  '9' and a.Origen=b.Origen  and a.Destino=b.Destino and   TipoVehiculo=@TipoVehiculo and Anual=@Año  group by Origen, Destino),0),Trimestre4=isnull((select  sum(CantidadViajes)/3  from[Encomienda].[DTM_Encomienda_Rutas_Vehiculos] b where Mes between'10' and  '12' and a.Origen=b.Origen and a.Destino=b.Destino and  TipoVehiculo=@TipoVehiculo and Anual=@Año group by Origen, Destino ),0),(Select sum(CantidadViajes)/3 as total  from [Encomienda].[DTM_Encomienda_Rutas_Vehiculos] c where c.Origen=a.Origen and c.Destino=a.Destino   and  TipoVehiculo=@TipoVehiculo and Anual=@Año group by Origen, Destino)as t from [Encomienda].[DTM_Encomienda_Rutas_Vehiculos] a group by Origen ,Destino order by t desc ";


            return consulta;
        }

          public String Grafica_Encomiendas_RutasVehiculos_GeneralRutaTiempo_AgrupacionBimestre_SQL(String anio, String tipo){
            consulta=
                    " declare  @Año nvarchar(max),@TipoVehiculo nvarchar(max) "+
                    " set @TipoVehiculo='"+tipo+"'; set @Año= '"+anio+"'; "+
                    "   if (@Año = '' and @TipoVehiculo = '' ) select top 10 CONCAT( Origen,'-',Destino ) as Origen,Bimestre1=isnull((select  sum(CantidadViajes)/2  from[Encomienda].[DTM_Encomienda_Rutas_Vehiculos] b where Mes between'1' and  '2' and a.Origen=b.Origen and a.Destino= b.Destino),0),Bimestre2=isnull((select  sum(CantidadViajes)/2  from[Encomienda].[DTM_Encomienda_Rutas_Vehiculos] b where Mes between'3' and  '4' and a.Origen=b.Origen  and a.Destino= b.Destino),0),Bimestre3=isnull((select  sum(CantidadViajes)/2  from[Encomienda].[DTM_Encomienda_Rutas_Vehiculos] b where Mes between'5' and  '6' and a.Origen=b.Origen and a.Destino= b.Destino ),0),Bimestre4=isnull((select  sum(CantidadViajes)/2  from[Encomienda].[DTM_Encomienda_Rutas_Vehiculos] b where Mes between'7' and  '8' and a.Origen=b.Origen and a.Destino= b.Destino ),0),Bimestre5=isnull((select  sum(CantidadViajes)/2  from[Encomienda].[DTM_Encomienda_Rutas_Vehiculos] b where Mes between'9' and  '10' and a.Origen=b.Origen  and a.Destino= b.Destino),0),Bimestre6=isnull((select  sum(CantidadViajes)/2  from[Encomienda].[DTM_Encomienda_Rutas_Vehiculos] b where Mes between'11' and  '12' and a.Origen=b.Origen and a.Destino= b.Destino),0),(Select sum(CantidadViajes)/2 as total  from [Encomienda].[DTM_Encomienda_Rutas_Vehiculos] c where c.Origen=a.Origen  and c.Destino=a.Destino)as t from [Encomienda].[DTM_Encomienda_Rutas_Vehiculos] a group by Origen, Destino order by t desc      "+
                    "  if (@Año = '' and @TipoVehiculo != '') select top 10  CONCAT( Origen,'-',Destino ) as Origen ,Bimestre1=isnull((select  sum(CantidadViajes)/2  from[Encomienda].[DTM_Encomienda_Rutas_Vehiculos] b where Mes between'1' and  '2' and a.Origen=b.Origen and a.Destino= b.Destino and  TipoVehiculo=@TipoVehiculo ),0),Bimestre2=isnull((select  sum(CantidadViajes)/2  from[Encomienda].[DTM_Encomienda_Rutas_Vehiculos] b where Mes between'3' and  '4' and a.Origen=b.Origen and a.Destino= b.Destino and  TipoVehiculo=@TipoVehiculo ),0),Bimestre3=isnull((select  sum(CantidadViajes)/2  from[Encomienda].[DTM_Encomienda_Rutas_Vehiculos] b where Mes between'5' and  '6' and a.Origen=b.Origen and a.Destino= b.Destino and  TipoVehiculo=@TipoVehiculo ),0),Bimestre4=isnull((select  sum(CantidadViajes)/2  from[Encomienda].[DTM_Encomienda_Rutas_Vehiculos] b where Mes between'7' and  '8' and a.Origen=b.Origen and a.Destino= b.Destino and  TipoVehiculo=@TipoVehiculo ),0),Bimestre5=isnull((select  sum(CantidadViajes)/2  from[Encomienda].[DTM_Encomienda_Rutas_Vehiculos] b where Mes between'9' and  '10' and a.Origen=b.Origen and a.Destino= b.Destino and  TipoVehiculo=@TipoVehiculo ),0),Bimestre6=isnull((select  sum(CantidadViajes)/2  from[Encomienda].[DTM_Encomienda_Rutas_Vehiculos] b where Mes between'11' and  '12' and a.Origen=b.Origen and a.Destino= b.Destino and  TipoVehiculo=@TipoVehiculo  ),0),(Select sum(CantidadViajes)/2 as total  from [Encomienda].[DTM_Encomienda_Rutas_Vehiculos] c where c.Origen=a.Origen and c.Destino= a.Destino and  TipoVehiculo=@TipoVehiculo )as t from [Encomienda].[DTM_Encomienda_Rutas_Vehiculos] a group by Origen, Destino order by t desc "+
                    "  if (@Año != '' and @TipoVehiculo = '' ) select top 10 CONCAT( Origen,'-',Destino ) as Origen,Bimestre1=isnull((select  sum(CantidadViajes)/2  from[Encomienda].[DTM_Encomienda_Rutas_Vehiculos] b where Mes between'1' and  '2' and a.Origen=b.Origen and  a.Destino= b.Destino and Anual=@Año ),0),Bimestre2=isnull((select  sum(CantidadViajes)/2  from[Encomienda].[DTM_Encomienda_Rutas_Vehiculos] b where Mes between'3' and  '4' and a.Origen=b.Origen and a.Destino= b.Destino and  Anual=@Año ),0),Bimestre3=isnull((select  sum(CantidadViajes)/2  from[Encomienda].[DTM_Encomienda_Rutas_Vehiculos] b where Mes between'5' and  '6' and a.Origen=b.Origen and a.Destino= b.Destino and Anual=@Año ),0),Bimestre4=isnull((select  sum(CantidadViajes)/2  from[Encomienda].[DTM_Encomienda_Rutas_Vehiculos] b where Mes between'7' and  '8' and a.Origen=b.Origen and a.Destino= b.Destino and Anual=@Año ),0),Bimestre5=isnull((select  sum(CantidadViajes)/2  from[Encomienda].[DTM_Encomienda_Rutas_Vehiculos] b where Mes between'9' and  '10' and a.Origen=b.Origen and a.Destino= b.Destino and  Anual=@Año ),0),Bimestre6=isnull((select  sum(CantidadViajes)/2  from[Encomienda].[DTM_Encomienda_Rutas_Vehiculos] b where Mes between'11' and  '12' and a.Origen=b.Origen and a.Destino= b.Destino and Anual=@Año  ),0),(Select sum(CantidadViajes)/2 as total  from [Encomienda].[DTM_Encomienda_Rutas_Vehiculos] c where c.Origen=a.Origen and c.Destino=a.Destino and  Anual=@Año )as t from [Encomienda].[DTM_Encomienda_Rutas_Vehiculos] a group by Origen ,Destino  order by t desc  "+
                    "  if (@Año != '' and @TipoVehiculo != '') select top 10 CONCAT( Origen,'-',Destino ) as Origen ,Bimestre1=isnull((select  sum(CantidadViajes)/2  from[Encomienda].[DTM_Encomienda_Rutas_Vehiculos] b where Mes between'1' and  '2' and a.Origen=b.Origen and a.Destino= b.Destino and  TipoVehiculo=@TipoVehiculo and Anual=@Año   ),0),Bimestre2=isnull((select  sum(CantidadViajes)/2  from[Encomienda].[DTM_Encomienda_Rutas_Vehiculos] b where Mes between'3' and  '4' and a.Origen=b.Origen and a.Destino= b.Destino and  TipoVehiculo=@TipoVehiculo and Anual=@Año   ),0),Bimestre3=isnull((select  sum(CantidadViajes)/2  from[Encomienda].[DTM_Encomienda_Rutas_Vehiculos] b where Mes between'5' and  '6' and a.Origen=b.Origen and a.Destino= b.Destino and TipoVehiculo=@TipoVehiculo and Anual=@Año   ),0),Bimestre4=isnull((select  sum(CantidadViajes)/2  from[Encomienda].[DTM_Encomienda_Rutas_Vehiculos] b where Mes between'7' and  '8' and a.Origen=b.Origen and a.Destino= b.Destino and  TipoVehiculo=@TipoVehiculo and Anual=@Año   ),0),Bimestre5=isnull((select  sum(CantidadViajes)/2  from[Encomienda].[DTM_Encomienda_Rutas_Vehiculos] b where Mes between'9' and  '10' and a.Origen=b.Origen and a.Destino= b.Destino and  TipoVehiculo=@TipoVehiculo and Anual=@Año   ),0),Bimestre6=isnull((select  sum(CantidadViajes)/2  from[Encomienda].[DTM_Encomienda_Rutas_Vehiculos] b where Mes between'11' and  '12' and a.Origen=b.Origen and a.Destino= b.Destino and  TipoVehiculo=@TipoVehiculo and Anual=@Año    ),0),(Select sum(CantidadViajes)/2 as total  from [Encomienda].[DTM_Encomienda_Rutas_Vehiculos] c where c.Origen=a.Origen and c.Destino= a.Destino and  TipoVehiculo=@TipoVehiculo and Anual=@Año   )as t from [Encomienda].[DTM_Encomienda_Rutas_Vehiculos] a group by Origen, Destino order by t desc ";
            return consulta;
        }

        //MEtodos para Cargar lso combos
         public String Encomiendas_RutasVehiculos_ListarAño_GRT_SQL(String mes, String Tipo){
            consulta=
           " declare  @Tipo nvarchar(max), @Mes varchar(max) " +
           "  set @Tipo='"+Tipo+"';set @Mes= '"+mes+"';  " +
           "  if ( @Mes  != '' and @Tipo = '' ) select distinct Anual as Anio from [Encomienda].[DTM_Encomienda_Rutas_Vehiculos] Where Mes=@Mes order by Anual  " +
           "  if ( @Mes  != '' and @Tipo != '' ) select distinct Anual as Anio from [Encomienda].[DTM_Encomienda_Rutas_Vehiculos] Where TipoVehiculo=@Tipo and Mes=@Mes order by Anual " +
           "  if ( @Mes = '' and @Tipo != '')   select distinct Anual as Anio from [Encomienda].[DTM_Encomienda_Rutas_Vehiculos] Where TipoVehiculo=@Tipo order by Anual " +
           "  if ( @Mes = '' and @Tipo = ''  ) select distinct Anual as Anio from [Encomienda].[DTM_Encomienda_Rutas_Vehiculos]  order by Anual " ; 
            

            return consulta;
         }
         public String Encomiendas_RutasVehiculos_ListarMes_GRT_SQL(String anio, String Tipo){
            consulta=
              " declare  @Tipo nvarchar(max), @Año varchar(max) "+
              "  set @Tipo='"+Tipo+"';set @Año= '"+anio+"'; "+
              "  if ( @Año  != '' and @Tipo = '' ) select distinct Mes as Mes from [Encomienda].[DTM_Encomienda_Rutas_Vehiculos] Where Anual=@Año order by Mes " +
              "  if ( @Año  != '' and @Tipo != '' ) select distinct Mes as Mes from [Encomienda].[DTM_Encomienda_Rutas_Vehiculos] Where TipoVehiculo=@Tipo and Anual=@Año order by Mes" +
              "  if ( @Año = '' and @Tipo != '')   select distinct Mes as Mes from [Encomienda].[DTM_Encomienda_Rutas_Vehiculos] Where TipoVehiculo=@Tipo order by Mes" +
              "  if ( @Año = '' and @Tipo = ''  ) select distinct Mes as Mes from [Encomienda].[DTM_Encomienda_Rutas_Vehiculos]  order by Mes";
            return consulta;
         }
         public String Encomiendas_RutaVehiculos_ListarTipo_GTR_SQL(String anio, String mes){
            consulta =
              " declare  @Año nvarchar(max),@Mes varchar(max); " +
              " set @Año= '" + anio + "';set @Mes= '" + mes + "';" +
              " if ( @Mes  != '' and @Año = '' ) select distinct TipoVehiculo from [Encomienda].[DTM_Encomienda_Rutas_Vehiculos] Where Mes=@Mes order by TipoVehiculo" +
              " if ( @Mes  != '' and @Año != '' ) select distinct TipoVehiculo from [Encomienda].[DTM_Encomienda_Rutas_Vehiculos] Where Anual=@Año and Mes=@Mes order by TipoVehiculo" +
              " if ( @Mes = '' and @Año != '')   select distinct TipoVehiculo from [Encomienda].[DTM_Encomienda_Rutas_Vehiculos] Where Anual=@Año order by TipoVehiculo" +
              " if ( @Mes = '' and @Año = ''  ) select distinct TipoVehiculo from [Encomienda].[DTM_Encomienda_Rutas_Vehiculos] order by TipoVehiculo ";
             return consulta;
         }

        
        // ##### GRAFICA: CANTIDAD VIAJE SEGUN RUTA  ######

        public String Grafica_Encomiendas_RutaVehiculos_CantidadViajesRuta_SQL( String Año, String Mes){
            consulta=
            " declare @Departamento varchar(max), @Mes nvarchar(max),@Año nvarchar(max),@TipoVehiculo nvarchar(max), @query  AS NVARCHAR(MAX), @cols AS NVARCHAR(MAX), @null  AS NVARCHAR(MAX), @cols2 AS NVARCHAR(MAX), @null2  AS NVARCHAR(MAX) "+
           " set @TipoVehiculo=''; set @Año= '"+Año+"';set @Mes= '"+Mes+"';"+
           " select @cols = STUFF((SELECT Distinct top 10 ',' + QUOTENAME(Origen)  from [Encomienda].[DTM_Encomienda_Rutas_Vehiculos] group by ',' + QUOTENAME(Origen)Order by ',' + QUOTENAME(Origen) desc FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)')  ,1,1,'')  "+
           " select @null = STUFF((SELECT Distinct top 10', isnull(' + QUOTENAME(Origen)+',0) as'  + QUOTENAME(Origen) from [Encomienda].[DTM_Encomienda_Rutas_Vehiculos]  group by ', isnull(' + QUOTENAME(Origen)+',0) as'  + QUOTENAME(Origen) Order by ', isnull(' + QUOTENAME(Origen)+',0) as'  + QUOTENAME(Origen) desc  FOR XML PATH(''), TYPE  ).value('.', 'NVARCHAR(MAX)')   ,1,1,'') "+
           " if ( @Mes != '' and @Año = '' and @TipoVehiculo != '' )  	 set @query = 'SELECT concat(Meses,'' de '',Anual) as MesAño,' + @null + '  from  ( select Mes,DATENAME(MONTH,Fecha) as Meses,Anual,Origen,CantidadViajes from [Encomienda].[DTM_Encomienda_Rutas_Vehiculos] where  TipoVehiculo = '''+@TipoVehiculo+''' and Mes='''+@Mes+''' and KeyRuta in(select max(KeyRuta) from [Encomienda].[DTM_Encomienda_Rutas_Vehiculos] where  TipoVehiculo = '''+@TipoVehiculo+''' and Mes='''+@Mes+'''  group by Anual,TipoVehiculo,Origen,Mes,Origen)  ) x  pivot ( sum(CantidadViajes) for Origen in (' + @cols + ') ) p  order by Anual' "+
           " if ( @Mes != '' and @Año = '' and @TipoVehiculo = '')  	 set @query = 'SELECT concat(Meses,'' de '',Anual) as MesAño,' + @null + '  from  ( select Mes,DATENAME(MONTH,Fecha) as Meses,Anual,Origen,CantidadViajes from [Encomienda].[DTM_Encomienda_Rutas_Vehiculos] where Mes = '''+@Mes+''' and KeyRuta in(select max(KeyRuta) from [Encomienda].[DTM_Encomienda_Rutas_Vehiculos]  where Mes = '''+@Mes+''' group by Anual,TipoVehiculo,Origen,Mes,Origen)  ) x  pivot ( sum(CantidadViajes) for Origen in (' + @cols + ') ) p order by Anual' "+
           " if ( @Mes = '' and @Año = '' and @TipoVehiculo = ''  )  	  set @query = 'SELECT concat(Meses,'' de '',Anual) as MesAño,' + @null + '  from  ( select Mes,DATENAME(MONTH,Fecha) as Meses,Anual,Origen,CantidadViajes from [Encomienda].[DTM_Encomienda_Rutas_Vehiculos] where   KeyRuta in(select max(KeyRuta) from [Encomienda].[DTM_Encomienda_Rutas_Vehiculos] group by Anual,TipoVehiculo ,Origen,Mes,Origen)  ) x  pivot ( sum(CantidadViajes) for Origen in (' + @cols + ') ) p order by Anual' "+
           " if ( @Mes = '' and @Año = '' and @TipoVehiculo != '')  	 set @query = 'SELECT concat(Meses,'' de '',Anual) as MesAño,' + @null + '  from  ( select Mes,DATENAME(MONTH,Fecha) as Meses,Anual,Origen,CantidadViajes from [Encomienda].[DTM_Encomienda_Rutas_Vehiculos] where TipoVehiculo = '''+@TipoVehiculo+''' and KeyRuta in(select max(KeyRuta) from [Encomienda].[DTM_Encomienda_Rutas_Vehiculos] where TipoVehiculo = '''+@TipoVehiculo+'''  group by  Anual,TipoVehiculo,Origen,Mes,Origen)   ) x  pivot ( sum(CantidadViajes) for Origen in (' + @cols + ') ) p order by Anual' "+
           " if ( @Mes != '' and @Año != '' and @TipoVehiculo != '' )  	 set @query = 'SELECT concat(Meses,'' de '',Anual) as MesAño,' + @null + '  from  ( select Mes, DATENAME(MONTH,Fecha) as Meses,Anual,Origen,CantidadViajes from [Encomienda].[DTM_Encomienda_Rutas_Vehiculos] where  TipoVehiculo = '''+@TipoVehiculo+''' and Anual= '''+@Año+''' and Mes='''+@Mes+''' and KeyRuta in(select max(KeyRuta) from [Encomienda].[DTM_Encomienda_Rutas_Vehiculos] where  TipoVehiculo = '''+@TipoVehiculo+''' and Mes='''+@Mes+''' and Anual= '''+@Año+''' group by Anual,TipoVehiculo,Origen,Mes,Origen)  ) x  pivot ( sum(CantidadViajes) for Origen in (' + @cols + ') ) p order by Anual' "+
           " if ( @Mes != '' and @Año != '' and @TipoVehiculo = '')  	 set @query = 'SELECT concat(Meses,'' de '',Anual) as MesAño,' + @null + '  from  ( select Mes, DATENAME(MONTH,Fecha) as Meses,Anual,Origen,CantidadViajes from [Encomienda].[DTM_Encomienda_Rutas_Vehiculos] where Mes = '''+@Mes+''' and Anual= '''+@Año+''' and KeyRuta in(select max(KeyRuta) from [Encomienda].[DTM_Encomienda_Rutas_Vehiculos]  where Mes = '''+@Mes+''' and Anual= '''+@Año+''' group by Anual,TipoVehiculo,Origen,Mes,Origen)  ) x  pivot ( sum(CantidadViajes) for Origen in (' + @cols + ') ) p order by Anual' "+
           " if ( @Mes = '' and @Año != '' and @TipoVehiculo = ''  )  	  set @query = 'SELECT concat(Meses,'' de '',Anual) as MesAño,' + @null + '  from  ( select Mes, DATENAME(MONTH,Fecha) as Meses,Anual,Origen,CantidadViajes from [Encomienda].[DTM_Encomienda_Rutas_Vehiculos] where  Anual= '''+@Año+''' and KeyRuta in(select max(KeyRuta) from [Encomienda].[DTM_Encomienda_Rutas_Vehiculos] where Anual= '''+@Año+''' group by Anual,TipoVehiculo,Origen,Mes,Origen)  ) x  pivot ( sum(CantidadViajes) for Origen in (' + @cols + ') ) p order by Mes' "+
           " if ( @Mes = '' and @Año != '' and @TipoVehiculo != '')  	 set @query = 'SELECT concat(Meses,'' de '',Anual) as MesAño,' + @null + '  from  ( select  Mes, DATENAME(MONTH,Fecha) as Meses,Anual,Origen,CantidadViajes from [Encomienda].[DTM_Encomienda_Rutas_Vehiculos] where TipoVehiculo = '''+@TipoVehiculo+'''and Anual= '''+@Año+''' and KeyRuta in(select max(KeyRuta) from [Encomienda].[DTM_Encomienda_Rutas_Vehiculos] where TipoVehiculo = '''+@TipoVehiculo+''' and Anual= '''+@Año+''' group by Anual,TipoVehiculo,Origen,Mes,Origen)   ) x  pivot ( sum(CantidadViajes) for Origen in (' + @cols + ') ) p order by Mes ' "+
           " execute(@query) ";

            return consulta;
        }


        public String Encomiendas_RutaVehiculos_ListarMes_CVR_SQL( String Año){
            consulta=
            " declare  @Tipo nvarchar(max),@Año varchar(max) " +
            " set @Tipo=''; set @Año= '"+Año+"';" +
            " if ( @Año  != '' and @Tipo = '' ) select distinct DATENAME(MONTH,Fecha) as Meses ,Mes from [Encomienda].[DTM_Encomienda_Rutas_Vehiculos] Where Anual=@Año order by Mes " +
            " if ( @Año  != '' and @Tipo != '' ) select distinct DATENAME(MONTH,Fecha) as Meses ,Mes from [Encomienda].[DTM_Encomienda_Rutas_Vehiculos] Where TipoVehiculo=@Tipo and Anual=@Año order by Mes" +
            " if ( @Año = '' and @Tipo != '')   select distinct DATENAME(MONTH,Fecha) as Meses ,Mes from [Encomienda].[DTM_Encomienda_Rutas_Vehiculos] Where TipoVehiculo=@Tipo order by Mes" +
            " if ( @Año = '' and @Tipo = ''  ) select distinct DATENAME(MONTH,Fecha) as Meses ,Mes from [Encomienda].[DTM_Encomienda_Rutas_Vehiculos] order by Mes" ;
            return consulta;

        }

        public String Encomiendas_RutaVehiculos_ListarAño_CVR_SQL( String Mes){
            consulta=
            " declare  @Tipo nvarchar(max),@Mes varchar(max) "+
            " set @Tipo='';set @Mes= '"+Mes+"'; "+
            " if ( @Mes  != '' and @Tipo = '' ) select distinct Anual as Anio from [Encomienda].[DTM_Encomienda_Rutas_Vehiculos] Where Mes=@Mes order by Anual "+
            " if ( @Mes  != '' and @Tipo != '' ) select distinct Anual as Anio from [Encomienda].[DTM_Encomienda_Rutas_Vehiculos] Where TipoVehiculo=@Tipo and Mes=@Mes order by Anual   "+
            " if ( @Mes = '' and @Tipo != '')   select distinct Anual as Anio from [Encomienda].[DTM_Encomienda_Rutas_Vehiculos] Where TipoVehiculo=@Tipo order by Anual "+
            " if ( @Mes = '' and @Tipo = ''  ) select distinct Anual as Anio from [Encomienda].[DTM_Encomienda_Rutas_Vehiculos]  order by Anual ";
            return consulta;

        }


        #endregion

    }
}
