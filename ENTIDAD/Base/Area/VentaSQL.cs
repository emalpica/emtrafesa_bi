﻿using System;

namespace ENTIDAD.Base.Area
{
    public class VentaSQL
    {

        #region KPI Ventas

        public String Volumen_Ventas_Anuales = "select dtm.Ano as Anio, sum(dtm.Ventas_R) as Ventas , sum(dtm.Cantidad_R) as Cantidad from [DT_DA_VENTAS_TOTALES] dtm group by dtm.Ano order by Ventas asc"; //"select dtm.Ano as Anio, CAST((isnull(sum(dtm.Ventas_R)/1000000,0)) AS DECIMAL(10,2)) as Ventas , CAST((isnull(sum(dtm.Cantidad_R)/1000000,0)) AS DECIMAL(10,2)) as Cantidad from[DT_DA_VENTAS_TOTALES] dtm group by dtm.Ano";

        public String Comparativo_Historico = "select Mes ,Ano, CAST( sum(Ventas_R) AS DECIMAL(10,2)) as Ventas from DT_DM_VENTAS_TOTALES group by  Mes, Ano  order by Ano";// " select Mes ,Ano, CAST( sum(Ventas_R) AS DECIMAL(10,2)) as Ventas from DT_DM_VENTAS_TOTALES group by  Mes, Ano  order by Ano ";

        public String Rank_MCM = "SELECT TOP 5 dt.Sucursal, Ventas  = CONVERT(VARCHAR(300),CONVERT(NUMERIC(10,2),(sum(dt.Cantidad_R)/(select sum(ss.Cantidad_R) from[dbo].[DT_DA_VENTAS_TOTALES] ss))*100) )+'%' FROM[dbo].[DT_DA_VENTAS_TOTALES] dt GROUP BY dt.Sucursal order by sum(dt.Cantidad_R) desc";

            public String Rank_NCM = "select dt.Sucursal, Ventas  = CONVERT(VARCHAR(300),CONVERT(NUMERIC(10,2),(sum(dt.Cantidad_R)/(select sum(ss.Cantidad_R)  from[dbo].[DT_DA_VENTAS_TOTALES] ss))*100) )+'%' into #temp from DT_DA_VENTAS_TOTALES dt  GROUP BY dt.Sucursal order by sum(dt.Cantidad_R) asc select top 5 Sucursal,Ventas  from #temp where Ventas!='0.00%'   order  by Ventas asc drop table #temp  ";

        #endregion
    }
}
