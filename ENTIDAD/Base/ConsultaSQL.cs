﻿using System;

namespace ENTIDAD.Base
{
    public class ConsultaSQL
    {
        String consulta;

        #region KPI Mantenimiento

        //KPI: Distribución de Flota:

        public String G_Mantenimiento_CantidadGeneral_Modelo_SQL(String modelo, String marca, String anio, String mes)
        {
            consulta =
            " declare @marca varchar(20),@modelo varchar(20), @anio varchar(5), @mes varchar(5), @filtroSelect varchar(max),@filtroCondicional varchar(max), @filtofinal varchar(max)" +
            " set @filtofinal = 'fecha, '; set @marca = '" + marca + "'; set @modelo = '" + modelo + "'; set @anio = '" + anio + "'; set @mes = '" + mes + "'; set @filtroSelect = ' * '; set @filtroCondicional = ''" +
            " select distinct Modelo into #ModeloT from DTM_Mantenimiento_Disponibilidad_Flotas dtm order by dtm.Modelo desc" +
            " if (@modelo != '') begin set @filtroSelect = ' concat(Mes, ''-'',Año) as Fecha, avg(isnull(' + '\"' + @modelo + '\",0)) '; end" +
            " ELSE" +
            " begin" +
            " set @filtroSelect = (SELECT STUFF((SELECT '\",0)),avg(isnull(\"' + Modelo FROM #ModeloT  FOR  XML PATH('')),1,1, '') +'\",0))'); " +
            " set @filtroSelect = ' concat(Mes, ''-'',Año) as Fecha ' + substring(@filtroSelect, 5, len(@filtroSelect));" +
            " end" +
            " if (@marca != '') set @filtroCondicional = 'where marca =' + '''' + @marca + '''';" +
            " if (@anio != '' and @mes != '' and @marca!= '') set @filtroCondicional = @filtroCondicional + ' and Año=''' + @anio + ''' and Mes=''' + @mes + '''';" +
            " if (@anio != '' and @mes != '' and @marca = '') set @filtroCondicional = N'  WHERE  Año=''' + @anio + ''' and Mes=''' + @mes + '''';" +
            " if (@anio != '' and @mes = '' and @marca = '') set @filtroCondicional = N'  WHERE  Año=''' + @anio + '''';" +
            " if (@anio = '' and @mes != '' and @marca = '') set @filtroCondicional = N'  WHERE  Mes=''' + @mes + '''';" +
            " if (@anio != '' and @mes = '' and @marca!= '') BEGIN set @filtroCondicional = @filtroCondicional + ' and Año=''' + @anio + ''''; end" +
            " if (@anio = '' and @mes != '' and @marca!= '')BEGIN set @filtroCondicional = @filtroCondicional + ' and Mes=''' + @mes + ''''; end" +
            " select distinct(Modelo) into #TemporalEjeYName from DTM_Mantenimiento_Disponibilidad_Flotas " +
            " DECLARE @cadena varchar(max)" +
            " set @cadena = N'select max(KeyDisponibilidadFlota) as ID, Año,Mes,(Convert(decimal(10, 2), (sum(TiempoOperativo)/(sum(TiempoOperativo) +  sum(TiempoReparacion))))) as KPI,marca, modelo into #temp from DTM_Mantenimiento_Disponibilidad_Flotas group by Año, Mes, modelo, marca, TiempoOperativo, TiempoReparacion order by Año,Mes SELECT * into #result" +
            " FROM(SELECT * FROM #temp)  p PIVOT (avg(KPI)for Modelo in('+(select STUFF((select cast(',\"' as varchar(max)) + (modelo) + cast('\"' as varchar(max)) from #TemporalEjeYName   order by  modelo  for xml path('')), 1, 1, '') as modelo) + '))  as GRAFICA_TABLE  select  '+ @filtroSelect +' from #result '+ @filtroCondicional +'group  by Año, Mes order by Año,Mes'; " +
            " exec(@cadena) drop table #TemporalEjeYName,#ModeloT";
            return consulta;
        }
   
        public String Disponibilidad_Flotas(String modelo, String marca, String anio, String mes)
        {
            consulta =
             " declare @año varchar(20), @mes nvarchar(max),@modelo nvarchar(max),@marca nvarchar(max), @filtro  NVARCHAR(MAX), @cols NVARCHAR(MAX), @null NVARCHAR(MAX), @consulta NVARCHAR(MAX), @cols2 NVARCHAR(MAX), @null2 NVARCHAR(MAX), @cols3 NVARCHAR(MAX), @null3 NVARCHAR(MAX); " +
             " set @año= '" + anio + "';set @modelo='" + modelo + "'; set @marca= '" + marca + "'; set @Mes='" + mes + "'; set @consulta =''; " +
            " SELECT [Marca] ,[Modelo] ,[Placa] ,[Año] ,[Mes] ,[Fecha] ,sum([TiempoOperativo]/([TiempoOperativo]+[TiempoReparacion])) as Prom into #temp FROM [DTM_Mantenimiento_Disponibilidad_Flotas] group by[Marca] ,[Modelo] ,[Placa] ,[Año],[Mes],[Fecha] " +
             " select @cols = STUFF((SELECT Distinct ',' + QUOTENAME(Marca)  from #temp group by ',' + QUOTENAME(Marca)Order by ',' + QUOTENAME(Marca) FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)')  ,1,1,'') " +
             " select @null = STUFF((SELECT Distinct ', isnull(' + QUOTENAME(Marca)+',0) as'  + QUOTENAME(Marca) from #temp group by ', isnull(' + QUOTENAME(Marca)+',0) as'  + QUOTENAME(Marca) Order by ', isnull(' + QUOTENAME(Marca)+',0) as'  + QUOTENAME(Marca)   FOR XML PATH(''), TYPE  ).value('.', 'NVARCHAR(MAX)')   ,1,1,'') " +
             " select @cols2 = STUFF((SELECT Distinct ',' + QUOTENAME(Modelo)  from #temp  Where Marca=@marca group by ',' + QUOTENAME(Modelo) Order by ',' + QUOTENAME(Modelo) FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)')  ,1,1,'') " +
             " select @null2 = STUFF((SELECT Distinct ', isnull(' + QUOTENAME(Modelo)+',0) as'  + QUOTENAME(Modelo) from #temp  Where Marca=@marca group by ', isnull(' + QUOTENAME(Modelo)+',0) as'  + QUOTENAME(Modelo) Order by ', isnull(' + QUOTENAME(Modelo)+',0) as'  + QUOTENAME(Modelo)   FOR XML PATH(''), TYPE  ).value('.', 'NVARCHAR(MAX)')   ,1,1,'') " +
             " select @cols3 = STUFF((SELECT Distinct ',' + QUOTENAME(Modelo)  from #temp  Where Marca=@marca AND Modelo=@modelo group by ',' + QUOTENAME(Modelo) Order by ',' + QUOTENAME(Modelo) FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)')  ,1,1,'') " +
             " select @null3 = STUFF((SELECT Distinct ', isnull(' + QUOTENAME(Modelo)+',0) as'  + QUOTENAME(Modelo) from #temp  Where Marca=@marca AND Modelo=@modelo group by ', isnull(' + QUOTENAME(Modelo)+',0) as'  + QUOTENAME(Modelo) Order by ', isnull(' + QUOTENAME(Modelo)+',0) as'  + QUOTENAME(Modelo)   FOR XML PATH(''), TYPE  ).value('.', 'NVARCHAR(MAX)')   ,1,1,'') " +
             " if  ( @año = '' and @mes != '' and @modelo != '' and @marca != '' )  set @consulta= 'SELECT Concat(Meses,'' de '',Año),' + @null3 + '  from  ( select Mes, DATENAME(MONTH,Fecha) as Meses,Año,Modelo,Prom from #temp  where Marca='''+@marca+'''  and Mes='''+@mes+''' and Modelo='''+@modelo+''') x  pivot ( avg(Prom) for Modelo in (' + @cols3 + ') ) p  order by Año' " +
             " if  ( @año = ''  and @mes != '' and @modelo  = '' and @marca != '' )  	 set @consulta= 'SELECT Concat(Meses,'' de '',Año),' + @null2 + '  from  ( select Mes, DATENAME(MONTH,Fecha) as Meses,Año,Modelo,Prom from #temp where Marca='''+@marca+'''  and Mes='''+@mes+''' ) x  pivot ( avg(Prom) for Modelo in (' + @cols2 + ') ) p  order by Año'  " +
             " if  ( @año = '' and @mes != '' and @modelo != '' and @marca = '')  	set @consulta= 'SELECT Concat(Meses,'' de '',Año),' + @null2 + '  from  ( select Mes, DATENAME(MONTH,Fecha) as Meses,Año,Modelo,Prom from #temp  where Modelo='''+@modelo+''' and Mes='''+@mes+''') x  pivot ( avg(Prom) for Modelo in (' + @cols2 + ') ) p  order by Año' " +
             " if  ( @año = ''  and @mes = '' and @modelo  != '' and @marca != '' )  	  set @consulta= 'SELECT Concat(Meses,'' de '',Año),' + @null3 + '  from  ( select Mes, DATENAME(MONTH,Fecha) as Meses,Año,Modelo,Prom from #temp where Modelo='''+@modelo+''' and Marca='''+@marca+'''  ) x  pivot ( avg(Prom) for Modelo in (' + @cols3 + ') ) p  order by Año' " +
             " if  ( @año = '' and @mes = '' and @modelo = '' and @marca != '')  	 set @consulta= 'SELECT Concat(Meses,'' de '',Año),' + @null2 + '  from  ( select Mes, DATENAME(MONTH,Fecha) as Meses,Año,Modelo,Prom from #temp where Marca='''+@marca+'''  ) x  pivot ( avg(Prom) for Modelo in (' + @cols2 + ') ) p  order by Año' " +
             " if  ( @año = ''  and @mes = '' and @modelo  != '' and @marca = '' ) 	 set @consulta= 'SELECT Concat(Meses,'' de '',Año),' + @null2 + '  from  ( select Mes, DATENAME(MONTH,Fecha) as Meses,Año,Modelo,Prom from #temp where Modelo='''+@modelo+''' ) x  pivot ( avg(Prom) for Modelo in (' + @cols2 + ') ) p  order by Año' " +
             " if  ( @año = '' and @mes != '' and @modelo = '' and @marca = '')  	 set @consulta= 'SELECT Concat(Meses,'' de '',Año),' + @null + '  from  ( select Mes, DATENAME(MONTH,Fecha) as Meses,Año,Marca,Prom from #temp  where Mes='''+@mes+''') x  pivot ( avg(Prom) for Marca in (' + @cols + ') ) p  order by Año' " +
             " if  ( @año = '' and @mes = '' and @modelo = '' and @marca = '')  	set @consulta= 'SELECT Concat(Meses,'' de '',Año),' + @null + '  from  ( select Mes, DATENAME(MONTH,Fecha) as Meses,Año,Marca,Prom from #temp  ) x  pivot ( avg(Prom) for Marca in (' + @cols + ') ) p  order by Año'  " +
             " if  ( @año != '' and @mes != '' and @modelo != '' and @marca != '' )  set @consulta= 'SELECT Concat(Meses,'' de '',Año),' + @null3 + '  from  ( select Mes, DATENAME(MONTH,Fecha) as Meses,Año,Modelo,Prom from #temp  where Marca='''+@marca+'''  and Mes='''+@mes+''' and Modelo='''+@modelo+''' and Año='''+@año+''') x  pivot ( avg(Prom) for Modelo in (' + @cols3 + ') ) p  order by Mes' " +
             " if  ( @año != ''  and @mes != '' and @modelo  = '' and @marca != '' )  	 set @consulta= 'SELECT Concat(Meses,'' de '',Año),' + @null2 + '  from  ( select Mes, DATENAME(MONTH,Fecha) as Meses,Año,Modelo,Prom from #temp where Marca='''+@marca+'''  and Mes='''+@mes+''' and Año='''+@año+''') x  pivot ( avg(Prom) for Modelo in (' + @cols2 + ') ) p  order by Mes' " +
             " if  ( @año != '' and @mes != '' and @modelo != '' and @marca = '')  	set @consulta= 'SELECT Concat(Meses,'' de '',Año),' + @null2 + '  from  ( select Mes, DATENAME(MONTH,Fecha) as Meses,Año,Modelo,Prom from #temp  where Modelo='''+@modelo+''' and Mes='''+@mes+''' and Año='''+@año+''') x  pivot ( avg(Prom) for Modelo in (' + @cols2 + ') ) p  order by Mes' " +
             " if  ( @año != ''  and @mes = '' and @modelo  != '' and @marca != '' )  	  set @consulta= 'SELECT Concat(Meses,'' de '',Año),' + @null3 + '  from  ( select Mes, DATENAME(MONTH,Fecha) as Meses,Año,Modelo,Prom from #temp where Modelo='''+@modelo+''' and Marca='''+@marca+''' and Año='''+@año+''' ) x  pivot ( avg(Prom) for Modelo in (' + @cols3 + ') ) p  order by Mes'  " +
             " if  ( @año != '' and @mes = '' and @modelo = '' and @marca != '')  	 set @consulta= 'SELECT Concat(Meses,'' de '',Año),' + @null2 + '  from  ( select Mes, DATENAME(MONTH,Fecha) as Meses,Año,Modelo,Prom from #temp where Marca='''+@marca+'''  and Año='''+@año+''') x  pivot ( avg(Prom) for Modelo in (' + @cols2 + ') ) p  order by Mes' " +
             " if  ( @año != ''  and @mes = '' and @modelo  != '' and @marca = '' ) 	 set @consulta= 'SELECT Concat(Meses,'' de '',Año),' + @null2 + '  from  ( select Mes, DATENAME(MONTH,Fecha) as Meses,Año,Modelo,Prom from #temp where Modelo='''+@modelo+''' and Año='''+@año+''') x  pivot ( avg(Prom) for Modelo in (' + @cols2 + ') ) p  order by Mes' " +
             " if  ( @año != '' and @mes != '' and @modelo = '' and @marca = '')  	 set @consulta= 'SELECT Concat(Meses,'' de '',Año),' + @null + '  from  ( select Mes, DATENAME(MONTH,Fecha) as Meses,Año,Marca,Prom from #temp  where Mes='''+@mes+''' and Año='''+@año+''') x  pivot ( avg(Prom) for Marca in (' + @cols + ') ) p  order by Mes' " +
             " if  ( @año != '' and @mes = '' and @modelo = '' and @marca = '')  	set @consulta= 'SELECT Concat(Meses,'' de '',Año),' + @null + '  from  ( select Mes, DATENAME(MONTH,Fecha) as Meses,Año,Marca,Prom from #temp  where  Año='''+@año+''') x  pivot ( avg(Prom) for Marca in (' + @cols + ') ) p  order by Mes' " +
             " execute(@consulta) drop table #temp ";
            return consulta;
        }

        public String F_Mantenimiento_General_ListarMarca_SQL(String filtroModelo, String anio, String mes, String tabla)
        {
            consulta =
            //KPI: MANTENIMIENTO - GENERAL -FILTRO MARCA
            //ERIKA MALPICA -10 / 07 / 2019
            " declare @modelo varchar(max), @anio char(5), @mes char(5), @consulta varchar(max), @tabla varchar(max)" +
            " set @modelo = '" + filtroModelo + "'; set @anio = '" + anio + "'; set @mes = '" + mes + "'; set @tabla = '" + tabla + "';" +
            " set @consulta = N'select distinct dtm.Marca as Marca from ' + @tabla + ' dtm '" +
            " if (@modelo != '' and @anio = '' and @mes = '') set @consulta = @consulta + 'where dtm.Modelo=''' + @modelo + ''''" +
            " if (@modelo = '' and @anio != '' and @mes = '') set @consulta = @consulta + 'where dtm.Año=''' + @anio + ''''" +
            " if (@modelo = '' and @anio = '' and @mes != '') set @consulta = @consulta + 'where  dtm.Mes=''' + @mes + ''''" +
            " if (@modelo != '' and @anio = '' and @mes != '') set @consulta = @consulta + 'where dtm.Modelo=''' + @modelo + ''' and  dtm.Mes=''' + @mes + ''''" +
            " if (@modelo != '' and @anio != '' and @mes = '') set @consulta = @consulta + 'where dtm.Modelo=''' + @modelo + ''' and  dtm.Año=''' + @anio + ''''" +
            " if (@modelo = '' and @anio != '' and @mes != '') set @consulta = @consulta + 'where dtm.Año=''' + @anio + ''' and  dtm.Mes=''' + @mes + ''''" +
            " if (@modelo != '' and @anio != '' and @mes != '')  set @consulta = @consulta + 'where dtm.Modelo=''' + @modelo + '''' + ' and  dtm.Año=''' + @anio + ''' and  dtm.Mes=''' + @mes + ''''" +
            " exec(@consulta)";

            return consulta;
        }

        public String F_DisponibilidadFlota_General_ListarTrimestre_SQL(String filtroModelo, String fechaInicio, String fechaFin, String marca)
        {
            consulta =
            " declare @marca varchar(max),@modelo varchar(max), @fechaInicio varchar(max), @fechaFin varchar(max), @consulta varchar(max)" +
            " set @marca = '" + marca + "'; set @modelo = '" + filtroModelo + "'; set @fechaInicio = '" + fechaInicio + "'; set @fechaFin = '" + fechaFin + "'" +
            " set @consulta = 'select distinct CONVERT(varchar(20),CONVERT( int, dtm.Mes )/3) as Trimestre  from DTM_Mantenimiento_Disponibilidad_Flotas dtm where '" +
            " if (@marca = '' and @modelo = '' and(@fechaInicio = '' and @fechaFin = '')) set @consulta = @consulta" +
            " if (@marca != '' and @modelo = '' and(@fechaInicio = '' and @fechaFin = '')) set @consulta = @consulta + ' dtm.Marca=''' + @marca + '''' + ' and'" +
            " if (@marca = '' and @modelo!= '' and(@fechaInicio = '' and @fechaFin = '')) set @consulta = @consulta + ' dtm.Modelo=''' + @modelo + '''' + ' and'" +
            " if (@marca = '' and @modelo = '' and(@fechaInicio != '' and @fechaFin != '')) set @consulta = @consulta + ' dtm.Fecha between ''' + @fechaInicio + ''' and ''' + @fechaFin + '''' + ' and'" +
            " if (@marca != '' and @modelo!= '' and(@fechaInicio = '' and @fechaFin = '')) set @consulta = @consulta + ' dtm.Marca=''' + @marca + '''' + ' and ' + ' dtm.Modelo=''' + @modelo + '''' + ' and'" +
            " if (@marca != '' and @modelo!= '' and(@fechaInicio != '' and @fechaFin != '')) set @consulta = @consulta + ' dtm.Marca=''' + @marca + '''' + ' and ' + ' dtm.Modelo=''' + @modelo + '''' + ' and  dtm.Fecha between ''' + @fechaInicio + ''' and ''' + @fechaFin + '''' + ' and'" +
            " if (@marca != '' and @modelo = '' and(@fechaInicio != '' and @fechaFin != '')) set @consulta = @consulta + ' dtm.Marca=''' + @marca + '''' + ' and  dtm.Fecha between ''' + @fechaInicio + ''' and ''' + @fechaFin + '''' + ' and'" +
            " if (@marca = '' and @modelo!= '' and(@fechaInicio != '' and @fechaFin != '')) set @consulta = @consulta + ' dtm.Modelo=''' + @modelo + '''' + ' and  dtm.Fecha between ''' + @fechaInicio + ''' and ''' + @fechaFin + '''' + ' and'" +
            " set @consulta = @consulta + ' (dtm.Mes=''3'' or dtm.Mes=''6'' or dtm.Mes=''9'' or dtm.Mes=''12'') '" +
            " exec(@consulta)";



            return consulta;
        }

        public String F_DisponibilidadFlota_General_ListarAnio_SQL(String filtroMarca, String filtroModelo, String filtroMes, String tabla)
        {
            consulta =
            //KPI: MANTENIMIENTO - GENERAL -AÑO
            //ERIKA MALPICA -17 / 07 / 2019
            " declare @marca varchar(max),@modelo varchar(max), @anio varchar(max), @mes varchar(max), @consulta varchar(max), @tabla varchar(max)" +
            " set @marca = '" + filtroMarca + "'; set @modelo = '" + filtroModelo + "'; set @mes = '" + filtroMes + "'; set @tabla = '" + tabla + "'" +
            " set @consulta = 'select distinct dtm.Año as Anio from ' + @tabla + ' dtm '" +
            " if (@marca != '' and @modelo = '' and @mes = '') set @consulta = @consulta + ' where dtm.Marca=''' + @marca + ''''" +
            " if (@marca = '' and @modelo!= ''  and @mes = '') set @consulta = @consulta + ' where dtm.Modelo=''' + @modelo + ''''" +
            " if (@marca = '' and @modelo = ''   and @mes != '') set @consulta = @consulta + ' where  dtm.Mes=''' + @mes + ''''" +
            " if (@marca != '' and @modelo!= '' and @mes = '') set @consulta = @consulta + ' where dtm.Marca=''' + @marca + '''' + ' and dtm.Modelo=''' + @modelo + ''''" +
            " if (@marca != '' and @modelo!= ''  and @mes != '') set @consulta = @consulta + ' where dtm.Marca=''' + @marca + '''' + ' and  dtm.Modelo=''' + @modelo + '''' + ' and dtm.Mes=''' + @mes + ''''" +
            " if (@marca != '' and @modelo = ''  and @mes != '') set @consulta = @consulta + ' where dtm.Marca=''' + @marca + '''' + '  and dtm.Mes=''' + @mes + ''''" +
            " if (@marca = '' and @modelo!= ''   and @mes != '') set @consulta = @consulta + ' where dtm.Modelo=''' + @modelo + '''' + '  and dtm.Mes=''' + @mes + ''''" +
            " set @consulta = @consulta + ' order by dtm.Año '" +
            " exec(@consulta)";

         

            return consulta;
        }

        public String F_DisponibilidadFlota_General_ListarMes_SQL(String filtroMarca, String filtroModelo, String filtroAnio, String tabla)
        {
            consulta =
            //KPI: MANTENIMIENTO - GENERAL -MES
            //ERIKA MALPICA -17 / 07 / 2019
            " declare @marca varchar(max),@modelo varchar(max), @anio varchar(max), @mes varchar(max), @consulta varchar(max), @tabla varchar(max)" +
            " set @marca = '" + filtroMarca + "'; set @modelo = '" + filtroModelo + "'; set @anio = '" + filtroAnio + "'; set @tabla = '" + tabla + "'" +
            " set @consulta = 'select distinct DATENAME(MONTH,Fecha) as Meses ,dtm.Mes as Mes from ' + @tabla + ' dtm '" +
            " if (@marca != '' and @modelo = '' and @anio = '') set @consulta = @consulta + ' where dtm.Marca=''' + @marca + ''''" +
            " if (@marca = '' and @modelo!= ''  and @anio = '') set @consulta = @consulta + ' where dtm.Modelo=''' + @modelo + ''''" +
            " if (@marca = '' and @modelo = ''   and @anio != '') set @consulta = @consulta + ' where  dtm.Año=''' + @anio + ''''" +
            " if (@marca != '' and @modelo!= '' and @anio = '') set @consulta = @consulta + ' where dtm.Marca=''' + @marca + '''' + ' and dtm.Modelo=''' + @modelo + ''''" +
            " if (@marca != '' and @modelo!= ''  and @anio != '') set @consulta = @consulta + ' where dtm.Marca=''' + @marca + '''' + ' and  dtm.Modelo=''' + @modelo + '''' + ' and dtm.Año=''' + @anio + ''''" +
            " if (@marca != '' and @modelo = ''  and @anio != '') set @consulta = @consulta + ' where dtm.Marca=''' + @marca + '''' + '  and dtm.Año=''' + @anio + ''''" +
            " if (@marca = '' and @modelo!= ''   and @anio != '') set @consulta = @consulta + ' where dtm.Modelo=''' + @modelo + '''' + '  and dtm.Año=''' + @anio + ''''" +
            " set @consulta = @consulta + ' order by dtm.Mes '" +
            " exec(@consulta)";
            return consulta;
        }

        public String F_Mantenimiento_General_ListarModelo_SQL(String filtroMarca, String filtroAnio, String filtroMes, String tabla)
        {
            consulta =
            //KPI: MANTENIMIENTO - GENERAL -FILTRO MODELO
            //ERIKA MALPICA -15 / 07 / 2019
            " declare @marca varchar(max), @anio char(5), @mes char(5), @consulta varchar(max),@tabla varchar(max)" +
            " set @marca = '" + filtroMarca + "'; set @anio = '" + filtroAnio + "'; set @mes = '" + filtroMes + "'; set @tabla = '" + tabla + "';" +
            " set @consulta = N'select distinct dtm.Modelo as Modelo from ' + @tabla + ' dtm '" +
            " if (@marca != '' and @anio = '' and @mes = '') set @consulta = @consulta + 'where dtm.Marca=''' + @marca + ''''" +
            " if (@marca = '' and @anio != '' and @mes = '') set @consulta = @consulta + 'where dtm.Año=''' + @anio + ''''" +
            " if (@marca = '' and @anio = '' and @mes != '') set @consulta = @consulta + 'where dtm.Mes=''' + @mes + ''''" +
            " if (@marca = '' and @anio != '' and @mes != '') set @consulta = @consulta + 'where dtm.Año''' + @anio + ''' and dtm.Mes''' + @mes + ''''" +
            " if (@marca != '' and @anio != '' and @mes != '')  set @consulta = @consulta + 'where dtm.Marca=''' + @marca + '''' + ' and dtm.Año''' + @anio + ''' and dtm.Mes''' + @mes + ''''" +
            " set @consulta = @consulta + ' order by dtm.Modelo'" +
            " exec(@consulta)";


            return consulta;
        }

        
        //KPI: Fallas del sistema:
        public String Cantidad_Fallas(String marca, String tipoMantenimiento, String anio, String mes)
        { //Giampiere Parimango -15 / 09 / 2019
            consulta =
       " declare @marca varchar(40), @año varchar(10), @mes varchar(10), @cadena varchar(max) , @TipoMantenimiento varchar(max)" +
       " set @marca = '" + marca + "'; set @año = '" + anio + "'; set @mes = '" + mes + "';set @TipoMantenimiento='" + tipoMantenimiento + "';" +
       " set @cadena = 'SELECT FS.SistemaImplicado, CantidadFalla = (count(FS.SistemaImplicado)) FROM DTM_Mantenimiento_Fallas_Sistema FS '" +
       " IF(@año = '' and @TipoMantenimiento = '' and @marca != ''  and @mes = '') SET @cadena = @cadena + '  where  FS.Marca = ''' + @marca + ''' group by  FS.SistemaImplicado order by CantidadFalla asc'" +
       " IF(@año = '' and @TipoMantenimiento = '' and @marca = ''  and @mes != '')SET @cadena = @cadena + ' where    FS.Mes= ''' + @mes + '''  group by  FS.SistemaImplicado order by CantidadFalla asc '" +
       " IF(@año = '' and @TipoMantenimiento = '' and @marca != ''  and @mes != '')SET @cadena = @cadena + ' where   FS.Marca = ''' + @marca + ''' and FS.Mes= ''' + @mes + ''' group by  FS.SistemaImplicado order by CantidadFalla asc ' " +
       " IF(@año = '' and @TipoMantenimiento = '' and @marca = ''  and @mes = '')SET @cadena = @cadena + ' group by  FS.SistemaImplicado order by CantidadFalla asc '" +
       " IF(@año = '' and @TipoMantenimiento != '' and @marca != ''  and @mes = '') SET @cadena = @cadena + '  where  FS.TipoMantenimiento = ''' + @TipoMantenimiento + ''' and FS.Marca = ''' + @marca + ''' group by  FS.SistemaImplicado order by CantidadFalla asc' " +
       " IF(@año = '' and @TipoMantenimiento != '' and @marca = ''  and @mes != '')SET @cadena = @cadena + ' where  FS.TipoMantenimiento = ''' + @TipoMantenimiento + ''' and FS.Mes= ''' + @mes + ''' group by  FS.SistemaImplicado order by CantidadFalla asc ' " +
       " IF(@año = '' and @TipoMantenimiento != '' and @marca != ''  and @mes != '')SET @cadena = @cadena + ' where  FS.TipoMantenimiento = ''' + @TipoMantenimiento + '''and  FS.Marca = ''' + @marca + ''' and FS.Mes= ''' + @mes + '''  group by  FS.SistemaImplicado order by CantidadFalla asc ' " +
       " IF(@año = '' and @TipoMantenimiento != '' and @marca = ''  and @mes = '')SET @cadena = @cadena + '  where   FS.TipoMantenimiento = ''' + @TipoMantenimiento + ''' group by  FS.SistemaImplicado order by CantidadFalla asc ' " +
       " IF(@año != '' and @TipoMantenimiento = '' and @marca != ''  and @mes = '') SET @cadena = @cadena + '  where  FS.Marca = ''' + @marca + ''' and FS.Año = ''' + @año + ''' group by  FS.SistemaImplicado order by CantidadFalla asc' " +
       " IF(@año != '' and @TipoMantenimiento = '' and @marca = ''  and @mes != '')SET @cadena = @cadena + ' where    FS.Mes= ''' + @mes + '''  and FS.Año = ''' + @año + ''' group by  FS.SistemaImplicado order by CantidadFalla asc ' " +
       " IF(@año != '' and @TipoMantenimiento = '' and @marca != ''  and @mes != '')SET @cadena = @cadena + ' where   FS.Marca = ''' + @marca + ''' and FS.Mes= ''' + @mes + ''' and FS.Año = ''' + @año + ''' group by  FS.SistemaImplicado order by CantidadFalla asc ' " +
       " IF(@año != '' and @TipoMantenimiento = '' and @marca = ''  and @mes = '')SET @cadena = @cadena + '  where  FS.Año = ''' + @año + ''' group by  FS.SistemaImplicado order by CantidadFalla asc '" +
       " IF(@año != '' and @TipoMantenimiento != '' and @marca != ''  and @mes = '') SET @cadena = @cadena + '  where  FS.TipoMantenimiento = ''' + @TipoMantenimiento + ''' and FS.Marca = ''' + @marca + ''' and FS.Año = ''' + @año + ''' group by  FS.SistemaImplicado order by CantidadFalla asc' " +
       " IF(@año != '' and @TipoMantenimiento != '' and @marca = ''  and @mes != '')SET @cadena = @cadena + ' where  FS.TipoMantenimiento = ''' + @TipoMantenimiento + ''' and FS.Mes= ''' + @mes + ''' and FS.Año = ''' + @año + ''' group by  FS.SistemaImplicado order by CantidadFalla asc '" +
       " IF(@año != '' and @TipoMantenimiento != '' and @marca != ''  and @mes != '')SET @cadena = @cadena + ' where  FS.TipoMantenimiento = ''' + @TipoMantenimiento + '''and  FS.Marca = ''' + @marca + ''' and FS.Mes= ''' + @mes + '''  and FS.Año = ''' + @año + ''' group by  FS.SistemaImplicado order by CantidadFalla asc ' " +
       " IF(@año != '' and @TipoMantenimiento != '' and @marca = ''  and @mes = '')SET @cadena = @cadena + '  where   FS.TipoMantenimiento = ''' + @TipoMantenimiento + ''' and FS.Año = ''' + @año + ''' group by  FS.SistemaImplicado order by CantidadFalla asc '" +
       " exec(@cadena)";

            return consulta;
        }
        public String Promedio_Fallas(String marca, String tipoMantenimiento, String anio, String  mes)
        {//Giampiere Parimango -15 / 09 / 2019
            consulta =
        " declare @marca varchar(40), @año varchar(10), @mes varchar(10), @cadena varchar(max) , @TipoMantenimiento varchar(max)" +
       " set @marca = '" + marca + "'; set @año = '" + anio + "'; set @mes = '" + mes + "';set @TipoMantenimiento='" + tipoMantenimiento + "';" +
      "  set @cadena = 'SELECT FS.SistemaImplicado, CantidadFalla = (count(FS.SistemaImplicado)) FROM DTM_Mantenimiento_Fallas_Sistema FS '" +
       " IF(@año = '' and @TipoMantenimiento = '' and @marca != ''  and @mes = '') SET @cadena = @cadena + '  where  FS.Marca = ''' + @marca + ''' group by  FS.SistemaImplicado order by CantidadFalla asc'" +
       " IF(@año = '' and @TipoMantenimiento = '' and @marca = ''  and @mes != '')SET @cadena = @cadena + ' where    FS.Mes= ''' + @mes + '''  group by  FS.SistemaImplicado order by CantidadFalla asc '" +
       " IF(@año = '' and @TipoMantenimiento = '' and @marca != ''  and @mes != '')SET @cadena = @cadena + ' where   FS.Marca = ''' + @marca + ''' and FS.Mes= ''' + @mes + ''' group by  FS.SistemaImplicado order by CantidadFalla asc ' " +
       " IF(@año = '' and @TipoMantenimiento = '' and @marca = ''  and @mes = '')SET @cadena = @cadena + ' group by  FS.SistemaImplicado order by CantidadFalla asc '" +
       " IF(@año = '' and @TipoMantenimiento != '' and @marca != ''  and @mes = '') SET @cadena = @cadena + '  where  FS.TipoMantenimiento = ''' + @TipoMantenimiento + ''' and FS.Marca = ''' + @marca + ''' group by  FS.SistemaImplicado order by CantidadFalla asc' " +
       " IF(@año = '' and @TipoMantenimiento != '' and @marca = ''  and @mes != '')SET @cadena = @cadena + ' where  FS.TipoMantenimiento = ''' + @TipoMantenimiento + ''' and FS.Mes= ''' + @mes + ''' group by  FS.SistemaImplicado order by CantidadFalla asc ' " +
       " IF(@año = '' and @TipoMantenimiento != '' and @marca != ''  and @mes != '')SET @cadena = @cadena + ' where  FS.TipoMantenimiento = ''' + @TipoMantenimiento + '''and  FS.Marca = ''' + @marca + ''' and FS.Mes= ''' + @mes + '''  group by  FS.SistemaImplicado order by CantidadFalla asc ' " +
       " IF(@año = '' and @TipoMantenimiento != '' and @marca = ''  and @mes = '')SET @cadena = @cadena + '  where   FS.TipoMantenimiento = ''' + @TipoMantenimiento + ''' group by  FS.SistemaImplicado order by CantidadFalla asc ' " +
       " IF(@año != '' and @TipoMantenimiento = '' and @marca != ''  and @mes = '') SET @cadena = @cadena + '  where  FS.Marca = ''' + @marca + ''' and FS.Año = ''' + @año + ''' group by  FS.SistemaImplicado order by CantidadFalla asc' " +
       " IF(@año != '' and @TipoMantenimiento = '' and @marca = ''  and @mes != '')SET @cadena = @cadena + ' where    FS.Mes= ''' + @mes + '''  and FS.Año = ''' + @año + ''' group by  FS.SistemaImplicado order by CantidadFalla asc ' " +
       " IF(@año != '' and @TipoMantenimiento = '' and @marca != ''  and @mes != '')SET @cadena = @cadena + ' where   FS.Marca = ''' + @marca + ''' and FS.Mes= ''' + @mes + ''' and FS.Año = ''' + @año + ''' group by  FS.SistemaImplicado order by CantidadFalla asc ' " +
       " IF(@año != '' and @TipoMantenimiento = '' and @marca = ''  and @mes = '')SET @cadena = @cadena + '  where  FS.Año = ''' + @año + ''' group by  FS.SistemaImplicado order by CantidadFalla asc '" +
       " IF(@año != '' and @TipoMantenimiento != '' and @marca != ''  and @mes = '') SET @cadena = @cadena + '  where  FS.TipoMantenimiento = ''' + @TipoMantenimiento + ''' and FS.Marca = ''' + @marca + ''' and FS.Año = ''' + @año + ''' group by  FS.SistemaImplicado order by CantidadFalla asc' " +
       " IF(@año != '' and @TipoMantenimiento != '' and @marca = ''  and @mes != '')SET @cadena = @cadena + ' where  FS.TipoMantenimiento = ''' + @TipoMantenimiento + ''' and FS.Mes= ''' + @mes + ''' and FS.Año = ''' + @año + ''' group by  FS.SistemaImplicado order by CantidadFalla asc '" +
       " IF(@año != '' and @TipoMantenimiento != '' and @marca != ''  and @mes != '')SET @cadena = @cadena + ' where  FS.TipoMantenimiento = ''' + @TipoMantenimiento + '''and  FS.Marca = ''' + @marca + ''' and FS.Mes= ''' + @mes + '''  and FS.Año = ''' + @año + ''' group by  FS.SistemaImplicado order by CantidadFalla asc ' " +
       " IF(@año != '' and @TipoMantenimiento != '' and @marca = ''  and @mes = '')SET @cadena = @cadena + '  where   FS.TipoMantenimiento = ''' + @TipoMantenimiento + ''' and FS.Año = ''' + @año + ''' group by  FS.SistemaImplicado order by CantidadFalla asc '" +
      "  exec(@cadena)";
            return consulta;
        }
        public String Mantenimiento_FallasSistema_SistemaImplicado_TipoMantenimiento_SQL(String marca, String anio, String mes)
        {//Giampiere Parimango -15 / 09 / 2019
            consulta =
        " declare @marca varchar(max),@año varchar(6), @mes varchar(3);" +
          " set @marca = '" + marca + "'; set @año = '" + anio + "'; set @mes = '" + mes + "';" +
         " IF(@año ='' and @marca != '' and @mes !='') select distinct TipoMantenimiento FROM DTM_Mantenimiento_Fallas_Sistema where Marca=@marca and Mes=@mes" +
         " IF(@año ='' and @marca != '' and @mes ='')select distinct TipoMantenimiento FROM DTM_Mantenimiento_Fallas_Sistema where Marca=@marca" +
         " IF(@año ='' and @marca = '' and @mes !='') select distinct TipoMantenimiento FROM DTM_Mantenimiento_Fallas_Sistema where Mes=@mes" +
         " IF(@año ='' and @marca = '' and @mes ='')select distinct TipoMantenimiento FROM DTM_Mantenimiento_Fallas_Sistema " +
         " IF(@año !='' and @marca != '' and @mes !='') select distinct TipoMantenimiento FROM DTM_Mantenimiento_Fallas_Sistema where Marca=@marca and Mes=@mes and Año=@año" +
         " IF(@año !='' and @marca != '' and @mes ='')select distinct TipoMantenimiento FROM DTM_Mantenimiento_Fallas_Sistema where Marca=@marca and Año=@año" +
         " IF(@año !='' and @marca = '' and @mes !='') select distinct TipoMantenimiento FROM DTM_Mantenimiento_Fallas_Sistema where Mes=@mes  and Año=@año" +
         " IF(@año !='' and @marca = '' and @mes ='')select distinct TipoMantenimiento FROM DTM_Mantenimiento_Fallas_Sistema where Año=@año";

            return consulta;
        }
       
        public String Mantenimiento_FallasSistema_SistemaImplicado_Marca_SQL(String tipoMantenimiento, String anio, String mes)
        {//Giampiere Parimango -15 / 09 / 2019
            consulta =
        " declare @TipoMantenimiento varchar(max),@año varchar(6), @mes varchar(3);" +
         " set @TipoMantenimiento = '" + tipoMantenimiento + "'; set @año = '" + anio + "'; set @mes = '" + mes + "';" +
         " IF(@año ='' and @TipoMantenimiento != '' and @mes !='') select distinct Marca FROM DTM_Mantenimiento_Fallas_Sistema where TipoMantenimiento=@TipoMantenimiento and Mes=@mes" +
         " IF(@año ='' and @TipoMantenimiento != '' and @mes ='')select distinct Marca FROM DTM_Mantenimiento_Fallas_Sistema where TipoMantenimiento=@TipoMantenimiento" +
         " IF(@año ='' and @TipoMantenimiento = '' and @mes !='') select distinct Marca FROM DTM_Mantenimiento_Fallas_Sistema where Mes=@mes" +
         " IF(@año ='' and @TipoMantenimiento = '' and @mes ='')select distinct Marca FROM DTM_Mantenimiento_Fallas_Sistema " +
         " IF(@año !='' and @TipoMantenimiento != '' and @mes !='') select distinct Marca FROM DTM_Mantenimiento_Fallas_Sistema where TipoMantenimiento=@TipoMantenimiento and Mes=@mes and Año=@año" +
         " IF(@año !='' and @TipoMantenimiento != '' and @mes ='')select distinct Marca FROM DTM_Mantenimiento_Fallas_Sistema where TipoMantenimiento=@TipoMantenimiento and Año=@año" +
         " IF(@año !='' and @TipoMantenimiento = '' and @mes !='') select distinct Marca FROM DTM_Mantenimiento_Fallas_Sistema where Mes=@mes  and Año=@año" +
         " IF(@año !='' and @TipoMantenimiento = '' and @mes ='')select distinct Marca FROM DTM_Mantenimiento_Fallas_Sistema where Año=@año";
            return consulta;
        }
        public String Mantenimiento_FallasSistema_SistemaImplicado_Anio_SQL(String tipoMantenimiento, String marca, String mes)
        {//Giampiere Parimango -15 / 09 / 2019
            consulta =
          " declare @TipoMantenimiento varchar(max),@Marca varchar(max), @mes varchar(3);" +
         " set @TipoMantenimiento = '" + tipoMantenimiento + "'; set @Marca = '" + marca + "'; set @mes = '" + mes + "';" +
         " IF(@Marca ='' and @TipoMantenimiento != '' and @mes !='') select distinct Año FROM DTM_Mantenimiento_Fallas_Sistema where TipoMantenimiento=@TipoMantenimiento and Mes=@mes order by Año " +
         " IF(@Marca ='' and @TipoMantenimiento != '' and @mes ='')select distinct Año FROM DTM_Mantenimiento_Fallas_Sistema where TipoMantenimiento=@TipoMantenimiento order by Año" +
         " IF(@Marca ='' and @TipoMantenimiento = '' and @mes !='') select distinct Año FROM DTM_Mantenimiento_Fallas_Sistema where Mes=@mes order by Año" +
         " IF(@Marca ='' and @TipoMantenimiento = '' and @mes ='')select distinct Año FROM DTM_Mantenimiento_Fallas_Sistema  order by Año" +
         " IF(@Marca !='' and @TipoMantenimiento != '' and @mes !='') select distinct Año FROM DTM_Mantenimiento_Fallas_Sistema where TipoMantenimiento=@TipoMantenimiento and Mes=@mes and Marca=@Marca order by Año" +
         " IF(@Marca !='' and @TipoMantenimiento != '' and @mes ='')select distinct Año FROM DTM_Mantenimiento_Fallas_Sistema where TipoMantenimiento=@TipoMantenimiento and Marca=@Marca order by Año" +
         " IF(@Marca !='' and @TipoMantenimiento = '' and @mes !='') select distinct Año FROM DTM_Mantenimiento_Fallas_Sistema where Mes=@mes  and Marca=@Marca order by Año" +
         " IF(@Marca !='' and @TipoMantenimiento = '' and @mes ='')select distinct Año FROM DTM_Mantenimiento_Fallas_Sistema where Marca=@Marca order by Año";
            return consulta;
        }
        public String Mantenimiento_FallasSistema_SistemaImplicado_Mes_SQL(String tipoMantenimiento, String marca, String anio)
        {//Giampiere Parimango -15 / 09 / 2019
            consulta =
          " declare @TipoMantenimiento varchar(max),@Marca varchar(max), @Año varchar(max);" +
         " set @TipoMantenimiento = '" + tipoMantenimiento + "'; set @Marca = '" + marca + "'; set @Año = '" + anio + "';" +
         " IF(@Marca ='' and @TipoMantenimiento != '' and @Año !='') select distinct DATENAME(MONTH,Fecha) as Meses ,Mes FROM DTM_Mantenimiento_Fallas_Sistema where TipoMantenimiento=@TipoMantenimiento and Año=@Año order by Mes" +
         " IF(@Marca ='' and @TipoMantenimiento != '' and @Año ='')select distinct DATENAME(MONTH,Fecha) as Meses ,Mes FROM DTM_Mantenimiento_Fallas_Sistema where TipoMantenimiento=@TipoMantenimiento order by Mes" +
         " IF(@Marca ='' and @TipoMantenimiento = '' and @Año !='') select distinct DATENAME(MONTH,Fecha) as Meses ,Mes FROM DTM_Mantenimiento_Fallas_Sistema where Año=@Año order by Mes" +
         " IF(@Marca ='' and @TipoMantenimiento = '' and @Año ='')select distinct DATENAME(MONTH,Fecha) as Meses ,Mes FROM DTM_Mantenimiento_Fallas_Sistema order by Mes " +
         " IF(@Marca !='' and @TipoMantenimiento != '' and @Año !='') select distinct DATENAME(MONTH,Fecha) as Meses ,Mes FROM DTM_Mantenimiento_Fallas_Sistema where TipoMantenimiento=@TipoMantenimiento and Año=@Año and Marca=@Marca order by Mes" +
         " IF(@Marca !='' and @TipoMantenimiento != '' and @Año ='')select distinct DATENAME(MONTH,Fecha) as Meses ,Mes FROM DTM_Mantenimiento_Fallas_Sistema where TipoMantenimiento=@TipoMantenimiento and Marca=@Marca order by Mes" +
         " IF(@Marca !='' and @TipoMantenimiento = '' and @Año !='') select distinct DATENAME(MONTH,Fecha) as Meses ,Mes FROM DTM_Mantenimiento_Fallas_Sistema where Año=@Año  and Marca=@Marca order by Mes" +
         " IF(@Marca !='' and @TipoMantenimiento = '' and @Año ='')select distinct DATENAME(MONTH,Fecha) as Meses ,Mes FROM DTM_Mantenimiento_Fallas_Sistema where Marca=@Marca order by Mes";
            return consulta;
        }
        public String Mantenimiento_FallasSistema_PorcentajeSistemaImplicado_TipoMantenimiento_SQL(String marca, String anio, String  mes)
        {//Giampiere Parimango -15 / 09 / 2019
            consulta =
          " declare @marca varchar(max),@año varchar(6), @mes varchar(3);" +
          " set @marca = '" + marca + "'; set @año = '" + anio + "'; set @mes = '" + mes + "';" +
         " IF(@año ='' and @marca != '' and @mes !='') select distinct TipoMantenimiento FROM DTM_Mantenimiento_Fallas_Sistema where Marca=@marca and Mes=@mes" +
         " IF(@año ='' and @marca != '' and @mes ='')select distinct TipoMantenimiento FROM DTM_Mantenimiento_Fallas_Sistema where Marca=@marca" +
         " IF(@año ='' and @marca = '' and @mes !='') select distinct TipoMantenimiento FROM DTM_Mantenimiento_Fallas_Sistema where Mes=@mes" +
         " IF(@año ='' and @marca = '' and @mes ='')select distinct TipoMantenimiento FROM DTM_Mantenimiento_Fallas_Sistema " +
         " IF(@año !='' and @marca != '' and @mes !='') select distinct TipoMantenimiento FROM DTM_Mantenimiento_Fallas_Sistema where Marca=@marca and Mes=@mes and Año=@año" +
         " IF(@año !='' and @marca != '' and @mes ='')select distinct TipoMantenimiento FROM DTM_Mantenimiento_Fallas_Sistema where Marca=@marca and Año=@año" +
         " IF(@año !='' and @marca = '' and @mes !='') select distinct TipoMantenimiento FROM DTM_Mantenimiento_Fallas_Sistema where Mes=@mes  and Año=@año" +
         " IF(@año !='' and @marca = '' and @mes ='')select distinct TipoMantenimiento FROM DTM_Mantenimiento_Fallas_Sistema where Año=@año";

            return consulta;
        }

        public String Mantenimiento_FallasSistema_PorcentajeSistemaImplicado_Marca_SQL(String tipoMantenimiento, String anio, String mes)
        {//Giampiere Parimango -15 / 09 / 2019
            consulta =
         " declare @TipoMantenimiento varchar(max),@año varchar(6), @mes varchar(3);" +
         " set @TipoMantenimiento = '" + tipoMantenimiento + "'; set @año = '" + anio + "'; set @mes = '" + mes + "';" +
         " IF(@año ='' and @TipoMantenimiento != '' and @mes !='') select distinct Marca FROM DTM_Mantenimiento_Fallas_Sistema where TipoMantenimiento=@TipoMantenimiento and Mes=@mes" +
         " IF(@año ='' and @TipoMantenimiento != '' and @mes ='')select distinct Marca FROM DTM_Mantenimiento_Fallas_Sistema where TipoMantenimiento=@TipoMantenimiento" +
         " IF(@año ='' and @TipoMantenimiento = '' and @mes !='') select distinct Marca FROM DTM_Mantenimiento_Fallas_Sistema where Mes=@mes" +
         " IF(@año ='' and @TipoMantenimiento = '' and @mes ='')select distinct Marca FROM DTM_Mantenimiento_Fallas_Sistema " +
         " IF(@año !='' and @TipoMantenimiento != '' and @mes !='') select distinct Marca FROM DTM_Mantenimiento_Fallas_Sistema where TipoMantenimiento=@TipoMantenimiento and Mes=@mes and Año=@año" +
         " IF(@año !='' and @TipoMantenimiento != '' and @mes ='')select distinct Marca FROM DTM_Mantenimiento_Fallas_Sistema where TipoMantenimiento=@TipoMantenimiento and Año=@año" +
         " IF(@año !='' and @TipoMantenimiento = '' and @mes !='') select distinct Marca FROM DTM_Mantenimiento_Fallas_Sistema where Mes=@mes  and Año=@año" +
         " IF(@año !='' and @TipoMantenimiento = '' and @mes ='')select distinct Marca FROM DTM_Mantenimiento_Fallas_Sistema where Año=@año";
            return consulta;
        }
        public String Mantenimiento_FallasSistema_PorcentajeSistemaImplicado_Anio_SQL(String tipoMantenimiento, String marca, String mes)
        {
            consulta =
         " declare @TipoMantenimiento varchar(max),@Marca varchar(max), @mes varchar(max);" +
         " set @TipoMantenimiento = '" + tipoMantenimiento + "'; set @Marca = '" + marca + "'; set @mes = '" + mes + "';" +
         " IF(@Marca ='' and @TipoMantenimiento != '' and @mes !='') select distinct Año FROM DTM_Mantenimiento_Fallas_Sistema where TipoMantenimiento=@TipoMantenimiento and Mes=@mes order by Año " +
         " IF(@Marca ='' and @TipoMantenimiento != '' and @mes ='')select distinct Año FROM DTM_Mantenimiento_Fallas_Sistema where TipoMantenimiento=@TipoMantenimiento order by Año" +
         " IF(@Marca ='' and @TipoMantenimiento = '' and @mes !='') select distinct Año FROM DTM_Mantenimiento_Fallas_Sistema where Mes=@mes order by Año" +
         " IF(@Marca ='' and @TipoMantenimiento = '' and @mes ='')select distinct Año FROM DTM_Mantenimiento_Fallas_Sistema  order by Año" +
         " IF(@Marca !='' and @TipoMantenimiento != '' and @mes !='') select distinct Año FROM DTM_Mantenimiento_Fallas_Sistema where TipoMantenimiento=@TipoMantenimiento and Mes=@mes and Marca=@Marca order by Año" +
         " IF(@Marca !='' and @TipoMantenimiento != '' and @mes ='')select distinct Año FROM DTM_Mantenimiento_Fallas_Sistema where TipoMantenimiento=@TipoMantenimiento and Marca=@Marca order by Año" +
         " IF(@Marca !='' and @TipoMantenimiento = '' and @mes !='') select distinct Año FROM DTM_Mantenimiento_Fallas_Sistema where Mes=@mes  and Marca=@Marca order by Año" +
         " IF(@Marca !='' and @TipoMantenimiento = '' and @mes ='')select distinct Año FROM DTM_Mantenimiento_Fallas_Sistema where Marca=@Marca order by Año";
            return consulta;
        }
        public String Mantenimiento_FallasSistema_PorcentajeSistemaImplicado_Mes_SQL(String tipoMantenimiento, String marca, String anio)
        {//Giampiere Parimango -15 / 09 / 2019
            consulta =
         " declare @TipoMantenimiento varchar(max),@Marca varchar(max), @Año varchar(max);" +
         " set @TipoMantenimiento = '" + tipoMantenimiento + "'; set @Marca = '" + marca + "'; set @Año = '" + anio + "';" +
         " IF(@Marca ='' and @TipoMantenimiento != '' and @Año !='') select distinct DATENAME(MONTH,Fecha) as Meses ,Mes FROM DTM_Mantenimiento_Fallas_Sistema where TipoMantenimiento=@TipoMantenimiento and Año=@Año order by Mes" +
         " IF(@Marca ='' and @TipoMantenimiento != '' and @Año ='')select distinct DATENAME(MONTH,Fecha) as Meses ,Mes FROM DTM_Mantenimiento_Fallas_Sistema where TipoMantenimiento=@TipoMantenimiento order by Mes" +
         " IF(@Marca ='' and @TipoMantenimiento = '' and @Año !='') select distinct DATENAME(MONTH,Fecha) as Meses ,Mes FROM DTM_Mantenimiento_Fallas_Sistema where Año=@Año order by Mes" +
         " IF(@Marca ='' and @TipoMantenimiento = '' and @Año ='')select distinct DATENAME(MONTH,Fecha) as Meses ,Mes FROM DTM_Mantenimiento_Fallas_Sistema order by Mes " +
         " IF(@Marca !='' and @TipoMantenimiento != '' and @Año !='') select distinct DATENAME(MONTH,Fecha) as Meses ,Mes FROM DTM_Mantenimiento_Fallas_Sistema where TipoMantenimiento=@TipoMantenimiento and Año=@Año and Marca=@Marca order by Mes" +
         " IF(@Marca !='' and @TipoMantenimiento != '' and @Año ='')select distinct DATENAME(MONTH,Fecha) as Meses ,Mes FROM DTM_Mantenimiento_Fallas_Sistema where TipoMantenimiento=@TipoMantenimiento and Marca=@Marca order by Mes" +
         " IF(@Marca !='' and @TipoMantenimiento = '' and @Año !='') select distinct DATENAME(MONTH,Fecha) as Meses ,Mes FROM DTM_Mantenimiento_Fallas_Sistema where Año=@Año  and Marca=@Marca order by Mes" +
         " IF(@Marca !='' and @TipoMantenimiento = '' and @Año ='')select distinct DATENAME(MONTH,Fecha) as Meses ,Mes FROM DTM_Mantenimiento_Fallas_Sistema where Marca=@Marca order by Mes";
            return consulta;
        }
        //kpi: Tiempo promedio de fallas - Historico
        //public String Filtro_Fecha_TMF = "select distinct FechaCompra as Fecha from DTM_Mantenimiento_Tiempo_Medio_Fallas";


        public String Filtro_Fecha_TMF = "  SELECT  (TRY_CONVERT(date, '1/' + CONVERT(varchar(10), dtm.Mes) + '/' + CONVERT(varchar(10), dtm.Año), 103)) as Fecha from[DTM_Mantenimiento_Tiempo_Medio_Fallas] dtm  GROUP BY dtm.Año,dtm.Mes order by dtm.Año ";

        public String G1_Tiempo_Fallas_Fallas(String anio, String mes)
        {//Giampiere Parimango -16 / 09 / 2019
            consulta =
          " declare @año varchar(max), @mes varchar(max) , @filtro NVARCHAR(MAX), @cols NVARCHAR(MAX), @null NVARCHAR(MAX), @consulta NVARCHAR(MAX);  " +
          " set @año = '" + anio + "'; set @mes = '" + mes + "';  " +
          " select @cols = STUFF((SELECT Distinct ',' + QUOTENAME(Marca)  from [dbo].[DTM_Mantenimiento_Tiempo_Medio_Fallas] group by ',' + QUOTENAME(Marca)Order by ',' + QUOTENAME(Marca) FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)')  ,1,1,'')  " +
          " select @null = STUFF((SELECT Distinct ', isnull(' + QUOTENAME(Marca)+',0) as'  + QUOTENAME(Marca) from [dbo].[DTM_Mantenimiento_Tiempo_Medio_Fallas]  group by ', isnull(' + QUOTENAME(Marca)+',0) as'  + QUOTENAME(Marca) Order by ', isnull(' + QUOTENAME(Marca)+',0) as'  + QUOTENAME(Marca)   FOR XML PATH(''), TYPE  ).value('.', 'NVARCHAR(MAX)')   ,1,1,'') " +
          " IF(@año =''  and @mes !='')  set @filtro = 'where Mes='''+@mes+''''" +
          " IF(@año ='' and @mes ='') set @filtro = ' '" +
          " IF(@año !='' and @mes !='')  set @filtro = 'where Mes='''+@mes+''' and Año='''+@año+'''' " +
          " IF(@año !=''  and @mes ='')set @filtro = ' where Año='''+@año+''''" +
          " set @consulta= 'SELECT concat(Meses,'' de '',Año) as MesAño,' + @null + '  from  ( select Mes, DATENAME(MONTH,Fecha) as Meses,Año,Marca,KPI_HISTORICO from [dbo].[DTM_Mantenimiento_Tiempo_Medio_Fallas]  '+@filtro+ '  ) x  pivot ( avg(KPI_HISTORICO) for Marca in (' + @cols + ') ) p  order by Año,Mes' " +
          " execute(@consulta)";
            return consulta;
        }
        public String Mantenimiemto_Tiempo_Fallas_Fallas_anio_SQL(String mes)
        {//Giampiere Parimango -16 / 09 / 2019
            consulta =
            " declare  @mes varchar(max); " +
            " set @mes = '" + mes + "'" +
            " IF( @mes !='') select distinct Año FROM [DTM_Mantenimiento_Tiempo_Medio_Fallas] where  Mes=@mes order by Año " +
            " IF( @mes ='')select distinct Año FROM [DTM_Mantenimiento_Tiempo_Medio_Fallas]  order by Año ";
          return consulta;
        }
        public String Mantenimiemto_Tiempo_Fallas_Fallas_Mes_SQL(String anio)
        {//Giampiere Parimango -16 / 09 / 2019
            consulta =
            " declare  @año varchar(max) " +
            " set @año = '" + anio + "' " +
            " IF( @año !='') select distinct DATENAME(MONTH,Fecha) as Meses ,Mes FROM [DTM_Mantenimiento_Tiempo_Medio_Fallas]where  Año=@año order by Mes  "+
            " IF( @año ='')select distinct DATENAME(MONTH,Fecha) as Meses ,Mes FROM [DTM_Mantenimiento_Tiempo_Medio_Fallas] order by Mes ";

          return consulta;
        }

        //kpi: Tiempo promedio de fallas - Anual
        public String Filtro_Anio_TMF_Anual = "select distinct Año as Anio from DTM_Mantenimiento_Tiempo_Medio_Fallas order by Año";

        public String Filtro_Marca_TMF_Anual = "select distinct dtm.Marca  from [dbo].[DTM_Mantenimiento_Tiempo_Medio_Fallas] dtm where dtm.Marca='SCANIA' or dtm.Marca='MERCEDES BENZ'";

        public String Tiempo_Fallas_Fallas_Anual(String filtroMarca, String anio)
        {
            // consulta = "select distinct dtm.Mes, '2017'= (isNULL((select avg(cc.KPI_ANUAL) from [dbo].[DTM_Mantenimiento_Tiempo_Medio_Fallas] cc where cc.Mes=dtm.Mes and cc.Año='2017'),0) ), '2018'= (isNULL((select avg(cc.KPI_ANUAL) from [dbo].[DTM_Mantenimiento_Tiempo_Medio_Fallas] cc where cc.Mes=dtm.Mes and cc.Año='2018'),0) ), '2019'= (isNULL((select avg(cc.KPI_ANUAL) from [dbo].[DTM_Mantenimiento_Tiempo_Medio_Fallas] cc where cc.Mes=dtm.Mes and cc.Año='2019'),0) ) from [dbo].[DTM_Mantenimiento_Tiempo_Medio_Fallas] dtm group by DTM.Año, DTM.Mes";
            consulta =

                "declare @marca varchar(20), @anio varchar(5) set @marca = '" + filtroMarca + "'; set @anio = '" + anio + "'; IF(@marca = '' and @anio = '') select distinct dtm.Mes,  '2017' = CAST((isNULL((select avg(cc.KPI_ANUAL) from[dbo].[DTM_Mantenimiento_Tiempo_Medio_Fallas] cc where cc.Mes = dtm.Mes and cc.Año = '2017'), 0)) as decimal(10, 2)),  '2018' = CAST((isNULL((select avg(cc.KPI_ANUAL) from[dbo].[DTM_Mantenimiento_Tiempo_Medio_Fallas] cc where cc.Mes = dtm.Mes and cc.Año = '2018'), 0)) as decimal(10, 2)),  '2019' = CAST((isNULL((select avg(cc.KPI_ANUAL) from[dbo].[DTM_Mantenimiento_Tiempo_Medio_Fallas] cc where cc.Mes = dtm.Mes and cc.Año = '2019'), 0)) as decimal(10, 2))  from[dbo].[DTM_Mantenimiento_Tiempo_Medio_Fallas] dtm where DTM.Marca = 'SCANIA'OR DTM.Marca = 'MERCEDES BENZ'   group by DTM.Año, DTM.Mes,DTM.Marca IF(@marca != '' and @anio = '') select distinct dtm.Mes,  '2017' = CAST((isNULL((select avg(cc.KPI_ANUAL) from[dbo].[DTM_Mantenimiento_Tiempo_Medio_Fallas] cc where cc.Mes = dtm.Mes and cc.Año = '2017' AND  cc.Marca = @marca), 0)) as decimal(10, 2)),  '2018' = CAST((isNULL((select avg(cc.KPI_ANUAL) from[dbo].[DTM_Mantenimiento_Tiempo_Medio_Fallas] cc where cc.Mes = dtm.Mes and cc.Año = '2018' AND  cc.Marca = @marca), 0)) as decimal(10, 2)),  '2019' = CAST((isNULL((select avg(cc.KPI_ANUAL) from[dbo].[DTM_Mantenimiento_Tiempo_Medio_Fallas] cc where cc.Mes = dtm.Mes and cc.Año = '2019' AND  cc.Marca = @marca), 0)) as decimal(10, 2))  from[dbo].[DTM_Mantenimiento_Tiempo_Medio_Fallas] dtm where dtm.Marca = @marca  group by DTM.Año, DTM.Mes,DTM.Marca IF(@marca = '' and @anio != '') select distinct dtm.Mes,  'AÑO' = CAST((isNULL((select avg(cc.KPI_ANUAL) from[dbo].[DTM_Mantenimiento_Tiempo_Medio_Fallas] cc where cc.Mes = dtm.Mes and cc.Año = @anio), 0)) as decimal(10, 2))  from[dbo].[DTM_Mantenimiento_Tiempo_Medio_Fallas] dtm where(DTM.Marca= 'SCANIA'OR DTM.Marca= 'MERCEDES BENZ') and dtm.Año = @anio  group by DTM.Año, DTM.Mes,DTM.Marca IF(@marca != '' and @anio != '') select distinct dtm.Mes,  'AÑO' = CAST((isNULL((select avg(cc.KPI_ANUAL) from[dbo].[DTM_Mantenimiento_Tiempo_Medio_Fallas] cc where cc.Mes = dtm.Mes and cc.Año = @anio and cc.Marca = @marca), 0)) as decimal(10, 2))  from[dbo].[DTM_Mantenimiento_Tiempo_Medio_Fallas] dtm where(DTM.Marca= 'SCANIA'OR DTM.Marca= 'MERCEDES BENZ') and dtm.Marca = @marca and dtm.Año = @anio  group by DTM.Año, DTM.Mes,DTM.Marca";
            return consulta;
        }

        //kpi: Productividad
        public String Productividad_Total_KPI(String parametro)
        {
            consulta = "declare @anio varchar(5)  set @anio = '" + parametro + "';  IF(@anio!='') select mp.Mes, 'Año'= isnull( (CAST((select avg(dtm.KPI) from DTM_Mantenimiento_Productividad dtm where dtm.Anual=@anio and dtm.Mes=mp.Mes) as decimal(10,2))),0) FROM DTM_Mantenimiento_Productividad mp group by mp.Mes  ELSE  select mp.Mes,   '2017'= isnull( (CAST((select avg(dtm.KPI) from DTM_Mantenimiento_Productividad dtm where dtm.Anual='2017' and dtm.Mes=mp.Mes) as decimal(10,2))),0),    '2018'= isnull( (CAST((select avg(dtm.KPI) from DTM_Mantenimiento_Productividad dtm where dtm.Anual='2018' and dtm.Mes=mp.Mes) as decimal(10,2))),0),   '2019'= isnull( (CAST((select avg(dtm.KPI) from DTM_Mantenimiento_Productividad dtm where dtm.Anual='2019' and dtm.Mes=mp.Mes) as decimal(10,2))),0)    FROM DTM_Mantenimiento_Productividad mp group by mp.Mes ";
            return consulta;
        }

        public String filtro_anio_productividad_total = "select distinct mp.Anual FROM DTM_Mantenimiento_Productividad mp order by mp.Anual";


        //KPI DE COSTOS EXTERNOS
        
        #region SubRegion_KPI_COSTOS_MANTENIMIENTO
        //Funcion Que consulta datos para la Grafica LINE CHART 
        public String Grafica_Mantenimiento_CostosPersonalRepuestos_LCH_SQL(String Anio, String Mes)
        {

            consulta =
            "  declare @Mes nvarchar(10),@Anio nvarchar(10), @filtro  NVARCHAR(MAX), @cols NVARCHAR(MAX), @null NVARCHAR(MAX), @consulta NVARCHAR(MAX); "+
            "   set @Mes='"+Mes+"' ;set @Anio='"+Anio+"'; set @filtro =''; set @consulta =''; "+
            "   select @cols = STUFF((SELECT Distinct ',' + QUOTENAME(Anual)  from DTM_Mantenimiento_Costos_Personal_Repuestos group by ',' + QUOTENAME(Anual)Order by ',' + QUOTENAME(Anual) FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)')  ,1,1,'') "+
            "   select @null = STUFF((SELECT Distinct ', isnull(' + QUOTENAME(Anual)+',0) as'  + QUOTENAME(Anual) from DTM_Mantenimiento_Costos_Personal_Repuestos  group by ', isnull(' + QUOTENAME(Anual)+',0) as'  + QUOTENAME(Anual) Order by ', isnull(' + QUOTENAME(Anual)+',0) as'  + QUOTENAME(Anual)   FOR XML PATH(''), TYPE  ).value('.', 'NVARCHAR(MAX)')   ,1,1,'') " +
            "    if  (@Mes = '' and @Anio =''  )  	  set @consulta= 'SELECT  Meses,' + @null + '  from  ( select Mes, DATENAME(MONTH,Fecha) as Meses,Anual,Total= round ((Sum(ImportePlanilla) + SUM(ImporteRepuestos)), 0)  from  DTM_Mantenimiento_Costos_Personal_Repuestos  group by Mes,DATENAME(MONTH,Fecha) ,Anual  ) x  pivot ( sum(Total) for Anual in (' + @cols + ') ) pvt  order by Mes'  "+
            "   else  begin  " +
            "   if  (@Mes != '' and @Anio != '')  	  set @filtro = '  Anual = '''+@Anio+''' and  Mes = '''+@Mes+'''  ' "+
            "   if  (@Mes != '' and @Anio = ''  )  	 set @filtro = '  Mes = '''+@Mes+''' ' "+
            "    if  (@Mes = '' and  @Anio!= '' )   set @filtro = ' Anual = '''+@Anio+'''  ' " +
            "   set @consulta= 'SELECT Meses, ' + @null + '  from  ( select Mes, DATENAME(MONTH,Fecha) as Meses,Anual, Total= round( (sum(ImportePlanilla) + sum(ImporteRepuestos) ),0) from  DTM_Mantenimiento_Costos_Personal_Repuestos  where '+@filtro+'  group by Mes ,Anual,DATENAME(MONTH,Fecha) ) x  pivot ( sum(Total) for Anual in (' + @cols + ') ) p  order by Mes'  " +
            "  end" +
            "  execute(@consulta) " ;
            return consulta;
        }

        public String Mantenimiento_CostoPersonalRepuestos_ListarMes_SQL( String Anio)
        {

            consulta = " declare @Año varchar(20), @Departamento varchar(20),@Consulta nvarchar(max),@filtro nvarchar(max),@select nvarchar(max); " +
           "  set @Año='" + Anio + "'; set @Departamento='Mantenimiento'; set @Consulta= ''; set @filtro= ''; set @select= ''; " +
           "  set @select= N'select distinct Mes from [DTM_Mantenimiento_Costos_Personal_Repuestos] ' " +
           "  if( @Año='' ) set @filtro = @select +  ' where  Departamento = '''+@Departamento+''' ' " +
           "  if ( @Año != ''  )   set @filtro = @select +  ' where  Departamento = '''+@Departamento+''' and Anual = '''+@Año+'''' " +
           "  set @Consulta= @filtro+ ' order by Mes';" +
           "  exec sp_sqlexec @Consulta";

            return consulta;
           
        }

        public String Mantenimiento_CostoPersonalRepuestos_ListarAño_SQL( String Mes)
        {
            consulta =
            " declare @Mes varchar(20), @Departamento varchar(20),@Consulta nvarchar(max),@filtro nvarchar(max),@select nvarchar(max); " +
            "  set @Mes='" + Mes + "'; set @Departamento='Mantenimiento'; set @Consulta= ''; set @filtro= ''; set @select= ''; " +
            "  set @select= N'select distinct Anual as Año from [DTM_Mantenimiento_Costos_Personal_Repuestos] ' " +
            "  if( @Mes='' ) set @filtro = @select +  ' where  Departamento = '''+@Departamento+''' '" +
            "  if ( @Mes != ''  )   set @filtro = @select+  ' where  Departamento = '''+@Departamento+''' and Mes = '''+@Mes+'''' " +
            "  set @Consulta= @filtro+ ' order by Anual'; " +
            "  exec sp_sqlexec @Consulta ";
            return consulta;
        }


        //Funcion que consulta datos para la Grafica GROUPED CHART 
        public String Grafica_Mantenimiento_CostosPersonalRepuestos_GCH_SQL(String Anio,String Mes)
        {
            consulta =
            "  declare @Mes nvarchar(10),@Anio nvarchar(10), @filtro  NVARCHAR(MAX), @anioActual NVARCHAR(MAX), @consulta NVARCHAR(MAX),  @cadena varchar(max);"+
            "  set @Mes='"+Mes+"' ;set @Anio='"+Anio+"'; set @filtro =''; set @consulta ='';  set @cadena='';"+
            "  select @anioActual = YEAR(GETDATE());   "+
            "  IF  (@Mes = '' and @Anio ='' )  	  "+
            "  set @consulta= ' SELECT Meses, MontoPlanilla,  MontoRepuestos  from (select Anual, Mes,DATENAME(MM,Fecha) as Meses ,round(sum(ImportePlanilla),0) as MontoPlanilla, cast(round(sum(ImporteRepuestos),0) as decimal(20,0)) as MontoRepuestos from DTM_Mantenimiento_Costos_Personal_Repuestos  Group by Anual, Mes ,DATENAME(MM,Fecha) ) DTM_Mantenimiento_Costos_Personal_Repuestos  where Anual='+@anioActual +' Group by Anual, Meses,Mes, MontoPlanilla,  MontoRepuestos order by Mes '   "+
            "  ELSE   "+
            "  begin   "+
            "  if  (@Mes! = '' and @Anio != '')  	  set @filtro = '  Anual = '''+@Anio+''' and  Mes = '''+@Mes+'''  '   "+ 
            "  if  (@Mes != '' and @Anio = ''  )  	 set @filtro = '  Mes = '''+@Mes+''' ' "+   
            "  if  (@Mes = '' and  @Anio!= '' )   set @filtro = ' Anual = '''+@Anio+'''  '   "+ 
            "  set @consulta = 'SELECT  Meses, MontoPlanilla,  MontoRepuestos  from (select Anual, Mes,DATENAME(MM,Fecha) as Meses ,round(sum(ImportePlanilla),0) as MontoPlanilla, cast(round(sum(ImporteRepuestos),0) as decimal(20,0)) as MontoRepuestos from DTM_Mantenimiento_Costos_Personal_Repuestos  Group by Anual, Mes ,DATENAME(MM,Fecha) ) DTM_Mantenimiento_Costos_Personal_Repuestos where '+  @filtro+' Group by Anual, Meses ,Mes, MontoPlanilla,  MontoRepuestos order by Mes' "+
            "  end  "+
            "  exec (@consulta)  ";
            return consulta;

        }
            
        public String Mantenimiento_CostoPersonalRepuestos_ListarMes_GCH_SQL(String anio2)
        {

            consulta = " declare @Año varchar(20), @Departamento varchar(20),@Consulta nvarchar(max),@filtro nvarchar(max),@select nvarchar(max); " +
           "  set @Año='" + anio2 + "'; set @Departamento='Mantenimiento'; set @Consulta= ''; set @filtro= ''; set @select= ''; " +
           "  set @select= N'select distinct Mes from [DTM_Mantenimiento_Costos_Personal_Repuestos] ' " +
           "  if( @Año='' ) set @filtro = @select +  ' where  Departamento = '''+@Departamento+''' ' " +
           "  if ( @Año != ''  )   set @filtro = @select +  ' where  Departamento = '''+@Departamento+''' and Anual = '''+@Año+'''' " +
           "  set @Consulta= @filtro+ ' order by Mes';" +
           "  exec sp_sqlexec @Consulta";

            return consulta;

        }

        public String Mantenimiento_CostoPersonalRepuestos_ListarAño_GCH_SQL(String mes2)
        {
           consulta =
           " declare @Mes varchar(20), @Departamento varchar(20),@Consulta nvarchar(max),@filtro nvarchar(max),@select nvarchar(max); " +
           "  set @Mes='" + mes2+ "'; set @Departamento='Mantenimiento'; set @Consulta= ''; set @filtro= ''; set @select= ''; " +
           "  set @select= N'select distinct Anual as Año from [DTM_Mantenimiento_Costos_Personal_Repuestos] ' " +
           "  if( @Mes='' ) set @filtro = @select +  ' where  Departamento = '''+@Departamento+''' '" +
           "  if ( @Mes != ''  )   set @filtro = @select+  ' where  Departamento = '''+@Departamento+''' and Mes = '''+@Mes+'''' " +
           "  set @Consulta= @filtro+ ' order by Anual'; " +
           "  exec sp_sqlexec @Consulta";
           return consulta;
        }

        #endregion KPI Mantenimiento
        





        #endregion KPI Mantenimiento






    }
}
