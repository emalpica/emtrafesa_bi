﻿using System;

namespace ENTIDAD.Base
{
    public class TipoFlota
    {
        //FLOTAS:
        //BUS - CAMINONETA 
        public int idTipoFlota { get; set; }
        public String bus { get; set; }
        public String cargeros { get; set; }
        public String miniban { get; set; }
        public String estado { get; set; }


    }
}
