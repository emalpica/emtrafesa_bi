﻿using System;
using System.Collections.Generic;

namespace ENTIDAD.Graficas
{
    public class LineChartString
    {
        public List<String> Name { get; set; }
        public List<Contenid> Data { get; set; }
        //-------------
        public List<Contenid> Series { get; set; }
        public List<String> Fecha { get; set; }

        public LineChartString()
        {
            Fecha = new List<string>();
            Series = new List<Contenid>();
        }


        public LineChartString(List<String> iFechas, List<String> namee, List<Contenid> datea)
        {
            Name = namee;
            Data = datea;
            Fecha = iFechas;
        }
    }

    public class Contenid
    {
        public String name { get; set; }
        public List<String> data { get; set; }

        public Contenid()
        {
            data = new List<String>();
        }
    }
}
