﻿using System;
using System.Collections.Generic;

namespace ENTIDAD.Graficas
{
    public class LineChart
    {
        public List<String> Name { get; set; }
        public List<Ratio> Data { get; set; }
        //-------------
        public List<Ratio> Series { get; set; }
        public List<String> Fecha { get; set; }

        public LineChart()
        {
            Fecha = new List<string>();
            Series = new List<Ratio>();
        }






        public LineChart(List<String> iFechas, List<String> namee, List<Ratio> datea)
        {
            Name = namee;
            Data = datea;
            Fecha = iFechas;
        }
    }

    public class Ratio
    {
        public String name { get; set; }
        public List<Decimal> data { get; set; }

        public Ratio()
        {
            data = new List<Decimal>();
        }
    }

}
