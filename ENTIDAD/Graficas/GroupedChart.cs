﻿using System;
using System.Collections.Generic;

namespace ENTIDAD.Graficas
{
    public class GroupedChart
    {
        public List<String> Name { get; set; } // Guarda los nombres en barras
        public List<Ratios> Data { get; set; }
        public List<Ratios> Serie { get; set; }
        public List<String> Fecha { get; set; }


        //Constructor
        public GroupedChart()
        {
            Fecha = new List<string>();
            Serie = new List<Ratios>();

        }

        public GroupedChart(List<String> GFechas, List<String> GName, List<Ratio> GDate)
        {
            Name = GName;
            //Data = iDate;
            Fecha = GFechas;
        }

    }


    public class Ratios
    {
        public String nameG { get; set; }
        public List<decimal> dataG { get; set; }


        public Ratios()
        {
            dataG = new List<decimal>();
        }
    }
}
