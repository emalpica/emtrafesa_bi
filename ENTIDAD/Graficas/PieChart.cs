﻿using System;
using System.Collections.Generic;

namespace ENTIDAD.Graficas
{
    public class PieChart
    {
        public String name { get; set; }
        public Boolean colorByPoint { get; set; }
        public List<Contenido> data { get; set; }
        public Boolean parametroLine { get; set; }


        public PieChart()
        {
            name = "'Brands'";
            parametroLine = true;
            data = new List<Contenido>();
            colorByPoint = true;
        }

    }
    public class Contenido
    {
        public String name { get; set; }
        public Decimal y { get; set; }
        public Boolean sliced { get; set; }
        public Boolean selected { get; set; }
        //public List<Decimal> data { get; set; }

        public Contenido()
        {
            sliced = true;
            selected = true;
        }
    }

}

