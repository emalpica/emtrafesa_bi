﻿using System;
using System.Collections.Generic;

namespace ENTIDAD.Graficas
{
    public class BarrasChart
    {
        public List<String> Name { get; set; }
        public List<Modelo> Data { get; set; }
        //-------------
        public List<Modelo> Series { get; set; }
        public List<String> Fecha { get; set; }

        public BarrasChart()
        {
            Fecha = new List<String>();
            Series = new List<Modelo>();
        }

        public BarrasChart(List<String> iFechas, List<String> namee, List<Modelo> datea)
        {
            Name = namee;
            Data = datea;
            Fecha = iFechas;
        }
    }

    public class Modelo
    {
        public String name { get; set; }
        public int data { get; set; }
        //public Modelo()
        //{
        //    data = new List<int>();
        //}
    }
}
