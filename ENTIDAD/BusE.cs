﻿using System;

namespace ENTIDAD
{
    public class BusE
    {

        public int idBus { get; set; }
        public String placa { get; set; }
        public String modelo { get; set; }
        public String marca { get; set; }
        public String estado { get; set; }


    }
}
